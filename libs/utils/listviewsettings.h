/***************************************************************************
 *   Copyright (C) 2004 by Piotr Mierzwi?ski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _LISTVIEWSETTINGS_H_
#define _LISTVIEWSETTINGS_H_

#include <QColor>

enum {
	ItemColor=1,
	ItemColorUnderCursor,
	ColorOfSelectedItem,
	ColorOfSelectedItemUnderCursor,
// 	BackgroundColorOfSelectedItem,
// 	BackgroundColorOfSelectedItemUnderCursor,
	BackgroundColor = 8-2,
	SecondBackgroundColor,
	CursorColor = 10-2,
	HiddenFileColor = 12-2,
	ExecutableFileColor,
	SymlinkColor,
	BrokenSymlinkColor
//	GridColor
};// ListViewColors;

enum ColumnNames {
	NAMEcol=0, SIZEcol, TIMEcol, PERMcol, OWNERcol, GROUPcol, FTYPEcol
	//EXTNAMEcol=1
};

struct ColorsScheme {
	QString name;
	bool bAlternatingRowColors;
	bool bItemColorFromSystem;
	bool bCursorColorFromSystem;
	bool bBackgroundColorFromSystem;
	bool bSecondBackgroundColorFromSystem;
//	bool bTwoColorCursor;
//	bool bShowGrid;
	QColor itemColor;
	QColor itemColorUnderCursor;
	QColor colorOfSelectedItem;
	QColor colorOfSelectedItemUnderCursor;
// 	QColor backgroundColorOfSelectedItem;
// 	QColor backgroundColorOfSelectedItemUnderCursor;
	QColor backgroundColor;
	QColor secondBackgroundColor;
	QColor cursorColor;
	QColor hiddenFileColor;
	QColor executableFileColor;
	QColor symlinkColor;
	QColor brokenSymlinkColor;
// 	QColor gridColor;
};

struct ListViewSettings {
	// font page
	QString fontFamily;
	uint fontSize;
	bool fontBold;
	bool fontItalic;
	// colors page
	bool showSecondBackgroundColor;
	bool itemColorFromSystem;
	bool cursorColorFromSystem;
	bool backgroundColorFromSystem;
	bool secondBackgroundColorFromSystem;
	QColor itemColor;
	QColor itemColorUnderCursor;
	QColor colorOfSelectedItem;
	QColor colorOfSelectedItemUnderCursor;
// 	QColor backgroundColorOfSelectedItem;
// 	QColor backgroundColorOfSelectedItemUnderCursor;
	QColor backgroundColor;
	QColor secondBackgroundColor;
	QColor cursorColor;
	QColor hiddenFileColor;
	QColor executableFileColor;
	QColor symlinkColor;
	QColor brokenSymlinkColor;
	// columns page
	int columnsWidthTab[7];
	bool synchronizeColWidthInBothPanels;
	// other page
	bool showIcons;
	bool showHiddenFiles;
	bool boldedDir;
	bool bytesRound;
	bool selectDirs;

};

#endif // _LISTVIEWSETTINGS_H_
