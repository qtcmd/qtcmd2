/***************************************************************************
 *   Copyright (C) 2004 by Piotr Mierzwiñski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along withthisprogram; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _KEYSHORTCUTS_H
#define _KEYSHORTCUTS_H

#include <QList>
#include <QHash>
#include <QObject>
#include <QKeyEvent>

/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
/** @short Helper class. Holding description of single key code.
 * Short definition includes: action Id, action description, default key code,
 * valid key code.
 * Constructor is required to be able make key code.
 * The class share the methods to get and set code value, but possible is only
 * setting the key code.
 */
class KeyCode
{
public:
	/** Constructor of class.
	 * @param nActionId - action Id,
	 * @param sDesc - action description,
	 * @param nDefaultCode - default key code,
	 * @param nCode - valid key code.
	 * Here you can find only initialisation of members.
	 */
	KeyCode( int nActionId, const QString & sDesc, int nDefaultCode, int nCode = Qt::Key_unknown ) :
		m_nAction(nActionId), m_sDescription(sDesc), m_defaultCode(nDefaultCode), code(nCode)
	{}

	/** Descructor.
	 * Empty definition.
	 */
	virtual ~KeyCode() {}

	/** Returns action description.
	 * @return action description as string.
	 */
	QString description() const { return m_sDescription; }

	/** Returns default key code.
	 * @return default key code.
	 */
	QKeySequence defaultCode() const { return m_defaultCode; }

	/** Returns action Id.
	 * @return action Id.
	 */
	int action() const { return m_nAction; }

	/** Sets default code for given keyCode.
	 * @param keyCode key code
	 */
	void setDefaultCode( const QKeySequence &keyCode ) { m_defaultCode = keyCode; }

	/** Set given description for key code.
	 * @param sDesc description
	 */
	void setDescription( const QString &sDesc ) { m_sDescription = sDesc; }

private:
	int m_nAction;
	QString m_sDescription;
	QKeySequence m_defaultCode;

public:
	/** Current key code.
	 */
	QKeySequence code;

};

// ooooooooooo-----------         -------------ooooooooooo

/** @short Class holds key shortcuts and provides API to managing them.
*/
class KeyShortcuts : public QObject
{
public:
	static const int ACTION_ID_OFFSET = 1001;

	/** Constructor (empty definition).
	 */
	KeyShortcuts();

	/** Destructor (empty definition).
	 */
	virtual ~KeyShortcuts() {}

	/** Updates all shortcuts in configuration file.
	 * @param sConfigFileKey key to key shortcuts section
	 */
	virtual void updateEntryList( const QString &sConfigFileKey );

	/** Returns pointer to KeyCode object for given ID.
	 * @param nActionId - ID of action
	 * @return pointer to KeyCode object
	 */
	virtual KeyCode *key( int nActionId ) const;

	/** Returns keyCode for given action ID.
	 * @param nActionId ID of action
	 * @return key code
	 */
	virtual const QKeySequence & keyCode( int nActionId ) const;

	/** Returns key shortcut as string for given action ID.
	 * @param nActionId ID of action
	 * @return key shortcut as string
	 */
	virtual QString keyStr( int nActionId ) const;

	/** Returns description for given action ID.
	 * @param nActionId ID of action
	 * @return description for given action ID
	 */
	const QString keyDesc( int nActionId ) const;

	/** Returns string of default key shortcut for given Id.
	 * @param nActionId Id of action
	 * @return string describing shortcut
	 */
	virtual QString keyDefaultStr( int nActionId ) const;

	/** Sets new key shortcut (placed it into hash with nAction as key), default key shortcut and description key shortcut
	 * @param nActionId - Id of action (look at: @see ActionsOfView, oraz @see ActionsOfApp),
	 * @param nNewShortcut - new key shortcut
	 * @param nDefaultShortcut - default key shortcut
	 * @param sDescription - description key shortcut
	 */
	void setKey( int nActionId, int nNewShortcut, int nDefaultShortcut, const QString &sDescription="" );

	/** Sets new key shortcut (placed it into hash with nAction as key)
	 * @param nActionId - Id of action (look at: @see ActionsOfView, oraz @see ActionsOfApp),
	 * @param sNewShortcut - new key shortcut
	 */
	void setKey( const int nActionId, const QString &sNewShortcut );

	/** Returns action description.
	 * @param nActionId Id of action
	 * @return action description as string.
	 */
	QString desc( uint nActionId ) const {
		return (m_keyCodes.value(nActionId+ACTION_ID_OFFSET))->description();
		//return (*m_KeyCodeList.at( nIndex )).description();
	}

	/** Returns action description.
	 * @param sShortcut key shortcut
	 * @return action description as string.
	 */
	const QString keyDesc( const QString &sShortcut );

	/** Returns number of added items.
	 * @return number of added items.
	 */
	int count() const { return m_keyCodes.count(); }

	/** Cleans list of items.
	 */
	void clear();


	/** Converts given object with type QKeyEvent to QKeySequence and returns QKeySequence type object.
	 * @param pKeyEvent - QKeyEvent pointer,
	 * @return QKeySequence type object
	 */
	QKeySequence ke2ks( QKeyEvent* pKeyEvent );


	/** Returns iterator pointing to the fist item in KeyCodes hash list.
	 * @return iterator for first item
	 */
	QHash <int, KeyCode*>::iterator fistItem() {
		return m_keyCodes.begin(); // QHash::iterator begin()
	}

	/** Returns iterator pointing to the last item in KeyCodes hash list.
	 * @return iterator for last item
	 */
	QHash <int, KeyCode*>::iterator lastItem() {
		return m_keyCodes.end(); // QHash::iterator begin()
	}

private:
	typedef QHash<int, KeyCode*> KeyCodes; // ActionId, KeyCode
	KeyCodes m_keyCodes;

	QKeySequence m_KeySequence;

};

#endif
