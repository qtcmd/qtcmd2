/***************************************************************************
 *   Copyright (C) 2004 by Piotr Mierzwi?ski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _FILESYSTEMSETTINGS_H_
#define _FILESYSTEMSETTINGS_H_

#include <QString>

struct FileSystemSettings {
	// archives page
	QString workDirectoryPath;

	// lfs page
	bool moveToTrash;
	bool alwaysWeighBeforeCopy;
	bool alwaysWeighBeforeMove;
	bool alwaysWeighBeforeRemove;
	QString pathToTrash;

	// ftp page
	bool standByConnection;
	int  numOfTimeToRetryIfFtpBusy;

	// other page
	bool closeProgressDlgWhenCopyFinished;
	bool closeProgressDlgWhenMoveFinished;
	bool closeProgressDlgWhenDeleteFinished;

	bool closeProgressDlgWhenArchivFinished;
	bool closeProgressDlgWhenExtractFinished;

	bool closeProgressDlgWhenDwonloadFinished;
	bool closeProgressDlgWhenUploadFinished;

	bool alwaysOverwriteInCaseCpMvDwExt;
	bool alwaysOverwriteWhenArchiving;
	bool alwaysOverwriteWithNewArchive;
	bool alwaysAskBeforeRemoving;

	bool alwaysSavePermission;
	bool openDirAfterCreationIt;

};

#endif // _FILESYSTEMSETTINGS_H_
