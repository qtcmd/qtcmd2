/***************************************************************************
 *   Copyright (C) 2005 by Mariusz Borowski <b0mar@nes.pl>                 *
 *   Copyright (C) 2011 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDir>
#include <QLocale>

#include "fileinfo.h"


namespace Vfs {

/*
FileInfo::FileInfo(const QVector< QVariant > &data, FileInfo *parent)
{
	parentItem = parent;
	itemData = data;
}
*/
// According to the SI standard KB is 1000 bytes, KiB is 1024
// but on windows sizes are calulated by dividing by 1024 so we do what they do.
const qint64 _kb_ = 1024;
const qint64 _mb_ = 1024 * _kb_;
const qint64 _gb_ = 1024 * _mb_;
const qint64 _tb_ = 1024 * _gb_;

QString FileInfo::bytesRound( qint64 bytes, uint precision )
{
	if (bytes < 0)
		return "?";

	double dBytes = bytes;

	if (bytes >= _tb_) {
		dBytes /= _tb_;
		return QString("%1 TB").arg(dBytes, 0, 'f', precision);
	}
	if (bytes >= _gb_) {
		dBytes /= _gb_;
		return QString("%1 GB").arg(dBytes, 0, 'f', precision);
	}
	if (bytes >= _mb_) {
		dBytes /= _mb_;
		return QString("%1 MB").arg(dBytes, 0, 'f', precision);
	}
	if (bytes >= _kb_) {
		dBytes /= _kb_;
		return QString("%1 KB").arg(dBytes, 0, 'f', precision);
	}

	return QLocale().toString(bytes) + " " + QObject::tr("bytes");
}

bool FileInfo::setData( int column, const QVariant &value, bool ffrMode )
{
	if (ffrMode) { // FindFilesResultSubsystem
		if (column == 0)
			m_fileName = value.toString();
		else
		if (column == 1)
			m_size = value.toLongLong();
		else
		if (column == 2)
			m_filePath = value.toString();
		else
		if (column == 3)
			m_lastModified = value.toDateTime();
		else
		if (column == 4)
			m_perm = value.toString();
		else
		if (column == 5)
			m_owner = value.toString();
		else
		if (column == 6)
			m_group = value.toString();
		else
		if (column == 7) {
			m_mimeType  = value.toString();
			m_isDir     = (m_mimeType == "folder" || m_mimeType == "folder/locked");
			m_isSymlink = (m_mimeType == "folder/symlink" || m_mimeType == "file/symlink");
		}
		else
		if (column == 8) {
			m_symLinkTarget = value.toString();
		}
		else {
// 			qDebug() << "FileInfo::setData. Cannot update data for column:" << column << "(column not found)";
			return false;
		}
	}
	else {
		if (column == 0)
			m_fileName = value.toString();
		else
		if (column == 1)
			m_size = value.toLongLong();
		else
		if (column == 2)
			m_lastModified = value.toDateTime();
		else
		if (column == 3)
			m_perm = value.toString();
		else
		if (column == 4)
			m_owner = value.toString();
		else
		if (column == 5)
			m_group = value.toString();
		else
		if (column == 6) {
			m_mimeType  = value.toString();
			m_isDir     = (m_mimeType == "folder" || m_mimeType == "folder/locked" || m_mimeType == "folder/symlink");
			m_isSymlink = (m_mimeType == "folder/symlink" || m_mimeType == "file/symlink");
		}
		else
		if (column == 7) {
			m_symLinkTarget = value.toString();
		}
		else {
// 			qDebug() << "FileInfo::setData. Cannot update data for column:" << column << "(column not found)";
			return false;
		}
	}

	return true;
}

QString FileInfo::absoluteFileName() const
{
	QString absFilePath = m_filePath;
	if (absFilePath != QDir::rootPath() && ! absFilePath.endsWith('/') && ! absFilePath.isEmpty())
		absFilePath += QDir::separator();
	absFilePath += m_fileName;

	return absFilePath;
}


/*
bool FileInfo::removeChildren( int position, int count )
{
	if (position < 0 || position + count > childItems.size())
		return false;

	for (int row = 0; row < count; ++row)
		delete childItems.takeAt(position);

	return true;
}

bool FileInfo::insertColumns( int position, int columns )
{
	if (position < 0 || position > itemData.size())
		return false;

	itemData.clear();
	for (int column = 0; column < columns; ++column)
		itemData.insert(position, QVariant());

	foreach (FileInfo *child, childItems)
		child->insertColumns(position, columns);

	return true;
}

bool FileInfo::removeColumns( int position, int columns )
{
	if (position < 0 || position + columns > itemData.size())
		return false;

	for (int column = 0; column < columns; ++column)
		itemData.remove(position);

	foreach (FileInfo *child, childItems)
		child->removeColumns(position, columns);

	return true;
}
*/

} // namespace Vfs
