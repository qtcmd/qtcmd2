/***************************************************************************
*   Copyright (C) 2009 by Piotr Mierzwiński <piom@nes.pl>                 *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
*                                                                         *
***************************************************************************/
#ifndef ICONCACHE_H
#define ICONCACHE_H

#include <QMap>
#include <QIcon>
#include <QStringList>


/**	@author Piotr Mierzwiński <piom@nes.pl>
*/
/** Class can be used for holding set of icons.
*/
class IconCache {

public:
	~IconCache() {}

	static IconCache *instance();


	/** Adds icons search path.
	@param sPath absolute path.
	*/
	void addPath( const QString &sPath );

	/** Gets icons paths list.
	 * @return String list with paths
	*/
	QStringList pathes() const {
		return m_slPathList;
	}

	/** Adds all icons from paths list to the properly map.
	*/
	void updateCache();

	/** Clears paths list and icon map.
	*/
	void clear() {
		m_slPathList.clear();
		m_IconMap.clear();
	}

	/** Sets usage generic icons for audio, image, package, video file types.
	 * @param bUseGenericIcons if TRUE then use otherwise don't
	 */
	void setUsageGenericIcons( bool bUseGenericIcons ) {
		m_bUseGenericIcons = bUseGenericIcons;
	}
	/** Gets status of usage generic icons for audio, image, package, video file types.
	 * @return TRUE if are used otherwise are not used
	 */
	bool usageGenericIcons() const { return m_bUseGenericIcons; }

	/** Returns icon related with given icon name.
	 * @param sInIconName name of icon.
	 * @return QIcon object.
	 */
	QIcon icon( const QString &sInIconName );

private:
	IconCache();

	QStringList m_slPathList;

	QMap <QString, QIcon> m_IconMap;

	bool m_bUseGenericIcons;

};

#endif // ICONCACHE_H
