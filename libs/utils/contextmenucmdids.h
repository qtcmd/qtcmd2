#ifndef CONTEXTMENUCMDIDS_H
#define CONTEXTMENUCMDIDS_H

// -- menu for file's list flat/tree
#define CMD_NoOption             10
#define CMD_Open_MENU           100
#define CMD_Open                101
#define CMD_OpenInOppositeActiveTab 102
#define CMD_OpenInNewTab        103
#define CMD_OpenWithSep         104

#define CMD_OpenWith_MENU       105
#define CMD_OpenWithSelected    106

#define CMD_View                107
#define CMD_Edit                109

#define CMD_CreateNew_MENU      110
#define CMD_CreateNewTxtFile    111
#define CMD_CreateNewDirectory  112

#define CMD_Rename              121
#define CMD_Remove              122
#define CMD_CopyTo              123
#define CMD_Makelink            124
#define CMD_EditSymlink         125
#define CMD_MoveTo              126
#define CMD_ShowProperties      127
#define CMD_FilterWithExtension 128
#define CMD_RemoveFilter        129

#define CMD_Select_MENU         130
#define CMD_SelectAll           131
#define CMD_UnSelectAll         132
#define CMD_SelectWithExt       133
#define CMD_UnSelectWithExt     134
#define CMD_SelectTemplate      135
#define CMD_UnSelectTemplate    136

#define CMD_Archive_SEP         140
#define CMD_AddToArchive        141
#define CMD_MoveToArchive       142
#define CMD_ExtractHere         143 // archive
#define CMD_ExtractTo           144 // archive or selected items
#define CMD_CreateArchive       145
#define CMD_TestArchive         146

// -- menu for find file list
#define CMD_FindFile_GoToFile   201
#define CMD_FindFile_ViewFile   202
#define CMD_FindFile_EditFile   203
#define CMD_FindFile_DelFile    204

#define CMD_BookmarksMenuInit   300

#define CMD_DrivesMenuInit      500

#define CMD_OpenWithSelectedMinId  10000
#define MAX_OPENWITH_BY_MIME    5

#endif
