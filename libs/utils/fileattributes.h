/***************************************************************************
 *   Copyright (C) 2013 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _FILEATTRIBUTES_H
#define _FILEATTRIBUTES_H

#include <QStringList>
#include <QDateTime>

/**
	@author Piotr Mierzwiński
*/
class FileAttributes
{
public:
	enum ChangesFor { NONE=-1, FILES_AND_DIRS=0, FILES_ONLY, DIRS_ONLY };

	FileAttributes() {
		nPermissions = -1;
		sOwner = "";
		sGroup = "";
		dtLastAccessTime    = QDateTime();
		dtLastModifiedTime  = QDateTime();

		changePermFor       = FILES_AND_DIRS;
		changeOwnGrpFor     = FILES_AND_DIRS;
		changeAccModTimeFor = FILES_AND_DIRS;
		changeAttribNameFor = NONE;
		bRecursiveChanges   = false;
	}

	int        changeAttribNameFor;   // name change status, if value > -1 then one or more file has changed name
	ChangesFor changePermFor;   // -1 no change, 0 files and dirs, 1 files only, 2 dirs only
	ChangesFor changeOwnGrpFor; // -1 no change, 0 files and dirs, 1 files only, 2 dirs only
	ChangesFor changeAccModTimeFor; // -1 no change, 0 files and dirs, 1 files only, 2 dirs only
	QString    sChangesForMsg; // used by progress dialog

	QStringList slOriginalFilesName, slRenamedFiles;
	int         nPermissions;
	QString     sOwner, sGroup;
	QDateTime   dtLastAccessTime, dtLastModifiedTime;

	bool bRecursiveChanges;

};

#endif
