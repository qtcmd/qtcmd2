/***************************************************************************
 *   Copyright (C) 2009 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   The class is based on class Mime from "nao" project (Mateusz Dworak)  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef MIMEINFO_H
#define MIMEINFO_H

#include <QMap>
#include <QStringList>


namespace Vfs {

class MimeInfo
{
	public:
		         MimeInfo() : m_bEmpty(true), m_bSubMime(false) {}
		explicit MimeInfo( const QString &sName );
		        ~MimeInfo() {}

		QMap <QString, QStringList> getMimeMap();

		static MimeInfo CreateEmpty( QString sName );

		void    setIcon( const QString &sIcon ) {  m_sIconName = sIcon;  }
		QString getIcon() const                 {  return m_sIconName;   }

		void    setDefaultCommand( const QString &sCommand )  {  m_sDefaultCommand = sCommand;  }
		QString getDefaultCommand() const                    {  return m_sDefaultCommand;      }

		void    setEmpty( bool bEmpty )  {  m_bEmpty = bEmpty;  }
		bool    isEmpty() const          {  return m_bEmpty;    }

		void        setCommands( const QStringList &slCommands )  {  m_slCommands = slCommands;  }
		QStringList getCommands() const                           {  return m_slCommands;        }

		void    setColor( const QString &sColor ) {  m_sColor = sColor;  }
		QString getColor() const                  {  return m_sColor;    }

		bool    isSubMime() const  {  return m_bSubMime;  }

		QString getName() const  {  return m_sName;  }


	protected:
		bool    m_bEmpty;
		QString m_sName;

	private:
		bool        m_bSubMime;
		QString     m_sIconName;
		QString     m_sColor;
		QString     m_sDefaultCommand;
		QStringList m_slCommands;

};

} // namespace Vfs

#endif // MIMEINFO_H
