/***************************************************************************
 *   Copyright (C) 2004 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _SYSTEMINFOHELPER_H_
#define _SYSTEMINFOHELPER_H_

#include <sys/stat.h> // for constans: S_ISUID, S_ISGID, S_ISVTX

#include <QStringList>

class QString;

/**
 * @author Piotr Mierzwiński <piom@nes.pl>
 */

namespace Vfs {

	/** Function gets file system info for given directory.\n
	 * @param path - directory name,
	 * @param freeBytes - variable, for returning amount of free bytes
	 * @param allBytes - variable, for returning amount of all of bytes
	 * @return TRUE, if getting finished success, otherwise FALSE.
	 */
	bool         getStatFS( const QString &path, long long &freeBytes, long long &allBytes );

	/** Returns real user Id of the current process.
	 * @return user Id
	 */
	unsigned int getCurrentUserId();

	/** Returns user Id for given user name.
	 * @return user Id
	 */
	unsigned int getUserId( const QString &sUserName );

	/** Returns real user name for the current process.
	 * @return user name
	 */
	QString      getCurrentUserName();

	/** Returns user name for given user Id.
	 * @return user name
	 */
	QString      getUserName( unsigned int userId );

	/** Returns real group Id for the current process.
	 * @return group Id
	 */
	unsigned int getCurrentUserGroupId();

	/** Returns group Id for given group name.
	 * @return group Id
	 */
	unsigned int getGroupId( const QString &sGroupName );

	/** Returns group name for given user Id.
	 * @return group name
	 */
	unsigned int getCurrentGroupIdForUserId( unsigned int userId );

	/** Returns group name for currently logged user.
	 * @return group name
	 */
	QString      getCurrentGroupName();

	/** Return group name for given group Id.
	 * @return group name
	 */
	QString      getGroupName( unsigned int groupId );

	/** Returns all of operating system users.
	 * @return list of users
	 */
	QStringList  getAllUsers();

	/** Returns all of operating system group for given user Id.
	 * @return list of groups
	 */
	QStringList  getAllUserGroups( unsigned int userId );

	/** Returns all of operating system groups.
	 * @return list of groups
	 */
	QStringList  getAllGroups();


	// ----------- P E R M I S S I O N S -------------

	/** Translates given permission from string to integer,
	 * @param sPerm string describing permissions in format lrwxrwxrwx
	 * @return permission as integer
	 */
	qint16       permissionsFromString( const QString &sPerm );

	/** Translates given permission to string.
	 * @param nPerm permission as integer
	 * @param isDir if permission refers to directory
	 * @return string describing permissions in format lrwxrwxrwx
	 */
	QString      permissionsToString( qint16 nPerm, bool isDir );

	/** Returns permissions as string.
	 * @return permissions as string
	 */
	QString      permissionsAsString( const QString& sAbsFileName );

	/** Special file types:
	 *  @li SOCKET - socked,
	 *  @li BLKDEV - block devive,
	 *  @li CHRDEV - character device,
	 *  @li FIFO   - fifo.
	 */
	enum SpecialFileType { NONEsft=0, SOCKET=S_IFSOCK, BLKDEV=S_IFBLK, CHRDEV=S_IFCHR, FIFO=S_IFIFO };

	/** Sticky bit type: UID, GID, VTX.
	 */
	enum SpecialPerm     { NONEsp=0,  UID=S_ISUID, GID=S_ISGID, VTX=S_ISVTX };

	/** Returns permissions sum for given file.
	 * @param sFileName - file name.
	 * @return permissions sum.
	 * Beside standard permissions, checking are following attributes:
	 * S_ISUID, S_ISGID, S_ISVTX
	 */
	int          permissions( const QString &sFileName );

	/** Checks given file for given permissions bit.
	 * @param sFileName - file name,
	 * @param sp - special permissions bit related to: @em SpecialPerm
	 */
	bool         specialPerm( const QString &sFileName, SpecialPerm sp );

	/** Checks given file is special file (i.e.: char device).
	 * @param sFileName - file name,
	 * @param sft - type of special file, related to @em SpecialFileType
	 */
	bool         specialFile( const QString & sFileName, SpecialFileType sft );

	/** For given file the function perform 'stat' operation.
	 * @param sFullFileName - file name,
	 * @param nStatInfo - type bit to checking, available are defined by:
	 * 	@em SpecialFileType i @em SpecialPerm .
	 * @return TRUE if given file has set a bit, otherwise FALSE.
	 */
	bool         doStat( const QString &sFullFileName, int nStatInfo );

	/** Function looks for given application name in PATH environment variable.
	 * @param sName application name
	 * @return found path for given application or empty string if found nothing
	 */
	QString      findExecutable( const QString &sName );

} // namespace SystemInfo

#endif
