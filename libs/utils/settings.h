/***************************************************************************
 *   Copyright (C) 2016 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef SETTINGS_H
#define SETTINGS_H

#include <QSettings>
#include <QVariant>

class Settings : public QSettings
{
public:
	Settings();
	virtual ~Settings() {}

	QList<QKeySequence> keySequenceList( const QString &sKey );

	void setKeySequenceList( const QString &sKey, const QList<QKeySequence> &value );

	/** Update configuration if this is necessary (when new and original value are different).
	 * @param sKey key in configuration file
	 * @param newValue new value for given key
	 */
	void update( const QString &sKey, const QVariant &newValue );

	/** Gets scheme name (mostly related to colors) of FileListView.
	 * @param bAddPrefix insert prefix like "flvStyle_"" in returned name of scheme
	 * @param nID identifier of scheme, where -1 means current one
	 * @return scheme name for given ID, prefixed with "flvStyle_", but if ID is greater than available amount of schemas then empty string.
	 */
	QString flvSchemeName( bool bAddPrefix=true, int nID=-1 ) const;
};

#endif // SETTINGS_H
