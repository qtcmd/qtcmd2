/***************************************************************************
 *   Copyright (C) 2011 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef PLUGINDATA_H
#define PLUGINDATA_H

#include <QWidget>
#include <QToolBar>

//class QAction;
class QToolBar;

/**
 * @author Piotr Mierzwiński
 */
class PluginData
{
public:
	PluginData() {
		toolBar = NULL;
		own_icon = NULL;
		configPage = NULL;
		finding_files_fun = NULL;
		finding_inside_files_fun = NULL;
	}
	virtual ~PluginData() {}

	/// Available types: ALL, CLASS, NAME, VERSION, DESCRIPTION, MIME_TYPE, PROTOCOL, FILE_EXT
	enum DataType { ALL=255, CLASS=1, NAME=2, VERSION=4, DESCRIPTION=8, MIME_TYPE=16, PROTOCOL=32, FILE_EXT=64 };

	QString       name;
	QString       version;
	QString       description;
	QString       plugin_class;
	QString       mime_icon;
	QIcon        *own_icon;
	QString       mime_type;
	QString       protocol;
	QString       file_ext; // (might be separated by semicolon) usable only for archive subsystem and used in input archiving dialog to show archive type
	int          *finding_files_fun;
	int          *finding_inside_files_fun;
// 	QActionGroup *popup_actions;
	QToolBar     *toolBar;
	QWidget      *configPage;

};

#endif // PLUGINDATA_H


/* PluginInterface (v2):
 * (Inf) name       - string contains plugin own name, eg. "localsubsystem_plugin", "rarsubsystem", "picureviewer" etc.
 * (Inf) version    - string describing version of plugin (Information)
 * (Inf) description - string contains short description about plugin
 * (Inf) class      - string describing class that belong plugin, eg. for localsubsystem_plugin should be 'subsystem', for picureviewer should be 'view', etc.
 * (Inf) mime_icon  - string describing icon name according to mime_type, eg. package-x-generic for archives. May use in configuration to display icon or for other objectives.
 * (Inf) own_icon   - QIcon object. May use in configuration to display icon or for other objectives. Field mime_icon have higher priority.
 * (Op)  mime_type  - string contains mime info for supported file type, eg. for rar archives will be: application/x-rar; for viewer may be: image/jpeg or image/jpeg:image/gif when support for multiple formats
 * (Op)  protocol   - string contains protocol name, eg. file for local file system, ftp for ftp servers, smb form smb servers, etc.
 * (Op)  finding_files - pointer to function, that support finding files into subsystem
 * (Op)  finding_inside_files - bool value deliviering information about support findind inside files
 * (Op)  QActionGroup *popup_actions (pointer to object QActionGroup type) NOT USED
 * (Op)  QToolBar *toolBar (pointer to object QToolBar type)
 * (Op)  ConfigPage *configuration (pointer to structure that contains pointer to: icon, title and container (maybe QFrame with scroll support) with widgets)
 *
 * Inf - Information meaning
 * Op  - Operation meaning
 */

/* PluginInterface (v1):
 * name (string contains plugin name) (name: local_fs is reserved)
 * mime_type (mime type for file plugins, eg.archives)
 * version (string describing version of plugn)
 * type (constans: fileviewer/subfilesystem/filesystem/virtual)  (virtual is useful for "findresult" file list view)
 * mime_type:  (mime string, eg. application/x-compressed-tar, image/jpeg)
 * mime_icon_name (package-x-generic) (may use in configuration to display icon or for other objectives)
 * content_type (mime_string) (used by "subfilesystem")
 * fileext (string contains extention of file) (default empty)
 * use_ext_instead_mime (yes/no) (default no, when set to yes then do force use this plugin - option not recommend!)
 * fs_protocol_prefix (string describing used protocol in filesystem, eg. ftp://, smb:// )
 * bool finding_files() (false/true) (default false)
 * bool finding_inside_files() (false/true) (default false)
 * QActionGroup *popup_actions (pointer to object QActionGroup type) NOT USED
 * QActionGroup *toolbar_actions (pointer to object QActionGroup type)
 * ConfigPage *configuration (pointer to structure that contains pointer to: icon, title and container (maybe QFrame with scroll support) with widgets)
 *
 * id (plugin index number, useful when we have registrered few plugins for this same mime/content) (in configuration we can choose which plugin need to use)
 *
 * - run (in VFS)
 * QList <FileInfo *> findfiles(FileInfo *vfi, QString content) (null or pointer to function that make find files by file attribute, eg.name, access time)
 */
