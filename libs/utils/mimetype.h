/***************************************************************************
 *   Copyright (C) 2009 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef MIMETYPE_H
#define MIMETYPE_H

#include <QString>

#include "mimeinfo.h"


/** Base mime type class.
	The class purposed to getting mime type and icon name for given file.
*/
class MimeType
{
	public:
		MimeType();
		~MimeType() {}

		/** Function returns mime type for given file name.
		Supports local and remote (gues by name) file system.
		@param sFile file name with full path.
		@param bIsDir set mime for directory (defalt false)
		@param bIsSymLink set mime for symlink and dir. (defalt false)
		@param bCheckFileReadable check it file is readable (defalt false). Useful only for localfs.
		@param bFollowSymLink follow symlink (defalt false)
		@returns mime type.
		If mime type hasn't recognized then returns unknown mime type.
		*/
		static QString getFileMime( const QString & sFile, bool bIsDir=false, bool bIsSymLink=false, bool bCheckFileReadable=false, bool bFollowSymLink=false );

		/** Function returns mime icon name.
		Most useful for archives, shell script.
		@param sMime mime name
		@returns mime icon name
		*/
		static QString getMimeIcon( const QString & sMime, bool bFullName=false );

		/** Function returns sub-class for given mimetype.
		@param sMime mime name
		@return mimetype of sub-class.
		*/
		static QString getMimeSubClass( const QString & sMime );

		/** Static function that returns parent of the mime type for given MimeInfo object.
		@param mimeInfo MimeInfo object
		@returns parent of the mime type
		*/
		static QString getParentMime( Vfs::MimeInfo mimeInfo );

		/** Static function that returns parent of the mime type for given mime type.
		@param sMimeName mime type
		@returns parent of the mime type
		*/
		static QString getParentMime( const QString & sMimeName );

		/** Static function that returns child of the mime type for given MimeInfo object.
		 @param sMime MimeInfo object
		 @returns child of the mime type
		*/
		static QString getChildMime( const QString & sMime );

		/** Static function that determines mime from path and returns mime for it.
		 * @param sInPath input path to analize
		 * @param sNewHost new returned host from sPath
		 * @param sNewPath new returned path from sPath without host
		 * @return mime string
		 */
		static QString getMimeFromPath( const QString & sInPath, QString & sNewHost, QString & sNewPath );

		/** Static function that determines mime from path and returns mime for it.
		 * @param sInPath input path to analize
		 * @return mime string
		 */
		static QString getMimeFromPath( const QString & sInPath );

};

#endif // MIMETYPE_H
