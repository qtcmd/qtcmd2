/***************************************************************************
 *   Copyright (C) 2016 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDebug>
#include <QTextCodec>
#include <QKeySequence>

#include "settings.h"


Settings::Settings()
	: QSettings()
{
// 	qDebug() << "Settings::Settings.";
}


QList<QKeySequence> Settings::keySequenceList( const QString &sKey )
{
	QVariant val = value(sKey);
	if (val.isNull()) {
		return QList<QKeySequence>();
	} else {
		return QKeySequence::listFromString(val.toString());
	}
}


void Settings::update( const QString &sKey, const QVariant &newValue )
{
	if (value(sKey).isNull()) {
		setValue(sKey, newValue);
	} else {
		QVariant val = value(sKey);
		if (val != newValue)
			setValue(sKey, newValue);
	}
}


void Settings::setKeySequenceList( const QString &sKey, const QList<QKeySequence> &value )
{
	setValue(sKey, QKeySequence::listToString(value));
}


QString Settings::flvSchemeName( bool bAddPrefix, int nID ) const
{
	QString sSchemesList = value("filelistviewStyles/Schemas", QObject::tr("DefaultColors")).toString();
	QStringList slSchemesNameList = sSchemesList.split('|');
	int nCurrentId = (nID < 0) ? value("filelistviewStyles/SchemeCurrentId", 0).toInt() : nID;
	if (nCurrentId < 0)
		nCurrentId = 0;
	if (nCurrentId > slSchemesNameList.count()-1)
		return "";

	QString sScheme;
	if (bAddPrefix)
		sScheme = "flvStyle_";
	sScheme += slSchemesNameList.at(nCurrentId);

	return sScheme;
}

