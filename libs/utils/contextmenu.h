/***************************************************************************
 *   Copyright (C) 2012 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef CONTEXTMENU_H
#define CONTEXTMENU_H

#include <QMenu>
#include <QKeyEvent>

#include "keyshortcuts.h"
/**
 * @author Piotr Mierzwiński
 **/
/** @short Class handles context menu for files list panel.
*/
class ContextMenu : public QMenu
{
   Q_OBJECT
public:
	/** Menu type.
	 */
	enum ContextMenuType {
		NoMenuType = -1,
		FilesPanelMENU = 0, // TODO: rename to FileListViewMENU
		FindFileListViewMENU, // TODO: rename to FoundFileListViewMENU
		TextFileMENU, ImageFileMENU, VideoFileMENU, SoundFileMENU, BinaryFileMENU, // context menu for FileView window
		UserDefinedMENU
	};

	/** Constructor.
	 * @param pParent parent pointer
	 * @param pKS pointer to KeyShortcuts object
	 */
	ContextMenu( QWidget *pParent );

	/** Only initializes KeyShortcuts object.
	 * @param pKS pointer to KeyShortcuts object
	 */
	void    init( KeyShortcuts *pKS ) {
		m_pKS = pKS;
	}

	/** Cleans menu object and builds new menu (depends to eContextMenuType from constructor).
	 * @param eContextMenuType menu type, look at: @see ContextMenuType
	 */
	void     setContextMenuType( ContextMenu::ContextMenuType eContextMenuType );

	void     clearMenu();

	/** Shows context menu.
	 * @param globalPos global position of menu.
	 */
	void     showMenu( const QPoint &globalPos );

	/** Adds action to the menu.
	 * @param pAction action to adding
	 */
	void     addAction( QAction *pAction ) { QMenu::addAction(pAction); }

	/** Adds action to the menu.
	 * @param icon icon for action
	 * @param sTitle title of action
	 */
	QAction *addAction( QIcon &icon, const QString &sTitle ) { return QMenu::addAction(icon, sTitle); }

	/** Adds an action (and sets it parent) to the menu.
	 *  @param sTitle title of action
	 *  @param nActionId id of action
	 *  @param shortcut key shortcut
	 *  @param nParentMenuId id of parent menu for given action
	 *  @return pointer to new action
	 */
	QAction *addAction( const QString &sTitle, int nActionId, const QKeySequence &shortcut/*=QKeySequence()*/, int nParentMenuId=-1 );

	/** Inserts an action (and sets it parent) to the menu.
	 *  @param sTitle title of action
	 *  @param nActionIdBefore id of action before new action
	 *  @param nActionId id of new action
	 *  @param nParentMenuId id of parent menu for new action
	 *  @param nMaxActions max number of actions to inserting, -1 is unlimited
	 *  @return pointer to new action
	 */
	QAction *insertAction( int nActionIdBefore, const QString &sTitle, int nActionId, int nParentMenuId, int nMaxActions=-1 );

	/** Adds action menu (and sets it parent) to the menu.
	 *  @param sTitle title of action
	 *  @param nMenuId id of menu
	 *  @param nParentMenuId id of parent menu for given action
	 *  @return pointer to new menu
	 */
	QMenu   *addActionMenu( const QString &sTitle, int nMenuId, int nParentMenuId=-1 );

	/** Adds separator with given Id and in menu with given id
	 *  @param nActionId id of separator (useful during using insertAction)
	 *  @param nParentMenuId id of parent menu (useful during using insertAction)
	 */
	QAction *addSeparator( int nActionId=-1, int nParentMenuId=-1 );

	/** Enables or disables menu item or group of menu.
	 * @param nActionId id's menu item
	 * @param bEnable true made enables menu item or group of menu otherwise disables
	 */
	void     setEnableAction( int nActionId, bool bEnable=true );

	/** Shows or hides menu item or group of menu.
	 * @param nActionId id's menu item
	 * @param bVisible true made showing menu item or group of menu otherwise hide it
	 */
	void     setVisibleAction( int nActionId, bool bVisible=true );

	/** Returns status referring to visibility action with given Id
	 * @param nActionId id's menu item
	 * @return TRUE if action visible otherwise FALSE
	 */
	bool     isActionVisible( int nActionId );

	/** Returns next available action id for options in submenu "Open with"
	 * @return action id
	 */
	int      openWithNextActionId();

	/** Returns text for given action id.
	 * @param nActionId id for action
	 * @return text's string
	 */
	QString  actionText( int nActionId ) const;

	/** Removes action with given Id.
	 * @param nActionId Id to remove
	 * @return true if remove finished success otherwise false
	 */
	bool     removeAction( int nActionId );

	/** Updates "Open width" sub menu reffering to FilesPanelMENU
	 * with applications name, which are pointed by given mime.
	 * @param sMime mime pointing an applications list in configuragion file
	 */
	void     updateOpenWithSelect( const QString &sMime );

private:
	ContextMenuType m_eContextMenuType;
	bool m_bMoveFile;
	QMap <int, QAction *> m_mapIdToAction;
	QList<int> m_actionIdList;

	int      m_nOpenWithSelectedMinId;

	KeyShortcuts *m_pKS;

	void     initFilesPanelMenu();
	void     initFindFileListViewMenu();
	void     initUserMenu();

signals:
	void signalContextMenuAction( int nActionId );

};

#endif
