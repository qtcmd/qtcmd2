/***************************************************************************
 *   Copyright (C) 2003 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _FINDCRITERION_H_
#define _FINDCRITERION_H_

#include <QRegExp>
#include <QDateTime>

/**
	@author Piotr Mierzwiński
*/
class FindCriterion
{
public:
	enum CheckFileSize { NONE=-1, EQUAL, ATLEAST, MAXIMUM, GREAT_THAN, LESS_THAN };
	enum FilesType     { ALL=0, FILESandLINKS, DIRECTORIES, LINKS, FILES_ONLY };

	QString nameLst, location;
// 	bool caseSensitiveName;
	Qt::CaseSensitivity caseSensitivity;
	QRegExp::PatternSyntax patternSyntax;

	QString containText;
	bool caseSensitiveText, regExpText, includeBinFiles;

	bool checkTime;
	QDateTime firstTime, secondTime;

	CheckFileSize checkFileSize;
	qint64 fileSize;

	bool checkFileOwner;
	int ownerId;
	QString owner;
	bool checkFileGroup;
	int groupId;
	QString group;

	FilesType filesType;
	bool findRecursive, searchIntoSelected, stopIfXMatched;
	int stopAfterXMaches;

	bool isEmpty() const { return nameLst.isEmpty(); }

};

#endif
