/***************************************************************************
 *   Copyright (C) 2009 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   The class is based on class Mime from "nao" project (Mateusz Dworak)  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "mimeinfo.h"


namespace Vfs {

MimeInfo::MimeInfo( const QString &sName )
	: m_bEmpty(false), m_sName(sName)
{
	m_bSubMime = (sName.indexOf("/") != -1);
}


QMap <QString, QStringList> MimeInfo::getMimeMap()
{
	QMap <QString, QStringList> mimeMap;
	QStringList v1;

	v1 << m_sName;
	mimeMap["name"] = v1;

	v1[0] = m_sIconName;
	mimeMap["icon"] = v1;

	if (! m_sDefaultCommand.isEmpty())
	{
		v1[0] = m_sDefaultCommand;
		mimeMap["default"] = v1;
	}

	if (! m_sColor.isEmpty())
	{
		v1[0] = m_sColor;
		mimeMap["color"] = v1;
	}

	if (m_slCommands.count() > 0)
	{
		mimeMap["command"] = m_slCommands;
	}

	return mimeMap;
}

MimeInfo MimeInfo::CreateEmpty( QString sName )
{
	MimeInfo mimeInfo(sName);
	mimeInfo.setEmpty(true);
	return mimeInfo;
}


} // namespace Vfs
