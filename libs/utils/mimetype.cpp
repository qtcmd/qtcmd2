/***************************************************************************
 *   Copyright (C) 2009 by Piotr Mierzwiñski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDebug>
#include <QDir>
#include <QFileInfo>

#include "errno.h"

#include "xdgmime.h"
#include "mimetype.h"


MimeType::MimeType()
{
}

QString MimeType::getFileMime( const QString & sFile, bool bIsDir, bool bIsSymLink, bool bCheckFileReadable, bool bFollowSymLink )
{
	if (sFile.isEmpty()) {
		qDebug() << "MimeType::getFileMime. Empty file name";
		return "";
	}
	//qDebug() << "MimeType::getFileMime(). fileName:" << sFile;

	const char *pMime = NULL;
	bool bDevDir   = false;
	QString sMimeName = XDG_MIME_TYPE_UNKNOWN;  // application/octet-stream

	// TODO: need optimalization: calls for each file/directory during listnig directory
	// need to add properties: fileIsReadable to the FileInfo class (get info from FileSystem target class)
	// Maybe good idea is move it to the VfsModel.

	if (bIsDir) {
		if (bIsSymLink) {
			sMimeName = "folder/symlink";
			// need to check is readble
			// "folder/symlink/locked"
		}
		else {
			if (sFile.endsWith("/..") || sFile == "..")
				sMimeName = "folder/up";
			else
				sMimeName = "folder";
		}

		if (sFile.startsWith("/") && bCheckFileReadable) { // useful in local-fs
			QFileInfo file( sFile );
			if (! file.isReadable()) {
				sMimeName = "folder/locked";
			}
		}
		return sMimeName;
	}

	if (sFile != "/" && sFile.startsWith("/")) // // useful in local-fs
	{
		bool bReadable = true;
		QString sFileName = sFile;
		QFileInfo file( sFileName );
		if (! file.exists()) {
			if (bIsSymLink)
				sMimeName = "symlink/broken";
			else
				qDebug() << "MimeType::getFileMime. File doesn't exist!" << sFileName;
			return sMimeName;
		}
		if (bCheckFileReadable) {
			if (file.isSymLink()) {
				sMimeName = "symlink/broken";
			}
			/*if (! file.exists()) {
				qDebug() << "MimeType::getFileMime(). File doesn't exist!" << sFileName;
				return sMimeName;
			}
			else*/
			if (! file.isReadable()) {
				qDebug() << "MimeType::getFileMime. Cannot read file:" << sFileName << "Try gues mime by name.";
				pMime = xdg_mime_get_mime_type_from_file_name(QFile::encodeName(sFile)); // gues mime by file name
				bReadable = false;
			}
		}
		if (file.isDir()) {
			if (file.isSymLink())
				sMimeName = "folder/symlink";
			else
			if (! bReadable)
				sMimeName = "folder/locked";
			else
				sMimeName = "folder";
			return sMimeName;
		}
		if (bReadable) {
			//if (file.isSymLink()) {
			if (bIsSymLink) {
				if (bFollowSymLink) {
					sFileName = file.symLinkTarget();
				}
				else {
					sMimeName = "file/symlink";
					// check is symlink broken
					file.setFile(file.symLinkTarget());
					if (! file.exists())
						sMimeName = "symlink/broken";
					return sMimeName;
				}
			}
			if ((bDevDir=sFileName.startsWith("/dev"))) {
				struct stat statBuffer;
				if (::lstat(QFile::encodeName(sFileName), & statBuffer) != 0)  {
					qDebug() << "MimeType::getFileMime. Calls lstat error=" << errno << "file=" << sFileName;
					//return "";
				}
				else {
					if (S_ISBLK( statBuffer.st_mode ))
						sMimeName = "x-block-device";
					else
					if (S_ISCHR( statBuffer.st_mode ))
						sMimeName = "x-character-device";
					else
					if (S_ISFIFO( statBuffer.st_mode ))
						sMimeName = "x-fifo";
					else
					if (S_ISSOCK( statBuffer.st_mode ))
						sMimeName = "x-socket";
				}
			}
			else { // non devices directory
				pMime = xdg_mime_get_mime_type_for_file(QFile::encodeName(sFileName), NULL); // recognize mime by header of file
			}
		}
	} // local FS
	else { // remote FS
		if (bIsSymLink)
			sMimeName = "file/symlink";
		else
			pMime = xdg_mime_get_mime_type_from_file_name(QFile::encodeName(sFile)); // gues mime by file name
	}

	if (! bDevDir) {
		if (pMime != NULL)
			sMimeName = pMime;
		else {
			if (sMimeName.isEmpty())
				qDebug() << "MimeType::getFileMime. Function xdg_mime_get_mime_type returns NULL! File:" << sFile;
		}
	}

	return sMimeName;
}

QString MimeType::getMimeIcon( const QString & sMime, bool bFullName )
{
	if (sMime.isEmpty()) {
		qDebug() << "MimeType::getMimeIcon. Empty mime!";
		return "";
	}
	//qDebug() << "MimeType::getMimeIcon(). mime:" << sMime;

	QString sMimeIcon;
	if (! bFullName) {
		//const char *pIconGeneric = xdg_mime_get_generic_icon(sMime.toLatin1().data());
		const char *pIconName = xdg_mime_get_icon(sMime.toLatin1().data());
		if (pIconName != NULL)
			sMimeIcon = pIconName;

		if (sMimeIcon.isEmpty()) {
			//qDebug() << "sMimeIcon.isEmpty()";
			QString sParentMime = MimeType::getParentMime(sMime);
			if (sParentMime == "application" || sParentMime == "text")
				sMimeIcon = sMime.right(sMime.length()-sMime.lastIndexOf("/")-1); // get last word sep.by slash from /name/name
			else
				sMimeIcon = sParentMime;
		}
	}
	else {
		sMimeIcon = sMime;
		sMimeIcon.replace("/", "-");
	}

	return sMimeIcon;
}

QString MimeType::getMimeSubClass( const QString & sMime )
{
	QString sSubClass;

// int          xdg_mime_mime_type_subclass           (const char *mime_a, const char *mime_b);
// returns info about content of file.
// For example for application/x-cbr  in file: x-cbr.xml  there is info about what is the content, here will be: application/x-rar

	// Need to use:
	// char **      xdg_mime_list_mime_parents        (const char *mime);
	// above only returns the status, or mime_b there is sub-class mime_a

	return sSubClass;
}



QString MimeType::getParentMime( Vfs::MimeInfo mimeInfo )
{
	QString sMimeName = mimeInfo.getName();
	int slashPos = sMimeName.indexOf("/");
	if (slashPos != -1) {
		sMimeName = sMimeName.left(slashPos);
		return sMimeName;
	}

	return "";
}

QString MimeType::getParentMime( const QString & sMimeName )
{
	QString sName;
	int slashPos = sMimeName.indexOf("/");
	if (slashPos != -1) {
		sName = sMimeName.left(slashPos);
	}

	return sName;
}

QString MimeType::getChildMime( const QString & sMimeName )
{
	QString sName;
	int slashPos = sMimeName.lastIndexOf("/");
	if (slashPos != -1) {
		sName = sMimeName.right(sMimeName.length()-slashPos-1);
	}

	return sName;
}

QString MimeType::getMimeFromPath( const QString & sInPath, QString & sNewHost, QString & sNewPath )
{
	QString sMime, sSeparator = QDir::separator();
	QString sPath = sInPath;

	while (! sPath.isEmpty()) {
		if (sPath.endsWith(sSeparator))
			sPath.remove(sPath.length() - 1, 1);
		if (sPath.startsWith(QDir::rootPath())) // local subsystem
			sMime = MimeType::getFileMime(sPath, false, false, true);
		//qDebug() << "MimeType::getMimeFromPath. sMime:" << sMime;
		if (sMime != XDG_MIME_TYPE_UNKNOWN) { // application/octet-stream
			if (sMime != "folder" && sMime != "folder/symlink") { // not local subsystem
					sNewHost = sPath;
					sNewPath = sNewPath.remove(sNewHost);
					if (sNewPath.isEmpty())
						sNewPath = QDir::rootPath();
			}
			else {
				sNewHost = "";
				sNewPath = sInPath;
			}
			break;
		}
		sPath = sPath.left(sPath.lastIndexOf(sSeparator));
	}
	if (sPath.isEmpty())
		sMime = "folder"; // local subsystem
	//if (sMime.isEmpty())
	//	sMime = XDG_MIME_TYPE_UNKNOWN;

	return sMime;
}

QString MimeType::getMimeFromPath( const QString & sInPath )
{
    QString sNewHost, sNewPath;
    return MimeType::getMimeFromPath(sInPath, sNewHost, sNewPath);
}

