/***************************************************************************
*   Copyright (C) 2009 by Piotr Mierzwiński <piom@nes.pl>                 *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
*                                                                         *
 $Id: iconcache.cpp 673 2010-03-17 23:19:31Z piotr $
***************************************************************************/
#include <QFileInfo>
#include <QDebug> // for qDebug()
#include <QDir>

#include "iconcache.h"


IconCache::IconCache()
{
	qDebug() << "IconCache::IconCache. PRIVATE (no definition)";
    m_bUseGenericIcons = true;
}

IconCache *IconCache::instance()
{
	static IconCache instance;
	return &instance;
}

void IconCache::addPath( const QString &sPath )
{
	qDebug() << "IconCache::addPath.";
	if (sPath.isEmpty()) {
		qDebug() << "IconCache::addPath. Try adding empty path!";
		return;
	}
	m_slPathList << sPath;
	m_slPathList.removeDuplicates();
	qDebug() << "-- added path=" << m_slPathList[m_slPathList.count()-1];
}


void IconCache::updateCache()
{
	qDebug() << "IconCache::updateCache.";
	QDir dir;
	QFileInfoList lst;
	QStringList slFilters;
	//slFilters << "*.svg" << "*.svgz"; // << "*.png";
	QString sFileName, sBaseName;

	dir.setNameFilters(slFilters);
	dir.setFilter(QDir::Files | QDir::NoDot | QDir::NoDotDot);
	m_IconMap.clear();

	for ( int i=0; i<m_slPathList.count(); i++ )
	{
		slFilters.clear();
		if (m_slPathList[i].contains("scalable"))
			slFilters << "*.svg" << "*.svgz";
		else
			slFilters << "*.png";

		dir.setPath(m_slPathList[i]);
		lst = dir.entryInfoList();

		qDebug() << " -- get icons from dir:" << QDir::cleanPath(m_slPathList[i]);
		for (QFileInfoList::const_iterator it = lst.begin(); it != lst.end(); ++it) {
			sFileName = it->fileName();
			sBaseName = it->completeBaseName().replace(it->completeBaseName().indexOf('-'), 1, '/');
// 			if (sFileName.contains("edit-undo"))
// 				qDebug() << " -- add icon sBaseName:" << sBaseName << "sBaseName:" << sFileName;

			m_IconMap[sBaseName] = QIcon(m_slPathList[i]+"/"+sFileName);
		}
	}
}


QIcon IconCache::icon( const QString &sInIconName )
{
	if (sInIconName.isEmpty()) {
		qDebug() << "IconCache::icon. Empty icon name!";
		return QIcon();
	}
	if (m_IconMap.count() == 0) {
		qDebug() << "IconCache::icon. Icon cache is empty!";
		return QIcon();
	}

	QString sIconName = sInIconName;
	if (m_bUseGenericIcons) {
		if (sInIconName.startsWith("audio/"))
			sIconName = "audio/x-generic";
		else
		if (sInIconName.startsWith("image/"))
			sIconName = "image/x-generic";
		else
		if (sInIconName.startsWith("package/"))
			sIconName = "package/x-generic";
		else
		if (sInIconName.startsWith("video/"))
			sIconName = "video/x-generic";
	}
// 	if (sInIconName.contains("edit-undo"))
// 		qDebug() << "IconCache::icon. Try to get icon with name:" << sIconName << ".contains(sIconName):" << m_IconMap.contains(sIconName);

	return m_IconMap.contains(sIconName) ? m_IconMap[sIconName] : m_IconMap["unknown"];
}

