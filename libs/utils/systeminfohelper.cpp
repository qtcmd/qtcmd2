/***************************************************************************
 *   Copyright (C) 2004 by Piotr Mierzwiñski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <sys/types.h>
#include <inttypes.h>
#include <sys/vfs.h> // or <sys/statfs.h>
#include <unistd.h>
#include <errno.h>
#include <pwd.h>
#include <grp.h>

#include <QDir>
#include <QFile>
#include <QString>
//#include <QUrlInfo>
#include <qurlinfo.h>
#include <QFileInfo>
#include <QProcessEnvironment>

#include "systeminfohelper.h"


namespace Vfs {


bool getStatFS( const QString &path, long long &freeBytes, long long &allBytes )
{
	struct statfs fsd;

	if (::statfs(QFile::encodeName(path), &fsd) != 0)
		return false;

	#define PROPAGATE_ALL_ONES(x) ((x) == -1 ? (uintmax_t) -1 : (uintmax_t) (x))
	const long blockSize = fsd.f_bsize;
	const long long freeBytesForNonSuperuser = PROPAGATE_ALL_ONES((long)fsd.f_bavail) * blockSize;
	const long long freeBytesForSuperuser    = PROPAGATE_ALL_ONES((long)fsd.f_bfree)  * blockSize;

	freeBytes = (::getuid()) ? freeBytesForNonSuperuser : freeBytesForSuperuser;
	allBytes  = PROPAGATE_ALL_ONES((long)fsd.f_blocks) * blockSize;
/*
	uint mbtotal = allBytes/(1024*1024);
	uint mbfree  = freeBytes/(1024*1024);
	qDebug("SoLF::getStatFS, totalMB=%d, totalFreeMB=%d, totalBytes=%lld, freeBytes=%lld",
		mbtotal, mbfree, allBytes, freeBytes );
*/
	return true;
}


unsigned int getCurrentUserId()
{
    return ::getuid();
}

QString getCurrentUserName()
{
    struct passwd *pw = ::getpwuid(::getuid()); // TODO use thread safe function: getpwuid_r
    Q_CHECK_PTR(pw);
    return QFile::decodeName(pw->pw_name);
}

QString getUserName( unsigned int userId )
{
    struct passwd *pw = ::getpwuid(userId); // TODO use thread safe function: getpwuid_r
    Q_CHECK_PTR(pw);
    return QFile::decodeName(pw->pw_name);
}

unsigned int getCurrentUserGroupId()
{
    return ::getgid();
}

unsigned int getCurrentGroupIdForUserId( unsigned int userId )
{
	struct passwd *pw = ::getpwuid(userId); // TODO use thread safe function: getpwuid_r
	Q_CHECK_PTR(pw);
	return pw->pw_gid;
}

QString getCurrentGroupName()
{
	struct group *gr = ::getgrgid(::getgid()); // TODO use thread safe function: getgrgid_r
	Q_CHECK_PTR(gr);
	return QFile::decodeName(gr->gr_name);
}

QString getGroupName( unsigned int groupId )
{
	struct group *gr = ::getgrgid( groupId ); // TODO use thread safe function: getgrgid_r
	Q_CHECK_PTR(gr);
	return QFile::decodeName(gr->gr_name);
}

QStringList getAllUsers()
{
	QStringList list;
	::setpwent();
	struct passwd *pw = 0;
	while ((pw = ::getpwent()) != 0) { // TODO use thread safe function: getpwent_r
		if (list.count() > 0)
			if (pw->pw_uid == 0)  // prevent to duplicate list
				break;
		list << QFile::decodeName(pw->pw_name);
	}
	::endpwent();
	return list;
}

QStringList getAllUserGroups( unsigned int userId )
{
	QStringList list;
	struct passwd *pw = ::getpwuid(userId); // for user name TODO use thread safe function: getpwuid_r
	Q_CHECK_PTR(pw);
	::setgrent();
	struct group *gr = 0;
	while ((gr = ::getgrent()) != 0) { // TODO use thread safe function: getgrent_r
		while (*gr->gr_mem) {
			if (::strcmp(pw->pw_name, *gr->gr_mem) == 0)
				list << QFile::decodeName(gr->gr_name);
		}
	}
	::endgrent();
	return list;
}

QStringList getAllGroups()
{
	QStringList list;
	::setgrent();
	struct group *gr = 0;
	while ((gr = ::getgrent()) != 0) { // TODO use thread safe function: getgrent_r
		if (list.count() > 0)
			if (gr->gr_gid == 0) // prevent to duplicate list
				break;
		list << QFile::decodeName(gr->gr_name);
	}
	::endgrent();

	return list;
}

unsigned int getUserId ( const QString &sUserName )
{
	struct passwd *pw = getpwnam(QFile::encodeName(sUserName)); // TODO use thread safe function: getpwnam_r
	Q_CHECK_PTR(pw);
	return pw->pw_uid;
}

unsigned int getGroupId ( const QString &sGroupName )
{
	struct group *gr = getgrnam(QFile::encodeName(sGroupName)); // TODO use thread safe function: getgrnam_r
	Q_CHECK_PTR(gr);
	return gr->gr_gid;
}


qint16 permissionsFromString( const QString &sPerm )
{
	if  (sPerm.length() < 10)
		return 0;

	int nPerm = 0;

	if (sPerm[1] == 'r') nPerm += QUrlInfo::ReadOwner;
	if (sPerm[2] == 'w') nPerm += QUrlInfo::WriteOwner;
	if (sPerm[3] == 'x') nPerm += QUrlInfo::ExeOwner;
	else
	if (sPerm[3] == 'S') nPerm += S_ISUID;

	if (sPerm[4] == 'r') nPerm += QUrlInfo::ReadGroup;
	if (sPerm[5] == 'w') nPerm += QUrlInfo::WriteGroup;
	if (sPerm[6] == 'x') nPerm += QUrlInfo::ExeGroup;
	else
	if (sPerm[6] == 'S') nPerm += S_ISGID;

	if (sPerm[7] == 'r') nPerm += QUrlInfo::ReadOther;
	if (sPerm[8] == 'w') nPerm += QUrlInfo::WriteOther;
	if (sPerm[9] == 'x') nPerm += QUrlInfo::ExeOther;
	else
	if (sPerm[9] == 'T') nPerm += S_ISVTX;

	return nPerm;
}

QString permissionsToString( qint16 nPerm, bool isDir )
{
	//int nPerm = permissions();
	QString sPermissions = "----------";
	if (isDir)
		sPermissions[0] = 'd';

	int permTab[9] = {
		QUrlInfo::ReadOwner, QUrlInfo::WriteOwner, QUrlInfo::ExeOwner,
		QUrlInfo::ReadGroup, QUrlInfo::WriteGroup, QUrlInfo::ExeGroup,
		QUrlInfo::ReadOther, QUrlInfo::WriteOther, QUrlInfo::ExeOther
	};
	char permChrTab[3] = { 'r','w','x' };

	for (int i=0; i<9; i++) {
		if ( nPerm & permTab[i] )
			sPermissions[i+1] = permChrTab[i%3];
	}

	if ( (nPerm & S_ISUID) )  sPermissions[3] = (nPerm & QFile::ExeUser)  ? 'S' : 's';
	if ( (nPerm & S_ISGID) )  sPermissions[6] = (nPerm & QFile::ExeGroup) ? 'S' : 's';
	if ( (nPerm & S_ISVTX) )  sPermissions[9] = (nPerm & QFile::ExeOther) ? 'T' : 't';
	//qDebug("permissionsStr(): name= %s, perm= %d", name().toLatin1().data(), nPerm);

	return sPermissions;
}

QString permissionsAsString( const QString &sAbsFileName )
{
	struct stat64 st;
	char perm[] = "----------";

	if (0 == lstat64(QFile::encodeName(sAbsFileName), &st)) {
		if (S_ISLNK(st.st_mode))
			return "lrwxrwxrwx";
		switch (st.st_mode & S_IFMT) {
			case S_IFBLK:
				perm[0] = 'b';
				break;
			case S_IFCHR:
				perm[0] = 'c';
				break;
			case S_IFDIR:
				perm[0] = 'd';
				break;
			case S_IFIFO:
				perm[0] = 'p';
				break;
			case S_IFLNK:
				perm[0] = 'l';
				break;
			case S_IFSOCK:
				perm[0] = 's';
				break;
		}

		unsigned int flags[] = { S_IRUSR, S_IWUSR, S_IXUSR, S_IRGRP, S_IWGRP, S_IXGRP, S_IROTH, S_IWOTH, S_IXOTH };
		char sym[] = "rwx";
		for (int i = 0; i < 9; ++i) {
			if ((st.st_mode & flags[i]) == flags[i])
				perm[i+1] = sym[i%3];
		}
		if (st.st_mode & S_ISUID)
			perm[3] = 'S';
		if (st.st_mode & S_ISGID)
			perm[6] = 'S';
		if (st.st_mode & S_ISVTX)
			perm[9] = 'T';
	}

	return QString(perm);
}

int permissions( const QString &sFileName )
{
	if ( ! sFileName.startsWith('/') || sFileName.isEmpty() ) {
		qDebug("permissions. Illegal file system '%s' !\nAvailable only for local file system.", qPrintable(sFileName));
		return -1;
	}

	int nPerm = 0;
	QFileInfo fileInfo(sFileName);

	if ( fileInfo.isSymLink() )
		fileInfo.setFile(fileInfo.symLinkTarget());
	else
	if ( ! fileInfo.exists() )
		return -1;

	if (fileInfo.permission( QFile::ReadOwner  ))   nPerm += QUrlInfo::ReadOwner;
	if (fileInfo.permission( QFile::WriteOwner ))   nPerm += QUrlInfo::WriteOwner;
	if (fileInfo.permission( QFile::ExeOwner   ))   nPerm += QUrlInfo::ExeOwner;
	if (fileInfo.permission( QFile::ReadGroup  ))   nPerm += QUrlInfo::ReadGroup;
	if (fileInfo.permission( QFile::WriteGroup ))   nPerm += QUrlInfo::WriteGroup;
	if (fileInfo.permission( QFile::ExeGroup   ))   nPerm += QUrlInfo::ExeGroup;
	if (fileInfo.permission( QFile::ReadOther  ))   nPerm += QUrlInfo::ReadOther;
	if (fileInfo.permission( QFile::WriteOther ))   nPerm += QUrlInfo::WriteOther;
	if (fileInfo.permission( QFile::ExeOther   ))   nPerm += QUrlInfo::ExeOther;

	if (specialPerm(sFileName, Vfs::UID))   nPerm += S_ISUID;
	if (specialPerm(sFileName, Vfs::GID))   nPerm += S_ISGID;
	if (specialPerm(sFileName, Vfs::VTX))   nPerm += S_ISVTX;
	//qDebug("FileInfoExt::permission, sFileName= %s perm= %d", sFileName.toLatin1().data(), nPerm);

	return nPerm;
}

bool specialFile( const QString &sFileName, SpecialFileType sft )
{
	return doStat( sFileName, (int)sft );
}

bool specialPerm( const QString &sFileName, SpecialPerm sp )
{
	return doStat( sFileName, (int)sp );
}

bool doStat( const QString &sFileName, int nStatInfo )
{
	if (sFileName.at(0) != '/') {
		qDebug("doStat. Illegal file system of file '%s' !\nAvailable only for local FS.", qPrintable(sFileName) );
		return false;
	}

	QString fName = sFileName;

	if (nStatInfo == S_IFSOCK || nStatInfo == S_IFBLK || nStatInfo == S_IFCHR || nStatInfo == S_IFIFO) {
		QFileInfo file( fName );
		if ( file.isSymLink() )
			fName = file.symLinkTarget();
	}

	struct stat statBuffer;
	if (::lstat(QFile::encodeName(fName), & statBuffer) != 0)  {
		qDebug("FileInfoExt::doStat(). lstat error=%d, file=%s", errno, qPrintable(fName) );
		return false;
	}

	bool bPermissions = statBuffer.st_mode & nStatInfo;

	if ( nStatInfo == S_IFCHR )  bPermissions = S_ISCHR( statBuffer.st_mode );
	else
	if ( nStatInfo == S_IFBLK )  bPermissions = S_ISBLK( statBuffer.st_mode );
	else
	if ( nStatInfo == S_IFIFO )  bPermissions = S_ISFIFO( statBuffer.st_mode );
	else
	if ( nStatInfo == S_IFSOCK)  bPermissions = S_ISSOCK( statBuffer.st_mode );

	return bPermissions;
}

QString findExecutable( const QString &sName )
{
	QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
	Q_ASSERT(env.contains("PATH"));

	QStringList paths = env.value("PATH").split(":");
	foreach(const QString &p, paths) {
		QFileInfo fi(QDir(p), sName);
		if (fi.isExecutable())
			return fi.absoluteFilePath();
	}

	return QString();
}


} // namespace Vfs
