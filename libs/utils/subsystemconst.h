/***************************************************************************
 *   Copyright (C) 2015 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _SUBSYSTEMCONST_H
#define _SUBSYSTEMCONST_H

#include <QString>


namespace Vfs {

	enum SubsystemCommand {
		NO_COMMAND=0,
		PROG_EXISTS, CHK_VER,
		CONNECT, DISCONNECT, LOGIN_CMD,
		OPEN, CD, LIST, VIEW,
		COPY, MOVE, RENAME, REMOVE, SET_ATTRIBUTES_CMD, SET_PERM_CMD, SET_MDATE_CMD, SET_OWNER_CMD, SET_GROUP_CMD,
		CREATE_DIR, CREATE_FILE,
		CREATE_LINK, EDIT_LINK,
		WEIGH, WEIGH_ITEM, WEIGH_ARCH,
		FIND,
		COMPRESS, DECOMPRESS, ARCHIVE, EXTRACT, EXTRACT_AND_REMOVE, POST_EXTRACT, COPY_TO_ARCHIVE, MOVE_TO_ARCHIVE, MAKE_ARCHIVE, TEST_ARCHIVE,
		READ_FILE_TO_VIEW, OPEN_WITH_CMD, DECOMPRESS_TO_VIEW,
		WATCHER_INOTIFY,
		DOWNLOAD, UPLOAD
	};

	enum SubsystemError   {
		NO_ERROR=0,
		CANNOT_CONNECT, CANNOT_LOGIN,
		CANNOT_OPEN, CANNOT_READ, CANNOT_WRITE, READING_ERROR,
		CANNOT_CREATE_DIR, CANNOT_CREATE_FILE,
		CANNOT_CREATE_LINK, CANNOT_EDIT_LINK,
		CANNOT_RENAME, CANNOT_REMOVE, CANNOT_SET_PERM, CANNOT_SET_MA_TIME, CANNOT_SET_OWNER_GROUP, CANNOT_COPY_TO_THESAME,
		CANNOT_FOUND,
		CANNOT_EXECUTE,
		CANNOT_GET, CANNOT_PUT, CANNOT_STAT,
		CANNOT_COMPRESS, CANNOT_DECOMPRESS, CANNOT_ARCHIVE, CANNOT_MAKE_ARCHIVE, CANNOT_EXTRACT,
		CANNOT_DOWNLOAD, CANNOT_UPLOAD,
		CMD_NOT_FOUND,
		NOT_SUPPORTED,
		DOES_NOT_EXIST, ALREADY_EXISTS,
		CANCELED,
		SKIP_FILE,
		NO_FREE_SPACE,
		INTERNAL_ERROR,
		OTHER_ERROR
	};

	enum SubsystemStatus  {
		CONNECTING=0, CONNECTED, UNCONNECTED, LOGIN, LOGGED_IN,
		CHANGING_DIR, CHANGED_DIR,
		LISTING, LISTED, LISTED_COMPL, VIEW_COMPL,
		CLOSING, CLOSED,
		CREATING_DIR, CREATED_DIR, CREATING_FILE, CREATED_FILE,
		CREATING_LINK, CREATED_LINK, EDITING_LINK, EDITED_LINK,
		RENAMING, RENAMED, REMOVING, REMOVED, COPYING, COPIED, MOVING, MOVED, SETTING_ATTRIBUTES, SET_ATTRIBUTES, SET_PERM, SET_MDATE, SET_OWNER, SET_GROUP,
		WEIGHING, WEIGHED,
		READING_FILE, READ_FILE,
		FINDING, FOUND,
		WATCHER_REQ_ADD, WATCHER_REQ_DEL, WATCHER_REQ_REN, WATCHER_REQ_CHATR, WATCHER_REQ_LOCK,
		OPENING_WITH, OPEN_WITH,
		COMPRESSING, COMPRESSED, DECOMPRESSING, DECOMPRESSED,
		ARCHIVING, ARCHIVED,
		MAKING_ARCHIVE, MADE_ARCHIVE,
		EXTRACTING, EXTRACTED, EXTRACTED_TO_VIEW,
		DOWNLOADING, DOWNLOADED, DOWNLOADED_TO_VIEW,
		UPLOADING, UPLOADED, OPENED_FOR_WRITE,
		READY_TO_READ, NOT_READY_TO_READ, DECOMPRESSED_TO_VIEW,
		OPR_STOPPED, OPR_PAUSED,
		ERROR_OCCURED,
		UNKNOWN_STAT
	};

	/// Available values: NEW_TAB, NEW_ACTIVE_TAB, NEW_NOACTIVE_TAB, OPPOSITE_ACTIVE_TAB
	enum OpenInTab { NEW_TAB=0, NEW_ACTIVE_TAB, NEW_NOACTIVE_TAB, OPPOSITE_ACTIVE_TAB, CURRENT_ACTIVE_TAB };

	QString toString( SubsystemCommand subsysCmd );
	QString toString( SubsystemStatus subsysStat );
	QString toString( SubsystemError subsysError );
	QString toString( OpenInTab openInTab );

	/// Available values: DIR_WRITABLE_UNKNOWN, DIR_WRITABLE, DIR_NOT_WRITABLE
	enum WritableStatus  { DIR_WRITABLE_UNKNOWN=0, DIR_WRITABLE, DIR_NOT_WRITABLE };

	/// Available values: DIR_COUNTER, FILE_COUNTER, WEIGH_COUNTER
	enum ProgressCounter { DIR_COUNTER=0, FILE_COUNTER, WEIGH_COUNTER };

	/// Available values: COL_NAME, COL_SIZE, COL_TIME, COL_PERM, COL_OWNER, COL_GROUP, COL_MIME, COL_SYMLINKTARGET
	enum FileListColumns  { COL_NAME=0, COL_SIZE, COL_TIME, COL_PERM, COL_OWNER, COL_GROUP, COL_MIME, COL_SYMLINKTARGET };


	/// Available values: EXTRACT_ARCHIVE, EXTRACT_FILES
	enum WhatExtract  { EXTRACT_ARCHIVE=0, EXTRACT_FILES };

	/// Available values: ARCHIVE_FILES, MAKE_ARCHIVE
	//enum WhatArchive  { ARCHIVE_FILES=0, MAKE_ARCHIVE };

	/// Available values: EXTRACT_HERE, EXTRACT_TO
	enum WhereExtract { EXTRACT_HERE=0, EXTRACT_TO };

	/// Available values: ARCHIVE_HERE, ARCHIVE_TO
	enum WhereArchive { ARCHIVE_HERE=0, ARCHIVE_TO };

	/// Available values: FTP, SFTP, SMB, OVER_SELL
	enum RemoteConnection { FTP=0, SFTP, SMB, OVER_SELL };

} // namespace Vfs

#endif // _SUBSYSTEMCONST_H
