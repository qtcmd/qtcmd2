/***************************************************************************
 *   Copyright (C) 2015 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDir>

#include "subsystem.h"


namespace Vfs {

bool Subsystem::fileIsMatchingToSubsystemMime( const QString &sSubsystemMime, FileInfo *pFI )
{
	if (pFI == NULL)
		return false;

	bool bMatch = false;
	QStringList slMimeList = QStringList(sSubsystemMime);
	if (sSubsystemMime.contains(':')) // multi mime
		slMimeList = sSubsystemMime.split(':');

	foreach (QString sMime, slMimeList) {
		if (pFI->mimeInfo() == sMime) {
			bMatch = true;
			break;
		}
	}

	return bMatch;
}

QString Subsystem::absFileNameInLocalFS( FileInfo *pFileInfo, const QString &sTmpDir )
{
	QString sFileName;
	// WORKAROUND. FileInfo pointer comes from file list view, so should not be changed.
	// if file path comes from remote or archive subsystem then use WorkingDirectoryPath
	if (pFileInfo->filePath().startsWith(QDir::rootPath()))
		sFileName = pFileInfo->absoluteFileName();
	else {
		sFileName = sTmpDir;
		if (! sFileName.endsWith(QDir::separator()))
			sFileName += QDir::separator();
		sFileName += pFileInfo->fileName();
	}

	return sFileName;
}

} // namespace Vfs
