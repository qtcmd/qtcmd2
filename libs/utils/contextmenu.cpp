/***************************************************************************
 *   Copyright (C) 2001 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDebug>

#include "settings.h"
#include "contextmenu.h"
#include "contextmenucmdids.h"
#include "../widgets/keyshortcutsappids.h"


ContextMenu::ContextMenu( QWidget *pParent )
	: QMenu(pParent)
{
	m_bMoveFile = false; // TODO: set true if user holds Shift during right button is clicked
	m_nOpenWithSelectedMinId = CMD_OpenWithSelectedMinId;
	m_eContextMenuType = NoMenuType;
	m_pKS = nullptr;
}

void ContextMenu::clearMenu()
{
	clear();
	m_mapIdToAction.clear();
	m_actionIdList.clear();
}

// TODO Ugly solution with using setContextMenuType (including next two functions) in ContextMenu class. It should be moved to specified classes which use these menu
void ContextMenu::setContextMenuType( ContextMenu::ContextMenuType eContextMenuType )
{
// 	qDebug() << "ContextMenu::setContextMenuType. ContextMenuType:" << eContextMenuType;
	m_eContextMenuType = eContextMenuType;

	clearMenu();

	if (m_eContextMenuType == FilesPanelMENU)
		initFilesPanelMenu();
	else
	if (m_eContextMenuType == FindFileListViewMENU)
		initFindFileListViewMenu();
	else
	if (m_eContextMenuType == UserDefinedMENU)
		initUserMenu();
}

// TODO: Idea for making menu
// 	m_pShowHideMainMenuAct  = pViewMenu->addAction(m_pKS->keyDesc(ShowHideMenuBar_APP), this, &MainWindow::slotShowHideMenuBar, m_pKS->keyCode(ShowHideMenuBar_APP));
// In this moment IDs are handled in FileListView::slotContexMenuAction( int nActionId )
//
void ContextMenu::initFilesPanelMenu()
{
	addActionMenu(tr("Open &with"),         CMD_OpenWith_MENU);
	addAction(tr("&Select application"),   CMD_OpenWithSelected, m_pKS->keyCode(NoKeyCode_APP), CMD_OpenWith_MENU);
	addAction(tr("&Open"),                 CMD_Open,             m_pKS->keyCode(NoKeyCode_APP), CMD_OpenWith_MENU);
	addAction(tr("Open in panel &beside"), CMD_OpenInOppositeActiveTab, m_pKS->keyCode(NoKeyCode_APP), CMD_OpenWith_MENU); // there are two: OpenCurrentFileOrDirInLeftPanel_APP, OpenCurrentFileOrDirInRightPanel_APP
	addAction(tr("Open in new &tab"),      CMD_OpenInNewTab,     m_pKS->keyCode(CMD_OpenInNewTab), CMD_OpenWith_MENU);

	addAction(tr("&View"),                 CMD_View,             m_pKS->keyCode(ViewFile_APP));
	addAction(tr("E&dit"),                 CMD_Edit,             m_pKS->keyCode(EditFile_APP));

	addActionMenu(tr("Create &new"),        CMD_CreateNew_MENU);
	addAction(tr("&Text file"),            CMD_CreateNewTxtFile,   m_pKS->keyCode(MakeAnEmptyFile_APP), CMD_CreateNew_MENU);
	addAction(tr("&Directory"),            CMD_CreateNewDirectory, m_pKS->keyCode(MakeDirectory_APP), CMD_CreateNew_MENU);

	addAction(tr("&Rename"),               CMD_Rename,           m_pKS->keyCode(QuickRename_APP));
	addAction(tr("&Delete"),               CMD_Remove,           m_pKS->keyCode(DeleteFiles_APP));
	addAction(tr("&Copy to"),              CMD_CopyTo,           m_pKS->keyCode(CopyFiles_APP));
	addAction(tr("&Move to"),              CMD_MoveTo,           m_pKS->keyCode(MoveFiles_APP));

	addSeparator(CMD_Archive_SEP);
	addAction(tr("&Extract to"),           CMD_ExtractTo,        m_pKS->keyCode(NoKeyCode_APP));
	addAction(tr("Extract &here"),         CMD_ExtractHere,      m_pKS->keyCode(NoKeyCode_APP));
	addAction(tr("&Add to archive"),       CMD_AddToArchive,     m_pKS->keyCode(CreateNewArchive_APP));
	addAction(tr("&Move to archive"),      CMD_MoveToArchive,    m_pKS->keyCode(MoveFilesToArchive_APP));

	addSeparator();

	addActionMenu(tr("&Select/UnSelect"),   CMD_Select_MENU);
	addAction(tr("Select &All"),           CMD_SelectAll,              m_pKS->keyCode(SelectAllItems_APP), CMD_Select_MENU);
	addAction(tr("&UnSelect All"),         CMD_UnSelectAll,            m_pKS->keyCode(DeselectAllFiles_APP), CMD_Select_MENU);
	addAction(tr("Select with the same &ext"),    CMD_SelectWithExt,   m_pKS->keyCode(SelectFilesWithTheSameExtention_APP), CMD_Select_MENU);
	addAction(tr("UnSelect with &the same e&xt"), CMD_UnSelectWithExt, m_pKS->keyCode(DeselectFilesWithTheSameExtention_APP), CMD_Select_MENU);
	addAction(tr("Select using &template"),   CMD_SelectTemplate,      m_pKS->keyCode(SelectGroupOfFiles_APP), CMD_Select_MENU);
	addAction(tr("UnSelect using te&mplate"), CMD_UnSelectTemplate,    m_pKS->keyCode(DeselectGroupOfFiles_APP), CMD_Select_MENU);

	addAction(tr("&Properties"),           CMD_ShowProperties,   m_pKS->keyCode(ShowProperties_APP));

	addAction(tr("&Filter with extension"), CMD_FilterWithExtension,   m_pKS->keyCode(NoKeyCode_APP));
	addAction(tr("Remove &filter"),         CMD_RemoveFilter,          m_pKS->keyCode(NoKeyCode_APP));
}


void ContextMenu::updateOpenWithSelect( const QString &sMime )
{
// 	qDebug() << "ContextMenu::updateOpenWithSelect. Update sub menu \"Open with\" applications name associated with:" << sMime;
	// -- clean sub menu
	int id = CMD_OpenWithSelectedMinId;
	while (1) {
		if (! removeAction(id))
			break;
		id++;
	}
	// -- add new item(s)
	Settings settings;
	QString sAppsName = settings.value("appAssociationWithMime/"+sMime).toString();
	if (! sAppsName.isEmpty()) {
		int nAppId = CMD_OpenWithSelectedMinId + 1;
		QStringList slAppNames = sAppsName.split('|');
		addSeparator(CMD_OpenWithSelectedMinId, CMD_OpenWith_MENU);
		foreach (QString sAppName, slAppNames) {
			addAction(sAppName.right(sAppName.length()-sAppName.lastIndexOf('/')-1), nAppId, QKeySequence(), CMD_OpenWith_MENU);
			nAppId++;
		}
	}
}

void ContextMenu::initFindFileListViewMenu()
{
//	m_pKS->keyCode(CreateNewArchive_APP)
	addAction( tr("&Go to file") +"\t"+tr("Return/Enter"), CMD_FindFile_GoToFile, m_pKS->keyCode(CMD_FindFile_GoToFile));
	addAction( tr("&View file")  +"\t"+tr("F3"),           CMD_FindFile_ViewFile, m_pKS->keyCode(CMD_FindFile_ViewFile));
	addAction( tr("&Edit file")  +"\t"+tr("F4"),           CMD_FindFile_EditFile, m_pKS->keyCode(CMD_FindFile_EditFile));
	addAction( tr("&Delete file")+"\t"+tr("Delete/F8"),    CMD_FindFile_DelFile,  m_pKS->keyCode(CMD_FindFile_DelFile));
}

void ContextMenu::initUserMenu()
{
/* TODO
 * User menu will be prepared based on value of "CMD_xxxx", which one will be save in some table
 * Such menu configuration will be done using tree view. Will have abilities adding and removing any item(s) from menu.
 * How configuration will looks like
 * The view will be split on two parts. In one of them will be available the items and in second the tree showing how menu looks like
 * On top of view will be combo boxes contain following options: "FilesPanelMenu", "ViewPanelMenu"
 * And below you will able to find check box contain option: "Use standard menu", "Use your own menu" or "Use customized menu"
 */
}


void ContextMenu::showMenu( const QPoint &globalPos )
{
	m_bMoveFile = false;

	QAction *pAction = exec(globalPos);

	int nActionId = m_mapIdToAction.key(pAction);
//	qDebug() << "ContextMenu::showMenu. nActionId:" << nActionId;
	if (nActionId == 0)
		return;
// 	if (nActionId == CMD_OpenWithSelected) // commented due to function is already handled!
// 		qDebug() << "ContextMenu::showMenu. save OpenWith for mime"; // TODO save OpenWith for given mime.

	if (pAction == nullptr) // nActionId == 10000
		return;

	emit signalContextMenuAction(nActionId);
}


QAction *ContextMenu::addAction( const QString &sTitle, int nActionId, const QKeySequence &shortcut, int nParentMenuId )
{
	QAction *pAction = nullptr;

	QAction *pActionTst = m_mapIdToAction.value(nActionId);
	if (pActionTst == nullptr) {
		QAction *pActionMenu = m_mapIdToAction.value(nParentMenuId);
		if (pActionMenu != nullptr) {
			if (shortcut == QKeySequence())
				pAction = pActionMenu->menu()->QMenu::addAction(sTitle);
			else
				pAction = pActionMenu->menu()->QMenu::addAction(sTitle+"\t"+shortcut.toString());
		}
		else {
			if (shortcut == QKeySequence())
				pAction = QMenu::addAction(sTitle);
			else
				pAction = QMenu::addAction(sTitle+"\t"+shortcut.toString());
		}
		// receiver usually might be FileListView
		// member might be: &MainWindow::slotShowHideMenuBar
		//QAction *	addAction(const QString &text, const QObject *receiver, const char *member, const QKeySequence &shortcut = 0)
		m_mapIdToAction.insert(nActionId, pAction);
	}
	else
		qDebug() << "ContextMenu::addAction. WARNING. Given actionId:" << nActionId << "already exists!";

	return pAction;
}


QAction *ContextMenu::insertAction( int nActionIdBefore, const QString &sTitle, int nActionId, int nParentMenuId, int nMaxActions )
{
	QAction *pAction = nullptr;
	QAction *pActionExists = m_mapIdToAction.value(nActionId);

	if (pActionExists == nullptr) {
		QAction *pActionParent = m_mapIdToAction.value(nParentMenuId);
		if (pActionParent != nullptr) {
			QAction *pActionMenu;
			if (nMaxActions == -1) {
				pActionMenu = m_mapIdToAction.value(nActionIdBefore);
				pAction = new QAction(sTitle, pActionParent);
				pActionParent->menu()->QMenu::insertAction(pActionMenu, pAction);
				m_mapIdToAction.insert(nActionId, pAction);
			}
			else { // rotated options list in contextmenu
				bool bDuplicated = false;
				for (QMap<int, QAction *>::iterator it = m_mapIdToAction.begin(); it != m_mapIdToAction.end(); ++it) {
					if (it.value()->text() == sTitle) {
						bDuplicated = true;
						break;
					}
				}
				if (! bDuplicated) {
					pAction = new QAction(sTitle, pActionParent);
					m_actionIdList.insert(0, nActionId);
					m_mapIdToAction.insert(nActionId, pAction);
// 					qDebug() << "IA m_actionIdList:" << m_actionIdList;
					int nNewActionIdBefore = (m_actionIdList.size() > 1) ? m_actionIdList.at(1) : nActionIdBefore;
// 					qDebug() << "IA newActionIdBefore" << nNewActionIdBefore;
// 					qDebug() << "IA insert into menu, actionId" << nActionId;
					pActionMenu = m_mapIdToAction.value(nNewActionIdBefore);
					pActionParent->menu()->QMenu::insertAction(pActionMenu, pAction);
					if (m_actionIdList.size() == nMaxActions+1) {
						QAction *pRemoveAction = m_mapIdToAction.value(m_actionIdList.at(nMaxActions)); // m_actionIdList.size()-1
// 						qDebug() << "IA menu removeActionId:" << m_actionIdList.at(nMaxActions);
						pActionParent->menu()->QMenu::removeAction(pRemoveAction);
					}
					if (m_actionIdList.size() > nMaxActions) {
// 						qDebug() << "IA remove from m_actionMap and m_actionIdList actionId:" << m_actionIdList.at(nMaxActions);
						m_mapIdToAction.remove(m_actionIdList.at(nMaxActions));
						m_actionIdList.removeAt(nMaxActions);
					}
				}
			}
		}
		else
			qDebug() << "WARNING. ContextMenu::insertAction. Not found parentMenuId:" << nParentMenuId;
	}
	else
		qDebug() << "WARNING. ContextMenu::insertAction. Given actionId:" << nActionId << "already exists!";

	return pAction;
}


QMenu *ContextMenu::addActionMenu( const QString &sTitle, int nMenuId, int nParentMenuId )
{
	QMenu   *pNewMenu   = nullptr;
	QAction *pActionTst = m_mapIdToAction.value(nMenuId);

	if (pActionTst == nullptr) {
		QMenu   *pMenu = nullptr;
		QAction *pActionMenu = m_mapIdToAction.value(nParentMenuId);
		if (pActionMenu != nullptr)
			pMenu = pActionMenu->menu();
		pNewMenu = new QMenu(sTitle, pMenu);
		if (pActionMenu != nullptr) {
			m_mapIdToAction.insert(nMenuId, pActionMenu->menu()->addMenu(pNewMenu));
		}
		else {
			m_mapIdToAction.insert(nMenuId, addMenu(pNewMenu));
		}
	}
	else
		qDebug() << "WARNING. ContextMenu::addActionMenu. Given menuId:" << nMenuId << "already exists!";

	return pNewMenu;
}

QAction *ContextMenu::addSeparator( int nActionId, int nParentMenuId )
{
	QAction *pAction = nullptr;

	if (nParentMenuId == -1)
		pAction = QMenu::addSeparator();
	else {
		QAction *pActionMenu = m_mapIdToAction.value(nParentMenuId);
		if (pActionMenu != nullptr) {
			pAction = pActionMenu->menu()->QMenu::addSeparator();
		}
		else {
			pAction = QMenu::addSeparator();
			qDebug() << "WARNING. ContextMenu::addSeparator. Not found given parentMenuId:" << nParentMenuId;
		}
		m_mapIdToAction.insert(nActionId, pAction);
	}

	return pAction;
}


bool ContextMenu::removeAction( int nActionId )
{
	QAction *pAction = m_mapIdToAction[nActionId];
	if (pAction == nullptr)
		return false;

	m_mapIdToAction.remove(nActionId);
	delete pAction;
	return true;
}


void ContextMenu::setEnableAction( int nActionId, bool bEnable )
{
	QAction *pAction = m_mapIdToAction.value(nActionId);
	if (pAction != nullptr)
		pAction->setEnabled(bEnable);
}

void ContextMenu::setVisibleAction( int nActionId, bool bVisible )
{
	QAction *pAction = m_mapIdToAction.value(nActionId);
	if (pAction != nullptr)
		pAction->setVisible(bVisible);
}

bool ContextMenu::isActionVisible( int nActionId )
{
	bool bVisible = false;
	QAction *pAction = m_mapIdToAction.value(nActionId);
	if (pAction != nullptr)
		bVisible = pAction->isVisible();

	return bVisible;
}

int ContextMenu::openWithNextActionId()
{
	m_nOpenWithSelectedMinId++;

	return m_nOpenWithSelectedMinId;
}

QString ContextMenu::actionText( int nActionId ) const
{
	QAction *pAction = m_mapIdToAction.value(nActionId);
	if (pAction == nullptr) {
		qDebug() << "WARNING. ContextMenu::actionText. Not found given actionId:" << nActionId;
		return "";
	}
	return pAction->text();
}
