/***************************************************************************
 *   Copyright (C) 2004 by Piotr Mierzwiñski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along withthisprogram; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <QtWidgets>

// #include "settings.h"
#include "keyshortcuts.h"


KeyShortcuts::KeyShortcuts()
{
}

void KeyShortcuts::clear()
{
	qDebug() << "KeyShortcuts::clear.";
	for (KeyCodes::Iterator it = m_keyCodes.begin(); it != m_keyCodes.end(); ++it) {
		KeyCode *kc = *it;
		delete kc;
		*it = NULL;
	}
	m_keyCodes.clear();
}

void KeyShortcuts::updateEntryList( const QString &sConfigFileKey )
{
	if (m_keyCodes.empty())
		return;

	//FIXME - read entries from configuration file
	// --- try to read shortcuts from config file
	//Settings settings;

	//QString sNewKey, sDefaultKey;
	//KeyCodeList::iterator it;

	//for (it = m_KeyCodeList.begin(); it != m_KeyCodeList.end(); ++it) {
	//	sDefaultKey = QKeySequence( (*it)->defaultCode() ).toString();
	//	sNewKey     = ""; //settings->readEntry( sConfigFileKey+(*it)->description(), sDefaultKey );
	//	if (sNewKey.isEmpty())
	//		sNewKey = sDefaultKey;

	//	QKeySequence k = QKeySequence(sNewKey); // below need to check combination too
	//	if ( k == Qt::SHIFT || k == Qt::ALT || k == Qt::CTRL ) // occured a wrong value
	//		sNewKey = sDefaultKey;

	//	(*it)->code = QKeySequence(sNewKey);
	//}
	//delete settings;
}


KeyCode *KeyShortcuts::key( int nActionId ) const
{
// 	qDebug() << "KeyShortcuts::key. actionId:" << nActionId;
	int id = nActionId;
	if (id < ACTION_ID_OFFSET)
		id = ACTION_ID_OFFSET+nActionId;

	return m_keyCodes.value(id);
}

QString KeyShortcuts::keyDefaultStr( int nActionId ) const
{
	int id = nActionId;
	if (id < ACTION_ID_OFFSET)
		id = ACTION_ID_OFFSET+nActionId;

	return (m_keyCodes.value(id))->defaultCode().toString();
}

const QKeySequence & KeyShortcuts::keyCode( int nActionId ) const
{
	KeyCode *kc = key(nActionId);
	//Q_ASSERT(kc != nullptr);
	if (kc == nullptr)
		return m_KeySequence; //QMetaType::QKeySequence;

	return kc->code;
}

const QString KeyShortcuts::keyDesc( int nActionId ) const
{
	KeyCode *kc = key(nActionId);
	Q_ASSERT(kc != nullptr);
	return kc->description();
}

const QString KeyShortcuts::keyDesc( const QString &sShortcut )
{
	QString sDescr;
	for (KeyCodes::Iterator it = m_keyCodes.begin(); it != m_keyCodes.end(); ++it) {
		KeyCode *kc = *it;
		if (kc->code.toString() == sShortcut) {
			sDescr = kc->description();
			break;
		}
	}
	return sDescr;
}

QString KeyShortcuts::keyStr( int nActionId ) const
{
	int id = nActionId;
	if (id < ACTION_ID_OFFSET)
		id = ACTION_ID_OFFSET+nActionId;

	return keyCode(id).toString();
}

void KeyShortcuts::setKey( int nActionId, int nNewShortcut, int nDefaultShortcut, const QString &sDescription )
{
	if (m_keyCodes.contains(nActionId)) {
		KeyCode *kc = m_keyCodes[nActionId];
		kc->code = QKeySequence(nNewShortcut);
		kc->setDefaultCode(QKeySequence(nDefaultShortcut));
		if (! sDescription.isNull() ) {
			kc->setDescription(sDescription);
		}
// 		qDebug() << "KeyShortcuts::setKey. (set present) key:" << kc->code << "default:" << kc->defaultCode() << "desc:" << kc->description();
	} else {
// 		qDebug() << "KeyShortcuts::setKey. (insert new)";
		KeyCode *kc = new KeyCode(nActionId, sDescription, nDefaultShortcut, nNewShortcut);
// 		m_keyCodes[nActionId] = kc;
		m_keyCodes.insert(nActionId, kc);
	}
// 	qDebug() << "KeyShortcuts::setKey. actionId:" << nActionId << "shortcut:" << QKeySequence(nNewShortcut).toString() << "defaultShortcut:" <<  QKeySequence(nDefaultShortcut).toString() << "descr.:" << sDescription;
}

void KeyShortcuts::setKey( const int nActionId, const QString &sNewShortcut )
{
	int nNewActId = nActionId + ACTION_ID_OFFSET;
	if (m_keyCodes.contains(nNewActId)) {
// 		qDebug() << "KeyShortcuts::setKey. newKey:" << sNewShortcut << "for actionId:" << nActionId;
		KeyCode *kc = m_keyCodes[nNewActId];
		kc->code = QKeySequence(sNewShortcut);
	}
	else {
		qDebug() << "KeyShortcuts::setKey. WARNING. Cannot set key shortcut for given actionId:" << nNewActId << "(actionId not found)";
	}
}

QKeySequence KeyShortcuts::ke2ks( QKeyEvent *pKeyEvent )
{
	int key = pKeyEvent->key();
	QString sKey = pKeyEvent->text();

	bool bKeysException = (key == Qt::Key_Space  || key == Qt::Key_Backspace
						|| key == Qt::Key_Delete || key == Qt::Key_Enter
						|| key == Qt::Key_Return);

	QKeySequence ksk = (sKey.isEmpty() || bKeysException) ? QKeySequence(key) : QKeySequence(sKey);
	//int keToKs = Qt::UNICODE_ACCEL + key + ((state & 0x0FFF)*0x2000);
	int keToKs = Qt::UNICODE_ACCEL + key + pKeyEvent->modifiers();
	if (ksk == QKeySequence(key)) // QKeySequence returns no UNICODE_ACCEL type
		keToKs -= Qt::UNICODE_ACCEL;

	return QKeySequence(keToKs);
}

