/***************************************************************************
 *   Copyright (C) 2005 by Mariusz Borowski <b0mar@nes.pl>                 *
 *   Copyright (C) 2011 by Piotr Mierzwi�ski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
// #include <QDebug> // need to qDebug()
#include <QDir>

#include "uri.h"
#include "mimetype.h"


namespace Vfs {


URI::URI( const QString &sUri )
{
	// protocol://[[username[:password]]@host]/path/to/file[[#access_method:[#access_method:[...]]/path/to/file]
	// where protocol = file, ftp, smb, ...
	// examples: ftp://qtcmd@nes.pl:password@nes.pl  ftp://piotr@ds214plus/
	setUri(sUri);
}

void URI::setUri( const QString &sInUri )
{
	if (sInUri.isEmpty())
		return;
	// every URI needs to have separator or rootPath inside
	if (! sInUri.contains(QDir::separator()) && ! sInUri.contains(QDir::rootPath())) {
		m_sProtocol = "file"; // required for toString fun.to return empty string
		return;
	}

	// TODO: URI::URI. Make URI validation and use QValidator
//	int nDblSlashPos
//	if ((nDblSlashPos=sUri.indexOf("://")) != -1) { // dbg (in KDevelop4 4.7.0) is going crazy after 'Step over' on this line'
	int nDblSlashPos = sInUri.indexOf("://");
	if (nDblSlashPos != -1) {
		m_sProtocol = sInUri.left(sInUri.indexOf(':'));
		m_sMime = "remote/" + m_sProtocol;

		// --- parse input Uri
		QString sDir, sUser, sPassword;
		int nLastMonkeyPos, nPortPos;
		m_nPort = 21; // default value

		sDir = sInUri;
		sDir = sDir.remove(0, nDblSlashPos+3);

		nLastMonkeyPos = sDir.lastIndexOf('@');
		if (nLastMonkeyPos != -1) {
			sUser = sDir.left( nLastMonkeyPos ); // cut user name (and maybe password)
			int nColonPos  = sUser.indexOf(':');
			if ( nColonPos != -1 ) {
				m_sPassword = sUser.right( sUser.length() - nColonPos - 1 );
				m_sUser     = sUser.left( nColonPos );
			}
			else
				m_sUser = sUser;
			sDir = sDir.right( sDir.length() - nLastMonkeyPos - 1 ); // rest (without '@') save into sDir
		}
		m_sHost = sDir.left( sDir.indexOf('/') );

		nPortPos = sDir.lastIndexOf(':');
		if (nPortPos != -1) {
			m_nPort = QString(m_sHost.right(m_sHost.length() - nPortPos - 1)).toInt();
			m_sHost = m_sHost.left( nPortPos ); // rest (without 'nPort:') save into sHost
		}
		m_sPath = sDir.right( sDir.length()-sDir.indexOf('/') );
		if (sDir == m_sPath) // fix path if no slash at end of string sUri
			m_sPath = "/";

		m_sUser = m_sUser.trimmed();
		m_sHost = m_sHost.trimmed();
		// m_sPath is not trimmed because is white spaces are allowed in path

	}
	else { // others treat like path from local subsystem
		m_sProtocol = "file";
		m_sHost     = "";
		m_sPath     = sInUri;
		m_nPort     = 0;
		m_sPassword = "";
		m_sUser     = "";
		m_sMime     = "folder";

		if (sInUri == QDir::rootPath())
			return;

		// -- determine host (for archive) and mime (mainly for files)
		QString sUri = sInUri;
		QString separator = QDir::separator();

		if (QFileInfo(sUri).exists()) {
			if (! sUri.endsWith(separator))
				m_sMime = MimeType::getFileMime(sUri, false, false, true);
			if (m_sMime == "folder") {
				if (! m_sPath.endsWith(separator) && m_sPath != QDir::rootPath())
					m_sPath += separator;
			}
			return;
		}
		// file or directory doesn't exist. Check if this is the inside of archive
		while (! QFileInfo(sUri).exists()) { // find the host (here it's archive)
			if (sUri.count(separator) == 1) {
				m_sMime = "";
				m_sPath = "";
				return;
			}
			sUri = sUri.left(sUri.lastIndexOf(separator));
		}
		m_sMime = MimeType::getFileMime(sUri, false, false, true);
		if (sUri != QDir::rootPath()) {
			if (! m_sMime.startsWith("folder") && ! m_sMime.startsWith("symlink") && ! m_sMime.startsWith("x-")) {
				// looks like the inside of archive
				m_sHost = sUri;
				m_sMime = "folder";
				m_sPath = m_sPath.remove(m_sHost);
				if (! m_sPath.endsWith(separator) && m_sPath != QDir::rootPath())
					m_sPath += separator;
			}
			else { // invalid file/directory in local subsystem
				m_sMime = "";
				m_sPath = "";
			}
		}
	}
// 	qDebug() << "URI::URI. sUri:" << sUri << " mime:" << mime() << "protocol:" << protocol() << "host:" << host() << "user:" << user();
}

void URI::reset()
{
	m_sProtocol = "";
	m_sHost     = "";
	m_sPath     = "";
	m_sUser     = "";
	m_sPassword = "";
	m_sMime     = "";
	m_nPort = 0;
}

QString URI::toString() const
{
	// constant format: "protocol://"+sUser+":"+sPass+"@"+sHost+":"+nPort+sDir;
	QString sConnStr = protocolWithSuffix();
	if (m_sProtocol != "file" && ! m_sProtocol.isEmpty()) {
		if (! m_sUser.isEmpty())
			sConnStr += m_sUser;
		if (! m_sPassword.isEmpty()) {
			if (! m_sUser.isEmpty())
				sConnStr += ":" + m_sPassword + "@";
		}
		else
		if (! m_sUser.isEmpty())
			sConnStr += "@";

		if (! m_sHost.isEmpty())
			sConnStr += m_sHost;
		if (m_nPort > 0)
			sConnStr += QString(":%1").arg(m_nPort);

		if (m_sPath.isEmpty())
			sConnStr += "/";
		else
			sConnStr += m_sPath;
		if (m_sHost.isEmpty())
			sConnStr = protocolWithSuffix();
	}
	else
		sConnStr += m_sHost + m_sPath;

	return sConnStr;
}


} // namespace Vfs
