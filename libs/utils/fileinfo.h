/***************************************************************************
 *   Copyright (C) 2005 by Mariusz Borowski <b0mar@nes.pl>                 *
 *   Copyright (C) 2009 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _FILEINFO_H
#define _FILEINFO_H

#include <QMap>
#include <QString>
#include <QDateTime>

namespace Vfs {
/**
	@author Piotr Mierzwiński <piom@nes.pl>
 */
class FileInfo
{
public:
	FileInfo() {}
// 	FileInfo(const QVector< QVariant >& data, Vfs::FileInfo *parent = 0);
	FileInfo( const FileInfo *fi )
		: m_fileName(fi->m_fileName),
		  m_filePath(fi->m_filePath),
		  m_size(fi->m_size),
		  m_isDir(fi->m_isDir),
		  m_isSymlink(fi->m_isSymlink),
		  m_lastModified(fi->m_lastModified),
		  m_perm(fi->m_perm),
		  m_owner(fi->m_owner),
		  m_group(fi->m_group),
		  m_mimeType(fi->m_mimeType),
		  m_symLinkTarget(fi->m_symLinkTarget)
	{
		m_dirLabel = "<DIR>";
	}
	FileInfo(const QString   &fileName,
			 const QString   &filePath,
				   qint64     size,
				   bool       isDir,
				   bool       isSymLink,
			 const QDateTime &lastModified,
			 const QString   &perm,
			 const QString   &owner,
			 const QString   &group,
			 const QString   &mimeType,
			 const QString   &symLinkTarget=""
			)
		: m_fileName(fileName),
		  m_filePath(filePath),
		  m_size(size),
		  m_isDir(isDir),
		  m_isSymlink(isSymLink),
		  m_lastModified(lastModified),
		  m_perm(perm),
		  m_owner(owner),
		  m_group(group),
		  m_mimeType(mimeType),
		  m_symLinkTarget(symLinkTarget),
		  m_location(filePath)
	{
		m_dirLabel = "<DIR>";
// 		QStringList lst = fileName.split(".");
// 		m_fileNameWithoutExt = lst.at(0);
// 		if (lst.size() > 1)
// 			m_fileExtension = lst.at(lst.size()-1); // old: lst.at(1);
	}

	virtual ~FileInfo() {}

			QString   fileName()           const { return m_fileName;           }
// 			QString   fileNameWithoutExt() const { return m_fileNameWithoutExt; }
// 			QString   fileExtension()      const { return m_fileExtension;      }
			QString   filePath()           const { return m_filePath;           }
			QString   absoluteFileName()   const;
			QString   mimeInfo()           const { return m_mimeType;           }
			qint64    size()               const { return m_size;               }
			bool      isDir()              const { return m_isDir;              }
			bool      isSymLink()          const { return m_isSymlink;          }
			QString   symLinkTarget()      const { return m_symLinkTarget;      }
			QDateTime lastModified()       const { return m_lastModified;       }
			QString   permissions()        const { return m_perm;               }
			QString   owner()              const { return m_owner;              }
			QString   group()              const { return m_group;              }
			bool      isHidden()           const { return m_fileName.startsWith(QChar('.')) && (m_fileName != ".."); }
			QString   dirLabel()           const { return m_dirLabel;           }
			QString   location()           const { return m_location;           } // used in copying from FindFileResult panel, to be able to find properly item for unmarking

	static  QString   bytesRound( qint64 bytes, uint precision=2 );
			bool      operator<( const FileInfo &fileInfo ) const { return (QString(m_fileName).compare(fileInfo.fileName()) < 0); }

			bool      setData( int column, const QVariant &value, bool ffrMode=false );
			void      setDirLabel( const QString &dirStr="<DIR>" ) { m_dirLabel = dirStr; }
			void      setLocation( const QString &location ) { m_location = location; }

// 			void      setSize( qint64 size ) { m_size = size; }
// 			void      setFilePath( const QString &filePath ) { m_filePath = filePath; } // too risky
// 			bool      removeChildren( int position, int count );
// 			bool      insertColumns( int position, int columns );
// 			bool      removeColumns( int position, int columns );

private:
	QString   m_fileName;
// 	QString   m_fileNameWithoutExt;
// 	QString   m_fileExtension;
	QString   m_filePath;
	qint64    m_size;
	bool      m_isDir;
	bool      m_isSymlink;
	QDateTime m_lastModified;
	QString   m_perm;
	QString   m_owner;
	QString   m_group;
	QString   m_mimeType;
	QString   m_symLinkTarget;
	QString   m_dirLabel;
	QString   m_location; // used with copying from FindFileResult panel, to be able to find properly item for unmarking
// 	FileInfo          *parentItem;
// 	QVector<QVariant>  itemData;
// 	QList<FileInfo*>   childItems;

};

typedef QList <FileInfo *> FileInfoList;    // items list on view or processed (in Subsystem's operation) items list
typedef QMultiMap  <FileInfo *, int> FileInfoToRowMap;    // selected items map

} // namespace Vfs

#endif
