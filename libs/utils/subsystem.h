/***************************************************************************
 *   Copyright (C) 2005 by Mariusz Borowski <b0mar@nes.pl>                 *
 *   Copyright (C) 2009 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _SUBSYSTEM_H
#define _SUBSYSTEM_H

#include <QFile>
#include <QVector>
#include <QDateTime>
#include <QStringList>
//#include <QUrlInfo>
#include <qurlinfo.h>

#include "uri.h"
#include "fileinfo.h"
#include "mimeinfo.h"
#include "contextmenu.h"
#include "findcriterion.h"
#include "fileattributes.h"
#include "subsystemconst.h" // enums using in Subsystem
#include "plugindata.h"


namespace Vfs {

/** Interface for virtual file system
 */
class Subsystem : public QObject
{
	Q_OBJECT
public:
	Subsystem() {
		m_bConfirmExtractingForOpenWith = true; // TODO: move to SubsystemArchive class
		m_bConfirmExtractingForView = true;
	}
	virtual ~Subsystem() {}

	bool m_bConfirmExtractingForOpenWith, m_bConfirmExtractingForView; // TODO: move to SubsystemArchive class

	virtual SubsystemError   error() const = 0;
	virtual SubsystemStatus  status() const = 0;
	virtual SubsystemCommand currentCommand() const = 0;

	virtual WritableStatus   dirWritableStatus() { return DIR_WRITABLE_UNKNOWN; }


	virtual void          init( QWidget *pParent, bool bInvokeDetection=false ) = 0;

	virtual FileInfo     *root( bool bEmptyName=false ) = 0;
	virtual FileInfoList  listDirectory() const = 0;
	virtual void          getListBuffer( QString &sListBuffer ) {}

	virtual bool          canUpThanRootDir() const = 0;
	virtual bool          canChangeDirTo( const QString &sAbsPath ) = 0;
	virtual bool          canOpenFileForReading( FileInfo *pFileInfo ) { return false; } // = 0; TODO would be nice if canOpenFileForReading would be implemented in all subsys.plugins

	virtual void          open( const URI &uri, const QString &sRequestingPanel, bool bCompleter=false ) = 0;
	virtual void          close( bool bForce=false ) = 0;
	virtual void          openWith( FileInfoToRowMap *pSelectionMap, const URI &uri, const QString &sRequestingPanel ) {}
	virtual void          readFileToViewer( const QString &sHost, FileInfo *pFileInfo, QByteArray &baBuffer, int & nReadBlockSize, int nFileLoadMethod, const QString &sRequestingPanel ) {}

	virtual void          makeDir( const QString &sParentDirPath, const QString &sInNewDirsList, const QString &sRequestingPanel ) = 0;
	virtual void          makeFile( const QString &sParentDirPath, const QString &sInFileNameList, const QString &sRequestingPanel ) = 0;
	virtual void          makeLink( FileInfoToRowMap *pSelectionMap, const QString &sTargetLinkName, bool bHardLink, const QString &sRequestingPanel ) {}
	virtual void          editSymLink( FileInfo *pFileInfo, const QString &sSymLinkNewTarget, const QString &sRequestingPanel ) {}
	virtual void          rename( FileInfo *fileInfoOldName, const QString &sNewName, const QString &sRequestingPanel ) = 0;
	virtual void          remove( FileInfoToRowMap *pSelectionMap, const URI &uri, bool bWeighBefore, const QString &sRequestingPanel, bool bForceRemoveAll=false ) = 0;
	virtual void          setAttributes( FileInfoToRowMap *pSelectionMap, const URI &uri, FileAttributes *pChangedAttrib, bool bWeighBefore, const QString &sRequestingPanel ) {}
	virtual void          searchFiles( FileInfoToRowMap *pSelectionMap, const URI &uri, const FindCriterion &fcFindCriterion, const QString &sRequestingPanel ) {}
	virtual QString       searchingResult() { return ""; }
	virtual void          copyFiles( FileInfoToRowMap *pSelectionMap, const QString &sTargetPath, bool bMove, bool bWeighBefore, const QString &sRequestingPanel ) {}
	virtual void          extractTo( WhatExtract whatExtract, bool bMove, FileInfoToRowMap *pSelectionMap, const QString &sTargetPath, const QString &sRequestingPanel )  {}

	virtual void          weigh( FileInfoToRowMap *pSelectionMap, const URI &uri, const QString &sRequestingPanel, bool bWeighingAsSubCmd=true, bool bResetCounters=true ) {};
	virtual void          weighItem( FileInfo *fileInfo, const URI &uri, const QString &sRequestingPanel, bool bClearCounters=true ) = 0;
	virtual void          getWeighingResult( qint64 &weight, qint64 &realWeight, uint &files, uint &dirs ) = 0;

	virtual void          setBreakOperation() {}
	virtual void          setPauseOperation( bool bPause ) {}

	/** Returns current URI into subsystem.
	 * @return URI
	 */
	virtual URI           currentURI() const = 0;

	virtual FileInfo     *fileInfo( const QString &sAbsFileName ) = 0;
	virtual FileInfo     *processedFileInfo() const { return NULL; }

	virtual SubsystemError isExecutableFile( const QString &sAbsFileName ) const { return CANNOT_EXECUTE; }
	virtual void          executeApplication( const QString &sFullFileName ) {};

	virtual PluginData   *pluginData() = 0;

	virtual FileInfoList *processedFiles() { return 0; }
	virtual FileInfoList *postMovedFiles() { return 0; }
	virtual QString       processedFileName() const { return ""; }

	virtual bool          operationFinished() const { return true; } // usable during checking recurency operations: weigh, remove, copy, move, setAttributes

	virtual void          updateContextMenu( ContextMenu *pContextMenu, KeyShortcuts *pKS ) = 0;

	virtual bool          theSameDirInBothPanels() const { return false; }
	virtual QStringList & listOfItemsToModify() { return QStringList() << ""; }

	virtual void          setWorkingDirectoryPath( const QString &sDirectoryPath ) = 0;

	virtual bool          handledOperation( SubsystemCommand sc ) { return false; }


	static bool    fileIsMatchingToSubsystemMime( const QString &sSubsystemMime, FileInfo *pFI ); // TODO: move to SubsystemArchive class
	static QString absFileNameInLocalFS( FileInfo *pFileInfo, const QString &sTmpDir ); // TODO: move to SubsystemArchive class

signals:
	void signalShowFileOverwriteDlg(const FileInfo &, FileInfo &, int &);
	void singalShowConnectionDlg( URI &uri, const QString &sProtocol, const QString &sNoPassUser, bool &bRetStat );
	void signalStatusChanged( Vfs::SubsystemStatus status, const QString &sRequestingPanel );

	void signalNameOfProcessedFile( const QString &srcFileName, const QString &trgFileName=QString() );

	/** Inits files and directries number in ProgressDialog and sets total value for files or directries.
	 * @param nValue number of files or directries
	 * @param counter type of counter
	 * eration (available: DIR_COUNTING, FILE_COUNTING, WEIGH_COUNTING).
	 * @param bSetTotalValue true made setting total number of files or directries (depends to fileCounting parameter)
	 */
	void signalCounterProgress( long long nValue, Vfs::ProgressCounter counter, bool bSetTotalValue=false );

	void signalTotalProgress( int nDoneInPercents, long long nTotalWeight );
	void signalDataTransferProgress( long long nBytesDone, long long nBytesTotal, uint nBytesTransfered, bool bSetTotalBytes );
	void signalTimerStartStop( bool bStart );
	void signalOperationFinished();
	void signalGetWeighingResult( qint64 &totalWeight, uint &totalItems );

	void signalShowConnectionDlg( URI &uri, const QString &sProtocol, const QString &sNoPassUser, bool &bRetStat );

};


/** Container class to holding command with arguments
 */
class Operation
{
public:
	Operation() {}
	Operation( Vfs::SubsystemCommand inCommand, FileInfo *inFileInfo, const QString &inTargetPath, const QString &inSubsystemManagerName ) : // localsubsystem command
		command(inCommand),
		subsysMngrName(inSubsystemManagerName),
		targetPath(inTargetPath),
		fileInfo(inFileInfo),
		selectionMap(NULL),
		booleanArg(false)
	{}
	Operation( Vfs::SubsystemCommand inCommand, const QString &inShellCmd, const QString &inSubsystemManagerName ) : // shell command, eg. listing archive
		command(inCommand),
		subsysMngrName(inSubsystemManagerName),
		fileInfo(NULL),
		selectionMap(NULL),
		booleanArg(false),
		shellCmd(inShellCmd)
	{}
	Operation( Vfs::SubsystemCommand inCommand, const URI &inUri, const QString &inSubsystemManagerName ) : // open
		command(inCommand),
		subsysMngrName(inSubsystemManagerName),
		fileInfo(NULL),
		selectionMap(NULL),
		uri(inUri),
		booleanArg(false)
	{}
	Operation( Vfs::SubsystemCommand inCommand, FileInfo *inFileInfo, const URI &inUri, const QString &inSubsystemManagerName, bool inClearCounters=false ) : // weighItem
		command(inCommand),
		subsysMngrName(inSubsystemManagerName),
		fileInfo(inFileInfo),
		selectionMap(NULL),
		uri(inUri),
		booleanArg(inClearCounters)
	{}
	Operation( Vfs::SubsystemCommand inCommand, FileInfoToRowMap *inSelectionMap, const URI &inUri, const QString &inSubsystemManagerName, bool inBoolean=false ) : // weigh, remove
		command(inCommand),
		subsysMngrName(inSubsystemManagerName),
		fileInfo(NULL),
		selectionMap(inSelectionMap),
		uri(inUri),
		booleanArg(false)
	{}

	Vfs::SubsystemCommand command;
	QString       subsysMngrName;
	QString       targetPath;
	FileInfo     *fileInfo;
	FileInfoToRowMap *selectionMap;
	URI           uri;
	bool          booleanArg;
	QString       shellCmd;

// 	QStringList   slChgFilesName;
// 	QDateTime     dtLastAccessTime;
// 	int           chgName, chgPerm, chgOwnGrp, chgAccModTime;

};

} // namespace Vfs

#endif
