/***************************************************************************
 *   Copyright (C) 2009 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _URI_H
#define _URI_H

#include <QString>

namespace Vfs {

/**
 * protocol://[[username[:password]]@host]/path/to/file[[#access_method:[#access_method:[...]]/path/to/file]
 *
 * protocol:        file, ftp, ftps, smb, sftp, ...
 * access_method:   gzip, bzip2, zip, rar, 7z, lzm, iso, ...
 */
class URI
{
public:
	URI() {}
	URI( const QString &sUri );

	void         setUri( const QString &sInUri );

	QString      protocolWithSuffix( bool emptyForFile=true ) const { return (emptyForFile && m_sProtocol == "file") ? "" : m_sProtocol+"://"; }
	QString      protocol() const { return m_sProtocol; }
	void         setProtocol( const QString &sProtocol ) { m_sProtocol = sProtocol; }

	QString      host() const     { return m_sHost;     }
	void         setHost( const QString &sHost )         { m_sHost = sHost; }

	QString      path() const     { return m_sPath;     }
	void         setPath( const QString &sPath )         { m_sPath = sPath; }

	QString      user() const     { return m_sUser;     }
	void         setUser( const QString &sUser )         { m_sUser = sUser; }

	QString      password() const { return m_sPassword; }
	void         setPassword( const QString &sPassword ) { m_sPassword = sPassword; }

	unsigned int port( qint16 nPort=0 ) const { return (nPort == 0) ? m_nPort : nPort; }
	void         setPort( unsigned int nPort ) { m_nPort = nPort; }

	QString      mime() const { return m_sMime; }
	void         setMime( const QString &sMime ) { m_sMime = sMime; }

	// useable for QFileSystemWatcher (local subsystem)
	QString      prevLocalPath() const { return m_prevLocalPath; }
	void         setPrevLocalPath( const QString &sPath ) { m_prevLocalPath = sPath; }


	bool         isValid() const { return (! m_sProtocol.isEmpty() && ! m_sPath.isEmpty()); }

	void         reset();


	QString      toString() const;

	inline bool operator == ( const URI &inUri ) const
    {
		return (
			inUri.m_sHost == m_sHost &&
			inUri.m_sPath == m_sPath &&
			inUri.m_nPort == m_nPort &&
			inUri.m_sUser == m_sUser &&
			inUri.m_sPassword == m_sPassword &&
			inUri.m_sProtocol == m_sProtocol
			   );
	}

	inline bool operator != ( const URI &inUri ) const
    {
		return (
			inUri.m_sHost != m_sHost &&
			inUri.m_sPath != m_sPath &&
			inUri.m_nPort != m_nPort &&
			inUri.m_sUser != m_sUser &&
			inUri.m_sPassword != m_sPassword &&
			inUri.m_sProtocol != m_sProtocol
			   );
	}

private:
	QString      m_sProtocol;
	QString      m_sHost;
	QString      m_sPath;
	QString      m_sUser;
	QString      m_sPassword;
	QString      m_sMime;
	unsigned int m_nPort;
	QString      m_prevLocalPath;

};

} // namespace Vfs

#endif
