/***************************************************************************
 *   Copyright (C) 2015 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "subsystemconst.h"


namespace Vfs {

	QString toString( SubsystemCommand subsysCmd )
	{
		QString msg;

		if      (subsysCmd == NO_COMMAND)	msg = "NO_COMMAND";
		else if (subsysCmd == PROG_EXISTS)	msg = "PROG_EXISTS";
		else if (subsysCmd == CONNECT)	msg = "CONNECT";
		else if (subsysCmd == DISCONNECT)	msg = "DISCONNECT";
		else if (subsysCmd == LOGIN_CMD)	msg = "LOGIN_CMD";
		else if (subsysCmd == OPEN)	msg = "OPEN";
		else if (subsysCmd == LIST)	msg = "LIST";
		else if (subsysCmd == COPY)	msg = "COPY";
		else if (subsysCmd == MOVE)	msg = "MOVE";
		else if (subsysCmd == VIEW)	msg = "VIEW";
		else if (subsysCmd == CREATE_DIR)	msg = "CREATE_DIR";
		else if (subsysCmd == CREATE_FILE)	msg = "CREATE_FILE";
		else if (subsysCmd == CREATE_LINK)	msg = "CREATE_LINK";
		else if (subsysCmd == EDIT_LINK)	msg = "EDIT_LINK";
		else if (subsysCmd == RENAME)	msg = "RENAME";
		else if (subsysCmd == REMOVE)	msg = "REMOVE";
		else if (subsysCmd == WEIGH)	msg = "WEIGH";
		else if (subsysCmd == WEIGH_ITEM) msg = "WEIGH_ITEM";
		else if (subsysCmd == WEIGH_ARCH) msg = "WEIGH_ARCH";
		else if (subsysCmd == FIND)	msg = "FIND";
		else if (subsysCmd == SET_ATTRIBUTES_CMD)	msg = "SET_ATTRIBUTES_CMD";
		else if (subsysCmd == SET_PERM_CMD)	msg = "SET_PERM_CMD";
		else if (subsysCmd == SET_MDATE_CMD)	msg = "SET_MDATE_CMD";
		else if (subsysCmd == SET_OWNER_CMD)	msg = "SET_OWNER_CMD";
		else if (subsysCmd == SET_GROUP_CMD)	msg = "SET_GROUP_CMD";
		else if (subsysCmd == COPY_TO_ARCHIVE)	msg = "COPY_TO_ARCHIVE";
		else if (subsysCmd == MOVE_TO_ARCHIVE)	msg = "MOVE_TO_ARCHIVE";
		else if (subsysCmd == MAKE_ARCHIVE)	msg = "MAKE_ARCHIVE";
		else if (subsysCmd == EXTRACT)	msg = "EXTRACT";
		else if (subsysCmd == EXTRACT_AND_REMOVE)	msg = "EXTRACT_AND_REMOVE";
		else if (subsysCmd == POST_EXTRACT)	msg = "POST_EXTRACT";
		else if (subsysCmd == COMPRESS)	msg = "COMPRESS";
		else if (subsysCmd == DECOMPRESS)	msg = "DECOMPRESS";
		else if (subsysCmd == TEST_ARCHIVE)	msg = "TEST_ARCHIVE";
		else if (subsysCmd == WATCHER_INOTIFY)	msg = "WATCHER_INOTIFY";
		else if (subsysCmd == CD)	msg = "CD";
		else if (subsysCmd == OPEN_WITH_CMD)	msg = "OPEN_WITH_CMD";
		else if (subsysCmd == READ_FILE_TO_VIEW)	msg = "READ_FILE_TO_VIEW";
		else if (subsysCmd == DECOMPRESS_TO_VIEW)	msg = "DECOMPRESS_TO_VIEW";
		else if (subsysCmd == DOWNLOAD)	msg = "DOWNLOAD";
		else if (subsysCmd == UPLOAD)	msg = "UPLOAD";
		else if (subsysCmd == CHK_VER)	msg = "CHK_VER";

		return msg;
	}


	QString toString( SubsystemStatus subsysStat )
	{
		QString msg;

		if      (subsysStat == UNCONNECTED)	msg = "UNCONNECTED";
		else if (subsysStat == CONNECTING)	msg = "CONNECTING...";
		else if (subsysStat == CONNECTED)	msg = "CONNECTED";
		else if (subsysStat == LOGIN)		msg = "LOGIN";
		else if (subsysStat == LOGGED_IN)	msg = "LOGGED_IN";
		else if (subsysStat == CHANGING_DIR)	msg = "CHANGING_DIR";
		else if (subsysStat == CHANGED_DIR)	msg = "CHANGED_DIR";
		else if (subsysStat == LISTING)	msg = "LISTING...";
		else if (subsysStat == LISTED)	msg = "LISTED";
		else if (subsysStat == LISTED_COMPL)	msg = "LISTED_COMPL";
		else if (subsysStat == CLOSING)	msg = "CLOSING...";
		else if (subsysStat == CLOSED)	msg = "CLOSED";
		else if (subsysStat == CREATING_DIR)	msg = "CREATING_DIR";
		else if (subsysStat == CREATING_FILE)	msg = "CREATING_FILE";
		else if (subsysStat == CREATED_DIR)	msg = "CREATED_DIR";
		else if (subsysStat == CREATED_FILE)	msg = "CREATED_FILE";
		else if (subsysStat == RENAMING)	msg = "RENAMING";
		else if (subsysStat == RENAMED)	msg = "RENAMED";
		else if (subsysStat == REMOVING)	msg = "REMOVING...";
		else if (subsysStat == REMOVED)	msg = "REMOVED";
		else if (subsysStat == COPYING)	msg = "COPYING...";
		else if (subsysStat == COPIED)	msg = "COPIED";
		else if (subsysStat == MOVING)	msg = "MOVING...";
		else if (subsysStat == MOVED)	msg = "MOVED";
		else if (subsysStat == WEIGHING)	msg = "WEIGHING...";
		else if (subsysStat == WEIGHED)	msg = "WEIGHED";
		else if (subsysStat == SETTING_ATTRIBUTES)	msg = "SETTING_ATTRIBUTES...";
		else if (subsysStat == SET_ATTRIBUTES)	msg = "SET_ATTRIBUTES";
		else if (subsysStat == SET_PERM)	msg = "SET_PERM";
		else if (subsysStat == SET_MDATE)	msg = "SET_MDATE";
		else if (subsysStat == SET_OWNER)	msg = "SET_OWNER";
		else if (subsysStat == SET_GROUP)	msg = "SET_GROUP";
		else if (subsysStat == FINDING)	msg = "FINDING";
		else if (subsysStat == FOUND)	msg = "FOUND";
		else if (subsysStat == CREATING_LINK)	msg = "CREATING_LINK";
		else if (subsysStat == CREATED_LINK)	msg = "CREATED_LINK";
		else if (subsysStat == EDITING_LINK)	msg = "EDITING_LINK";
		else if (subsysStat == EDITED_LINK)	msg = "EDITED_LINK";
		else if (subsysStat == OPENING_WITH)	msg = "OPENING_WITH...";
		else if (subsysStat == OPEN_WITH)	msg = "OPEN_WITH...";
		else if (subsysStat == OPENED_FOR_WRITE)	msg = "OPENED_FOR_WRITE";
		else if (subsysStat == WATCHER_REQ_ADD)	msg = "WATCHER_REQ_ADD";
		else if (subsysStat == WATCHER_REQ_DEL)	msg = "WATCHER_REQ_DEL";
		else if (subsysStat == WATCHER_REQ_REN)	msg = "WATCHER_REQ_REN";
		else if (subsysStat == WATCHER_REQ_CHATR)	msg = "WATCHER_REQ_CHATR";
		else if (subsysStat == WATCHER_REQ_LOCK)	msg = "WATCHER_REQ_LOCK";
		else if (subsysStat == OPR_STOPPED)	msg = "OPR_STOPPED";
		else if (subsysStat == COMPRESSING)	msg = "COMPRESSING";
		else if (subsysStat == COMPRESSED)	msg = "COMPRESSED";
		else if (subsysStat == DECOMPRESSING)	msg = "DECOMPRESSING";
		else if (subsysStat == DECOMPRESSED)	msg = "DECOMPRESSED";
		else if (subsysStat == ARCHIVING)	msg = "ARCHIVING";
		else if (subsysStat == ARCHIVED)	msg = "ARCHIVED";
		else if (subsysStat == MAKING_ARCHIVE)	msg = "MAKING_ARCHIVE";
		else if (subsysStat == MADE_ARCHIVE)	msg = "MADE_ARCHIVE";
		else if (subsysStat == EXTRACTING)	msg = "EXTRACTING";
		else if (subsysStat == EXTRACTED)	msg = "EXTRACTED";
		else if (subsysStat == EXTRACTED_TO_VIEW)	msg = "EXTRACTED_TO_VIEW";
		else if (subsysStat == DOWNLOADING)	msg = "DOWNLOADING";
		else if (subsysStat == DOWNLOADED)	msg = "DOWNLOADED";
		else if (subsysStat == DOWNLOADED_TO_VIEW)	msg = "DOWNLOADED_TO_VIEW";
		else if (subsysStat == UPLOADING)	msg = "UPLOADING";
		else if (subsysStat == UPLOADED)	msg = "UPLOADED";
		else if (subsysStat == ERROR_OCCURED)	msg = "ERROR_OCCURED";
		else if (subsysStat == READY_TO_READ)	msg = "READY_TO_READ";
		else if (subsysStat == NOT_READY_TO_READ)	msg = "NOT_READY_TO_READ";
		else if (subsysStat == DECOMPRESSED_TO_VIEW)	msg = "DECOMPRESSED_TO_VIEW";
		else if (subsysStat == READING_FILE)	msg = "READING_FILE";
		else if (subsysStat == READ_FILE)	msg = "READ_FILE";
		else if (subsysStat == VIEW_COMPL)	msg = "VIEW_COMPL";
		else if (subsysStat == OPR_PAUSED)	msg = "OPR_PAUSED";
		else if (subsysStat == UNKNOWN_STAT)	msg = "UNKNOWN_STAT";

		return msg;
	}


	QString toString( SubsystemError subsysError )
	{
		QString msg;

		if      (subsysError == NO_ERROR)	msg = "NO_ERROR";
		else if (subsysError == CANNOT_CONNECT)	msg = "CANNOT_CONNECT";
		else if (subsysError == CANNOT_LOGIN)	msg = "CANNOT_LOGIN";
		else if (subsysError == CANNOT_OPEN)	msg = "CANNOT_OPEN";
		else if (subsysError == CANNOT_READ)	msg = "CANNOT_READ";
		else if (subsysError == CANNOT_WRITE)	msg = "CANNOT_WRITE";
		else if (subsysError == CANNOT_FOUND)	msg = "CANNOT_FOUND";
		else if (subsysError == CANNOT_CREATE_DIR)	msg = "CANNOT_CREATE_DIR";
		else if (subsysError == CANNOT_CREATE_FILE)	msg = "CANNOT_CREATE_FILE";
		else if (subsysError == CANNOT_CREATE_LINK)	msg = "CANNOT_CREATE_LINK";
		else if (subsysError == CANNOT_EDIT_LINK)	msg = "CANNOT_EDIT_LINK";
		else if (subsysError == CANNOT_RENAME)	msg = "CANNOT_RENAME";
		else if (subsysError == CANNOT_REMOVE)	msg = "CANNOT_REMOVE";
		else if (subsysError == CANNOT_EXECUTE)	msg = "CANNOT_EXECUTE";
		else if (subsysError == CANNOT_SET_PERM)	msg = "CANNOT_SET_PERM";
		else if (subsysError == CANNOT_SET_MA_TIME)	msg = "CANNOT_SET_MA_TIME";
		else if (subsysError == CANNOT_SET_OWNER_GROUP)	msg = "CANNOT_SET_OWNER_GROUP";
		else if (subsysError == CMD_NOT_FOUND)	msg = "CMD_NOT_FOUND";
		else if (subsysError == CANCELED)	msg = "CANCELED";
		else if (subsysError == DOES_NOT_EXIST)	msg = "DOES_NOT_EXIST";
		else if (subsysError == ALREADY_EXISTS)	msg = "ALREADY_EXISTS";
		else if (subsysError == CANNOT_GET)	msg = "CANNOT_GET";
		else if (subsysError == CANNOT_PUT)	msg = "CANNOT_PUT";
		else if (subsysError == CANNOT_STAT)	msg = "CANNOT_STAT";
		else if (subsysError == CANNOT_COMPRESS)	msg = "CANNOT_COMPRESS";
		else if (subsysError == CANNOT_DECOMPRESS)	msg = "CANNOT_DECOMPRESS";
		else if (subsysError == CANNOT_ARCHIVE)	msg = "CANNOT_ARCHIVE";
		else if (subsysError == CANNOT_MAKE_ARCHIVE)	msg = "CANNOT_MAKE_ARCHIVE";
		else if (subsysError == CANNOT_EXTRACT)	msg = "CANNOT_EXTRACT";
		else if (subsysError == CANNOT_DOWNLOAD)	msg = "CANNOT_DOWNLOAD";
		else if (subsysError == CANNOT_UPLOAD)	msg = "CANNOT_UPLOAD";
		else if (subsysError == CANNOT_COPY_TO_THESAME)	msg = "CANNOT_COPY_TO_THESAME";
		else if (subsysError == SKIP_FILE)	msg = "SKIP_FILE";
		else if (subsysError == NO_FREE_SPACE)	msg = "NO_FREE_SPACE";
		else if (subsysError == READING_ERROR)	msg = "READING_ERROR";
		else if (subsysError == NOT_SUPPORTED)	msg = "NOT_SUPPORTED";
		else if (subsysError == INTERNAL_ERROR)	msg = "INTERNAL_ERROR";
		else if (subsysError == OTHER_ERROR)	msg = "OTHER_ERROR";

		return msg;
	}


	QString toString( OpenInTab openInTab )
	{
		QString msg;
		if      (openInTab == NEW_TAB)	msg = "NEW_TAB";
		else if (openInTab == NEW_ACTIVE_TAB)	msg = "NEW_ACTIVE_TAB";
		else if (openInTab == NEW_NOACTIVE_TAB)	msg = "NEW_NOACTIVE_TAB";
		else if (openInTab == OPPOSITE_ACTIVE_TAB)	msg = "OPPOSITE_ACTIVE_TAB";

		return msg;
	}

} // namespace Vfs

