/***************************************************************************
 *   Copyright (C) 2003 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _SQUEEZELABEL_H
#define _SQUEEZELABEL_H

#include <QLabel>

/**
 * @author Piotr Mierzwiński
 **/
class SqueezeLabel : public QLabel
{
	Q_OBJECT
	Q_PROPERTY( bool showToolTip READ showToolTip WRITE setShowToolTip )

public:
	SqueezeLabel( QWidget *pParent );

// 	virtual ~SqueezeLabel() {}

	enum SqueezingMode { SQUEEZING_LINE, WRAP_LINE };

	void setShowToolTip( bool bShow ) { m_bShowTip = bShow; }
	void setSqueezingMode( SqueezingMode squeezingMode ) { m_squeezingMode = squeezingMode; }

	bool showToolTip() const { return m_bShowTip; }

	static QString squeezeLine( const QString &sLine, const int &nMaxChars, QWidget *w );

private:
	bool m_bShowTip;
	QString m_sFullText;
	SqueezingMode m_squeezingMode;

	void setSqueezingText( uint nLabelWidth );

	QString squeezeLine( const QString &sLine, uint nLabelWidthInPixels );

public slots:
	virtual void setText( const QString &sText );

protected:
	virtual void resizeEvent( QResizeEvent * );

};

#endif
