/***************************************************************************
 *   Copyright (C) 2005 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _CONNECTIONDIALOG_H
#define _CONNECTIONDIALOG_H

#include "ui_connectiondlg.h"
#include "uri.h"


using namespace Vfs;
/**
	@author Piotr Mierzwiński
*/
class ConnectionDialog : public QDialog, Ui::ConnectionDlg
{
	Q_OBJECT
public:
	ConnectionDialog();
	~ConnectionDialog() {}

	/** Static method that shows FTP connection dialog.
	 * @param pParent pointer to parent window (main window of application)
	 * @param uri - initialization URI,
	 * @param sProtocol protocol name (used for pointing path to read value of ShowConnDlgIfEmptyPassword)
	 * @param sNoPassUser user name which no requires password for connection
	 * @return TRUE if user accepted parameters otherwise FALSE.
	 */
	static bool showDlg( QWidget *pParent, URI &uri, const QString &sProtocol, const QString &sNoPassUser );

private:
	/** Initialisation of connection parameters.
	 * Try to read user names from configuration file.
	 * @param uri - init URI
	 */
	void init( const URI &uri );

	/** Generating new URI like a string's format.
	 * @param sNewURL - formated connection string (matching to following template: "ftp://user:pass@host/dir")
	 */
	void makeNewURL( QString &sNewURL );

private:
	QStringList m_sUsersList;
	QStringList m_sUDirsList;

};

#endif // _CONNECTIONDIALOG_H
