/***************************************************************************
 *   Copyright (C) 2008 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <QtGui>

#include "shortkeyeditboxdata.h"
#include "shortkeyeditboxeditor.h"
#include "shortkeyeditboxdelegate.h"


ShortKeyEditBoxDelegate::ShortKeyEditBoxDelegate( QObject *pParent ) : QItemDelegate(pParent)
{
}


QWidget *ShortKeyEditBoxDelegate::createEditor( QWidget *pParent, const QStyleOptionViewItem &option, const QModelIndex &index ) const
{
	//qDebug() << "ShortKeyEditBoxDelegate::createEditor. variant=" << qVariantCanConvert<ShortKeyEditBoxData>(index.data()) << "modelIndex= " << index << "index.data()= " << index.data();
	QVariant variant = index.data();
	if (variant.canConvert<ShortKeyEditBoxData>()) {
		//qDebug() << "ShortKeyEditBoxDelegate::createEditor - ShortKeyEditBoxData, data valid";
		ShortKeyEditBoxEditor *pEditor = new ShortKeyEditBoxEditor(pParent);
		connect(pEditor, &ShortKeyEditBoxEditor::signalDone, this, &ShortKeyEditBoxDelegate::slotCommitAndCloseEditor);

		return pEditor;
	}
	else {
		return QItemDelegate::createEditor(pParent, option, index);
	}
}

void ShortKeyEditBoxDelegate::setEditorData( QWidget *pEditor, const QModelIndex &index ) const
{
	QVariant variant = index.data();
	if (variant.canConvert<ShortKeyEditBoxData>()) {
		ShortKeyEditBoxData shortKeyEditBoxData = variant.value<ShortKeyEditBoxData>();
		ShortKeyEditBoxEditor *pShortKeyEditBoxEditor = qobject_cast<ShortKeyEditBoxEditor *>(pEditor);
		pShortKeyEditBoxEditor->setShortKeyEditBoxData(shortKeyEditBoxData);
		pShortKeyEditBoxEditor->setText(shortKeyEditBoxData.text());
	}
	else {
		QItemDelegate::setEditorData(pEditor, index);
	}
}

void ShortKeyEditBoxDelegate::setModelData( QWidget *pEditor, QAbstractItemModel *pModel, const QModelIndex &index ) const
{
	QVariant variant = index.data();
	if (variant.canConvert<ShortKeyEditBoxData>()) {
		ShortKeyEditBoxEditor *pShortKeyEditBoxEditor = qobject_cast<ShortKeyEditBoxEditor *>(pEditor);
		pModel->setData(index, QVariant::fromValue(pShortKeyEditBoxEditor->shortKeyEditBoxData()));
	} else {
		QItemDelegate::setModelData(pEditor, pModel, index);
	}
}

void ShortKeyEditBoxDelegate::updateEditorGeometry( QWidget *pEditor, const QStyleOptionViewItem &option, const QModelIndex & ) const
{
	pEditor->setGeometry(option.rect);
}


void ShortKeyEditBoxDelegate::paint( QPainter *pPainter, const QStyleOptionViewItem &option, const QModelIndex &index ) const
{
	QVariant variant = index.data();
	if (variant.canConvert<ShortKeyEditBoxData>()) {
		ShortKeyEditBoxData shortKeyEditBoxData = variant.value<ShortKeyEditBoxData>();
		if (option.state & QStyle::State_Selected) {
			pPainter->fillRect(option.rect, option.palette.highlight());
			pPainter->setPen(option.palette.highlightedText().color());
		}
		else
			pPainter->setPen(option.palette.text().color());

		shortKeyEditBoxData.paint(pPainter, option.rect, option.palette);
	}
	else {
		QItemDelegate::paint(pPainter, option, index);
	}
}


void ShortKeyEditBoxDelegate::slotCommitAndCloseEditor()
{
// 	qDebug() << "ShortKeyEditBoxDelegate::slotCommitAndCloseEditor (emit commitData and closeEditor)";
	ShortKeyEditBoxEditor *pEditor = qobject_cast<ShortKeyEditBoxEditor *>(sender());
	emit commitData(pEditor);
	emit closeEditor(pEditor);
}

