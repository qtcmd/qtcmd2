/***************************************************************************
 *   Copyright (C) 2008 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QtGui>

#include "listwidgetbutton.h"


ListWidgetButton::ListWidgetButton( QTableWidgetItem *pItem, Button eButton, Qt::Alignment eAlign, QWidget *pParent )
	: QWidget(pParent)
	, m_pTableWidgetItem(pItem), m_eButton(eButton), m_eAlignment(eAlign)
	, m_pRadioButton(NULL), m_pPushButton(NULL), m_pCheckBox(NULL)
{
	m_pLayout = new QHBoxLayout(this);
	setLayout(m_pLayout);

	if (m_eButton == RadioButton) {
		m_pRadioButton = new QRadioButton;
		m_pRadioButton->setAutoExclusive(false);
		m_pRadioButton->setMinimumSize(m_pRadioButton->sizeHint().width(), m_pRadioButton->sizeHint().height()-8);
// 		m_pRadioButton->setMaximumSize(QSize(28, 16777215));
	}
	else
	if (m_eButton == PushButton) {
		m_pPushButton = new QPushButton;
		m_pPushButton->setMinimumSize(m_pPushButton->sizeHint().width()-40, m_pPushButton->sizeHint().height()-12);
// 		QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
// 		m_pPushButton->setSizePolicy(sizePolicy);
		m_pPushButton->setMaximumSize(QSize(28, 16777215));
		//m_pPushButton->setFlat(true); // for flat is hard to set color
	}
	else
	if (m_eButton == CheckBox) {
		m_pCheckBox = new QCheckBox;
		m_pCheckBox->setMinimumSize(m_pCheckBox->sizeHint().width(), m_pCheckBox->sizeHint().height()-8);
	}

	// work around to achieve alignment of widget
	if (buttonPtr() != NULL) {
		m_pLayout->addWidget(buttonPtr(), 0, Qt::AlignVCenter | eAlign);

		m_Color = buttonPtr()->palette().windowText().color();
		buttonPtr()->setFocusPolicy(Qt::NoFocus);
// 		connect(buttonPtr(), SIGNAL(clicked()), this, SIGNAL(clicked()));
		connect(buttonPtr(), &QAbstractButton::clicked, this, &ListWidgetButton::clicked);
	}

	m_eAlignment = eAlign;
}

ListWidgetButton::~ListWidgetButton()
{
	delete m_pRadioButton;  m_pRadioButton=NULL;
	delete m_pPushButton;  m_pPushButton=NULL;
	delete m_pCheckBox;  m_pCheckBox=NULL;
	delete m_pLayout;  m_pLayout=NULL;
}


void ListWidgetButton::setColor( const QColor &color )
{
	m_Color = color;
	QPalette newPalette = palette();
	newPalette.setColor(QPalette::Button, color);

	m_pButton->setPalette(newPalette);
}


QAbstractButton * ListWidgetButton::buttonPtr()
{
	if (m_eButton == RadioButton)
		m_pButton = dynamic_cast<QAbstractButton *>(m_pRadioButton);
	else
	if (m_eButton == PushButton)
		m_pButton = dynamic_cast<QAbstractButton *>(m_pPushButton);
	else
	if (m_eButton == CheckBox) {
		m_pButton = dynamic_cast<QAbstractButton *>(m_pCheckBox);
	}

	return m_pButton;
}


