/***************************************************************************
 *   Copyright (C) 2017 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDebug> // needs to qDebug()
#include <QKeyEvent>
#include <QLineEdit>

#include "settings.h"
#include "findreplacetextbar.h"

#define CLOSE_TIMEOUT  5000


FindReplaceTextBar::FindReplaceTextBar( QWidget *pParent, const QString &sConfigKey )
{
	setupUi(this);
	setParent(pParent);

	connect(m_nextButton, &QPushButton::clicked, this, &FindReplaceTextBar::slotNextMatching);
	connect(m_prevButton, &QPushButton::clicked, this, &FindReplaceTextBar::slotPrevMatching);

	connect(m_findWholeWordsChkBox, &QCheckBox::toggled, this, &FindReplaceTextBar::signalFindWholeWords);
	connect(m_caseSensitiveChkBox, &QCheckBox::toggled, this, &FindReplaceTextBar::slotMatchWithCaseSensitive);
	connect(m_startFromCursorChkBox, &QCheckBox::toggled, this, &FindReplaceTextBar::signalStartSearchFromCursor);
	connect(m_regularExpressionChkBox, &QCheckBox::toggled, this, &FindReplaceTextBar::slotMatchAsRegularExpression);
	connect(m_pReplaceNext, &QPushButton::clicked, this, &FindReplaceTextBar::slotReplaceNextOnly);
	connect(m_pReplaceAll,  &QPushButton::clicked, this, &FindReplaceTextBar::slotReplaceAll);

	m_cbFindModel = new QStringListModel();
	m_cbReplaceModel = new QStringListModel();
	m_pFindTextComboBox->setModel(m_cbFindModel);
	m_pFindTextComboBox->installEventFilter(this);
	m_pReplaceTextComboBox->setModel(m_cbReplaceModel);

	n_sConfigKey = sConfigKey;
	if (sConfigKey.isEmpty()) {
		qDebug() << "FindReplaceTextBar::FindReplaceTextBar. WARNING. Empty ConfigKey, will be use key \"unknown\"";
		n_sConfigKey = "unknown";
	}

	connect(m_pMessageCloseButton, &QPushButton::clicked, this, &FindReplaceTextBar::slotHideErrorMessage);
	connect(m_pQuestionPushButton1,  &QPushButton::clicked, this, &FindReplaceTextBar::slotClickedQuestionBtn1);
	connect(m_pQuestionPushButton2,  &QPushButton::clicked, this, &FindReplaceTextBar::slotClickedQuestionBtn2);

	// -- set replace combobox as white color
	QPalette palette;
	QLineEdit *pLineEdit = m_pReplaceTextComboBox->lineEdit();
	palette.setColor(pLineEdit->backgroundRole(), Qt::white);
	palette.setColor(pLineEdit->foregroundRole(), Qt::black);
	pLineEdit->setPalette(palette);

	// support for auto closing
	m_closeTimer = new QTimer(this);
 	connect(m_closeTimer, &QTimer::timeout, this, &FindReplaceTextBar::slotHideErrorMessage);
}

FindReplaceTextBar::~FindReplaceTextBar()
{
	Settings settings;
	settings.setValue(n_sConfigKey+"/FindReplaceCaseSensitive", m_caseSensitiveChkBox->isChecked());
	settings.setValue(n_sConfigKey+"/FindReplaceRegularExpression", m_regularExpressionChkBox->isChecked());

	if (m_pFindTextComboBox->count() > 0)
		settings.setValue(n_sConfigKey+"/FindText", m_cbFindModel->stringList());
	if (m_pReplaceTextComboBox->count() > 0)
		settings.setValue(n_sConfigKey+"/ReplaceText", m_cbReplaceModel->stringList());

	if (m_pFindTextComboBox->currentIndex() >= 0)
		settings.setValue(n_sConfigKey+"/FindTextLastId", m_pFindTextComboBox->findText(m_pFindTextComboBox->currentText()));
	if (m_pReplaceTextComboBox->currentIndex() >= 0)
		settings.setValue(n_sConfigKey+"/ReplaceTextLastId", m_pReplaceTextComboBox->findText(m_pReplaceTextComboBox->currentText()));

// 	qDebug() << "FindReplaceTextBar::~FindReplaceTextBar. m_pFindTextComboBox->currentIndex" << m_pFindTextComboBox->currentIndex();

	if (m_closeTimer != NULL) {
		delete m_closeTimer;  m_closeTimer = NULL;
	}
	if (m_cbFindModel != NULL) {
		delete m_cbFindModel;  m_cbFindModel = NULL;
	}
	if (m_cbReplaceModel != NULL) {
		delete m_cbReplaceModel;  m_cbReplaceModel = NULL;
	}
}


void FindReplaceTextBar::init( const QString &sInitStr, bool bDocEditMode, bool bVisibleReplaceBar )
{
	qDebug() << "FindReplaceTextBar::init. sInitStr:" << sInitStr << "bDocEditMode:" << bDocEditMode;
	m_searchDirection = SD_none;
	slotUpdateMatchingStatus(MS_none); // set background (as white) of edit field

	m_bFoundMatching = false;
	m_FindMode = (! bVisibleReplaceBar);

	m_pMessageFrame->hide();
	m_pQuestionPushButton1->hide();
	m_pQuestionPushButton2->hide();

// 	m_startFromCursorChkBox->setChecked(m_pSettings->value(n_sConfigKey+"/StartFromCursor", true).toBool());
	m_startFromCursorChkBox->setChecked(true);
	m_pReplaceFrame->setVisible(bVisibleReplaceBar);

	Settings settings;
	if (settings.value(n_sConfigKey+"/CaseSensitive", false).toBool()) {
		m_matchingType = MatchType::MT_caseSensitive;
		m_caseSensitiveChkBox->setChecked(true);
	}
	else
	if (settings.value(n_sConfigKey+"/RegularExpression", false).toBool()) {
		m_matchingType = MatchType::MT_regularExpression;
		m_regularExpressionChkBox->setChecked(true);
	}

	m_pFindTextComboBox->clear();
	QStringList slFindItems =  settings.value(n_sConfigKey+"/FindText").toStringList();
	if (! slFindItems.isEmpty()) {
		m_pFindTextComboBox->addItems(slFindItems);
		m_pFindTextComboBox->setCurrentIndex(settings.value(n_sConfigKey+"/FindTextLastId", 0).toInt());
	}

	m_pReplaceTextComboBox->clear();
	QStringList slReplaceItems =  settings.value(n_sConfigKey+"/ReplaceText").toStringList();
	if (! slReplaceItems.isEmpty()) {
		m_pReplaceTextComboBox->addItems(slReplaceItems);
		m_pReplaceTextComboBox->setCurrentIndex(settings.value(n_sConfigKey+"/ReplaceTextLastId", 0).toInt());
	}

	if (! sInitStr.isEmpty()) {
		if (m_pFindTextComboBox->findText(sInitStr) == -1)
			m_pFindTextComboBox->addItem(sInitStr);
		m_pFindTextComboBox->lineEdit()->setText(sInitStr);
	}
	m_pFindTextComboBox->lineEdit()->setFocus();

	m_startFromCursorChkBox->setChecked(bDocEditMode);
	m_startFromCursorChkBox->setEnabled(bDocEditMode);
}

void FindReplaceTextBar::setMessage( MessageType eMessageType, const QString &sMessage, bool bHideAfterCoupleSec, const QString &sButton1Msg, const QString &sButton2Msg )
{
    m_pMessageLabel->setText(sMessage);

	QColor bgColor;
	QPalette palette;
	if (eMessageType == MT_Error)
		bgColor = QColor(176,121,121); // #b07979
	else
		bgColor = QColor(201,241,215);

	palette.setColor(m_pMessageLabel->backgroundRole(), bgColor);
	if (eMessageType == MT_Info)
		palette.setColor(m_pMessageLabel->foregroundRole(), Qt::black);

	m_pMessageLabel->setPalette(palette);
	m_pMessageLabel->setAutoFillBackground(true);

	if (! sButton1Msg.isEmpty()) {
		m_pQuestionPushButton1->show();
		m_pQuestionPushButton1->setText(sButton1Msg);
	}
	else
		m_pQuestionPushButton1->hide();

	if (! sButton2Msg.isEmpty()) {
		m_pQuestionPushButton2->show();
		m_pQuestionPushButton2->setText(sButton2Msg);
	}
	else
		m_pQuestionPushButton2->hide();

	if (bHideAfterCoupleSec)
		m_closeTimer->start(CLOSE_TIMEOUT);

    m_pMessageFrame->show();
}

void FindReplaceTextBar::keyPressed( QKeyEvent *pKeyEvent )
{
	m_searchDirection = SD_none;
	m_bStartSearch = false;

	unsigned int key = pKeyEvent->key();
	QString     text = pKeyEvent->text();
// 	bool bCtrlPressed = (pKeyEvent->modifiers() == Qt::CTRL && key == Qt::Key_Control);
	bool bCtrlPressed = (pKeyEvent->modifiers() == Qt::CTRL);

	if (m_pMessageFrame->isVisible())
		m_pMessageFrame->hide();

	switch ( key ) {
		case Qt::Key_Down:
		case Qt::Key_Enter:
		case Qt::Key_Return: {
			slotNextMatching();
			break;
		}

		case Qt::Key_Left:
		case Qt::Key_Right:
		case Qt::Key_PageUp:
		case Qt::Key_PageDown:
		case Qt::Key_Escape:
			break;

		case Qt::Key_Up:
			slotPrevMatching();
			break;

		case Qt::Key_Home:
			if (bCtrlPressed) {
				m_searchDirection = SD_home;
				search(true, SF_FromVeryBegining);
			}
			break;

		case Qt::Key_End:
			if (bCtrlPressed) {
				m_searchDirection = SD_end;
				search(true, SF_FromEnd);
			}
			break;

		case Qt::Key_F:
			if (bCtrlPressed) {
				m_searchDirection = SD_behind;
			}
			break;

		case Qt::Key_R: { // clear (Remove) string to find
			if (bCtrlPressed) {
				if (m_pFindTextComboBox->hasFocus()) {
					m_pFindTextComboBox->lineEdit()->clear();
					slotUpdateMatchingStatus(MS_none);
				}
				else
				if (m_pReplaceTextComboBox->hasFocus())
					m_pReplaceTextComboBox->lineEdit()->clear();
			}
		}
		break;

		default: {
			if (text.isEmpty()) {
				if (pKeyEvent->modifiers() == Qt::NoModifier) {
					if (key == Qt::Key_AltGr) // right Alt
						break;
					break;
				}
				else { // modifier pressed
					m_searchDirection = SD_behind;
					break;
				}
			}
// 			qDebug() << "FindReplaceTextBar::keyPressed (default) text.length:" << text.length() << "SearchDirection:" << toString(m_searchDirection);
			if (text.length() == 1)
				m_searchDirection = SD_down_inc;
		} // default
		break;
	} // switch
}

void FindReplaceTextBar::search( bool bStart, FindReplaceTextBar::StartFrom eStartFrom )
{
	QString sCurrentText = m_pFindTextComboBox->lineEdit()->text();
	if (sCurrentText.isEmpty()) {
		slotUpdateMatchingStatus(MS_none);
		emit signalSearch("", false, SF_ignoreFlg);
		return;
	}
// 	qDebug() << "FindReplaceTextBar::search. sCurrentText:" << sCurrentText << "start:" << bStart << "SearchDirection:" << toString(m_searchDirection);
	if (m_bStartSearch && m_searchDirection != SD_down_inc && m_searchDirection != SD_none) {
		if (m_pFindTextComboBox->findText(sCurrentText) == -1)
			m_pFindTextComboBox->addItem(sCurrentText);
	}

	emit signalSearch(sCurrentText, bStart, eStartFrom);
}

QString FindReplaceTextBar::toString( SearchDirection eSearchDirection ) const
{
	if (eSearchDirection == SD_home) return "SD_home";
	else
	if (eSearchDirection == SD_up)   return "SD_up";
	else
	if (eSearchDirection == SD_none) return "SD_none";
	else
	if (eSearchDirection == SD_down) return "SD_down";
	else
	if (eSearchDirection == SD_end)  return "SD_end";
	else
	if (eSearchDirection == SD_down_inc) return "SD_down_inc";
	else
	if (eSearchDirection == SD_behind) return "SD_behind";

	return "unknown";
}


bool FindReplaceTextBar::eventFilter( QObject *obj, QEvent *event )
{
	if (obj == m_pFindTextComboBox) {
		QEvent::Type eventType = event->type();
		QKeyEvent *pKeyEvent = static_cast <QKeyEvent *> (event);
		unsigned int key = pKeyEvent->key();

		if (eventType == QEvent::KeyPress) {
			keyPressed(pKeyEvent);
			if (m_searchDirection != SD_none && m_searchDirection != SD_down_inc)
				return true; // eat event
		}
		else
		if (eventType == QEvent::KeyRelease) {
			bool bCtrlPressed  = (pKeyEvent->modifiers() == Qt::CTRL || key == Qt::Key_Control);
			bool bEnterPressed = (key == Qt::Key_Enter || key == Qt::Key_Return);
			if (m_searchDirection == SD_down_inc && ! bCtrlPressed && ! bEnterPressed && pKeyEvent->modifiers() == Qt::NoModifier) {
// 				qDebug() << "FindReplaceTextBar::eventFilter. KeyRelease. m_searchDirection:" << toString(m_searchDirection);
				m_bStartSearch = false;
				search(true);
			}
			return false;
		}
	}

	return QWidget::eventFilter(obj, event);
}

// ---------------------- SLOTs -----------------------

void FindReplaceTextBar::slotUpdateMatchingStatus( MatchingStatus eMatchStatus )
{
	QColor   bgColor;
	QPalette palette;

	if (eMatchStatus == MS_match)
		bgColor = QColor(201,241,215); // old: 228, 240, 223
	else
	if (eMatchStatus == MS_notMatch)
		bgColor = QColor(255, 229, 229); // old: 255, 229, 229
//		bgColor = QColor(241,229,229); // old: 255, 229, 229
	else
		bgColor = Qt::white;

	QLineEdit *pLineEdit = m_pFindTextComboBox->lineEdit();
	palette.setColor(pLineEdit->backgroundRole(), bgColor);
	palette.setColor(pLineEdit->foregroundRole(), Qt::black);
	pLineEdit->setPalette(palette);
}


void FindReplaceTextBar::slotNextMatching()
{
    m_pMessageFrame->hide();
	m_searchDirection = SD_down;
	m_bStartSearch = true;
	search(false);
}

void FindReplaceTextBar::slotPrevMatching()
{
    m_pMessageFrame->hide();
	m_searchDirection = SD_up;
	m_bStartSearch = true;
	search(false);
}

void FindReplaceTextBar::slotClickedQuestionBtn1()
{
    m_pMessageFrame->hide();
    emit signalClickedQuestionBtn1();
}

void FindReplaceTextBar::slotClickedQuestionBtn2()
{
    m_pMessageFrame->hide();
    emit signalClickedQuestionBtn2();
}

void FindReplaceTextBar::slotReplaceNextOnly()
{
	QString sNewString = m_pReplaceTextComboBox->lineEdit()->text();
	QString sStringToReplace = m_pFindTextComboBox->lineEdit()->text();
	if (m_pReplaceTextComboBox->findText(sNewString) == -1)
		m_pReplaceTextComboBox->addItem(sNewString);

    emit signalStartReplace(ReplaceType::RT_ReplaceNextOnly, sStringToReplace, sNewString);
}

void FindReplaceTextBar::slotReplaceAll()
{
	QString sNewString = m_pReplaceTextComboBox->lineEdit()->text();
	QString sStringToReplace = m_pFindTextComboBox->lineEdit()->text();
	if (m_pReplaceTextComboBox->findText(sNewString) == -1)
		m_pReplaceTextComboBox->addItem(sNewString);

    emit signalStartReplace(ReplaceType::RT_ReplaceAll, sStringToReplace, sNewString);
}


void FindReplaceTextBar::slotMatchWithCaseSensitive( bool bMatchCaseSensitive )
{
	m_searchDirection = SD_none;
	m_matchingType = MatchType::MT_caseSensitive;

	if (bMatchCaseSensitive)
		m_regularExpressionChkBox->setChecked(false);

	Settings settings;
	settings.setValue(n_sConfigKey+"/CaseSensitive", m_caseSensitiveChkBox->isChecked());

	if (! m_pFindTextComboBox->lineEdit()->text().isEmpty())
		search(true);
}

void FindReplaceTextBar::slotMatchAsRegularExpression( bool bMatchRegularExpr )
{
	m_searchDirection = SD_none;
	m_matchingType = MatchType::MT_regularExpression;

	if (bMatchRegularExpr)
		m_caseSensitiveChkBox->setChecked(false);

	Settings settings;
	settings.setValue(n_sConfigKey+"/RegularExpression", m_caseSensitiveChkBox->isChecked());

	if (! m_pFindTextComboBox->lineEdit()->text().isEmpty())
		search(true);
}

