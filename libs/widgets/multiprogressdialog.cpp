/***************************************************************************
 *   Copyright (C) 2015 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDebug>

#include "multiprogressdialog.h"


MultiProgressDialog::MultiProgressDialog( QWidget *pParent, bool bModal )
{
	setupUi(this);
	setModal(bModal);
	setParent(pParent);
	setWindowFlags(Qt::Dialog);

	m_pProcessedFilesListView->setParent(this); // needed when child (m_pFoundFilesListView) calls method from parent (this)

	m_pCancelBtn->setShortcut(Qt::Key_Escape);
	m_pCancelBtn->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogCancelButton));

	m_nTotalNum = 0;
	m_lastSubsysCmd = Vfs::NO_COMMAND;
	m_pMultiProgressList = NULL;
}

MultiProgressDialog::~MultiProgressDialog()
{
	delete m_pMultiProgressList;
}


void MultiProgressDialog::init( const QString &sTargetName, FileInfoToRowMap *pSelectionMap )
{
	if (pSelectionMap == NULL) {
		qDebug() << "MultiProgressDialog::init. Internal ERROR. pSelectionMap is NULL";
		return;
	}
	if (pSelectionMap->size() == 0) {
		qDebug() << "MultiProgressDialog::init. SelectionMap is empty";
		return;
	}
	m_pTargetLabValue->setText(sTargetName);

	if (m_pMultiProgressList == NULL)
		m_pMultiProgressList = new MultiProgressList;
	m_pMultiProgressList->clear();
	m_multiProgressMap.clear();

	MultiProgressListViewItem *pItem;
	FileInfoToRowMap::const_iterator it = pSelectionMap->constBegin();
	while (it != pSelectionMap->constEnd()) {
		pItem = new MultiProgressListViewItem(it.key(), tr("Unknown"), 0);
		m_pMultiProgressList->append(pItem);
		m_multiProgressMap.insert(it.key(), pItem);
		++it;
	}
	m_pProcessedFilesListView->initListView(m_pMultiProgressList);
}

void MultiProgressDialog::showDialog( Vfs::SubsystemCommand eSubsysCmd, bool bCloseAfterFinished )
{
	setWndTitle(eSubsysCmd);
	m_lastSubsysCmd = eSubsysCmd;

	m_pCloseAfterFinishingChkBox->setChecked(bCloseAfterFinished);

	show();
}



void MultiProgressDialog::setWndTitle( Vfs::SubsystemCommand eSubsysCmd )
{
	QString sTitle, sLabel;

	if (eSubsysCmd == Vfs::COPY) {
		sTitle = tr("Copying");
		sLabel = tr("Copied:");
	}
	if (eSubsysCmd == Vfs::MOVE) {
		sTitle = tr("Moving");
		sLabel = tr("Moved:");
	}
	else
	if (eSubsysCmd == Vfs::REMOVE) {
		sTitle = tr("Removing");
		sLabel = tr("Removed:");
	}
	else
	if (eSubsysCmd == Vfs::WEIGH) {
		sTitle = tr("Weighing");
		sLabel = tr("Weight:");
	}
	else
	if (eSubsysCmd == Vfs::ARCHIVE) {
		sTitle = tr("Archiving");
		sLabel = tr("Archived:");
	}
	else
	if (eSubsysCmd == Vfs::EXTRACT) {
		sTitle = tr("Extracting");
		sLabel = tr("Extracted:");
	}
	else
	if (eSubsysCmd == Vfs::SET_ATTRIBUTES_CMD) {
		sTitle = tr("Setting the attributes");
		sLabel = tr("Set:");
	}

	m_pProcessedLabel->setText(sLabel);
	setWindowTitle(sTitle+" "+tr("files")+" - QtCommander");
}


void MultiProgressDialog::slotUpdateFileStatus( FileInfo *pInFileInfo, const QString &sStatus )
{
	if (pInFileInfo != NULL)
		m_multiProgressMap[pInFileInfo]->status = sStatus;
}

void MultiProgressDialog::slotUpdateFileProgress( FileInfo *pInFileInfo, long long nCurrentValue, long long nMaxValue )
{
	if (pInFileInfo == NULL)
		return;

	int nPercentValue = (100 * nCurrentValue) / nMaxValue;
	m_multiProgressMap[pInFileInfo]->progress = nPercentValue;
}

/*
void MultiProgressDialog::slotUpdateTotalProgress( int nDoneInPercents, long long int nTotalWeight )
{
	m_pTotalProgressBar->setValue(nDoneInPercents);
}
*/
void MultiProgressDialog::slotUpdateCounterOfProcessed( int nValue, bool bTotal )
{
	QString sValue;
	if (bTotal) {
		m_nTotalNum = nValue;
		sValue = QString("%1 / %2").arg(0).arg(m_nTotalNum);
	}
	else
		sValue = QString("%1 / %2").arg(nValue).arg(m_nTotalNum);

	if (m_nTotalNum == 0 || m_nTotalNum < 0) {
		qDebug() << "MultiProgressDialog::slotUpdateCounterOfProcessed. Internal ERROR. Parameter nValue should be different that 0.";
		return;
	}
	m_pProcessedLabValue->setText(sValue);
	int nDoneInPercents = (100 * nValue) / m_nTotalNum;
	m_pTotalProgressBar->setValue(nDoneInPercents);
}

void MultiProgressDialog::slotUpdateLabelOfCancelBtnWithDone()
{
	m_pCancelBtn->setText(tr("&Done"));
	m_pCancelBtn->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogOkButton));
	m_pCancelBtn->setFocus();
}


void MultiProgressDialog::closeEvent( QCloseEvent *pCloseEvent )
{
	emit signalCancel();
	pCloseEvent->ignore();
}

