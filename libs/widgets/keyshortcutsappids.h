/***************************************************************************
 *   Copyright (C) 2019 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _KEYSHORTCUTSIDS_H
#define _KEYSHORTCUTSIDS_H

#define NoKeyCode_APP             9999
#define Help_APP                  1001
#define QuickRename_APP           1002
#define ViewFile_APP              1003
#define EditFile_APP              1004
#define CopyFiles_APP             1005
#define MoveFiles_APP             1006
#define MakeDirectory_APP         1007
#define DeleteFiles_APP           1008
#define MakeAnEmptyFile_APP       1009
#define CopyFilesToCurrentDirectory_APP   1010
#define SelectUnselectItem_APP    1011
#define QuickChangeOfFileModifiedDate_APP 1012
#define QuickChangeOfFilePermission_APP   1013
#define QuickChangeOfFileOwner_APP        1014
#define QuickChangeOfFileGroup_APP        1015
#define GoToUpLevel_APP           1016
#define RefreshDirectory_APP      1017
#define MakeALink_APP             1018
#define EditCurrentLink_APP       1019
#define InvertSelection_APP       1020 // readonly
#define SelectFilesWithTheSameExtention_APP   1021
#define DeselectFilesWithTheSameExtention_APP 1022
#define SelectAllItems_APP        1023
#define DeselectAllFiles_APP      1024
#define SelectGroupOfFiles_APP    1025
#define DeselectGroupOfFiles_APP  1026
#define ChangeViewToList_APP      1027
#define ChangeViewToTree_APP      1028
#define QuickItemSearch_APP       1029
#define ShowHideEmbdTerminal_APP  1030
#define NewTabInCurrentPanel_APP  1031
#define ShowHistory_APP           1032
#define ShowHideCmdLineBar_APP    1033
#define GoToPreviousURL_APP       1034
#define GoToNextURL_APP           1035
#define GoToUserHomeDirectory_APP 1036
#define MoveCursorToPathView_APP  1037
#define ShowConnectionManager_APP 1038
#define QuickItemFiltering_APP    1039
#define ShowStoragesList_APP      1040
#define ShowFreeSpace_APP         1041
#define ShowBookmarks_APP         1042
#define FindFile_APP              1043
#define QuickGoOutFromCurrentSubSystem_APP    1044
#define OpenCurrentFileOrDirInLeftPanel_APP   1045
#define OpenCurrentFileOrDirInRightPanel_APP  1046
#define WeighCurrentDirectory_APP 1047
#define ConfigureApplication_APP  1048
#define ConfigurePreview_APP      1049
#define ShowHideMenuBar_APP       1050
#define ShowHideToolBar_APP       1051
#define ShowHideButtonBar_APP     1052
#define ShowProperties_APP        1053
#define QuitApplication_APP       1054
#define CloseCurrentTab_APP       1055
#define DuplicateCurrentTab_APP   1056
#define GoToRootDirectory_APP     1057
#define ShowContextMenu_APP       1058
#define CreateNewArchive_APP      1059
#define MoveFilesToArchive_APP    1060
#define ShowHideStatusBar_APP     1061
#define OneDirBackward_APP        1062
#define OneDirForward_APP         1063

// alternatives
// #define MoveCursorToPathView2_APP 1058
// #define GoToPreviousURL2_APP      1059
// #define GoToNextURL2_APP          1060
// #define SelectAllFiles2_APP       1061
// #define ShowBookmarks2_APP        1062
// #define SelectFilesWithTheSameExtention2_APP   1063
// #define DeselectFilesWithTheSameExtention2_APP 1064
// #define SelectAllFiles2_APP       1065
// #define DeselectAllFiles2_APP     1066
// #define EditCurrentLink2_APP      1067

#endif // _KEYSHORTCUTSIDS_H
