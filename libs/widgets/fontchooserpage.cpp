/***************************************************************************
 *   Copyright (C) 2007 by Piotr Mierzwi?ski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QtGui>

#include "settings.h"
#include "fontchooserpage.h"


FontChooserPage::FontChooserPage( FontChooserPage::ConfType confType )
{
	setupUi(this);
	m_eConfType = confType;

	connect(m_pBoldChkBox, &QCheckBox::clicked, this, &FontChooserPage::slotSetFontAttribut);
	connect(m_pItalicChkBox, &QCheckBox::clicked, this, &FontChooserPage::slotSetFontAttribut);
	connect(m_pOriginalFontBtn, &QPushButton::clicked, this, &FontChooserPage::slotSetOriginalFont);

	connect<void(QListWidget::*)(int)>(m_pFontSizeListWidget, &QListWidget::currentRowChanged, this, &FontChooserPage::slotFontSizeHighlighted);
	connect(m_pFontListWidget, &QListWidget::itemSelectionChanged, this, &FontChooserPage::slotFamilySelectionChanged);

	init();
}


void FontChooserPage::init()
{
	QFont defaultFont = QApplication::font();
	m_sDefaultFont = defaultFont.family()+","+QString("%1").arg(defaultFont.pointSize())+","+defaultFont.styleName();

	QString sKey = (m_eConfType == FILE_LIST_VIEW) ? "filelistview" : "textViewer";
	Settings settings;
	m_sOriginalFont = settings.value(sKey+"/Font", "").toString(); // Noto Sans Adlam
	if (m_sOriginalFont.isEmpty()) // use default in system
		m_sOriginalFont = m_sDefaultFont; // DejaVu Sans

// 	qDebug() << "FontChooserPage::init. defaultFont:" << m_sDefaultFont;
// 	qDebug() << "FontChooserPage::init. originalFont:" << m_sOriginalFont;

	m_pOriginalFontBtn->setToolTip(m_pOriginalFontBtn->toolTip()+" "+m_sOriginalFont);

	updateFontFamilyList();
	slotSetOriginalFont();
}


void FontChooserPage::updateFontFamilyList()
{
// --- get font sFamily list
	m_slFamilyName = m_fdFontDataBase.families();

	QString sFont;
	QStringList slNewFontList;
	QStringList::Iterator it = m_slFamilyName.begin();

	for ( ; it != m_slFamilyName.end() ; it++) {
		sFont = *it;
		if (sFont.contains('-')) {
			int i = sFont.indexOf('-');
			sFont = sFont.right( sFont.length() - i - 1 ) + " [" + sFont.left( i ) + "]";
		}
		sFont[0] = sFont[0].toUpper(); // make Capitalic
#if 0
		if (m_fdFontDataBase.isSmoothlyScalable(*it))
			slNewFontList.append(sFont + "(TT)");
		else if (m_fdFontDataBase.isBitmapScalable(*it))
			slNewFontList.append(sFont + "(BT)");
		else
#endif
		slNewFontList.append(sFont);
	}

	m_pFontListWidget->addItems(slNewFontList);
	//m_pFontListWidget->insertStringList(slNewFontList);
}


void FontChooserPage::updateSizeList()
{
// 	QString sName = "FontChooserPage::updateSizeList.";
	m_pFontSizeListWidget->clear();
	QList<int> nlSize = m_fdFontDataBase.pointSizes(m_pFontListWidget->currentItem()->text()); // no style (default "Black") helps fetch size list

	if (nlSize.isEmpty())  {
		qDebug() << "FontChooserPage::updateSizeList.Internal error!";
		qDebug() << "    No pointsizes for family \"" << m_pFontListWidget->currentItem()->text()
				 << "with script:" << m_sCharSet << "and style:" << m_sStyle;
//		m_pFontListWidget->currentItem()->text().toLatin1().data(), m_sCharSet.toLatin1().data(), m_sStyle.toLatin1().data());
// 		m_pFontListWidget->removeItemWidget(m_pFontListWidget->currentItem());
		m_pFontListWidget->takeItem(m_pFontListWidget->row(m_pFontListWidget->currentItem())); // FIXME. Crashed here after selected font Work Sans Semibold
		qDebug() << "FontChooserPage::updateSizeList.Item removed from list.";
		return;
	}

	for (int i=0; i<nlSize.count(); i++) {
		m_pFontSizeListWidget->addItem(QString("%1").arg(nlSize[i]));
	}
}



void FontChooserPage::slotFamilySelectionChanged()
{
// 	qDebug() << "FontChooserPage::slotFamilySelectionChanged.";
	QString sFamily = m_slFamilyName[m_pFontListWidget->currentRow()];

//	slCharSetName = m_fdFontDataBase.charSets( sFamily ); // Qt-ver. < 3.x
	QStringList slCharSetName = m_fdFontDataBase.families();

	if (slCharSetName.isEmpty()) {
		qDebug() << "FontChooserPage::slotFamilySelectionChanged. Internal error, no character sets for family \"" << sFamily << "\"";
		return;
	}
	if (! slCharSetName.isEmpty())
		m_sCharSet = slCharSetName[0];

	QStringList slStyle = m_fdFontDataBase.styles(sFamily);
	m_sStyle = slStyle[0]; // select always style "normal"

	int nFontSizeId = m_pFontSizeListWidget->currentRow();
	if (nFontSizeId < 0)
		nFontSizeId = 0;
// 	int nCurrentFontSize = (m_pFontSizeListWidget->count() > 0) ? m_pFontSizeListWidget->item(nCurrentSizePoint)->text().toInt() : 0;
	int nFontSize = 3;
	if (m_pFontSizeListWidget->count() > 0) {
		QListWidgetItem *item = m_pFontSizeListWidget->item(nFontSizeId);
		if (item != nullptr) {
			nFontSize = item->text().toInt();
		}
	}
	updateSizeList();

	// -- check if current familly has on own list size previous font familly too
	QList<int> nlSize = m_fdFontDataBase.pointSizes(sFamily, m_sStyle);
	for (int nSizePoint=0; nSizePoint < nlSize.count(); nSizePoint++) {
		if (nlSize[nSizePoint] == nFontSize)  {
			nFontSize = nlSize[nSizePoint];
			nFontSizeId = nSizePoint;
			// nCurrentSizeId and nCurrentFontSize seems to be correct, but do another check
			break;
		}
	}
	// -- check if original font size is prezent in new list of sizes
	QString sCurrentSize = QString("%1").arg(nFontSize);
	nFontSizeId = 0; // finally will be 0 if not found proper size, so font will be set on the smallest
	for (int nId=0; nId < m_pFontSizeListWidget->count(); nId++) {
		if (m_pFontSizeListWidget->item(nId)->text()  == sCurrentSize) {
			nFontSizeId = nId; // seems Id always points to proper size
			break;
		}
	}
	slotFontSizeHighlighted(nFontSizeId);
	m_pFontSizeListWidget->setCurrentRow(nFontSizeId);
}


void FontChooserPage::slotFontSizeHighlighted( int nId )
{
	//qDebug() << "FontChooserPage::slotFontSizeHighlighted. nId= " << nId;
	if (nId < 0 || nId > (m_pFontSizeListWidget->count()-1))
		return;
	if (m_pFontListWidget->currentItem() == nullptr)
		return;

	int nFontSize = m_pFontSizeListWidget->item(nId)->text().toInt();
	QString sCurrentFontFamily = m_pFontListWidget->currentItem()->text();

	int nFontWeight = (m_pBoldChkBox->isChecked()) ? QFont::Bold : QFont::Normal;
	m_pFontTestLineEdit->setFont(QFont(sCurrentFontFamily, nFontSize, nFontWeight, m_pItalicChkBox->isChecked()));
}

void FontChooserPage::setDefaults()
{
	m_pBoldChkBox->setChecked(false);
	m_pItalicChkBox->setChecked(false);

	QFont defaultFont = QApplication::font();
	QList< QListWidgetItem* > list = m_pFontListWidget->findItems(defaultFont.family(), Qt::MatchContains);
	if (list.isEmpty()) {
		qDebug() << "FontChooserPage::slotSetDefaultFont. Not found font " << defaultFont.family() << "on list!";
		QString sFamily = defaultFont.family();
		sFamily = sFamily.left(sFamily.length()-sFamily.lastIndexOf(" ")-1);
		qDebug() << "FontChooserPage::slotSetDefaultFont. Try to find with:" << sFamily;
		return;
	}
	QListWidgetItem *pFamilyItem = list.first();
//	QListWidgetItem *pFamilyItem = m_pFontListWidget->findItems(originalFont.family(), Qt::MatchExactly).first();
	m_pFontListWidget->setCurrentItem(pFamilyItem);
	m_pFontListWidget->scrollToItem(pFamilyItem, QAbstractItemView::PositionAtCenter); // vertical policy must be 'prefered'

	QListWidgetItem *pSizeItem = m_pFontSizeListWidget->findItems(QString("%1").arg(defaultFont.pointSize()), Qt::MatchExactly).first();
	m_pFontSizeListWidget->setCurrentItem(pSizeItem);
	m_pFontSizeListWidget->scrollToItem(pSizeItem, QAbstractItemView::PositionAtCenter); // vertical policy must be 'prefered'

	setPreviewFont(defaultFont.family(), defaultFont.pointSize()); // default font
}


void FontChooserPage::slotSetOriginalFont()
{
// 	qDebug() << "FontChooserPage::slotSetOriginalFont.";
	// parse 'm_sOriginalFont'
	QString sFontStyle = m_sOriginalFont.section(',', 2,2).simplified().toLower();
	QString sFontSize  = m_sOriginalFont.section(',', 1,1).simplified();
	bool bOk;
	uint nFontSize = sFontSize.toInt(&bOk);
	if (! bOk)
		nFontSize = 10; // default size

	int nFontWeight = (m_pBoldChkBox->isChecked()) ? QFont::Bold : QFont::Normal;
	QFont originalFont(m_sOriginalFont.section(',', 0,0), nFontSize, nFontWeight, m_pItalicChkBox->isChecked());
	QList< QListWidgetItem* > list = m_pFontListWidget->findItems(originalFont.family(), Qt::MatchExactly);
	if (list.isEmpty()) {
		qDebug() << "FontChooserPage::slotSetOriginalFont. not found font " << originalFont.family() << "on list!";
		return;
	}
	QListWidgetItem *pFamilyItem = list.first();
//	QListWidgetItem *pFamilyItem = m_pFontListWidget->findItems(originalFont.family(), Qt::MatchExactly).first();
	m_pFontListWidget->setCurrentItem(pFamilyItem);
	m_pFontListWidget->scrollToItem(pFamilyItem, QAbstractItemView::PositionAtCenter); // vertical policy must be 'prefered'

	QListWidgetItem *pSizeItem = m_pFontSizeListWidget->findItems(QString("%1").arg(originalFont.pointSize()), Qt::MatchExactly).first();
	m_pFontSizeListWidget->setCurrentItem(pSizeItem);
	m_pFontSizeListWidget->scrollToItem(pSizeItem, QAbstractItemView::PositionAtCenter); // vertical policy must be 'prefered'

//	m_pBoldChkBox->setChecked((sFontStyle == "light")); // regular
	m_pBoldChkBox->setChecked((sFontStyle == "bold"));
	m_pItalicChkBox->setChecked((sFontStyle == "italic"));
	setPreviewFont(m_sOriginalFont.section(',', 0,0), nFontSize);
}


void FontChooserPage::setPreviewFont( const QString & sFamily, int nSize )
{
	int nIdFontFamily = -1;
	// if passed font family not found in loaded list then use first from list
	QList<QListWidgetItem *> fontList = m_pFontListWidget->findItems(sFamily, Qt::MatchExactly);
	if (fontList.count() > 0)
		nIdFontFamily = m_pFontListWidget->row(fontList.at(0));

	QString sDefaultFontFamily = m_sDefaultFont.section(',', 0,0);

	if (nIdFontFamily < 0) {
		if (sFamily != sDefaultFontFamily) { // passed font family different than default font family
			qDebug() << "FontChooserPage::setPreviewFont. font family:" << sFamily << "not found!";
			setDefaults();
			return;
		}
		else // not found passed font family, get first avilable
			nIdFontFamily = 0;
	}
	m_pFontListWidget->setCurrentRow(nIdFontFamily);

	int nIdFontSize = -1;
	int nSizeToFind = nSize;
	while (nIdFontSize == -1) {
		for (int i=0; i<m_pFontSizeListWidget->count(); i++) {
			if (m_pFontSizeListWidget->item(i)->text().toInt() == nSizeToFind) {
				nIdFontSize = i;
				break; // 'for' loop
			}
		}
		if (nIdFontSize == -1)
			qDebug() << "FontChooserPage::setPreviewFont. font size:" << nSizeToFind << "not found!";
		nSizeToFind++; // one didn't match passed font size
		if (nSizeToFind > 100)
			break;
	}
	m_pFontSizeListWidget->setCurrentRow(nIdFontSize);
	slotSetFontAttribut();
}

void FontChooserPage::slotSetFontAttribut()
{
	slotFontSizeHighlighted(m_pFontSizeListWidget->currentRow());
}

void FontChooserPage::slotSave()
{
	QString sFont = fontFamily() + "," + QString::number(fontSize());

	if (m_pBoldChkBox->isChecked())
		sFont += ",Bold";
	if (m_pItalicChkBox->isChecked())
		sFont += ",Italic";

	QString sKey = (m_eConfType == FILE_LIST_VIEW) ? "filelistview" : "textViewer";
	Settings settings;
	settings.setValue(sKey+"/Font", sFont);
}

