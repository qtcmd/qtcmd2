/***************************************************************************
 *   Copyright (C) 2005 by Piotr Mierzwiñski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
// #include <QDebug>

#include "mediacontrolwidget.h"

/*
 *  Constructs a MediaControlWidget as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  true to construct a modal dialog.
 */
MediaControlWidget::MediaControlWidget( QWidget *pParent )
{
    setupUi(this);
	setParent(pParent);

	m_pIconCache = NULL;
	m_pAudioOutput = NULL;

	connect(m_pPlayBtn, &QPushButton::pressed, this, &MediaControlWidget::slotPlay);
	connect(m_pPauseBtn, &QPushButton::pressed, this, &MediaControlWidget::slotPause);
	connect(m_pStopBtn,   &QPushButton::pressed, this, &MediaControlWidget::slotStop);
	connect(m_pForwardBtn, &QPushButton::pressed, this, &MediaControlWidget::slotForward);
	connect(m_pBackwardBtn, &QPushButton::pressed, this, &MediaControlWidget::slotBackward);
}

void MediaControlWidget::init()
{
	m_pIconCache = IconCache::instance();

	m_pPlayBtn->setText("");
	m_pPauseBtn->setText("");
	m_pStopBtn->setText("");
	m_pForwardBtn->setText("");
	m_pBackwardBtn->setText("");

	m_pPlayBtn->setIcon(icon("media/playback-start"));
	m_pPauseBtn->setIcon(icon("media/playback-pause"));
	m_pStopBtn->setIcon(icon("media/playback-stop"));
	m_pForwardBtn->setIcon(icon("media/seek-forward"));
	m_pBackwardBtn->setIcon(icon("media/seek-backward"));

	m_pPlayBtn->setShortcut(Qt::Key_P);
	m_pPauseBtn->setShortcut(Qt::Key_Space);
	m_pStopBtn->setShortcut(Qt::Key_S);
	m_pForwardBtn->setShortcut(Qt::Key_Right);
	m_pBackwardBtn->setShortcut(Qt::Key_Left);

	if (m_pAudioOutput != NULL)
		connect(m_pSpinBoxVolume, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), this, &MediaControlWidget::slotVolumeChanged);
}

void MediaControlWidget::setVolumeSlider( Phonon::AudioOutput *pAudioOutput )
{
	m_pVolumeSlider->setAudioOutput(pAudioOutput);
	//m_pSpinBoxVolume->setValue(pAudioOutput->volume()*100);
}

void MediaControlWidget::updateButtonsState( bool bPlayEnable, bool bPauseEnable, bool bStopEnable, bool bSeekForward, bool bSeekBackward )
{
	m_pPlayBtn->setEnabled(bPlayEnable);
	m_pPauseBtn->setEnabled(bPauseEnable);
	m_pStopBtn->setEnabled(bStopEnable);
	m_pForwardBtn->setEnabled(bSeekForward);
	m_pBackwardBtn->setEnabled(bSeekBackward);
}

void MediaControlWidget::slotVolumeChanged( int nInValue )
{
	qreal volume    = nInValue; // for casting only
	qreal volumeVal = volume/100;
// 	qDebug() << "MediaControlWidget::slotVolumeChanged. newVolumne (in %):" << nInValue;
	if (m_pAudioOutput != NULL) {
		m_pAudioOutput->setVolume(volumeVal);
		m_pVolumeSlider->setAudioOutput(m_pAudioOutput);
	}
}

void MediaControlWidget::updateCurrentTime( const QString &sTime )
{
	QString sCurrentTime = m_TotalCurrTimeLab->text().split('/').at(1).trimmed();
	QString sNewTime = sTime+" / "+sCurrentTime;
	m_TotalCurrTimeLab->setText(sNewTime);
}

void MediaControlWidget::setTotalTime( const QString &sTime )
{
	QString sTotalTime = m_TotalCurrTimeLab->text().split('/').at(0).trimmed();
	QString sNewTime = sTotalTime+" / "+sTime;
	m_TotalCurrTimeLab->setText(sNewTime);
}

