/***************************************************************************
 *   Copyright (C) 2005 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                 2019 improved, updated, finished                        *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _TOOLBARPAGE_H
#define _TOOLBARPAGE_H

#include <QActionGroup>

#include "iconcache.h"
#include "toolbarpagesettings.h"

#include "ui_toolbarpagedlg.h"

/**
	@author Piotr Mierzwiński
*/
class ToolBarPage : public QWidget, Ui::ToolBarPageDlg
{
	Q_OBJECT
public:
	ToolBarPage( QWidget *pParent=nullptr );

	/** Initializes this page.
	 * Adds actions from given list to InputActionsListWidgets.
	 * Inserts actions into toolbar map using configuration from file.
	 * Updates listBox widgets with actions
	 * @param pToolbarActionsLst list of action for given toolbar
	 * @param sActionsOrigin originating of actions, so: mainwindow or view
	 */
	void init( QList< QActionGroup * > *pToolbarActionsLst, const QString &sActionsOrigin );

	/** Returns list of names of actions for give toolbar name.
	 */
	QStringList actions( const QString &sActionGroupName ) {
		return m_toolBarsMap.values(sActionGroupName);
	}

	/**Set defauls settings, so tool bar settings.
	 */
	void setDefaults();

private:
	IconCache *m_pIconCache;
	QList <QActionGroup *> *m_pToolbarActionsLst;
	ToolBarPageSettings m_toolBarBase, m_toolBarTools, m_toolBarNavi;
	QString m_sActionsOrigin;

	/** Holds all all actions linked with toolbars.
	 *  Used to save configuration.
	 */
	//QHash <QString, QString> m_toolBarsMap;
	QMultiHash <QString, QString> m_toolBarsMap;


	/** Returns icon for gicen path, where might contains 2 parts like: "document/open-folder", whereas source file name calls: document-open-folder.png.
	 *  Second case is 1 part, like "settings", whereas source file name calls settings.ong.
	 * @param sIconName path to icon described above
	 * @return icon object
	 */
	QIcon icon( const QString &sIconName ) const {
		return m_pIconCache->icon(sIconName);
	}

	enum Direct { UP=0, DOWN };

	void moveCurrentItem( Direct eDirect );
	void updateControlButtons();
	void updateToolBarsMap();
	void saveToolbar( const QString &sOrigin, const QString &sKey, const QString &sName );

public Q_SLOTS:
	void slotSave();

private Q_SLOTS:
	void slotUp() {
		moveCurrentItem(UP);
	}
	void slotDown() {
		moveCurrentItem(DOWN);
	}
	void slotDelete();
	void slotInsert();
	void slotChangeCurrentBar( int nCurrentBarId );
	void slotItemSelectionChanged();

};

#endif  // _TOOLBARPAGE_H
