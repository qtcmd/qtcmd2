/***************************************************************************
 *   Copyright (C) 2008 by Piotr Mierzwi?ski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
//#include <QtGui> // for qDebug

#include "shortkeyeditboxdata.h"

const int PaintingScaleFactor = 20;


ShortKeyEditBoxData::ShortKeyEditBoxData( const QString &sShortKey, Qt::AlignmentFlag eAlign )
	: m_sShortKey(sShortKey), m_eAlign(eAlign)
	, m_TxtColor(QColor("black")), m_bTxtBold(false), m_bTxtItalic(false)
{
}


void ShortKeyEditBoxData::paint( QPainter *pPainter, const QRect &rect, const QPalette &palette ) const
{
	pPainter->save();
	pPainter->setRenderHint(QPainter::Antialiasing, true);

	int yOffset = (rect.height() - PaintingScaleFactor) / 2;
	pPainter->translate(rect.x(), rect.y() + yOffset);

	QString sTxt = m_sShortKey;
	//pPainter->setPen(m_TxtColor);

	int fontHeight = pPainter->fontMetrics().height();
	int yPos = (rect.height() - fontHeight)/2 + 1;
	int xPos = 0; // Qt::AlignLeft and others except below is checked
	//qDebug() << "ComboBoxData::paint, m_eAlign=" << m_eAlign;
	if (m_eAlign == Qt::AlignHCenter || m_eAlign == (Qt::AlignHCenter|Qt::AlignVCenter)) {
		xPos = (rect.width() - pPainter->fontMetrics().horizontalAdvance(sTxt))/2 + 1;
	}
	else
	if (m_eAlign == Qt::AlignRight) {
		// TODO implement me  (m_eAlign == Qt::AlignRight)
	}
	//qDebug() << "height="<< rect.height() << ", yOffset=" << yOffset << ", fontHeight=" << fontHeight;
	sTxt = pPainter->fontMetrics().elidedText( sTxt, Qt::ElideRight, rect.width() ); // option.textElideMode
	pPainter->drawText(xPos, yPos+yOffset, sTxt );
	//qDebug() << "ShortKeyEditBoxData::paint, pen_color=" << pPainter->pen()->color().name() << ", xpos= " << xPos << ", yPos= " << yPos+yOffset << ", txt= " << sTxt;

	pPainter->restore();
}


void ShortKeyEditBoxData::setTextAlignment( Qt::AlignmentFlag eAlign )
{
	m_eAlign = eAlign;
	//qDebug() << "setTextAlignment, m_eAlign=" << m_eAlign;
}

