/***************************************************************************
 *   Copyright (C) 2008 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _SHORTKEYEDITBOXDELEGATE_H
#define _SHORTKEYEDITBOXDELEGATE_H

#include <QItemDelegate>
#include <QTableWidgetItem>

/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class ShortKeyEditBoxDelegate : public QItemDelegate
{
	Q_OBJECT
public:
	ShortKeyEditBoxDelegate( QObject *pParent=nullptr );

	QWidget *createEditor( QWidget *pParent, const QStyleOptionViewItem &option, const QModelIndex &index ) const;

	void setEditorData( QWidget *pEditor, const QModelIndex &index ) const;
	void setModelData( QWidget *pEditor, QAbstractItemModel *pModel, const QModelIndex &index ) const;

	void updateEditorGeometry( QWidget *pEditor, const QStyleOptionViewItem &option, const QModelIndex & ) const;

	void paint( QPainter *pPainter, const QStyleOptionViewItem &option, const QModelIndex &index ) const;

private Q_SLOTS:
	void slotCommitAndCloseEditor();

};

#endif // _SHORTKEYEDITBOXDELEGATE_H
