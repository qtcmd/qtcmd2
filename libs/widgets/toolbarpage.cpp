/***************************************************************************
 *   Copyright (C) 2005 by Piotr Mierzwi?ski <piom@nes.pl>                 *
 *                 2019 improved, updated, finished                        *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QImage>
#include <QDebug>
#include <QListWidget>

#include "settings.h"
#include "messagebox.h"
#include "toolbarpage.h"


ToolBarPage::ToolBarPage( QWidget */*pParent*/ )
{
	setupUi(this);
	m_pIconCache = nullptr;
	m_pToolbarActionsLst = nullptr;

	connect<void(QComboBox::*)(int)>(m_pBarsComboBox, &QComboBox::activated, this, &ToolBarPage::slotChangeCurrentBar);

	connect(m_pAddActPushBtn, &QPushButton::clicked, this, &ToolBarPage::slotInsert);
	connect(m_pUpPushBtn,     &QPushButton::clicked, this, &ToolBarPage::slotUp);
	connect(m_pDownPushBtn,   &QPushButton::clicked, this, &ToolBarPage::slotDown);
	connect(m_pDeletePushBtn, &QPushButton::clicked, this, &ToolBarPage::slotDelete);

	connect(m_pInputActionsListWidgets, &QListWidget::itemSelectionChanged, this, &ToolBarPage::slotItemSelectionChanged);
}


void ToolBarPage::init( QList <QActionGroup *> *pToolbarActionsLst, const QString &sActionsOrigin )
{
	qDebug() << "ToolBarPage::init. sActionsOrigin:" << sActionsOrigin;
	m_pIconCache = IconCache::instance();
	m_pToolbarActionsLst = pToolbarActionsLst;
	m_sActionsOrigin = sActionsOrigin;
	m_pInputActionsListWidgets->setFocus();

	QStringList slBarsList;
	QString sAction;

	// -- update actions list with passed actions
	m_pInputActionsListWidgets->clear();
	for (int toolBarId=0; toolBarId < pToolbarActionsLst->size(); ++toolBarId) {
		slBarsList << pToolbarActionsLst->at(toolBarId)->objectName();
		// -- feed input list with all available actions
		QList<QAction *> actionsLst = pToolbarActionsLst->at(toolBarId)->actions(); // take all actions for given item
		for (int i = 0; i < actionsLst.size(); ++i) {
			QAction *pAction = actionsLst.at(i);
			sAction = pAction->text();	sAction.replace("&", "");
			QListWidgetItem *pItem = new QListWidgetItem(pAction->icon(), sAction, m_pInputActionsListWidgets);
			//pItem->setStatusTip(pToolbarActionsLst->at(toolBarId)->objectName());
			pItem->setToolTip(pToolbarActionsLst->at(toolBarId)->objectName()); // set parent toolbar name
			pItem->setStatusTip(pAction->objectName()); // set action name
			//qDebug() << "ToolBarPage::init. objectName:" << pAction->objectName() << "pItem->toolTip:" << pItem->toolTip();
			m_pInputActionsListWidgets->addItem(pItem);
		}
	}
	// -- add the names of all given actions group
	if (slBarsList.count() > 0) {
		m_pBarsComboBox->addItems(slBarsList);
		m_pBarsComboBox->setCurrentIndex(0);
	}

	// -- update toolsBar map using info from configuration file
	Settings settings;
	if (sActionsOrigin == "mainwindow") {
		QStringList slBaseTB  = settings.value("mainwindow/BaseToolBarActions",  "open_dir, config_qtcmd, config_prev").toString().remove(" ").split(",");
		QStringList slToolsTB = settings.value("mainwindow/ToolsToolBarActions", "show_hide_term, find_files, conn_manager, favorite_folders, storages_list").toString().remove(" ").split(",");
		QStringList slNaviTB  = settings.value("mainwindow/NaviToolBarActions",  "quick_get_out_subsys, dir_backward, dir_forward, go_to_root_dir, go_to_home_dir").toString().remove(" ").split(",");

		m_toolBarsMap.clear();
		for (int i=slBaseTB.count()-1; i>=0; i--) {
			m_toolBarsMap.insert("Base", slBaseTB.at(i));
		}
		for (int i=slToolsTB.count()-1; i>=0; i--) {
			m_toolBarsMap.insert("Tools", slToolsTB.at(i));
		}
		for (int i=slNaviTB.count()-1; i>=0; i--) {
			m_toolBarsMap.insert("Navigation", slNaviTB.at(i));
		}
	}
	else if (sActionsOrigin == "view") {
		QStringList slBaseTB = settings.value("viewer/BaseToolBarActions", "new_doc, open_doc, save_doc, saveAs_doc, reload_doc, change_view, config_viewer, side_list").toString().remove(" ").split(",");
		QStringList slViewTB = settings.value("viewer/ViewToolBarActions", "print_doc, zoomin_doc, zoomout_doc").toString().remove(" ").split(",");
		QStringList slEditTB = settings.value("viewer/EditToolBarActions", "selectall_doc, find_doc, findnext_doc, undo_doc, redo_doc, cut_doc, copy_doc, paste_doc, replace_doc, goto_doc").toString().remove(" ").split(",");

		for (int i=0; i<slBaseTB.count(); i++) {
			m_toolBarsMap.insert(tr("Base"), slBaseTB.at(i));
		}
		for (int i=0; i<slViewTB.count(); i++) {
			m_toolBarsMap.insert(tr("View"), slViewTB.at(i));
		}
		for (int i=0; i<slEditTB.count(); i++) {
			m_toolBarsMap.insert(tr("Edit"), slEditTB.at(i));
		}
	}

	slotChangeCurrentBar(0);
	m_pInputActionsListWidgets->setCurrentRow(0);

	// -- update buttons with icons
	m_pAddActPushBtn->setText("");
	m_pAddActPushBtn->setIcon(icon("list/add"));

	m_pUpPushBtn->setText("");
	m_pUpPushBtn->setIcon(icon("arrow/up"));
	m_pDownPushBtn->setText("");
	m_pDownPushBtn->setIcon(icon("arrow/down"));
	m_pDeletePushBtn->setText("");
	m_pDeletePushBtn->setIcon(icon("edit/delete"));
	m_pDeletePushBtn->setShortcut(QKeySequence(Qt::Key_Delete));
}

void ToolBarPage::setDefaults()
{
	if (m_sActionsOrigin == "mainwindow") {
		QStringList slBaseTB  = QString("open_dir, config_qtcmd, config_prev").remove(" ").split(",");
		QStringList slToolsTB = QString("show_hide_term, find_files, conn_manager, favorite_folders, storages_list").remove(" ").split(",");
		QStringList slNaviTB  = QString("quick_get_out_subsys, dir_backward, dir_forward, go_to_root_dir, go_to_home_dir").remove(" ").split(",");

		m_toolBarsMap.clear();
		for (int i=slBaseTB.count()-1; i>=0; i--) {
			m_toolBarsMap.insert("Base", slBaseTB.at(i));
		}
		for (int i=slToolsTB.count()-1; i>=0; i--) {
			m_toolBarsMap.insert("Tools", slToolsTB.at(i));
		}
		for (int i=slNaviTB.count()-1; i>=0; i--) {
			m_toolBarsMap.insert("Navigation", slNaviTB.at(i));
		}
	}
	else if (m_sActionsOrigin == "view") {
		QStringList slBaseTB = QString("new_doc, open_doc, save_doc, saveAs_doc, reload_doc, change_view, config_viewer, side_list").remove(" ").split(",");
		QStringList slViewTB = QString("print_doc, zoomin_doc, zoomout_doc").remove(" ").split(",");
		QStringList slEditTB = QString("selectall_doc, find_doc, findnext_doc, undo_doc, redo_doc, cut_doc, copy_doc, paste_doc, replace_doc, goto_doc").remove(" ").split(",");

		for (int i=0; i<slBaseTB.count(); i++) {
			m_toolBarsMap.insert(tr("Base"), slBaseTB.at(i));
		}
		for (int i=0; i<slViewTB.count(); i++) {
			m_toolBarsMap.insert(tr("View"), slViewTB.at(i));
		}
		for (int i=0; i<slEditTB.count(); i++) {
			m_toolBarsMap.insert(tr("Edit"), slEditTB.at(i));
		}
	}

	slotChangeCurrentBar(0);
	m_pInputActionsListWidgets->setCurrentRow(0);
}


void ToolBarPage::slotInsert()
{
	if (! m_pInputActionsListWidgets->hasFocus())
		m_pInputActionsListWidgets->setFocus();

	QListWidgetItem *pItem = m_pInputActionsListWidgets->currentItem();
	QString sInpBarName = pItem->toolTip();
	QString sOutBarName = m_pBarsComboBox->currentText();
	if (sInpBarName != sOutBarName) {
		MessageBox::msg(this, MessageBox::WARNING, tr("One cannot add item linked to different toolbar than selected in target view!"), tr("Item comes from toolbar")+": "+sInpBarName);
		return;
	}

	if (m_pOutputActionsListWidget->findItems(m_pInputActionsListWidgets->currentItem()->text(), Qt::MatchExactly).count() == 0) {
		m_pOutputActionsListWidget->addItem(new QListWidgetItem(*m_pInputActionsListWidgets->currentItem()));
		updateControlButtons();
	}
	else {
		MessageBox::msg(this, MessageBox::WARNING, tr("Selected item is already present in target view!"));
		return;
	}
	updateToolBarsMap();
}

void ToolBarPage::slotDelete()
{
	if (m_pOutputActionsListWidget->count() == 0)
		return;

	if (! m_pOutputActionsListWidget->hasFocus())
		m_pOutputActionsListWidget->setFocus();

	m_pOutputActionsListWidget->takeItem(m_pOutputActionsListWidget->currentRow());
	updateControlButtons();

	// -- set item after deleted as current item or current if no previous
	QListWidgetItem *pPrevItem = m_pOutputActionsListWidget->item(m_pOutputActionsListWidget->currentRow()-1);
	QListWidgetItem *pNextItem = m_pOutputActionsListWidget->item(m_pOutputActionsListWidget->currentRow()+1);
	if (pPrevItem != nullptr) {
		if (pNextItem != nullptr)
			m_pOutputActionsListWidget->setCurrentItem(pNextItem);
	}
    updateToolBarsMap();
}

void ToolBarPage::moveCurrentItem( Direct eDirect )
{
	if (m_pOutputActionsListWidget->count() < 2)
		return;

	if (! m_pOutputActionsListWidget->hasFocus())
		m_pOutputActionsListWidget->setFocus();

	QListWidgetItem *pReplacedItem = nullptr;
	QListWidgetItem *pCurrentItem  = m_pOutputActionsListWidget->currentItem();
	if (pCurrentItem == nullptr)
		return;

	int nItemHeight   = m_pOutputActionsListWidget->visualItemRect(pCurrentItem).height();
	int nCurrItemYpos = m_pOutputActionsListWidget->visualItemRect(pCurrentItem).y()+6;

	if (eDirect == UP)
		pReplacedItem = m_pOutputActionsListWidget->itemAt(QPoint(5,nCurrItemYpos-nItemHeight));
	else
	if (eDirect == DOWN)
		pReplacedItem = m_pOutputActionsListWidget->itemAt(QPoint(5,nCurrItemYpos+nItemHeight));

	if (pReplacedItem == nullptr || pReplacedItem == pCurrentItem)
		return;

	m_pOutputActionsListWidget->insertItem(m_pOutputActionsListWidget->currentRow()+((eDirect == UP) ? -1 : 2),
											(pReplacedItem=new QListWidgetItem(*pCurrentItem)) );
	m_pOutputActionsListWidget->takeItem(m_pOutputActionsListWidget->row(pCurrentItem));
	m_pOutputActionsListWidget->setCurrentItem(pReplacedItem);
	updateToolBarsMap();
}

void ToolBarPage::updateControlButtons()
{
	m_pUpPushBtn->setEnabled((m_pOutputActionsListWidget->count() > 1));
	m_pDownPushBtn->setEnabled((m_pOutputActionsListWidget->count() > 1));
	m_pDeletePushBtn->setEnabled((m_pOutputActionsListWidget->count() > 0));
}

void ToolBarPage::slotChangeCurrentBar( int nCurrentBarId )
{
	// -- updateo output list of actions
	if (nCurrentBarId < 0 || nCurrentBarId > m_pToolbarActionsLst->size()-1) {
		qDebug() << "ToolBarPage::slotChangeCurrentBar. nCurrentBarId out of range!";
		return;
	}

	bool bAddItem;
	QString sActionId, sAction;

	m_pOutputActionsListWidget->clear();
	QList<QAction *> actionsLst = m_pToolbarActionsLst->at(nCurrentBarId)->actions(); // take all actions for given Id of item
	for (int i = 0; i < actionsLst.size(); ++i) {
		QAction *pAction = actionsLst.at(i);
		sActionId = pAction->objectName(); // key like 'print_doc'
		bAddItem = (m_toolBarsMap.values(m_pBarsComboBox->currentText()).indexOf(sActionId) >= 0);
// 		qDebug() << "ToolBarPage::slotChangeCurrentBar. objectName" << sActionId << "bAddItem:" << bAddItem << "m_pBarsComboBox->currentText:" << m_pBarsComboBox->currentText()
// 		<< m_toolBarsMap.values(m_pBarsComboBox->currentText()) << "is:" << m_toolBarsMap.values(m_pBarsComboBox->currentText()).indexOf(sActionId);
		if (bAddItem) {
			sAction = pAction->text(); sAction.replace("&", "");
			QListWidgetItem *pItem = new QListWidgetItem(pAction->icon(), sAction, m_pOutputActionsListWidget);
// 			pItem->setStatusTip(sToolBarName+":"+pAction->objectName());
			pItem->setStatusTip(sActionId);
 			m_pOutputActionsListWidget->addItem(pItem);
		}
	}
	updateControlButtons();
}

void ToolBarPage::slotItemSelectionChanged()
{
	if (m_pInputActionsListWidgets->count() > 0) {
		QListWidgetItem *pItem = m_pInputActionsListWidgets->currentItem();
		if (pItem != nullptr)
			m_ToolBarName->setText(pItem->toolTip());
	}
	else
		m_ToolBarName->setText("");
}

void ToolBarPage::slotSave()
{
	if (m_sActionsOrigin == "mainwindow") {
		saveToolbar("mainwindow", "BaseToolBarActions",  "Base");
		saveToolbar("mainwindow", "ToolsToolBarActions", "Tools");
		saveToolbar("mainwindow", "NaviToolBarActions",  "Navigation");
	}
	else
	if (m_sActionsOrigin == "view") {
		saveToolbar("viewer", "BaseToolBarActions", "Base");
		saveToolbar("viewer", "ViewToolBarActions", "View");
		saveToolbar("viewer", "EditToolBarActions", "Edit");
	}
}

void ToolBarPage::saveToolbar( const QString &sOrigin, const QString &sKey, const QString &sName )
{
	qDebug() << "ToolBarPage::slotSave" << sOrigin << sKey;
	QString sActionNamLst;

	QStringList slActionsList = m_toolBarsMap.values(sName);
	for (int i=0; i< slActionsList.count(); i++) {
		sActionNamLst += slActionsList.at(i);
		if (i < slActionsList.count()-1)
			sActionNamLst += ",";
	}
	Settings settings;
	settings.setValue(sOrigin+"/"+sKey, sActionNamLst);
}


void ToolBarPage::updateToolBarsMap()
{
	QString sOutBarName = m_pBarsComboBox->currentText();
	QStringList lst = m_toolBarsMap.values(sOutBarName);
	m_toolBarsMap.remove(sOutBarName); // clear for given key

	QList <QListWidgetItem *> list = m_pOutputActionsListWidget->findItems("*", Qt::MatchWildcard);
	if (! list.empty()) {
		for (int i=list.count()-1; i>=0; i--) {
			m_toolBarsMap.insert(sOutBarName, list.at(i)->statusTip());
			//qDebug() << "- add:" << list.at(i)->statusTip();
		}
	}
	//qDebug() << "ToolBarPage::updateToolBarsMap." << sOutBarName << m_toolBarsMap.values(sOutBarName);
}

