/***************************************************************************
 *   Copyright (C) 2005 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef PROGRESSDIALOG_H
#define PROGRESSDIALOG_H

#include "ui_progressdlg.h"

#include "subsystemconst.h"

#include <QDialog>
#include <QCloseEvent>
#include <QElapsedTimer>

class QTimer;
class QDateTime;
class QQElapsedTimer;
/**
	@author Piotr Mierzwiński
*/
class ProgressDialog : public QDialog, Ui::ProgressDlg
{
	Q_OBJECT
public:
	ProgressDialog( QWidget *pParent, bool bModal, int Id );

	void showProgress( Vfs::SubsystemCommand eSubsysCmd, bool bCloseAfterFinished, const QString &sTargetPath=QString() );

	void timerStop( bool bRemoveIt=false );
	void timerStart();

	bool isCloseAfterFinished() { return m_pCloseAfterFinishedChkBox->isChecked(); }

	void setTotalVisible( bool totalVisible ) { m_bTotalVisible = totalVisible; }
	void setAttributesChangeForInfo( const QString &sMsg ) { m_pTransferLabValue->setText(sMsg); }

	Vfs::SubsystemCommand cmdInProgress() { return m_eSubsysCmd; }

private:
	int        m_dlgdId;

	QElapsedTimer m_pTime;
	QTimer    *m_pTimer;
	QDateTime  m_pDateTime;
	QString    m_sTotalFiles, m_sTotalDirs, m_sTotalWeigh;
	QString    m_sNumFiles, m_sNumDirs, m_sWeigh;
	uint       m_nBytesTransfered;
	long long  m_nTotalWeight;
	bool       m_bTotalVisible;

	Vfs::SubsystemCommand m_eSubsysCmd;
	void setWndTitle( Vfs::SubsystemCommand eSubsysCmd );

public slots:
	void slotSetFilesName( const QString &sSourceName, const QString &sTargetName );
	void slotDataTransferProgress( long long nBytesDone, long long nBytesTotal, uint nBytesTransfered, bool bSetTotalBytes );
	void slotSetTotalProgress( int nDoneInPercents, long long nTotalWeight );
	void slotSetCounter( long long nValue, Vfs::ProgressCounter counter, bool bSetTotalValue );
	void slotSetOperation( Vfs::SubsystemCommand eSubsysCmd );
	void slotTimerStartStop( bool bStart );

	/** Updates label on Cancel button to "Done"
	 */
	void slotUpdateLabelOfCancelBtnWithDone();

private slots:
	void slotSetTime(); // and transfer
	void slotCancel() { emit signalCancel(m_dlgdId); }

protected:
	void closeEvent( QCloseEvent *pCE );

signals:
	void signalCancel( int nDlgId );
	void signalBackground();

};

#endif
