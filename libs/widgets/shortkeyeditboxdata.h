/***************************************************************************
 *   Copyright (C) 2008 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _SHORTKEYEDITBOXDATA_H
#define _SHORTKEYEDITBOXDATA_H

#include <QMetaType>
#include <QPainter>
#include <QPalette>

/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class ShortKeyEditBoxData
{
public:
	ShortKeyEditBoxData() {}
	ShortKeyEditBoxData( const QString & sShortKey, Qt::AlignmentFlag eAlign=Qt::AlignLeft );

	void paint( QPainter *pPainter, const QRect &rect, const QPalette &palette ) const;

	QString text() const { return m_sShortKey; }
	void setText( const QString & sShortKey ) { m_sShortKey = sShortKey; }

	//void setTextAlignment( Qt::AlignmentFlag eAlign ) { m_eAlign = eAlign; }
	void setTextAlignment( Qt::AlignmentFlag eAlign );
	Qt::AlignmentFlag textAlignment() const { return m_eAlign; }

	void setTextColor( const QColor &txtColor ) { m_TxtColor = txtColor; };
	void setTextBold( bool bTxtBold ) { m_bTxtBold = bTxtBold; }
	void setTextItalic( bool bTxtItalic ) { m_bTxtItalic = bTxtItalic; }

private:
	int m_nShortKey;
	QString m_sShortKey;

	Qt::AlignmentFlag m_eAlign;

	QColor m_TxtColor;
	bool m_bTxtBold;
	bool m_bTxtItalic;

};

Q_DECLARE_METATYPE(ShortKeyEditBoxData)

#endif // _SHORTKEYEDITBOXDATA_H
