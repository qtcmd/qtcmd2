/***************************************************************************
 *   Copyright (C) 2005 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QTimer>
// #include <QDebug>
#include <QStyle>

#include "fileinfo.h"
#include "progressdialog.h"


ProgressDialog::ProgressDialog( QWidget *pParent, bool bModal, int nId )
	: m_dlgdId(nId)
{
	setupUi(this);
	setModal(bModal);
	setParent(pParent);
	setWindowFlags(Qt::Dialog);

	m_bTotalVisible = true;
	m_eSubsysCmd = Vfs::NO_COMMAND;

	m_pCancelBtn->setShortcut(Qt::Key_Escape);
	m_pCancelBtn->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogCancelButton));
}


void ProgressDialog::showProgress( Vfs::SubsystemCommand eSubsysCmd, bool bCloseAfterFinished, const QString &sTargetPath )
{
	setWndTitle(eSubsysCmd);
	m_eSubsysCmd = eSubsysCmd; // used by slotSetCounter
	m_pInBrgdBtn->setVisible(false); // operation in background not yet supported

	slotSetOperation(eSubsysCmd);

	m_pTimeLabel->hide();
	m_pTimeLabValue->hide();
	if (eSubsysCmd != Vfs::SET_ATTRIBUTES_CMD) {
		m_pTransferLabel->hide();
		m_pTransferLabValue->hide();
	}
	if (eSubsysCmd != Vfs::EXTRACT) {
		m_pEstimatedTimeLabel->hide();
		m_pEstimatedTimeLabValue->hide();
	}

	// remove not useable objects
	if (eSubsysCmd == Vfs::REMOVE || eSubsysCmd == Vfs::WEIGH || eSubsysCmd == Vfs::SET_ATTRIBUTES_CMD) {
		delete m_pTrgLabel;         m_pTrgLabel        = NULL;
		delete m_pTargetSLabValue;  m_pTargetSLabValue = NULL;
		delete m_pFileProgress;     m_pFileProgress    = NULL;
		if (eSubsysCmd != Vfs::SET_ATTRIBUTES_CMD) {
			delete m_pTransferLabel;  delete m_pTransferLabValue;  m_pTransferLabValue = NULL;
		}
		delete m_pEstimatedTimeLabel; delete m_pEstimatedTimeLabValue; m_pEstimatedTimeLabValue = NULL;
		delete m_pLabCurrentWeight; m_pLabCurrentWeight = NULL;

		if (eSubsysCmd == Vfs::WEIGH)
			delete m_pTotalProgress;
		if (eSubsysCmd == Vfs::SET_ATTRIBUTES_CMD) {
			delete m_pLabTotalWeight; m_pLabTotalWeight = NULL;
		}
		adjustSize();
	}
	else
	if (eSubsysCmd == Vfs::EXTRACT || eSubsysCmd == Vfs::COPY_TO_ARCHIVE || eSubsysCmd == Vfs::MOVE_TO_ARCHIVE || eSubsysCmd == Vfs::ARCHIVE) {
		delete m_pFileProgress;  m_pFileProgress  = NULL;
		delete m_pLabCurrentWeight; m_pLabCurrentWeight = NULL;
		delete m_pTransferLabel;  m_pTransferLabel = NULL;
		delete m_pTransferLabValue;  m_pTransferLabValue = NULL;
		delete m_pLabTotalWeight; m_pLabTotalWeight = NULL;
		delete m_pEstimatedTimeLabel;  m_pEstimatedTimeLabel = NULL;
		delete m_pEstimatedTimeLabValue;  m_pEstimatedTimeLabValue = NULL;
		adjustSize();
	}
// 	if (eSubsysCmd == Vfs::SET_ATTRIBUTES_CMD) {
// 		delete m_pTrgLabel;         m_pTrgLabel        = NULL;
// 	}

	bool bFileProgressCmd = (eSubsysCmd == Vfs::COPY || eSubsysCmd == Vfs::MOVE);

	if (bFileProgressCmd && eSubsysCmd != Vfs::SET_ATTRIBUTES_CMD)
		m_pTargetSLabValue->setText(sTargetPath);

	m_pCloseAfterFinishedChkBox->setChecked(bCloseAfterFinished);

	connect(m_pCancelBtn, &QPushButton::released, this, &ProgressDialog::slotCancel);
	connect(m_pInBrgdBtn, &QPushButton::released, this, &ProgressDialog::signalBackground);

	if (bFileProgressCmd) {
		m_pFileProgress->setRange(0, 100);
		m_pTransferLabValue->setText( "? "+tr("bytes per second") );
	}
	if (bFileProgressCmd || eSubsysCmd == Vfs::REMOVE || eSubsysCmd == Vfs::SET_ATTRIBUTES_CMD)
		m_pTotalProgress->setRange(0, 100);

	m_nBytesTransfered = 0;
	m_sTotalFiles = "0";
	m_sTotalDirs = "0";
	m_sNumFiles = "0";
	m_sNumDirs = "0";
	m_sWeigh = "0 bytes";
	m_nTotalWeight = 0;


	m_pTimer = new QTimer(this);
	connect(m_pTimer, &QTimer::timeout, this, &ProgressDialog::slotSetTime);

	if (m_bTotalVisible) {
		m_pTime.restart();
		m_pTime.start();
//		warning: ‘void QTime::start()’ is deprecated: Use QElapsedTimer instead [-Wdeprecated-declarations]

		slotSetTime();
		timerStart();
	}
	if (bFileProgressCmd)
		m_pFileProgress->setVisible(m_bTotalVisible);
	m_pTotalProgress->setVisible(m_bTotalVisible);

	show();
}

void ProgressDialog::setWndTitle( Vfs::SubsystemCommand eSubsysCmd )
{
	QString sTitle;

	if (eSubsysCmd == Vfs::COPY)	sTitle = tr("Copying");
	else
	if (eSubsysCmd == Vfs::MOVE)	sTitle = tr("Moving");
	else
	if (eSubsysCmd == Vfs::REMOVE)	sTitle = tr("Removing");
	else
	if (eSubsysCmd == Vfs::WEIGH)	sTitle = tr("Weighing");
	else
	if (eSubsysCmd == Vfs::EXTRACT)	sTitle = tr("Extracting");
	else
	if (eSubsysCmd == Vfs::SET_ATTRIBUTES_CMD)	sTitle = tr("Setting the attributes");
	else
	if (eSubsysCmd == Vfs::COPY_TO_ARCHIVE || eSubsysCmd == Vfs::MOVE_TO_ARCHIVE || eSubsysCmd == Vfs::ARCHIVE)
		sTitle = tr("Archiving");

	setWindowTitle(sTitle+" "+tr("files")+" - QtCommander");
}

void ProgressDialog::timerStart()
{
	slotTimerStartStop(true);
}

void ProgressDialog::timerStop( bool bRemoveIt )
{
	slotTimerStartStop(false);
	if (bRemoveIt)
		delete m_pTimer;
}



void ProgressDialog::slotSetFilesName( const QString &sSourceName, const QString &sTargetName )
{
	if (m_pSourceSLabValue != NULL)
		m_pSourceSLabValue->setText(sSourceName);

	if (m_pTargetSLabValue != NULL)
		m_pTargetSLabValue->setText(sTargetName);
}


void ProgressDialog::slotDataTransferProgress( long long nBytesDone, long long nBytesTotal, uint nBytesTransfered, bool bSetTotalBytes )
{
	if (nBytesTransfered > 0 && m_bTotalVisible) {
		if (m_pFileProgress != NULL) {
			if (nBytesDone == nBytesTotal)
				m_pFileProgress->setValue( 100 );
			else {
				if (nBytesTotal > 0)
					m_pFileProgress->setValue( (nBytesDone*100) / nBytesTotal );
			}
		}
		if (nBytesTotal > 0)
			m_nBytesTransfered += nBytesTransfered;
	}
// 	else {
		QString sTotalValue;
		if (bSetTotalBytes) {
			if (m_pLabTotalWeight != NULL) {
				sTotalValue = (m_bTotalVisible) ? Vfs::FileInfo::bytesRound(nBytesTotal) : "?";
				m_pLabTotalWeight->setText(Vfs::FileInfo::bytesRound(nBytesDone) + " / " + sTotalValue);
			}
		}
		else {
			if (m_pLabCurrentWeight != NULL) {
				sTotalValue = (m_bTotalVisible) ? Vfs::FileInfo::bytesRound(nBytesTotal) : "?";
				m_pLabCurrentWeight->setText(Vfs::FileInfo::bytesRound(nBytesDone) + " / " + sTotalValue);
			}
		}
// 	}
}


void ProgressDialog::slotSetTotalProgress( int nDoneInPercents, long long nTotalWeight )
{
	if (! m_bTotalVisible)
		return;

	if ( nDoneInPercents > 0 )
		m_pTotalProgress->setValue( nDoneInPercents );

	m_nTotalWeight = nTotalWeight;

	if (nDoneInPercents == 100)
		timerStop();
}


void ProgressDialog::slotSetTime() // and transfer
{
	if (! m_bTotalVisible)
		return;
	// -- init time and transfer labels
	if (m_pDateTime.time().second() == 0) {
		m_pTimeLabel->show(); m_pTimeLabValue->show();

		if (m_pTransferLabValue != NULL) {
			m_pTransferLabel->show();
			m_pTransferLabValue->show();
		}
		if (m_pEstimatedTimeLabValue != NULL) {
			m_pEstimatedTimeLabel->show();
			m_pEstimatedTimeLabValue->show();
		}
	}

	m_pDateTime.setTimeSpec(Qt::UTC);
	m_pDateTime.setTime_t(m_pTime.elapsed() / 1000);
	m_pTimeLabValue->setText((m_pDateTime.time()).toString());
	// TODO try to change m_pDateTime to QTime (do like below)
	if (m_nBytesTransfered > 0) {
		//m_pTransferLabValue->setText( formatNumber(m_nBytesTransfered,BKBMBGBformat)+" "+tr("per second") );
		m_pTransferLabValue->setText( Vfs::FileInfo::bytesRound(m_nBytesTransfered)+" "+tr("per second") );
		// --- set an estimated time
		if (m_nTotalWeight > 0) {
			QTime estimatedTime(0, 0);
			estimatedTime = estimatedTime.addSecs( m_nTotalWeight/m_nBytesTransfered );
			m_pEstimatedTimeLabValue->setText( estimatedTime.toString() );
		}
		m_nBytesTransfered = 0;
	}
}

void ProgressDialog::slotSetCounter( long long nValue, Vfs::ProgressCounter counter, bool bSetTotalValue )
{
	if (bSetTotalValue) {
		if (counter == Vfs::FILE_COUNTER)
			m_sTotalFiles = QString::number(nValue);
		else
		if (counter == Vfs::DIR_COUNTER)
			m_sTotalDirs  = QString::number(nValue);
		else
		if (counter == Vfs::WEIGH_COUNTER)
			m_sTotalWeigh = Vfs::FileInfo::bytesRound(nValue);
		// -- update labels
		if (counter == Vfs::WEIGH_COUNTER) {
			if (m_pLabTotalWeight != NULL)
				m_pLabTotalWeight->setText(m_sWeigh + " / " + m_sTotalWeigh);
		}
		else {
			QString sTotalFiles = (m_bTotalVisible) ? m_sTotalFiles : "?";
			QString sTotalDirs  = (m_bTotalVisible) ? m_sTotalDirs  : "?";
			m_pProcessedLabValue->setText(
				tr("files")+": "+m_sNumFiles+" / "+sTotalFiles+", "+tr("directories")+": "+m_sNumDirs+" / "+sTotalDirs  // usable for weighing operation
// 				tr("files")+": "+m_sTotalFiles+", "+tr("directories")+": "+m_sTotalDirs  // usable for weighing operation
			);
			setWndTitle(m_eSubsysCmd);
		}
	}
	else {
		if (counter == Vfs::FILE_COUNTER)
			m_sNumFiles = QString::number( nValue );
		else
		if (counter == Vfs::DIR_COUNTER)
			m_sNumDirs  = QString::number( nValue );
		else
		if (counter == Vfs::WEIGH_COUNTER)
			m_sWeigh    = Vfs::FileInfo::bytesRound(nValue);
		// -- update labels
		if (counter == Vfs::WEIGH_COUNTER) {
			if (m_pLabTotalWeight != NULL) {
				QString sTotalWeigh = (m_bTotalVisible) ? m_sTotalWeigh : "?";
				m_pLabTotalWeight->setText(m_sWeigh + " / " + sTotalWeigh);
			}
		}
		else {
			QString sTotalFiles = (m_bTotalVisible) ? m_sTotalFiles : "?";
			QString sTotalDirs  = (m_bTotalVisible) ? m_sTotalDirs  : "?";
			m_pProcessedLabValue->setText(
				tr("files")+": "+m_sNumFiles+" / "+sTotalFiles+", "+
				tr("directories")+": "+m_sNumDirs +" / "+sTotalDirs
			);
			setWndTitle(m_eSubsysCmd);
		}
	}
// 	qDebug("%s %s", m_pProcessedLabel->text().toLatin1().data(), m_pProcessedLabValue->text().toLatin1().data() );
}


void ProgressDialog::slotSetOperation( Vfs::SubsystemCommand eSubsysCmd )
{
	QString sMsg;

	if (eSubsysCmd == Vfs::COPY)   sMsg = tr("Copied");
	else
	if (eSubsysCmd == Vfs::MOVE)   sMsg = tr("Moved");
	else
	if (eSubsysCmd == Vfs::REMOVE) sMsg = tr("Removed");
	else
	if (eSubsysCmd == Vfs::WEIGH)  sMsg = tr("Weighed");
	else
	if (eSubsysCmd == Vfs::EXTRACT) sMsg = tr("Extracted");
	else
	if (eSubsysCmd == Vfs::COPY_TO_ARCHIVE || eSubsysCmd == Vfs::MOVE_TO_ARCHIVE || eSubsysCmd == Vfs::ARCHIVE) sMsg = tr("Archived");
	else
	if (eSubsysCmd == Vfs::SET_ATTRIBUTES_CMD) {
		sMsg = tr("Set the attributes");
		m_pTransferLabel->setText(tr("Set for :"));
		//m_pTransferLabValue
	}
	else // unknown operation
		return;

	m_pProcessedLabel->setText(sMsg+" :");
}


void ProgressDialog::slotTimerStartStop( bool bStart )
{
	if (! m_bTotalVisible)
		return;

	if (bStart)
		m_pTimer->start(1000); // emits signal every 1 seconds
	else
		m_pTimer->stop();

	m_pInBrgdBtn->setEnabled(bStart);
}

void ProgressDialog::slotUpdateLabelOfCancelBtnWithDone()
{
	m_pCancelBtn->setText(tr("&Done"));
	m_pCancelBtn->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogOkButton));
	m_pCancelBtn->setFocus();
}


void ProgressDialog::closeEvent( QCloseEvent *pCloseEvent )
{
	if (m_bTotalVisible)
		m_pTimer->stop();

	emit signalCancel(m_dlgdId);
	pCloseEvent->ignore();
}

