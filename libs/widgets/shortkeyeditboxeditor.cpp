/***************************************************************************
 *   Copyright (C) 2008 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QtGui>

#include "shortkeyeditboxeditor.h"


ShortKeyEditBoxEditor::ShortKeyEditBoxEditor( QWidget *pParent ) : QLineEdit(pParent)
{
	this->setFrame(false);
	this->setReadOnly(true);
	m_sModifiers = "";

	m_KeysStateMap[Qt::Key_Menu]    = "Menu";
	m_KeysStateMap[Qt::Key_Meta]    = "Win";
	m_KeysStateMap[Qt::Key_Alt]     = "Alt";
	m_KeysStateMap[Qt::Key_Control] = "Ctrl";
	m_KeysStateMap[Qt::Key_Shift]   = "Shift";
	m_KeysStateMap[Qt::Key_Super_L] = "LeftWin";
	m_KeysStateMap[Qt::Key_Super_R] = "RightWin";

	m_KeysStateMap[Qt::Key_Hyper_L] = "LeftHyper";
	m_KeysStateMap[Qt::Key_Hyper_R] = "RightHyper";
	m_KeysStateMap[Qt::Key_Direction_L] = "LeftDirection";
	m_KeysStateMap[Qt::Key_Direction_R] = "RightDirection";
}


// FIXME Key_Menu is not grabbed (probably parent class or QTableWidget class handles it higher), but maybe eventFilter could help
void ShortKeyEditBoxEditor::keyPressEvent( QKeyEvent *pKeyEvent )
{
	QString sState = "";
	int key = pKeyEvent->key();
// 	qDebug() << "ShortKeyEditBoxEditor::keyPressEvent. key:" << key;

	if (key == Qt::Key_Tab || key == Qt::Key_Backtab || key == Qt::Key_Escape) {
		pKeyEvent->ignore();
		return;
	}
// 	m_sModifiers = "";
	Qt::KeyboardModifiers modifiers = pKeyEvent->modifiers();
	KeysMap::Iterator itState = m_KeysStateMap.find( key );
	if (itState != m_KeysStateMap.end()) {
		if (! itState.value().isEmpty())
			sState = itState.value()+"+"; // for singlr mofifiers (like Ctrl+)

		if (modifiers == (Qt::AltModifier | Qt::ControlModifier))
			sState = "Alt+Ctrl+";
		else
		if (modifiers == (Qt::AltModifier | Qt::ShiftModifier))
			sState = "Alt+Shift+";
		else
		if (modifiers == (Qt::ControlModifier | Qt::ShiftModifier))
			sState = "Ctrl+Shift+";
		else
		if (modifiers == (Qt::AltModifier | Qt::ControlModifier | Qt::ShiftModifier))
			sState = "Alt+Ctrl+Shift+";
		m_sModifiers = sState;
	}
	if (key == Qt::Key_AltGr) { // RightAlt
		qDebug() << "ShortKeyEditBoxEditor::keyPressEvent. Right ALT is not handled! This is modifier for achieving national characters.";
		pKeyEvent->ignore();
		return;
	}
	QString sKeyTxt = pKeyEvent->text();
	QString sKeyCombin = m_sModifiers + sKeyTxt;

	//qDebug() << "keyText=" << sKey << "key=" << key << "sKeySeq=" << sKeySeq << "sKeySeq.length()=" << sKeySeq.length() << "keyCombin=" << sKeyCombin << "modifiers=" << modifiers;
	//ShortKeyEditBoxEditor::keyPressEvent. sModifiers: "" m_sModifiers: "Ctrl+" sKeyCombin: "Ctrl+\u000B" key: "K" Key printable: K sKeyTxt: "\u000B" sKeyTxt printable: true true
// 	qDebug() << "ShortKeyEditBoxEditor::keyPressEvent. sModifiers:" << sState << "m_sModifiers:" << m_sModifiers << "sKeyCombin:" << sKeyCombin
// 		<< "key:" << QKeySequence(key).toString() << "Key printable:" << qPrintable(QKeySequence(key).toString())
// 		<< "sKeyTxt:" << sKeyTxt;

	sKeyTxt = qPrintable(QKeySequence(key).toString());
	sKeyCombin = m_sModifiers + ((sState.isEmpty()) ? sKeyTxt : "");
	QLineEdit::setText(sKeyCombin);
	qDebug() << "ShortKeyEditBoxEditor::keyPressEvent. Final shortcut:" << sKeyCombin; // << "OldKeyshortcut:" << m_sOldKeyshortcut;
	if (sKeyTxt.isEmpty())
		m_sModifiers = "";

	if (! sKeyCombin.endsWith('+') && sKeyCombin != "+")
		emit signalDone();

} // Seems to work, only incorrectly restore old shortcut, incorrectly works checking collisions, shortcut using in env.like Ctrl+F2 are grabbed by env. (KDE)

