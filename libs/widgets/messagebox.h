/***************************************************************************
 *   Copyright (C) 2005 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef MESSAGEBOX_H
#define MESSAGEBOX_H

#include <QMessageBox>
#include <QButtonGroup>

#include "subsystemconst.h"
#include "ui_messageboxdlg.h"

/**
	@author Piotr Mierzwiński
*/
class MessageBox : public QDialog, Ui::MessageBoxDlg
{
public:
	MessageBox();

	enum MsgButtons { Yes=1, No, All, None, Cancel, // remove directory
	                  Continue=1, Stop,
					 Skip=1, SkipAll, Again, Break };   // cannot remove

	enum MsgType    { INFORMATION=0, WARNING, CRITICAL };

	/** Shows message window with button(s).
	  * @param pParent NOT USED NOW,
	  * @param sCaption title of window,
	  * @param sMsg window message,
	  * @param slBtnsNames buttons name list,
	  * @param nDefaultButton default button id,
	  * @param icon QMessageBox ikon,
	  * @param sChkBoxMsg message for CheckBox if empty then not show CheckBox.
	  * @return button id (type MsgButtons), but when sChkBoxMsg is not empty then button id + 100
	  **/
	static int common( QWidget *pParent, const QString &sCaption, const QString &sFImsg, const QString &sMsg, bool &bChecked, const QStringList &slBtnsNames=QStringList(), int nDefaultButton=-1, QMessageBox::Icon icon=QMessageBox::NoIcon, const QString &sChkBoxMsg=QString() );

	static int msg( QWidget *pParent, MessageBox::MsgType msgType, const QString &sMsg, const QString &sFImsg=QString(), Vfs::SubsystemCommand eFScommand=Vfs::NO_COMMAND, const QString &sButton0Text=QString() );

	static int information( QWidget *pParent, const QString &sCaption, const QString &sMsg, const QString &sButton0Text=QString() );
	static int warning( QWidget *pParent, const QString &sCaption, const QString &sFImsg, const QString &sMsg, bool &bChecked, const QString &sChkBoxMsg=QString(), const QString &sButtonText=QString() );
	static int critical( QWidget *pParent, const QString &sCaption, const QString &sFImsg, const QString &sMsg, bool &bChecked, const QString &sChkBoxMsg=QString(), const QString &sButtonText=QString() );
	static int yesNo( QWidget *pParent, const QString &sCaption, const QString &sFImsg, const QString &sMsg, bool &bChecked, int nDefaultButton=-1, const QString &sChkBoxMsg=QString(), bool bThreeButtons=false, const QString &sButtonText=QString() );

private:
	static QPixmap standardIcon(QMessageBox::Icon icon);

private:
	QButtonGroup *m_pButtonGroup;

};

#endif
