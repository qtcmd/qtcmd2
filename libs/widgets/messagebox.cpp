/***************************************************************************
 *   Copyright (C) 2005 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QIcon>
#include <QLabel>
#include <QPixmap>
#include <QMessageBox>
#include <QPushButton>
#include <QApplication>
#include <QStyle>
#include <QDebug>

#include "messagebox.h"


MessageBox::MessageBox()
{
	setupUi(this);
	m_pButtonGroup = new QButtonGroup;

	connect(m_pButtonGroup, static_cast<void (QButtonGroup::*)(int)>(&QButtonGroup::idClicked), this, &::MessageBox::done);
}

int MessageBox::common( QWidget *pParent, const QString &sCaption, const QString &sFImsg, const QString &sMsg, bool &bChecked, const QStringList &slBtnsNames, int nDefaultButton, QMessageBox::Icon icon, const QString &sChkBoxMsg )
{
	MessageBox *pDlg = new MessageBox;
	Q_CHECK_PTR( pDlg );

	pDlg->setParent(pParent);
	pDlg->setWindowFlags(Qt::Dialog);
	pDlg->m_pixmapLab->setPixmap( standardIcon(icon) );

	if ( ! sCaption.isEmpty() )
		pDlg->setWindowTitle( sCaption );

	if ( ! sFImsg.isEmpty() ) {
		pDlg->m_fileInfoLabel->setText( sFImsg );
	}
	if ( ! sMsg.isEmpty() ) {
		pDlg->m_msgLabel->setText( sMsg );
		if (slBtnsNames.count() == 1)
			pDlg->m_msgLabel->setSqueezingMode(SqueezeLabel::WRAP_LINE);
	}

	// --- set buttons
	const int nTotalButtons = 5;
	int nMaxButtons = slBtnsNames.count();

	if (nMaxButtons > nTotalButtons) {
		qDebug() << "MessageBox::common. Too much buttons! Max is:" << nTotalButtons;
		nMaxButtons = 5;
	}
	if ( nMaxButtons < 1 )
		nMaxButtons = 1;

	QPushButton *pButtonsTab[nTotalButtons] =
		{ pDlg->m_btn1, pDlg->m_btn2, pDlg->m_btn3, pDlg->m_btn4, pDlg->m_btn5 };

	for (int i=0; i<nTotalButtons; i++ )
		pDlg->m_pButtonGroup->addButton( pButtonsTab[i], i + Yes );

	// set buttons text
	if (nMaxButtons > 1) {
		if ( ! slBtnsNames[0].isEmpty() )
			for ( int i=0; i<slBtnsNames.count(); i++ ) {
				pButtonsTab[i]->setText( slBtnsNames[i] );
				if (pButtonsTab[i]->text() == tr("&Cancel"))
					pButtonsTab[i]->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogCancelButton));
// 				else if (pButtonsTab[i]->text() == tr("&Yes"))
// 					pButtonsTab[i]->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogYesButton));
// 				else if (pButtonsTab[i]->text() == tr("&No"))
// 					pButtonsTab[i]->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogNoButton));
			}
			pButtonsTab[0]->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogYesButton));
			pButtonsTab[1]->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogNoButton));
	}
	else { // nMaxButtons == 1
		if (slBtnsNames[0].isEmpty()) {
			pDlg->m_btn1->setText(tr("&OK"));
			pDlg->m_btn1->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogOkButton));
		}
		else
			pDlg->m_btn1->setText(slBtnsNames[0]);
	}

	int nMaxDlgWidth = 0; // it's width of dialog with everyone buttons
	for (int i = 0; i < nTotalButtons; i++) {
		pButtonsTab[i]->setAutoDefault(true);
		nMaxDlgWidth += pButtonsTab[i]->width()+10; // 10 - margin
	}

	// set default button
	if (nDefaultButton > 0) {
// 		pButtonsTab[nDefaultButton-1]->setDefault(true);
		pButtonsTab[nDefaultButton-1]->setFocus();
	}
	else
	if (nDefaultButton == -1)
		; // don't set default button
	else { // set nDefaultButton on first button
// 		pButtonsTab[0]->setDefault(true);
		pButtonsTab[0]->setFocus();
	}

	//qDebug("MessageBox::common, sChkBoxMsg= %s", sChkBoxMsg.toLatin1().data());
	// support for check box
	if (sChkBoxMsg.isEmpty())
		delete pDlg->m_pCheckBox;
	else {
		pDlg->m_pCheckBox->setText(sChkBoxMsg);
		pDlg->m_pCheckBox->setChecked(bChecked);
		pDlg->m_pCheckBox->setFocusPolicy(Qt::NoFocus);
	}

	// remove not used buttons
	int i = nMaxButtons;
	while (i<nTotalButtons) {
		delete pButtonsTab[i];
		i++;
	}

	if (pDlg->width() > nMaxDlgWidth)
		pDlg->adjustSize();
	else {
		if (nMaxButtons < 3)
			nMaxDlgWidth += (nMaxDlgWidth/2)/2;
		else
		if (nMaxButtons > 2)
			nMaxDlgWidth += nMaxDlgWidth/2;

		nMaxDlgWidth /= 2;
		nMaxDlgWidth += (nMaxDlgWidth / 4);
		pDlg->resize(nMaxDlgWidth+2, pDlg->sizeHint().height());
	}
	pDlg->setMinimumWidth(pDlg->width());

	// --- show dialog
	int nResult = pDlg->exec();

	// return checkBox state
	if (! sChkBoxMsg.isEmpty()) {
		bChecked = pDlg->m_pCheckBox->isChecked();
	}
	delete pDlg;  pDlg = 0;

	return nResult;
}


int MessageBox::msg( QWidget *pParent, MsgType msgType, const QString &sMsg, const QString &sFImsg, Vfs::SubsystemCommand eFScommand, const QString &sButton0Text )
{
	QString sCaption;
	QMessageBox::Icon icon = QMessageBox::NoIcon;
	if (msgType == INFORMATION)   {
		icon = QMessageBox::Information;
		sCaption = tr("Information");
	}
	else if (msgType == WARNING) {
		icon = QMessageBox::Warning;
		sCaption = tr("Warning");
	}
	else if (msgType == CRITICAL) {
		icon = QMessageBox::Critical;
		if      (eFScommand == Vfs::WEIGH)
			sCaption = tr("Weighing error");
		else if (eFScommand == Vfs::CONNECT)
			sCaption = tr("Connecting error");
		else if (eFScommand == Vfs::OPEN)
			sCaption = tr("Opening error");
		else if (eFScommand == Vfs::COPY)
			sCaption = tr("Copying error");
		else if (eFScommand == Vfs::MOVE)
			sCaption = tr("Moving error");
		else if (eFScommand == Vfs::REMOVE)
			sCaption = tr("Removing error");
		else if (eFScommand == Vfs::RENAME)
			sCaption = tr("Renaming error");
		else if (eFScommand == Vfs::FIND)
			sCaption = tr("Finding error");
		else if (eFScommand == Vfs::SET_ATTRIBUTES_CMD)
			sCaption = tr("Setting attributes error");
		else if (eFScommand == Vfs::CREATE_DIR)
			sCaption = tr("Creating dir error");
		else if (eFScommand == Vfs::CREATE_FILE)
			sCaption = tr("Creating file error");
		else if (eFScommand == Vfs::EXTRACT)
			sCaption = tr("Extracting error");
		else if (eFScommand == Vfs::COPY_TO_ARCHIVE || eFScommand ==Vfs::MOVE_TO_ARCHIVE)
			sCaption = tr("Archiving error");
		else
			sCaption = tr("Error");
	}
	QStringList lst;
	bool bChecked;
	lst << sButton0Text;
	return common( pParent, sCaption + " - QtCommander", sFImsg, sMsg, bChecked, lst, 0, icon );
}


int MessageBox::information( QWidget *pParent, const QString &sCaption, const QString &sMsg, const QString &sButton0Text )
{
	QStringList lst;
	bool bChecked;
	lst << sButton0Text;
	return common( pParent, sCaption + " - QtCommander", "", sMsg, bChecked, lst, 0, QMessageBox::Information );
}

int MessageBox::warning( QWidget *pParent, const QString &sCaption, const QString &sFImsg, const QString &sMsg, bool &bChecked, const QString &sChkBoxMsg, const QString &sButtonText )
{
	QStringList lst;
	lst << sButtonText;
	return common( pParent, sCaption + " - QtCommander", sFImsg, sMsg, bChecked, lst, 0, QMessageBox::Warning, sChkBoxMsg );
}

int MessageBox::critical( QWidget *pParent, const QString &sCaption, const QString &sFImsg, const QString &sMsg, bool &bChecked, const QString &sChkBoxMsg, const QString &sButtonText )
{
	QStringList lst;
	lst << sButtonText;
	return common( pParent, sCaption + " - QtCommander", sFImsg, sMsg, bChecked, lst, 0, QMessageBox::Critical, sChkBoxMsg );
}

int MessageBox::yesNo( QWidget *pParent, const QString &sCaption, const QString &sFImsg, const QString &sMsg, bool &bChecked, int nDefaultButton, const QString &sChkBoxMsg, bool bThreeButtons, const QString &sButtonText )
{
	QStringList slBtnsNames;
	slBtnsNames << tr("&Yes") << tr("&No");

	if ( bThreeButtons ) {
		if ( sButtonText.isEmpty() )
			slBtnsNames.append( tr("&Cancel") );
		else
			slBtnsNames.append( sButtonText );
	}

	return common( pParent, sCaption + " - QtCommander", sFImsg, sMsg, bChecked, slBtnsNames, nDefaultButton, QMessageBox::Warning, sChkBoxMsg );
}


QPixmap MessageBox::standardIcon( QMessageBox::Icon icon )
{
	int iconSize = QApplication::style()->pixelMetric(QStyle::PM_MessageBoxIconSize);
	QIcon tmpIcon;

	switch (icon) {
		case QMessageBox::Information:
			tmpIcon = QApplication::style()->standardIcon(QStyle::SP_MessageBoxInformation);
			break;
		case QMessageBox::Warning:
			tmpIcon = QApplication::style()->standardIcon(QStyle::SP_MessageBoxWarning);
			break;
		case QMessageBox::Critical:
			tmpIcon = QApplication::style()->standardIcon(QStyle::SP_MessageBoxCritical);
			break;
		case QMessageBox::Question:
			tmpIcon = QApplication::style()->standardIcon(QStyle::SP_MessageBoxQuestion);
		default:
			break;
	}
	if (! tmpIcon.isNull())
		return tmpIcon.pixmap(iconSize, iconSize);

	return QPixmap();
}
