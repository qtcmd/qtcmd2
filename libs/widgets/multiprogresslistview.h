/***************************************************************************
 *   Copyright (C) 2015 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _MULTIPROGRESSLISTVIEW_H
#define _MULTIPROGRESSLISTVIEW_H

#include <QWidget>
#include <QMouseEvent>
#include <QtUiPlugin/QDesignerExportWidget>

#include "treeview.h"
#include "iconcache.h"
#include "contextmenu.h"
#include "multiprogresslist.h"
#include "multiprogresslistviewmodel.h"


class QDESIGNER_WIDGET_EXPORT MultiProgressListView : public TreeView
{
	Q_OBJECT // necessary for designer (even if there is no any slots/signals)
public:
	MultiProgressListView( QWidget *pParent );
	~MultiProgressListView();

	void initListView( MultiProgressList *pMultiProgressList );

	void clear() { m_pModel->clear(); }

	int  rowCount() const { return m_pModel->rowCount(); }

// 	void setFirstAsCurrent();

	// TODO: below methods could be moved to the TreeView, and then ContextMenu should be moved to libs/widgets
// 	void setContextMenuEnabled( bool bEnableContextMenu );
// 	void setContextMenuType( ContextMenu::ContextMenuType contextMenuType );
// 	bool contextMenuEnabled() const { return (m_pContextMenu != NULL); }
// 	ContextMenu *contextMenu() { return m_pContextMenu; }

// 	void getItemsFromList( MultiProgressList *pMultiProgressList ) { m_pModel->collectVisibleItems(pMultiProgressList); }

// 	void updateRemoved( FileInfo *pFileInfo ) { m_pModel->removeItem(pFileInfo); }

// 	MultiProgressListViewItem *currentProcessedItem();

	void setParent( QWidget *pParent ) { m_pParent = pParent; }

private:
	MultiProgressListViewModel      *m_pModel;
// 	MultiProgressListViewProxyModel *m_pProxyModel;

	ContextMenu *m_pContextMenu;;
	QWidget     *m_pParent;


// 	QModelIndex currentModelIndex() const { return model()->index(currentIndex().row(), 0); }

// 	void showContextMenu( bool bOnCursorPos );

// private slots:
// 	void slotContexMenuAction( int actionId );

// protected:
// 	void mouseReleaseEvent ( QMouseEvent *pME );

};

#endif // _MULTIPROGRESSLISTVIEW_H
