/***************************************************************************
 *   Copyright (C) 2005 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QtGui>
#include <QRadioButton>

#include "settings.h"
#include "messagebox.h"
#include "listwidgetview.h"

#include "keyshortcutspage.h"
#include "keyshortcutsappids.h"


KeyShortcutsPage::KeyShortcutsPage( bool bFilesPanelSettings )
	: m_pKeyShortcuts(nullptr)
{
	setupUi(this);

	connect(m_pEditCurrShortcutBtn, &QPushButton::clicked, this, &KeyShortcutsPage::slotEditCurrentShortcut);
	connect(m_pCleanBtn, &QPushButton::clicked, this, &KeyShortcutsPage::slotCleanLineEdit);

	m_sConfigFileKey = (bFilesPanelSettings) ? "FilesPanelShortcuts/" : "FileViewShortcuts/";
	m_sOldShortcut = "";

	init();
}


void KeyShortcutsPage::init()
{
// 	qDebug() << "KeyShortcutsPage::init.";
	Settings settings;
	QString sScheme = settings.value("FilesPanel/ColorsScheme", "DefaultColors").toString();
	QString sColor  = settings.value(sScheme+"/SecondBackgroundColor", "FFEBDC").toString();

	// prepare view
	// -- add columns
	m_pKeyShortcutsListView->addColumn(ListWidgetView::TextLabel,   tr("Action")); //, Qt::AlignLeft);
	m_pKeyShortcutsListView->addColumn(ListWidgetView::KeyEditBox,  tr("Shortcut"));
	m_pKeyShortcutsListView->addColumn(ListWidgetView::RadioButton, tr("Own"), Qt::AlignHCenter);
	m_pKeyShortcutsListView->addColumn(ListWidgetView::RadioButton, tr("None"), Qt::AlignHCenter);
	m_pKeyShortcutsListView->addColumn(ListWidgetView::RadioButton, tr("Default shortcut"), Qt::AlignLeft); // includes text label
// 	m_pKeyShortcutsListView->addColumn(ListWidgetView::TextLabel,   tr("Default shortcut"));

// 	m_pLineEditSeachShortcut->setFocus();
	setValidationStatusAsBgColor(ValidStatusType::VS_none);

	// -- set look&feel
	//m_pKeyShortcutsListView->setSortingEnabled(true); // do not move to inside of ListWidgetView class, do not change order
	//m_pKeyShortcutsListView->setSecondColorOfBg(color(sColor, "FFEBDC")); // not yet supported
	//m_pKeyShortcutsListView->setEnableTwoColorsOfBg(true); // true is default setting of list and it uses system colors
// 	m_pKeyShortcutsListView->header()->resizeSection(m_pListWidget->header()->logicalIndex(0), (m_pListWidget->window()->width()/2)+20); // set as 1/4 of width
// 	m_pKeyShortcutsListView->header()->resizeSection(0, (width()-20)/2);

	connect(m_pKeyShortcutsListView, static_cast<void(ListWidgetView::*)(QTableWidgetItem *, const QString &)>(&ListWidgetView::editBoxTextApplied),
			this, &KeyShortcutsPage::slotEditBoxTextApplied);
	connect(m_pKeyShortcutsListView, static_cast<void(ListWidgetView::*)(QTableWidgetItem *)>(&ListWidgetView::buttonClicked),
			this, &KeyShortcutsPage::slotButtonClicked);

	connect(m_pLineEditSeachShortcut, &QLineEdit::textChanged, this, &KeyShortcutsPage::slotTextFilterChanged);
}

void KeyShortcutsPage::setKeyShortcuts( KeyShortcuts *pKeyShortcuts )
{
	qDebug() << "KeyShortcutsPage::setKeyShortcuts.";
	if (pKeyShortcuts != nullptr)
		m_pKeyShortcuts = pKeyShortcuts;
	if (pKeyShortcuts == nullptr && m_pKeyShortcuts == nullptr) {
		qDebug() << "KeyShortcutsPage::setKeyShortcuts. KeyShortcuts object is empty!";
		return;
	}
// 	const int ReadOnlyKeys[1] = {
// 		InvertSelection_APP
// 	}; // save list to configuration file

	int actionId, rowId = 0;
	QHash <int, KeyCode*>::iterator it;
	QString sShortcut, sDefaultShortcut, sDefaultState;
	// -- init values
	for (it = m_pKeyShortcuts->fistItem(); it != m_pKeyShortcuts->lastItem(); ++it) {
		actionId = it.key() - KeyShortcuts::ACTION_ID_OFFSET;
// 		qDebug() << "KeyShortcutsPage::setKeyShortcuts. actionId" << actionId << "desc:" << m_pKeyShortcuts->desc(actionId);
		sDefaultShortcut =  m_pKeyShortcuts->keyDefaultStr(actionId);
		sShortcut        = (m_pKeyShortcuts->keyStr(actionId) == "none") ? sDefaultShortcut : m_pKeyShortcuts->keyStr(actionId);
		//if (actionId == 7 || actionId == 8)
			//qDebug() << "KeyShortcutsPage::setKeyShortcuts. put:" << m_pKeyShortcuts->keyStr(actionId) << "in row:" << rowId << "keyId:" << rowId;
// 		qDebug() << "KeyShortcutsPage::setKeyShortcuts. key:" << m_pKeyShortcuts->keyStr(actionId) << "defaultShortcut:" << sDefaultShortcut << m_pKeyShortcuts->desc(actionId); // DEBUG

// 		m_pKeyShortcutsListView->setSpan(rowId, 4, rowId, 5);
		m_pKeyShortcutsListView->addRow(true); // set buttons exclusive mode
		// - set values
		m_pKeyShortcutsListView->setCellValue(rowId, ActionCOL, m_pKeyShortcuts->desc(actionId));
		n_hActions[actionId] = m_pKeyShortcuts->desc(actionId);
		if (sShortcut == "none")
            m_pKeyShortcutsListView->setCellValue(rowId, NoneCOL, "TRUE");
		else {
			m_pKeyShortcutsListView->setCellValue(rowId, ShortcutCOL, sShortcut);
			m_pKeyShortcutsListView->setCellValue(rowId, OwnCOL, "TRUE");
		}
// 		m_pKeyShortcutsListView->setCellValue(rowId, DefaulCOL, sDefaultShortcut);
		if (sShortcut == sDefaultShortcut)
			m_pKeyShortcutsListView->setCellValue(rowId, DefaultCOL, "TRUE");
		sDefaultState = (sShortcut == sDefaultShortcut) ? "TRUE" : "FALSE"; // for RadioButton with label
		m_pKeyShortcutsListView->setCellValue(rowId, DefaultCOL, sDefaultState, sDefaultShortcut); // for RadioButton with label
// 		m_pKeyShortcutsListView->setSpan(rowId, DefaultCOL, 1, 2); // works well only if first is text col, and next RadioButton
		rowId++;
	}

	m_pKeyShortcutsListView->setFocus();
    m_pKeyShortcutsListView->resizeColumnsToContents();
// 	m_pKeyShortcutsListView->setCurrentItem(m_pKeyShortcutsListView->item(0,0), QItemSelectionModel::ClearAndSelect);
// 	m_pKeyShortcutsListView->currentItem()->setSelected(true);
	m_pKeyShortcutsListView->horizontalHeader()->resizeSection(0, m_pKeyShortcutsListView->sizeHint().width()); // half of view
	m_pKeyShortcutsListView->sortBy(ActionCOL);
	m_pKeyShortcutsListView->selectRow(0);

	m_nTotalShortcuts = m_pKeyShortcutsListView->rowCount();
}

KeyShortcutsPage::ValidStatus KeyShortcutsPage::updateList()
{
	bool bEmptySearchStr = m_pLineEditSeachShortcut->text().isEmpty();
	if (bEmptySearchStr) { // restore whole list
		m_pKeyShortcutsListView->removeRows();
		setKeyShortcuts(nullptr);
		m_pLineEditSeachShortcut->setFocus();
		return ValidStatus::VS_none;
	}

	if (m_nTotalShortcuts == m_pKeyShortcutsListView->rowCount()) {
		if (m_pLineEditSeachShortcut->text().count() < 2) // need to type more than 1 character - at least 2
			return ValidStatus::VS_none;
	}
	// - check if any of item on list matches to pattern given in text field
	QList<QTableWidgetItem *> lstOfFound;
	lstOfFound = m_pKeyShortcutsListView->findItems(m_pLineEditSeachShortcut->text(), Qt::MatchContains);
// 	qDebug() << "KeyShortcutsPage::updateList. Found matching (in cols):" << lstOfFound.count() << "all:" << m_pKeyShortcutsListView->rowCount();
	ValidStatus validStatus = ValidStatus::VS_none;
	if (! bEmptySearchStr) {
		bool validOK = (lstOfFound.count() > 0 && lstOfFound.count() <= m_pKeyShortcutsListView->rowCount());
		validStatus = (validOK) ? ValidStatus::VS_valid : ValidStatus::VS_notValid;
	}
	// - collect all rows ID where found matching items
	QList <int> lstOfRowsID;
	if (validStatus == ValidStatus::VS_valid) { // found something
		foreach (QTableWidgetItem *item, lstOfFound) {
			lstOfRowsID << n_hActions.key(item->text());
// 			qDebug() << "KeyShortcutsPage::updateList. Found item:" << item->text() << "row:" << item->row();
		}
	}
	else { // found nothing, maybe no need to change list
		if (m_nTotalShortcuts == m_pKeyShortcutsListView->rowCount())
			return validStatus;
	}

	if (lstOfRowsID.count() > 0 || bEmptySearchStr) // CleanLst
		m_pKeyShortcutsListView->removeRows();

// 	qDebug() << "KeyShortcutsPage::updateList. All visible rows is:" << m_pKeyShortcutsListView->rowCount() << ". Found rows to filtering:" << (lstOfRowsID.count() > 0);
	int actionId, rowId = 0;
	QString sShortcut, sDefaultShortcut, sDefaultState;
	// -- init values
	foreach (actionId, lstOfRowsID) {
		if (m_pKeyShortcuts->key(actionId) == nullptr) { // keyshortcut with given ID doesn't exist
			qDebug() << "KeyShortcutsPage::updateList. FOUND EMPTY keyshortcut";
			continue;
		}
// 		qDebug() << "KeyShortcutsPage::updateList. Show row:" << rowToShow; // lstOfRowsID.at(rowId);
		sDefaultShortcut =  m_pKeyShortcuts->keyDefaultStr(actionId);
		sShortcut        = (m_pKeyShortcuts->keyStr(actionId) == "none") ? sDefaultShortcut : m_pKeyShortcuts->keyStr(actionId);

		m_pKeyShortcutsListView->addRow(true); // set exclusive mode for buttons
// 		qDebug() << "KeyShortcutsPage::updateList. All visible rows (after adding) is:" << m_pKeyShortcutsListView->rowCount();
		// - sm_sOldShortcutet values in row
		m_pKeyShortcutsListView->setCellValue(rowId, ActionCOL, m_pKeyShortcuts->desc (actionId));
// 		qDebug() << "KeyShortcutsPage::updateList. Found at Id:" << actionId << "act:" << m_pKeyShortcuts->description(actionId);
		if (sShortcut == "none")
            m_pKeyShortcutsListView->setCellValue(rowId, NoneCOL, "TRUE");
		else {
			m_pKeyShortcutsListView->setCellValue(rowId, ShortcutCOL, sShortcut);
			m_pKeyShortcutsListView->setCellValue(rowId, OwnCOL, "TRUE");
		}
// 		m_pKeyShortcutsListView->setCellValue(rowId, DefaultCOL, sDefaultShortcut);
// 		if (sShortcut == sDefaultShortcut)
//             m_pKeyShortcutsListView->setCellValue(rowId, DefaultCOL, "TRUE");
		sDefaultState = (sShortcut == sDefaultShortcut) ? "TRUE" : "FALSE"; // for RadioButton with label
		m_pKeyShortcutsListView->setCellValue(rowId, DefaultCOL, sDefaultState, sDefaultShortcut); // for RadioButton with label

        rowId++;
	}

	return validStatus;
}

void KeyShortcutsPage::setValidationStatusAsBgColor( ValidStatus validStatus )
{
	QLineEdit *pLineEdit = m_pLineEditSeachShortcut;
	if (pLineEdit == NULL)
		return;

	QColor   bgColor;
	QPalette palette;

	if (validStatus == ValidStatusType::VS_valid)
		bgColor = QColor( 201,241,215 ); //old: 228, 240, 223
	else
	if (validStatus == ValidStatusType::VS_notValid)
		bgColor = QColor( 241,229,229 ); //old: 255, 229, 229
	else
		bgColor = Qt::white;

	palette.setColor(pLineEdit->backgroundRole(), bgColor);
	palette.setColor(pLineEdit->foregroundRole(), Qt::black);
	pLineEdit->setAutoFillBackground(true);
	pLineEdit->setPalette(palette);
}

void KeyShortcutsPage::setDefaults()
{
	for (int nRow=0; nRow < m_pKeyShortcutsListView->rowCount(); nRow++) {
		m_pKeyShortcutsListView->setCellValue(nRow, ShortcutCOL, m_pKeyShortcutsListView->item(nRow, DefaultCOL)->text());
		m_pKeyShortcutsListView->setCellValue(nRow, DefaultCOL, "TRUE");
	}
}


void KeyShortcutsPage::slotTextFilterChanged()
{
// 	qDebug() << "KeyShortcutsPage::slotTextFilterChanged" << m_pLineEditSeachShortcut->text();
	ValidStatus validStatus = updateList(); // filter the list of key shortcuts
	setValidationStatusAsBgColor(validStatus);
}

void KeyShortcutsPage::slotButtonClicked( QTableWidgetItem *pItem )
{
	int nRow    = pItem->row();
	int nColumn = pItem->column();
// 	qDebug() << "KeyShortcutsPage::slotButtonClicked. row/col:" << nRow << nColumn;
	if (nColumn < OwnCOL || nColumn > DefaultCOL) // because one appears after each other
		return;

	if (nColumn == OwnCOL) {
		//slotEditCurrentShortcut(); // don't use it due to in this case is taking incorrect row
		m_pKeyShortcutsListView->editItem(m_pKeyShortcutsListView->item(nRow, ShortcutCOL));
		//m_pKeyShortcutsListView->openPersistentEditor(pItem); // not necessary if was used "setCurrentCell"
		m_pKeyShortcutsListView->setCurrentCell(nRow, ShortcutCOL); // opens PersistentEditor
	}
	else
	if (nColumn == NoneCOL)
        m_pKeyShortcutsListView->setCellValue(nRow, ShortcutCOL, "");
	else
	if (nColumn == DefaultCOL) {
		m_pKeyShortcutsListView->setCellValue(nRow, ShortcutCOL, m_pKeyShortcutsListView->cellValue(nRow, DefaultCOL, true));
		//qDebug() << "KeyShortcutsPage::slotButtonClicked." << "currentCellValue" << m_pKeyShortcutsListView->cellValue(nRow, DefaultCOL, true) << "nRow:" << nRow;
		slotEditBoxTextApplied(m_pKeyShortcutsListView->item(nRow, ShortcutCOL), m_pKeyShortcutsListView->cellValue(nRow, DefaultCOL, true));
	}
}


// FIXME  close closePersistentEditor when user click on different item
void KeyShortcutsPage::slotEditBoxTextApplied( QTableWidgetItem *pItem, const QString &sNewShortcut )
{
	//qDebug() << "KeyShortcutsPage::slotEditBoxTextApplied. NewShortcut:" << sNewShortcut << "pItem->row:" << pItem->row();
	if (m_pKeyShortcutsListView->isPersistentEditorOpen(pItem)) {
		m_pKeyShortcutsListView->closePersistentEditor(pItem); // prevents to double sending signal "editBoxTextApplied"
// 		qDebug() << "KeyShortcutsPage::slotEditBoxTextApplied. closePersistentEditor, closed:" << m_pKeyShortcutsListView->isPersistentEditorOpen(pItem); // false after double click, and false after openPersistentEditor
	}
	// --- check a new key shortcut if already used
	//int actionId;
	int nRow = 0;
	bool bOk = true;
	QString sOwnShortcut, sConflictedShortcut;
	const int nCurrentRow = pItem->row();
	QHash <int, KeyCode*>::iterator it;

	// -- find collision
	for (it = m_pKeyShortcuts->fistItem(); it != m_pKeyShortcuts->lastItem(); ++it) {
		sOwnShortcut = m_pKeyShortcutsListView->cellValue(nRow, ShortcutCOL);
		if (sOwnShortcut == sNewShortcut && nCurrentRow != nRow) {
			//actionId = it.key() - KeyShortcuts::ACTION_ID_OFFSET;
			//qDebug() << "KeyShortcutsPage::slotEditBoxTextApplied. sOwnShortcut:"<< sOwnShortcut << "sNewShortcut:"<<sNewShortcut << "nCurrentRow:"<<nCurrentRow << "nRow:"<<nRow << "actionId:"<< actionId;
			sConflictedShortcut = m_pKeyShortcuts->keyDesc(sNewShortcut);
			if (sConflictedShortcut.isEmpty())
				break;
			int nResult = MessageBox::yesNo(this, tr("Shortcut already used"),
				sNewShortcut+" "+tr("shortcut is already set for action")+":\n\n\""+sConflictedShortcut +"\"\n",
				tr("Are you sure you want to reassign and unset mentioned action?"),
					bOk, MessageBox::No // set focus on this button
				);
			if (nResult == 0 || nResult == MessageBox::No) { // user closed dialog or clicked No or pressed Escape
				if (m_sOldShortcut.isEmpty()) { // very first canceling of shortcut
					m_sOldShortcut = m_pKeyShortcutsListView->cellValue(nCurrentRow, DefaultCOL, true);
					m_pKeyShortcutsListView->setCellValue(nCurrentRow, DefaultCOL, "TRUE");
					m_pKeyShortcutsListView->setCellValue(nCurrentRow, ShortcutCOL, m_sOldShortcut);
					return;
				}
				m_pKeyShortcutsListView->setCellValue(nCurrentRow, ShortcutCOL, m_sOldShortcut); // m_pKeyShortcuts->keyStr(nCurrentRow) // incorrect because in this moment here is dupl.value
				//qDebug() << "KeyShortcutsPage::slotEditBoxTextApplied. Canceled changing keyshortcut, sOldShortcut:" << m_sOldShortcut;
			}
			else { // set new shortcut (actualy left what has been set already)
				m_pKeyShortcutsListView->setCellValue(nRow, ShortcutCOL, ""); // clear shortcut for found action
				m_pKeyShortcutsListView->setCellValue(nRow, NoneCOL, "TRUE");
			}
			break;
		}
		nRow++;
	}

	QString sDefaultShortcut = m_pKeyShortcutsListView->cellValue(nCurrentRow, DefaultCOL, true);
	//qDebug() << "KeyShortcutsPage::slotEditBoxTextApplied. sDefaultShortcut:" << sDefaultShortcut << "sNewShortcut:" << sNewShortcut << "nCurrentRow:" << nCurrentRow;
	if (sNewShortcut == sDefaultShortcut)
		m_pKeyShortcutsListView->setCellValue(nCurrentRow, DefaultCOL, "TRUE");
	else
		m_pKeyShortcutsListView->setCellValue(nCurrentRow, OwnCOL, "TRUE");

	m_sOldShortcut = m_pKeyShortcutsListView->cellValue(nCurrentRow, ShortcutCOL);
// 	m_pKeyShortcutsListView->closePersistentEditor(pItem); // required in exit to editor was correctly closed
}

void KeyShortcutsPage::slotEditCurrentShortcut()
{
// 	qDebug() << "KeyShortcutsPage::slotEditCurrentShortcut:";
	int nRow = m_pKeyShortcutsListView->currentRow(); // not usable if user clicks directly into Own button (returns incorrect row)
	QTableWidgetItem *pItem = m_pKeyShortcutsListView->item(nRow, ShortcutCOL);
	m_pKeyShortcutsListView->editItem(pItem);
	//m_pKeyShortcutsListView->openPersistentEditor(pItem); // not necessary if one uses "setCurrentCell"
	m_pKeyShortcutsListView->setCurrentCell(nRow, ShortcutCOL); // opens PersistentEditor
}


void KeyShortcutsPage::slotSave()
{
	int actionId, nRow = 0;
	Settings settings;
	QHash <int, KeyCode*>::iterator it;
	QString sShortcut, sActionID, sDefaultShortcut;
	// -- init values
	for (it = m_pKeyShortcuts->fistItem(); it != m_pKeyShortcuts->lastItem(); ++it) {
		actionId = it.key() - KeyShortcuts::ACTION_ID_OFFSET;
// 		qDebug() << "KeyShortcutsPage::setKeyShortcuts. actionId" << actionId << "desc:" << m_pKeyShortcuts->desc(actionId);DefaultCOL
		sDefaultShortcut =  m_pKeyShortcuts->keyDefaultStr(actionId);
		sShortcut        = (m_pKeyShortcuts->keyStr(actionId) == "none") ? sDefaultShortcut : m_pKeyShortcuts->keyStr(actionId);

		if (sShortcut.isEmpty())
            sShortcut = m_pKeyShortcutsListView->cellValue(nRow, DefaultCOL, true);
		if (sShortcut.isEmpty())
			continue; // no shortcut for given action ID

		sActionID = QString("%1").arg(actionId+KeyShortcuts::ACTION_ID_OFFSET);
// 		qDebug() << "KeyShortcutsPage::slotSave:" << m_sConfigFileKey+"ActionID_"+sActionID << "=" << sShortcut;
		settings.setValue(m_sConfigFileKey+"ActionID_"+sActionID, sShortcut);
		nRow++;
	}
}

