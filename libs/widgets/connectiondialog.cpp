/***************************************************************************
 *   Copyright (C) 2005 by Piotr Mierzwi�ski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDebug> // required for qDebug()

#include "settings.h"
#include "connectiondialog.h"


ConnectionDialog::ConnectionDialog()
{
	setupUi(this);
	setWindowTitle(tr("Connect to")+" ... - QtCommander 2");
}


void ConnectionDialog::init( const URI & uri )
{
	Q_ASSERT(uri.isValid());

	QString sUser = uri.user();
	if (sUser.isEmpty())
		sUser = "anonymous";
	m_sUsersList.append(sUser);

	QString sPath = uri.path();
	if (sPath.isEmpty())
		sPath = "/";
	m_sUDirsList.append(sPath);

/*
	// --- read user's list and directories from config file
	// TODO: might to add here combo contains list of sessions
	Settings settings;

	QString sProtocol = uri.protocol();
	m_sUsersList = settings.readListEntry("/qtcmd2/"+sProtocol+"/UsersName", '\\');
	if (m_sUsersList.isEmpty()) {
		m_sUsersList.append("anonymous");
		settings.writeEntry("/qtcmd2/"+sProtocol+"/UsersName", "anonymous");
		//settings.writeEntry("/qtcmd2/"+sProtocol+"/UsersName", "anonymous", '\\');
	}
	if (m_sUDirsList.isEmpty()) {
		m_sUDirsList.append("/");
		settings.writeEntry("/qtcmd2/"+sProtocol+"/UsersName", "/");
		//settings.writeEntry("/qtcmd2/"+sProtocol+"/UsersName", "/", '\\');
	}
*/
	m_pUserCombo->addItems(m_sUsersList);
	m_pDirCombo->addItems(m_sUDirsList);
	m_pHostEdit->setText(uri.host());
	m_pPortSpin->setValue(uri.port());
	m_pPasswordEdit->setText(uri.password());
	m_pProtocolLab->setText(uri.protocol()+"://");

// 	QString sUser = uri.user();
// 	if (sUser.isEmpty()) // URI not holds user name
// 		return;

	bool bInsertUser = true;
	for (int id=0; id < m_sUsersList.count(); id++) {
		if (m_sUsersList[id] == sUser) {
			m_pUserCombo->setCurrentIndex(id);
			bInsertUser = false;
			break;
		}
	}

	if (bInsertUser) {
		m_pUserCombo->addItem(sUser);
		m_pUserCombo->setCurrentIndex(m_pUserCombo->findText(sUser));
	}
}

bool ConnectionDialog::showDlg( QWidget *pParent, URI &uri, const QString &sProtocol, const QString &sNoPassUser )
{
	ConnectionDialog *pDlg = new ConnectionDialog;
	Q_CHECK_PTR(pDlg);

	pDlg->init(uri);
	pDlg->setParent(pParent);
	pDlg->setWindowFlags(Qt::Dialog);

	Settings settings;
	bool bShowConnDlgIfEmptyPassword = settings.value(sProtocol+"/ShowConnDlgIfEmptyPassword", true).toBool();
	if (! bShowConnDlgIfEmptyPassword && ! uri.host().isEmpty()) {
		if (uri.password().isEmpty() && (uri.user() == sNoPassUser || uri.user().isEmpty()))
			return true;
	}

	bool bRunConnection = false;
	uint nCount = 0;

	while (pDlg->exec() == QDialog::Accepted) {
		bRunConnection = true;  nCount = 0;
		if (pDlg->m_pUserCombo->currentText().isEmpty())
			//MessageBox::critical(0, tr("Do not gived user name!"));
			qDebug() << "Didn't pass user name!!";
		else
			nCount++;

/*		// password might be empty
		if (pDlg->m_pPasswordEdit->text().isEmpty())
			//MessageBox::critical(0, tr("Do not gived password!"));
			qDebug() << "Do not gived password!";
		else
			nCount++;
*/
		if (nCount == 1)
			break;
	}

	if (bRunConnection) {
		QString sProtocol = uri.protocol().toUpper();
		QString sInputURL;
		pDlg->makeNewURL(sInputURL); // the 'sInputURL' will be contains a new URL
		uri = URI(sInputURL);
/*
		Settings settings;
		if (pDlg->m_pSaveSessionChkBox->isChecked()) {
			// --- save an URL to the end of session list
			int n = -1;
			bool addSession = true;
			QString session = "x";
			while (! session.isEmpty()) { // find number of last session
				n++;
				session = settings.readEntry(QString("/qtcmd2/"+sProtocol+"/S%1").arg(n));
				if (session.section( "\\", 1,1 ) == sInputURL) // disable add this same session
					addSession = FALSE;
			}
			if ( addSession )
				settings.writeEntry( QString("/qtcmd2/"+sProtocol+"/S%1").arg(n),
					tr("new session")+"\\"+sInputURL );
		}
		// --- add new sUser name (only if not exist it on the list) and save new list
		QString sCurrentUserName = pDlg->m_pUserCombo->currentText();
		bool bAddUser = true;
		for ( int id=0; id < pDlg->m_sUsersList.count(); id++ ) {
			if ( pDlg->m_sUsersList[id] == sCurrentUserName ) {
				bAddUser = FALSE;
				break;
			}
		}
		if ( bAddUser ) { // new sUser, add it's name to the config file
			pDlg->m_sUsersList.append( sCurrentUserName );
			settings.writeEntry( "/qtcmd2/"+sProtocol+"/UsersName", pDlg->m_sUsersList, '\\' );
		}
*/
	}

	delete pDlg;
	pDlg = 0;

	return bRunConnection;
}

void ConnectionDialog::makeNewURL( QString &sNewURL )
{
	QString sHost = m_pHostEdit->text();
	QString sPath = m_pDirCombo->currentText();
	QString sUser = m_pUserCombo->currentText();
	QString sPass = m_pPasswordEdit->text();
	QString nPort = QString::number(m_pPortSpin->value());

	if (sPath.isEmpty())
		sPath = "/";
	if (sPath.at(0) != '/')
		sPath.insert(0, '/');
	if (! sPath.endsWith('/'))
		sPath += "/";

	int nProtId;
	if ((nProtId=sHost.indexOf("://")) != -1)
		sHost.remove(0, nProtId+3);
	if (sHost.isEmpty())
		sHost = "localhost";

	if (sUser.isEmpty())
		sUser = "anonymous";
	if (! sPass.isEmpty())
		sPass.insert(0, ':');
	if (nPort.isEmpty())
		nPort = "21";

	unsigned int hostLen = sHost.length()-1;
	if (sHost.at(hostLen) == '/') // sHost name cannot be ended by the slash
		sHost.truncate(hostLen);

	sUser = sUser.trimmed();
	sHost = sHost.trimmed();
	// sDir is not trimmed because is white spaces are allowed in path
	sNewURL = m_pProtocolLab->text()+sUser+sPass+"@"+sHost+":"+nPort+ sPath;
// 	qDebug() << "sNewURL:" << sNewURL;
}

