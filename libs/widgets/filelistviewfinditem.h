/***************************************************************************
 *   Copyright (C) 2011 by Piotr Mierzwi�ski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef FILELISTVIEWFINDITEM_H
#define FILELISTVIEWFINDITEM_H

#include <QTimer>

#include "ui_filelistviewfinditemdlg.h"

class QKeyEvent;
class TreeView;
class ListWidgetView;

/**
 @author Piotr Mierzwiński
 */
class FileListViewFindItem : public QWidget, Ui::FileListViewFindItemDlg
{
	Q_OBJECT
public:
	FileListViewFindItem( QWidget *pParent );
	~FileListViewFindItem();

	/** Inits dialog.
	 */
	void init( QChar firstChar=0 );

	/** Enables or disables auto hiding after constant time.
	 * Be default auto hiding is disabled.
	 * @param bAutoHide if true then auto hide after constant time otherwise dosn't hide'
	 */
	void setAutoHide( bool bAutoHide );

	/** Sets pointer to proper list view widget on which user will navigate.
	 * @param pTreeView pointer to list view class TreeView
	 */
	void setListView( TreeView *pTreeView ) { m_treeView = pTreeView; }

	/** Sets pointer to proper list view widget on which user will navigate.
	 * @param pListWidgetView pointer to list view class ListWidgetView
	 */
	void setListView( ListWidgetView *pListWidgetView ) { m_listWidget = pListWidgetView; }

private:
	/** Cursor movement directions for matching items.
	 */
	enum CursorMovements { CM_home = -2, CM_up, CM_none, CM_down, CM_end, CM_behind = 99 };

	 /** Text match status.
	 */
	enum MatchStatus     { MS_none, MS_match, MS_notMatch };


	TreeView       *m_treeView;
	ListWidgetView *m_listWidget;
	QTimer         *m_closeTimer;
	QKeyEvent      *m_keyEvent;
	bool            m_bMatchCaseSensitive;
	bool            m_bAutoHide;
	CursorMovements m_movementDirection;


	void preInit();

	/** Updates background's color of QLineEdit object depends to matching status.
	 * @param matchStatus - matching status (found/not found/other)
	 */
	void updateMatchStatus( FileListViewFindItem::MatchStatus matchStatus );

	/** Tries to match current text from QLineEdit to item on QTreeView.
	 * When item will be matched then item becomes like current item.
	 */
	void matchItem();


private slots:
	/** Moves cursor to next matching item.
	 */
	void slotNextMatching();

	/** Moves cursor to previous matching item.
	 */
	void slotPrevMatching();

public slots:
	/** Turn on/off checking case sensitive during matching item.
	 * @param bMatchCaseSensitive - true for case sensitive otherwise false
	 */
	void slotSetMatchCaseSensitive( bool bMatchCaseSensitive );

protected:
	/** Keyboard support for QLineEdit.
	 */
	void keyPressed( QKeyEvent *keyEvent );

	/** Used for stops m_blinkTimer and reseting current object.
	 *  @param ce - close dialog event.
	 */
	void closeEvent( QCloseEvent *ce );

	/** Catch events from QLineEdit (keyPress and keyRelease).
	 */
	bool eventFilter( QObject *obj, QEvent *event );

signals:
	/** Signal sent when not supported (in this class) key has been pressed
	 * Example: modifiers, function key, control key (Enter, Esc, etc.).
	 *  @param pKeyEvent - key event pointer.
	 */
	void signalKeyPressed( QKeyEvent *pKeyEvent );

//private:
//	Ui::FileListViewFindItemDlg ui;

};

#endif // FILELISTVIEWFINDITEM_H
