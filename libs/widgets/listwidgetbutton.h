/***************************************************************************
 *   Copyright (C) 2008 by Piotr Mierzwi?ski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _LISTWIDGETBUTTON_H
#define _LISTWIDGETBUTTON_H

#include <QTableWidgetItem>
#include <QRadioButton>
#include <QHBoxLayout>
#include <QPushButton>
#include <QCheckBox>
#include <QWidget>

/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class ListWidgetButton : public QWidget
{
	Q_OBJECT
public:
	enum Button { RadioButton=0, CheckBox, PushButton };

	ListWidgetButton( QTableWidgetItem *pItem, Button eButton, Qt::Alignment eAlign=Qt::AlignLeft, QWidget* pParent=NULL );
	~ListWidgetButton();

	Button button() { return m_eButton; }

	void setAlignment( Qt::Alignment eAlign ) { m_pLayout->setAlignment( buttonPtr(), eAlign ); }
	Qt::Alignment alignment() const { return m_eAlignment; }

	void setColor( const QColor &color );
	QColor color() const { return m_Color; }

	void setText( const QString &sText ) { m_pButton->setText(sText); }
	QString text() const { return m_pButton->text(); }

	QRadioButton *radioButton() const { return m_pRadioButton; }
	QPushButton  *pushButton() const  { return m_pPushButton;  }
	QCheckBox    *checkBox() const    { return m_pCheckBox; }

	QTableWidgetItem *tableWidgetItem() const { return m_pTableWidgetItem; }

private:
	QTableWidgetItem *m_pTableWidgetItem;

	Button m_eButton;
	Qt::Alignment m_eAlignment;

	QRadioButton *m_pRadioButton;
	QPushButton  *m_pPushButton;
	QCheckBox    *m_pCheckBox;

	QHBoxLayout  *m_pLayout;

	QColor m_Color;

	QAbstractButton *m_pButton;
	QAbstractButton *buttonPtr();

Q_SIGNALS:
	void clicked();

};

#endif // _LISTWIDGETBUTTON_H
