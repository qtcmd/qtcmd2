/***************************************************************************
 *   Copyright (C) 2008 by Piotr Mierzwi?ski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _LISTWIDGETVIEW_H
#define _LISTWIDGETVIEW_H

#include <QTableWidget>

/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class ListWidgetView : public QTableWidget
{
	Q_OBJECT
public:
	/** Initializes parameters of list view.
	 * - hides header
	 * - sets single selection
	 * - sets selection as selecting row
	 * - turns on alternating row colors
	 * - hides grid
	 */
	ListWidgetView( QWidget *pParent );

	enum Widget     { CheckBox=0, RadioButton, PushButton, ColorChooser, ComboBox, EditBox, KeyEditBox, TextLabel };
	enum TextStyle  { Regular=0, Bold=2, Italic=4 };


	/** Adds column with given type and name. Additionally sets align for it.
	 * @param eWidget type of widget check @em Widget list for details
	 * @param sName column name
	 * @param eColumnAlign align for column
	 */
	void addColumn( Widget eWidget, const QString &sName, Qt::AlignmentFlag eColumnAlign=Qt::AlignLeft );

	/** Adds row into the list and create in all defined columns set widget type.
	 * @oaram bButtonsExclusive if true then buttons (CheckBox and RadioButton) will work in exclusive mode, otherwise all can check
	 */
	void addRow( bool bButtonsExclusive=false );

	/** Clean all list (removing all rows).
	 */
	void removeRows();

	/** Sets value in cell for given index.
	 * @param nRow row on list
	 * @param nColumn column on list
	 * @param sValue value to set
	 * - "TRUE"" of "FALSE"" for RadioButton, CheckBox and
	 * - hexadecimal color code for ColorChooser
	 * - list of strings for ComboBox
	 * - label for other
	 * @param sBtnLabel label for widget type: RadioButton, CheckBox
	 */
	void setCellValue( int nRow, int nColumn, const QString &sValue, const QString &sBtnLabel=QString() ) {
		setCellValue(nRow, nColumn, QStringList(sValue), sBtnLabel);
	}
	/** Sets value in cell for given index.
	 * @param nRow row on list
	 * @param nColumn column on list
	 * @param sValue value to set
	 * @param sBtnLabel label for widget type: RadioButton, CheckBox
	 * @param nInitId ID of item to set on ComboBox widget
	 */
	void setCellValue( int nRow, int nColumn, const QStringList &slValue, const QString &sBtnLabel=QString(), int nInitId=0 );

	/** Returns cell value from given index. If given IDs will be less than 0 then will be use current row or/and column.
	 * @param nRow row on list
	 * @param nColumn column on list
	 * @param bUseLabel get value from label (usable only for: RadioButton and CheckBox)
	 * @return value from given index
	 */
	QString cellValue( int nRow=-1, int nColumn=-1, bool bUseLabel=false ) const;

	/** Changes color of widget to white/black (depends on theme) or gray (widget will look like enabled/disabled).
	 * Only if has been set next parameter then widget will be actual enabled/disabled.
	 * @param nRow row on list
	 * @param nColumn column on list
	 * @param bEnable if true then text in widget has white/black color, otherwise gray
	 * @param bEnableWidget if true then enables widget placed on given index, otherwise disable it
	 */
	void setEnableCell( int nRow, int nColumn, bool bEnable, bool bEnableWidget=true );

	/** Status of enabled widget in cell for given index.
	 * @param nRow row on list
	 * @param nColumn column on list
	 * @return true is widget is enabled otherwise false
	 */
	bool cellEnable( int nRow, int nColumn ) const;

	/** Sets ItemIsEditable flag for widget for given index otherwise doesn't set this flag
	 * @param nRow row on list
	 * @param nColumn column on list
	 */
	void setEditableCell( int nRow, int nColumn, bool bEditable );

	/** Staus of editable widget for given index.
	 * @param nRow row on list
	 * @param nColumn column on list
	 * @return true is widget is editable otherwise false
	 */
	bool editableCell( int nRow, int nColumn ) const;

	/** Sets attribute (here text color) in cell for given index.
	 * @param nRow row on list
	 * @param nColumn column on list
	 * @param color color of text
	 * @param eTextStyle style of text (regular, bold italic)
	 */
	void setCellAttributes( int nRow, int nColumn, const QColor &color, TextStyle eTextStyle=ListWidgetView::Regular );

	/** Sets attribute (style of text) in cell for given index.
	 * @param nRow row on list
	 * @param nColumn column on list
	 * @param eTextStyle style of text (regular, bold italic)
	 */
	void setCellAttributes( int nRow, int nColumn, TextStyle eTextStyle );

	/** Finds item with given value in given column.
	 * @param sText value to find
	 * @param nColumn ID of column where needs to find item
	 */
	int findItem( const QString &sText, int nColumn=-1 );

	/** Converts given value to boolean value.
	 * If value is "TRUE" then returns true otherwise false
	 * @param sValue value to convert, otherwise false
	 * @return true if value is "TRUE"
	 */
	bool getBoolValue( const QString &sValue );

	/** Sort items by given column with given order.
	 * @column column to sort
	 * @order order of sorting
	 */
	void sortBy( int column, Qt::SortOrder order = Qt::AscendingOrder ) {
		sortItems(column, order);
	}

private:
	QWidget *m_pParent;

	int m_nColumnCount;
	int m_nRowCount;

	bool m_bButtonsExclusive;

	QStringList m_slHeaderLabels;

	QList < Widget > m_eRowWidgetList;
	QList < Qt::AlignmentFlag > m_eColumnAlignList;
	QList < QTableWidgetItem * > m_pTableWidgetItemList;


	Widget columnWidget( int nColumn ) const { return m_eRowWidgetList[nColumn]; }

private Q_SLOTS:
	void slotButtonClicked();

protected Q_SLOTS:
	virtual void commitData( QWidget * pEditor );

Q_SIGNALS:
	void buttonClicked( QTableWidgetItem *pItem );
	void colorChanged( QTableWidgetItem *pItem );
	void editBoxTextApplied( QTableWidgetItem *pItem, const QString &sText );

};

#endif // _LISTWIDGETVIEW_H
