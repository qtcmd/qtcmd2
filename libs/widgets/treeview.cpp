/***************************************************************************
 *   Copyright (C) 2009 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QHelpEvent>

#include "treeview.h"

// Solution: Nils Jeisecke (adapted for QTreeView: Piotr Mierzwinski)

bool TreeView::viewportEvent( QEvent* event )
{
	if (event->type() == QEvent::ToolTip) {
		QHelpEvent *helpEvent = static_cast<QHelpEvent*>(event);
		QModelIndex index = indexAt(helpEvent->pos());

		QRect vr  = visualRect(index);
		QSize ish = itemDelegate(index)->sizeHint(viewOptions(), index);

		if (ish.width() > vr.width())
			return QTreeView::viewportEvent(event);
		else
			return false;
	}
	else
		return QTreeView::viewportEvent(event);

	return true;
}

void TreeView::mousePressEvent( QMouseEvent *event )
{
	//qDebug() << "TreeView::mousePressEvent";
	if (event->button() == Qt::RightButton) {
		// Unaccepting this event give an ability handling mouse events (except QEvent::ContextMenu, because it is always handled) in FileListView class
		// or ability overloading mousePressEvent(QMouseEvent *event) in FileListView class
		event->setAccepted(false);
	}
	if (event->button() == Qt::LeftButton) {
		event->setAccepted(false);
	}
	QTreeView::mousePressEvent(event);
}

