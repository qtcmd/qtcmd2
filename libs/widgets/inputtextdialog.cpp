/***************************************************************************
 *   Copyright (C) 2012 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDebug>
#include <QLineEdit>
#include <QCompleter>
#include <QFileDialog>
#include <QPushButton>
#include <QFileSystemModel>
#include <QDialogButtonBox>

#include "settings.h"
#include "inputtextdialog.h"


InputTextDialog::InputTextDialog()
{
	setupUi(this);
}


QString InputTextDialog::getText( QWidget *pParent, GetTextMode getMode, const QString &sInInitText, bool &bParam, const QString &sInLabel, const QStringList &slItemsList, const QString &sInWndTitle )
{
	InputTextDialog *pDlg = new InputTextDialog;
	Q_CHECK_PTR( pDlg );

	pDlg->setParent(pParent);
	pDlg->setWindowFlags(Qt::Dialog);
	pDlg->m_inputDlg = pDlg; // for slotChooseDir()
	pDlg->m_buttonBox->button(QDialogButtonBox::Ok)->setShortcut(Qt::Key_Return);

	bool bPostCreatingFileGoTo, bPostCreatingDirGoTo, bUseRegularExpr;
	bool bMakeFileOrDir = (getMode == MAKE_DIR || getMode == MAKE_FILE);
	bool bPatternSelectOrUnselect = (getMode == PATTERN_SELECT || getMode == PATTERN_UNSELECT);
	bool bCopyOrMove = (getMode == COPY || getMode == MOVE || getMode == UPLOAD || getMode == ADD_TO_ARCH || getMode == MOVE_TO_ARCH);
	bool bSaveAttributes, bAlwaysOverwrite, bWeighBeforeCopying;
	bool bPreservePath, bWeighArchBeforeExtracting;
	QString sInitText = sInInitText;

	// -- init key for config file and labels
	QString sConfigFileKeyList;
	QString sConfigFileKeyLastId;
	QString sWndTitle = sInWndTitle;
	QString sLabel    = sInLabel;
	if      (getMode == MAKE_FILTER) {
		sConfigFileKeyList = "MakeFilterList";
		if (sWndTitle.isEmpty())
			sWndTitle = tr("Filter name");
		if (sLabel.isEmpty())
			sLabel    = tr("New name for defined filter:");
	}
	else if (getMode == MAKE_DIR) {
		sConfigFileKeyList = "MakeDirectoryList";
		if (sWndTitle.isEmpty())
			sWndTitle = tr("Creating directory");
		if (sLabel.isEmpty())
			sLabel    = tr("Please enter new directory name:");
	}
	else if (getMode == MAKE_FILE) {
		sConfigFileKeyList = "MakeEmptyFileList";
		if (sWndTitle.isEmpty())
			sWndTitle = tr("Creating empty file");
		if (sLabel.isEmpty())
			sLabel    = tr("Please enter new file name:");
	}
	else if (getMode == MAKE_LINK) {
		sConfigFileKeyList = "MakeSymlinkList";
		if (sWndTitle.isEmpty())
			sWndTitle = tr("Creating link");
		if (sLabel.isEmpty())
			sLabel    = "?";
	}
	else if (getMode == EDIT_LINK) {
// 		configFileKeyList = "EditSymlinkList";
		if (sWndTitle.isEmpty())
			sWndTitle = tr("Modifying symlink");
		if (sLabel.isEmpty())
			sLabel    = "?";
	}
	else if (getMode == COPY) {
		sConfigFileKeyList = "CopyTargetList";
		sConfigFileKeyLastId = "CopyTargetLastId";
		if (sWndTitle.isEmpty())
			sWndTitle = tr("Copying files");
		if (sLabel.isEmpty())
			sLabel    = "?";
	}
	else if (getMode == MOVE) {
		sConfigFileKeyList = "MoveTargetList";
		sConfigFileKeyLastId = "MoveTargetLastId";
		if (sWndTitle.isEmpty())
			sWndTitle = tr("Moving files");
		if (sLabel.isEmpty())
			sLabel    = "?";
	}
	else if (getMode == UPLOAD) {
		sConfigFileKeyList = "";
		if (sWndTitle.isEmpty())
			sWndTitle = tr("Uploading");
		if (sLabel.isEmpty())
			sLabel    = "?";
	}
	else if (getMode == ADD_TO_ARCH) {
		sConfigFileKeyList = "";
		if (sWndTitle.isEmpty())
			sWndTitle = tr("Adding to archive");
		if (sLabel.isEmpty())
			sLabel    = "?";
	}
	else if (getMode == MOVE_TO_ARCH) {
		sConfigFileKeyList = "";
		if (sWndTitle.isEmpty())
			sWndTitle = tr("Moving to archive");
		if (sLabel.isEmpty())
			sLabel    = "?";
	}
	else if (getMode == EXTRACT_ARCHIVE) {
		sConfigFileKeyList = "";
		if (sWndTitle.isEmpty())
			sWndTitle = tr("Extracting");
		if (sLabel.isEmpty())
			sLabel    = "";
		else
			sLabel.insert(0, "<strong>"+tr("Extract archive:")+"</strong> ");
	}
	else if (getMode == EXTRACT_FILES) {
		sConfigFileKeyList = "";
		if (sWndTitle.isEmpty())
			sWndTitle = tr("Extracting files");
		if (sLabel.isEmpty())
			sLabel    = tr("Extract selected");
	}
	else if (getMode == RENAME_IN_OVR) {
		sConfigFileKeyList = "RenameTargetList";
		sConfigFileKeyLastId = "RenameTargetLastId";
		if (sWndTitle.isEmpty())
			sWndTitle = tr("Rename copied/moved file");
		if (sLabel.isEmpty())
			sLabel    = tr("New name for given file");
	}
	else if (getMode == PUT_RESULT_TO_PANEL) {
		sConfigFileKeyList = "";
		sConfigFileKeyLastId = "";
		if (sWndTitle.isEmpty())
			sWndTitle = tr("Files collection name");
		if (sLabel.isEmpty())
			sLabel    = tr("Please enter new collection name for found files:");
	}
	else if (getMode == GO_TO_LINE) {
		sConfigFileKeyList = "ListOfLineNumbers";
		sConfigFileKeyLastId = "ListOfLineNumbersLastId";
		if (sWndTitle.isEmpty())
			sWndTitle = tr("Go to line");
		if (sLabel.isEmpty())
			sLabel    = tr("Please enter new line number:");
	}
	else if (getMode == BOOKMARK_NAME) {
		sConfigFileKeyList = "";
		sConfigFileKeyLastId = "";
		if (sWndTitle.isEmpty())
			sWndTitle = tr("Bookmark name");
		if (sLabel.isEmpty())
			sLabel = tr("Please enter bookmark name:");
		else
			sLabel = tr("Please enter bookmark name for path:") +"\n\n\t"+sLabel;
	}
	else if (bPatternSelectOrUnselect) {
		sConfigFileKeyList = "PatternSelectList";
		sConfigFileKeyLastId = "PatternSelectLastId";
		if (sWndTitle.isEmpty()) {
			if (getMode == PATTERN_SELECT)
				sWndTitle = tr("Marking");
			else
				sWndTitle = tr("Unmarking");
			sWndTitle += " ";
			sWndTitle += tr("accordance with the template");
		}
		if (sLabel.isEmpty()) {
			sLabel = tr("Please enter new template(s):");
			pDlg->m_inputComboBox->setToolTip(
			tr("You can put here single template like: *.txt")+"\n"+
			tr("or more templates separated by '|', for example: *.cpp|*.h"));
		}
	}

	// --- populate comboBox by items list
	int initLastId = 0;
	QString sSection = "vfs/";
	if (getMode == MAKE_FILTER || bPatternSelectOrUnselect)
		sSection = "filelistview/";
	if (getMode == EXTRACT_ARCHIVE || getMode == EXTRACT_FILES || getMode == CREATE_ARCH)
		sSection = "archive/";

	Settings settings;
	QString sItemList;
	if (! sConfigFileKeyList.isEmpty()) { // read only when we have a key
		sItemList = settings.value(sSection+sConfigFileKeyList).toString();
		QStringList slItemList;
		if (! sItemList.isEmpty()) {
			if (bMakeFileOrDir || bPatternSelectOrUnselect)
				slItemList = sItemList.split("||");
			else
				slItemList = sItemList.split('|');

			pDlg->m_inputComboBox->clear();
			pDlg->m_inputComboBox->insertItems(0, slItemList);

			if (bPatternSelectOrUnselect) {
				initLastId = settings.value(sSection+sConfigFileKeyLastId, 0).toInt();
				if (initLastId < 0 || initLastId > slItemList.size()-1)
					initLastId = 0;
				sInitText = pDlg->m_inputComboBox->itemText(initLastId);
			}
		}
	}

	// --- populate archives type list
	pDlg->m_bCreateAnArchiveMode = (getMode == ADD_TO_ARCH || getMode == MOVE_TO_ARCH);
	if (getMode == ADD_TO_ARCH || getMode == MOVE_TO_ARCH) {
		if (! slItemsList.isEmpty()) {
			QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
			sizePolicy1.setHorizontalStretch(0);
			sizePolicy1.setVerticalStretch(0);
			//sizePolicy1.setHeightForWidth(pRadioButton->sizePolicy().hasHeightForWidth());
			QRadioButton *pRadioButton;

			foreach(QString sName, slItemsList) {
				pRadioButton = new QRadioButton(pDlg->m_scrollAreaWidgetContents);
				pRadioButton->setText(sName);
				pRadioButton->setObjectName(sName);
				pRadioButton->setSizePolicy(sizePolicy1);
				pDlg->verticalLayout->addWidget(pRadioButton);
				pDlg->m_radioButtonLst.append(pRadioButton);
				connect(pRadioButton, &QRadioButton::clicked, pDlg, &InputTextDialog::slotRadioButtonClicked);
			}
		}
		else
			qDebug() << "InputTextDialog::getText. WARNING. Empty incomming items list.";
	}

	// --- init window
	((QWidget *)pDlg)->setWindowTitle(sWndTitle + " - QtCommander");
	pDlg->m_option1chkBox->setText(tr("Save file &permission"));
	pDlg->m_option2chkBox->setText(tr("Always over&write"));
	pDlg->m_actionLabel->setText(sLabel);
	pDlg->m_inputComboBox->setEditText(sInitText);
	QLineEdit *pLineEdit = pDlg->m_inputComboBox->lineEdit();
	if (pLineEdit->text().contains('/')) {
		pLineEdit->setSelection(pLineEdit->text().lastIndexOf('/')+1, pLineEdit->text().length() - pLineEdit->text().lastIndexOf('/') - 1);
	}
	else
		pLineEdit->selectAll();

	QRadioButton *pCurrentRB = NULL;
	// below is used only in context: (getMode == ADD_TO_ARCH || getMode == MOVE_TO_ARCH)
	bool bCreateArchiveInCurrentPath = settings.value("archive/CreateArchiveInCurrentPath", false).toBool();
	bool bRememberLastUsedArchiveType = settings.value("archive/RememberLastUsedArchiveType", true).toBool();
	bool bWeighBeforeArchiving = settings.value("vfs/WeighBeforeArchiving", true).toBool();
	if (getMode == ADD_TO_ARCH || getMode == MOVE_TO_ARCH) {
		if (! slItemsList.isEmpty()) {
			if (bRememberLastUsedArchiveType) {
				QString sLastUsedArchiveType= settings.value("archive/LastUsedArchiveType", "tar").toString();
				QRadioButton *pRB = NULL;
				foreach(pRB, pDlg->m_radioButtonLst) {
					if (pRB->text() == sLastUsedArchiveType) {
						pDlg->m_sRecentlyUseExt = sLastUsedArchiveType;
						pRB->setChecked(true);
						pDlg->m_pLastClickedRB = pRB;
						pCurrentRB = pRB;
						break;
					}
				}
				if (pDlg->m_pLastClickedRB == nullptr)
					pDlg->m_pLastClickedRB = pDlg->m_radioButtonLst.at(0);
			}
			if (pCurrentRB == NULL) {
				pDlg->m_radioButtonLst.at(0)->setChecked(true);
				pDlg->m_sRecentlyUseExt = pDlg->m_radioButtonLst.at(0)->objectName();
			}
			pDlg->updateExtentionsInTargetArchivesNames(pDlg->m_sRecentlyUseExt);
		}
	}

	connect(pDlg->m_cleanToolButton, &QToolButton::clicked, pDlg->m_inputComboBox, &QComboBox::clearEditText);

	if ((getMode != ADD_TO_ARCH && getMode != MOVE_TO_ARCH) || slItemsList.isEmpty()) {
		pDlg->m_scrollArea->hide();
		pDlg->m_option4chkBox->hide();
	}

	if (getMode != EXTRACT_ARCHIVE && getMode != EXTRACT_FILES && ! bCopyOrMove) {
		pDlg->m_chooseDirToolBtn->hide();
		pDlg->m_toLabel->hide();
	}
	else {
		pDlg->m_toLabel->show();
		pDlg->m_fileNameLabel->hide();
		connect(pDlg->m_chooseDirToolBtn, &QToolButton::clicked, pDlg, &InputTextDialog::slotChooseDir);
	}

	// -- hide not necessary widgets, set toolTips, set label of additional checkBox, read config
	if (bPatternSelectOrUnselect ||
		getMode == MAKE_FILTER || getMode == MAKE_DIR || getMode == MAKE_FILE || getMode == PUT_RESULT_TO_PANEL ||
		getMode == RENAME_IN_OVR || getMode == GO_TO_LINE || getMode == BOOKMARK_NAME || getMode == MAKE_LINK || getMode == EDIT_LINK) {

		if (getMode != MAKE_LINK)
			pDlg->m_fileNameLabel->hide();
		else {
			if (slItemsList.size() > 0)
				pDlg->m_fileNameLabel->setText(slItemsList.at(0));
		}
		pDlg->m_option1chkBox->hide();
		pDlg->m_option2chkBox->hide();
		QString sAfterCreatingMsg = tr("After creating &open") +" "+ (sInitText.contains('|') ? tr("the last one") : tr("it"));

		if (getMode == MAKE_FILE) {
			pDlg->m_inputComboBox->setToolTip(
				tr("Templates:")+"\n"+
				tr("  fileName - create single empty file")+"\n"+
				tr("  fileName1|fileName2 - create two empty files: 'fileName1' and 'fileName2'"));
			pDlg->m_option3chkBox->setText(sAfterCreatingMsg);
			bPostCreatingFileGoTo = settings.value("vfs/PostCreatingFileGoTo", false).toBool();
			pDlg->m_option3chkBox->setChecked(bPostCreatingFileGoTo);
		}
		else
		if (getMode == MAKE_DIR) {
			pDlg->m_inputComboBox->setToolTip(
				tr("Templates:")+"\n"+
				tr("  dirName - create single directory")+"\n"+
				tr("  dirName/subDirName - create 'dirName' and subdirectory 'subDirName'")+"\n"+
				tr("  dirName1|dirName2 - create two directories: 'dirName1' and 'dirName2'")+"\n"+
				tr("  dirName1/subdir1|dirName2/subdir2 - create two directories with subdirectories"));
			pDlg->m_option3chkBox->setText(sAfterCreatingMsg);
			bPostCreatingDirGoTo = settings.value("vfs/PostCreatingDirGoTo", false).toBool();
			pDlg->m_option3chkBox->setChecked(bPostCreatingDirGoTo);
		}
		else
		if (getMode == MAKE_LINK)
			pDlg->m_option3chkBox->setText(tr("&Hard link"));
		else
		if (getMode == EDIT_LINK)
			pDlg->m_option3chkBox->hide();
		else
		if (getMode == MAKE_FILTER || getMode == PUT_RESULT_TO_PANEL || getMode == RENAME_IN_OVR || getMode == GO_TO_LINE || getMode == BOOKMARK_NAME) {
			pDlg->m_option3chkBox->hide();
			pDlg->m_chooseDirToolBtn->hide();
		}
		else
		if (bPatternSelectOrUnselect) {
			bUseRegularExpr = settings.value("filelistview/PatternSelectUsesRegExp", false).toBool();
			pDlg->m_option3chkBox->setChecked(bUseRegularExpr);
			pDlg->m_option3chkBox->setText(tr("&Regular expression"));
		}
	}
	else if (bCopyOrMove) {
		pDlg->m_option3chkBox->setText(tr("&Weigh items"));
		bAlwaysOverwrite = settings.value("vfs/AlwaysOverwrite", false).toBool();
		bWeighBeforeCopying = settings.value("vfs/WeighBeforeCopying", true).toBool();
		if (getMode == UPLOAD || getMode == ADD_TO_ARCH || getMode == MOVE_TO_ARCH) {
			if (sInInitText.contains('|')) {
				pDlg->m_inputComboBox->setEnabled(false);
				pDlg->m_cleanToolButton->setEnabled(false);
			}
		}
		else {
			bSaveAttributes = settings.value("vfs/SaveAttributes", true).toBool();
			pDlg->m_option1chkBox->setChecked(bSaveAttributes);
		}
		pDlg->m_option2chkBox->setChecked(bAlwaysOverwrite);
		pDlg->m_option3chkBox->setChecked(bWeighBeforeCopying);
		if (getMode == ADD_TO_ARCH || getMode == MOVE_TO_ARCH) {
			pDlg->m_chooseDirToolBtn->hide();
			pDlg->m_option2chkBox->setChecked(true);
			pDlg->m_option2chkBox->setEnabled(false); // NOTE not overwriting is not supported yet
			pDlg->m_option3chkBox->setText(tr("&Remember last used archive type"));
			pDlg->m_option3chkBox->setChecked(bRememberLastUsedArchiveType);
			pDlg->m_option1chkBox->setText(tr("Create &archive in current path"));
			pDlg->m_option1chkBox->setChecked(bCreateArchiveInCurrentPath);
			pDlg->m_option4chkBox->setText(tr("&Weigh before making archive"));
			pDlg->m_option4chkBox->setChecked(bWeighBeforeArchiving);
			pDlg->m_sTargetPath = QFileInfo(pDlg->m_inputComboBox->lineEdit()->text()).path();
			pDlg->slotUpdatePathForCreateArchiveInCurrentPath();
			connect(pDlg->m_option1chkBox, &QCheckBox::released, pDlg, &InputTextDialog::slotUpdatePathForCreateArchiveInCurrentPath);
		}
	}
	else if (getMode == EXTRACT_ARCHIVE) {
		bool bAlwaysOverwrite = true;// pSettings->value("archive/AlwaysOverwrite").toBool();
		pDlg->m_option2chkBox->setChecked(bAlwaysOverwrite);
		pDlg->m_option2chkBox->setDisabled(true); // option not yet supported (always is doing overwriting)
		bWeighArchBeforeExtracting = settings.value("archive/WeighBeforeArchExtracting", true).toBool();
		pDlg->m_option1chkBox->setText("&Weigh archive before extracting");
		pDlg->m_option1chkBox->setChecked(bWeighArchBeforeExtracting);
		bPreservePath = settings.value("archive/PreservePathWhenExtracting", true).toBool();
		pDlg->m_option3chkBox->setChecked(bPreservePath);
		pDlg->m_option3chkBox->setText(tr("&Preserve path when extracting"));
		pDlg->m_option3chkBox->setToolTip(tr("If checked then for extracted archive will be saved internal structure of directories."));
	}
	else if (getMode == EXTRACT_FILES) {
		bool bAlwaysOverwrite = true;// pSettings->value("archive/AlwaysOverwrite").toBool();
		pDlg->m_option2chkBox->setChecked(bAlwaysOverwrite);
		pDlg->m_option2chkBox->setDisabled(true); // option not yet supported (always is doing overwriting)
		pDlg->m_option1chkBox->setChecked(true);
		pDlg->m_option1chkBox->setDisabled(true); // option not yet supported (always is doing overwriting)
		pDlg->m_option3chkBox->setVisible(false);
	}

	if (! bCopyOrMove) {
		pDlg->adjustSize();
		pDlg->setMinimumSize(pDlg->sizeHint());
	}
	if (getMode == GO_TO_LINE)
		pDlg->resize(pDlg->width()/2, pDlg->height());
	else
	if (getMode == BOOKMARK_NAME)
		pDlg->resize(pDlg->width()+pDlg->width()/2, pDlg->height());
	else
	if (getMode == MAKE_LINK || getMode == EDIT_LINK)
		pDlg->resize(pDlg->width()/2+pDlg->width()/2, pDlg->height());

	if (bCopyOrMove || getMode == EXTRACT_ARCHIVE || getMode == EXTRACT_FILES) {
		pDlg->setMinimumWidth(pDlg->width()+pDlg->width()/3);
		pDlg->setMinimumHeight(pDlg->height()+5);
	}
	if ((getMode == ADD_TO_ARCH || getMode == MOVE_TO_ARCH) && pCurrentRB != NULL) {
		pDlg->resize(pDlg->width(), pDlg->height()+(pDlg->height()/5)-5);
		pDlg->m_scrollArea->setMinimumWidth(pDlg->m_scrollArea->width()+5);
		pDlg->show(); // because m_scrollArea has to know its final size to be able to ensureWidgetVisible
		//qApp->processEvents(); // The idea is that when the geometry changes, events for doing that are pushed on the event queue... So you execute the events to make the geometry changes and then you can ensure the widget is visible. Unfortunately it works only after first call
	 	pDlg->m_scrollArea->ensureWidgetVisible(pCurrentRB);
	}

	QString sCurrentPath = pDlg->m_inputComboBox->currentText();
	if (sCurrentPath.startsWith('/')) {
		QCompleter *completer = new QCompleter(pDlg);
		completer->setMaxVisibleItems(15);
		// Unsorted QFileSystemModel
		QFileSystemModel *fsModel = new QFileSystemModel(completer);
		fsModel->setRootPath(sCurrentPath);
		fsModel->setFilter(QDir::AllDirs);
		completer->setModel(fsModel);
		pDlg->m_inputComboBox->setCompleter(completer);
	}

	// --- exec window
	QString sReturnStr;
	bool bOkPressed = (((QDialog *)pDlg)->exec() == QDialog::Accepted);
	if (bOkPressed) {
		bool bValue;
		sReturnStr = pDlg->m_inputComboBox->currentText();
		if (sReturnStr.isEmpty())
			return sReturnStr;

		bParam = pDlg->m_option3chkBox->isChecked();
		// -- save configuration
		if (getMode == MAKE_FILE) {
			bPostCreatingFileGoTo = bParam;
			if (settings.value("vfs/PostCreatingFileGoTo").isNull())
				settings.setValue("vfs/PostCreatingFileGoTo", bPostCreatingFileGoTo);
			else {
				bValue = settings.value("vfs/PostCreatingFileGoTo").toBool();
				if (bValue != bPostCreatingFileGoTo)
					settings.setValue("vfs/PostCreatingFileGoTo", bPostCreatingFileGoTo);
			}
		}
		else
		if (getMode == MAKE_DIR) {
			bPostCreatingDirGoTo = bParam;
			if (settings.value("vfs/PostCreatingDirGoTo").isNull())
				settings.setValue("vfs/PostCreatingDirGoTo", bPostCreatingDirGoTo);
			else {
				bValue = settings.value("vfs/PostCreatingDirGoTo").toBool();
				if (bValue != bPostCreatingDirGoTo)
					settings.setValue("vfs/PostCreatingDirGoTo", bPostCreatingDirGoTo);
			}
		}
		else
		if (bPatternSelectOrUnselect) {
			bUseRegularExpr = bParam;
			if (settings.value("filelistview/PatternSelectUsesRegExp").isNull())
				settings.setValue("filelistview/PatternSelectUsesRegExp", bUseRegularExpr);
			else {
				bValue = settings.value("filelistview/PatternSelectUsesRegExp").toBool();
				if (bValue != bUseRegularExpr)
					settings.setValue("filelistview/PatternSelectUsesRegExp", bUseRegularExpr);
			}
		}
		else
		if (bCopyOrMove && getMode != ADD_TO_ARCH && getMode != MOVE_TO_ARCH) {
			if (getMode != UPLOAD) {
				bSaveAttributes = pDlg->m_option1chkBox->isChecked();
				if (settings.value("vfs/SaveAttributes").isNull())
					settings.setValue("vfs/SaveAttributes", bSaveAttributes);
				else {
					bValue = settings.value("vfs/SaveAttributes").toBool();
					if (bValue != bSaveAttributes)
						settings.setValue("vfs/SaveAttributes", bSaveAttributes);
				}
			}
			bAlwaysOverwrite = pDlg->m_option2chkBox->isChecked();
			if (settings.value("vfs/AlwaysOverwrite").isNull())
				settings.setValue("vfs/AlwaysOverwrite", bAlwaysOverwrite);
			else {
				bValue = settings.value("vfs/AlwaysOverwrite").toBool();
				if (bValue != bAlwaysOverwrite)
					settings.setValue("vfs/AlwaysOverwrite", bAlwaysOverwrite);
			}
			bWeighBeforeCopying = pDlg->m_option3chkBox->isChecked();
			if (settings.value("vfs/WeighBeforeCopying").isNull())
				settings.setValue("vfs/WeighBeforeCopying", bWeighBeforeCopying);
			else {
				bValue = settings.value("vfs/WeighBeforeCopying").toBool();
				if (bValue != bWeighBeforeCopying)
					settings.setValue("vfs/WeighBeforeCopying", bWeighBeforeCopying);
			}
		}
		else
		if (getMode == EXTRACT_ARCHIVE) {
			bPreservePath = bParam;
			if (settings.value("archive/PreservePathWhenExtracting").isNull())
				settings.setValue("archive/PreservePathWhenExtracting", bPreservePath);
			else {
				bValue = settings.value("archive/PreservePathWhenExtracting").toBool();
				if (bValue != bPreservePath)
					settings.setValue("archive/PreservePathWhenExtracting", bPreservePath);
			}
			bWeighBeforeCopying = pDlg->m_option1chkBox->isChecked();
			if (settings.value("archive/WeighBeforeArchExtracting").isNull())
				settings.setValue("archive/WeighBeforeArchExtracting", bWeighBeforeCopying);
			else {
				bValue = settings.value("archive/WeighBeforeArchExtracting").toBool();
				if (bValue != bWeighBeforeCopying)
					settings.setValue("archive/WeighBeforeArchExtracting", bWeighBeforeCopying);
			}
		}

		QStringList slItemsLst;
		if (bMakeFileOrDir || bPatternSelectOrUnselect)
			slItemsLst = sItemList.split("||");
		else {
			if (getMode != ADD_TO_ARCH && getMode != MOVE_TO_ARCH)
				slItemsLst = sItemList.split("|");
		}

		if (slItemsLst.indexOf(sReturnStr) == -1 && getMode != PUT_RESULT_TO_PANEL) {
			if (! sItemList.isEmpty()) {
				if (bMakeFileOrDir || bPatternSelectOrUnselect)
					sItemList += "||";
				else
				if (getMode != ADD_TO_ARCH && getMode != MOVE_TO_ARCH)
					sItemList += "|";
			}
			sItemList += sReturnStr;
			if (! sConfigFileKeyList.isEmpty())
				settings.setValue(sSection+sConfigFileKeyList, sItemList);
		}
		if (bPatternSelectOrUnselect || bCopyOrMove) {
			int lastId = pDlg->m_inputComboBox->currentIndex();
			if (initLastId != lastId && ! sConfigFileKeyLastId.isEmpty())
				settings.setValue(sSection+sConfigFileKeyLastId, lastId);
		}
		if (getMode == ADD_TO_ARCH || getMode == MOVE_TO_ARCH) {
			bCreateArchiveInCurrentPath = pDlg->m_option1chkBox->isChecked();
			if (settings.value("archive/CreateArchiveInCurrentPath").isNull())
				settings.setValue("archive/CreateArchiveInCurrentPath", bCreateArchiveInCurrentPath);
			else {
				bValue = settings.value("archive/CreateArchiveInCurrentPath").toBool();
				if (bValue != bCreateArchiveInCurrentPath)
					settings.setValue("archive/CreateArchiveInCurrentPath", bCreateArchiveInCurrentPath);
			}
			bAlwaysOverwrite = pDlg->m_option2chkBox->isChecked();
			if (settings.value("archive/AlwaysOverwriteWithNewArchive").isNull())
				settings.setValue("archive/AlwaysOverwriteWithNewArchive", bAlwaysOverwrite);
			else {
				bValue = settings.value("archive/AlwaysOverwriteWithNewArchive").toBool();
				if (bValue != bAlwaysOverwrite)
					settings.setValue("archive/AlwaysOverwriteWithNewArchive", bAlwaysOverwrite);
			}
			bRememberLastUsedArchiveType = pDlg->m_option3chkBox->isChecked();
			if (settings.value("archive/RememberLastUsedArchiveType").isNull())
				settings.setValue("archive/RememberLastUsedArchiveType", bRememberLastUsedArchiveType);
			else {
				bValue = settings.value("archive/RememberLastUsedArchiveType").toBool();
				if (bValue != bRememberLastUsedArchiveType)
					settings.setValue("archive/RememberLastUsedArchiveType", bRememberLastUsedArchiveType);
			}
			bWeighBeforeArchiving = pDlg->m_option4chkBox->isChecked();
			if (settings.value("vfs/WeighBeforeArchiving").isNull())
				settings.setValue("vfs/WeighBeforeArchiving", bWeighBeforeArchiving);
			else {
				bValue = settings.value("vfs/WeighBeforeArchiving").toBool();
				if (bValue != bWeighBeforeArchiving)
					settings.setValue("vfs/WeighBeforeArchiving", bWeighBeforeArchiving);
			}
			if (bRememberLastUsedArchiveType && ! slItemsList.isEmpty())
				settings.setValue("archive/LastUsedArchiveType", pDlg->getCheckedRadioButtonLabel());
			if (! slItemsList.isEmpty())
				sReturnStr += "||" + pDlg->getCheckedRadioButtonLabel();
		}
	} // OK pressed

	return sReturnStr;
}

void InputTextDialog::updateExtentionsInTargetArchivesNames( const QString &sExt )
{
	QString sPath = m_inputComboBox->lineEdit()->text();
	if (m_sRecentlyUseExt != sExt)
		sPath.replace("."+m_sRecentlyUseExt, "");
	m_sRecentlyUseExt = sExt;

	int nSepId = 0;
	while (true) {
		nSepId = sPath.indexOf('|', nSepId);
		if (nSepId < 0)
			break;
		sPath.insert(nSepId, "."+sExt);
		nSepId += sExt.length()+2;
	}

	sPath += "."+sExt;
	m_inputComboBox->lineEdit()->setText(sPath);
}

QString InputTextDialog::getCheckedRadioButtonLabel() const
{
	QRadioButton *rb = NULL;
	foreach(rb, m_radioButtonLst) {
		if (rb->isChecked())
			break;
	}
	return rb->objectName(); // Qt-5.9: rb->text() returns tar.&xz for "tar.xz" string stored by setText (every str.str is replaced by str.&str)
}


void InputTextDialog::slotChooseDir()
{
	QString sSelectedPath = QFileDialog::getExistingDirectory(this, tr("Open directory..."), QDir::homePath());
	if (! sSelectedPath.isEmpty())
		m_inputDlg->m_inputComboBox->setEditText(sSelectedPath);
}

void InputTextDialog::slotRadioButtonClicked()
{
	QString sSenderName = (sender() != NULL) ? sender()->objectName() : "?";
// 	qDebug() << "InputTextDialog::slotRadioButtonClicked. objectName:" << sSenderName;

	if (m_pLastClickedRB->text() == sSenderName)
		return;
	m_pLastClickedRB = dynamic_cast <QRadioButton *> (sender());

	updateExtentionsInTargetArchivesNames(sSenderName);
}

void InputTextDialog::slotUpdatePathForCreateArchiveInCurrentPath()
{
	if (! m_bCreateAnArchiveMode)
		return;

	QString sTargetPath = m_inputDlg->m_inputComboBox->lineEdit()->text();
	if (m_inputDlg->m_option1chkBox->isChecked()) {
		sTargetPath.replace(m_sTargetPath, ".");
	}
	else {
		if (sTargetPath.startsWith("./")) {
			sTargetPath.remove(0,1);
			sTargetPath.prepend(m_sTargetPath);
		}
	}
	m_inputDlg->m_inputComboBox->setEditText(sTargetPath);
}

