/***************************************************************************
 *   Copyright (C) 2008 by Piotr Mierzwi?ski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QtGui>
#include <QLineEdit>
#include <QHeaderView>
#include <QColorDialog>
#include <QButtonGroup>

#include "listwidgetview.h"
#include "listwidgetbutton.h"

#include "comboboxdata.h"
#include "comboboxdelegate.h"
#include "shortkeyeditboxdata.h"
#include "shortkeyeditboxdelegate.h"


ListWidgetView::ListWidgetView( QWidget *pParent ) : QTableWidget(pParent)
	, m_pParent(pParent), m_nColumnCount(0), m_nRowCount(0)
{
	verticalHeader()->setVisible(false);

	setSelectionMode(QAbstractItemView::SingleSelection);
	setSelectionBehavior(QAbstractItemView::SelectRows);
	setAlternatingRowColors(true);

	setShowGrid(false);
}


void ListWidgetView::addColumn( Widget eWidget, const QString &sName, Qt::AlignmentFlag eColumnAlign )
{
	m_nColumnCount++;
	m_eRowWidgetList << eWidget;
	m_slHeaderLabels << sName;
	m_eColumnAlignList << eColumnAlign;
	// DO NOT TO CHANGE ORDER
	setColumnCount(m_nColumnCount);
	setHorizontalHeaderLabels(m_slHeaderLabels);
}

void ListWidgetView::addRow( bool bButtonsExclusive )
{
	insertRow(m_nRowCount);

	QButtonGroup *pButtonGrp = nullptr;
	QTableWidgetItem *pItem = nullptr;
	QStringList slEmptyList;

	for (int nCol=0; nCol < m_nColumnCount; nCol++) {
		m_pTableWidgetItemList << (pItem = new QTableWidgetItem);
/*		if (nCol == 0) {
			QFont cellFont(pItem->font());
			//setRowHeight(m_nRowCount, cellFont.pixelSize()+4);
			setRowHeight(m_nRowCount, cellFont.pointSize()*2); // 9*3
		} */
		setItem(m_nRowCount, nCol, pItem);
		ListWidgetButton *pListWidgetButton = nullptr;
		Qt::AlignmentFlag alignment = m_eColumnAlignList[nCol];

		switch (columnWidget(nCol)) {
			case TextLabel: {
				pItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
  				//pItem->setTextAlignment(Qt::AlignVCenter | alignment); // no need if default is AlignLeft
				break;
			}
			case ComboBox: {
// 				pItem->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter); // done in setCellValue()
				if (m_nRowCount == 0) // delegating need to perform only one time
					setItemDelegateForColumn(nCol, new ComboBoxDelegate);
				//pItem->setData(Qt::DisplayRole, QVariant::fromValue(ComboBoxData(slEmptyList)));
				//pItem->data(Qt::DisplayRole).value<ComboBoxData>().setTextAlignment(m_eColumnAlignList[nCol]);
 				//pItem->data(Qt::DisplayRole).value<ComboBoxData>().setTextAlign(Qt::AlignHCenter);
				break;
			}
			case EditBox: {
				break;
			}
			case KeyEditBox: {
				if (m_nRowCount == 0) // delegating need to perform only one time
					setItemDelegateForColumn(nCol, new ShortKeyEditBoxDelegate);
				break;
			}
			case RadioButton: {
				if (pButtonGrp == nullptr && bButtonsExclusive) { // make one per one row
					pButtonGrp = new QButtonGroup(this);
					pButtonGrp->setExclusive(true);
				}
				pListWidgetButton = new ListWidgetButton(pItem, ListWidgetButton::RadioButton, alignment);
				setCellWidget(m_nRowCount, nCol, pListWidgetButton);
				if (bButtonsExclusive)
					pButtonGrp->addButton(pListWidgetButton->radioButton());
				//setItemDelegateForColumn(nCol, new RadioButtonDelegate); // would be useful for implement, but inmplemented workaround with QLayout
				break;
			}
			case CheckBox: {
				pListWidgetButton = new ListWidgetButton(pItem, ListWidgetButton::CheckBox, alignment);
				setCellWidget(m_nRowCount, nCol, pListWidgetButton);
				//setItemDelegateForColumn(nCol, new CheckBoxDelegate); // would be useful for implement, but inmplemented workaround with QLayout
				break;
			}
			case PushButton:
			case ColorChooser: {
				pListWidgetButton = new ListWidgetButton(pItem, ListWidgetButton::PushButton, alignment);
				setCellWidget(m_nRowCount, nCol, pListWidgetButton);
				break;
			}
			default:
				break;
		}
		if (columnWidget(nCol) == ColorChooser || columnWidget(nCol) == CheckBox || columnWidget(nCol) == RadioButton || columnWidget(nCol) == PushButton)
			connect(pListWidgetButton, &ListWidgetButton::clicked, this, &ListWidgetView::slotButtonClicked);
	}

	m_nRowCount++;
}

// ButtonDelegate
/*
class AlignDelegate(QtWidgets.QStyledItemDelegate):
    def initStyleOption(self, option, index):
        super(AlignDelegate, self).initStyleOption(option, index)
        option.displayAlignment = QtCore.Qt.AlignCenter
*/

void ListWidgetView::setCellValue( int nRow, int nColumn, const QStringList &slValue, const QString &sBtnLabel, int nInitId )
{
	if (nRow > rowCount()) {
		qDebug() << "ListWidgetView::setCellValue: Row Id over the range!";
		return;
	}
	if (nColumn > columnCount()) {
		qDebug() << "ListWidgetView::setCellValue: Column Id over the range!";
		return;
	}
	if (item(nRow, nColumn) == nullptr) {
		qDebug() << "ListWidgetView::setCellValue: Not found item on given position!";
		return;
	}

	Widget eWidget = columnWidget(nColumn);
	QString sValue;
	if (eWidget != ComboBox)
		sValue = slValue[0];

	switch (eWidget) {
		case EditBox:
		case TextLabel: {
				item(nRow, nColumn)->setData(Qt::DisplayRole, QVariant::fromValue(sValue));
			break;
		}
		case ComboBox: {
			if (nInitId < 0)
				nInitId = 0;
			else if (nInitId > slValue.count())
				nInitId = slValue.count();
			item(nRow, nColumn)->setData(Qt::DisplayRole, QVariant::fromValue(ComboBoxData(slValue, nInitId, Qt::AlignHCenter))); // works
			//pItem->data(Qt::DisplayRole).value<ComboBoxData>().setTextAlignment(m_eColumnAlignList[nCol]);
			break;
		}
		case KeyEditBox: {
			item(nRow, nColumn)->setData(Qt::DisplayRole, QVariant::fromValue(ShortKeyEditBoxData(sValue)));
			break;
		}
		case RadioButton: {
			ListWidgetButton *pButton = dynamic_cast<ListWidgetButton *>(cellWidget(nRow, nColumn));
			if (pButton != nullptr) {
				pButton->radioButton()->setChecked(getBoolValue(sValue));
				if (! sBtnLabel.isEmpty())
					pButton->setText(sBtnLabel);
			}
			break;
		}
		case CheckBox: {
			ListWidgetButton *pButton = dynamic_cast<ListWidgetButton *>(cellWidget(nRow, nColumn));
			if (pButton != nullptr)
				pButton->checkBox()->setChecked(getBoolValue(sValue));
			break;
		}
		case PushButton: {
			ListWidgetButton *pButton = dynamic_cast<ListWidgetButton *>(cellWidget(nRow, nColumn));
			if (pButton != nullptr) {
				pButton->setText(sValue);
				if (! sBtnLabel.isEmpty())
					pButton->setText(sBtnLabel);
			}
			break;
		}
		case ColorChooser: {
			ListWidgetButton *pButton = dynamic_cast<ListWidgetButton *>(cellWidget(nRow, nColumn));
			if (pButton != nullptr)
				pButton->setColor(QColor(sValue));
			break;
		}
		default:
			break;
	}
}

QString ListWidgetView::cellValue( int nRow, int nColumn, bool bUseLabel ) const
{
	if (nRow > rowCount()) {
		qDebug() << "ListWidgetView::cellValue: Row Id over the range!";
		return QString("");
	}
	if (nColumn > columnCount()) {
		qDebug() << "ListWidgetView::cellValue: Column Id over the range!";
		return QString("");
	}
	int nRowCell    = (nRow < 0)    ? currentRow()    : nRow;
	int nColumnCell = (nColumn < 0) ? currentColumn() : nColumn;

	Widget eWidget = columnWidget(nColumnCell);
	QString sValue;

	switch (eWidget)
	{
		case EditBox:
		case TextLabel: {
			sValue = item(nRowCell, nColumnCell)->text();
			break;
		}
		case ComboBox: {
			sValue = item(nRowCell, nColumnCell)->data(Qt::DisplayRole).value<ComboBoxData>().text();
			break;
		}
		case KeyEditBox: {
			sValue = item(nRowCell, nColumnCell)->data(Qt::DisplayRole).value<ShortKeyEditBoxData>().text();
			break;
		}
		case RadioButton: {
			ListWidgetButton *pButton = dynamic_cast<ListWidgetButton *>(cellWidget(nRow, nColumn));
			if (pButton != nullptr) {
				if (bUseLabel)
					sValue = pButton->text();
				else
					sValue = (pButton->radioButton()->isChecked()) ? "true" : "false";
			}
			break;
		}
		case CheckBox: {
			ListWidgetButton *pButton = dynamic_cast<ListWidgetButton *>(cellWidget(nRow, nColumn));
			if (pButton != nullptr) {
				if (bUseLabel)
					sValue = pButton->text();
				else
					sValue = (pButton->checkBox()->isChecked()) ? "true" : "false";
			}
			break;
		}
		case PushButton:
		case ColorChooser: {
			ListWidgetButton *pButton = dynamic_cast<ListWidgetButton *>(cellWidget(nRow, nColumn));
			if (pButton != nullptr)
				sValue = pButton->color().name();
			break;
		}
		default:
			break;
	}

	return sValue;
}

bool ListWidgetView::getBoolValue( const QString &sValue )
{
	return (sValue.toUpper() == "TRUE");
}


void ListWidgetView::setCellAttributes( int nRow, int nColumn, const QColor &color, TextStyle eTextStyle )
{
	if (nRow > rowCount()) {
		qDebug() << "ListWidgetView::setCellAttributes: Row Id over the range!";
		return;
	}
	if (nColumn > columnCount()) {
		qDebug() << "ListWidgetView::setCellAttributes: Column Id over the range!";
		return;
	}
	QTableWidgetItem *pItem = item(nRow, nColumn);

	QFont cellFont(pItem->font());
	if (eTextStyle == ListWidgetView::Bold) {
		cellFont.setItalic(false);
		cellFont.setBold(true);
	}
	else
	if (eTextStyle == ListWidgetView::Italic) {
		cellFont.setItalic(true);
		cellFont.setBold(false);
	}
	else
	if (eTextStyle == (ListWidgetView::Bold | ListWidgetView::Italic)) {
		cellFont.setItalic(true);
		cellFont.setBold(true);
	}
	else
	if (eTextStyle == ListWidgetView::Regular) {
		cellFont.setItalic(false);
		cellFont.setBold(false);
	}

	//font = pItem->font();
	if (columnWidget(nColumn) != ComboBox) {
		pItem->setForeground(color);
		pItem->setFont(cellFont);
	}
	else {
		item(nRow, nColumn)->data(Qt::DisplayRole).value<ComboBoxData>().setTextColor(QColor("blue")); // DOESN'T WORK! Probably because empty constructor of ComboBoxData does nothing
		//paint(QPainter * pPainter, const QRect & rect, const QPalette & /*palette*/)
		//item(nRow, nColumn)->data(Qt::DisplayRole).value<ComboBoxData>().repaint();
	}
}

void ListWidgetView::setCellAttributes( int nRow, int nColumn, TextStyle eTextStyle )
{
	setCellAttributes(nRow, nColumn, item(nRow, nColumn)->foreground().color(), eTextStyle);
}


void ListWidgetView::setEnableCell( int nRow, int nColumn, bool bEnable, bool bEnableWidget )
{
	if (nRow > rowCount()) {
		qDebug() << "ListWidgetView::setEnableCell: Row Id over the range!";
		return;
	}
	if (nColumn > columnCount()) {
		qDebug() << "ListWidgetView::setEnableCell: Column Id over the range!";
		return;
	}
	if (! bEnable)
		bEnableWidget = false;

	QPalette sysPalette = palette();
	QColor systemBgCol = sysPalette.window().color();
	QColor textColor = (systemBgCol.lightness() > 150) ? Qt::black : Qt::white;
	QTableWidgetItem *pItem = item(nRow, nColumn);
	pItem->setForeground(bEnable ? textColor : QColor("gray"));
	pItem->setFlags(bEnable ? (Qt::ItemIsEnabled | Qt::ItemIsSelectable | (bEnableWidget ? Qt::ItemIsEditable : Qt::NoItemFlags)) : Qt::NoItemFlags);

	ListWidgetButton *pButton = dynamic_cast<ListWidgetButton *>(cellWidget(nRow, nColumn));
	if (pButton != nullptr)
		pButton->setEnabled(bEnableWidget);
}

bool ListWidgetView::cellEnable( int nRow, int nColumn ) const
{
	if (nRow > rowCount()) {
		qDebug() << "ListWidgetView::cellEnable: Row Id over the range!";
		return false;
	}
	if (nColumn > columnCount()) {
		qDebug() << "ListWidgetView::cellEnable: Column Id ver the range!";
		return false;
	}
	return (item(nRow, nColumn)->flags() != Qt::NoItemFlags);
}


void ListWidgetView::setEditableCell( int nRow, int nColumn, bool bEditable )
{
	if (nRow > rowCount()) {
		qDebug() << "ListWidgetView::setEditableCell: Row Id over the range!";
		return;
	}
	if (nColumn > columnCount()) {
		qDebug() << "ListWidgetView::setEditableCell: Column Id over the range!";
		return;
	}
	QTableWidgetItem *pItem = item(nRow, nColumn);

	if (bEditable) {
		if (! editableCell(nRow, nColumn))
			pItem->setFlags(pItem->flags() | Qt::ItemIsEditable);
	}
	else {
		if (editableCell(nRow, nColumn))
			pItem->setFlags((Qt::ItemFlags)(pItem->flags() - Qt::ItemIsEditable));
	}
}

bool ListWidgetView::editableCell( int nRow, int nColumn ) const
{
	if (nRow > rowCount()) {
		qDebug() << "ListWidgetView::editableCell: Row Id over the range!";
		return false;
	}
	if (nColumn > columnCount()) {
		qDebug() << "ListWidgetView::editableCell: Column Id over the range!";
		return false;
	}
	return (item(nRow, nColumn)->flags() & Qt::ItemIsEditable);
}


int ListWidgetView::findItem( const QString &sText, int nColumn )
{
	if (rowCount() < 1)
		return -1;
	if (columnWidget(nColumn) == CheckBox || columnWidget(nColumn) == RadioButton || columnWidget(nColumn) == ColorChooser || columnWidget(nColumn) == PushButton)
		return -1;

	if (nColumn < 0)
		nColumn = currentColumn();

	int nRow = -1;
	for (nRow=0; nRow<rowCount(); nRow++) {
		if (item(nRow, nColumn)->text() == sText)
			break;
	}

	return nRow;
}


void ListWidgetView::slotButtonClicked()
{
	ListWidgetButton *pButton = qobject_cast<ListWidgetButton *>(sender());
	if (pButton == nullptr) {
		return;
	}
	int nColumn = pButton->tableWidgetItem()->column();

	if (columnWidget(nColumn) == ColorChooser && pButton->button() == ListWidgetButton::PushButton) {
		int nRow = pButton->tableWidgetItem()->row();
		QColor color = QColorDialog::getColor(pButton->color());
		if (color.isValid()) {
			setCellValue(nRow, nColumn, QStringList(color.name()));
			emit colorChanged(item(nRow, nColumn));
		}
	}
	else
		emit buttonClicked(pButton->tableWidgetItem());
}

void ListWidgetView::removeRows()
{
	if (rowCount() > 0) {
		m_nRowCount = 0;
		clearContents();
		setRowCount(0);
	}
}

void ListWidgetView::commitData( QWidget *pEditor )
{
	int nColumn = currentColumn();

	if (columnWidget(nColumn) == EditBox || columnWidget(nColumn) == KeyEditBox) {
		QString sNewText = dynamic_cast<QLineEdit *>(pEditor)->text();
		if (columnWidget(nColumn) == KeyEditBox) { // commit
			if (! sNewText.endsWith('+') || sNewText == "+") // workaround for Tab key
				setCellValue(currentRow(), nColumn, sNewText);
		}
		emit editBoxTextApplied(item(currentRow(), nColumn), sNewText);
	}
	else
		QTableWidget::commitData(pEditor);
}
