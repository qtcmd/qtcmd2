/***************************************************************************
 *   Copyright (C) 2007 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _FONTCHOOSERPAGE_H
#define _FONTCHOOSERPAGE_H

#include <QFontDatabase>

#include "ui_fontchooserpagedlg.h"

/**
	@author Piotr Mierzwi�ski <piom@nes.pl>
*/
class FontChooserPage : public QWidget, Ui::FontChooserPageDlg
{
	Q_OBJECT
private:
	QFontDatabase m_fdFontDataBase;

	QStringList m_slFamilyName;

	QString m_sStyle;
	QString m_sCharSet;
	QString m_sDefaultFont, m_sOriginalFont;

public:
	enum ConfType {
		FILE_LIST_VIEW=0,
		PREVIEW_FILE
	};

	FontChooserPage( FontChooserPage::ConfType confType );

	QString fontFamily() const {
		return m_pFontListWidget->currentItem()->text();
	}
	bool fontItalic() const {
		return m_pItalicChkBox->isChecked();
	}
	bool fontBold() const {
		return m_pBoldChkBox->isChecked();
	}
	uint fontSize() const {
		return m_pFontSizeListWidget->currentItem()->text().toInt();
	}

	void init();
	void setPreviewFont( const QString &sFamily, int nSize );

	/**Set defauls settings, so defaul font.
	 */
	void setDefaults();

private:
	FontChooserPage::ConfType m_eConfType;

	void updateFontFamilyList();
	void updateSizeList();

private Q_SLOTS:
	void slotFamilySelectionChanged();
	void slotFontSizeHighlighted( int nId );
	void slotSetFontAttribut();

	void slotSetOriginalFont();

public Q_SLOTS:
	void slotSave();


};

#endif // _FONTCHOOSERPAGE_H
