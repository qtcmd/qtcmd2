/***************************************************************************
 *   Copyright (C) 2008 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                 2018 port to Qt5 (same author)                          *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
//#include <QtGui> // for qDebug

#include "comboboxdata.h"
#include "comboboxeditor.h"
#include "comboboxdelegate.h"


QWidget *ComboBoxDelegate::createEditor( QWidget *pParent, const QStyleOptionViewItem &option, const QModelIndex &index ) const
{
	QVariant variant = index.data();
	if (variant.canConvert<ComboBoxData>()) {
		ComboBoxEditor *pEditor = new ComboBoxEditor(pParent);
		connect(pEditor, static_cast<void(ComboBoxEditor::*)(int)>(&ComboBoxEditor::activated), this, &ComboBoxDelegate::slotCommitAndCloseEditor);
		return pEditor;
	}

	return QItemDelegate::createEditor(pParent, option, index);
}

void ComboBoxDelegate::setEditorData( QWidget *pEditor, const QModelIndex &index ) const
{
	QVariant variant = index.data();
	if (variant.canConvert<ComboBoxData>()) {
		ComboBoxData comboBoxData = variant.value<ComboBoxData>();
		ComboBoxEditor *pComboBoxEditor = qobject_cast<ComboBoxEditor *>(pEditor);
		pComboBoxEditor->setComboBoxData(comboBoxData);
		pComboBoxEditor->addItems(comboBoxData.items()); // update data
		//pComboBoxEditor->showPopup();  // shows popup, but instantly hides it (item is activated :/)
	}
	else {
		QItemDelegate::setEditorData(pEditor, index);
	}
}

void ComboBoxDelegate::setModelData( QWidget *pEditor, QAbstractItemModel *pModel, const QModelIndex &index ) const
{
	QVariant variant = index.data();
	if (variant.canConvert<ComboBoxData>()) {
		ComboBoxEditor *pComboBoxEditor = qobject_cast<ComboBoxEditor *>(pEditor);
		pModel->setData(index, QVariant::fromValue(pComboBoxEditor->comboBoxData()));
		//pModel->setData(index, QVariant::fromValue(pComboBoxEditor->comboBoxData().text())); // changes dataModel and trigger calls lineEdit
	}
	else {
		QItemDelegate::setModelData(pEditor, pModel, index);
	}
}

void ComboBoxDelegate::updateEditorGeometry( QWidget *pEditor,
											const QStyleOptionViewItem &option, const QModelIndex &/*index */ ) const
{
	pEditor->setGeometry(option.rect);
}


void ComboBoxDelegate::paint( QPainter *pPainter, const QStyleOptionViewItem &option, const QModelIndex &index ) const
{
	QVariant variant = index.data();
	if (variant.canConvert<ComboBoxData>()) {
		ComboBoxData comboBoxData = variant.value<ComboBoxData>();

		if (option.state & QStyle::State_Selected) {
			pPainter->fillRect(option.rect, option.palette.highlight());
			pPainter->setPen(option.palette.highlightedText().color());
		}
		else
			pPainter->setPen(option.palette.text().color());

		//qDebug() << "ComboBoxDelegate::paint, comboBoxData.textAlignment()=" << comboBoxData.textAlignment();
		//comboBoxData.setPainter(pPainter, option.rect, option.palette);
		comboBoxData.paint(pPainter, option.rect, option.palette);
	}
	else {
		QItemDelegate::paint(pPainter, option, index);
	}
}


void ComboBoxDelegate::slotCommitAndCloseEditor( int /*nCurrentItem*/ )
{
	ComboBoxEditor *pEditor = qobject_cast<ComboBoxEditor *>(sender());
	emit commitData (pEditor);
	emit closeEditor(pEditor);
}


