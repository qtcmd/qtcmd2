/***************************************************************************
 *   Copyright (C) 2012 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef INPUTTEXTDIALOG_H
#define INPUTTEXTDIALOG_H

#include <QRadioButton>

#include "ui_inputtextdlg.h"


/**
 * @author Piotr Mierzwiński
 */
class InputTextDialog : public QDialog, Ui::InputTextDlg
{
	Q_OBJECT
public:
	InputTextDialog();

	enum GetTextMode {
		MAKE_FILTER=0,
		PATTERN_SELECT,
		PATTERN_UNSELECT,
		MAKE_DIR,
		MAKE_FILE,
		MAKE_LINK,
		EDIT_LINK,
		COPY,
		MOVE,
		DELETE,
		UPLOAD,
		ADD_TO_ARCH,
		MOVE_TO_ARCH,
		CREATE_ARCH,
		EXTRACT_ARCHIVE,
		EXTRACT_FILES,
		RENAME_IN_OVR,
		PUT_RESULT_TO_PANEL,
		GO_TO_LINE,
		BOOKMARK_NAME
	};

	static QString getText( QWidget *pParent, GetTextMode getMode, const QString &sInInitText, bool &bParam, const QString &sInLabel=QString(), const QStringList &slItemsList=QStringList(), const QString &sInWndTitle=QString() );

private:
	InputTextDialog *m_inputDlg;
	// memebers related to createAnArchive
	QList <QRadioButton *> m_radioButtonLst;
	QRadioButton *m_pLastClickedRB;
	QString m_sRecentlyUseExt, m_sTargetPath;
	bool m_bCreateAnArchiveMode;

	QString getCheckedRadioButtonLabel() const;
	void    updateExtentionsInTargetArchivesNames( const QString &sExt );

private slots:
	void slotChooseDir();
	void slotUpdatePathForCreateArchiveInCurrentPath();
	void slotRadioButtonClicked();

};

#endif // INPUTTEXTDIALOG_H
