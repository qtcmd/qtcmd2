/***************************************************************************
 *   Copyright (C) 2005 by Piotr Mierzwiñski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _MEDIACONTROLWIDGET_
#define _MEDIACONTROLWIDGET_

#include "ui_mediacontrolwidgetgui.h"
#include "iconcache.h"

#include <QWidget>
#include <phonon/MediaObject>
#include <phonon/AudioOutput>

/**
	@author Piotr Mierzwiñski
*/
class MediaControlWidget : public QWidget, Ui::MediaControlWidgetGui
{
	Q_OBJECT
public:
	MediaControlWidget( QWidget *pParent );
	~MediaControlWidget() {}

	void init();

	void updateButtonsState( bool bPlayEnable, bool bPauseEnable, bool bStopEnable, bool bSeekForward, bool bSeekBackward );

	void setSeekSlider( Phonon::MediaObject *pMediaObj ) {
		m_pSeekSlider->setMediaObject(pMediaObj);
	}
	void setVolumeOutput( Phonon::AudioOutput *pAudioOutput ) {
		m_pAudioOutput = pAudioOutput;
	}
	void updateVolumeSpin( int value ) {
		m_pSpinBoxVolume->setValue(value);
	}

	void setVolumeSlider( Phonon::AudioOutput *pAudioOutput );

	void updateCurrentTime( const QString &sTime );
	void setTotalTime( const QString &sTime );


	enum ControlCmd {
		MEDIA_PLAY=0,
		MEDIA_PAUSE,
		MEDIA_STOP,
		MEDIA_FORWARD,
		MEDIA_BACKWARD
	};

private:
	IconCache *m_pIconCache;
	Phonon::AudioOutput *m_pAudioOutput;

	QIcon icon( const QString &sIconName ) const {
		return m_pIconCache->icon(sIconName);
	}

private slots:
	void slotPlay() {
		emit signalControl(MEDIA_PLAY);
	}
	void slotStop() {
		emit signalControl(MEDIA_STOP);
	}
	void slotPause() {
		emit signalControl(MEDIA_PAUSE);
	}
	void slotForward() {
		emit signalControl(MEDIA_FORWARD);
	}
	void slotBackward() {
		emit signalControl(MEDIA_BACKWARD);
	}

	void slotVolumeChanged( int nValue );

signals:
	void signalControl( MediaControlWidget::ControlCmd eControlCmd );

};

#endif // MEDIACONTROLWIDGET
