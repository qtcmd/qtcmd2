/***************************************************************************
 *   Copyright (C) 2008 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef _SHORTKEYEDITBOXEDITOR_H
#define _SHORTKEYEDITBOXEDITOR_H

#include <QMap>
#include <QLineEdit>

#include "shortkeyeditboxdata.h"

/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class ShortKeyEditBoxEditor : public QLineEdit
{
	Q_OBJECT
public:
	ShortKeyEditBoxEditor( QWidget *pParent=nullptr );

	void setShortKeyEditBoxData( const ShortKeyEditBoxData &shortKeyEditBoxData ) {
		m_ShortKeyEditBoxData = shortKeyEditBoxData;
	}
	ShortKeyEditBoxData shortKeyEditBoxData() { return m_ShortKeyEditBoxData; }

private:
	ShortKeyEditBoxData m_ShortKeyEditBoxData;

	typedef QMap<int, QString> KeysMap;
	KeysMap m_KeysStateMap;

	QString m_sModifiers;

protected:
	void keyPressEvent( QKeyEvent *pKeyEvent );

Q_SIGNALS:
	void signalDone();

};

#endif // _SHORTKEYEDITBOXEDITOR_H
