/***************************************************************************
 *   Copyright (C) 2013 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QtWidgets>
#include <QDebug>

#include "multiprogresslistview.h"
// #include "contextmenucmdids.h"


MultiProgressListView::MultiProgressListView( QWidget *pParent )
	: TreeView(pParent)
{
// 	qDebug() << "MultiProgressListView::MultiProgressListView.";
	setWindowTitle(tr("MultiProgressListView"));
	resize(400, 200);

	setItemsExpandable(false);
	setRootIsDecorated(false);
	setAlternatingRowColors(true);
	setEditTriggers(QAbstractItemView::NoEditTriggers);
	setSelectionMode(QAbstractItemView::NoSelection);
// 	setSortingEnabled(true); // default is disabled
// 	sortByColumn(0, Qt::AscendingOrder);

	m_pModel = new MultiProgressListViewModel(true);

// 	m_pProxyModel = new MultiProgressListViewProxyModel(this);
// 	m_pProxyModel->setSourceModel(m_pModel);

	m_pContextMenu = NULL;
}

MultiProgressListView::~MultiProgressListView()
{
	qDebug() << "MultiProgressListView::~MultiProgressListView.";
	delete m_pContextMenu;
// 	delete m_pProxyModel;
	delete m_pModel;
}

void MultiProgressListView::initListView( MultiProgressList *pMultiProgressList )
{
	if (pMultiProgressList == NULL)
		return;
	if (pMultiProgressList->isEmpty())
		qDebug() << "MultiProgressListView::initListView. WARNING. Initialization the model with empty list!";

	m_pModel->initModel(pMultiProgressList);

// 	setModel(m_pProxyModel);
	setModel(m_pModel);

	//tr("Name") <<  tr("Status") << tr("Progress") << tr("Size") << tr("File type");
	header()->resizeSection(header()->logicalIndex(0), width()/2); // NAME
	header()->resizeSection(header()->logicalIndex(1), width()/4); // STATUS
	header()->resizeSection(header()->logicalIndex(2), width()/4); // PROGRESS
	header()->resizeSection(header()->logicalIndex(3), width()/4); // SIZE
	header()->resizeSection(header()->logicalIndex(4), width()/2); // FILE TYPE
}

/*
void MultiProgressListView::setFirstAsCurrent()
{
	if (m_pModel->rowCount() > 0)
		setCurrentIndex(model()->index(0,0));
}

MultiProgressListViewItem * MultiProgressListView::currentProcessedItem()
{
	QModelIndex srcIndex = static_cast <const MultiProgressListViewProxyModel *>(currentIndex().model())->mapToSource(currentIndex());
	return m_model->fileInfo(srcIndex);
}
*/

/*
void MultiProgressListView::setContextMenuEnabled( bool bEnableContextMenu )
{
	if (bEnableContextMenu && m_pContextMenu != NULL) // alredy created
		return;

	if (! bEnableContextMenu && m_pContextMenu != NULL) { // created, remove it
		delete m_pContextMenu;
		m_pContextMenu = NULL;
		return;
	}

	if (bEnableContextMenu && m_pContextMenu == NULL) { // not yet created
		m_pContextMenu = new ContextMenu(this);
		connect(m_pContextMenu, &signalContextMenuAction, this, &slotContexMenuAction);
	}
}

void MultiProgressListView::setContextMenuType( ContextMenu::ContextMenuType contextMenuType )
{
	if (m_pContextMenu == NULL) {
		qDebug() << "MultiProgressListView::setContextMenuType. ContextMenu is not enabled!";
		return;
	}
	m_pContextMenu->setContextMenuType(contextMenuType);
}

void MultiProgressListView::showContextMenu( bool bOnCursorPos )
{
	if (m_pContextMenu == NULL)
		return;

	QPoint position;
	QRect vr = visualRect(currentModelIndex());

	if (bOnCursorPos)
		position = QPoint(QCursor::pos().x() - 2, QCursor::pos().y() + 2);
	else // for Key_Menu
		position = mapToGlobal(QPoint( header()->sectionSize(0), vr.y() + vr.height() + header()->height() + 1 ));

	int y = position.y(), menuHeight = m_pContextMenu->sizeHint().height();
	if (y + menuHeight > QApplication::desktop()->height()) {
		position.setY( y - menuHeight );
		if (! bOnCursorPos) // correction for Key_Menu
			position.setY(y - vr.height() - 1);
	}

// 	QModelIndex index = static_cast <const MultiProgressListViewProxyModel *>(currentModelIndex().model())->mapToSource(currentModelIndex());
// 	FileInfo *fi = m_model->fileInfo(index);

	m_pContextMenu->showMenu(position);
}

void MultiProgressListView::slotContexMenuAction( int actionId )
{
	if (     actionId == CMD_FindFile_GoToFile)
		(static_cast <MultiProgressDialog *> (m_pParent))->slotJumpToFile(QModelIndex());
}


void MultiProgressListView::mouseReleaseEvent( QMouseEvent *pME )
{
	if (pME->button() == Qt::RightButton)
		showContextMenu(true);

	return QTreeView::mouseReleaseEvent(pME);
}
*/
