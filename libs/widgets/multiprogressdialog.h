/***************************************************************************
 *   Copyright (C) 2015 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _MULTIPROGRESSDIALOG_H
#define _MULTIPROGRESSDIALOG_H

#include "ui_multiprogressdlg.h"

#include <QDialog>
#include <QCloseEvent>

#include "subsystemconst.h"
#include "multiprogresslist.h"


/**
 * @author Piotr Mierzwiński
 */
class MultiProgressDialog : public QDialog, Ui::MultiProgressDlg
{
	Q_OBJECT
public:
	MultiProgressDialog( QWidget *pParent, bool bModal=true );
	~MultiProgressDialog();

	void init( const QString &sTargetName, FileInfoToRowMap *pSelectionMap );

	Vfs::SubsystemCommand lastSubsysCmd() const { return m_lastSubsysCmd; }

	void showDialog( Vfs::SubsystemCommand eSubsysCmd, bool bCloseAfterFinished );

	bool isCloseAfterFinished() { return m_pCloseAfterFinishingChkBox->isChecked(); }

private:
	MultiProgressList *m_pMultiProgressList;

	Vfs::SubsystemCommand m_lastSubsysCmd;

	QMap <FileInfo *, MultiProgressListViewItem *> m_multiProgressMap;
	long long int m_nTotalNum;

	void setWndTitle( Vfs::SubsystemCommand eSubsysCmd );

public slots:
	void slotUpdateFileStatus( Vfs::FileInfo *pInFileInfo, const QString &sStatus );
	void slotUpdateFileProgress( Vfs::FileInfo *pInFileInfo, long long int nCurrentValue, long long int nMaxValue );
// 	void slotUpdateTotalProgress( int nDoneInPercents, long long nTotalWeight );
	void slotUpdateCounterOfProcessed( int nValue, bool bTotal );

	void slotUpdateLabelOfCancelBtnWithDone();

protected:
	void closeEvent( QCloseEvent *pCE );

signals:
	void signalCancel();

};

#endif // _MULTIPROGRESSDIALOG_H
