/***************************************************************************
 *   Copyright (C) 2013 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDebug>
#include <QToolTip>

#include "settings.h"
#include "mimetype.h"
#include "multiprogresslistviewmodel.h"

using namespace Vfs;


MultiProgressListViewModel::MultiProgressListViewModel( bool bRoundFileSize )
	: m_bBytesRound(bRoundFileSize)
{
// 	qDebug() << "MultiProgressListViewModel::MultiProgressListViewModel.";
	m_headerItems << tr("Name") <<  tr("Status") << tr("Progress") << tr("Size") << tr("File type");

	m_updateDataFlag = false;

	m_bShowIcons = true;
	m_bBoldDir = true;

	m_pIconCache = IconCache::instance();
	m_pItemsList = NULL;

	Settings settings;
	m_hiddenFgColor.setNamedColor(settings.value("filelistview/HiddenFileColor", "#A0A0A0").toString());
	m_executableFgColor.setNamedColor(settings.value("filelistview/ExecutableFileColor", "#008000").toString());
	m_symlinkColor.setNamedColor(settings.value("filelistview/SymlinkColor", "#808080").toString());
	m_brokenSymlinkColor.setNamedColor(settings.value("filelistview/BrokenSymlinkColor", "#800000").toString());
}

MultiProgressListViewModel::~MultiProgressListViewModel()
{
	qDebug() << "MultiProgressListViewModel::~MultiProgressListViewModel.";
// 	delete m_pItemsList; // removed in main parent class
}
/*
virtual Qt::ItemFlags flags( const QModelIndex &index ) const;
Qt::ItemFlags MultiProgressListViewModel::flags( const QModelIndex &index ) const
{
	if (! index.isValid())
		return 0;

	return Qt::ItemIsEditable | Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}*/

QVariant MultiProgressListViewModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
	if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
		return m_headerItems.at(section);

	return QAbstractItemModel::headerData(section, orientation, role);
}

QModelIndex MultiProgressListViewModel::parent( const QModelIndex &/*index*/ ) const
{
	return QModelIndex();
}

int MultiProgressListViewModel::columnCount( const QModelIndex &parent ) const
{
	if (parent.column() > 0)
		return 0;

	return m_headerItems.size();
}

int MultiProgressListViewModel::rowCount( const QModelIndex &parent ) const
{
	if (parent.column() > 0)
		return 0;

	return m_pItemsList->count();
}


MultiProgressListViewItem * MultiProgressListViewModel::item( const QModelIndex &index ) const
{
	Q_ASSERT(index.isValid());
	return m_pItemsList->at(index.row()); // only after first sorting (QDir obj) work correct!!!
}


QVariant MultiProgressListViewModel::data( const QModelIndex &index, int role ) const
{
	// Qt4 engine call this every time when (mouse) cursor is over the item's list!
	if (! index.isValid())
		return QVariant();
// 	qDebug() << "MultiProgressListViewModel::data";
	if (role != Qt::ToolTipRole)
		QToolTip::hideText();

	int idxColumn = index.column();
	MultiProgressListViewItem *pPI = item(index);
	if (pPI == NULL)
		return QVariant();

	if (role == Qt::DisplayRole || role == Qt::EditRole || role == Qt::ToolTipRole) {
		switch (idxColumn) {
			case 0: return pPI->fileInfo->fileName();
			case 1: return pPI->status;
			case 2: return QString("%1 %").arg(pPI->progress);
			case 3: {
				if      (pPI->fileInfo->isDir())
					return pPI->fileInfo->dirLabel();
				else if (pPI->fileInfo->isSymLink())
					return (m_bBytesRound) ? FileInfo::bytesRound(pPI->fileInfo->symLinkTarget().size()) : QString::number(pPI->fileInfo->symLinkTarget().size());
				else
					return (m_bBytesRound) ? FileInfo::bytesRound(pPI->fileInfo->size())                 : QString::number(pPI->fileInfo->size());
			}
			case 4: {
				return pPI->fileInfo->mimeInfo();
			}
			default:
				break;
// 				Q_ASSERT(false);
		}
	}

	if (role == Qt::TextAlignmentRole) {
		if (idxColumn == 1 || idxColumn == 2)
			return Qt::AlignHCenter;
	}
/*	else
	if (role == Qt::ForegroundRole) {
		if (pPI->fileInfo->isHidden())
			return m_hiddenFgColor;
		if (pPI->fileInfo->isSymLink() && pPI->fileInfo->mimeInfo() == "symlink/broken")
			return m_brokenSymlinkColor;
		if (! pPI->fileInfo->isDir() && pPI->fileInfo->isSymLink())
			return m_symlinkColor;
		if (! pPI->fileInfo->isDir() && pPI->fileInfo->permissions().contains('x'))
			return m_executableFgColor;
	}*/
	if (idxColumn == 0) {
		if (role == Qt::DecorationRole && m_pIconCache != NULL && m_bShowIcons) {
			return m_pIconCache->icon(pPI->fileInfo->mimeInfo());
		}
		if (role == Qt::FontRole) {
			if (m_bBoldDir && pPI->fileInfo->isDir()) {
				QFont fontBold;
				fontBold.setBold(true);
				return fontBold;
			}
		}
		else
		if (role == Qt::StatusTipRole)
			return pPI->fileInfo->symLinkTarget();
	}

	return QVariant();
}

QModelIndex MultiProgressListViewModel::index( int row, int column, const QModelIndex &parent ) const
{
	if ( ! hasIndex(row, column, parent) )
		return QModelIndex();

	if (row >= 0 && row < m_pItemsList->count()) {
		return createIndex(row, column);
	}
	return QModelIndex();
}

bool MultiProgressListViewModel::hasChildren( const QModelIndex &parent ) const
{
	if (parent.isValid())
		return false;

	return true;
}


void MultiProgressListViewModel::clear()
{
	beginResetModel();
	m_pItemsList->clear();
	endResetModel();
}


bool MultiProgressListViewModel::setData( const QModelIndex &index, const QVariant &value, int role )
{
	if (role != Qt::EditRole || ! m_updateDataFlag) {
// 		m_setDataValue = value; // usable for setDataPost
// 		m_setDataIndex = index; // usable for setDataPost
		return false;
	}
	MultiProgressListViewItem *pItem = item(index);
	bool result = pItem->setData(index.column(), value);
// 	qDebug() << "VfsFlatListModel::setData" << "item" << pItem->fileInfo->fileName() << "value:" << value << objectName();
	if (result) {
// 		m_pItemsList[index.row()] = pItem;
		emit dataChanged(index, index);
		m_updateDataFlag = false;
	}

	return result;
}


void MultiProgressListViewModel::addItem( MultiProgressListViewItem *pInItem )
{
	if (pInItem == NULL)
		return;

	beginResetModel();
	m_pItemsList->append(pInItem);
	endResetModel();
}

void MultiProgressListViewModel::removeItem( MultiProgressListViewItem *pInItem )
{
	if (pInItem == NULL)
		return;

	MultiProgressListViewItem *pItemOnList;
	beginResetModel();
	for (int i = 0; i < m_pItemsList->count(); ++i) {
		pItemOnList = m_pItemsList->at(i);
		if (pItemOnList->fileInfo->absoluteFileName() == pInItem->fileInfo->absoluteFileName())
			m_pItemsList->removeAt(i);
	}
	endResetModel();
}

void MultiProgressListViewModel::collectVisibleItems( MultiProgressList *pCollectedItems )
{
	if (pCollectedItems != NULL)
		pCollectedItems->clear();
	if (pCollectedItems == NULL || m_pItemsList == NULL || m_pItemsList->count() == 0)
		return;

	for (int i = 0; i < m_pItemsList->count(); ++i) {
		pCollectedItems->append(m_pItemsList->at(i));
	}
}

// -----------------------------------------
/*
void MultiProgressListViewProxyModel::initSortKey( char *k, const MultiProgressListViewItem &fi, bool asc, int sortColumn ) const
{
	if (asc) {
		k[0] = fi.fileInfo->fileName() == ".." ? '0' : '1';
		k[1] = fi.fileInfo->isDir() ? '0' : '1';
	} else {
		k[0] = fi.fileInfo->fileName() == ".." ? '1' : '0';
		k[1] = fi.fileInfo->isDir() ? '1' : '0';
	}
	k[2] = fi.fileInfo->isHidden() ? '0' : '1';
	if ((1 == sortColumn && ! fi.fileInfo->isDir()) || 2 == sortColumn)
		k[2] = '0';
}

bool MultiProgressListViewProxyModel::lessThan( const QModelIndex &left, const QModelIndex &right ) const
{
	char lf[] = "0000";
	char rt[] = "0000";
	const MultiProgressListViewItem &leftItem  = *sourceModel()->item(left);
	const MultiProgressListViewItem &rightItem = *sourceModel()->item(right);

// 	const FileInfo &lfi = sourceModel()->fileInfo(left);
// 	const FileInfo &rfi = sourceModel()->fileInfo(right);

	bool isAscOrder = sortOrder() == Qt::AscendingOrder;
	int  col = sortColumn();
	initSortKey(lf, leftItem, isAscOrder, col);
	initSortKey(rt, rightItem, isAscOrder, col);

	QVariant leftData  = sourceModel()->data(left);
	QVariant rightData = sourceModel()->data(right);
	qint64  lfiSize = leftItem.fileInfo->isSymLink()  ? leftItem.fileInfo->symLinkTarget().length()  : leftItem.fileInfo->size();
	qint64  rfiSize = rightItem.fileInfo->isSymLink() ? rightItem.fileInfo->symLinkTarget().length() : rightItem.fileInfo->size();
	bool isLessThan = (1 == col ? lfiSize < rfiSize : QString::localeAwareCompare(leftData.toString(), rightData.toString()) < 0);
	if (isLessThan) {
		lf[3] = '0';
		rt[3] = '1';
	} else {
		lf[3] = '1';
		rt[3] = '0';
	}

	return qstrcmp(lf, rt) < 0;
}

*/
