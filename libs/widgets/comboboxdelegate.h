/***************************************************************************
 *   Copyright (C) 2008 by Piotr Mierzwi?ski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _COMBOBOXDELEGATE_H
#define _COMBOBOXDELEGATE_H

#include <QItemDelegate>

/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class ComboBoxDelegate : public QItemDelegate
{
	Q_OBJECT
public:
	ComboBoxDelegate( QObject *pParent = 0 ) : QItemDelegate(pParent) {}

	QWidget *createEditor( QWidget *pParent, const QStyleOptionViewItem &option, const QModelIndex &index ) const;

	void setEditorData( QWidget *pEditor, const QModelIndex &index ) const;
	void setModelData( QWidget *pEditor, QAbstractItemModel *pModel, const QModelIndex &index ) const;

	void updateEditorGeometry( QWidget *pEditor, const QStyleOptionViewItem &option, const QModelIndex &index ) const;

	void paint( QPainter *pPainter, const QStyleOptionViewItem &option, const QModelIndex &index ) const;


private Q_SLOTS:
	void slotCommitAndCloseEditor( int nCurrentItem );

};

#endif // _COMBOBOXDELEGATE_H
