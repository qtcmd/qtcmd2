/***************************************************************************
 *   Copyright (C) 2015 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _MULTIPROGRESSLISTVIEWMODEL_H
#define _MULTIPROGRESSLISTVIEWMODEL_H

#include <QAbstractItemModel>
#include <QSortFilterProxyModel>

#include "fileinfo.h"
#include "iconcache.h"
#include "multiprogresslist.h"

// class QIcon;
// class IconCache;

using namespace Vfs;

/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class MultiProgressListViewModel : public QAbstractItemModel // remove when will be support for TREE and FLAT list (use VfsModel)
{
//	Q_OBJECT
public:
	MultiProgressListViewModel( bool bRoundFileSize=true );
	virtual ~MultiProgressListViewModel();

	virtual QVariant    headerData( int section, Qt::Orientation orientation, int role = Qt::DisplayRole ) const;

	virtual QModelIndex parent( const QModelIndex &index ) const;

	virtual int         columnCount( const QModelIndex &parent = QModelIndex() ) const;

	virtual int         rowCount( const QModelIndex &parent = QModelIndex() ) const;

	virtual QVariant    data( const QModelIndex &index, int role = Qt::DisplayRole ) const;
	virtual bool        setData( const QModelIndex &index, const QVariant &value, int role = Qt::EditRole );

	virtual QModelIndex index( int row, int column = 0, const QModelIndex &parent = QModelIndex() ) const;

	virtual bool        hasChildren( const QModelIndex &parent = QModelIndex() ) const;


	/** Clears model (removes all items from internal list).
	 */
    void                clear();

	/** Inits model by items from given list.
	 * @param pMultiProgressList list of progress dialogs
	 */
	void                initModel( MultiProgressList *pMultiProgressList ) { m_pItemsList = pMultiProgressList; }
// 	void                setFileInfoList( const FileInfoList &fileInfoList ) { m_fileInfoList = fileInfoList; }

	/** Adds item to model.
	 * @param pInItem item to add
	 */
	void                addItem( MultiProgressListViewItem *pInItem );

	/** Removes item from model.
	 * @param pInItem item to remove
	 */
	void                removeItem( MultiProgressListViewItem *pInItem );

	MultiProgressListViewItem *item( const QModelIndex &index ) const;

	MultiProgressList  *list() const { return m_pItemsList; }

	void                collectVisibleItems( MultiProgressList *pCollectedItems );

private:
	QList<QString>     m_headerItems;
	MultiProgressList *m_pItemsList;

	bool               m_updateDataFlag;
	QVariant           m_setDataValue;
	QModelIndex        m_setDataIndex;

	IconCache *m_pIconCache;

	bool    m_bBoldDir;
	bool    m_bShowIcons;
	bool    m_bBytesRound;

	QColor  m_hiddenFgColor;
	QColor  m_executableFgColor;
	QColor  m_symlinkColor;
	QColor  m_brokenSymlinkColor;

};


/*
class MultiProgressListViewProxyModel : public QSortFilterProxyModel
{
public:
	MultiProgressListViewProxyModel( QObject *parent = 0 ) : QSortFilterProxyModel(parent) {}

	MultiProgressListViewModel *sourceModel() const { return static_cast <MultiProgressListViewModel *> (QSortFilterProxyModel::sourceModel()); }

private:
	void initSortKey( char *k, const MultiProgressListViewItem &fi, bool asc, int sortColumn ) const;

protected:
	virtual bool lessThan ( const QModelIndex &left, const QModelIndex &right ) const;

};
*/
#endif // _MULTIPROGRESSLISTVIEWMODEL_H
