/***************************************************************************
 *   Copyright (C) 2015 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _MULTIPROGRESSLISTVIEWITEM_H
#define _MULTIPROGRESSLISTVIEWITEM_H

#include <QWidget>
#include <QVariant>

#include "fileinfo.h"

#define MAX_COLUMNS  3


class MultiProgressListViewItem
{
public:
	MultiProgressListViewItem() {}
	MultiProgressListViewItem( Vfs::FileInfo *pFileInfo, const QString &sStatus, qint16 nProgress ) :
		fileInfo(pFileInfo), status(sStatus), progress(nProgress)
	{}

	bool setData( int nColumn, const QVariant &value ) {
		if (nColumn < 1 && nColumn >= MAX_COLUMNS)
			return false;

		if (nColumn == 1)
			status = value.toString();
		else
		if (nColumn == 2)
			progress = value.toInt();

		return true;
	}

	Vfs::FileInfo *fileInfo;
	QString status;
	qint16  progress;
};

typedef QList <MultiProgressListViewItem *> MultiProgressList;


#endif // _MULTIPROGRESSLISTVIEWITEM_H
