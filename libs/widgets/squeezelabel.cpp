/***************************************************************************
 *   Copyright (C) 2003 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "squeezelabel.h"

#include <QDebug>
#include <QStringList>


SqueezeLabel::SqueezeLabel( QWidget *pParent )
	: QLabel(pParent),
	  m_bShowTip(true),
	  m_squeezingMode(SQUEEZING_LINE)
{
	setScaledContents(true);
	setTextInteractionFlags(Qt::TextSelectableByMouse);
}


void SqueezeLabel::setText( const QString &sText )
{
	if ( ! hasScaledContents() )
		QLabel::setText( sText );
	else {
		m_sFullText = sText;
		setSqueezingText( width() );
	}
}


void SqueezeLabel::setSqueezingText( uint nLabelWidth )
{
	if ( ! hasScaledContents() )
		return;

	QString sSqueezeText, sFullText = m_sFullText;

	if ( sFullText.isEmpty() ) {
		sFullText   = text();
		m_sFullText = text();
	}

	if ( sFullText.contains('\n') ) {
		QStringList textLines = sFullText.split("\n");
		for ( int i=0; i<textLines.count(); i++)
			sSqueezeText += squeezeLine( textLines[i], nLabelWidth ) + "\n";
	}
	else
		sSqueezeText = squeezeLine( sFullText, nLabelWidth );

	if ( sSqueezeText.endsWith('\n') )
		sSqueezeText.chop(1); // removes last character

	QLabel::setText( sSqueezeText );

	if ( sSqueezeText.contains("...") && sSqueezeText != "..." ) {
		if ( m_bShowTip )
			setToolTip( sFullText );
		else
			setToolTip("");
	}
	else
		setToolTip("");
}


QString SqueezeLabel::squeezeLine( const QString &sLine, uint nLabelWidthInPixels )
{
	if ( sLine.isEmpty() || sLine == "..." )
		return QString("");

	QString sNewLine = sLine;
	// - replace each occurrence of tab character with 8 space characters.
	sNewLine.replace( QRegExp("\\t"), "        " );
	// - simple rich text support
	QString sNewLineWithNoTags = sNewLine;
	sNewLineWithNoTags.remove(QRegExp("<[^>]*>"));
	int nTagsLen = (sNewLine.length() - sNewLineWithNoTags.length());

	QString leftHalfOfText, rightHalfOfText;
	uint nTextWidthInPixels = fontMetrics().horizontalAdvance( sNewLineWithNoTags, sNewLineWithNoTags.length() );
	uint nThreeDotsWidth    = fontMetrics().horizontalAdvance( "...", 3 );

	if ( nLabelWidthInPixels < nTextWidthInPixels ) {
		QString sChr;
		int i;
		uint nTxtWidth = 0;
		if (m_squeezingMode == SQUEEZING_LINE) {
			// text measuring
			for (i=0; i< sNewLine.length(); i++) {
				sChr = sNewLine[i];
				nTxtWidth += fontMetrics().horizontalAdvance(sChr, 1);
				if (nTxtWidth+nThreeDotsWidth+nThreeDotsWidth > nLabelWidthInPixels)
					break;
			}
			leftHalfOfText  = sNewLine.left( (i/2) + nTagsLen );
			rightHalfOfText = sNewLine.right( (i/2) + nTagsLen );
			sNewLine = leftHalfOfText + "..." + rightHalfOfText;
		}
		else if (m_squeezingMode == WRAP_LINE) {
			for (i=0; i< sNewLineWithNoTags.length(); i++) {
				sChr = sNewLine[i];
				nTxtWidth += fontMetrics().horizontalAdvance(sChr, 1);
				if (nTxtWidth > nLabelWidthInPixels)
					break;
			}
			int rightLen = sNewLineWithNoTags.length() - i;
			leftHalfOfText  = sNewLine.left(i);
			rightHalfOfText = sNewLine.right(rightLen);
			sNewLine = leftHalfOfText + "\n" + rightHalfOfText;
		}
	}
	return sNewLine;
}

QString SqueezeLabel::squeezeLine( const QString &sLine, const int &nMaxChars, QWidget *w )
{
	if (sLine.isEmpty() || sLine == "...")
		return QString("");

	QString sNewLine = sLine;
	// replace each occurence of tab character to 8 space char.
	sNewLine.replace( QRegExp("\\t"), "        " );

	// TODO SqueezeLabel::squeezeLine(). need to add a rich text support!
	// TODO SqueezeLabel::squeezeLine(). posibility adding tree dots at end of string

	QString leftHalfOfText, rightHalfOfText;
	uint nTextWidthInPixels = w->fontMetrics().horizontalAdvance( sNewLine, sNewLine.length() );
	uint nThreeDotsWidth    = w->fontMetrics().horizontalAdvance( "...", 3 );

	int n = 0;
	QString sCharacters;
	while (n++ < nMaxChars) {
		sCharacters += "A";
	}
	uint nLabelWidthInPixels = w->fontMetrics().horizontalAdvance(sCharacters, nMaxChars)+10; // width in pixels + margin;

	if (nLabelWidthInPixels < nTextWidthInPixels) {
		// text measuring
		QString sChr;
		int i, nTxtWidth = 0;
		for (i=0; i<sNewLine.length(); i++) {
			sChr = sNewLine[i];
			nTxtWidth += w->fontMetrics().horizontalAdvance(sChr, 1);
			if (nTxtWidth+nThreeDotsWidth > nLabelWidthInPixels)
				break;
		}
		leftHalfOfText  = sNewLine.left( i/2 );
		rightHalfOfText = sNewLine.right( i/2 );
		sNewLine = leftHalfOfText + "..." + rightHalfOfText;
	}

	return sNewLine;
}


void SqueezeLabel::resizeEvent( QResizeEvent * )
{
	setSqueezingText( width() );
}

