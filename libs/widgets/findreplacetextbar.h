/***************************************************************************
 *   Copyright (C) 2017 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef FINDREPLACETEXTBAR_H
#define FINDREPLACETEXTBAR_H

#include "ui_findreplacetextbar.h"

#include <QTimer>
#include <QTextEdit>
#include <QStringListModel>

class QKeyEvent;
// class QTextEdit;

/**
 @author Piotr Mierzwiński
 */
class FindReplaceTextBar : public QWidget, Ui::FindReplaceTextBar
{
	Q_OBJECT
public:
	/** Constructor of class.
	 * Makes signal/slot connections. Creates objects and initaializes variables.
	 * @param pParent pointer to parent for current object
	 * @param sConfigKey key string used in configuration, for saving history of comboboxes
	 */
	FindReplaceTextBar( QWidget *pParent, const QString &sConfigKey );
	~FindReplaceTextBar();

	/** Inits dialog.
	 * @param sInitStr default string which should be put in edit field
	 * @param bDocEditMode TRUE if opened document is in edit mode, otherwise FALSE
	 * @param bVisibleReplaceBar TRUE if Replace bar should be visible, otherwise FALSE
	 */
	void init( const QString &sInitStr, bool bDocEditMode, bool bVisibleReplaceBar=false );

	/** State of checkox pointing to "Start search from cursor".
	 * @return TRUE if "Start search from cursor" checked otherwise FALSE
	 */
	bool startFromCursor() const        { return m_startFromCursorChkBox->isChecked(); }

	/** State of checkox pointing to "Find whole words".
	 * @return TRUE if "Find whole words" checked otherwise FALSE
	 */
	bool findWholeWordsOnly() const     { return m_findWholeWordsChkBox->isChecked(); }

	/** State of checkox pointing to "Case sensitive".
	 * @return TRUE if "Case sensitive" checked otherwise FALSE
	 */
	bool caseSensitive() const          { return m_caseSensitiveChkBox->isChecked(); }

	/** State of checkox pointing to "Regular expression".
	 * @return TRUE if "Case sensitive" checked otherwise FALSE
	 */
	bool regularExpression() const      { return m_regularExpressionChkBox->isChecked(); }

	/** State of checkox pointing to "Replace in selection only".
	 * @return TRUE if "Replace in selection only" checked otherwise FALSE
	 */
	bool replaceInSelectionOnly() const { return m_replaceInSelectionOnlyChkBox->isChecked(); }

	/** State of visible ReplaceBar.
	 * @return TRUE if ReplaceBar visible otherwise FALSE
	 */
	bool replaceBarVisible() const { return  m_pReplaceFrame->isVisible(); }

// 	QStringList & searchingList() { return m_slSarchingList; }

	/** Returns status of visibility the MessageBar.\n
	 @return TRUE if bar is visible, otherwise FALSE.
	*/
	bool errorMessageBarVisible() const { return m_pMessageFrame->isVisible(); }

	/** Shows or hides the ErrorMessageBar.
	 * @param bVisible if TRUE the shows the FindReplaceTextBar otherwise hide it.
	 */
	void setMessageBarVisible( bool bVisible ) { m_pMessageFrame->setVisible(bVisible); }


	/** Cursor movement directions for matching items.
 	 * \li \b SD_home start search from very begining of document
 	 * \li \b SD_up search in up directoin
 	 * \li \b SD_none no direction in search
 	 * \li \b SD_down search in down directoin
 	 * \li \b SD_end start search from end of document
 	 * \li \b SD_down_inc search in down directoin but by incremental
 	 * \li \b SD_behind no direction in search (behind any directions)
	 */
	enum SearchDirection { SD_home = -2, SD_up, SD_none, SD_down, SD_end, SD_down_inc, SD_behind = 99 };

	/** Type of replacing. Available values:
 	 * \li \b RT_ReplaceNextOnly replace only next occurence \n
 	 * \li \b RT_ReplaceAll replace all found occurences
	*/
	enum ReplaceType     { RT_ReplaceNextOnly=0, RT_ReplaceAll };

	/// Flag describing start point of search.
	enum StartFrom       { SF_ignoreFlg=0, SF_FromVeryBegining, SF_FromCursor, SF_FromEnd };

	/// Matching status for searched string.
	enum MatchingStatus  { MS_none=0, MS_match, MS_notMatch };

	/** Message type for message bar. Describing color
 	 * \li \b MT_Error red background of message bar \n
 	 * \li \b MT_Info green background of message bar
	 */
	enum MessageType { MT_Error=0, MT_Info };

//	enum MessageBarButton { MBB_ContinueFrom=0, MBB_Yes, MBB_No };

	/** Returns search direction.
	 * @return direction of search, for details @see SearchDirection
	 */
	SearchDirection searchDirection() const { return m_searchDirection; }

	/** Shows MessageBar with given message.
	 * @param eMessageType background for message: red for error and green for information
	 * @param sMessage message to display
	 * @param bHideAfterCoupleSec TRUE makes hide after defined (5 seconds) time otherwise bar will be visible till user close it
	 * @param sButton1Msg label for first button, if empty then button will not be shown
	 * @param sButton2Msg label for second button, if empty then button will not be shown
	 */
	void setMessage( MessageType eMessageType, const QString &sMessage, bool bHideAfterCoupleSec=true, const QString &sButton1Msg=QString(), const QString &sButton2Msg=QString() );

	/** Translates given enum type SearchDirection into string.
	 * @param eSearchDirection direction search, for details @see SearchDirection
	 * @return string describing given enum value
	 */
	QString toString ( SearchDirection eSearchDirection ) const;

private:

	/// Type of matching
	enum MatchType      { MT_none=0, MT_caseSensitive, MT_regularExpression };

	/// Kind of combobox
	enum ComboKind      { CK_Find=0, CK_Replace };

	/// Start/Continue searching
	enum Searching      { S_Start=0, S_Continue };

	QTimer         *m_closeTimer;
	SearchDirection m_searchDirection;
	MatchType       m_matchingType;

	QStringListModel *m_cbFindModel, *m_cbReplaceModel;
	bool            m_bFoundMatching;
	bool            m_FindMode;
	bool            m_bStartSearch;

	QString         n_sConfigKey; // key used in handle of configuration


	/** Invokes searching in document.
	 * @param bStart TRUE means this is begin of searching, FALSE means continue searching
	 * @param eStartFrom if different than SF_ignoreFlg then will be use to pointing direction of searching.
	 */
	void search( bool bStart, FindReplaceTextBar::StartFrom eStartFrom=SF_ignoreFlg );

private slots:
	/** Moves cursor to next matching item.
	 */
	void slotNextMatching();

	/** Moves cursor to previous matching item.
	 */
	void slotPrevMatching();

	/** Invokes signal triggering replacing for first matching string.
	 */
	void slotReplaceNextOnly();

	/** Invokes signal triggering replacing for all matching strings.
	 */
	void slotReplaceAll();

	/** Turns on/off checking case sensitive during matching item.
	 * Additionally turning on makes that matching by regular expression will be disabled.
	 * @param bMatchCaseSensitive - TRUE turns on matching with case sensitive, FALSE makes skip case sensitive
	 */
	void slotMatchWithCaseSensitive( bool bMatchCaseSensitive );

	/** Turns on/off checking case sensitive during matching item.
	 * Additionally turning on makes that matching with case sensitive will be disabled.
	 * @param bMatchRegularExpr - TRUE for case sensitive otherwise FALSE
	 */
	void slotMatchAsRegularExpression( bool bMatchRegularExpr );

	/** Makes hide message bar.
	 */
	void slotHideErrorMessage() { m_pMessageFrame->hide(); }

	/** Hides message bar and emits signal: signalClickedQuestionBtn1
	 */
	void slotClickedQuestionBtn1();

	/** Hides message bar and emits signal: signalClickedQuestionBtn2
	 */
	void slotClickedQuestionBtn2();

public slots:
	/** Updates background's color of QLineEdit object depends to matching status.
	 * @param eMatchStatus - matching status (found/not found/other)
	 */
	void slotUpdateMatchingStatus( MatchingStatus eMatchStatus );

protected:
	/** Supports keyboard handle for QLineEdit.
	 */
	void keyPressed( QKeyEvent *pKeyEvent );

	/** Catches events from QLineEdit (keyPress and keyRelease)
	 */
	bool eventFilter( QObject *obj, QEvent *event );

signals:
	/** Signal sent when user click into checkbox "Search from current cursor position".
	 */
	void signalStartSearchFromCursor( bool bChecked );

	/** Signal sent when user click into checkBox: "Search whole words only"".
	 */
	void signalFindWholeWords( bool bChecked );

	/** Signal sent to start searching.
	 * @param sString string to search
	 * @param bFromBeginOrCont TRUE means this is begin of searching, FALSE means continue searching
	 * @param eStartFrom force starting search from given point. For details @see FindReplaceTextBar::StartFrom
	 */
	void signalSearch( const QString &sString, bool bFromBeginOrCont, FindReplaceTextBar::StartFrom eStartFrom );

	/** Signal sent to start replaing.
	 * @param eReplaceType type of replacing. For details please check @see ReplaceType
	 * @param sOldString string to replace
	 * @param sNewString string to search
	 */
	void signalStartReplace( FindReplaceTextBar::ReplaceType eReplaceType, const QString &sOldString, const QString &sNewString );

	/** Signal sent when user click into first button placed in MessageBar.
	 */
	void signalClickedQuestionBtn1();

	/** Signal sent when user click into second button placed in MessageBar.
	 */
	void signalClickedQuestionBtn2();

};

#endif // FINDREPLACETEXTBAR_H
