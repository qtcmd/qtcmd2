/***************************************************************************
 *   Copyright (C) 2008 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QtGui> // for qDebug
//#include <QtGlobal>

#include "comboboxdata.h"

const int PaintingScaleFactor = 20;


ComboBoxData::ComboBoxData( const QStringList &slComboItems, int nComboId, Qt::AlignmentFlag eAlign )
	: m_slComboItems(slComboItems), m_eAlign(eAlign), m_nComboId(nComboId)
	, m_TxtColor(QColor("black")), m_bTxtBold(false), m_bTxtItalic(false)
{
	//qDebug() << "ComboBoxData::ComboBoxData, m_eAlign=" << m_eAlign << ", slComboItems.size()=" << slComboItems.size();
//	m_pPainter = NULL;
}

void ComboBoxData::paint( QPainter * pPainter, const QRect & rect, const QPalette & /*palette*/ ) const
{
	pPainter->save();
	pPainter->setRenderHint(QPainter::Antialiasing, true);

	int yOffset = (rect.height() - PaintingScaleFactor) / 2;
	pPainter->translate(rect.x(), rect.y() + yOffset);

	QString sTxt = (m_slComboItems.size() == 0) ? "" : m_slComboItems[m_nComboId];
	//pPainter->setPen(m_TxtColor);

	int fontHeight = pPainter->fontMetrics().height();
	int yPos = (rect.height() - fontHeight)/2 + 1;
	int xPos = 0; // Qt::AlignLeft and others except below is checked
	//qDebug() << "ComboBoxData::paint, m_eAlign=" << m_eAlign;
	if (m_eAlign == Qt::AlignHCenter || m_eAlign == (Qt::AlignHCenter|Qt::AlignVCenter)) {
		xPos = (rect.width() - pPainter->fontMetrics().horizontalAdvance(sTxt))/2 + 1;
	}
	else
	if (m_eAlign == Qt::AlignRight) {
		// TODO implement me  (m_eAlign == Qt::AlignRight)
	}
	//qDebug() << "height="<< rect.height() << ", yOffset=" << yOffset << ", fontHeight=" << fontHeight;
	sTxt = pPainter->fontMetrics().elidedText( sTxt, Qt::ElideRight, rect.width() ); // option.textElideMode
	pPainter->drawText(xPos, yPos+yOffset, sTxt );

	pPainter->restore();
}

QString ComboBoxData::text() const
{
	return m_slComboItems[m_nComboId];
}

void ComboBoxData::setTextAlignment( Qt::AlignmentFlag eAlign )
{
	m_eAlign = eAlign;
	//qDebug() << "setTextAlignment, m_eAlign=" << m_eAlign;
}

/*
	void repaint();
void ComboBoxData::repaint()
{
	qDebug() << "m_pPainter=" << m_pPainter;
	if (m_pPainter != NULL)
		paint( m_pPainter, m_rect, m_palette );
}

	void setPainter(QPainter *pPainter, const QRect &rect, const QPalette &palette);
void ComboBoxData::setPainter(QPainter * pPainter, const QRect & rect, const QPalette & palette)
{
	//if (m_pPainter == NULL) {
		m_pPainter = pPainter;
		m_rect = rect;
		m_palette = palette;
	//}
}
*/

