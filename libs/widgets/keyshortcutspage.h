/***************************************************************************
 *   Copyright (C) 2005 by Piotr Mierzwi?ski <piom@nes.pl>                 *
 *                 2019 by Piotr Mierzwi?ski <piom@nes.pl>                 *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _KEYSHORTCUTSPAGE_H
#define _KEYSHORTCUTSPAGE_H

#include "keyshortcuts.h"
#include "ui_keyshortcutspagedlg.h"


class FileListViewFindItem;
/**
	@author Piotr Mierzwiński <piom@nes.pl>
 */
class KeyShortcutsPage : public QWidget, Ui::KeyShortcutsPageDlg
{
	Q_OBJECT
private:
	enum Columns { ActionCOL=0, ShortcutCOL, OwnCOL, NoneCOL, DefaultCOL, DefaultShortcutCOL };

	struct ValidStatusType {
		enum ValidStatusEnum { VS_none=0, VS_valid, VS_notValid };
	};
	/** Validation status.
	 * Available values: VS_none=0, VS_valid, VS_notValid
	 */
    typedef ValidStatusType::ValidStatusEnum ValidStatus;

	KeyShortcuts *m_pKeyShortcuts;
	QString m_sConfigFileKey, m_sOldShortcut;
	int m_nTotalShortcuts;
	QHash <int, QString> n_hActions;

	/** Initializes list widget view by adding all necessary columns and updating its look&feel.
	 */
	void init();

	/** Filtering the list without proxy. Matched string is entered through LineEdit field placed above list.
	 * @return validation status. For details check also @em ValidStatusType
	 */
	KeyShortcutsPage::ValidStatus updateList();

	/** Set background of LineEdit field (where user enters searching string).
	 * @param validStatus status of validation (matching entered string). For details check also @em ValidStatusType
	 */
	void setValidationStatusAsBgColor( ValidStatus validStatus );


public:
	KeyShortcutsPage( bool bFilesPanelSettings );

	/** Returns current list of key shortcuts.
	 * @return pointer to object contains list of key shortcuts
	 */
	KeyShortcuts *keyShortcuts() const { return m_pKeyShortcuts; }

	/** Updates list of key shortcuts (using given object).
	 * @param pKeyShortcuts pointer to object contains list of key shortcuts
	 */
	void setKeyShortcuts( KeyShortcuts *pKeyShortcuts );

	/**Set defauls settings, so defaul keyShortcuts.
	 */
	void setDefaults();

public Q_SLOTS:
	void slotSave();

private Q_SLOTS:
	/** Slot handles clicking of user into RadioButton widget.
	 * @param pItem pointer to clicked item.
	 */
	void slotButtonClicked( QTableWidgetItem *pItem );
	void slotEditBoxTextApplied( QTableWidgetItem *pItem, const QString &sNewShortcut );
	void slotEditCurrentShortcut();
	void slotTextFilterChanged();
	void slotCleanLineEdit()  {
		m_pLineEditSeachShortcut->clear();
	}

};

#endif // _KEYSHORTCUTSPAGE_H
