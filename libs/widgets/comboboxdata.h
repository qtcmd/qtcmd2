/***************************************************************************
 *   Copyright (C) 2008 by Piotr Mierzwi?ski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _COMBOBOXDATA_H
#define _COMBOBOXDATA_H

#include <QMetaType>
#include <QPainter>
#include <QPalette>

/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class ComboBoxData
{
public:
	ComboBoxData() {}
	ComboBoxData( const QStringList &slComboItems, int nComboId=0, Qt::AlignmentFlag eAlign=Qt::AlignLeft );

	void paint( QPainter *pPainter, const QRect &rect, const QPalette &palette ) const;

	QStringList items() { return m_slComboItems; }
	void setItems( const QStringList & slComboItems ) { m_slComboItems = slComboItems; }
	void addItem( const QString & sItem ) { m_slComboItems.append(sItem); };

	int  comboId() const { return m_nComboId; }
	void setComboId( int nComboId ) { m_nComboId = nComboId; }

	//void setTextAlignment( Qt::AlignmentFlag eAlign ) { m_eAlign = eAlign; }
	void setTextAlignment( Qt::AlignmentFlag eAlign );
	Qt::AlignmentFlag textAlignment() const { return m_eAlign; }

	QString text() const;

	void setTextColor( const QColor &txtColor ) { m_TxtColor = txtColor; };
	void setTextBold( bool bTxtBold ) { m_bTxtBold = bTxtBold; }
	void setTextItalic( bool bTxtItalic ) { m_bTxtItalic = bTxtItalic; }

private:
	QStringList m_slComboItems;

	Qt::AlignmentFlag m_eAlign;

	int m_nComboId;

	QColor m_TxtColor;
	bool m_bTxtBold;
	bool m_bTxtItalic;

// 	QPainter *m_pPainter;
 	QRect m_rect;
	QPalette m_palette;

};

Q_DECLARE_METATYPE(ComboBoxData)

#endif // _COMBOBOXDATA_H
