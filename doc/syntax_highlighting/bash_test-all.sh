#!/bin/sh
# deklaracja zapowiadajaca typ pliku (tak jak powyzej), 
# jesli sie pojawi, musi zawsze byc na poczatku pliku

 # zmienne
${zmienna_1}
$(ZMIENNA_2)
$zmienna_3
${ zmienna_4		}
$( zmienna_5 )

nr=$((nr+1))
expr $FILES + 1 # DIRS = DIRS + 1

 # parametry
$1 $234 $ $! $@ $# $$ $* $?

 # slowa kluczowe
if fi then else elif case esac
for in do done while select until
alias exit read

# polecenia systemowe (prawie wszystkie pliki z /katalogu /bin)
cd echo test mkdir ps touch ls cp mv rm rmdir chmod chown kill cat grep mknod ln pwd basename
true false df sed ed awk egrep su date sleep uname arch gzip tar ex chgrp csh dd dmesg gawk
rpm sort vi vim mount umount zcat sync ping nice netstat more mktemp mail login hostname gunzip
dd cpio chgrp

inne polecenia
dirname wc tail head find du free less which seq clear ulimit whoami make bzip2 cut env history
uptime lilo route

# separator1
;

# separator2
;;

# wykonywane polecenie systemowe
zmienna=`ps -ax | grep "proces"`

# ci�g tekstowy
ciag1="aaaa"
ciag2='bbbbb'
ciag3="aa\\aa\"aa\@aa\$aa"
ciag4="bb$*bb$1 bb$#bb i inne prametry"
ciag5="bb${a}bb$f bb$(a)bb${ a }bb i inne zmiene"

# funkcja
function   nazwa_funkcji() {
    echo "funkcja"
}

tekst zwykly, 
