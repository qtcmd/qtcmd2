#!/bin/bash

# Script converts files type svgz and xpm (default) to png (for given size - default 32x32) and remove source files.
# Author Piotr Mierzwinski
# ver.0.2
# Input parameters: size and input extention

convertExists=`which convert | grep 'which' >/dev/null`
[ $convertExists != "" ] && exit 1

size=$1;   [ "$size" == "" ] && size=32
inpExt=$2; [ "$inpExt" == "" ] && inpExt="xpm" # svgz
outExt="png"

for i in *.$inpExt; do 
	echo "convert: $i to $outExt (${size}x${size})"; 
	if [ "$inpExt" == "xpm" ]; then # required two phazes (resizing inside xpm and convert to png)
		convert -format $outExt32 -background transparent -resize ${size}x${size} $i `basename $i $inpExt`${outExt}${inpExt}
#		echo "converted to: "`basename $i $inpExt`${outExt}${inpExt}
#		echo "try to convert from: "`basename $i $inpExt`${outExt}${inpExt}
#		echo "try to convert to:" `basename $i ${inpExt}`$outExt
		convert -format $outExt32 -background transparent -resize ${size}x${size} `basename $i $inpExt`${outExt}${inpExt} `basename $i ${inpExt}`$outExt
		rm -f `basename $i $inpExt`${outExt}${inpExt}
	else
		convert -format $outExt32 -background transparent -resize ${size}x${size} $i `basename $i $inpExt`$outExt
	fi
	rm -f $i;
done

exit 0

# http://www.sitepoint.com/forums/showthread.php?t=528593
# /usr/local/bin/convert /path/to/photo/4.jpg -format png32 -background transparent -thumbnail x330 -resize '440x<' -resize 50% -gravity center -crop 220x165+0+0 -rotate "-4.4" /path/to/photo/4.png 2>&1
