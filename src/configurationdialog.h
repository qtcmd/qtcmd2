/***************************************************************************
 *   Copyright (C) 2018 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _CONFIGURATIONDIALOG_H
#define _CONFIGURATIONDIALOG_H

#include <QDialog>

#include "plugindata.h"
#include "keyshortcuts.h"
#include "listviewsettings.h"
#include "filesystemsettings.h"
#include "textviewsettings.h"
#include "toolbarpagesettings.h"
// #include "imageviewsettings.h"
// #include "soundviewsettings.h"
// #include "videoviewsettings.h"
// #include "otherviewsettings.h"

/**
	@author Piotr Mierzwiński
*/
class ConfigurationDialog : public QDialog
{
	Q_OBJECT
public:
	ConfigurationDialog() {}
	virtual ~ConfigurationDialog() {}

	virtual PluginData *pluginData() = 0;

	virtual void init( QWidget *pParent, KeyShortcuts *pKeyShortcuts, QList <QActionGroup *> *pActionGroupLst ) = 0;
	virtual void raiseWindow( const QString &sWidgetName ) = 0;

signals:
	// -- Main window. Page: Files lists
	void signalApply( const ListViewSettings &, const QString & );
	// -- Main window. Page: Files Systems
	void signalApply( const FileSystemSettings &, const QString & );
	// -- Main window. Page: Others
	void signalApplyKeyShortcuts( KeyShortcuts * );

	// -- Main window and Viewer window. Page ToolsBar
	void signalApply( const ToolBarPageSettings & );

	// -- View. Page: Text preview
	void signalApply( const TextViewSettings & , const QString & );

// 	void signalApply( const ImageViewSettings & );
// 	void signalApply( const SoundViewSettings & );
// 	void signalApply( const VideoViewSettings & );
// 	void signalApply( const OtherViewSettings & );

};

#endif // _CONFIGURATIONDIALOG_H
