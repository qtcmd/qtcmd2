/***************************************************************************
 *   Copyright (C) 2011 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _PATHVIEW_H
#define _PATHVIEW_H

#include <QPushButton>
#include <QComboBox>

//#include "locationchooser.h"
#include "vfsflatlistmodel.h"
#include "subsystemconst.h"
#include "pathcompleter.h"
#include "uri.h"

// class PathCompleter;

class PathView : public QWidget
{
	Q_OBJECT
public:
	PathView( QWidget *pParent, const QString &sPanelName );
	~PathView();

	enum GetPathFromHistory { CURRENT=0, PREVIOUS, NEXT };

	void setPath( const URI &uri );
	void removePath( const QString &sPath );

	void setModelForCompleter( QAbstractItemModel *pModel );

	void setWritableStatus( WritableStatus writableStatus );

	void popup()     { m_comboPathView->showPopup(); }
	void setActive() { m_comboPathView->setFocus(); }

	void setDontWriteConfig( bool bDontWriteConfig ) {
        m_bDontWriteConfig = bDontWriteConfig;
	}

	/** Sets policy of visiting locations. By default not active.
	 * @param bSetAsLast if true the recently opened will be set as last one, otherwise order will be not changed.
	 * Note. In case this setting function Forward and Backward will work incorrectly
	 */
	void setRecentlyOpenedAsLastInHistory( bool bSetAsLast ) {
		m_bRecentlyOpenedAsLastInList = bSetAsLast;
	}

	QComboBox *pathView() const { return m_comboPathView; }

	/** Returns strings list from the PathView.
	 * @return pathes list
	 */
	QStringList & list();

	/** Sets given strings list to the PathView
	 * @param slItemsList pathes list
	 */
	void setItemsList( const QStringList &slItemsList );

	//void pathChanged( const QString &sPath ) { emit signalPathChanged(sPath); }

	URI prevUri() const;
	URI nextUri() const;
	URI currentUri() const;

	static QString cleanPath( const QString &sPath, bool bSlashAtEnd=true );

	void setFindResultName( const QString &sFindResultName );

	void completerNeedsToPathChange( const QString &sNewPath );

	void showNavigationButtons( bool bShow );

private:
	const int        PATHVIEW_HEIGHT;

	QWidget         *m_pContainer;
    QPushButton     *m_pGoBack, *m_pGoForwrd, *m_pOpenDir;
	QComboBox       *m_comboPathView;
	//LocationChooser *m_comboPathView;
	PathCompleter   *m_completer;
	QStringList      m_pathList, m_initPathList;

	bool             m_bFindResultPanel;
	QString          m_sFindResultName;

	QString          m_sNewPath; // used for singleShot slotPathChanged

	bool             m_bDontWriteConfig;
	bool             m_bRecentlyOpenedAsLastInList;

public slots:
	void slotPathChanged( const QString &sPath );

private slots:
	void slotPathChanged2();
	void slotGoToPrevLocation();
	void slotGoToNextLocation();

protected:
	/** Change size for current obj.
	 *  @param e - pointer to resizeEvent.
	 */
	void resizeEvent( QResizeEvent *e );
	bool eventFilter( QObject *pObj, QEvent *pEvent );

signals:
	void signalPathChanged( const QString &sPath );
	void signalPathChangedForCompleter( const QString &sPath );
	void signalRestorePath();
	void signalOpenFileDialog();

};

#endif // _PATHVIEW_H
