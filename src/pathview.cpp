/***************************************************************************
 *   Copyright (C) 2011 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDir>
#include <QDebug>
#include <QTimer>
#include <QEvent>
#include <QKeyEvent>
#include <QLineEdit>
#include <QHBoxLayout>

#include "settings.h"
#include "pathview.h"
#include "mimetype.h"


PathView::PathView( QWidget *pParent, const QString &sPanelName )
	: QWidget(pParent), PATHVIEW_HEIGHT(26)
{
	setMinimumHeight(PATHVIEW_HEIGHT);
	setMaximumHeight(PATHVIEW_HEIGHT);

	setAccessibleName(sPanelName);
	setObjectName(sPanelName+":PathView");
	qDebug() << "PathView::PathView. Name:" << objectName();

	m_completer = new PathCompleter(this);
	//m_completer->setCaseSensitivity(Qt::CaseSensitive); // this is default value
	//m_completer->setCompletionMode(QCompleter::PopupCompletion); // this is default value, available is also: QCompleter::InlineCompletion
	m_completer->setMaxVisibleItems(15);
	//m_completer->setWrapAround(true); // this is default value

	m_pGoBack   = new QPushButton("<");
	m_pGoBack->setFlat(true);
	m_pGoBack->setToolTip(tr("Go to the previous location"));
 	m_pGoBack->setMaximumWidth(PATHVIEW_HEIGHT-4);
	m_pGoBack->setFocusPolicy(Qt::NoFocus);

	m_pGoForwrd = new QPushButton(">");
	m_pGoForwrd->setFlat(true);
	m_pGoForwrd->setToolTip(tr("Go to the next location"));
 	m_pGoForwrd->setMaximumWidth(PATHVIEW_HEIGHT-4);
	m_pGoForwrd->setFocusPolicy(Qt::NoFocus);

	m_pOpenDir  = new QPushButton("o");
	m_pOpenDir->setFlat(true);
	m_pOpenDir->setToolTip(tr("Select directory to open"));
	m_pOpenDir->setMaximumWidth(PATHVIEW_HEIGHT-4);
	m_pOpenDir->setFocusPolicy(Qt::NoFocus);

	connect(m_pGoBack,   &QPushButton::clicked, this, &PathView::slotGoToPrevLocation);
	connect(m_pGoForwrd, &QPushButton::clicked, this, &PathView::slotGoToNextLocation);
	connect(m_pOpenDir,  &QPushButton::clicked, this, &PathView::signalOpenFileDialog);

	m_pContainer = new QWidget(this);
	m_pContainer->setFocusPolicy(Qt::NoFocus);
	m_comboPathView = new QComboBox();
	m_comboPathView->setMaxCount(35);
	m_comboPathView->setEditable(true);
	m_comboPathView->setMaxVisibleItems(10);
	m_comboPathView->setFocusPolicy(Qt::ClickFocus);
	//m_comboPathView->setInsertPolicy(QComboBox::NoInsert); // needs to add eventFilter for support: Enter pressed. Default is AtBottom

	QHBoxLayout *pPathViewLayout = new QHBoxLayout(m_pContainer);
	pPathViewLayout->setContentsMargins(0,0,0,0);
	pPathViewLayout->setSpacing(0);
	pPathViewLayout->addWidget(m_pGoBack);
	pPathViewLayout->addWidget(m_pGoForwrd);
	pPathViewLayout->addWidget(m_pOpenDir);
	pPathViewLayout->addWidget(m_comboPathView);

	// --- read configuration
	m_bFindResultPanel = false;
	Settings settings;
	QString sName = sPanelName;

	bool bShowNavigationButtons = settings.value(sName+"/ShowNavigationButtons", true).toBool();
	showNavigationButtons(bShowNavigationButtons);

	sName.insert(sName.indexOf('_'), "Tab"); // leftPanelTab_NUM or rightPanelTab_NUM
	if (! settings.value(sName+"/FileOfModelData").isNull()) {
		QFileInfo fi(settings.value(sName+"/FileOfModelData").toString());
		setFindResultName(fi.completeBaseName()); // sets m_sFindResultName
	}
	else {
		QString sPathList = settings.value(sName+"/PathList").toString();
		if (! sPathList.isEmpty()) {
			m_initPathList = sPathList.split('|');
			m_comboPathView->addItems(m_initPathList);
		}
	}

	connect<void(QComboBox::*)(const QString &)>(m_comboPathView, &QComboBox::textActivated, this, &PathView::slotPathChanged);
	m_comboPathView->installEventFilter(this);
	m_bRecentlyOpenedAsLastInList = false;
	m_bDontWriteConfig = false;
}

PathView::~PathView()
{
	QString sName = accessibleName();
	qDebug() << "PathView::~PathView. Name:" << sName;
	if (! m_bDontWriteConfig ) { // set by Mainwindow::slotCloseTab
		// --- update configuration
		QString sPath, sPathList;
		sName.insert(sName.indexOf('_'), "Tab"); // leftPanelTab_NUM or rightPanelTab_NUM
//		qDebug() << "  -- sName" << sName;
		Settings settings;
		if (! m_bFindResultPanel) {
			sPath = m_comboPathView->currentText();
			//qDebug() << sName << "sPath" << sPath;
			if (settings.value(sName+"/Path").isNull())
				settings.setValue(sName+"/Path", sPath);
			else {
				QString sValue = settings.value(sName+"/Path").toString();
				if (sValue != sPath)
					settings.setValue(sName+"/Path", sPath);
			}

			if (m_initPathList != list() && list().count() > 0) {
				foreach(sPath, list()) {
					sPathList += sPath + "|";
				}
				sPathList.remove(sPathList.length()-1, 1);
				settings.setValue(sName+"/PathList", sPathList);
			}
			// -- remove old FindFileResult settings
			if (! settings.value(sName+"/FileOfModelData").isNull()) {
				QFileInfo fi(settings.value(sName+"/FileOfModelData").toString());
				QString sPath = QFileInfo(settings.fileName()).absolutePath() + QDir::separator();
				QString sFileName = sPath + fi.completeBaseName() + ".txt";
				QFile(sFileName).remove();
				settings.remove(sName+"/FileOfModelData");
			}
		}
		else { // find file result Panel
			if (settings.value(sName+"/FileOfModelData").isNull())
				settings.setValue(sName+"/FileOfModelData", m_sFindResultName+".txt");
		}
	}
	delete m_completer;
	delete m_comboPathView;
}

void PathView::setFindResultName( const QString &sFindResultName )
{
	m_bFindResultPanel = ! sFindResultName.isEmpty();
	setVisible(! m_bFindResultPanel);
	m_sFindResultName = sFindResultName;
}

void PathView::setPath( const URI &uri )
{
	QString sPath = uri.toString();
	qDebug() << "PathView::setPath. AccessibleName" << accessibleName() << "uri:"<< sPath;
	const QString separator = QDir::separator();
	// prevent to save path without separator, for egzample: /path/..
	if (! sPath.endsWith(separator)) {
		m_comboPathView->removeItem(m_comboPathView->findText(sPath));
		sPath += separator;
	}

	const QString sUpSep = ".." + separator;
	if (sPath == uri.protocolWithSuffix() && ! m_comboPathView->currentText().endsWith(sUpSep))
		return;
	// prevent to overload history
	if (m_comboPathView->count()+1 > m_comboPathView->maxCount())
		m_comboPathView->removeItem(0);

	int pathId;
	if ((pathId=m_comboPathView->findText(sPath)) < 0)
		m_comboPathView->addItem(sPath);
	else { // found path in history
		if ( m_bRecentlyOpenedAsLastInList ) {
			// save chronology of visiting locations (recently opened as last in history)
			m_comboPathView->removeItem(pathId);
			m_comboPathView->addItem(sPath);
		}
	}
	m_comboPathView->setCurrentIndex(m_comboPathView->findText(sPath));

	QString sHost = uri.host();
	if (uri.protocol() != "file") {
		sHost = uri.toString();
		if (uri.path() == "/")
			sHost = sHost.left(sHost.lastIndexOf(QDir::separator()));
		else
			sHost = sHost.remove(uri.path());
	}
	m_completer->setHost(sHost);
}


void PathView::removePath( const QString &sPath )
{
	if (sPath != "*")
		m_comboPathView->removeItem(m_comboPathView->findText(sPath));
	else
		m_comboPathView->clear();
}

URI PathView::prevUri() const
{
	int currentId = m_comboPathView->currentIndex();
	if (currentId > 0)
		currentId--;

	return URI(m_comboPathView->itemText(currentId));
}

URI PathView::nextUri() const
{
	int currentId = m_comboPathView->currentIndex();
	if (currentId < m_comboPathView->count() - 1)
		currentId++;

	return URI(m_comboPathView->itemText(currentId));
}

URI PathView::currentUri() const
{
	return URI(m_comboPathView->currentText());
}


QStringList & PathView::list()
{
	m_pathList.clear();
	for (int i=0; i<m_comboPathView->count(); i++) {
		m_pathList.append(m_comboPathView->itemText(i));
	}
	return m_pathList;
}


void PathView::setModelForCompleter( QAbstractItemModel *pModel )
{
	if (pModel != nullptr) {
		qDebug() << "PathView::setModelForCompleter. Name:" << objectName() << "model:" << pModel;
		m_completer->setModel(pModel);
		m_comboPathView->lineEdit()->setCompleter(m_completer);
	}
}

void PathView::setWritableStatus( WritableStatus writableStatus )
{
	QColor   bgColor;
	QPalette palette = m_comboPathView->palette();

	if (writableStatus == DIR_WRITABLE)
		bgColor = QColor( 201,241,215 ); //old: 228, 240, 223
	else
	if (writableStatus == DIR_NOT_WRITABLE)
		bgColor = QColor( 241,229,229 ); //old: 255, 229, 229
	else
		bgColor = Qt::white;

	QLineEdit *pLineEdit = m_comboPathView->lineEdit();
	if (writableStatus == DIR_WRITABLE || writableStatus == DIR_NOT_WRITABLE) {
		palette.setColor(pLineEdit->backgroundRole(), bgColor);
		palette.setColor(pLineEdit->foregroundRole(), Qt::black);
	}

	//QColor lineEditBg = m_comboPathView->palette().color(pLineEdit->backgroundRole());
	//QString sheet = QString::fromLatin1("QComboBox::down-arrow { background: %1; image: url(../../iconsets/default/16x16/actions/arrow-down.png) }").arg(lineEditBg.name());

	//QString sheet = QString::fromLatin1("QComboBox::down-arrow { image: url(../../iconsets/default/16x16/actions/go-down-3-dark.svg) }");
	//QString sheet = QString::fromLatin1("QComboBox::down-arrow { image: url(../../iconsets/default/16x16/actions/arrow-down.png) }"); // dark arrow
 	QString sheet = QString::fromLatin1("QComboBox:down-arrow:on { image: url(../../iconsets/default/16x16/actions/arrow-down.png) }"); // bright on dark bgr
	m_comboPathView->setStyleSheet(sheet);
	pLineEdit->setPalette(palette);
}

// TODO might to wonder about moving method: PathView::cleanPath to class URI  or create class: SubsystemTools and put there function  or global method into namespace Vfs
QString PathView::cleanPath( const QString &sPath, bool bSlashAtEnd )
{
	QString sCleanedPath = sPath;
	QString separator = QDir::separator();

	if (! (sPath.endsWith(separator+"..") || sPath.endsWith(separator+".") ||
		   sPath.contains(separator+".."+separator) || sPath.contains(separator+"."+separator))) {
		if (! sCleanedPath.endsWith(separator) && bSlashAtEnd)
			sCleanedPath += separator;
		return sCleanedPath;
	}
	if (sPath == QDir::rootPath()+".."+separator)
		return QDir::rootPath();

	URI uri(sPath);
	if (uri.protocol() == "file") {
		sCleanedPath = QDir::cleanPath(sPath);
		if (sPath != QDir::rootPath()) {
			if (! QFileInfo(sPath).isFile() && sCleanedPath != QDir::rootPath() && sCleanedPath != separator && bSlashAtEnd)
				sCleanedPath += separator;
		}
		return sCleanedPath;
	}
	// remote path or archive
	sCleanedPath = uri.toString();
	sCleanedPath.remove(uri.protocolWithSuffix());
	sCleanedPath.insert(0, separator); // needed for QDir::cleanPath
	sCleanedPath = QDir::cleanPath(sCleanedPath);
	sCleanedPath.remove(0, 1);
	if (sCleanedPath.isEmpty())
		sCleanedPath = uri.protocol()+":";
	else
		sCleanedPath.insert(0, uri.protocolWithSuffix());

	if (! sCleanedPath.endsWith(separator) && bSlashAtEnd)
		sCleanedPath += separator;

	qDebug() << "PathView::cleanPath. Ret.sCleanedPath:" << sCleanedPath;
	return sCleanedPath;
}

void PathView::setItemsList( const QStringList &slItemsList )
{
	m_comboPathView->clear();
	m_comboPathView->addItems(slItemsList);
}

void PathView::completerNeedsToPathChange( const QString &sNewPath )
{
// 	qDebug() << "PathView::completerNeedToPathChange. path:" << sNewPath;
	if (! sNewPath.isEmpty())
		emit signalPathChangedForCompleter(sNewPath);
}

void PathView::slotPathChanged( const QString &sPath )
{
	qDebug() << "PathView::slotPathChanged. Name:" << accessibleName() << "path:" << sPath;
	QString sNewPath = sPath;
	const QString sSeparator = QDir::separator();
	const QString sRootUpDir = QDir::rootPath()+"..";
	const QString sUpSep     = ".." + sSeparator;
	if (sNewPath.contains(QRegExp("^ +/")))
		sNewPath.replace(QRegExp("^ +/"), "/");

	URI uri(sNewPath);
	if (sNewPath == uri.protocolWithSuffix() && ! m_comboPathView->currentText().endsWith(sUpSep))
		return;

	if (sNewPath != uri.protocolWithSuffix()) {
		if (sNewPath == sRootUpDir) {
			m_comboPathView->setCurrentIndex(m_comboPathView->findText(QDir::rootPath()));
			return;
		}
		if (! sNewPath.endsWith(sSeparator)) {
			m_comboPathView->removeItem(m_comboPathView->findText(sNewPath)); // correct for auto added item after Enter pressed
			sNewPath += sSeparator;
		}
		// save chronology
		if (! sPath.endsWith(sUpSep)) {
			m_comboPathView->removeItem(m_comboPathView->findText(sNewPath));
			m_comboPathView->addItem(sNewPath);
			m_comboPathView->setCurrentIndex(m_comboPathView->findText(sNewPath));
		} else {
			m_comboPathView->removeItem(m_comboPathView->findText(sPath)); // correct for auto added item after Enter pressed
			sNewPath = cleanPath(sPath);
		}
	}
	if (sNewPath.endsWith(sSeparator) && ! sPath.endsWith("..") && sPath != sRootUpDir) {
		m_sNewPath = sNewPath;
		QTimer::singleShot(0, this, &PathView::slotPathChanged2);
	}
}

void PathView::slotPathChanged2()
{
	emit signalPathChanged(m_sNewPath); // caught by: SubsystemManager::slotPathChanged
}

void PathView::slotGoToPrevLocation()
{
	emit signalPathChanged(prevUri().toString()); // caught by: SubsystemManager::slotPathChanged
}

void PathView::slotGoToNextLocation()
{
	emit signalPathChanged(nextUri().toString()); // caught by: SubsystemManager::slotPathChanged
}


bool PathView::eventFilter( QObject *pObj, QEvent *pEvent )
{
	QEvent::Type eventType = pEvent->type();

	if (eventType == QEvent::FocusOut) {
		setObjectName(accessibleName()+":PathView:"+currentUri().toString());
		emit signalRestorePath(); // caught by: SubsystemManager::slotPathChanged
		return false;
	}
	else
	if (eventType == QEvent::KeyPress) {
		QKeyEvent *pKeyEvent = (QKeyEvent *)pEvent;
		int key = pKeyEvent->key();
		if (key == Qt::Key_Enter || key == Qt::Key_Return) {
			QString sCurrentText = m_comboPathView->currentText();
			if (sCurrentText == QDir::rootPath()+"..") { // try quit of local subsystem
				m_comboPathView->setCurrentIndex(m_comboPathView->findText(QDir::rootPath()));
				return true;
			}
			QString sCurrentURI = cleanPath(sCurrentText);
			//QString sCurrentURI  = QDir::cleanPath(sCurrentText); // made broken remote path, eg.: ftp://ftp.kde.org/pub/.. -> ftp:/ftp.kde.org
			if (sCurrentURI.endsWith(QDir::separator()) || (sCurrentURI == QDir::rootPath() && sCurrentText.endsWith(".."))) {
				URI uri(sCurrentURI);
				if (sCurrentURI != uri.protocolWithSuffix()) {
					emit signalPathChanged(sCurrentURI); // caught by: VfsFlatListModel::slotPathChanged
					return true;
				}
			}
		}
	}

	return QWidget::eventFilter(pObj, pEvent);
}

void PathView::showNavigationButtons( bool bShow )
{
	m_pGoBack->setVisible(bShow);
	m_pGoForwrd->setVisible(bShow);
	m_pOpenDir->setVisible(bShow);
}


void PathView::resizeEvent( QResizeEvent *e )
{
	m_pContainer->resize(width()+1, PATHVIEW_HEIGHT);
}

