/***************************************************************************
 *   Copyright (C) 2002 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _STATUSBAR_H_
#define _STATUSBAR_H_

#include <QProgressBar>
#include <QResizeEvent>

#include "squeezelabel.h"

/**
 * @author Piotr Mierzwiñski
 **/
class StatusBar : public SqueezeLabel
{
	Q_OBJECT
public:
	StatusBar( QWidget *pParent, bool bShowListStatus );
	~StatusBar();

	void clear( bool bProgress=false );

	void setSqueezingText( bool bSqueezing ) { setScaledContents( bSqueezing ); }

private:
	QProgressBar *m_pProgress;

	uint m_nHtmlTagsWidth, m_ntWidthOfListStatus[3];
	uint m_nNumOfAllItems, m_nNumOfSelectedItems;
	qint64 m_nWeightOfSelectedItems;
	QString m_sStatus, m_sOldStatus, m_sListStatus;
	bool m_bShowListStatus, m_bShowPrevMsg;

	enum SizeOfMessage { SmallMSG, TinyMSG, BigMSG };
	SizeOfMessage m_eSizeOfMessage;

protected:
	void setStatusList();
	void squeezeListStatus();
	void resizeEvent( QResizeEvent * );

public slots:
	/**
	 * @param nDoneInPercents - percentage progress value.
	 * @param nTotalWeight - total weight of file
	 */
	void slotSetProgress( int nDoneInPercents, long long nTotalWeight );

	/** Set given message on status bar and hide it after given miliseconds
	 */
	void slotSetMessage( const QString &sMessage, uint nMiliSec, bool bShowPrevious );
	void slotSetMessage( const QString &sMessage ) { slotSetMessage(sMessage, 0, false); }

	void slotUpdateOfListStatus( uint nNumOfSelectedItems, uint nNumOfAllItems, qint64 nWeightOfSelected );

private slots:
	void slotTimerDone();

};

#endif
