/***************************************************************************
 *   Copyright (C) 2011 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDir> // includes QFileInfo
#include <QDebug>

#include "mimetype.h"
#include "pathview.h" // includes: pathcompleter.h


QString	PathCompleter::pathFromIndex( const QModelIndex &index ) const
{
	qDebug() << "PathCompleter::pathFromIndex; index.data:" << index.data().toString() << "host:" << m_sHost;
	// come from Qt4 example: TreeModelCompleter::pathFromIndex
    // navigate up and accumulate data
	QStringList slDataList;
	for (QModelIndex i = index; i.isValid(); i = i.parent()) {
		slDataList.prepend(model()->data(i, completionRole()).toString());
	}
	if (! m_sHost.isEmpty())
		slDataList.prepend(m_sHost); // for local subsystem m_sHost is empty excluding archive. This is set in PathView::setPath
// 	ftp://anonymous@ftp.kde.org:21/
//	ftp://ftp.icm.edu.pl/
	return slDataList.join(QDir::separator());
}

//	Completion list (data model) considers filter built in subsystem in listing function)
QStringList PathCompleter::splitPath( const QString &sInPath ) const
{
	QString sOutPath, sNewPath, separator = QDir::separator();
	QString sPath = sInPath;
	if (sPath.startsWith("~/"))
		sPath.replace("~/", QDir::homePath()+separator);
	URI uri(sPath);
	QString sHost = uri.host();
	bool bRemoteUrl = uri.mime().startsWith("remote/");
// 	m_sHost = uri.host(); // unfortunately this is const fun., hence there is needed external set

	if (bRemoteUrl) { // remote path
		sHost = sPath;
		if (uri.path() == "/")
			sHost = sHost.left(sHost.lastIndexOf("/"));
		else
			sHost.remove(uri.path());
		sOutPath = PathView::cleanPath(uri.path(), false);
		sOutPath.remove(0, 1);
	}
	else { // simplify path
		sOutPath = PathView::cleanPath(sPath, false);
		if (! sHost.isEmpty())
			sOutPath.remove(sHost+separator);
	}
	// 	qDebug() << "PathCompleter::splitPath. RealPath:" << sPath << "sOutPath:" << sOutPath << "sMime:" << uri.mime() << "host:" << m_sHost << "sHost:" << sHost;
	// PathCompleter::splitPath. sPath: "ftp://ftp.icm.edu.pl/" sOutPath: "/" sMime: "remote/ftp" host: "" sHost: "ftp.icm.edu.pl"

	if (sPath.endsWith(separator)) {
		if (! sPath.endsWith(":/") && ! sPath.endsWith("://")) {
// 			if (! popup()->isVisible()) {
				if (! bRemoteUrl) {
					sNewPath = sHost;
					if (! sHost.isEmpty()) // because path inside archive doesn't start with slash
						sNewPath += separator;
					if (sOutPath != separator)
						sNewPath += sOutPath;
					if (sNewPath.isEmpty()) // root path in local subsystem
						sNewPath = QDir::separator();
					PathView *pPathView = dynamic_cast <PathView *> (parent());
					pPathView->completerNeedsToPathChange(sNewPath);
				}
				else
					return QCompleter::splitPath(sPath);
// 			}
		}
		else {
			return QCompleter::splitPath(sPath);
		}
	}

	if (bRemoteUrl) // disable completer list for remote or similar to remote location
		return QCompleter::splitPath(sPath);

	return QStringList(sOutPath);
}

