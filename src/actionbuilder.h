/***************************************************************************
 *   Copyright (C) 2017 by Mariusz Borowski <mariusz@nes.pl>               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along withthisprogram; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _ACTIONBUILDER_H_
#define _ACTIONBUILDER_H_

#include <QKeySequence>
#include <QIcon>

class QAction;
class ActionRegister;
class Settings;


class ActionBuilder
{
public:
	explicit ActionBuilder(ActionRegister *pRegister, Settings *pSettings, const QString &sSection);	
	
	QAction* create(int nActionId, const QString &sName, const QKeySequence &defaultKeySequence, QObject *parent = Q_NULLPTR);
	
	QAction* create(int nActionId, const QString &sName, const QIcon &icon, const QKeySequence &defaultKeySequence, QObject *parent = Q_NULLPTR);
	
	QAction* createSeparator(QObject *parent = Q_NULLPTR);
	
	void changeSection(const QString &sSection);

private:
	QString nameToKey(const QString &sActionName);
	
	ActionRegister *m_pActionRegister;
	Settings *m_pSettings;
	QString m_sCurrentSection;
};


#endif
