/***************************************************************************
 *   Copyright (C) 2011 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _PATHCOMPLETER_H
#define _PATHCOMPLETER_H

#include <QCompleter>

class PathCompleter : public QCompleter
{
public:
	PathCompleter( QObject *pParent = 0 ) : QCompleter(pParent) {}
	PathCompleter( QAbstractItemModel *pModel, QObject *pParent=0 ) : QCompleter(pModel, pParent) {}

	void setHost( const QString &sHost ) { m_sHost = sHost; }

protected:
	QStringList splitPath( const QString &sInPath ) const;
	QString	pathFromIndex( const QModelIndex &index ) const;

private:
	QString m_sHost;

};

#endif // _PATHCOMPLETER_H
