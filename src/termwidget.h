/***************************************************************************
 *   Copyright (C) 2016 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _TERMWIDGET_H
#define _TERMWIDGET_H

#include <qtermwidget5/qtermwidget.h>

// #define DEFAULT_FONT  "Monospace"

class TermWidget : public QTermWidget
{
	Q_OBJECT
public:
	TermWidget( const QString &sWorkDir, QWidget *pParent, const QString &sShell=QString() );
	~TermWidget();

	void setPathOfWorkingDirectory( const QString &sPath ) { setWorkingDirectory(sPath); }

	QString currentPath() const { return n_sCurrentPath; }
	void setCurrentPath( const QString &sPath ) { n_sCurrentPath = sPath; }

	bool shellIsWorking();

private:
	QString    m_sColorScheme;
	QString    m_sKeyBindings;

	uint       m_nHistorySize;
	uint       m_nFontSize;

	QFont      m_font;
	QString    m_sFont;

	QWidget   *n_pParent;
	QString    m_sShell;
	QString    n_sCurrentPath;
	QString    n_sFontFamily;

	void loadSettings();
	void setProperties();

public Q_SLOTS:
	void slotPathChanged( const QString &sPath );
	void slotExeCommandInTerminal( const QString &sCmd );

private Q_SLOTS:
	void slotGetCurrentPath();

Q_SIGNALS:
	void signalPathChangedInTerminal();

};

#endif // _TERMWIDGET_H
