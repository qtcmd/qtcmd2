/***************************************************************************
 *   Copyright (C) 2007 by Mariusz Borowski <b0mar@nes.pl>                 *
 *   Copyright (C) 2009 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _FILELISTVIEW_H
#define _FILELISTVIEW_H

#include <QWidget>

#include "treeview.h"
#include "bookmarks.h"
#include "drivesmenu.h"
#include "contextmenu.h"
#include "subsystemmanager.h"
#include "listviewitemdelegate.h"
#include "filelistviewfinditem.h"
#include "filelistviewfilter.h"
#include "propertiesdialog.h"
#include "listviewsettings.h"
#include "fileattributes.h"
#include "iconcache.h"

class QModelIndex;
class VfsFlatListModel;

/**
  @author Mariusz Borowski, Piotr Mierzwiński
  */
class FileListView : public QWidget
{
	Q_OBJECT
public:
	FileListView( SubsystemManager *pSubsystemMngr, const QString &sName, QWidget *pParent );
	~FileListView();

	void setVfsModel( QAbstractItemModel *pModel );

	void hideQuickSearch() { if (m_FileListViewFindItem->isVisible()) m_FileListViewFindItem->close(); }

	/** Activates Tree View  (sets focus on file list).
	 */
	void setActive() { m_tvFileList->setFocus(); }
	bool isActive()  { return m_tvFileList->hasFocus(); }
	bool hadFocus() { return m_bHadFocus; }

	void setColumnWidth( int nLogicalIndex, int nNewSize );

	/** Returns string describing all columns width like they are saved in config file.
	 * @return configuration string for columns
	 */
	QString columnsWidthStr() const;

	// used to setTabOrder in Panel
	TreeView *filelistView() const { return m_tvFileList; }

	void selectAll( bool mark );
	void selectWithTheSameExtention( bool mark );
	void selectByPattern( bool mark );

	bool findFileResultMode() const { return m_vfsModel->findFileResultMode(); }
	void setColumns( bool bFindFileResultMode ) { m_vfsModel->setColumns(bFindFileResultMode); }


	void openNewRemoteConnection(Vfs::RemoteConnection eRemoteConnection);

	void showBookmarksMenu();

	void showDrivesMenu();

	void copyMoveFile( bool bCopy ) {
		slotBreakCurrentOperation();
		m_subsystemMngr->copyFiles(m_selectionModel->selectionMap(), bCopy, this);
	}

	void makeDir() {
		slotBreakCurrentOperation();
		m_subsystemMngr->createNewEmptyFile(currentModelIndex(), false, this);
	}

	void deleteFile() {
		slotBreakCurrentOperation();
		m_subsystemMngr->removeFiles(m_selectionModel->selectionMap(), this);
	}

	void makeLink();

	/** Marks current item if no one marked.
	 * @return number of marked.
	 */
	int numberOfSelectedItems() {
		return m_selectionModel->markIfNoOneMarked(currentModelIndex());
	}

	VfsFlatListModel *vfsModel() { return m_vfsModel; }
	VfsFlatListSelectionModel *vfsSelectionModel() { return m_selectionModel; }

	void setKeyShortcuts( KeyShortcuts *pKeyShortcuts ) { // is set just after creating object this class
        m_pKS = pKeyShortcuts;
	}

	void goToUserHomeDirectory() {
		slotBreakCurrentOperation();
		m_subsystemMngr->slotPathChanged("~/");
	}

	void goToRootDirectory() {
		slotBreakCurrentOperation();
		m_subsystemMngr->slotPathChanged("/|/");
	}

	bool quickGetOutOfSubsystem();
	void oneDirBackward( const URI &nextURI );
	void oneDirForward( const URI &prevURI );

	/** Helper function calling (or not if not necessary) subsystemmanager function.
	 * @param bMove if TRUE then invoke move operation
	 */
	void createAnArchive( bool bMove );

	/** Apply filter with extension as in clicked file.
	 */
	void filterWithExtension();

	/** Returns status of list be filtered.
	 * @return true if list is filtered already, otherwise not filtered at all
	 */
	bool isListFiltered() const { return m_pFileListViewFilter->filterApplied(); }

	/** Removes currently applied filter (including "Filter With Extension")
	 */
	void removeFilter();

	/** Applies settings for list view getting data from given object.
	 * @param listViewSettings reference to container containing list view settings
	 * @param sCategory category in opened section
	 */
	void applyListViewSettings( const ListViewSettings &listViewSettings, const QString &sCategory );

	static const int FIND_FILES_COLUMNS = 8; // found files list
	static const int LIST_VIEW_COLUMNS  = 7; // common files list

private:
	TreeView              *m_tvFileList;
	VfsFlatListModel      *m_flmModel, *m_vfsModel;
	VfsFlatListProxyModel *m_flpModel;
	VfsFlatListSelectionModel *m_selectionModel;

	bool                   m_bInitSelectionFlag;
	bool                   m_bHadFocus;

	SubsystemManager      *m_subsystemMngr;

	FileListViewFindItem  *m_FileListViewFindItem;
	FileListViewFilter    *m_pFileListViewFilter;

	QString                m_sLastSelectedItemName;
	QModelIndex            m_lastSelectedModelIndex;

	bool                   m_lineEditDelegateVisible;

	ListViewItemDelegate  *m_pListViewItemDelegate;

	// configuration variables
	bool                   m_bAlternatingRowColors;
	bool                   m_bBoldedDir;
	bool                   m_bShowIcons;
	bool                   m_bBytesRound;
	bool                   m_bSelectDirs;
	bool                   m_bShowHiddenFiles;
	bool                   n_bItemColorFromSystem;
	QString                n_sItemColor, m_sItemColorUnderCursor;
	QString                m_sColorOfSelectedItem, m_sColorOfSelectedItemUnderCurs;
	QString                m_sBackgroundColor, m_sSecondBackgroundColor;
	QString                m_sCursorColor;
	QString                m_sHiddenFileColor;
	QString                m_sExecutableFileColor;
	QString                m_sSymlinkColor;
	QString                m_sBrokenSymlinkColor;
	bool                   m_bSetAttribConfirmation;
	bool                   m_bWeighBeforeSettingAttrib, m_bWeighBeforeCopying, m_bWeighBeforeMoving;
	bool                   m_contextMenuVisible;

	ContextMenu           *m_pContextMenu;
	Bookmarks             *m_pBookmarks;
	DrivesMenu            *m_pDrivesMenu;

	bool                   m_bTopOpenWithAppUseAsDefault;
	bool                   m_bViewCurrentIfMoreSelected;
	bool                   m_bUseExtViewerIfIntIsNotAvailable;
	bool                   m_bAlwaysOpenExtViewIfFewMarked;

	enum { START_SEARCH_WHEN_TYPING=1, INVOKE_SEARCH_BAR_ONLY_BY_SHORTCUT };
	int                    m_nTheWayOfInvokingSearchBar;

	FileInfo              *m_weighFI;
	QTimer                *m_pWeighAnimTimer;
	int                    m_nFraneCounter;

	PropertiesDialog      *m_pPropertiesDlg;
	QModelIndex            m_currentItemBeforeMark;
	QModelIndex            m_currentIndex;
	QString                m_sTabName;

	bool                   m_bShiftPressed, m_bCtrlPressed, m_bMouseBtnPressed;
	QModelIndex            m_previousModelIndex;

	bool                   m_bSyncColWidthInBothPanels;

	QString                m_sTreeViewFont;

	QString                m_sFilterWithExtension;

// 	ListViewSettings      *m_pLVSet; // TODO: ListViewSettings a class, and all its members are initialized in constructor with default values

	QString     currentFileExtention() const;
	QString     currentFileName() const;
	QModelIndex currentModelIndex() const;

	KeyShortcuts          *m_pKS;

	int m_ntFindFilesCol[FIND_FILES_COLUMNS];
	int m_ntListViewCol[LIST_VIEW_COLUMNS];


	/** Shows context menu.
	 *  @param onCursorPos if true then show on mouse cursor position otherwise below focused item.
	 */
	void   showContextMenu( bool onCursorPos );

	void   loadConfiguration();
	void   saveConfiguration();
	void   updateView();

	void   openCurrentItemInNewTab();

	void   weighCurrentItem();

	void   showPropertiesDialog();

	void   addCurrentPathToTheTheBookmarks();
	void   bookmarksNeedToChangePath( int nId );

	/** Shows or hides given section.
	 * @param nId logical ID of section
	 * @param bShow if true then show given section, otherwise hide it
	 */
	void   showHideSection( int nId, bool bShow );

protected:
	bool eventFilter( QObject *obj, QEvent *event );

	/** Handled keys and shortcuts.
	 *	 @param pKeyEvent key event
	 *	 @return true if to be "eaten" KeyEvent otherwise false
	 */
	bool keyPressed( QKeyEvent *pKeyEvent );
	//void keyPressEvent( QKeyEvent *pKeyEvent );


private Q_SLOTS:
	void slotCloseEditor( QWidget *pEditor, QAbstractItemDelegate::EndEditHint );
	void slotCommitEditor( QWidget *pEditor );

	void slotOpenSelected();

	void slotContexMenuAction( int nActionId );
	void slotUpdateContextMenu();
	void slotMenuEntryHovered( QAction *pAction );

	void slotWeighStartAnimation();

	void slotSetAttributes( FileInfo *pFileInfo, FileAttributes *pChangedAttrib, bool bChangesForAllSelected );
	void slotStartWeighing( FileInfo *pFileInfo, bool bStartWeigh );

	void slotClosePropertiesDlg();

	void slotDrivesMenuPathChanged( const QString &sPath );
// 	void slotResortView();
	void slotSelectionChanged( const QItemSelection &, const QItemSelection & );

	void slotSectionResized( int logicalIndex, int oldSize, int newSize );

public Q_SLOTS:
	void slotUpdateFocusSelection( bool bInitSelectionFlag, const QString &sItemName );

	void slotAdjustColumnSize();
	void slotUpdateListViewStatusBar();
	void slotWeighStopAnimation();

	void slotBreakCurrentOperation();

	void slotInitRename();
	void slotViewSelected( bool bViewInWindow, bool bOpenInEditMode );
	void slotFindItemKeyPressed( QKeyEvent *pKeyEvent );

Q_SIGNALS:
	void signalDuplicateTab();
	void signalCloseTab();
	void signalAddTab();
	void signalSwitchToTab( int nTabId );

	void signalUpdateOfListStatus( uint nNumOfSelectedItems, uint nNumOfAllItems, qint64 nWeightOfSelected );
	void signalUpdateOfListStatus( const QString &sMsg );

	void signalFocusToSecondPanel(); // usable for "open in panel beside" operation

	void signalGetPathFromOppositeActiveTab( QString &sPath );

	void signalUpdateStatusBar( const QString &sMsg );

	void signalUpdatePathInTermianl();

	void signalUpdateColWidthInOppositePanel( int logicalIndex, int oldSize, int newSize );

};

#endif // _FILELISTVIEW_H
