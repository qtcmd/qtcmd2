/***************************************************************************
 *   Copyright (C) 2017 by Mariusz Borowski <mariusz@nes.pl>               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along withthisprogram; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "actionregister.h"

#include <QAction>


ActionRegister* ActionRegister::instance()
{
	static ActionRegister instance;
	return &instance;
}


void ActionRegister::add(const QString &section, QAction *action)
{
	Q_ASSERT(action);

	addToSection(section, action);
	int actionId = action->data().toInt();
	m_actions[actionId] = action;
}


void ActionRegister::remove(int actionId, const QString &section)
{
	Q_ASSERT(m_actions.contains(actionId));
	Q_ASSERT(m_sections.contains(section));

	QAction *action = m_actions[actionId];
	//m_sections[section].removeAll(action); // original
	m_sections[section].remove(action);
}


QList<QString> ActionRegister::sections() const
{
	return m_sections.keys();
}

const ActionList& ActionRegister::sectionActions(const QString &section) const
{
	Q_ASSERT(m_sections.contains(section));

	return m_sections[section];
}


QAction* ActionRegister::action(int actionId)
{
	Q_ASSERT(m_actions.contains(actionId));

	return m_actions[actionId];
}

void ActionRegister::updatePrimaryShortcut(int actionId, const QKeySequence &seq)
{
	Q_ASSERT(m_actions.contains(actionId));

	QAction *action = m_actions[actionId];
	QList<QKeySequence> shortcuts = action->shortcuts();
	if (shortcuts.size() == 0) {
		shortcuts.append(seq);
	} else {
		shortcuts[0] = seq; // primary shortcut as the first element
	}

	action->setShortcuts(shortcuts);
}


void ActionRegister::updateSecondaryShortcut(int actionId, const QKeySequence &seq)
{
	Q_ASSERT(m_actions.contains(actionId));

	QAction *action = m_actions[actionId];
	QList<QKeySequence> shortcuts = action->shortcuts();
	Q_ASSERT(shortcuts.size() >= 1); // primary shortcut must be set

	if (shortcuts.size() == 1) {
		shortcuts.append(seq);
	} else {
		shortcuts[1] = seq;
	}

	action->setShortcuts(shortcuts);
}


void ActionRegister::addToSection(const QString &section, QAction *action)
{
	if ( ! m_sections.contains(section) ) {
		m_sections[section] = ActionList();
	}

	m_sections[section].push_back(action);
}
