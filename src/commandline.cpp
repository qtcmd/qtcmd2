/***************************************************************************
 *   Copyright (C) 2016 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDebug>
#include <QHostInfo>
#include <QKeyEvent>
#include <QLineEdit>

#include "settings.h"
#include "commandline.h"
#include "lineeditstyle.h"
#include "systeminfohelper.h"

// TODO Add completion for command "cd" where try to match (maybe after pressing Tab) all directories in current path (just fill model by all listed directories)

CommandLine::CommandLine( QWidget *pParent )
	: QToolBar(pParent)
{
	setMovable(false);

	qDebug() << "CommandLine::CommandLine." << objectName();

	QString sCommandLineLabel = "<b>["+ Vfs::getUserName(Vfs::getCurrentUserId())+"</b>@<b>"+QHostInfo::localHostName()+":]$ </b>";
	m_pCommandLineLabel = new QLabel(this);
	m_pCommandLineLabel->setTextInteractionFlags(Qt::TextSelectableByMouse);
	m_pCommandLineLabel->setText(sCommandLineLabel);
	m_pCommandLineLabel->setFocusPolicy(Qt::NoFocus);

	m_comboCommandLine = new QComboBox(this);
	QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Fixed);
	sizePolicy1.setHorizontalStretch(0);
	sizePolicy1.setVerticalStretch(0);
	sizePolicy1.setHeightForWidth(m_comboCommandLine->sizePolicy().hasHeightForWidth());
	m_comboCommandLine->setSizePolicy(sizePolicy1);
	m_comboCommandLine->setMaxCount(35);
	m_comboCommandLine->setEditable(true);
	m_comboCommandLine->setMaxVisibleItems(10);
	m_comboCommandLine->setFocusPolicy(Qt::ClickFocus);
	m_comboCommandLine->setInsertPolicy(QComboBox::InsertAtTop);
	//m_comboCommandLine->setDuplicatesEnabled(false); // this is default value
	m_comboCommandLine->lineEdit()->setStyle(new LineEditStyle(style()));
	m_comboCommandLine->installEventFilter(this);

	Settings settings;
	QString sFont = settings.value("Terminal/Font", "Monospace,10").toString();
	QString sFontFamily = sFont.left(sFont.indexOf(','));
	QFont font = qvariant_cast<QFont>(settings.value("Terminal/Font", sFontFamily));
	//font.setFamily("Monospace");
	font.setBold(true);
	m_comboCommandLine->setFont(font);

	changeBackgroundColor();

	addWidget(m_pCommandLineLabel);
	addWidget(m_comboCommandLine);

	// -- load history commands
	QString sCmdsList = settings.value("CommandLine/History").toString();
	if (! sCmdsList.isEmpty()) {
		m_initCmdsList = sCmdsList.split('|');
		m_comboCommandLine->addItems(m_initCmdsList);
		m_comboCommandLine->lineEdit()->clear();
	}

	// NOTE default completer is working for all items placed in list
	//m_completer = new CmdCompleter(this);
	//m_completer->setCompletionMode(QCompleter::InlineCompletion);
}

CommandLine::~CommandLine()
{
	QString sName = accessibleName();
	qDebug() << "CommandLine::~CommandLine." << sName;

	Settings settings;
	QString sCmd, sCmdsList;
	if (m_initCmdsList != list() && list().count() > 0) {
		foreach(sCmd, list()) {
			sCmdsList += sCmd + "|";
		}
		sCmdsList.remove(sCmdsList.length()-1, 1);
		settings.setValue("CommandLine/History", sCmdsList);
	}
	settings.update("CommandLine/BackgroundColor", m_sBackgroundColor);

//	delete m_completer;
	delete m_comboCommandLine;
}


QStringList & CommandLine::list()
{
	m_cmdsList.clear();
	for (int i=0; i<m_comboCommandLine->count(); i++) {
		m_cmdsList.append(m_comboCommandLine->itemText(i));
	}
	return m_cmdsList;
}

void CommandLine::changeBackgroundColor()
{
	Settings settings;
	m_sBackgroundColor = settings.value("CommandLine/BackgroundColor", "#000000").toString(); // #808080

	QColor   bgColor = QColor(m_sBackgroundColor);
	QPalette palette;

	QLineEdit *pLineEdit = m_comboCommandLine->lineEdit();
	palette.setColor(pLineEdit->backgroundRole(), bgColor);
	palette.setColor(pLineEdit->foregroundRole(), Qt::white);
// 	pLineEdit->setAutoFillBackground(true);
	pLineEdit->setPalette(palette);
}

// void CommandLine::setModelForCompleter( QAbstractItemModel *pModel )
// {
// 	if (pModel != NULL) {
// 		qDebug() << "CommandLine::setModelForCompleter. model:" << pModel;
// 		m_completer->setModel(pModel);
// 		m_comboCommandLine->lineEdit()->setCompleter(m_completer);
// 	}
// }

void CommandLine::slotEnterPressed( const QString &sCmd )
{
	qDebug() << "CommandLine::slotEnterPressed. CurrentPath:" << m_sCurrentPath;
	qDebug() << "CommandLine::slotEnterPressed. Sent cmd:" << sCmd;
	emit signalChangePathInTerminal(m_sCurrentPath); // caught in MainWindow class
	emit signalExeCommandInTerminal(m_comboCommandLine->lineEdit()->text());
	m_comboCommandLine->lineEdit()->clear();
}


bool CommandLine::eventFilter( QObject *pObj, QEvent *pEvent )
{
	QEvent::Type eventType = pEvent->type();

	if (eventType == QEvent::FocusOut) {
		return false;
	}
	else
	if (eventType == QEvent::KeyPress) {
		QKeyEvent *pKeyEvent = (QKeyEvent *)pEvent;
		int key = pKeyEvent->key();
		int keyMod = pKeyEvent->modifiers();
		QLineEdit *pLineEdit = m_comboCommandLine->lineEdit();

		if (key == Qt::Key_Enter || key == Qt::Key_Return) {
			slotEnterPressed(m_comboCommandLine->currentText());
			return true;
		}
		else
		// TODO add support for other bash shortcuts: http://ss64.com/bash/syntax-keyboard.html
		if (keyMod == Qt::NoModifier) {
			if (key == Qt::Key_Up) { // Previous command (Up arrow)
				int id = m_comboCommandLine->currentIndex();
				if (id > 0)
					id--;
				m_comboCommandLine->setCurrentIndex(id);
				return true;
			}
			else if (key == Qt::Key_Down) { // Next command (Down arrow)
				int id = m_comboCommandLine->currentIndex();
				if (id < m_comboCommandLine->count())
					id++;
				m_comboCommandLine->setCurrentIndex(id);
				return true;
			}
		}
		else
		if (keyMod == Qt::CTRL) {
			if (key == Qt::Key_A) {     // Go to the beginning of the line (Home)
				pLineEdit->setCursorPosition(0);
				return true;
			}
			else if (key == Qt::Key_E) { // Go to the End of the line (End)
				pLineEdit->setCursorPosition(pLineEdit->text().length());
				return true;
			}
			else if (key == Qt::Key_K) { // Remove characters from right side of cursors
				pLineEdit->setSelection(pLineEdit->cursorPosition(), pLineEdit->text().length()-pLineEdit->cursorPosition());
				pLineEdit->del();
				return true;
			}
			else if (key == Qt::Key_W) { // Remove characters from left side of cursors
				pLineEdit->setSelection(0, pLineEdit->cursorPosition());
				pLineEdit->del();
				return true;
			}
			else if (key == Qt::Key_P) { // Previous command (Up arrow)
				int id = m_comboCommandLine->currentIndex();
				if (id > 0)
					id--;
				m_comboCommandLine->setCurrentIndex(id);
				return true;
			}
			else if (key == Qt::Key_N) { // Next command (Down arrow)
				int id = m_comboCommandLine->currentIndex();
				if (id < m_comboCommandLine->count())
					id++;
				m_comboCommandLine->setCurrentIndex(id);
				return true;
			}
		}
		else
		if (keyMod == Qt::ALT) {
			if (key == Qt::Key_Delete) { // Delete the Word before the cursor
				pLineEdit->cursorWordBackward(true);
				pLineEdit->del();
				return true;
			}
			else if (key == Qt::Key_D) { // Delete the Word after the cursor
				pLineEdit->cursorWordForward(true);
				pLineEdit->del();
				return true;
			}
			else if (key == Qt::Key_B) { // Back (left) one word
				pLineEdit->cursorWordBackward(false);
				return true;
			}
			else if (key == Qt::Key_F) { // Forward (right) one word
				pLineEdit->cursorWordForward(false);
				return true;
			}
		}
	}

	return QWidget::eventFilter(pObj, pEvent);
}

