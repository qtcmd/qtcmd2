/***************************************************************************
 *   Copyright (C) 2005 by Mariusz Borowski <b0mar@nes.pl>                 *
 *   Copyright (C) 2009 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDesktopWidget>
#include <QApplication>
#include <QFileDialog>
#include <QSplitter>
#include <QProcess>
#include <QDebug> // need to qDebug()
#include <QFile>
#include <QDir>

#include "panel.h"
#include "viewer.h"
#include "settings.h"
#include "mimetype.h"
#include "iconcache.h"
#include "mainwindow.h"
#include "messagebox.h"
#include "plugindata.h"
#include "pluginfactory.h"
#include "keyshortcutsappids.h"


MainWindow::MainWindow()
{
	setObjectName("MainWindow");
	setWindowTitle("QtCommander");

	QString sConfFileName = "QtCommander2";
	if (qApp->applicationDirPath().contains("build"))
		sConfFileName += "-dev";

	QCoreApplication::setOrganizationName("QtCommander2"); // global settings for all newly created QSettings obj.
	QCoreApplication::setApplicationName(sConfFileName);   // global settings for all newly created QSettings obj.
	QGuiApplication::setAttribute(Qt::AA_UseHighDpiPixmaps); // enable HiDPI

	Settings settings;

	QString sMainWndSize = settings.value("mainwindow/Size", "800,600").toString();
	m_initWidth = sMainWndSize.split(',').at(0).toInt();
	m_initHeight = sMainWndSize.split(',').at(1).toInt();
	if (m_initWidth <= 600 || m_initWidth >= QApplication::desktop()->width()+1)
		m_initWidth = 800;
	if (m_initHeight <= 400 || m_initHeight >= QApplication::desktop()->height()+1)
		m_initHeight = 600;
	resize(m_initWidth, m_initHeight);

	m_bAppCloseConfirmation = settings.value("mainwindow/AppCloseConfirmation", true).toBool();

	m_sTmpDir = settings.value("vfs/TempDir", QDir::tempPath()+"/qtcmd2/").toString();
	qDebug() << "MainWindow::MainWindow. Application style:" << QApplication::style()->objectName();
// 	removeTempDir();
	QDir tmpDir(m_sTmpDir);
	if (! tmpDir.exists()) {
		if (! tmpDir.mkpath(m_sTmpDir)) {
			qDebug() << "MainWindow::MainWindow. ERROR. Cannot create temporary directory!" << m_sTmpDir;
			qDebug() << "\t\tSome operations in remote and archive subsystems will not be available.";
			m_sTmpDir = "";
		}
	}
	if (! m_sTmpDir.isEmpty())
		settings.setValue("vfs/TempDir", m_sTmpDir);

	createIconCache(settings); // create object related to m_pIconCache pointer
	loadPlugins(settings);

	m_pKS = nullptr;
	m_pTermWidget = nullptr;
	m_lastTabsPanel = nullptr;
	m_pLastActivePanel = nullptr;
	m_leftPanelListedTabsCount = 0;
	m_rightPanelListedTabsCount = 0;
	m_bInitFocusOnLeftPanel = false;
	m_bAppStarted = false;

	initKeyShortcuts();
	initMenuBar(settings);
	initTabPanels();
	initEmbededTerminal(false); // create and hide
	initToolBar(settings);
	initStatusBar(settings);
	initButtonBar(settings); // bottom tool bar
	initCommandLine(settings);
	initFindFileDialog(); // create and hide

// 	m_pFindInTermAct->setVisible(m_pTermWidget->isVisible());
	m_pTerminalMenu->setEnabled(m_pTermWidget->isVisible());

	m_bPathSyncBetweenTermAndFLV = settings.value("mainwindow/PathSyncBetweenTermAndFileListView", true).toBool();

	QString sDefaultTrashPath = settings.value("vfs/sPathToTrash", QDir::homePath()+QStringLiteral("/.trash_QtCmd/")).toString();
	createTrashDir(sDefaultTrashPath);
}

MainWindow::~MainWindow()
{
	qDebug() << "MainWindow::~ MainWindow.";
	int nLeftTabCount  = m_leftTabsPanel->count();
	int nRightTabCount = m_rightTabsPanel->count();
	Settings settings;

	TabPanelsWidget *pTabPanel;
	if (m_leftTabsPanel->hasFocus())
		pTabPanel = m_leftTabsPanel;
	else
		pTabPanel =  m_rightTabsPanel;
	Panel *pPanel = pTabPanel->tabPanel(pTabPanel->currentIndex());
	settings.update("filelistview/CL_Columns", pPanel->columnsWidthStr());

	settings.update("mainwindow/UsageGenericIconsForMediaTypes", true);
	settings.update("mainwindow/IconSchemeName", "default");
	settings.update("mainwindow/PathSyncBetweenTermAndFileListView", m_bPathSyncBetweenTermAndFLV);
	settings.update("mainwindow/AppCloseConfirmation", m_bAppCloseConfirmation);
	settings.update("mainwindow/ShowCommandLine", m_bShowCommandLine);
	settings.update("mainwindow/FlatButtonBar", m_bFlatButtonBar );
	settings.update("mainwindow/ShowButtonBar", m_bShowButtonBar);
	settings.update("mainwindow/ShowStatusBar", m_bShowStatusBar);
	settings.update("mainwindow/ShowMenuBar", m_bShowMenuBar);
	settings.update("mainwindow/ToolBarVisible", m_bShowHideToolBar);

	if (m_initWidth != width() || m_initHeight != height())
		settings.setValue("mainwindow/Size", QString("%1,%2").arg(width()).arg(height()));
	else
	if (settings.value("mainwindow/Size").isNull())
		settings.setValue("mainwindow/Size", "800,600");

	QString sVfsPlugins = settings.value("vfs/Plugins").toString();
	if (sVfsPlugins.isEmpty())
		settings.setValue("vfs/Plugins", m_sVfsPlugins);  // plugins separated by ","
	else
	if (! sVfsPlugins.contains("localsubsystem")) {
		      sVfsPlugins.insert(0, "localsubsystem,");
		settings.setValue("vfs/Plugins", sVfsPlugins);  // plugins separated by ","
	}
	QString sViewPlugins = settings.value("viewer/Plugins").toString();
	if (sViewPlugins.isEmpty())
		settings.setValue("viewer/Plugins", m_sViewPlugins);  // plugins separated by ","
	QString sConfigPlugins = settings.value("viewer/Plugins").toString();
	if (sConfigPlugins.isEmpty())
		settings.setValue("configuration/Plugins", m_sConfigPlugins);  // plugins separated by ","

	delete m_leftTabsPanel;
	delete m_rightTabsPanel;
	delete m_split;

	delete m_pStatusBar;
	delete m_pTermWidget;
	delete m_pCommandLine;
	delete m_pToolbarActionsAppLst;

	delete m_pFoundFileSelectionMap;
	delete m_pFoundFileInfoList;
	delete m_pFindFileDialog;

	removeTempDir();
	// sorting out here due to in PathView Tab entries are updated
	sortOutTabEntriesInConfig("left", nLeftTabCount);
	sortOutTabEntriesInConfig("right", nRightTabCount);
	settings.sync();

	delete m_pKS;
} // destructor


void MainWindow::sortOutTabEntriesInConfig( const QString &sSide, int nTabCount )
{
	// -- sort out the tabs (if it's required)
	Settings settings;
	QStringList slSettingsGroups(settings.childGroups());
	QStringList slTabNameLst = slSettingsGroups.filter(QRegExp(sSide+"PanelTab_[0-9]+"));
	QString sPanelTabName, sTabId;
	int nTabId, nMaxIdFromName=0;
	// -- find tabMaxId
	for (int i=0; i<slTabNameLst.size(); i++) {
		sPanelTabName = slTabNameLst.at(i);
		sTabId = sPanelTabName;
		sTabId.remove(sSide+"PanelTab_");
		nTabId = sTabId.toInt();
		if (nTabId > nMaxIdFromName)
			nMaxIdFromName = nTabId;
	}
	// -- check if sort out is required and do it
	if (nMaxIdFromName > nTabCount) {
		QString sKey, sValue;
		QStringList slKeys;
		QMap <QString, QString > entry2Value;
		for (int i=0; i<slTabNameLst.size(); i++) {
			sPanelTabName = slTabNameLst.at(i);
			settings.beginGroup(sPanelTabName);
			slKeys = settings.childKeys();
			settings.endGroup();
			entry2Value.clear();
			for (int j=0; j<slKeys.size(); j++) {
				sKey   = slKeys[j];
				sValue = settings.value(sPanelTabName+"/"+sKey).toString();
				entry2Value.insert(sKey, sValue);
			}
			settings.remove(sPanelTabName);
			sPanelTabName = sPanelTabName.left(sPanelTabName.indexOf('_')+1);
			sPanelTabName = QString(sPanelTabName+"%1").arg(i+1);
			for (int j=0; j<slKeys.size(); j++) {
				sKey = slKeys[j];
				settings.setValue(sPanelTabName+"/"+sKey, entry2Value.value(sKey));
			}
		}
	}
} // sortOutTabEntriesInConfig

void MainWindow::createIconCache( const Settings &settings )
{
	qDebug() << "MainWindow::createIconCache.";
	QString sCurrentIconSchemeName = settings.value("mainwindow/IconSchemeName", "default").toString();

	m_pIconCache = IconCache::instance();
	bool bUsageGenericIcons = settings.value("mainwindow/UsageGenericIconsForMediaTypes", true).toBool();
	m_pIconCache->setUsageGenericIcons(bUsageGenericIcons); // if true then use generic icons for audio, image, package, video

	QString sIconsPath, sIconDirName;
	QString sGlobalLocation, sDevLocation;
	QString sAppsPath = qApp->applicationDirPath();
	QString sSize = "32x32";

	QStringList slPathsList;
	slPathsList << "places" << "mimetypes" << "status" << "actions" << "devices";
	for (int i=0; i< slPathsList.count(); i++)
	{
		sIconsPath   = sAppsPath + "/";
		sIconDirName = slPathsList[i];
		sGlobalLocation = "../share/icons/qtcmd2/"+sCurrentIconSchemeName+"/"+sSize+"/"+sIconDirName;
		sDevLocation    = "../iconsets/"+sCurrentIconSchemeName+"/"+sSize +"/"+sIconDirName;

		if (QFile::exists(sIconsPath + sGlobalLocation))
			sIconsPath += sGlobalLocation;
		else
		if (QFile::exists(sIconsPath + sDevLocation))
			sIconsPath += sDevLocation;
		else {
			qDebug() << "MainWindow::createIconCache()."
					 << QString(tr("Not found directory icons for '%1'. Following location doesn't exist:")).arg(sIconDirName)
					 << "\n\t" << sIconsPath+sGlobalLocation << "\n\t" << sIconsPath+sDevLocation;
			continue;
		}
		m_pIconCache->addPath(sIconsPath);
	}
	m_pIconCache->updateCache();

	QFileInfo fi(sIconsPath);
	setWindowIcon(QIcon(fi.path()+"/qtcmd.png"));
} // createIconCache

void MainWindow::loadPlugins( const Settings &settings )
{
	QString sFN = "MainWindow::loadPlugins.";

	QString sAppsPath =  qApp->applicationDirPath();
// 	sAppsPath = sAppsPath.left(sAppsPath.lastIndexOf('/')); // or usage of /.. in setting pluginSearchingPathes

	QDir dir;
	QFileInfoList lst;
	QString sPluginToLoad, sPluginName;
	QStringList nameFilters;
	nameFilters << "*.so";
	dir.setNameFilters(nameFilters);

	QStringList pluginSearchingPathes;
	pluginSearchingPathes << sAppsPath + "/../lib/qtcmd2/plugins";      // path used after instalation

	m_sVfsPlugins = settings.value("vfs/Plugins", "localsubsystem,ftpsubsystem,rarsubsystem,tarsubsystem,zipsubsystem").toString();
	//m_sPlugins = m_pSettings->value("vfs/Plugins", "localsubsystem").toString();
	if (! m_sVfsPlugins.contains("localsubsystem"))
		m_sVfsPlugins.insert(0, "localsubsystem,");
	QStringList slVfsPluginsList = m_sVfsPlugins.split(',');
	if (slVfsPluginsList.isEmpty()) {
		qDebug() << sFN << "FATAL ERROR. VFS plugins not found!";
		return;
	}
	m_sViewPlugins = settings.value("viewer/Plugins", "textviewer,imageviewer,videoviewer,archiveviewer,audioviewer").toString();
	QStringList slViewPluginsList = m_sViewPlugins.split(',');
	if (slViewPluginsList.isEmpty()) {
		qDebug() << sFN << "WARNING. Viewer plugins not found!";
	}
	m_sConfigPlugins = settings.value("configuration/Plugins", "qtcmd,preview").toString();
	QStringList slConfigPluginsList = m_sConfigPlugins.split(',');
	if (slConfigPluginsList.isEmpty()) {
		qDebug() << sFN << "WARNING. Configuration plugins not found!";
	}

	for (int i=0; i<slVfsPluginsList.size(); ++i) {
		pluginSearchingPathes << sAppsPath + "/../plugins/vfs/" + slVfsPluginsList[i]; // path used by developer
	}
	for (int i=0; i<slViewPluginsList.size(); ++i) {
		pluginSearchingPathes << sAppsPath + "/../plugins/preview/" + slViewPluginsList[i]; // path used by developer
	}
	for (int i=0; i<slConfigPluginsList.size(); ++i) {
		pluginSearchingPathes << sAppsPath + "/../plugins/configuration/" + slConfigPluginsList[i]; // path used by developer
	}

	bool bConfirmExtractingForOpenWith = settings.value("archive/ConfirmExtractingForOpenWith", true).toBool();
	bool bConfirmExtractingForView = settings.value("archive/ConfirmExtractingForView", true).toBool();

	qDebug() << sFN << "---- scanning plugin's directories...";
	bool bChangeDir;
	QString sShortPluginData, sPluginClass, sMime;
	PluginFactory *pluginFactory = PluginFactory::instance();
	for (QStringList::const_iterator dirIt = pluginSearchingPathes.begin(); dirIt != pluginSearchingPathes.end(); ++dirIt) {
		qDebug() << sFN << "--- checking directory:" << *dirIt;
		bChangeDir = (QFileInfo(*dirIt).exists() && QFileInfo(*dirIt).isReadable() && QFileInfo(*dirIt).isExecutable());
		if (! bChangeDir) {
			qDebug() << sFN << "... not found or not readable/executable";
			continue;
		}
		dir.cd(*dirIt);
		lst = dir.entryInfoList();
		for (QFileInfoList::const_iterator it = lst.begin(); it != lst.end(); ++it) {
			sPluginToLoad = (*dirIt) +"/"+ (*it).fileName();
			qDebug() << sFN << "--> try load plugin:" << (*it).fileName();
			//bLoaded = pluginFactory->load(sPluginToLoad);
			sShortPluginData = pluginFactory->load(sPluginToLoad); // returns string in format: plugin_class|mime
			if (! sShortPluginData.isEmpty()) { // plugin loaded (and subsystem created)
				sPluginClass = sShortPluginData.left(sShortPluginData.indexOf('|'));
				sMime = sShortPluginData.right(sShortPluginData.length() - sShortPluginData.lastIndexOf('|') - 1);
				if (sPluginClass == "subsystem") {
					PluginManager *pPluginManager = PluginManager::instance();
					if (sMime.contains(':')) // multi-mime plugin, take first one
						sMime = sMime.split(':').at(0);
					Subsystem *pSubsysten = pPluginManager->subsystem(sMime);
					if (pSubsysten != nullptr) {
						pSubsysten->m_bConfirmExtractingForOpenWith = bConfirmExtractingForOpenWith;
						pSubsysten->m_bConfirmExtractingForView = bConfirmExtractingForView;
						pSubsysten->setWorkingDirectoryPath(m_sTmpDir);
						pSubsysten->init(this, true); // true - invoke detection (usable for archiving subsystems)
// 						qDebug() << "-- setWorkingDirectoryPath:" << m_sTmpDir << "for subsystem: " << pSubsysten << "mime:" << sMime;
					}
				}
			}
		}
	}
} // loadPlugins


void MainWindow::initMenuBar( const Settings &settings )
{
	m_bShowMenuBar = settings.value("mainwindow/ShowMenuBar", true).toBool();

	m_pMenuBar = new QMenuBar(this);

	QMenu *pFileMenu  = new QMenu(tr("&File"),  m_pMenuBar);
	QMenu *pViewMenu  = new QMenu(tr("&View"),  m_pMenuBar);
	QMenu *pToolsMenu = new QMenu(tr("&Tools"), m_pMenuBar);
	QMenu *pTabsMenu  = new QMenu(tr("&Tabs"),  m_pMenuBar);
	QMenu *pNaviMenu  = new QMenu(tr("&Navigation"),  m_pMenuBar);
	QMenu *pSettingsMenu = new QMenu(tr("&Settings"), m_pMenuBar);
	QMenu *pHelpMenu  = new QMenu(tr("&Help"),  m_pMenuBar);

	m_pMenuBar->addMenu(pFileMenu);
	m_pMenuBar->addMenu(pViewMenu);
	m_pMenuBar->addMenu(pToolsMenu);
	m_pMenuBar->addMenu(pTabsMenu);
	m_pMenuBar->addMenu(pNaviMenu);
	m_pMenuBar->addMenu(pSettingsMenu);
	m_pMenuBar->addMenu(pHelpMenu);
	setMenuBar(m_pMenuBar);

	QMenu *pOpenMenu = new QMenu(tr("Open"), m_pMenuBar);
	m_pOpenDirAct = pOpenMenu->addAction(icon("folder"), tr("Open &directory"), this, &MainWindow::slotOpenDirectory); // , QKeySequence(Qt::CTRL + Qt::Key_O (no compatible with mc, where this shortcut shows terminal)
	m_pOpenDocAct = pOpenMenu->addAction(tr("Open &document"), this, &MainWindow::slotOpenDocument);
	m_pOpenDocAct->setDisabled(true); // not yet supported

	QMenu *pArchiveMenu = new QMenu(tr("Archives"), m_pMenuBar);
	m_pOpenArchAct = pArchiveMenu->addAction(tr("Open &archive"), this, &MainWindow::slotOpenArchive);
	m_pOpenArchAct->setDisabled(true); // not yet supported
	m_pCreateNewArchAct   = pArchiveMenu->addAction(m_pKS->keyDesc(CreateNewArchive_APP),   this, &MainWindow::slotCreateNewArchive,   m_pKS->keyCode(CreateNewArchive_APP));
	m_pMoveFilesToArchAct = pArchiveMenu->addAction(m_pKS->keyDesc(MoveFilesToArchive_APP), this, &MainWindow::slotMoveFilesToArchive, m_pKS->keyCode(MoveFilesToArchive_APP));

	pFileMenu->addMenu(pOpenMenu);
	pFileMenu->addMenu(pArchiveMenu);
	pFileMenu->addAction(m_pKS->keyDesc(QuitApplication_APP), this, &MainWindow::slotQuitApp, m_pKS->keyCode(QuitApplication_APP));

	m_pOpenDirAct->setObjectName("open_dir");
	m_pOpenDocAct->setObjectName("open_document");
	m_pOpenDocAct->setObjectName("open_archive");

	m_pShowHideMainMenuAct  = pViewMenu->addAction(m_pKS->keyDesc(ShowHideMenuBar_APP),    this, &MainWindow::slotShowHideMenuBar,   m_pKS->keyCode(ShowHideMenuBar_APP));
	m_pShowHideToolBarAct   = pViewMenu->addAction(m_pKS->keyDesc(ShowHideToolBar_APP),    this, &MainWindow::slotShowHideToolBar,   m_pKS->keyCode(ShowHideToolBar_APP));
	n_pShowHideButtonBarAct = pViewMenu->addAction(m_pKS->keyDesc(ShowHideButtonBar_APP),  this, &MainWindow::slotShowHideButtonBar, m_pKS->keyCode(ShowHideButtonBar_APP));
	m_pShowHideStatusBarAct = pViewMenu->addAction(m_pKS->keyDesc(ShowHideStatusBar_APP),  this, &MainWindow::slotShowHideStatusBar, m_pKS->keyCode(ShowHideStatusBar_APP));
	m_pShowHideTermAct      = pViewMenu->addAction(icon("utilities/terminal"), m_pKS->keyDesc(ShowHideEmbdTerminal_APP), this, &MainWindow::slotShowHideTerminal, m_pKS->keyCode(ShowHideEmbdTerminal_APP));
	m_pShowHideCmdLineAct   = pViewMenu->addAction(m_pKS->keyDesc(ShowHideCmdLineBar_APP), this, &MainWindow::slotShowHideCommandLine, m_pKS->keyCode(ShowHideCmdLineBar_APP));

	m_pShowHideTermAct->setObjectName("show_hide_term");

	m_pTerminalMenu = new QMenu(tr("Terminal"), m_pMenuBar);
	m_pTerminalMenu->setIcon(icon("utilities/terminal"));
	m_pFindInTermAct = m_pTerminalMenu->addAction(tr("Find in terminal.."), this, &MainWindow::slotShowHideTerminalSearchBar, QKeySequence(Qt::CTRL+Qt::SHIFT+Qt::Key_F));
	pToolsMenu->addMenu(m_pTerminalMenu);
	m_pConnManagerAct = pToolsMenu->addAction(icon("folder/remote"), m_pKS->keyDesc(ShowConnectionManager_APP), this, &MainWindow::slotShowFtpSessionsManagerDlg, m_pKS->keyCode(ShowConnectionManager_APP));
	m_pFindFilesAct   = pToolsMenu->addAction(icon("findfiles"), m_pKS->keyDesc(FindFile_APP), this, &MainWindow::slotShowFindFilesDialog, m_pKS->keyCode(FindFile_APP));
	m_pFavFoldersAct  = pToolsMenu->addAction(icon("folder/favorites"), m_pKS->keyDesc(ShowBookmarks_APP), this, &MainWindow::slotShowFavoriteFoldersMenu, m_pKS->keyCode(ShowBookmarks_APP));
// 	m_pFavDocsAct     = pToolsMenu->addAction(icon("doc-favorites"), tr("Favorite documents"), this, &MainWindow::slotShowFavoriteDocsMenu); // top 10 most often opened docs, and marked as favorite
	m_pStoragesAct    = pToolsMenu->addAction(icon("drivers"), m_pKS->keyDesc(ShowStoragesList_APP), this, &MainWindow::slotShowStoragesMenu, m_pKS->keyCode(ShowStoragesList_APP));

	m_pFindFilesAct->setObjectName("find_files");
	m_pConnManagerAct->setObjectName("conn_manager");
	m_pFavFoldersAct->setObjectName("favorite_folders");
	m_pStoragesAct->setObjectName("storages_list");

	pTabsMenu->addAction(m_pKS->keyDesc(NewTabInCurrentPanel_APP), this, &MainWindow::slotAddTab, m_pKS->keyCode(NewTabInCurrentPanel_APP));
	pTabsMenu->addAction(m_pKS->keyDesc(CloseCurrentTab_APP),      this, &MainWindow::slotCloseCurrentTab, m_pKS->keyCode(CloseCurrentTab_APP));
	pTabsMenu->addAction(m_pKS->keyDesc(DuplicateCurrentTab_APP),  this, &MainWindow::slotDuplicateCurrentTab, m_pKS->keyCode(DuplicateCurrentTab_APP));

	m_pQuickGetOutSubsysAct = pNaviMenu->addAction(icon("go/top"), m_pKS->keyDesc(QuickGoOutFromCurrentSubSystem_APP), this, &MainWindow::slotQuickGetOutOfSubsystem, m_pKS->keyCode(QuickGoOutFromCurrentSubSystem_APP));
	m_pOneDirBackwardAct = pNaviMenu->addAction(icon("go/previous"), m_pKS->keyDesc(OneDirBackward_APP), this, &MainWindow::slotOneDirBackward, m_pKS->keyCode(OneDirBackward_APP));
	m_pOneDirForwardAct = pNaviMenu->addAction(icon("go/next"), m_pKS->keyDesc(OneDirForward_APP), this, &MainWindow::slotOneDirForward, m_pKS->keyCode(OneDirForward_APP));
	m_pGoToRootDirAct = pNaviMenu->addAction(icon("go/root"), m_pKS->keyDesc(GoToRootDirectory_APP), this, &MainWindow::slotGoToRootDirectory, m_pKS->keyCode(GoToRootDirectory_APP));
	m_pGoToHomeDirAct = pNaviMenu->addAction(icon("go/home"), m_pKS->keyDesc(GoToUserHomeDirectory_APP), this, &MainWindow::slotGoToUserHomeDirectory, m_pKS->keyCode(GoToUserHomeDirectory_APP));

	m_pQuickGetOutSubsysAct->setObjectName("quick_get_out_subsys");
	m_pOneDirBackwardAct->setObjectName("dir_backward");
	m_pOneDirForwardAct->setObjectName("dir_forward");
	m_pGoToRootDirAct->setObjectName("go_to_root_dir");
	m_pGoToHomeDirAct->setObjectName("go_to_home_dir");

	m_pConfigQtcmdAct = pSettingsMenu->addAction(icon("settings"), m_pKS->keyDesc(ConfigureApplication_APP), this, &MainWindow::slotConfigureQtCommander, m_pKS->keyCode(ConfigureApplication_APP));
// 	m_pConfigPrevAct  = pSettingsMenu->addAction(icon("settings"), m_pKS->keyDesc(ConfigurePreview_APP), this, &MainWindow::slotConfigurePreiew, m_pKS->keyCode(ConfigurePreview_APP)); // created in FileViewer class
	pSettingsMenu->addAction(icon("settings"), tr("Configure &shortcuts"), this, &MainWindow::slotConfigureQtCmdShortcuts);
	pSettingsMenu->addAction(icon("settings"), tr("Configure &toolbar"),   this, &MainWindow::slotConfigureToolbar);

	m_pConfigQtcmdAct->setObjectName("config_qtcmd");

	pHelpMenu->addAction(tr("&Help"), this, &MainWindow::slotShowHelp, Qt::Key_F1);
	pHelpMenu->addAction(tr("&About QtCommander"), this, &MainWindow::slotShowAbout);
	pHelpMenu->addAction(tr("About &Qt"), this, &MainWindow::slotShowAboutQt);

	if (m_bShowMenuBar)
		m_pShowHideMainMenuAct->setText(tr("Hide menu bar"));
	else
		m_pShowHideMainMenuAct->setText(tr("Show menu bar"));

	// -- update tool tip (in tool bar) for actions with keyshortcut
//	addKeyshortcutToActionToolTip(m_pOpenDirAct, OpenDirectory_APP);
	addKeyshortcutToActionToolTip(m_pConfigQtcmdAct, ConfigureApplication_APP);

	addKeyshortcutToActionToolTip(m_pShowHideTermAct, ShowHideEmbdTerminal_APP);

	addKeyshortcutToActionToolTip(m_pConnManagerAct, ShowConnectionManager_APP);
	addKeyshortcutToActionToolTip(m_pFindFilesAct, FindFile_APP);
	addKeyshortcutToActionToolTip(m_pFavFoldersAct, ShowBookmarks_APP);
	addKeyshortcutToActionToolTip(m_pStoragesAct, ShowStoragesList_APP);

	addKeyshortcutToActionToolTip(m_pQuickGetOutSubsysAct, QuickGoOutFromCurrentSubSystem_APP);
	addKeyshortcutToActionToolTip(m_pOneDirBackwardAct, OneDirBackward_APP);
	addKeyshortcutToActionToolTip(m_pOneDirForwardAct, OneDirForward_APP);
	addKeyshortcutToActionToolTip(m_pGoToRootDirAct, GoToRootDirectory_APP);
	addKeyshortcutToActionToolTip(m_pGoToHomeDirAct, GoToUserHomeDirectory_APP);
} // initMenuBar

void MainWindow::initToolBar( const Settings &settings )
{
	// -- prepare actions for configuration dialog (input view need to contain all actions)
	m_pBaseToolBarAG  = new QActionGroup(this);
	m_pBaseToolBarAG->setObjectName(tr("Base"));
	m_pBaseToolBarAG->addAction(m_pOpenDirAct);
	m_pBaseToolBarAG->addAction(m_pConfigQtcmdAct);

	m_pToolsToolBarAG = new QActionGroup(this);
	m_pToolsToolBarAG->setObjectName(tr("Tools"));
	m_pToolsToolBarAG->addAction(m_pShowHideTermAct);
	m_pToolsToolBarAG->addAction(m_pFindFilesAct);
	m_pToolsToolBarAG->addAction(m_pConnManagerAct);
	m_pToolsToolBarAG->addAction(m_pFavFoldersAct);
	m_pToolsToolBarAG->addAction(m_pStoragesAct);

	m_pNaviToolBarAG  = new QActionGroup(this);
	m_pNaviToolBarAG->setObjectName(tr("Navigation"));
	m_pNaviToolBarAG->addAction(m_pQuickGetOutSubsysAct);
	m_pNaviToolBarAG->addAction(m_pOneDirBackwardAct);
	m_pNaviToolBarAG->addAction(m_pOneDirForwardAct);
	m_pNaviToolBarAG->addAction(m_pGoToRootDirAct);
	m_pNaviToolBarAG->addAction(m_pGoToHomeDirAct);

	// -- for configuration dialog
	m_pToolbarActionsAppLst = new QList <QActionGroup *>;
	m_pToolbarActionsAppLst->append(m_pBaseToolBarAG);
	m_pToolbarActionsAppLst->append(m_pToolsToolBarAG);
	m_pToolbarActionsAppLst->append(m_pNaviToolBarAG);

	// useful for loading actions from confg file
	m_toolBarActionsMap.insert("open_dir",         m_pOpenDirAct);
	m_toolBarActionsMap.insert("config_qtcmd",     m_pConfigQtcmdAct);

	m_toolBarActionsMap.insert("show_hide_term",   m_pShowHideTermAct);
	m_toolBarActionsMap.insert("find_files",       m_pFindFilesAct);
	m_toolBarActionsMap.insert("conn_manager",     m_pConnManagerAct);
	m_toolBarActionsMap.insert("favorite_folders", m_pFavFoldersAct);
	m_toolBarActionsMap.insert("storages_list",    m_pStoragesAct);

	m_toolBarActionsMap.insert("quick_get_out_subsys", m_pQuickGetOutSubsysAct);
	m_toolBarActionsMap.insert("dir_backward",     m_pOneDirBackwardAct);
	m_toolBarActionsMap.insert("dir_forward",      m_pOneDirForwardAct);
	m_toolBarActionsMap.insert("go_to_root_dir",   m_pGoToRootDirAct);
	m_toolBarActionsMap.insert("go_to_home_dir",   m_pGoToHomeDirAct);

	// -- init tool bars
	m_pBaseToolBar  = new QToolBar;
	m_pToolsToolBar = new QToolBar;
	m_pNaviToolBar  = new QToolBar;

	// -- update tool bars
	m_bShowHideToolBar = settings.value("mainwindow/ToolBarVisible", true).toBool();
	if (m_bShowHideToolBar) {
		bool bBaseTBVisible  = settings.value("mainwindow/BaseToolBarVisible",  true).toBool();
		bool bToolsTBVisible = settings.value("mainwindow/ToolsToolBarVisible", true).toBool();
		bool bNaviTBVisible  = settings.value("mainwindow/NaviToolBarVisible",  true).toBool();
		m_pBaseToolBar->setVisible(bBaseTBVisible);
		m_pToolsToolBar->setVisible(bToolsTBVisible);
		m_pNaviToolBar->setVisible(bNaviTBVisible);
		updateToolBars(); // show/hide actions on toolbar
		addToolBar(m_pBaseToolBar);
		addToolBar(m_pToolsToolBar);
		addToolBar(m_pNaviToolBar);
		m_pShowHideToolBarAct->setText(tr("Hide tool bar"));
	}
	else
		m_pShowHideToolBarAct->setText(tr("Show tool bar"));
} // initToolBar

void MainWindow::initStatusBar( const Settings &settings )
{
	m_bShowStatusBar = settings.value("mainwindow/ShowStatusBar", true).toBool();

	m_pStatusBar = new QStatusBar();
	setStatusBar(m_pStatusBar);
	m_pStatusBar->setVisible(m_bShowStatusBar);

	if (m_bShowStatusBar)
		m_pShowHideStatusBarAct->setText(tr("Hide status bar"));
	else
		m_pShowHideStatusBarAct->setText(tr("Show status bar"));
}

void MainWindow::initTabPanels()
{
	m_split = new QSplitter(this);
	setCentralWidget(m_split); // will destroy all previous obj. placed on CentralWidget

	m_leftTabsPanel  = new TabPanelsWidget("left_TabWidget",  this, m_pKS, m_split);
	m_rightTabsPanel = new TabPanelsWidget("right_TabWidget", this, m_pKS, m_split);

 	connect(m_leftTabsPanel, &TabPanelsWidget::signalUpdateStatusBar, this, &MainWindow::slotRefreshStatusBar);
 	connect(m_rightTabsPanel, &TabPanelsWidget::signalUpdateStatusBar, this, &MainWindow::slotRefreshStatusBar);
 	connect(m_leftTabsPanel, &TabPanelsWidget::signalUpdateTerminalBar, this, &MainWindow::slotUpdateTerminalBar);
 	connect(m_rightTabsPanel, &TabPanelsWidget::signalUpdateTerminalBar, this, &MainWindow::slotUpdateTerminalBar);
}

void MainWindow::initEmbededTerminal( bool bShowTerminal )
{
	m_pTermWidget = new TermWidget(m_sTmpDir, this);
// 	setFocusProxy(m_pTermWidget);
	if (bShowTerminal)
		m_pTermWidget->show();
	else
		m_pTermWidget->hide();

	connect(m_pTermWidget, &QTermWidget::finished,     this, &MainWindow::slotExitTerminal);
	connect(m_pTermWidget, &QTermWidget::urlActivated, this, &MainWindow::slotUrlActivated);

	if (m_pTermWidget->isVisible())
		m_pShowHideTermAct->setText("Hide terminal");
	else
		m_pShowHideTermAct->setText("Show terminal");
}

void MainWindow::initCommandLine( const Settings &settings )
{
	m_bShowCommandLine = settings.value("mainwindow/ShowCommandLine", true).toBool();

	m_pCommandLine = new CommandLine(this);
	connect(m_pCommandLine, &CommandLine::signalExeCommandInTerminal, m_pTermWidget, &TermWidget::slotExeCommandInTerminal);
	connect(m_pCommandLine, &CommandLine::signalChangePathInTerminal, m_pTermWidget, &TermWidget::slotPathChanged);
	connect(m_pTermWidget, &TermWidget::signalPathChangedInTerminal, this, &MainWindow::slotPathChangedInTerminal);
 	addToolBarBreak(Qt::BottomToolBarArea);
	addToolBar(Qt::BottomToolBarArea, m_pCommandLine);
 	addToolBarBreak(Qt::BottomToolBarArea);

	if (m_bShowCommandLine)
		m_pShowHideCmdLineAct->setText(tr("Hide command line"));
	else
		m_pShowHideCmdLineAct->setText(tr("Show command line"));
}

void MainWindow::initButtonBar( const Settings &settings )
{
	m_bShowButtonBar = settings.value("mainwindow/ShowButtonBar", true).toBool();
	m_bFlatButtonBar = settings.value("mainwindow/FlatButtonBar", true).toBool();

	m_pButtonBar = new QToolBar(this);
	addToolBar(Qt::BottomToolBarArea, m_pButtonBar);

	QString sF2 = m_pKS->keyStr(QuickRename_APP) + " - "+tr("Rename");
	QString sF3 = m_pKS->keyStr(ViewFile_APP)    + " - "+tr("View");
	QString sF4 = m_pKS->keyStr(EditFile_APP)    + " - "+tr("Edit");
	QString sF5 = m_pKS->keyStr(CopyFiles_APP)   + " - "+tr("Copy");
	QString sF6 = m_pKS->keyStr(MoveFiles_APP)   + " - "+tr("Move");
	QString sF7 = m_pKS->keyStr(MakeDirectory_APP) + " - "+tr("Make dir");
	QString sF8 = m_pKS->keyStr(DeleteFiles_APP) + " - "+tr("Delete");
// 	QString sF9 = m_pKS->keyStr(MakeALink_APP);
	QStringList slButtonLabel;
	slButtonLabel << "F1 - "+tr("Help") << sF2 << sF3 << sF4 << sF5 << sF6 << sF7 << sF8 << "F9 - "+tr("Make a link") << "F10 - "+tr("Quit");

	const char *buttonSlotTab[] = {
		SLOT(slotShowHelp()),   // F1
		SLOT(slotRename()),     // F2
		SLOT(slotFileView()),   // F3
		SLOT(slotFileEdit()),   // F4
		SLOT(slotCopy()),       // F5
		SLOT(slotMove()),       // F6
		SLOT(slotMakeDir()),    // F7
		SLOT(slotDelete()),     // F8
		SLOT(slotMakeLink()),   // F9
		SLOT(slotQuitApp())     // F10
	};
	QPushButton *pBtn;
	QFont boldFont;
	boldFont.setWeight(QFont::Bold);
	const uint nMaxButtons = slButtonLabel.count();

	for (uint i=0; i<nMaxButtons; i++) {
		pBtn = new QPushButton(slButtonLabel.at(i), this);
		QWidget *pSpacer = new QWidget();
		pSpacer->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Fixed);
		m_pButtonBar->addWidget(pSpacer);
		pBtn->setFocusPolicy(Qt::NoFocus);
		pBtn->setFlat(m_bFlatButtonBar);
		pBtn->setFont(boldFont);
		pBtn->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
		if (i == nMaxButtons-1) // F10
			pBtn->setShortcut(Qt::Key_F10);
		connect(pBtn, SIGNAL(clicked()), this, buttonSlotTab[i]);

		m_pButtonBar->addWidget(pBtn);
		if (i != nMaxButtons-1) // put separator next to button except last button
			m_pButtonBar->addSeparator();
		m_btpButtonBar[i] = pBtn;
	}

	if (! m_bShowButtonBar) {
		m_pButtonBar->setVisible(false);
		n_pShowHideButtonBarAct->setText(tr("Show button bar"));
	}
	else {
		n_pShowHideButtonBarAct->setText(tr("Hide button bar"));
	}
} // initButtonBar

void MainWindow::initFindFileDialog()
{
	m_pFoundFileInfoList     = new FileInfoList; // for initialization foundFilesList on the FindFileDialog
	m_pFoundFileSelectionMap = new FileInfoToRowMap;
	m_pHelperSelectionMap    = new FileInfoToRowMap;

	m_pFindFileDialog = new FindFileDialog(Q_NULLPTR);
	m_pFindFileDialog->hide();

	connect(m_pFindFileDialog, &FindFileDialog::signalClose,      this, &MainWindow::slotCloseFindFilesDialog);
	connect(m_pFindFileDialog, &FindFileDialog::signalPause,      this, &MainWindow::slotPauseSearch);
	connect(m_pFindFileDialog, &FindFileDialog::signalStop,       this, &MainWindow::slotStopOfSearching);
	connect(m_pFindFileDialog, &FindFileDialog::signalStartFind,  this, &MainWindow::slotStartFindFiles);
	connect(m_pFindFileDialog, &FindFileDialog::signalPutInPanel, this, &MainWindow::slotFoundFilesPutInPanel);
	connect(m_pFindFileDialog, &FindFileDialog::signalJumpToFile, this, &MainWindow::slotJumpToFoundFile);
	connect(m_pFindFileDialog, &FindFileDialog::signalRemoveFile, this, &MainWindow::slotRemoveFoundFile);
	connect(m_pFindFileDialog, &FindFileDialog::signalViewFile,   this, &MainWindow::slotViewOfFoundFile);
}

TabPanelsWidget *MainWindow::tabPanelsWidget( bool bFromOppositeActiveTab )
{
	//Q_ASSERT(sender());
	QString sFN = "MainWindow::tabPanelsWidget.";
	QString sSenderName;
	if (sender() != NULL)
		sSenderName = sender()->objectName();
// 	qDebug() << sFN << "SenderName:" << sSenderName << "bFromOppositeActiveTab:" << bFromOppositeActiveTab;
	TabPanelsWidget *pTabPanel = nullptr;
	bool bSenderFromLeft, bSenderFromRight;

	if (sSenderName.isEmpty() || (sSenderName.indexOf("left") == -1 && sSenderName.indexOf("right") == -1)) {
		bSenderFromLeft  = m_leftTabsPanel->tabPanel(m_leftTabsPanel->currentIndex())->getFileListViewFocus();
		bSenderFromRight = m_rightTabsPanel->tabPanel(m_rightTabsPanel->currentIndex())->getFileListViewFocus();
		qDebug() << sFN << "  WARNING. bSenderFromLeft:" << bSenderFromLeft << "bSenderFromRight:" << bSenderFromRight;
		if (! bSenderFromLeft && ! bSenderFromRight) {  // both panels lost focus (maybe command line is active or some dialog)
			qDebug() << sFN << "  WARNING. No focus in both panels!";
			qDebug() << sFN << "  WARNING. Need to check past focus";
			bSenderFromLeft  = m_leftTabsPanel->tabPanel(m_leftTabsPanel->currentIndex())->getFileListViewPastFocus();
			bSenderFromRight = m_rightTabsPanel->tabPanel(m_rightTabsPanel->currentIndex())->getFileListViewPastFocus();
			qDebug() << sFN << "  leftPanel had focus:" << bSenderFromLeft << ", rightPanel had focus:" << bSenderFromRight; // FIXME both had focus (are true)
			// m_lastTabsPanel is null, because is initialized in slotShowHideTerminal
			if (! bSenderFromLeft && ! bSenderFromRight) {
				qDebug() << sFN << "  WARNING. No past focus in both panels!";
				if (m_lastTabsPanel != nullptr) {
					sSenderName = m_lastTabsPanel->objectName();
					qDebug() << sFN << "Last panel name:" << sSenderName;
					bSenderFromLeft = (sSenderName.startsWith("left"));
				}
				else {
					qDebug() << sFN << "  ERROR. Cannot determine active tab!";
					return nullptr;
				}
			}
		}
	}
	else { // -- try to detect which panel has focus
		bSenderFromLeft  = m_leftTabsPanel->tabPanel(m_leftTabsPanel->currentIndex())->getFileListViewFocus();
		bSenderFromRight = m_rightTabsPanel->tabPanel(m_rightTabsPanel->currentIndex())->getFileListViewFocus();
// 		qDebug() << sFN << "Empty or unknown sender. leftPanel has focus:" << bSenderFromLeft << ", rightPanel has focus:" << bSenderFromRight;
		if (! bSenderFromLeft && ! bSenderFromRight) {
 			//qDebug() << sFN << "  WARNING. No focus in both panels. Set as left.";
			bSenderFromLeft = (sSenderName.startsWith("left"));
		}
	}

	if (bFromOppositeActiveTab)
		pTabPanel = bSenderFromLeft ? m_rightTabsPanel : m_leftTabsPanel;
	else
		pTabPanel = bSenderFromLeft ? m_leftTabsPanel  : m_rightTabsPanel;

	return pTabPanel;
} // tabPanelsWidget

Panel *MainWindow::currentPanel( bool bCurrent )
{
	TabPanelsWidget *pTabPanel = tabPanelsWidget(! bCurrent);
	Q_ASSERT((pTabPanel != nullptr));
	return pTabPanel->tabPanel(pTabPanel->currentIndex());
}


void MainWindow::setTabTitle( const QString &sTabTitle, const QString &sSenderName, int nTabId )
{
	QString sFN = "MainWindow::setTabTitle.";
	qDebug() << sFN << "SenderName:" << sSenderName << "TabTitle:" << sTabTitle << "TabId:" << nTabId << "AppStarted:" << m_bAppStarted;
	TabPanelsWidget *tabPanelWidget = (sSenderName.startsWith("left")) ? m_leftTabsPanel : m_rightTabsPanel;
	if (nTabId > tabPanelWidget->count()) {
		qDebug() << sFN << "WARNING. TabId out of range: TabId >" << tabPanelWidget->count();
		return;
	}
	if (nTabId < 0) {
		if (m_bAppStarted)
			nTabId = tabPanelWidget->currentIndex();
		else {
			for (int i=0; i<tabPanelWidget->count(); i++ ) {
				if (tabPanelWidget->tabPanel(i)->objectName() == sSenderName) {
					nTabId = i;
					break;
				}
			}
		}
	}
	if (nTabId < 0) {
		qDebug() << sFN << "WARNING. Cannot set tab title due to not found TabId!";
		return;
	}
	if (! m_bAppStarted && ! sSenderName.isEmpty()) { // remove focus ability from newly created tab
		qDebug() << sFN << "App not started yet, InitFocusOnLeftPanel:" << m_bInitFocusOnLeftPanel;
		int id = (sSenderName.startsWith("left")) ? m_leftPanelListedTabsCount : m_rightPanelListedTabsCount;
		tabPanelWidget->tabPanel(id)->setFileListViewFocusPolicy(Qt::NoFocus);
		tabPanelWidget->tabPanel(id)->clearFileListViewFocus();
	}

	tabPanelWidget->setTabTitle(sTabTitle, nTabId);
}

void MainWindow::removeTempDir()
{
	qDebug() << "MainWindow::removeTempDir. Remove temporary directory.";
	QProcess *run = new QProcess;
#if QT_VERSION >= QT_VERSION_CHECK(5, 15, 0)
	QStringList slArgs = QString("-rf|"+m_sTmpDir).split(QLatin1Char('|'), Qt::SkipEmptyParts);
	run->start("rm", slArgs);
#else
	run->start("rm -rf "+m_sTmpDir);
#endif
}

void MainWindow::createTrashDir( const QString& sPathToTrash )
{
	qDebug() << "MainWindow::createTrashDir. Create default trash directory (if doesn't exits) like:." << sPathToTrash;
	QDir d(sPathToTrash);
 	if (! d.exists())
		d.mkdir(sPathToTrash);
}

void MainWindow::initKeyShortcuts()
{
	m_pKS = new KeyShortcuts;
	m_pKS->setKey(Help_APP, Qt::Key_F1, Qt::Key_F1, tr("Show help window"));
	m_pKS->setKey(QuickRename_APP, Qt::Key_F2, Qt::Key_F2, tr("Quick (inline) rename file"));
// 	m_pKeyShortcuts->setKey(QuickRename_APP, Qt::SHIFT+Qt::Key_F6, Qt::SHIFT+Qt::Key_F6, tr("Quick (inline) rename file")); // alternative
	m_pKS->setKey(ViewFile_APP, Qt::Key_F3, Qt::Key_F3, tr("View file or open directory"));
	m_pKS->setKey(EditFile_APP, Qt::Key_F4, Qt::Key_F4, tr("Edit file in internal editor"));
	m_pKS->setKey(CopyFiles_APP, Qt::Key_F5, Qt::Key_F5, tr("Copy file(s)"));
	m_pKS->setKey(MoveFiles_APP, Qt::Key_F6, Qt::Key_F6, tr("Move file(s)"));
	m_pKS->setKey(MakeDirectory_APP, Qt::Key_F7, Qt::Key_F7, tr("Create directory"));
	m_pKS->setKey(DeleteFiles_APP, Qt::Key_F8, Qt::Key_F8, tr("Delete selected file(s)"));
// 	m_pKeyShortcuts->setKey(DeleteFiles_APP, Qt::Key_Delete, Qt::Key_Delete, tr("Delete selected file(s)")); // alternative
	m_pKS->setKey(MakeAnEmptyFile_APP, Qt::SHIFT+Qt::Key_F4, Qt::SHIFT+Qt::Key_F4, tr("Create an empty file"));
	m_pKS->setKey(CopyFilesToCurrentDirectory_APP, Qt::SHIFT+Qt::Key_F5, Qt::SHIFT+Qt::Key_F5, tr("Make copy of currently highlighted file/directory"));
// 	m_pKeyShortcuts->setKey(QuickChangeOfFileModifiedDate_APP, Qt::SHIFT+Qt::Key_F7,  Qt::SHIFT+Qt::Key_F7,  tr("Quick change of file modified date")); // future feature
// 	m_pKeyShortcuts->setKey(QuickChangeOfFilePermission_APP,   Qt::SHIFT+Qt::Key_F8,  Qt::SHIFT+Qt::Key_F8,  tr("Quick change of file permission")); // future feature
// 	m_pKeyShortcuts->setKey(QuickChangeOfFileOwner_APP,        Qt::SHIFT+Qt::Key_F9,  Qt::SHIFT+Qt::Key_F9,  tr("Quick change of file owner")); // future feature
// 	m_pKeyShortcuts->setKey(QuickChangeOfFileGroup_APP,        Qt::SHIFT+Qt::Key_F10, Qt::SHIFT+Qt::Key_F10, tr("Quick change of file group")); // future feature
	m_pKS->setKey(GoToUpLevel_APP, Qt::Key_Backspace, Qt::Key_Backspace, tr("Go to level up"));
	m_pKS->setKey(RefreshDirectory_APP, Qt::CTRL+Qt::Key_R, Qt::CTRL+Qt::Key_R, tr("Refresh directory"));
	m_pKS->setKey(MakeALink_APP, Qt::CTRL+Qt::Key_L, Qt::CTRL+Qt::Key_L, tr("Make a link"));
	m_pKS->setKey(EditCurrentLink_APP, Qt::CTRL+Qt::Key_M, Qt::CTRL+Qt::Key_M, tr("Edit currently highlighted link"));
// 	m_pKeyShortcuts->setKey(EditCurrentLink2_APP, Qt::CTRL+Qt::SHIFT+Qt::Key_L, Qt::CTRL+Qt::SHIFT+Qt::Key_L, tr("Edit currently highlighted link"));
	m_pKS->setKey(InvertSelection_APP, Qt::Key_Asterisk, Qt::Key_Asterisk, tr("Invert current selection")); // readonly
	m_pKS->setKey(SelectFilesWithTheSameExtention_APP, Qt::CTRL+Qt::Key_E, Qt::CTRL+Qt::Key_E, tr("Select items with the same extention as currently highlighted"));
// 	m_pKeyShortcuts->setKey(SelectFilesWithTheSameExtention2_APP, Qt::ALT+Qt::Key_Plus, Qt::ALT+Qt::Key_Plus, tr("Select files with the same extention")); // alternative
	m_pKS->setKey(DeselectFilesWithTheSameExtention_APP, Qt::CTRL+Qt::SHIFT+Qt::Key_E, Qt::CTRL+Qt::SHIFT+Qt::Key_E, tr("Deselect items with the same extention"));
// 	m_pKeyShortcuts->setKey(DeselectFilesWithTheSameExtention2_APP, Qt::ALT+Qt::Key_Minus, Qt::ALT+Qt::Key_Minus, tr("Deselect items with the same extention")); // alternative
	m_pKS->setKey(SelectAllItems_APP, Qt::CTRL+Qt::Key_A, Qt::CTRL+Qt::Key_A, tr("Select all items"));
// 	m_pKeyShortcuts->setKey(SelectAllFiles2_APP, Qt::CTRL+Qt::Key_Plus, Qt::CTRL+Qt::Key_Plus, tr("Select all files")); // alternative
	m_pKS->setKey(DeselectAllFiles_APP, Qt::CTRL+Qt::SHIFT+Qt::Key_A, Qt::CTRL+Qt::SHIFT+Qt::Key_A, tr("Deselect all items"));
// 	m_pKeyShortcuts->setKey(DeselectAllFiles2_APP, Qt::CTRL+Qt::Key_Minus, Qt::CTRL+Qt::Key_Minus, tr("Deselect all files")); // alternative
	m_pKS->setKey(SelectGroupOfFiles_APP, Qt::Key_Plus, Qt::Key_Plus, tr("Select group of items"));
	m_pKS->setKey(DeselectGroupOfFiles_APP, Qt::Key_Minus, Qt::Key_Minus, tr("Deselect group of items"));
// 	m_pKeyShortcuts->setKey(ChangeViewToList_APP, Qt::ALT+Qt::CTRL+Qt::Key_I, Qt::ALT+Qt::CTRL+Qt::Key_I, tr("Change view to list")); // future feature
// 	m_pKeyShortcuts->setKey(ChangeViewToTree_APP, Qt::ALT+Qt::CTRL+Qt::Key_T, Qt::ALT+Qt::CTRL+Qt::Key_T, tr("Change view to tree")); // future feature
	m_pKS->setKey(QuickItemSearch_APP, Qt::CTRL+Qt::Key_S, Qt::CTRL+Qt::Key_S, tr("Quick item search"));
	m_pKS->setKey(NewTabInCurrentPanel_APP, Qt::CTRL+Qt::Key_N, Qt::CTRL+Qt::Key_N, tr("Creates new tab in current panel"));
	m_pKS->setKey(CloseCurrentTab_APP, Qt::CTRL+Qt::Key_W, Qt::CTRL+Qt::Key_W, tr("Closes current tab"));
	m_pKS->setKey(DuplicateCurrentTab_APP, Qt::CTRL+Qt::Key_D, Qt::CTRL+Qt::Key_D, tr("Duplicates current tab"));
	m_pKS->setKey(ShowHistory_APP, Qt::ALT+Qt::Key_Down, Qt::ALT+Qt::Key_Down, tr("Show history (list of visited locations)"));
	m_pKS->setKey(GoToPreviousURL_APP, Qt::CTRL+Qt::Key_Up, Qt::CTRL+Qt::Key_Up, tr("Go to the previous location")); // ???
// 	m_pKeyShortcuts->setKey(GoToPreviousURL2_APP, Qt::SHIFT+Qt::Key_Left, Qt::SHIFT+Qt::Key_Left, tr("Go to the previous location")); // alternative
	m_pKS->setKey(GoToNextURL_APP, Qt::CTRL+Qt::Key_Down, Qt::CTRL+Qt::Key_Down, tr("Go to next location")); // ???
//	m_pKeyShortcuts->setKey(GoToNextURL2_APP, Qt::SHIFT+Qt::Key_Right, Qt::SHIFT+Qt::Key_Right, tr("Go to next location")); // alternative
	m_pKS->setKey(GoToUserHomeDirectory_APP, Qt::CTRL+Qt::Key_Home, Qt::CTRL+Qt::Key_Home, tr("Go to user's home directory"));
	m_pKS->setKey(GoToRootDirectory_APP, Qt::CTRL+Qt::Key_Slash, Qt::CTRL+Qt::Key_Slash, tr("Go to root directory in current subsystem"));
	m_pKS->setKey(MoveCursorToPathView_APP, Qt::Key_Backtab, Qt::Key_Backtab, tr("Move cursor to path view")); // Qt::SHIFT+Qt::Key_Tab
// 	m_pKeyShortcuts->setKey(MoveCursorToPathView2_APP, Qt::ALT+Qt::Key_Up, Qt::ALT+Qt::Key_Up, tr("Move cursor to path view")); // alternative
	m_pKS->setKey(ShowConnectionManager_APP, Qt::ALT+Qt::CTRL+Qt::Key_N, Qt::ALT+Qt::CTRL+Qt::Key_N, tr("Show connection manager"));
	m_pKS->setKey(QuickItemFiltering_APP, Qt::CTRL+Qt::Key_F, Qt::CTRL+Qt::Key_F, tr("Quick item filtering"));
	m_pKS->setKey(ShowStoragesList_APP, Qt::ALT+Qt::CTRL+Qt::Key_D, Qt::ALT+Qt::CTRL+Qt::Key_D, tr("Show storage devices list"));
	m_pKS->setKey(ShowBookmarks_APP, Qt::CTRL+Qt::Key_Backslash, Qt::CTRL+Qt::Key_Backslash, tr("Show favorites locations")); //
// 	m_pKeyShortcuts->setKey(ShowBookmarks2_APP, Qt::CTRL+Qt::Key_B, Qt::CTRL+Qt::Key_B, tr("Show favorites URLs)")); // alternative
	m_pKS->setKey(FindFile_APP, Qt::ALT+Qt::Key_F7, Qt::ALT+Qt::Key_F7, tr("Find file(s)"));
	m_pKS->setKey(QuickGoOutFromCurrentSubSystem_APP, Qt::CTRL+Qt::Key_Backspace, Qt::CTRL+Qt::Key_Backspace, tr("Fast leaving of current subsystem"));
	m_pKS->setKey(OpenCurrentFileOrDirInLeftPanel_APP, Qt::CTRL+Qt::Key_Left, Qt::CTRL+Qt::Key_Left, tr("Open current directory in left panel"));
	m_pKS->setKey(OpenCurrentFileOrDirInRightPanel_APP, Qt::CTRL+Qt::Key_Right, Qt::CTRL+Qt::Key_Right, tr("Open current directory in right panel"));
	m_pKS->setKey(WeighCurrentDirectory_APP, Qt::Key_Space, Qt::Key_Space, tr("Weigh current directory"));
	m_pKS->setKey(ConfigureApplication_APP, Qt::ALT+Qt::CTRL+Qt::Key_C, Qt::ALT+Qt::CTRL+Qt::Key_C, tr("Show settings of application"));
	m_pKS->setKey(ConfigurePreview_APP, Qt::ALT+Qt::CTRL+Qt::Key_V, Qt::ALT+Qt::CTRL+Qt::Key_V, tr("Show settings of preview"));
// 	m_pKeyShortcuts->setKey(ShowSettingsOfView_APP, Qt::CTRL+Qt::SHIFT+Qt::Key_C, Qt::CTRL+Qt::SHIFT+Qt::Key_C, tr("Show settings of view")); // future feature
	m_pKS->setKey(ShowHideMenuBar_APP, Qt::ALT+Qt::CTRL+Qt::Key_M, Qt::ALT+Qt::CTRL+Qt::Key_M, tr("Show/hide menu bar"));
	m_pKS->setKey(ShowHideToolBar_APP, Qt::ALT+Qt::CTRL+Qt::Key_T, Qt::ALT+Qt::CTRL+Qt::Key_T, tr("Show/hide tool bar"));
	m_pKS->setKey(ShowHideButtonBar_APP, Qt::ALT+Qt::CTRL+Qt::Key_B, Qt::ALT+Qt::CTRL+Qt::Key_B, tr("Show/hide button bar"));
	m_pKS->setKey(ShowHideEmbdTerminal_APP, Qt::CTRL+Qt::Key_O, Qt::CTRL+Qt::Key_O, tr("Show/hide (embedeed) terminal"));
	m_pKS->setKey(ShowHideCmdLineBar_APP, Qt::ALT+Qt::CTRL+Qt::Key_C, Qt::ALT+Qt::CTRL+Qt::Key_C, tr("Show/hide command line"));
	m_pKS->setKey(ShowProperties_APP, Qt::ALT+Qt::Key_Return, Qt::ALT+Qt::Key_Return, tr("Show file properties dialog"));
	m_pKS->setKey(QuitApplication_APP, Qt::CTRL+Qt::Key_Q, Qt::CTRL+Qt::Key_Q, tr("Quit this application"));
	m_pKS->setKey(ShowContextMenu_APP, Qt::SHIFT+Qt::Key_F2, Qt::SHIFT+Qt::Key_F2, tr("Show context menu on file list view"));
// 	m_pKeyShortcuts->setKey(ShowContextMenu_APP, Qt::Key_Menu, Qt::Key_Menu, tr("Show context menu on file list view")); // alternative
	m_pKS->setKey(SelectUnselectItem_APP, Qt::Key_Insert, Qt::Key_Insert, tr("Select/Deselect currently highlighted item"));
	m_pKS->setKey(CreateNewArchive_APP, Qt::ALT+Qt::Key_F5, Qt::ALT+Qt::Key_F5, tr("Create new archive(s)"));
	m_pKS->setKey(MoveFilesToArchive_APP, Qt::ALT+Qt::Key_F6, Qt::ALT+Qt::Key_F6, tr("Move selected files/directories to new archive"));
	m_pKS->setKey(ShowHideStatusBar_APP, Qt::ALT+Qt::CTRL+Qt::Key_S, Qt::ALT+Qt::CTRL+Qt::Key_S, tr("Show/Hide status bar"));
	m_pKS->setKey(OneDirBackward_APP, Qt::ALT+Qt::Key_Left, Qt::ALT+Qt::Key_Left, tr("Path backward in history"));
	m_pKS->setKey(OneDirForward_APP, Qt::ALT+Qt::Key_Right, Qt::ALT+Qt::Key_Right, tr("Path forward in history"));

	// -- load keyshortcuts from config
	int actionId;
	Settings settings;
	QString sActionID, sKeyShortcut, sDefaulsKeyShortcut;
	QHash <int, KeyCode*>::iterator it;
	// -- set new values
	for (it = m_pKS->fistItem(); it != m_pKS->lastItem(); ++it) {
		actionId = it.key() - KeyShortcuts::ACTION_ID_OFFSET;
// 		qDebug() << "MainWindow::initKeyShortcuts. actionId:" << actionId << "desc:" << m_pKS->desc(actionId);
		sActionID = QStringLiteral("ActionID_") + QString("%1").arg(actionId+KeyShortcuts::ACTION_ID_OFFSET);
		sDefaulsKeyShortcut = m_pKS->keyDefaultStr(actionId);
		sKeyShortcut = settings.value("FilesPanelShortcuts/"+sActionID, sDefaulsKeyShortcut).toString();
// 		if (sActionID == "ActionID_1008")
// 			qDebug() << "MainWindow::initKeyShortcuts." << "sActionID:" << sActionID << "new keyShortcut:" << sKeyShortcut;
		if (sKeyShortcut != sDefaulsKeyShortcut)
			m_pKS->setKey(actionId, sKeyShortcut);
	}
	// TODO need to make validation if happened any conflict, if yes then print properly message on output.
	// This may happen in case of manually modification of config file.
} // initKeyShortcuts

void MainWindow::addKeyshortcutToActionToolTip( QAction *pAction, int nActionId )
{
	pAction->setToolTip(pAction->toolTip()+" ("+m_pKS->keyStr(nActionId)+")");
}

void MainWindow::slotApplyKeyShortcutsSettings( KeyShortcuts *pKeyShortcuts )
{
	qDebug() << "MainWindow::slotApplyKeyShortcuts";
}

void MainWindow::slotApplyListViewSettings( const ListViewSettings &listViewSettings, const QString &sCategory )
{
// 	qDebug() << "MainWindow::slotApplyListViewSettings";
	TabPanelsWidget *pLeftTabPanel  = tabPanelsWidget(false);
	TabPanelsWidget *pRightTabPanel = tabPanelsWidget(true);
	Panel *pPanel;

	for(int i=0; i < pRightTabPanel->count(); i++) {
		pPanel = pRightTabPanel->tabPanel(i);
		//qDebug() << "MainWindow::slotApplyListViewSettings. name:" << pPanel->objectName();
		pPanel->setColumnWidthSynchro(false);
		pPanel->applyListViewSettings(listViewSettings, sCategory);
		pPanel->setColumnWidthSynchro(true);
	}
	for(int i=0; i < pLeftTabPanel->count(); i++) {
		pPanel = pLeftTabPanel->tabPanel(i);
		//qDebug() << "MainWindow::slotApplyListViewSettings. name:" << pPanel->objectName();
		pPanel->setColumnWidthSynchro(false);
		pPanel->applyListViewSettings(listViewSettings, sCategory);
		pPanel->setColumnWidthSynchro(true);
	}
}

void MainWindow::slotApplyFileSystemSettings( const FileSystemSettings &fileSystemSettings )
{
// 	qDebug() << "MainWindow::slotApplyFileSystemSettings. Apply changed in FS class";
	// TODO. need to appluy in all opened tabs in each Panel
	createTrashDir(fileSystemSettings.pathToTrash);
	currentPanel(true)->applyFileSystemSettings(fileSystemSettings);  // ?? will be applied settings in all opened panels or only in active?
	currentPanel(false)->applyFileSystemSettings(fileSystemSettings);
}

void MainWindow::slotApplyToolbarsSettings( const ToolBarPageSettings &toolBarPageSettings )
{
	QList<QAction *> listOfActions;
	QString     sName              = toolBarPageSettings.tbName;
	QStringList sToolbarActionsLst = toolBarPageSettings.tbActions;
	int nActionsNum = sToolbarActionsLst.count();
// 	qDebug() << "MainWindow::slotApplyToolbars" << sName << sToolbarActionsLst;

	if (sName == "Base") {
		if (nActionsNum == 0) {
			m_pBaseToolBar->hide();
			return;
		}
		if (! m_pBaseToolBar->isVisible())
			m_pBaseToolBar->show();
		m_pBaseToolBar->clear();
		listOfActions = m_pBaseToolBarAG->actions();
	}
	else if (sName == "Tools") {
		if (nActionsNum == 0) {
			m_pToolsToolBar->hide();
			return;
		}
		if (! m_pToolsToolBar->isVisible())
			m_pToolsToolBar->show();
		m_pToolsToolBar->clear();
		listOfActions = m_pToolsToolBarAG->actions();
	}
	else if (sName == "Navigation") {
		if (nActionsNum == 0) {
			m_pNaviToolBar->hide();
			return;
		}
		if (! m_pNaviToolBar->isVisible())
			m_pNaviToolBar->show();
		m_pNaviToolBar->clear();
		listOfActions = m_pNaviToolBarAG->actions();
	}

	QAction *pAct;
	for (int i=0; i< listOfActions.count(); i++) {
		pAct = listOfActions.at(i);
		// NOTE: Cannot use setVisible. becaua action is shared with MenuBar and ToolBar
		if (sName == "Base") {
			if (sToolbarActionsLst.indexOf(pAct->objectName()) >= 0)
				m_pBaseToolBar->addAction(pAct);
		}
		else if (sName == "Tools") {
			if (sToolbarActionsLst.indexOf(pAct->objectName()) >= 0)
				m_pToolsToolBar->addAction(pAct);
		}
		else if (sName == "Navigation") {
			if (sToolbarActionsLst.indexOf(pAct->objectName()) >= 0)
				m_pNaviToolBar->addAction(pAct);
		}
	}
} // slotApplyToolbarsSettings

void MainWindow::slotAddTab()
{
	Settings settings;
	bool bSwitchToNewlyOpenedTab = settings.value("filelistview/SwitchToNewlyOpenedTab", true).toBool();

	qDebug() << "MainWindow::slotAddTab. SwitchToNewlyOpenedTab:" << bSwitchToNewlyOpenedTab;
	TabPanelsWidget *tabPanelWidget = tabPanelsWidget();

	QString sNewPanelName = sender()->objectName();
	if (sNewPanelName.isEmpty()) // called from main menu
		sNewPanelName = currentPanel()->objectName();

	sNewPanelName.replace(QRegExp("[0-9]+"), QString("%1").arg(tabPanelWidget->count()+1));
	tabPanelWidget->addTab(sNewPanelName);

	if (bSwitchToNewlyOpenedTab) {
		tabPanelWidget->setCurrentIndex(tabPanelWidget->count()-1);
		currentPanel()->setFocus();
	}
}

void MainWindow::slotDuplicateCurrentTab()
{
	Settings settings;
	bool bSwitchToDuplicatedTab = settings.value("filelistview/SwitchToDuplicatedTab", false).toBool();
	qDebug() << "MainWindow::slotDuplicateCurrentTab. SwitchToDuplicatedTab:" << bSwitchToDuplicatedTab;
	TabPanelsWidget *tabPanelWidget = tabPanelsWidget();

	QString sNewPanelName = sender()->objectName();
	if (sNewPanelName.isEmpty()) // called from main menu
		sNewPanelName = currentPanel()->objectName();

	sNewPanelName.replace(QRegExp("[0-9]+"), QString("%1").arg(tabPanelWidget->count()+1));
	tabPanelWidget->addTab(sNewPanelName, true);

	if (bSwitchToDuplicatedTab)
		tabPanelWidget->setCurrentIndex(tabPanelWidget->count()-1);
}

void MainWindow::slotCloseCurrentTab()
{
	QString sNewPanelName = sender()->objectName();
	TabPanelsWidget *tabPanelWidget = tabPanelsWidget();
	tabPanelWidget->closeTab(tabPanelWidget->currentIndex());

	if (sNewPanelName.startsWith("right"))
		m_rightPanelListedTabsCount--;
	else
	if (sNewPanelName.startsWith("left"))
		m_leftPanelListedTabsCount--;

	m_pCommandLine->setEnabled(true); // FIXME before enable CommandLine we have to check if current panel is FindResult's panel
}

void MainWindow::slotOpenInTab( const URI &uri, Vfs::OpenInTab openInTab )
{
	if (openInTab == Vfs::OPPOSITE_ACTIVE_TAB) {
		currentPanel(false)->openInOppositeActiveTab(uri);
		return;
	}
	else
	if (openInTab == Vfs::CURRENT_ACTIVE_TAB) {
 		currentPanel(false)->openInCurrentActiveTab(uri);
		return;
	}

	TabPanelsWidget *tabPanelWidget = tabPanelsWidget();
	QString sNewPanelName = sender()->objectName();
	sNewPanelName.replace(QRegExp("[0-9]+"), QString("%1").arg(tabPanelWidget->count()+1));
	qDebug() << "MainWindow::slotOpenInTab. NewPanelName:" << sNewPanelName << " OpenInTab:" << toString(openInTab);
	tabPanelWidget->addTab(sNewPanelName, uri);

	if (openInTab == Vfs::NEW_TAB) {
		Settings settings;
		bool bSwitchToNewlyOpenedTab = settings.value("filelistview/SwitchToNewlyOpenedTab", true).toBool();
		openInTab = bSwitchToNewlyOpenedTab ? Vfs::NEW_ACTIVE_TAB : Vfs::NEW_NOACTIVE_TAB;
	}
	if (openInTab == Vfs::NEW_ACTIVE_TAB) {
		tabPanelWidget->setCurrentIndex(tabPanelWidget->count()-1);
	}
}

void MainWindow::slotGetPathFromOppositeActiveTab( QString &sPath )
{
	sPath = currentPanel(false)->getCurrentURI().toString();
}

void MainWindow::slotGetPathFromTab( const QString &sTabObjName, QString &sPath )
{
	TabPanelsWidget *pTabPanel = (sTabObjName.startsWith("left")) ? m_leftTabsPanel : m_rightTabsPanel;
	Panel *pPanel = pTabPanel->tabPanel(sTabObjName);
	if (pPanel != nullptr)
		sPath = pPanel->getCurrentURI().path();
	else
		qDebug() << "MainWindow::slotGetPathFromTab. Internal ERROR. Not found panel pointer for TabObjName" << sTabObjName;
}


void MainWindow::slotCopyFilesFromOppositePanel( const QString &sTargetPath, FileInfoToRowMap *pSelectionMap, bool bCopy, bool bWeighBefore )
{
	m_sRequestingPanel = currentPanel()->objectName();
	currentPanel(false)->copyFilesTo(sTargetPath, pSelectionMap, bCopy, bWeighBefore);
}

void MainWindow::slotRemoveFiles( FileInfoToRowMap *pSelectionMap, bool bWeighBefore )
{
	m_sRequestingPanel = currentPanel()->objectName();
	currentPanel(false)->startRemoveFiles(pSelectionMap, bWeighBefore);
}

void MainWindow::slotFocusToSecondPanel()
{
	currentPanel(false)->slotSetFileListViewActive();
}

void MainWindow::slotPanelListed()
{
	QString sName = "MainWindow::slotPanelListed.";
	if (sender()->objectName().startsWith("left")) {
		if (m_leftPanelListedTabsCount != m_leftTabsPanel->count())
			m_leftPanelListedTabsCount++;
	}
	else {
		if (m_rightPanelListedTabsCount != m_rightTabsPanel->count())
			m_rightPanelListedTabsCount++;
	}
	m_bAppStarted = (m_rightPanelListedTabsCount == m_rightTabsPanel->count() && m_leftPanelListedTabsCount == m_leftTabsPanel->count()); // usable for setTabTitle
	//qDebug() << sName << "m_appStarted:" << m_bAppStarted << " InitFocusOnLeftPanel:" << m_bInitFocusOnLeftPanel;
	Panel *pPanel;
	QString sPath;
	// -- when application opened all tabs then set focus onto the left panel in current tab (will be done only one time thanks m_bInitFocusOnLeftPanel flag)
	if (m_bAppStarted && ! m_bInitFocusOnLeftPanel) {
		m_bInitFocusOnLeftPanel = true;
		Qt::FocusPolicy defaultFocusPolicy = (Qt::FocusPolicy)(Qt::TabFocus | Qt::ClickFocus | Qt::StrongFocus | Qt::WheelFocus); // 15
		m_leftTabsPanel->tabPanel(m_leftTabsPanel->currentIndex())->setFileListViewFocusPolicy(defaultFocusPolicy);
// 		m_leftTabsPanel->tabPanel(m_leftTabsPanel->currentIndex())->setFileListViewFocus();
		m_leftTabsPanel->tabPanel(m_leftTabsPanel->currentIndex())->slotSetFileListViewActive();
		//qDebug() << "MainWindow::slotPanelListed. currentIndex:" << m_leftTabsPanel->currentIndex();
		// restore focus ability for all opened panels
		for (int i = 0; i < m_leftTabsPanel->count(); ++i)
			m_leftTabsPanel->tabPanel(i)->setFileListViewFocusPolicy(defaultFocusPolicy);
		for (int i = 0; i < m_rightTabsPanel->count(); ++i)
			m_rightTabsPanel->tabPanel(i)->setFileListViewFocusPolicy(defaultFocusPolicy);

		slotUpdateTerminalBar("left");

		pPanel = currentPanel((m_bAppStarted && ! m_bInitFocusOnLeftPanel)); // false if met
		sPath = pPanel->getCurrentURI().toString();
		qDebug() << sName << "(started) Set foloowing path for cmdline:" << sPath << "name:" << pPanel->objectName();
		m_pCommandLine->setCurrentPath(sPath);
		m_pTermWidget->setCurrentPath(sPath);
		m_pLastActivePanel = pPanel;
	}
	else {
		pPanel = currentPanel(! (m_bAppStarted && ! m_bInitFocusOnLeftPanel)); // false if met
		sPath = pPanel->getCurrentURI().toString();
		qDebug() << sName << "(not started) Set foloowing path for cmdline:" << sPath << "name:" << pPanel->objectName();
		m_pCommandLine->setCurrentPath(sPath);
	}
}

void MainWindow::slotPanelListedFR( const QString &sPanelName ) // FR - Find Result (panel)
{
	if (sPanelName.startsWith("right"))
		m_rightPanelListedTabsCount++;
	else
	if (sPanelName.startsWith("left"))
		m_leftPanelListedTabsCount++;
}

void MainWindow::slotUpdateItemInAllMatchingTabs( const QString &sFileName, bool bExists ) // used after file modified
{
	qDebug() << "MainWindow::slotUpdateItemInAllMatchingTabs.";
	Panel *pPanel;
	QString sPath, sInpPath = QFileInfo(sFileName).path() + "/";
	for (int id = 0; id < m_leftTabsPanel->count(); ++id) {
		pPanel = m_leftTabsPanel->tabPanel(id);
		sPath = pPanel->getCurrentURI().toString();
// 		qDebug() << " --- sInpPath" << sInpPath << "panel" << pPanel->objectName() << "path:" << sPath;
		if (sPath == sInpPath)
			pPanel->updateItemOnLV(sFileName, bExists);
	}
	for (int id = 0; id < m_rightTabsPanel->count(); ++id) {
		pPanel = m_rightTabsPanel->tabPanel(id);
		sPath = pPanel->getCurrentURI().toString();
// 		qDebug() << " --- sInpPath" << sInpPath << "panel" << pPanel->objectName() << "path:" << sPath;
		if (sPath == sInpPath)
			pPanel->updateItemOnLV(sFileName, bExists);
	}
}

void MainWindow::slotSetMsgOnStatusBar( const QString &sMsg )
{
	if (m_bShowStatusBar && m_pStatusBar->isVisible())
		m_pStatusBar->showMessage(sMsg, 5000);
}

void MainWindow::slotRefreshStatusBar()
{
	if (m_bShowStatusBar && m_pStatusBar->isVisible())
		currentPanel()->updateStatusBar();
}

void MainWindow::slotForceUnmarkItemsInOppositeActiveTab( FileInfoList *pProcessedFilesLst )
{
	currentPanel(false)->forceUnmarkItems(pProcessedFilesLst);
}

void MainWindow::slotSwitchToTab( int nTabId ) // used only when user switches tab by Alt+NUMBER
{
// 	qDebug() << "MainWindow::slotSwitchToTab. TabId:" << nTabId;
	tabPanelsWidget(false)->setActiveTab(nTabId);
}

void MainWindow::slotShowHideTerminal()
{
	QString sFN = "MainWindow::slotShowHideTerminal.";
	if (m_pTermWidget == nullptr)
		initEmbededTerminal(true); // create and show

	QString sCurrentPath;
	bool bTermVisible = (m_pTermWidget->isVisible());
	Panel *pCurrentPanel = currentPanel(true);
	if (pCurrentPanel != m_pLastActivePanel)
		pCurrentPanel = m_pLastActivePanel;
	qDebug() << sFN << "PanelName:" << ((pCurrentPanel != nullptr) ? pCurrentPanel->objectName() : "cannot determine") << " TermVisible" << bTermVisible;

	if (bTermVisible) { // then hide terminal
		m_pShowHideTermAct->setText("Show terminal");
		sCurrentPath = m_pTermWidget->currentPath();
		qDebug() << sFN << "Hide term. Terminal currentPath:" << sCurrentPath;
		m_pTermWidget->hide();
		qDebug() << sFN << "PanelName (2):" << ((pCurrentPanel != nullptr) ? pCurrentPanel->objectName() : "cannot determine") << " TermVisible" << bTermVisible;
		if (m_bPathSyncBetweenTermAndFLV)
			pCurrentPanel->changePath(sCurrentPath);
		m_split->setVisible(true); // restore panels visibility
		pCurrentPanel->setFileListViewFocus();
	}
	else { // otherwise show terminal
		m_lastTabsPanel = tabPanelsWidget(); // remember last active panel, usueful in setting focus on correct panel after back to file list view
		m_pShowHideTermAct->setText("Hide terminal");
		sCurrentPath = m_pTermWidget->currentPath(); // path is updated in slotPanelListed()
		qDebug() << sFN << " Show term. Current panel path:" << sCurrentPath << "PathSyncBetweenTermAndFLV: " << m_bPathSyncBetweenTermAndFLV;
		m_pCommandLine->setVisible(false);
		m_split->setVisible(false); // to avoid displaying part of FileListView widget after opening search bar in TermWidget
		QPoint termPos = m_leftTabsPanel->mapTo(this, m_leftTabsPanel->pos());
		m_pTermWidget->setGeometry(termPos.x(), termPos.y(), m_split->width(), m_split->height()+m_pCommandLine->height()+contentsMargins().bottom());
		if (m_bPathSyncBetweenTermAndFLV)
			m_pTermWidget->slotPathChanged(sCurrentPath); // proper path is setting in slotPanelListed()
		m_pTermWidget->show();
		m_pTermWidget->setFocus();
	}

	if (! m_pTermWidget->isVisible())
		m_pCommandLine->setVisible(m_bShowCommandLine);
	if (m_pCommandLine->isVisible())
		m_pShowHideCmdLineAct->setText(tr("Hide command line"));
	m_pShowHideCmdLineAct->setVisible(! m_pTermWidget->isVisible());

// 	m_pFindInTermAct->setVisible(m_pTermWidget->isVisible());
	m_pTerminalMenu->setEnabled(m_pTermWidget->isVisible());

	m_pButtonBar->setVisible(m_bShowButtonBar && ! m_pTermWidget->isVisible());
}

void MainWindow::slotExitTerminal()
{
	qDebug() << "MainWindow::slotExitTerminal. Restart terminal.";
	delete m_pTermWidget; m_pTermWidget = nullptr;
	// below is necessary, because tabPanelsWidget() used indirectly in slotShowHideTerminal requires any sender
	QTimer::singleShot(5, this, &MainWindow::slotShowHideTerminal);
}

void MainWindow::slotPathChangedInTerminal()
{
	QString sPath = m_pTermWidget->currentPath();
	currentPanel(! (m_bAppStarted && ! m_bInitFocusOnLeftPanel))->clearFileListViewFocus();
	QString sCurrentPath = currentPanel((m_bAppStarted && ! m_bInitFocusOnLeftPanel))->getCurrentURI().toString();
	qDebug() << "MainWindow::slotPathChangedInTerminal. new Path:" << sPath << "currentPath:" << sCurrentPath;
	if (sPath != sCurrentPath) {
		qDebug() << "MainWindow::slotPathChangedInTerminal. Try to change path in current panel to:" << sPath;
 		currentPanel((m_bAppStarted && ! m_bInitFocusOnLeftPanel))->changePath(sPath);
		currentPanel(! (m_bAppStarted && ! m_bInitFocusOnLeftPanel))->clearFileListViewFocus();
// 		currentPanel((m_bAppStarted && ! m_bInitFocusOnLeftPanel))->setFileListViewFocus();
		m_pCommandLine->setFocus();
		QTimer::singleShot(15, this, &MainWindow::slotActivateCommandLine);
	}
	// TODO: Needs to improveme path changing in terminal. Using external tool is not reliable.
	// https://unix.stackexchange.com/questions/94357/find-out-current-working-directory-of-a-running-process
}

void MainWindow::slotShowHideTerminalSearchBar()
{
	if (m_pTermWidget->isVisible())
		m_pTermWidget->toggleShowSearchBar();
}

void MainWindow::slotUpdateTerminalBar( const QString &sPanelName )
{
	TabPanelsWidget *tabPanelWidget = (sPanelName.startsWith("left")) ? m_leftTabsPanel : m_rightTabsPanel;
	QString sCurrentPanelPath = tabPanelWidget->tabPanel(tabPanelWidget->currentIndex())->getCurrentURI().toString(); // because left panel is always active after start
	qDebug() << "MainWindow::slotUpdateTerminalBar. CurrentPanelPath:" << sCurrentPanelPath << "PanelName:" << sPanelName;
	QString sMime = MimeType::getMimeFromPath(sCurrentPanelPath);
	bool bPluginSupport = PluginFactory::instance()->isPluginAvailable(sMime, "subsystem");
	bool isSupportedArchive = (bPluginSupport && sMime != "folder" && sMime != "folder/symlink" && ! sMime.startsWith("remote/"));
	bool bLocalSubsys = (sCurrentPanelPath.startsWith('/') && ! isSupportedArchive);
	m_pCommandLine->setVisible(bLocalSubsys && m_bShowCommandLine);
	m_pShowHideTermAct->setVisible(bLocalSubsys);
	m_pShowHideCmdLineAct->setEnabled(bLocalSubsys);
}

void MainWindow::slotUpdatePathInTermianl()
{
	QString sPath;
	slotGetPathFromOppositeActiveTab(sPath);
	m_pTermWidget->setCurrentPath(sPath);
	m_pCommandLine->setCurrentPath(sPath);
}

void MainWindow::slotShowHideCommandLine()
{
	m_pCommandLine->setVisible(! m_pCommandLine->isVisible());
	m_bShowCommandLine = m_pCommandLine->isVisible();

	if (m_pCommandLine->isVisible() && m_bShowCommandLine)
		m_pShowHideCmdLineAct->setText(tr("Hide command line"));
	else
		m_pShowHideCmdLineAct->setText(tr("Show command line"));
}

void MainWindow::slotShowHideMenuBar()
{
	m_bShowMenuBar = ! m_pMenuBar->isVisible();
	m_pMenuBar->setVisible(m_bShowMenuBar);

	if (m_pMenuBar->isVisible())
		m_pShowHideMainMenuAct->setText(tr("Hide menu bar"));
	else
		m_pShowHideMainMenuAct->setText(tr("Show menu bar"));
}

void MainWindow::slotShowHideToolBar()
{
	m_bShowHideToolBar = ! m_pBaseToolBar->isVisible();
	m_pBaseToolBar->setVisible(m_bShowHideToolBar);
	m_pToolsToolBar->setVisible(m_bShowHideToolBar);

	if (m_pBaseToolBar->isVisible())
		m_pShowHideToolBarAct->setText(tr("Hide tool bar"));
	else
		m_pShowHideToolBarAct->setText(tr("Show tool bar"));
}

void MainWindow::slotShowHideButtonBar()
{
	m_bShowButtonBar = ! m_pButtonBar->isVisible();
	m_pButtonBar->setVisible(m_bShowButtonBar);

	if (m_pButtonBar->isVisible())
        n_pShowHideButtonBarAct->setText(tr("Hide button bar"));
	else
        n_pShowHideButtonBarAct->setText(tr("Show button bar"));
}

void MainWindow::slotShowHideStatusBar()
{
	m_bShowStatusBar = ! m_pStatusBar->isVisible();
	m_pStatusBar->setVisible(m_bShowStatusBar);

	if (m_pStatusBar->isVisible())
		m_pShowHideStatusBarAct->setText(tr("Hide status bar"));
	else
		m_pShowHideStatusBarAct->setText(tr("Show status bar"));
}


void MainWindow::slotConfigureQtCommander()
{
	qDebug() << "MainWindow::slotConfigureQtCommander. Try to get an object loaded with plugin."; // plugin is loaded when app.is starting
	m_pQtCmdConfigurationDialog = PluginManager::instance()->config("config/app");
	if (m_pQtCmdConfigurationDialog == nullptr) {
		MessageBox::msg(0, MessageBox::CRITICAL, tr("Configuration window of application is not available.")+"\n\n"+tr("Cannot find properly plugin:"));
		return;
	}

// 	if (m_leftTabsPanel->hasFocus())
// 		setTabOrder(m_leftTabsPanel, m_leftTabsPanel);
// 	else
// 	if (m_rightTabsPanel->hasFocus())
// 		setTabOrder(m_rightTabsPanel, m_rightTabsPanel);

	connect(m_pQtCmdConfigurationDialog, static_cast<void(ConfigurationDialog::*)(const ListViewSettings &, const QString &)>(&ConfigurationDialog::signalApply), this, &MainWindow::slotApplyListViewSettings);
	connect(m_pQtCmdConfigurationDialog, static_cast<void(ConfigurationDialog::*)(const FileSystemSettings &, const QString &)>(&ConfigurationDialog::signalApply), this, &MainWindow::slotApplyFileSystemSettings);
	connect(m_pQtCmdConfigurationDialog, &ConfigurationDialog::signalApplyKeyShortcuts, this, &MainWindow::slotApplyKeyShortcutsSettings);
	connect(m_pQtCmdConfigurationDialog, static_cast<void(ConfigurationDialog::*)(const ToolBarPageSettings &)>(&ConfigurationDialog::signalApply), this, &MainWindow::slotApplyToolbarsSettings);

	m_pQtCmdConfigurationDialog->init(m_split, m_pKS, m_pToolbarActionsAppLst);
	m_pQtCmdConfigurationDialog->raiseWindow("FilesListPage");
//	m_pQtCmdConfigurationDialog->setObjectName(objectName());

//	connect<void(Viewer::*)(Vfs::SubsystemStatus, const QString &)>(m_pViewer, &Viewer::signalStatusChanged, (SubsystemManager *)m_pSubsMgrParent, &SubsystemManager::slotSubsystemStatusChanged);
// 	setTabOrder(m_rightTabsPanel, m_leftTabsPanel);
}


void MainWindow::slotConfigureQtCmdShortcuts()
{

}

void MainWindow::slotConfigureToolbar()
{

}


void MainWindow::slotUrlActivated( const QUrl &url )
{
	qDebug() << "MainWindow::slotUrlActivated. Path:" << url.path();
}



void MainWindow::slotShowAbout()
{
//     AboutDlg aboutDlg(VERSION);
//     aboutDlg.exec();
    QMessageBox::about(this, "About QtCommander",
        "<font size=\"+1\"><B>QtCommander</B></font> - " +tr("advanced twin panels file manager")
        + "<BR>Version: <I>" QTCMD_VERSION +"</I>"
        +"<P>(C) 2012-2018 - " QTCMD_AUTHOR1", " QTCMD_AUTHOR2
        +"<P>Any bugs and wishes please report by: <A HREF=\"https://bitbucket.org/qtcmd/qtcmd2/issues?status=new&status=open\">bugtracker</A> webpage"
        +"<BR><P><I>"+tr("Distributed under the terms of the GNU General Public License v2")+"</I>"
    );
}

void MainWindow::slotShowAboutQt()
{
    QMessageBox::aboutQt(this, "QtCommander");
}

void MainWindow::slotOpenDocument()
{
// 	QString sPathToFile = QFileDialog::getOpenFileName(this, tr("Select file to open")+ " - QtCommander", QDir::rootPath());
// 	URI uri(sPathToFile);
// 	if (! sPathToFile.isEmpty())
// 		slotOpenInTab(uri, CURRENT_ACTIVE_TAB);
}

void MainWindow::slotOpenArchive()
{
 	QString sPathToFile = QFileDialog::getOpenFileName(this, tr("Select file to open")+ " - QtCommander", QDir::homePath(),
		tr("Archives (*.rar *.tar *.zip *.tar.Z *.tar.gz *.tar.bz2 *.tar.lzma *.tar.xz)")
	);
// 	URI uri(sPathToFile);
// 	if (! sPathToFile.isEmpty())
// 		slotOpenInTab(uri, CURRENT_ACTIVE_TAB);
}

void MainWindow::slotOpenDirectory()
{
	TabPanelsWidget *pTabPanel = tabPanelsWidget();
	Panel *pCurrentPanel = pTabPanel->tabPanel(pTabPanel->currentIndex());

	QString sPathToDir = QFileDialog::getExistingDirectory(this, tr("Select directory to open")+ " - QtCommander", QDir::homePath());
	URI uri(sPathToDir);
	if (! sPathToDir.isEmpty()) {
 		pCurrentPanel->openInCurrentActiveTab(uri);
		pCurrentPanel->setFileListViewActive(".");
	}
}

void MainWindow::slotUpdateColWidthInOppositePanel( int nLogicalIndex, int /*nOldSize*/, int nNewSize )
{
	TabPanelsWidget *pTabPanel = tabPanelsWidget(true);
	Panel *pOppositePanel = pTabPanel->tabPanel(pTabPanel->currentIndex());
	pOppositePanel->setColumnWidth(nLogicalIndex, nNewSize);
}

void MainWindow::updateToolBars()
{
	if (m_toolBarActionsMap.count() == 0)
		return;

	Settings settings;
	QStringList slBaseTB  = settings.value("mainwindow/BaseToolBarActions",  "open_dir, config_qtcmd").toString().remove(" ").split(",");
	QStringList slToolsTB = settings.value("mainwindow/ToolsToolBarActions", "show_hide_term, find_files, conn_manager, favorite_folders, storages_list").toString().remove(" ").split(",");
	QStringList slNaviTB  = settings.value("mainwindow/NaviToolBarActions",  "quick_get_out_subsys, dir_backward, dir_forward, go_to_root_dir, go_to_home_dir").toString().remove(" ").split(",");

	if (m_pBaseToolBar->isVisible()) {
		m_pBaseToolBar->clear();
		for (int i=0; i<slBaseTB.count(); i++)
			m_pBaseToolBar->addAction(m_toolBarActionsMap.value(slBaseTB.at(i)));
	}
	if (m_pToolsToolBar->isVisible()) {
		m_pToolsToolBar->clear();
		for (int i=0; i<slToolsTB.count(); i++)
			m_pToolsToolBar->addAction(m_toolBarActionsMap.value(slToolsTB.at(i)));
	}
	if (m_pNaviToolBar->isVisible()) {
		m_pNaviToolBar->clear();
		for (int i=0; i<slNaviTB.count(); i++)
			m_pNaviToolBar->addAction(m_toolBarActionsMap.value(slNaviTB.at(i)));
	}
}


void MainWindow::slotShowFindFilesDialog()
{
	TabPanelsWidget *pCurrentTabPanel = tabPanelsWidget(); // get current tab (where listView has foxus)
	Panel *pPanel = pCurrentTabPanel->tabPanel(pCurrentTabPanel->currentIndex());
	//qDebug() << "MainWindow::showFindFilesDialog. " << pPanel->objectName();
	m_lastTabsPanel = tabPanelsWidget(); // remember last active panel, usueful in setting focus on correct panel after back to file list view

	URI uri = pPanel->getCurrentURI();
	m_pFindFileDialog->init(uri, m_pFoundFileInfoList, m_pKS);
	pPanel->showFindFilesDialog(m_pFindFileDialog, m_pFoundFileSelectionMap);
}

void MainWindow::slotCloseFindFilesDialog()
{
	// searching operation is stoppped by signalStop sent in FindFileDialog::closeEvent
	m_pFindFileDialog->getFoundFilesList(m_pFoundFileInfoList);
	//qDebug() << "MainWindow::slotCloseFindFileDlg. Called m_pFindFileDlg->getFoundFilesList. Got items:" << m_pFoundFileInfoList->count();
	m_pFindFileDialog->hide();
	// -- set focus in last visited panel
	if (m_lastTabsPanel != nullptr) {
		Panel *pPanel = m_lastTabsPanel->tabPanel(m_lastTabsPanel->currentIndex());
		pPanel->updateStatusBar(); // WORK AROUND to set m_bMouseBtnPressed = false, because is checking in FileListView::slotUpdateFocusSelection
		pPanel->setFileListViewFocus();
	}
}

void MainWindow::slotRemoveFoundFile( FileInfo *pFileInfo, QWidget *pParent )
{
	m_pHelperSelectionMap->clear();
	m_pHelperSelectionMap->insert(pFileInfo, 0); // fake index

	currentPanel()->removeFiles(m_pHelperSelectionMap, pParent);
}

void MainWindow::slotAddMatchedItemIntoFindFileDialog( FileInfo *foundFileInfo, const QString &sSearchingResult )
{
	if (! sSearchingResult.isEmpty()) {
		QStringList slSearchingResult = sSearchingResult.split(' ');
		m_pFindFileDialog->updateSearchingStatus(slSearchingResult[0], slSearchingResult[1], slSearchingResult[2]);
		m_pFindFileDialog->addMatchedItem(foundFileInfo);
	}
}

void MainWindow::slotFoundFilesPutInPanel()
{
	m_pFindFileDialog->getFoundFilesList(m_pFoundFileInfoList);
	slotCloseFindFilesDialog();
	TabPanelsWidget *tabPanelWidget = tabPanelsWidget();

	//QString sNewPanelName = sender()->objectName();
	QString sNewPanelName = currentPanel()->getName();
	sNewPanelName.replace(QRegExp("[0-9]+"), QString("%1").arg(tabPanelWidget->count()+1));
	m_pCommandLine->setEnabled(false);
	tabPanelWidget->addTab(sNewPanelName, false, m_pFoundFileInfoList);
	tabPanelWidget->setCurrentIndex(tabPanelWidget->count()-1);

	if (sNewPanelName.startsWith("right"))
		m_rightPanelListedTabsCount++;
	else
	if (sNewPanelName.startsWith("left"))
		m_leftPanelListedTabsCount++;
}

void MainWindow::slotJumpToFoundFile( FileInfo *pFileInfo )
{
	slotStopOfSearching();
	slotCloseFindFilesDialog();
	currentPanel()->jumpToFoundFile(pFileInfo);
}

void MainWindow::slotViewOfFoundFile( FileInfo *pFileInfo, bool bEditMode )
{
	if (! currentPanel()->viewOfFoundFile(pFileInfo, bEditMode))
		slotCloseFindFilesDialog();
}



void MainWindow::closeEvent( QCloseEvent *pCE )
{
	if (m_bAppCloseConfirmation) {
		int nResult = MessageBox::yesNo(this, tr("Closing application"), "",
										tr("Do you really want to close application?"),
										m_bAppCloseConfirmation,
										MessageBox::No,
										tr("&Confirm before close")
									);
		if (nResult == MessageBox::Yes)
			pCE->accept();
		else
			pCE->ignore();
	}
}

void MainWindow::resizeEvent( QResizeEvent *pRE )
{
	if (m_pTermWidget->isVisible()) {
		QPoint termPos = m_leftTabsPanel->mapTo(this, m_leftTabsPanel->pos());
		int nStatusBarHeight = (m_pStatusBar->isVisible()) ? m_pStatusBar->height() : 0;
		m_pTermWidget->setGeometry(termPos.x(), termPos.y(), width()-contentsMargins().left(), height()-(m_pMenuBar->height()+nStatusBarHeight+contentsMargins().bottom()));
		return;
	}
	QMainWindow::resizeEvent(pRE);
}

