/***************************************************************************
 *   Copyright (C) 2012 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _TABPANELSWIDGET_H
#define _TABPANELSWIDGET_H

#include <QTabWidget>

#include "panel.h"
#include "iconcache.h"
#include "keyshortcuts.h"


class TabPanelsWidget : public QTabWidget
{
	Q_OBJECT
public:
	TabPanelsWidget( const QString &sTabTitle, QWidget *pMainWnd, KeyShortcuts *pKeyShortcuts, QWidget *pParent=0 );
	~TabPanelsWidget();

	void   addTab( const QString &sPanelName, bool bDuplicate=false, FileInfoList *pFileInfoList=NULL, const QString &sInFindResulTabTitle="" );
	void   addTab( const QString &sPanelName, const URI &uri );

	void   closeTab( int nTabId );
	void   setActiveTab( int nTabId );

	void   setTabTitle( const QString &sTabTitle, int nTabId = -1 );

	Panel *tabPanel( int nInTabId );
	Panel *tabPanel( const QString &sTabObjName );

private:
	QVector <QWidget *> m_widgetsList;
	QVector <Panel *>   m_panelsList;

	QWidget   *m_pMainWnd;

	QString    m_sPanelName;
	bool       m_bHideTabIfOpenedOnlyOne;
	int        m_clickedTabId;
	int        m_currentTabId;
	URI        m_TabURI;

	FileInfoList *m_pImportFileInfoList;
	KeyShortcuts *m_pKS;

protected:
	void mousePressEvent( QMouseEvent *me );
// 	void paintEvent( QPaintEvent *pe );

private slots:
	void slotAddTab();
	void slotCloseTab();
	void slotCloseOtherTabs();
	void slotDuplicateTab();
	void slotSetActiveList( int nTabId );
	void slotHideTabs();

signals:
	void signalUpdateStatusBar();
	void signalUpdateTerminalBar( const QString &sPanelName );

};

#endif // _TABPANELSWIDGET_H
