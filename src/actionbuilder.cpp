/***************************************************************************
 *   Copyright (C) 2017 by Mariusz Borowski <mariusz@nes.pl>               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along withthisprogram; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <QAction>

#include "actionbuilder.h"
#include "actionregister.h"
#include "settings.h"



ActionBuilder::ActionBuilder(ActionRegister *pRegister, Settings *pSettings, const QString &section)
	: m_pActionRegister(pRegister), m_pSettings(pSettings), m_sCurrentSection(section)
{
}


QAction* ActionBuilder::create(int nActionId, const QString &sName, const QKeySequence &defaultKeySequence, QObject *parent)
{
	QAction *action = new QAction(sName, parent);
	action->setData(nActionId);
	// check for key sequence in config -> "Shortcuts/" + m_sCurrentSection + "/" + nameTokey(sName)
	// 
	if (m_pSettings) {
		QString key = "Shortcuts/" + m_sCurrentSection + "/" + nameToKey(sName);
		QList<QKeySequence> shortcuts = m_pSettings->keySequenceList(key);
		if (shortcuts.empty()) { // use default key sequence
			shortcuts.append(defaultKeySequence);
		}
		
		action->setShortcuts(shortcuts);
	}
	
	// register action
	m_pActionRegister->add(m_sCurrentSection, action);
	
	return action;
}


QAction* ActionBuilder::create(int nActionId, const QString &sName, const QIcon &icon, const QKeySequence &defaultKeySequence, QObject *parent)
{
	QAction *action = create(nActionId, sName, defaultKeySequence, parent);
	action->setIcon(icon);
	
	return action;
}


QAction* ActionBuilder::createSeparator(QObject *parent)
{	// separators are not registered
	QAction *action = new QAction(parent);
	action->setSeparator(true);
	
	return action;
}


void ActionBuilder::changeSection(const QString &sSection)
{
	Q_ASSERT( ! sSection.isEmpty() );
	m_sCurrentSection = sSection;
}


QString ActionBuilder::nameToKey(const QString &sActionName)
{
	QString key = sActionName;
	key.remove("&").replace(" ", "_");
	return key;
}
