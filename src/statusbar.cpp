/***************************************************************************
 *   Copyright (C) 2002 by Piotr Mierzwi?ski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QTimer>
#include <QFrame>
#include <QDebug>

#include "statusbar.h"


StatusBar::StatusBar( QWidget *pParent, bool bShowListStatus )
	: SqueezeLabel( pParent ), m_bShowListStatus(bShowListStatus)
{
	//setFrameStyle(QFrame::Panel/* | QFrame::Sunken*/);
	setAlignment(Qt::AlignCenter);
	setFocusPolicy(Qt::NoFocus);
	setMinimumWidth(200); // contents will be scaled

	m_pProgress = new QProgressBar(this);
	m_pProgress->setAlignment(Qt::AlignHCenter); // centred precentage value
	m_pProgress->move(mapToGlobal(QPoint(0,0)));
	m_pProgress->setTextVisible(true);
	m_pProgress->setRange(0, 100);
	m_pProgress->hide();

	m_nHtmlTagsWidth = fontMetrics().horizontalAdvance("<font color=red><b></b></font><b></b><font color=red><b></b></font>");

	m_nNumOfSelectedItems = 0;  m_nNumOfAllItems = 0;  m_nWeightOfSelectedItems = 0;
	m_eSizeOfMessage = BigMSG;
	m_bShowPrevMsg   = false;
}


StatusBar::~StatusBar()
{
	delete m_pProgress;
}


void StatusBar::clear( bool bProgress )
{
	if (bProgress)
		m_pProgress->hide();
	else
		setText("");
}


void StatusBar::setStatusList()
{
	QString sNoSI = QString::number(m_nNumOfSelectedItems);
	QString sNoAI = QString::number(m_nNumOfAllItems);
	QString sWoSI = QString::number(m_nWeightOfSelectedItems);

	uint nInsertPos = sWoSI.length();
	// make (max) three digits group, eg. input = 12345,  output = 12 345
	if ( m_eSizeOfMessage != SmallMSG ) {
		while ( nInsertPos > 3 )  {
			nInsertPos -= 3;
			sWoSI.insert( nInsertPos, " " );
		}
	}

	if ( m_eSizeOfMessage == BigMSG )  {
		m_sListStatus = tr("Selected")+" <font color=red><b>"+sNoSI+"</b></font> "
			+ tr("files, from")+ " <b>"+sNoAI+"</b>, "+tr("with total size")+" <font color=red><b>"+sWoSI+"</b></font> "
			+tr("bytes");
	}
	else if ( m_eSizeOfMessage == TinyMSG )  {
		m_sListStatus = "<font color=red><b>"+sNoSI+"</b></font> "+tr("files, from")+" <b>"+sNoAI+"</b>,"
			+ tr("size")+" <font color=red><b>"+sWoSI+"</b></font>";
	}
	else if ( m_eSizeOfMessage == SmallMSG )  {
		m_sListStatus = "<font color=red><b>"+sNoSI+"</b></font> "+", <b>"+sNoAI+"</b>, "
			+"<font color=red><b>"+sWoSI+"</b></font>";
	}

	m_sStatus = m_sListStatus;
}


void StatusBar::squeezeListStatus()
{
	m_ntWidthOfListStatus[m_eSizeOfMessage] = fontMetrics().horizontalAdvance( m_sListStatus ) - m_nHtmlTagsWidth;
	uint nWidgetWidth = width();

	if ( nWidgetWidth < m_ntWidthOfListStatus[m_eSizeOfMessage]+3 ) {
		if ( m_eSizeOfMessage > SmallMSG ) {
			int nSize = m_eSizeOfMessage;
			nSize--;
			m_eSizeOfMessage = (SizeOfMessage)nSize;
			//(int)m_eSizeOfMessage -= 1;
		}
		setStatusList();
	}
	else { // widget is wider than string
		int sil = ((int)m_eSizeOfMessage+1 > BigMSG) ? (int)m_eSizeOfMessage : (int)m_eSizeOfMessage+1;
		// whether widget is wider than string (level current+1 - here: Tiny+1 = Big )
		if ( nWidgetWidth > m_ntWidthOfListStatus[sil]+3 ) { // yes, cat make wide string
			if ( m_eSizeOfMessage < BigMSG ) {
				int nSize = m_eSizeOfMessage;
				nSize++;
				m_eSizeOfMessage = (SizeOfMessage)nSize;
				//(int)m_eSizeOfMessage += 1;
			}
		}
	}

	setStatusList();
}

// -------- SLOTs --------

void StatusBar::slotSetProgress( int nDoneInPercents, long long nTotalWeight )
{
// 	qDebug() << "StatusBar::slotSetProgress" << "nDoneInPercents:" << nDoneInPercents << "isVisible:" << m_pProgress->isVisible();
	if (nDoneInPercents >= 100) {
		m_pProgress->hide();
		return;
	}
	if (! m_pProgress->isVisible()) {
		m_pProgress->resize(width()/4, height());
		m_pProgress->show();
	}
	m_pProgress->setValue(nDoneInPercents);
}


void StatusBar::slotSetMessage( const QString &sMessage, uint nMiliSec, bool bShowPrevious )
{
// 	qDebug() << "StatusBar::slotSetMessage. msg:" << sMessage << "time:" << nMiliSec << "showPrev:" << bShowPrevious;
//	if (! sMessage.isEmpty()) {
		//if (nMiliSec == 0) {
			m_sOldStatus = m_sStatus;
			//qDebug() << "StatusBar::slotSetMessage. Save old msg:" << m_sOldStatus << "nMiliSec:" << nMiliSec;
		//}
		m_bShowPrevMsg = bShowPrevious;
//	}

	m_bShowListStatus = (sMessage.contains("</b>")); // or "</font>"
	if (m_bShowListStatus) {
		setStatusList();
		squeezeListStatus();
	}
	else
		m_sStatus = sMessage;

	setText(m_sStatus);
// 	qDebug() << "::slotSetMessage. set text:" << m_sStatus;

	if (nMiliSec > 0)
		QTimer::singleShot(nMiliSec, this, &StatusBar::slotTimerDone);
}


void StatusBar::slotUpdateOfListStatus( uint nNumOfSelectedItems, uint nNumOfAllItems, qint64 nWeightOfSelected )
{
	m_nNumOfSelectedItems    = nNumOfSelectedItems;
	m_nNumOfAllItems         = nNumOfAllItems;
	m_nWeightOfSelectedItems = nWeightOfSelected;

	m_eSizeOfMessage = BigMSG;
	setStatusList();
	squeezeListStatus();

	setScaledContents(false);
	setText(m_sStatus);
}


void StatusBar::slotTimerDone()
{
// 	qDebug() << "StatusBar::slotTimerDone() m_bShowPrevMsg:" << m_bShowPrevMsg << "m_sOldStatus:" << m_sOldStatus;
	if (m_bShowPrevMsg)
		setText(m_sOldStatus);
//	setText( m_bShowPrevMsg ? m_sOldStatus : QString("") );
}

// -------- EVENTs --------

void StatusBar::resizeEvent( QResizeEvent * )
{
	if (m_pProgress->isVisible()) {
		m_pProgress->resize(width()/4, height());
		if (m_pProgress->value() == 100)
			m_pProgress->hide();
	}

	if (m_bShowListStatus) {
		squeezeListStatus();
		setText(m_sStatus);
	}
}

