/***************************************************************************
 *   Copyright (C) 2003 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef VIEWER_H
#define VIEWER_H

#include "subsystemconst.h"
#include "keyshortcuts.h"
#include "plugindata.h"
#include "textviewsettings.h"
// #include "imageviewsettings.h"
// #include "soundviewsettings.h"
// #include "videoviewsettings.h"
// #include "binaryviewsettings.h"

#include <QWidget>
#include <QActionGroup>

class QMenu;
class QToolBar;


namespace Preview {

class Viewer : public QWidget
{
	Q_OBJECT
public:
	enum ViewerType  { TEXT_viewer=0, IMAGE_viewer, SOUND_viewer, VIDEO_viewer, BINARY_viewer, UNKNOWN_viewer };
	enum ViewerMode  { RAW_mode=0, RENDER_mode, HEX_mode, EDIT_mode, UNKNOWN_mode };
	enum ReadingMode { ALL=0, APPEND, ON_DEMAND, GET_READABILITY, NONE };

	static QString      toString( ViewerType eViewerType );
	static QString      toString( ViewerMode eViewerMode );
	static QString      toString( ReadingMode eReadingMode );

	Viewer() {}
	virtual            ~Viewer() {}

	virtual void        init( QWidget *, QList <QActionGroup *> * , bool , KeyShortcuts * ) = 0;
	virtual void        initKeyShortcuts( KeyShortcuts * ) {}

	virtual PluginData *pluginData() const = 0;
	virtual void        setPluginData( PluginData *pluginData ) = 0;

	/** Status shows if plugin handles loading file itself or requires internal loading.
	 * Application supports loading internaly, but some plugins don't require that.
	 * @return TRUE if loading must be doing internally, otherwise FALSE.
	 */
	virtual bool        requiredIntLoadingOfFile() const { return true; }

	virtual ViewerMode  viewerMode() const { return UNKNOWN_mode; }
	virtual bool        viewerModeAvailable( const QString &sMime, ViewerMode eViewerMode ) const { return false; }

	/** @brief Updates the viewer.\n
	 * @param eReadingMode - reading data mode, for details please check @see Preview::Viewer::ReadingMode
	 * @param eViewerMode - view mode, for details please check @see Preview::Viewer::ViewerMode
	 * @param sMime - mime for opened file or opened file name for plugin which itself supports loading of file
	 * @param baBinBuffer - buffer contains data for filling view.\n
	 */
	virtual void        updateContents( ReadingMode eReadingMode, ViewerMode eViewerMode, const QString &sMime, const QByteArray &baBinBuffer ) = 0;

	/** Updates status bar with information about amount of read bytes.
	 */
	virtual void        updateStatus() {};

	/** Gets content of the view and put it to given buffer.\n
	 * @param baBinBuffer - buffer for data.
	 */
	virtual void        getData( QByteArray &baBinBuffer ) {}
	virtual void        clear() {}

	virtual void        setGeometry( int , int , int , int ) {}

	virtual bool        saveToFile( const QString & ) { return false; }
	virtual bool        isModified() const { return false; }

	virtual bool        findReplaceBarVisible() const { return false; }
	virtual void        setFindReplaceBarVisible( bool bVisible ) {} // used in FileViewer::closeEvent

	virtual bool        errorMessageBarVisible() const { return false; }
	virtual void        setErrorMessageBarVisible( bool bVisible ) {}

	virtual QActionGroup *editMenuBarActions() const { return 0; }
	virtual QActionGroup *viewMenuBarActions() const { return 0; }
	virtual QActionGroup *editToolBarActions() const { return 0; }
	virtual QActionGroup *viewToolBarActions() const { return 0; }

	virtual void        updateEditMenuBarActions( bool bEditMode ) {}

public slots:
	virtual void        slotSaveSettings()  {}

	virtual void        slotApplyTextViewSettings( const TextViewSettings & , const QString & ) {}

	// specific for ImageView
// 	virtual void slotApplyImageViewSettings( const ImageViewSettings & ) {}

	// specific for SoundView
// 	virtual void slotApplySoundViewSettings( const SoundViewSettings & ) {}

	// specific for VideoView
// 	virtual void slotApplyVideoViewSettings( const VideoViewSettings & ) {}

	// specific for BinaryView
// 	virtual void slotApplyBinaryViewSettings( const BinaryViewSettings & ) {}

signals:
	void signalUpdateStatus( const QString &sMessage );
	void signalUpdateStatus( const QString &sMessage, uint time, bool bShowOnViewer );
	void signalClose();
	/// used for catching errors in viewers loading file itself
	void signalStatusChanged( Vfs::SubsystemStatus status, const QString &sRequestingPanel );
	void signalDocumentChanged( bool bChanged );

};

} // namespace Preview

#endif
