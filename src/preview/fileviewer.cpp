/***************************************************************************
 *   Copyright (C) 2002 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along withthisprogram; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDebug>
#include <QMenuBar>
#include <QToolBar>
#include <QSplitter>
#include <QVBoxLayout>
#include <QTreeWidget>
#include <QFileDialog>
#include <QDesktopWidget>

#include "panel.h"
#include "settings.h"
#include "statusbar.h"
#include "fileviewer.h"
#include "messagebox.h"
#include "pluginmanager.h"
#include "pluginfactory.h"
#include "keyshortcutsappids.h"

static QList<int> sm_SW;  // width of 2 spliter obj.
static bool sm_bCanShowViewer;


FileViewer::FileViewer( FileInfoToRowMap *pSelectionMap, const QString &sHost, int currentId, bool bInWindow, bool bEditMode, const QString &sName, QWidget *pPParent, KeyShortcuts *pKeyShortcuts, QObject *pSMParent )
	: Panel(pPParent, nullptr, pKeyShortcuts, sName)
	, m_pPanelParent(pPParent), m_pSubsMgrParent(pSMParent), m_bPreviewInWindow(bInWindow), m_bInitEditMode(bEditMode)
{
	QString sFN = "FileViewer::FileViewer. (c1)";
	m_pIconCache = IconCache::instance();
	qDebug() << sFN << sName << "host:" << sHost;
	setObjectName(sName);
	m_sHost = sHost;

	if (pSelectionMap != nullptr) {
		if (pSelectionMap->isEmpty()) {
			qDebug() << sFN << "Internal ERROR. Empty SelectionMap!";
			return;
		}
	}
	FileInfo *pCurrentFI;
	if (currentId != -1)
		pCurrentFI = pSelectionMap->key(currentId);
	else {
		pCurrentFI = pSelectionMap->constBegin().key();
	}
	m_sPanelPath = pCurrentFI->filePath();
	m_sCurrentFileName = pCurrentFI->absoluteFileName();
	m_pFileInfoList = nullptr;
	m_pSettingsWidget = nullptr;
	m_pPrevieConfigDialog = nullptr;
	m_pExtractedFI = nullptr;
	m_pKS = pKeyShortcuts;

	m_bEscapePressed = false;
	m_bSkipUpdateFileInfoList = true;
	init(pSelectionMap); // invokes also updateSideList
	m_bSkipUpdateFileInfoList = false;
}

FileViewer::FileViewer( const QString &sFullFileName, bool bInWindow, bool bEditMode, const QString &sName, QWidget *pPParent, KeyShortcuts *pKeyShortcuts, QObject *pSMParent )
	: Panel(pPParent, nullptr, pKeyShortcuts, sName)
	, m_pPanelParent(pPParent), m_pSubsMgrParent(pSMParent), m_bPreviewInWindow(bInWindow), m_bInitEditMode(bEditMode)
{
	QString sFN = "FileViewer::FileViewer. (c2)";
	m_pIconCache = IconCache::instance();
	qDebug() << sFN << sName << "pKeyShortcuts:" << pKeyShortcuts;
	setObjectName(sName);

	if (sFullFileName.isEmpty()) {
		qDebug() << sFN << "Internal ERROR. Empty incoming file name!";
		return;
	}
	if (! sFullFileName.startsWith('/')) {
		if (! sFullFileName.contains("://")) {
			qDebug() << sFN << "Internal ERROR. Incoming file name:" << sFullFileName << "doesn't contain absolute path!";
			return;
		}
	}
	m_sCurrentFileName = sFullFileName;
	m_sPanelPath = QFileInfo(sFullFileName).path();
	m_pSettingsWidget = nullptr;
	m_pPrevieConfigDialog = nullptr;
	m_pExtractedFI = nullptr;
	m_pKS = pKeyShortcuts;

	m_pFileInfoList = new FileInfoList;
    QFileInfo fi(sFullFileName);
    QString sCurrentFileName = fi.fileName();
    QString sPerm = "----------";
	//QString sPerm = permissionsAsString(sCreatedDirName);
    FileInfo *pNewFileInfo = new FileInfo(sCurrentFileName, fi.absolutePath(), fi.size(), false, false, fi.lastModified(), sPerm, fi.owner(), fi.group(), MimeType::getFileMime(sFullFileName));
	m_pFileInfoList->append(pNewFileInfo);

	m_bSkipUpdateFileInfoList = false;
	init(nullptr, m_pFileInfoList); // invokes also updateSideList
}

FileViewer::~FileViewer()
{
// 	qDebug() << "FileViewer::~FileViewer. destroyAllObjects.";
	destroyAllObjects();
}

void FileViewer::destroyAllObjects()
{
// 	qDebug() << "FileViewer::destroyAllObjects.";
	delete m_pToolbarActionsLstForConfig;  m_pToolbarActionsLstForConfig = nullptr;
	//if (m_pSettingsWidget != nullptr)
		delete m_pSettingsWidget; m_pSettingsWidget = nullptr;
	// -- m_pViewer is destroed by Panel class
	delete m_pExtractedFI; m_pExtractedFI = nullptr;
	if (m_pFileInfoList != nullptr) {
		for (int id = 0; id < m_pFileInfoList->count(); ++id) {
			delete m_pFileInfoList->at(id);
		}
		delete m_pFileInfoList; m_pFileInfoList = nullptr;
	}
	delete m_pKS; m_pKS = nullptr;
// 	qDebug() << "FileViewer::destroyAllObjects (end)";
}

void FileViewer::init(Vfs::FileInfoToRowMap* pSelectionMap, Vfs::FileInfoList* pFileInfoList)
{
	QString sFN = QStringLiteral("FileViewer::init.");
	if (m_bPreviewInWindow) {
		setWindowFlags(Qt::Window);
		setWindowTitle(m_sCurrentFileName + " - QtCommander 2");
	}
	m_pViewer = nullptr;
	m_pStatusBar = nullptr;
	m_pSideListOfPreview = nullptr;
	m_pToolbarActionsLstForConfig = nullptr;
	m_pSettingsWidget = nullptr;
	m_pExtractedFI = nullptr;
	m_pViewerParent = (m_bPreviewInWindow) ? this : m_pPanelParent;
	m_slRecentFilesMenuList.clear();
	m_eTypeOfFile = UNKNOWN_file;
	sm_bCanShowViewer = true;

	// -- local path and came empty host, try to determine it (the most likely this is archive) (the case with file from archive on "Find result" tab)
	bool bInsideArchive = m_sHost.isEmpty() && ! QFileInfo(m_sCurrentFileName).exists();
	if (m_sCurrentFileName.startsWith('/') && bInsideArchive) {
		QString sHost = m_sCurrentFileName;
		if (sHost.endsWith('/'))
			sHost.remove(sHost.length()-1, 1);
		while (! QFileInfo(sHost).exists()) {
			sHost = sHost.left(sHost.lastIndexOf('/'));
		}
		m_sHost = sHost;
		m_sCurrentFileName.remove(m_sHost+"/");
		qDebug() << sFN << "found host:" << m_sHost << "updated m_sCurrentFileName to:" << m_sCurrentFileName;
	}
	m_bFileFromLocalFS = m_sCurrentFileName.startsWith('/');
	qDebug() << sFN << "Start creating layout and update sidebar list. File from localsubsystem:" << m_bFileFromLocalFS << "currentFileName:" << m_sCurrentFileName << "host:" << m_sHost;

	Settings settings;
	// -- inits: m_nInitWidth, m_nInitHeigth, m_bShowMenuBar, m_bShowToolBar and m_slPluginsNameList using config file
	if (m_bPreviewInWindow) {
		QString sSize = settings.value("viewer/Size").toString();
		m_nInitWidth  = 800; // default value
		m_nInitHeigth = 600; // default value
		bool bOK;

		if (! sSize.isEmpty()) {
			int nWidth = (sSize.left( sSize.indexOf(',') )).toInt(&bOK);
			if (bOK) {
				if (nWidth >= 200 && nWidth <= QApplication::desktop()->width())
					m_nInitWidth = nWidth;
			}
			int nHeight = (sSize.right( sSize.length()-sSize.lastIndexOf(',')-1 )).toInt(&bOK);
			if (bOK) {
				if (nHeight >= 100 && nHeight <= QApplication::desktop()->height())
					m_nInitHeigth = nHeight;
			}
		}
		m_bShowMenuBar = settings.value("viewer/ShowMenuBar", true).toBool();
	}
	else {
		m_nInitHeigth = m_pViewerParent->height();
		m_nInitWidth  = m_pViewerParent->width();
		m_bShowMenuBar = false;
	}
	m_bShowToolBar = settings.value("viewer/ShowToolBar", true).toBool();
	// m_bShowToolBar is inversed at end of initMenuBarAndToolBar by calling slotShowHideToolBar

	// --- init plugins settings
	m_slPluginsNameList.clear();
	m_slPluginsNameList << "textviewer" << "imageviewer" << "soundviewer" << "videoviewer" << "binaryviewer" << "";

	// --- Layouts
	QVBoxLayout *pMainLayout = new QVBoxLayout;
	pMainLayout->setSpacing(1);
	pMainLayout->setContentsMargins(1,1,1,1);
	// add menubar
	m_pMenuBar = new QMenuBar(m_pViewerParent);
	pMainLayout->setMenuBar(m_pMenuBar);
	// add toolbar
	m_pBaseToolBar = new QToolBar("BaseToolBar", m_pViewerParent);
	pMainLayout->addWidget(m_pBaseToolBar);

	// add splitter
	m_pSplitter = new QSplitter(Qt::Horizontal, m_pViewerParent);
	m_pSplitter->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	pMainLayout->addWidget(m_pSplitter);
	// add statusbar
	m_pStatusBar = new StatusBar(m_pViewerParent, false);
	pMainLayout->addWidget(m_pStatusBar);
	this->setLayout(pMainLayout);

	m_pSplitter->setOpaqueResize(true); // during size change still shows contains of panels
	m_pSplitter->setHandleWidth(3); // default is 6
	m_pSplitter->show();

	m_pStatusBar->setSqueezingText(true);

	initKeyShortcuts();
	initMenuBarAndToolBar();

	// --- side list
	m_pSideListOfPreview = new QTreeWidget(m_pSplitter);
	QStringList slLabels;
	slLabels << tr("Name") << tr("Location");
	m_pSideListOfPreview->setHeaderLabels(slLabels);
	m_pSideListOfPreview->setIndentation(0);
	connect(m_pSideListOfPreview, &QTreeWidget::itemActivated, this, &FileViewer::slotOpenCurrentItem);

	resize(m_nInitWidth, m_nInitHeigth);

	// - update side list
	QString sFName;
	QStringList slFilesList;
	if (pSelectionMap != nullptr) {
		FileInfoToRowMap::const_iterator it = pSelectionMap->constBegin();
		while (it != pSelectionMap->constEnd()) {
			if (m_bFileFromLocalFS)
				slFilesList << it.key()->absoluteFileName();
			else { // workaround, because all archivers creating FileInfo where path includes slash on end
				if (bInsideArchive) {
					sFName = it.key()->filePath() +"/"+ it.key()->fileName();
					sFName.remove(m_sHost+"/");
					slFilesList << sFName;
				}
				else
					slFilesList << it.key()->filePath() + it.key()->fileName();
			}
			if (bInsideArchive)
				m_FileNameToFileInfo.insert(sFName, it.key());
			else
				m_FileNameToFileInfo.insert(it.key()->absoluteFileName(), it.key());
			++it;
		}
	}
	else {
		slFilesList << m_sCurrentFileName;
		m_FileNameToFileInfo.insert(m_sCurrentFileName, pFileInfoList->at(0));
	}

	// -- init side list width
	m_nSideListWidth = width()*24/100; // 24% of total width
	uint initPreviewListWidth = 0;
	if (pSelectionMap != nullptr) {
		if (pSelectionMap->size() > 1)
			initPreviewListWidth = m_nSideListWidth;
	}
	sm_SW.clear();
	sm_SW << initPreviewListWidth << width()-initPreviewListWidth;
	m_bShowTheFilesList = (initPreviewListWidth) ? true : false;
	QString sMsg = m_bShowTheFilesList ? tr("Hide side &list") : tr("Show side &list");
	m_pSideListA->setText(sMsg);
	//m_pSideListA->setToolTip(sMsg+" ("+m_pKS->keyStr(ShowHideSideList_VIEWER)+")");

	updateSideList(slFilesList); // updates also m_FileNameToMimeMap. Method uses sm_SW

	//	-- find item and invokes file loading
	QFileInfo fi(m_sCurrentFileName);
	QTreeWidgetItem *pItem = nullptr;
	QList<QTreeWidgetItem *> twItemsList = m_pSideListOfPreview->findItems(fi.fileName(), Qt::MatchExactly, 0);
	if (twItemsList.count() > 0) { // check path
		foreach(pItem, twItemsList) {
			if (pItem->text(1) == fi.path()) {
				m_pSideListOfPreview->setCurrentItem(pItem, 0, QItemSelectionModel::SelectCurrent);
				break;
			}
		}
	}
	if (pItem != nullptr)
// 		QTimer::singleShot(0, this, SIGNAL(slotOpenCurrentFile())); // sets m_sCurrentFileName and invokes: loadingWithDelay()
		QTimer::singleShot(0, this, &FileViewer::slotOpenCurrentFile); // sets m_sCurrentFileName and invokes: loadingWithDelay()
	else
		qDebug() << sFN << "Cannot find item on side list";
}

void FileViewer::initMenuBarAndToolBar()
{
	// --- inits main menu
	// File menu
	m_pFileMenu = m_pMenuBar->addMenu(tr("&File"));
	m_pNewA     = m_pFileMenu->addAction(icon("document/new"), tr("Open in &new window"), this, &FileViewer::slotNew, m_pKS->keyCode(OpenInNewWindow_VIEWER));
	m_pOpenA    = m_pFileMenu->addAction(icon("document/open"), tr("&Open file"), this, &FileViewer::slotOpen, m_pKS->keyCode(Open_VIEWER));
	m_pRecentFilesMenu = m_pFileMenu->addMenu(tr("Open &recent files"));
	m_pSaveA    = m_pFileMenu->addAction(icon("document/save"), tr("&Save file"), this, &FileViewer::slotSave, m_pKS->keyCode(Save_VIEWER));
	m_pSaveAsA  = m_pFileMenu->addAction(icon("document/save-as"), tr("Save file &as"), this, &FileViewer::slotSaveAs, m_pKS->keyCode(SaveAs_VIEWER));
	m_pFileMenu->addSeparator();
	m_pReloadA  = m_pFileMenu->addAction(icon("view/refresh"), tr("&Reload file"), this, &FileViewer::slotReload, m_pKS->keyCode(Reload_VIEWER));
	QMenu *pSideListMenu = m_pFileMenu->addMenu(tr("Side files &list"));
	pSideListMenu->addAction(tr("&Load old files list"),    this, &FileViewer::slotLoadFilesList);
	pSideListMenu->addAction(tr("Save current files list"), this, &FileViewer::slotSaveFilesList);
	m_pQuitA    = m_pFileMenu->addAction(tr("&Quit"), this, &FileViewer::close, m_pKS->keyCode(Quit_VIEWER));

	connect(m_pRecentFilesMenu, &QMenu::triggered, this, &FileViewer::slotItemOfRecentFilesMenuActivated);

	// Edit menu
	m_pEditMenu = m_pMenuBar->addMenu(tr("&Edit"));

	// Viewer menu
	m_pViewerMenu = m_pMenuBar->addMenu(tr("&View"));

	QActionGroup *pChangeViewActGrp = new QActionGroup(this);
	m_pRawModeA    = pChangeViewActGrp->addAction(tr("Ra&w"));
	m_pRenderModeA = pChangeViewActGrp->addAction(tr("&Render"));
	m_pHexModeA    = pChangeViewActGrp->addAction(tr("&Hex"));
	m_pEditModeA   = pChangeViewActGrp->addAction(tr("&Edit"));
	pChangeViewActGrp->setExclusive(true);

	m_pRawModeA->setShortcut(m_pKS->keyCode(ToggleToRawMode_VIEWER));
	m_pRenderModeA->setShortcut(m_pKS->keyCode(ToggleToRenderMode_VIEWER));
	m_pHexModeA->setShortcut(m_pKS->keyCode(ToggleToRenderMode_VIEWER));
	m_pEditModeA->setShortcut(m_pKS->keyCode(ToggleToEditMode_VIEWER));

	m_pRawModeA->setCheckable(true);
	m_pRenderModeA->setCheckable(true);
	m_pHexModeA->setCheckable(true);
	m_pEditModeA->setCheckable(true);

	QAction *pChangeViewA = m_pViewerMenu->addAction(icon("view/mode-change"), tr("Change viewer type to")+"...");
	QMenu *pChangeViewMenu = new QMenu(m_pViewerParent);
	pChangeViewMenu->addAction(m_pRawModeA);
	pChangeViewMenu->addAction(m_pRenderModeA);
	pChangeViewMenu->addAction(m_pHexModeA);
	pChangeViewMenu->addAction(m_pEditModeA);
	pChangeViewA->setMenu(pChangeViewMenu);

	// map actions
	m_actionsModes[m_pRawModeA   ] = Viewer::RAW_mode;
	m_actionsModes[m_pRenderModeA] = Viewer::RENDER_mode;
	m_actionsModes[m_pHexModeA   ] = Viewer::HEX_mode;
	m_actionsModes[m_pEditModeA  ] = Viewer::EDIT_mode;

	connect(pChangeViewMenu, &QMenu::triggered, this, &FileViewer::slotToggleViewingMode);

	m_pViewerMenu->addSeparator();
 	m_pSideListA = m_pViewerMenu->addAction(icon("sidelist"), tr("Show/Hide side &list"), this, &FileViewer::slotShowTheFilesList, m_pKS->keyCode(ShowHideSideList_VIEWER));
	m_pMenuBarA  = m_pViewerMenu->addAction(tr("Show/Hide &menu bar"), this, &FileViewer::slotShowHideMenuBar, m_pKS->keyCode(ShowHideMenuBar_VIEWER));
	m_pToolBarA  = m_pViewerMenu->addAction(tr("Show/Hide &tool bar"), this, &FileViewer::slotShowHideToolBar, m_pKS->keyCode(ShowHideToolBar_VIEWER));

	// Options menu
	m_pOptionsMenu = m_pMenuBar->addMenu(tr("&Options"));
	m_pConfigurationA = m_pOptionsMenu->addAction(icon("configure"), tr("&Settings"), this, &FileViewer::slotShowSettings, m_pKS->keyCode(ShowSettings_VIEWER));
	m_pSaveSettingsA = m_pOptionsMenu->addAction(tr("Save settings"), this, &FileViewer::slotSaveSettings);


	// --- init ToolBar
	m_pNewA->setObjectName("new_doc");
	m_pOpenA->setObjectName("open_doc");
	m_pSaveA->setObjectName("save_doc");
	m_pSaveAsA->setObjectName("saveAs_doc");
	m_pReloadA->setObjectName("reload_doc");
	m_pSideListA->setObjectName("side_list");
	pChangeViewA->setObjectName("change_view");
	m_pConfigurationA->setObjectName("config_viewer");

	m_toolBarActionsMap.insert("new_doc", m_pNewA);
	m_toolBarActionsMap.insert("open_doc", m_pOpenA);
	m_toolBarActionsMap.insert("save_doc", m_pSaveA);
	m_toolBarActionsMap.insert("saveAs_doc", m_pSaveAsA);
	m_toolBarActionsMap.insert("reload_doc", m_pReloadA);
	m_toolBarActionsMap.insert("side_list", m_pSideListA);
	m_toolBarActionsMap.insert("change_view", pChangeViewA);
	m_toolBarActionsMap.insert("config_viewer", m_pConfigurationA);
	updateToolBars();

	// to add another toolBar needs just add new toolBar to previous by addWidget met.

	// prepare actions for configuration dialog
	m_pBaseToolBarAG  = new QActionGroup(this);
	m_pBaseToolBarAG->setObjectName(tr("Base"));
	m_pBaseToolBarAG->addAction(m_pNewA);
	m_pBaseToolBarAG->addAction(m_pOpenA);
	m_pBaseToolBarAG->addAction(m_pSaveA);
	m_pBaseToolBarAG->addAction(m_pSaveAsA);
	m_pBaseToolBarAG->addAction(m_pReloadA);
	m_pBaseToolBarAG->addAction(pChangeViewA);
	m_pBaseToolBarAG->addAction(m_pConfigurationA);
	m_pBaseToolBarAG->addAction(m_pSideListA);
	// next actions come from opened view type (textview etc)

	m_pToolbarActionsLstForConfig = new QList <QActionGroup *>;
	m_pToolbarActionsLstForConfig->append(m_pBaseToolBarAG);
	// in createView are added another action by procedure m_pViewer->init

	// set tooltip for all actions
	setKeyshortcutToActionToolTip(m_pNewA, OpenInNewWindow_VIEWER);
	setKeyshortcutToActionToolTip(m_pOpenA, Open_VIEWER);
	setKeyshortcutToActionToolTip(m_pSaveA, Save_VIEWER);
	setKeyshortcutToActionToolTip(m_pSaveAsA, SaveAs_VIEWER);
	setKeyshortcutToActionToolTip(m_pReloadA, Reload_VIEWER);
	setKeyshortcutToActionToolTip(m_pSideListA, ShowHideSideList_VIEWER);
	//addKeyshortcutToActionToolTip(pChangeViewA, );
	setKeyshortcutToActionToolTip(m_pConfigurationA, ShowSettings_VIEWER);

	if (! m_bPreviewInWindow) {
		m_pBaseToolBar->addAction(icon("view/close"), tr("Close viewer"), this, &FileViewer::signalClose)->setShortcut(Qt::Key_Escape);
	}
	slotShowHideMenuBar();
	slotShowHideToolBar();
}

void FileViewer::updateMenuBar()
{
	// activate an appropriate action
	QMap<QAction*, Viewer::ViewerMode>::iterator it = std::find(m_actionsModes.begin(), m_actionsModes.end(), m_eViewerMode);
	Q_ASSERT(it != m_actionsModes.end());
	it.key()->setChecked(true);

	m_pSaveA->setVisible((m_eViewerMode == Viewer::EDIT_mode && m_eViewerType == Viewer::TEXT_viewer));
	// hide "Save as" for views unsupported this option
	QString sFileMime = MimeType::getFileMime(m_sCurrentFileName);
	bool bAudioFile = sFileMime.startsWith("audio/");
	bool bVideoFile = sFileMime.startsWith("video/");
	bool bArchive= PluginFactory::instance()->isPluginAvailable(sFileMime, "subsystem");
	m_pSaveAsA->setVisible(! bAudioFile && ! bVideoFile && ! bArchive);

	if (! m_bIsItTextFile) { // binary file
		m_pSaveA->setVisible(false);
		m_pEditModeA->setVisible(false);
	}
	else { // it's text file
		// edit mode is not yet supported for archives and remote locations, so there is need to disable it
		m_pEditModeA->setVisible(m_sCurrentFileName.startsWith(QDir::rootPath()));
		// info: m_sHost for remote location contains for example: ftp.gnome.org and for archive full path of archive
	}

	m_pRenderModeA->setVisible(true);
	// FIXME set properly RenderMode (check if this is binary viewer and not supported - by internal - mime)
// 	if (m_eViewerType == Viewer::BINARY_viewer && (m_eTypeOfFile == ARCHIVE_file || m_eTypeOfFile == UNKNOWN_file))
// 		m_pRenderModeA->setVisible(false);
// 	if (m_eViewerType == Viewer::TEXT_viewer && (m_eTypeOfFile == SOURCE_file || m_eTypeOfFile == UNKNOWN_file))
// 		m_pRenderModeA->setVisible(false);

	m_pSaveA->setEnabled(m_pEditModeA->isChecked()); // enable/disable save
	QString sMime = m_FileNameToMimeMap[m_sCurrentFileName];
	m_pRenderModeA->setEnabled(m_pViewer->viewerModeAvailable(sMime, Viewer::RENDER_mode));
	m_pRawModeA->setVisible(m_pViewer->viewerModeAvailable(sMime, Viewer::RAW_mode));
	m_pHexModeA->setEnabled(false); // TODO implemnt binary view
}

void FileViewer::initKeyShortcuts()
{
	m_pKS = new KeyShortcuts;
// 	qDebug() << "FileViewer::initKeyShortcuts.";

	//setKey( int nAction, int nShortcut, int nDefaultCode, QString & sDescription )
	m_pKS->setKey(Help_VIEWER, Qt::Key_F1, Qt::Key_F1, tr("Help"));
	m_pKS->setKey(Reload_VIEWER, Qt::Key_F5, Qt::Key_F5, tr("Reload"));

	m_pKS->setKey(Open_VIEWER, Qt::CTRL+Qt::Key_O, Qt::CTRL+Qt::Key_O, tr("Open"));
	m_pKS->setKey(OpenInNewWindow_VIEWER, Qt::CTRL+Qt::Key_N, Qt::CTRL+Qt::Key_N, tr("Open in new window"));
	m_pKS->setKey(Save_VIEWER, Qt::CTRL+Qt::Key_S, Qt::CTRL+Qt::Key_S, tr("Save"));
	m_pKS->setKey(SaveAs_VIEWER, Qt::ALT+Qt::CTRL+Qt::Key_S, Qt::ALT+Qt::CTRL+Qt::Key_S, tr("Save as"));

	m_pKS->setKey(ShowHideMenuBar_VIEWER, Qt::CTRL+Qt::Key_M, Qt::CTRL+Qt::Key_M,  tr("Show/hide menu bar"));
	m_pKS->setKey(ShowHideToolBar_VIEWER, Qt::CTRL+Qt::Key_T, Qt::CTRL+Qt::Key_T,  tr("Show/hide tool bar"));
	m_pKS->setKey(ShowHideSideList_VIEWER, Qt::CTRL+Qt::Key_L, Qt::CTRL+Qt::Key_L, tr("Show/hide side list"));
	m_pKS->setKey(ToggleToRawMode_VIEWER, Qt::ALT+Qt::CTRL+Qt::Key_W, Qt::ALT+Qt::CTRL+Qt::Key_W, tr("Toggle to Raw mode"));
	m_pKS->setKey(ToggleToEditMode_VIEWER, Qt::Key_F4, Qt::Key_F4, tr("Toggle to Edit mode"));
	m_pKS->setKey(ToggleToRenderMode_VIEWER, Qt::ALT+Qt::CTRL+Qt::Key_R, Qt::ALT+Qt::CTRL+Qt::Key_R, tr("Toggle to Render mode"));
	m_pKS->setKey(ToggleToHexMode_VIEWER, Qt::ALT+Qt::CTRL+Qt::Key_H, Qt::ALT+Qt::CTRL+Qt::Key_H, tr("Toggle to Hex mode"));
	m_pKS->setKey(ShowSettings_VIEWER, Qt::CTRL+Qt::SHIFT+Qt::Key_S, Qt::CTRL+Qt::SHIFT+Qt::Key_S, tr("Show settings"));
	m_pKS->setKey(Quit_VIEWER, Qt::CTRL+Qt::Key_Q, Qt::CTRL+Qt::Key_Q, tr("Quit"));

	m_pKS->updateEntryList( "FileViewerShortcuts/"); // update from config file

	// -- load keyshortcuts from config
	int actionId;
	Settings settings;
	QString sActionID, sKeyShortcut, sDefaulsKeyShortcut;
	QHash <int, KeyCode*>::iterator it;
	// -- set new values
	for (it = m_pKS->fistItem(); it != m_pKS->lastItem(); ++it) {
		actionId = it.key() - KeyShortcuts::ACTION_ID_OFFSET;
// 		qDebug() << "FileViewer::initKeyShortcuts. actionId:" << actionId << "desc:" << m_pKS->desc(actionId);
		sActionID = QStringLiteral("ActionID_") + QString("%1").arg(actionId+KeyShortcuts::ACTION_ID_OFFSET);
		sDefaulsKeyShortcut = m_pKS->keyDefaultStr(actionId);
		sKeyShortcut = settings.value("FileViewShortcuts/"+sActionID, sDefaulsKeyShortcut).toString();
// 		if (sActionID == "ActionID_1008")
// 			qDebug() << "FileViewer::initKeyShortcuts." << "sActionID:" << sActionID << "new keyShortcut:" << sKeyShortcut;
		if (sKeyShortcut != sDefaulsKeyShortcut)
			m_pKS->setKey(actionId, sKeyShortcut);
	}
	// TODO need to make validation if happened any conflict, if yes then print properly message on output.
	// This may happen in case of manually modification of config file.

	//slotApplyKeyShortcuts(m_pKS);
}


bool FileViewer::createView( const QString &sInMime, Viewer::ViewerType eInViewerType )
{
	QString sFN = "FileViewer::createView.";
	qDebug() << sFN << "Incomming mime:" << sInMime << "viewerType:" << Viewer::toString(eInViewerType);
	Viewer::ViewerType eViewerType = eInViewerType;
	// --- detect kind of file and init viewer type
	if (eViewerType == Viewer::UNKNOWN_viewer) { // then try to recognize it
		qDebug() << sFN << "Came 'UNKNOWN_viewer'. Start detection of viewer type...";
// 		if (m_pFilesAssociation) {
// 			if (m_pFilesAssociation->loaded())
// 				m_eTypeOfFile = m_pFilesAssociation->typeOfFile(pFileInfo);
// 			else
// 				m_eTypeOfFile = typeOfFile(pFileInfo); // FilesAssociation not loaded
// 		}
// 		else // FilesAssociation not loaded
			m_eTypeOfFile = typeOfFile(sInMime); // use mime for detect

		if (m_eTypeOfFile == IMAGE_file)  eViewerType = Viewer::IMAGE_viewer;
		else
		if (m_eTypeOfFile == SOUND_file)  eViewerType = Viewer::SOUND_viewer;
		else
		if (m_eTypeOfFile == VIDEO_file)  eViewerType = Viewer::VIDEO_viewer;
		else
		if (! m_bIsItTextFile && (m_eTypeOfFile == UNKNOWN_file || m_eTypeOfFile == BINARY_file))
			eViewerType = Viewer::BINARY_viewer;
		else
			eViewerType = Viewer::TEXT_viewer;
	}
	qDebug() << sFN << "Detected ViewerType:" << Viewer::toString(eViewerType) << "Detected TypeOfFile:" << m_eTypeOfFile;

	if (m_pViewer != nullptr) {
		if (eViewerType == m_eViewerType) { // attempt to change to the same viewer type
			qDebug() << sFN << "Attempt of change to the same viewer type, no need to create view";
			return true;
		}
		else {
			qDebug() << sFN << "Destroy old viewer";
			delete m_pViewer;  m_pViewer = nullptr;
		}
	}
	// --- get object loaded with plugin
	bool bNewlyCreateView = false;
	if (m_pViewer == nullptr) {
		qDebug() << sFN << "Try to get an object loaded with plugin."; // plugin is loaded when app.is starting
		m_pViewer = PluginManager::instance()->viewer(sInMime);
		if (m_pViewer == nullptr) {
			QApplication::restoreOverrideCursor();
			MessageBox::msg(this, MessageBox::CRITICAL, tr("Internal viewer is not available.")+"\n\n"+tr("Cannot find plugin related with mime:")+"\n\t"+sInMime);
			//TODO: When binary view will be available then we can ask: Do you want to use binary viewer?
			return false;
		}
		m_pViewer->init(this, m_pToolbarActionsLstForConfig, ! m_bShowToolBar, m_pKS);
		//m_pViewer->initKeyShortcuts(m_pKS); // called inside of init()
		m_pViewer->setObjectName(objectName());
		// to be able to catch changes in state of viewer for viewers handling file loading itself
// 		connect(m_pViewer, SIGNAL(signalStatusChanged(Vfs::SubsystemStatus, const QString &)), m_pSubsMgrParent, SLOT(slotSubsystemStatusChanged(Vfs::SubsystemStatus, const QString &)));
		connect<void(Viewer::*)(Vfs::SubsystemStatus, const QString &)>(m_pViewer, &Viewer::signalStatusChanged, (SubsystemManager *)m_pSubsMgrParent, &SubsystemManager::slotSubsystemStatusChanged);

		m_pSplitter->addWidget(m_pViewer);
		bNewlyCreateView = true;
	}
	// Viewer already created
	m_pSplitter->setSizes(sm_SW);
	m_pSideListOfPreview->resize(sm_SW[0], m_pSplitter->height());

	// --- initialize menu bar, status bar and tool bar
	m_eViewerType = eViewerType;

	qDebug() << sFN << "Viewer:" << Viewer::toString(m_eViewerType) << (bNewlyCreateView ? "has been" : "already") << "created";
	if (! bNewlyCreateView)
		return true;

	m_pViewer->clear();
	// - update the actions for menu bar
	if (m_eViewerType == Viewer::TEXT_viewer || m_eViewerType == Viewer::BINARY_viewer) {
		QActionGroup *pEditMenu = m_pViewer->editMenuBarActions();
		QActionGroup *pViewMenu = m_pViewer->viewMenuBarActions();
		qDebug() << "------ pEditMenu count: " << pEditMenu->actions().count();
		qDebug() << "------ pViewMenu count: " << pViewMenu->actions().count();

		if (pEditMenu != nullptr) { // actions available
			foreach (QAction *pAction, pEditMenu->actions()) {
				m_pEditMenu->addAction(pAction);
				qDebug() << "------ m_pEditMenu,   add pAction" << pAction->objectName() << pAction->toolTip() << pAction->statusTip() << pAction->text();
			}
		}
		if (pViewMenu != nullptr) {
			foreach (QAction *pAction, pViewMenu->actions()) {
				m_pViewerMenu->addAction(pAction);
				qDebug() << "------ m_pViewerMenu, add pAction" << pAction->objectName() << pAction->toolTip() << pAction->statusTip() << pAction->text();
			}
		}
	}
	// - update the actions for tool bars
	//m_pSpecificViewToolBar->clear();
	if (m_pViewer->viewToolBarActions() != nullptr) { // actions available
		foreach (QAction *pAction, m_pViewer->viewToolBarActions()->actions())
			m_pBaseToolBar->addAction(pAction);
	}
	if (m_pViewer->editToolBarActions() != nullptr) {
		foreach (QAction *pAction, m_pViewer->editToolBarActions()->actions())
			m_pBaseToolBar->addAction(pAction);
	}

	// - initialize status bar
	if (m_pStatusBar != nullptr) {
		m_pStatusBar->showNormal();
 		connect(m_pViewer, static_cast<void (Viewer::*)(const QString &, uint, bool)>(&Viewer::signalUpdateStatus), m_pStatusBar, static_cast<void (StatusBar::*)(const QString &, uint, bool)>(&StatusBar::slotSetMessage));
 		connect(m_pViewer, static_cast<void (Viewer::*)(const QString &)>(&Viewer::signalUpdateStatus), m_pStatusBar, static_cast<void (StatusBar::*)(const QString &)>(&StatusBar::slotSetMessage));
	}
	connect(m_pViewer, &Viewer::signalDocumentChanged, this, &FileViewer::slotDocumentChanged);

	m_pViewer->updateEditMenuBarActions(m_bInitEditMode);

	return true;
}

void FileViewer::updateToolBars()
{
	if (m_toolBarActionsMap.count() == 0)
		return;

	Settings settings;
	QStringList slBaseTB = settings.value("viewer/BaseToolBarActions", "new_doc, open_doc, save_doc, saveAs_doc, reload_doc, change_view, config_viewer, side_list").toString().remove(" ").split(",");

	m_pBaseToolBar->clear();
	for (int i=0; i<slBaseTB.count(); i++) {
		m_pBaseToolBar->addAction(m_toolBarActionsMap.value(slBaseTB.at(i)));
	}
}


void FileViewer::updateSideList( const QStringList &slInFilesList, bool bClearBefore )
{
	if (m_pSideListOfPreview == nullptr || slInFilesList.isEmpty()) {
		qDebug() << "FileViewer::updateSideListView. Cannot update side list (the most likely input list is empty).";
		return;
	}
	if (bClearBefore)
		m_pSideListOfPreview->clear();

	FileInfo *pFI;
	QString sMime;
	QStringList lst;
	QTreeWidgetItem *pItem;
	for (int i = 0; i < slInFilesList.count(); ++i) {
		QFileInfo fi(slInFilesList[i]);
		lst.clear();
		lst << fi.fileName() << fi.path();
		if (findItemInSideList(slInFilesList[i]) == nullptr) { // item doesn't exist, lets add it
			m_pSideListOfPreview->addTopLevelItem((pItem = new QTreeWidgetItem(lst)));
			pFI = m_FileNameToFileInfo[QFileInfo(slInFilesList[i]).filePath()];
			sMime = pFI->mimeInfo();
			if (pFI->isSymLink())
				sMime = MimeType::getFileMime(pFI->symLinkTarget());
			m_FileNameToMimeMap.insert(slInFilesList[i], sMime);
		}
		else
			qDebug() << "FileViewer::updateSideListView. File already exists:" << slInFilesList[i];
	}
	if (slInFilesList.count() == 1) // select item (deselecting other)
		m_pSideListOfPreview->setCurrentItem(pItem, 0, QItemSelectionModel::ClearAndSelect);

	if (m_pSideListOfPreview->topLevelItemCount () < 2)
		return;

	if (sm_SW[0] > 0)
		return;

	m_bShowTheFilesList = true;
	QString sMsg = m_bShowTheFilesList ? tr("Hide side &list") : tr("Show side &list");
	m_pSideListA->setText(sMsg);
	m_pSideListA->setToolTip(sMsg+" ("+m_pKS->keyStr(ShowHideSideList_VIEWER)+")");
	sm_SW[0] = (m_bShowTheFilesList) ? m_nSideListWidth : 0;
	m_pSplitter->setSizes(sm_SW);
	m_pSideListOfPreview->header()->resizeSection(0, m_nSideListWidth);
}

void FileViewer::loadingWithDelay( const QString &sFileName )
{
	if (! sFileName.isEmpty()) {
		m_sCurrentFileName = sFileName;
		m_bFileFromLocalFS = m_sCurrentFileName.startsWith('/');
	}
	m_bFileExtracted = false; // includes FileDownloaded status
	qDebug() << "\"FileViewer::loadingWithDelay.\" m_sCurrentFileName:" << m_sCurrentFileName;
	slotReadyRead(); // asynchronously loading file with name 'm_sCurrentFileName'
}

FileViewer::TypeOfFile FileViewer::typeOfFile( const QString &sMime )
{
	if (sMime.startsWith("text/"))
			return TEXT_file;
	else
	if (sMime.startsWith("image/"))
		return IMAGE_file;
	else
	if (sMime.startsWith("audio/"))
		return SOUND_file;
	else
	if (sMime.startsWith("video/"))
		return VIDEO_file;

	return UNKNOWN_file;
}

QTreeWidgetItem *FileViewer::findItemInSideList( const QString &sFullFileName, bool bSelect )
{
	QTreeWidgetItem *pItem = nullptr;
	bool bFound = false;
	QFileInfo fi(sFullFileName);
	// -- look for file name in first column
	QList<QTreeWidgetItem *> twItemsList = m_pSideListOfPreview->findItems(fi.fileName(), Qt::MatchExactly, 0);
	// -- find path in second column
	if (twItemsList.count() > 0) {
		foreach(pItem, twItemsList) {
			if (pItem->text(1) == fi.path()) {
				bFound = true;
				if (bSelect)
					m_pSideListOfPreview->setCurrentItem(pItem, 0, QItemSelectionModel::SelectCurrent);
				break;
			}
		}
	}
	return bFound ? pItem : nullptr;
}

// -------------- SLOTs ---------------

void FileViewer::slotShowHideMenuBar()
{
	if (! m_bPreviewInWindow) { // embedded viewer must be without menu bar
		m_pMenuBar->setVisible(false);
		return;
	}
	QString sMsg = m_bShowMenuBar ? tr("Hide &menu bar") : tr("Show &menu bar");
	m_pMenuBarA->setText(sMsg);
	m_pMenuBar->setVisible(m_bShowMenuBar);
	m_bShowMenuBar = ! m_bShowMenuBar;
}

void FileViewer::slotShowHideToolBar()
{
	QString sMsg = m_bShowToolBar ? tr("Hide &tool bar") : tr("Show &tool bar");
	m_pToolBarA->setText(sMsg);
	m_pBaseToolBar->setVisible(m_bShowToolBar);
	//m_pSpecificViewToolBar->setVisible(m_bShowToolBar);
	m_bShowToolBar = ! m_bShowToolBar;
}

void FileViewer::slotOpenCurrentFile()
{
	QString sPath = m_pSideListOfPreview->currentItem()->text(1);
	if (! sPath.endsWith(QDir::separator()))
		sPath += QDir::separator();

	m_sCurrentFileName = m_pSideListOfPreview->currentItem()->text(0);
	m_sCurrentFileName.insert(0, sPath);
	qDebug() << "\"FileViewer::slotOpenCurrentFile.\" m_sCurrentFileName:" << m_sCurrentFileName;
	loadingWithDelay(); // will be used: m_sCurrentFileName
}

void FileViewer::slotReadyRead()
{
	qDebug() << "\"FileViewer::slotReadyRead.\" FileFromLocalFS:" << m_bFileFromLocalFS << "PreviewInWindow:" << m_bPreviewInWindow;
	if (sm_bCanShowViewer) { // needs for FTP, because file might be read piece by piece
		sm_bCanShowViewer = false;
		if (m_bPreviewInWindow)
			show();
		if (! m_bFileFromLocalFS)
			if (! preparationToView()) // initializes variables using to read and creates viewer obj.
				return;
	}
	if (m_bFileFromLocalFS) {
		if (preparationToView()) // initializes variables using to read and invokes read
			QTimer::singleShot(0, this, &FileViewer::slotReadFile);
	}
	else
		slotReadFile();
}

bool FileViewer::preparationToView()
{
	QString sFN = "FileViewer::preparationToView.";
	if (m_sCurrentFileName.startsWith("./")) // workaround for files placed on 0 level (root path) coming from archive subsystem
		m_sCurrentFileName.remove(0,2);
	qDebug() << sFN << "Init variables. CurrentFileName:" << m_sCurrentFileName;
	if (m_bPreviewInWindow)
		setWindowTitle(m_sCurrentFileName + " - QtCommander 2");

	// -- check if file is readable
	m_nBytesRead = 0;
	m_nFileReadingMode = Viewer::GET_READABILITY;
	emit signalReadFile(m_sHost, m_FileNameToFileInfo[m_sCurrentFileName], m_baBinBuffer, m_nBytesRead, m_nFileReadingMode, objectName()); // invokes SubsystemManager::slotReadFileToViewer
	if (m_nBytesRead < 0) {
		qDebug() << sFN << "Cannot read file:" << m_sCurrentFileName;
		return false;
	}
	QApplication::setOverrideCursor(Qt::WaitCursor);
	m_pStatusBar->slotSetMessage(tr("Loading file in progress")+"...");
	// -- create viewer
	QString sMime = m_FileNameToMimeMap[m_sCurrentFileName];
	// -- checking if this is compressed file (not tar archive)
	bool bPluginSupport = PluginFactory::instance()->isPluginAvailable(sMime, "subsystem");
	bool bArchive = (sMime.contains("-tar") || sMime.contains(".rar") || sMime.contains("/zip")); // here mime is correctly detected, we one might to use it to exclude archives
	if (bPluginSupport && ! bArchive) { // this is compressed file
		QString sFileName = QFileInfo(m_sCurrentFileName.left(m_sCurrentFileName.lastIndexOf('.'))).fileName(); // cut last file extention
		sMime = MimeType::getFileMime(sFileName);
	}

	// -- detect text file
	m_bIsItTextFile = (sMime.startsWith("text/") || sMime == "application/x-zerosize");
	if (! m_bIsItTextFile) {
		// - get list of mimes for textviewer_plugin
		QStringList slMimes = PluginManager::instance()->getAllRegistreredPluginsInfo(PluginData::MIME_TYPE, "viewer");
		QStringList slNames = PluginManager::instance()->getAllRegistreredPluginsInfo(PluginData::NAME, "viewer");
		int nTxtViewerId = slNames.indexOf("textviewer_plugin");
		if (nTxtViewerId > -1 && slMimes.size() > 0) {
			QString sNewMimeLst = slMimes.at(nTxtViewerId);
			foreach (QString sMimeLst, sNewMimeLst.split(':')) {
				if (sMimeLst == sMime) {
					m_bIsItTextFile = true;
					break;
				}
			}
		}
	}
	qDebug() << sFN << "Text file:" << m_bIsItTextFile << " - try to create properly viewer";

	if (! createView(sMime)) { // try to load properly plugin (sets m_pViewer, uses m_bIsItTextFile)
		close();
		return false;
	}
	if (m_bIsItTextFile) {
		if (m_bInitEditMode) {
			m_bInitEditMode = false;
			m_eViewerMode = Viewer::EDIT_mode;
		}
		else
			m_eViewerMode = (m_eTypeOfFile == RENDER_file) ? Viewer::RENDER_mode : Viewer::RAW_mode;
	}
	else { // default binary file will be opened in RENDER_mode
		// TODO and need to check if incomming mime is registred for this textviewer plugin (or some other plugin)
		// flag m_bItIsTextFile must be set here for method: FileViewer::createPreview wher is choose type of viewer
		// using this flag ToolBarMenu and MenuBar are updated
		// NOTE. In this moment one not able to determine in which plugin mime is registred
		m_eViewerMode = ((m_eTypeOfFile == IMAGE_file || m_eTypeOfFile == SOUND_file || m_eTypeOfFile == VIDEO_file || m_eTypeOfFile == OFFICE_file)
			? Viewer::RENDER_mode : Viewer::HEX_mode);
	}

	updateMenuBar(); // uses m_bItIsTextFile

	// -- select reading mode and buffer size
	m_nFileReadingMode = Viewer::APPEND;
	if (typeOfFile(sMime) == IMAGE_file)
		m_nFileReadingMode = Viewer::ALL;
	m_nBytesRead = (m_nFileReadingMode == Viewer::ALL) ? -1 : 102400; // 10240  102400

	// -- update recent files list with files name
	if (m_pRecentFilesMenu != nullptr) {
		bool bInsertName = true;
		for (int i=0; i<m_slRecentFilesMenuList.count(); i++) {
			if (m_slRecentFilesMenuList[i] == m_sCurrentFileName) {
				bInsertName = false;
				break;
			}
		}
		if (bInsertName) {
			m_pRecentFilesMenu->addAction(m_sCurrentFileName);
			m_slRecentFilesMenuList << m_sCurrentFileName;
		}
	}

	return true;
}

void FileViewer::readExtractedFile()
{
	qDebug() << "FileViewer::readExtractedFile.";
	m_bFileExtracted = true;
	Settings settings;
	m_sTempDir = settings.value("vfs/TempDir").toString();
	FileInfo *pFI = m_FileNameToFileInfo[m_sCurrentFileName];
	delete m_pExtractedFI;
	QString sFileName = pFI->fileName();
	QString sMime = MimeType::getFileMime(m_sCurrentFileName);
	bool bPluginSupport = PluginFactory::instance()->isPluginAvailable(sMime, "subsystem");
	if (bPluginSupport) { // this is compressed file
		sFileName = QFileInfo(m_sCurrentFileName).fileName();
		sFileName = sFileName.left(sFileName.lastIndexOf('.')); // remove last extention
		sMime = MimeType::getFileMime(m_sTempDir+sFileName);
	}
	m_pExtractedFI = new FileInfo (sFileName, m_sTempDir, pFI->size(), false, false, pFI->lastModified(), pFI->permissions(), pFI->owner(), pFI->group(), sMime);
	slotReadFile();
}

void FileViewer::setKeyshortcutToActionToolTip( QAction *pAction, int nActionId )
{
	pAction->setToolTip(pAction->toolTip()+" ("+m_pKS->keyStr(nActionId)+")");
}


void FileViewer::slotApplyToolbarsSettings( const ToolBarPageSettings &toolBarPageSettings )
{
	QList<QAction *> listOfActions;
	QString     sName              = toolBarPageSettings.tbName;
	QStringList sToolbarActionsLst = toolBarPageSettings.tbActions;
	int nActionsNum = sToolbarActionsLst.count();
	//qDebug() << "FileViewer::slotApplyToolbarsSettings. ()" << sName << nActionsNum << sToolbarActionsLst;

	if (sName == tr("Base")) {
		if (nActionsNum == 0) {
			m_pBaseToolBar->hide();
			return;
		}
		if (! m_pBaseToolBar->isVisible())
			m_pBaseToolBar->show();
		m_pBaseToolBar->clear();
		listOfActions = m_pBaseToolBarAG->actions();
		//m_pBaseToolBar->addWidget(m_pSpecificViewToolBar); // needs to add, becaua base toolBAr was cleared before
	}
	else
	if (sName == tr("View")) {
		if (nActionsNum == 0) {
			//qDebug() << "FileViewer::slotApplyToolbarsSettings. View. m_pSpecificViewToolBar hide";
			//m_pSpecificViewToolBar->hide();
			return;
		}
		/*if (! m_pSpecificViewToolBar->isVisible()) {
			//m_pBaseToolBar->addWidget(m_pSpecificViewToolBar); // needs to add, becaua base toolBAr was cleared before
			m_pSpecificViewToolBar->show();
		}*/
		//qDebug() << "FileViewer::slotApplyToolbarsSettings. View. m_pSpecificViewToolBar->isVisible" << m_pSpecificViewToolBar->isVisible();// << m_pSpecificViewToolBar->actions().count();
		//m_pSpecificViewToolBar->clear();
		listOfActions = m_pViewer->viewToolBarActions()->actions();
		//qDebug() << "FileViewer::slotApplyToolbarsSettings. View. listOfActions.count" << listOfActions.count();
	}
	else
	if (sName == tr("Edit")) {
		if (nActionsNum == 0) {
			//m_pSpecificViewToolBar->hide();
			//qDebug() << "FileViewer::slotApplyToolbarsSettings. Edit. m_pSpecificViewToolBar hide";
			return;
		}
		//if (! m_pSpecificViewToolBar->isVisible())
		//	m_pSpecificViewToolBar->show();
		//qDebug() << "FileViewer::slotApplyToolbarsSettings. Edit. m_pSpecificViewToolBar->isVisible" << m_pSpecificViewToolBar->isVisible();
		//m_pSpecificViewToolBar->clear();
		listOfActions = m_pViewer->editToolBarActions()->actions();
		//qDebug() << "FileViewer::slotApplyToolbarsSettings. Edit. listOfActions.count" << listOfActions.count();
	}

	QAction *pAction = nullptr;
	QString sActName;
	for (int i=0; i< listOfActions.count(); i++) {
		pAction = listOfActions.at(i);
		sActName = pAction->objectName();
		// NOTE: (???) Cannot use setVisible. becaua action is shared with MenuBar and ToolBar
		if (sName == tr("Base")) {
			if (sToolbarActionsLst.indexOf(sActName) >= 0)
				m_pBaseToolBar->addAction(pAction);
		}
		else
		if (sName == tr("View")) {
			if (sToolbarActionsLst.indexOf(sActName) >= 0)
				m_pBaseToolBar->addAction(pAction);
		}
		else
		if (sName == tr("Edit")) {
			if (sToolbarActionsLst.indexOf(sActName) >= 0)
				m_pBaseToolBar->addAction(pAction);
		}
	}
}

void FileViewer::slotReadFile()
{
	QString sFN = "FileViewer::slotReadFile.";
	QString sFileName = m_sCurrentFileName;
	QString sMime = MimeType::getFileMime(m_sCurrentFileName); // previous: m_FileNameToMimeMap[m_sCurrentFileName];
	bool bPluginSupport = PluginFactory::instance()->isPluginAvailable(sMime, "subsystem");
	if (bPluginSupport) { // this is compressed file
		Settings settings;
		m_sTempDir = settings.value("vfs/TempDir").toString();
		QString sFileName1 = QFileInfo(m_sCurrentFileName).fileName();
		sFileName1 = m_sTempDir + sFileName1.left(sFileName1.lastIndexOf('.')); // remove last extention
		if (QFileInfo(sFileName1).exists()) { // if compressed file exists in temporary file
			sFileName = sFileName1;
			m_sHost = m_sTempDir; // QFileInfo(m_sCurrentFileName).filePath();
		}
	}
	qDebug() << sFN << "Reading file:" << sFileName << "host:" << m_sHost << "mime:" << sMime << "Viewer:" << m_pViewer;
	if (m_pViewer == nullptr)
		return;

	if (m_pViewer->requiredIntLoadingOfFile()) {
		// invokes readFileToViewer in properly subsystem (for localsubsystem will be actual reading, for archives will be triggered extract, for remote will be triggered download)
		if (! m_bFileExtracted) {
			emit signalReadFile(m_sHost, m_FileNameToFileInfo[m_sCurrentFileName], m_baBinBuffer, m_nBytesRead, m_nFileReadingMode, objectName());
			if (m_nBytesRead < 0) { // user abandoned view, so let close window
				QApplication::restoreOverrideCursor();
// 				QTimer::singleShot(0, this, SIGNAL(signalClose()));
				QTimer::singleShot(0, this, &FileViewer::signalClose);
				return;
			}
		}
		else { // extracted file. Note. m_pExtractedFI is prepared in readExtractedFile()
// 			QString sPath = m_pSettings->value("vfs/TempDir").toString();
// 			FileInfo *pFI = m_FileNameToFileInfo[m_sCurrentFileName];
// 			FileInfo fi(pFI->fileName(), sPath, pFI->size(), false, false, pFI->lastModified(), pFI->permissions(), pFI->owner(), pFI->group(), pFI->mimeInfo());
			emit signalReadFile(m_sTempDir, m_pExtractedFI, m_baBinBuffer, m_nBytesRead, m_nFileReadingMode, objectName());
		}
		if (! m_bFileFromLocalFS && ! m_bFileExtracted)
			return;
		if (m_nBytesRead <= 0) { // m_nBytesRead < 0 when an error occured
			if (m_nBytesRead == -1 || m_nBytesRead == 0) { // empty file
				m_pViewer->updateContents(Viewer::ALL, m_eViewerMode, sMime, m_baBinBuffer);
				m_nBytesRead = 0; // required for update status in Viewer
			}
			qDebug() << sFN << "One didn't read bytes from file:" << m_sCurrentFileName << "BytesRead:" << m_nBytesRead << "The most likely whole file has been read.";
			m_baBinBuffer.resize(0); // release buffer
			QApplication::restoreOverrideCursor();
			if (m_pViewer != nullptr) {
				if (m_nBytesRead == 0) // file has been loaded
					m_pViewer->updateStatus();
				if ((m_eViewerMode == Viewer::HEX_mode || (! m_bIsItTextFile && m_eViewerMode == Viewer::RAW_mode)) && m_baBinBuffer.size() > 0) // need to move contents to top of view
					m_pViewer->updateContents(Viewer::NONE, m_eViewerMode, sMime, m_baBinBuffer);
				//updateMenuBar();
				raise();
			}
			else // appeared an error, need to close window
				close();
			return;
		}
		qDebug() << sFN << "BytesRead:" << m_nBytesRead << "buffer size:" << m_baBinBuffer.size();
	}
	else {
		if (m_bFileFromLocalFS) {
// 			sMime = sMime+"|"+m_sCurrentFileName; // ugly workaround
			sMime += "|"+sFileName; // ugly workaround
		}
		else {
			qDebug() << sFN << "Try to extract file before view.";
			if (! m_bFileExtracted) // need to extract file before loading
				emit signalReadFile(m_sHost, m_FileNameToFileInfo[m_sCurrentFileName], m_baBinBuffer, m_nBytesRead, m_nFileReadingMode, objectName());
			else // file extracted
				sMime = sMime+"|"+m_sTempDir + QFileInfo(m_sCurrentFileName).fileName(); // ugly workaround
		}
		qDebug() << sFN << "File loading handled by plugin. Use:" << m_pViewer;
		if (! m_bFileFromLocalFS && ! m_bFileExtracted)
			return;
	}

	if (m_pViewer != nullptr)
		m_pViewer->updateContents((Viewer::ReadingMode)m_nFileReadingMode, m_eViewerMode, sMime, m_baBinBuffer);
	else {
		qDebug() << sFN << "Plugin not loaded yet.";
		return;
	}
	if (m_nFileReadingMode == Viewer::ALL || m_nFileReadingMode == Viewer::ON_DEMAND) {
		QApplication::restoreOverrideCursor();
		m_baBinBuffer.resize(0); // release buffer
		return;
	}
	// only for HTML files (sets all content of file - need to fix bug in the HTML rendering)
	if (m_nFileReadingMode == Viewer::APPEND && m_nBytesRead < 100240 && m_bIsItTextFile && m_eViewerMode == Viewer::RENDER_mode)
		m_pViewer->updateContents(Viewer::ALL, m_eViewerMode, sMime, m_baBinBuffer);

	if (m_pViewer->requiredIntLoadingOfFile())
		QTimer::singleShot(0, this, &FileViewer::slotReadFile);
	else
		QApplication::restoreOverrideCursor();

	if (m_pViewer->requiredIntLoadingOfFile())
		m_pStatusBar->slotSetMessage(tr("Loading file in progress")+"...");
}


void FileViewer::slotToggleViewingMode( QAction *pAction )
{
	Q_CHECK_PTR(pAction);
	Viewer::ViewerMode eCurrentViewerMode = m_eViewerMode;
	m_eViewerMode = m_actionsModes[pAction];
	QString sMime = m_FileNameToMimeMap[m_sCurrentFileName];
	qDebug() << "FileView::slotToggleViewingMode. Change mode from: " << Viewer::toString(eCurrentViewerMode) << "to:" << Viewer::toString(m_eViewerMode) << "for:" << m_FileNameToMimeMap.key(sMime);;

	if (m_bIsItTextFile) {
		if (m_eViewerMode == (int)Viewer::HEX_mode) {
			if (m_eViewerType == Viewer::TEXT_viewer)
				m_pViewer->getData(m_baBinBuffer); // gets changed data
			createView(sMime, Viewer::BINARY_viewer);
		}
		else
			createView(sMime);

// 		if (m_eViewerMode == (int)Viewer::RENDER_mode)
// 			m_pViewer->clear(); // need to HTML render turn on

		m_pViewer->updateContents(Viewer::ALL, m_eViewerMode, sMime, m_baBinBuffer);
		m_pSaveA->setVisible((m_eViewerMode == Viewer::EDIT_mode && m_eViewerType == Viewer::TEXT_viewer));
		m_pSaveA->setEnabled(m_pEditModeA->isChecked()); // enable/disable save
	}
	else { // it's binary file
		;/*
		//m_pView->getData( m_baBinBuffer); // b.wolno dziala (dla HEXmode), lepiej wczytac jeszcze raz
		Viewer::ViewerType eViewerType = (m_eViewerMode == Viewer::RAW_mode || m_eViewerMode == Viewer::HEX_mode) ? Viewer::BINARY_viewer : Viewer::UNKNOWN_viewer;
		createPreview(m_sCurrentFileName, eViewerType);
		bool bIsLocalFS = (FSManager::kindOfFilesSystem(m_sCurrentFileName) == VFS::LOCALfs);
		m_nFilenReadingMode = Viewer::APPEND;
		if (m_eTypeOfFile == IMAGE_file && m_eViewerType != Viewer::BINARY_viewer && bIsLocalFS)
			m_nFilenReadingMode = Viewer::ALL;
		m_nBytesRead = (m_nFilenReadingMode == Viewer::ALL) ? -1 : 10240;
		m_pViewer->clear();
		if (bIsLocalFS)
			QTimer::singleShot(0, this, &FileViewer::slotReadFile);
		*/
	}

	m_pViewer->updateEditMenuBarActions((m_eViewerMode == Viewer::EDIT_mode));
}


void FileViewer::slotItemOfRecentFilesMenuActivated( QAction *pAction )
{
	Q_CHECK_PTR(pAction);
	QString sFileName = pAction->text();
	if (sFileName != m_sCurrentFileName) {
		m_pViewer->clear();
		loadingWithDelay(sFileName); // invokes asynchronously file loading
	}
}


void FileViewer::slotNew()
{
	QString sFullFileName = QFileDialog::getOpenFileName(this, tr("Open file in new window")+" - QtCommander 2", m_sPanelPath);
	if (sFullFileName.isEmpty())
		return;

	qDebug() << "FileViewer::slotNew.";
	QString sObjectName = objectName();
	FileViewer *pFV = new FileViewer(sFullFileName, true, false, sObjectName, m_pPanelParent, m_pKS, m_pSubsMgrParent);
	connect(pFV, &FileViewer::signalReadFile, (SubsystemManager *)m_pSubsMgrParent, &SubsystemManager::slotReadFileToViewer);
	connect(pFV, &FileViewer::signalUpdateItemOnLV, (SubsystemManager *)m_pSubsMgrParent, &SubsystemManager::slotUpdateItemOnLV);
}


void FileViewer::slotOpen()
{
	QString sFullFileName = QFileDialog::getOpenFileName(this, tr("Open file")+" ... - QtCommander 2", m_sPanelPath);

	if (! sFullFileName.isEmpty()) {
		if (m_pViewer != nullptr) { // remove old vieer
			delete m_pViewer;  m_pViewer = nullptr;
			m_eViewerType = Viewer::UNKNOWN_viewer;
		}
		QStringList slFilesList(sFullFileName);
		updateSideList(slFilesList); // updates also m_FileNameToMimeMap
		loadingWithDelay(sFullFileName); // invokes asynchronously file loading
	}
	else
		m_pStatusBar->slotSetMessage(tr("Loading aborted"), 2000, false);
}

void FileViewer::slotSave()
{
	if (! m_pViewer->isModified() || ! m_bIsItTextFile)
		return;

	if (m_pViewer->saveToFile(m_sCurrentFileName)) {
		m_pStatusBar->slotSetMessage(tr("File has been saved."), 2000, false);
		emit signalUpdateItemOnLV(m_sCurrentFileName, true);
	}
}

void FileViewer::slotSaveAs()
{
	QString sFileName  = QFileDialog::getSaveFileName(this, tr("Save as")+" ... - QtCommander 2", m_sPanelPath);

	if (! sFileName.isEmpty()) {
		if (m_bIsItTextFile || MimeType::getFileMime(m_sCurrentFileName).startsWith("image/")) {
			m_pViewer->saveToFile(sFileName);
			emit signalUpdateItemOnLV(sFileName, false);
		}
	}
	else
		m_pStatusBar->slotSetMessage(tr("Saving aborted"), 2000, false);
}


void FileViewer::slotReload()
{
	if (m_pViewer) {
		m_pViewer->clear();
		m_eViewerType = Viewer::UNKNOWN_viewer;
	}
	loadingWithDelay(m_sCurrentFileName); // asynchronously file loading
}


void FileViewer::slotSaveSettings()
{
	if (! m_bPreviewInWindow)
		return;

	m_pSaveSettingsA->setChecked(false);

	QString sSize = QString::number(width())+","+QString::number(height());
	Settings settings;
	settings.setValue("viewer/Size", sSize);
	settings.setValue("viewer/ShowMenuBar", ! m_bShowMenuBar);
	settings.setValue("viewer/ShowToolBar", ! m_bShowToolBar);

	m_pViewer->slotSaveSettings();
}


void FileViewer::slotShowSettings()
{
// 	qDebug() << "FileViewer::slotShowSettings. Try to get an object loaded with plugin."; // plugin is loaded when app.is starting
	m_pPrevieConfigDialog = PluginManager::instance()->config("config/preview");
	if (m_pPrevieConfigDialog == nullptr) {
		MessageBox::msg(0, MessageBox::CRITICAL, tr("Configuration window of preview is not available.")+"\n\n"+tr("Cannot find properly plugin:"));
		return;
	}
	m_pPrevieConfigDialog->init(this, m_pKS, m_pToolbarActionsLstForConfig);
	m_pPrevieConfigDialog->raiseWindow("TextViewPage"); // show first page

	connect(m_pPrevieConfigDialog, static_cast<void(ConfigurationDialog::*)(const TextViewSettings &,
			const QString &)>(&ConfigurationDialog::signalApply), m_pViewer, &Viewer::slotApplyTextViewSettings);
	//connect(m_pHighlightingComboBox,  static_cast<void(QComboBox::*)(int)>(&QComboBox::activated), this, &SyntaxHighlightingPage::slotSelectHighlighting);

// 	connect(m_pQtCmdPrevieDialog, static_cast<void(ConfigurationDialog::*)(const ImageViewSettings &,
// 			const QString &)>(&ConfigurationDialog::signalApply), m_pViewer, &Viewer::slotApplyImageViewSettings);
// 	connect(m_pQtCmdPrevieDialog, static_cast<void(ConfigurationDialog::*)(const SoundViewSettings &,
// 			const QString &)>(&ConfigurationDialog::signalApply), m_pViewer, &Viewer::slotApplySoundViewSettings);
// 	connect(m_pQtCmdPrevieDialog, static_cast<void(ConfigurationDialog::*)(const VideoViewSettings &,
// 			const QString &)>(&ConfigurationDialog::signalApply), m_pViewer, &Viewer::slotApplyVideoViewSettings);
// 	connect(m_pQtCmdPrevieDialog, static_cast<void(ConfigurationDialog::*)(const BinaryViewSettings &,
// 			const QString &)>(&ConfigurationDialog::signalApply), m_pViewer, &Viewer::slotApplyBinaryViewSettings);

	connect(m_pPrevieConfigDialog, &ConfigurationDialog::signalApplyKeyShortcuts, this, &FileViewer::slotApplyKeyShortcuts);
	connect(m_pPrevieConfigDialog, static_cast<void(ConfigurationDialog::*)(const ToolBarPageSettings &)>(&ConfigurationDialog::signalApply), this, &FileViewer::slotApplyToolbarsSettings);

	// --- init settings dlg.
// 	connect(this, &FileViewer::signalSetCurrentViewerType, m_pQtCmdPrevieDialog, &pSettingsWidget::slotSetCurrentViewerType);
// 	connect(this, &FileViewer::signalSetKeyShortcuts, m_pQtCmdPrevieDialog, &pSettingsWidget::slotSetKeyShortcuts);
// 	emit signalSetCurrentViewerType(m_eViewerType);
// 	emit signalSetKeyShortcuts(m_pKeyShortcuts);
}



void FileViewer::slotLoadFilesList()
{
	Settings settings;
	QString sFilesList = settings.value("viewer/URLs").toString();
	if (sFilesList.isEmpty())
		return;

	QStringList slFilesList;
#if QT_VERSION >= QT_VERSION_CHECK(5, 15, 0)
	slFilesList = sFilesList.split(QLatin1Char('|'), Qt::SkipEmptyParts);
#else
	slFilesList = sFilesList.split('|', QString::SkipEmptyParts);
#endif
	if (m_pFileInfoList == nullptr)
		m_pFileInfoList = new FileInfoList;

	foreach(QString sFileName, slFilesList) {
		QFileInfo fi(sFileName);
		QString sCurrentFileName = fi.fileName();
		QString sPerm = "----------";
		//QString sPerm = permissionsAsString(sCreatedDirName);
		QString sMime = MimeType::getFileMime(sFileName);
		FileInfo *pFileInfo = new FileInfo(sCurrentFileName, fi.absolutePath(), fi.size(), false, false, fi.lastModified(), sPerm, fi.owner(), fi.group(), sMime);
		m_pFileInfoList->append(pFileInfo);
		qDebug() << "FileViewer::slotLoadFilesList. --- added" << pFileInfo->absoluteFileName() << "to m_pFileInfoList";
		if (m_FileNameToFileInfo[sFileName] == nullptr)
			m_FileNameToFileInfo.insert(sFileName, pFileInfo);
	}

	updateSideList(slFilesList); // updates also m_FileNameToMimeMap

	m_pStatusBar->slotSetMessage(tr("The files list has been loaded"), 2000, true);
}

void FileViewer::slotSaveFilesList()
{
	QString sFilesList;

	for (int i = 0; i < m_pSideListOfPreview->topLevelItemCount(); ++i) {
		QTreeWidgetItem *pItem = m_pSideListOfPreview->topLevelItem(i);
		Q_CHECK_PTR(pItem);
		sFilesList.append(QString(pItem->text(1) + "/" + pItem->text(0) + "|"));
	}

	Settings settings;
	settings.setValue("viewer/URLs", sFilesList);
	m_pStatusBar->slotSetMessage(tr("The files list has been saved"), 2000, true);
}

void FileViewer::slotShowTheFilesList()
{
	m_bShowTheFilesList = ! m_bShowTheFilesList;
	QString sMsg = m_bShowTheFilesList ? tr("Hide side &list") : tr("Show side &list");
	m_pSideListA->setText(sMsg);
	sm_SW[0] = (m_bShowTheFilesList) ? m_nSideListWidth : 0;
	m_pSplitter->setSizes(sm_SW);
	m_pSideListOfPreview->header()->resizeSection(0, m_nSideListWidth);
}


void FileViewer::slotOpenCurrentItem( QTreeWidgetItem *item, int )
{
// 	qDebug() << "FileViewer::slotOpenCurrentItem.";
	m_pViewer->clear();
	loadingWithDelay(item->text(1) +"/"+ item->text(0)); // asynchronously file loading
}

void FileViewer::slotDocumentChanged( bool bChanged )
{
	QString sChangedMarker;
	if (bChanged)
		sChangedMarker = "*";

	setWindowTitle(m_sCurrentFileName +sChangedMarker+ " - QtCommander 2");
}

void FileViewer::slotApplyKeyShortcuts( KeyShortcuts *pKeyShortcuts )
{
	if (pKeyShortcuts != nullptr)
		return;

	if (m_pViewer != nullptr)
		m_pViewer->initKeyShortcuts(pKeyShortcuts);
}


void FileViewer::keyPressEvent( QKeyEvent *pKE )
{
	m_bEscapePressed = false;
	if (pKE->key() == Qt::Key_Escape) {
		m_bEscapePressed = true;
		close(); // close and delete this
	}
}

void FileViewer::closeEvent( QCloseEvent *pCE )
{
	if (m_pViewer == nullptr || pCE == nullptr)
		return;

	if ((! m_pViewer->isModified() && ! m_pViewer->findReplaceBarVisible()) ||
		(! m_bEscapePressed && m_pViewer->findReplaceBarVisible())) {
		m_pViewer->clear(); // for example to stop playing music or video
		pCE->accept();
		//qDebug() << "FileViewer::closeEvent. delete this";  delete this; // force call destruktor (for unload a plugin)
		//qDebug() << "FileViewer::closeEvent. Call destruktor explicitly";  this->FileViewer::~FileViewer();
		destroyAllObjects();
// 		qDebug() << "FileViewer::closeEvent. return";
		return;
	}

	if (m_pViewer->findReplaceBarVisible()) {
		if (m_pViewer->errorMessageBarVisible())
			m_pViewer->setErrorMessageBarVisible(false);
		else
			m_pViewer->setFindReplaceBarVisible(false);

		pCE->ignore();
		return;
	}

	bool bChk = false;
	int nResult = MessageBox::yesNo(this,
		tr("Save file")+" - QtCommander 2", m_sCurrentFileName,
		tr("Do you want to save changed document")+"?",
		bChk, MessageBox::Yes // TRUE - show the Cancel button, too
	);
	switch (nResult)  {
		case MessageBox::Yes:
			slotSave();
			pCE->accept();
			delete this; // force call destruktor (for unload a plugin)
			break;

		case MessageBox::No:
			pCE->accept();
			delete this; // force call destruktor (for unload a plugin)
			break;

		case MessageBox::All: // 'Cancel' button pressed
		default: // just for sanity
			pCE->ignore();
			break;
	}
}

