/***************************************************************************
 *   Copyright (C) 2003 by Piotr Mierzwiñski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef VIEWERHELPER_H
#define VIEWERHELPER_H

#include <QColor>


static QColor color( const QString & hexColorStr, const QString & defaultHexColorStr="000000" );
// static QString colorToString( const QColor & color );

QColor color( const QString & hexColorStr, const QString & defaultHexColorStr ) // TODO drop this fun., because in Qt4 QColor has constructior with QString
{
	QString colorStr = hexColorStr;
	bool okRed, okGreen, okBlue;

	if ( hexColorStr.length() < 6 || hexColorStr.length() > 6 )
		colorStr = defaultHexColorStr;

	int red   = (colorStr.left( 2 )).toInt( &okRed, 16 );
	int green = (colorStr.mid( 2,2 )).toInt( &okGreen, 16 );
	int blue  = (colorStr.right( 2 )).toInt( &okBlue, 16 );

	if ( ! okRed || ! okGreen || ! okBlue ) {
		colorStr = defaultHexColorStr;
		red   = colorStr.left( 2 ).toInt( &okRed, 16 );
		green = colorStr.mid( 2,2 ).toInt( &okGreen, 16 );
		blue  = colorStr.right( 2 ).toInt( &okBlue, 16 );
	}

	return QColor( red, green, blue );
}

/*
QString colorToString( const QColor & color ) // TODO drop this fun., because in Qt4 there is met. name() for QColor
{
	QString redStr, greenStr, blueStr;

	redStr.sprintf( "%.2X", color.red() );
	greenStr.sprintf( "%.2X", color.green() );
	blueStr.sprintf( "%.2X", color.blue() );

	QString colorStr = redStr+greenStr+blueStr;

	return colorStr;
}
*/

#endif
