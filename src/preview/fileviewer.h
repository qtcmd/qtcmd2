/***************************************************************************
 *   Copyright (C) 2002 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along withthisprogram; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef FILEVIEWER_H
#define FILEVIEWER_H

#include "../panel.h" // path must be relatively due to parser cannot see it
#include "viewer.h"
#include "mimetype.h"

class QTimer;
class QAction;
class QMenuBar;
class QToolBar;
class QSplitter;
class QTreeWidget;
class QTreeWidgetItem;
class QToolButton;
class QStringList;
class StatusBar;
class QCloseEvent;

using namespace Preview;

class FileViewer : public Panel
{
	Q_OBJECT
public:
	FileViewer( Vfs::FileInfoToRowMap *pSelectionMap, const QString &sHost, int currentId, bool bInWindow, bool bEditMode, const QString &sName, QWidget *pPParent, KeyShortcuts *pKeyShortcuts, QObject *pSMParent );
	FileViewer( const QString &sFullFileName, bool bInWindow, bool bEditMode, const QString &sName, QWidget *pPParent, KeyShortcuts *pKeyShortcuts, QObject *pSMParent );
	~FileViewer();

	void readExtractedFile();

	StatusBar *statusBar() const { return m_pStatusBar; }

	void setKeyshortcutToActionToolTip( QAction *pAction, int nActionId );

private:
	Viewer    *m_pViewer;

	QSplitter *m_pSplitter;
	QWidget   *m_pViewerParent;
	StatusBar *m_pStatusBar;
	QTreeWidget *m_pSideListOfPreview;

	QWidget   *m_pSettingsWidget;
	QWidget   *m_pPanelParent;
	QObject   *m_pSubsMgrParent;

	IconCache *m_pIconCache;


	Viewer::ViewerType m_eViewerType;
	Viewer::ViewerMode m_eViewerMode;

	QString      m_sCurrentFileName, m_sPanelPath, m_sHost;

	QMenuBar    *m_pMenuBar;
	QMenu       *m_pFileMenu, *m_pViewerMenu, *m_pEditMenu, *m_pOptionsMenu, *m_pRecentFilesMenu;

	QToolBar    *m_pBaseToolBar;
	QActionGroup *m_pBaseToolBarAG;
	QToolButton *m_pOpenBtn;
	/** list of actions used in configuration dialog. */
	QList <QActionGroup *> *m_pToolbarActionsLstForConfig;

	QByteArray m_baBinBuffer;

	QTimer       *m_pTimer;
	KeyShortcuts *m_pKS;

	bool m_bPreviewInWindow, m_bInitEditMode;
	bool m_bShowMenuBar, m_bShowToolBar, m_bShowTheFilesList;
	uint m_nInitWidth, m_nInitHeigth, m_nSideListWidth;
	bool m_bIsItTextFile;

	QStringList m_slPluginsNameList;
	QStringList m_slRecentFilesMenuList;

	int  m_nBytesRead;
	int  m_nFileReadingMode;
	bool m_bFileFromLocalFS;

	QMap <QAction*, Viewer::ViewerMode> m_actionsModes;
	QMap <QString, QString > m_FileNameToMimeMap;
	QMap <QString, FileInfo *> m_FileNameToFileInfo;
	FileInfoList *m_pFileInfoList;
	bool m_bSkipUpdateFileInfoList;

	QString m_sTempDir;
	bool m_bFileExtracted;
	FileInfo *m_pExtractedFI;

	bool m_bEscapePressed;

	QAction /*m_pHelpA,*/
			*m_pOpenA,
			*m_pSaveA,
			*m_pSaveAsA,
			*m_pReloadA,
			*m_pNewA,
			*m_pMenuBarA,
			*m_pToolBarA,
			*m_pSideListA,
			*m_pRawModeA,
			*m_pEditModeA,
			*m_pRenderModeA,
			*m_pHexModeA,
			*m_pConfigurationA,
			*m_pSaveSettingsA,
			*m_pQuitA;

	enum TypeOfFile {
		 TEXT_file,   // might to apply for: plain text or source code, scripts
		 IMAGE_file,
		 SOUND_file,
		 VIDEO_file,
		 RENDER_file, // might to apply for: html, pdf, rtf, man
		 BINARY_file,
		 OFFICE_file, // might to apply for binary document like following: Microsoft Office or Libre Office or Kalligra file
		 UNKNOWN_file
	};

	enum ActionsOfViewer {
		Help_VIEWER=1001,
		Open_VIEWER,
		Save_VIEWER,
		SaveAs_VIEWER,
		Reload_VIEWER,
		OpenInNewWindow_VIEWER,
		ShowHideMenuBar_VIEWER,
		ShowHideToolBar_VIEWER,
		ShowHideSideList_VIEWER,
		ToggleToRawMode_VIEWER,
		ToggleToEditMode_VIEWER,
		ToggleToRenderMode_VIEWER,
		ToggleToHexMode_VIEWER,
		ShowSettings_VIEWER,
		Quit_VIEWER
	};

	TypeOfFile typeOfFile( const QString &sMime );
	TypeOfFile m_eTypeOfFile;

	ConfigurationDialog *m_pPrevieConfigDialog;

	//typedef QList <QActionGroup *> ActionGroupLst;
	QMap <QString, QAction *> m_toolBarActionsMap;

	void init( FileInfoToRowMap *pSelectionMap, FileInfoList *pFileInfoList=NULL );
	bool preparationToView();

	void updateMenuBar();

	/** Initializes all configurable key key shortcuts (not all, because some are fixed).
	 * Loads configuration of shortcuts from config file.
	 */
	void initKeyShortcuts();

	void initMenuBarAndToolBar();
	void updateSideList( const QStringList &slInFilesList, bool bClearBefore=false );
	QTreeWidgetItem *findItemInSideList( const QString &sFullFileName, bool bSelect=false );
	QIcon icon( const QString &sIconName ) const {
		return m_pIconCache->icon(sIconName);
	}

	void destroyAllObjects();

	/** Updates toolbars checking config file.
	 */
	void updateToolBars();

protected:
	bool createView( const QString &sInMime, Viewer::ViewerType eInViewerType=Viewer::UNKNOWN_viewer );
	void loadingWithDelay( const QString &sFileName="" );

	void keyPressEvent( QKeyEvent *pKE );
	void closeEvent( QCloseEvent *pCE );


private slots:
	void slotReadyRead();
	void slotReadFile();
	void slotOpenCurrentFile();
	void slotToggleViewingMode( QAction *pAction );
	void slotItemOfRecentFilesMenuActivated( QAction *pAction );

	void slotNew();
	void slotOpen();
	void slotSave();
	void slotSaveAs();
	void slotReload();

	void slotShowHideMenuBar();
	void slotShowHideToolBar();
	void slotSaveSettings();
	void slotShowSettings();

	void slotLoadFilesList();
	void slotSaveFilesList();

	void slotDocumentChanged( bool bChanged );

	void slotShowTheFilesList();
	void slotOpenCurrentItem( QTreeWidgetItem *item, int column );

	/** Apply (if not nullptr) KeyShortcuts for Preview::Viewer object.
	 * @param pKeyShortcuts pointer to KeyShortcuts object
	 */
	void slotApplyKeyShortcuts( KeyShortcuts *pKeyShortcuts );

	/** Applies settings for tool bar getting data from given object.
	 * @param toolBarPageSettings reference to container containing tool bar settings
	 */
	void slotApplyToolbarsSettings( const ToolBarPageSettings &toolBarPageSettings );

signals:
	void signalReadFile( const QString &sHost, FileInfo *pFileInfo, QByteArray &baBuffer, int &nBytesRead, int nFileLoadMethod, const QString &sName );
/*	void signalSetCurrentViewerType( Viewer::ViewerType eCurrentViewerType );
	void signalSetKeyShortcuts( KeyShortcuts *pKeyShortcuts );*/
	void signalUpdateItemOnLV( const QString &sFileName, bool bExists );
 	void signalClose();

};

#endif
