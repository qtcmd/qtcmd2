/***************************************************************************
 *   Copyright (C) 2003 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "viewer.h"

namespace Preview {

QString Viewer::toString( ViewerType eViewerType )
{
	if (eViewerType == TEXT_viewer)
		return "TEXT_viewer";
	else
	if (eViewerType == IMAGE_viewer)
		return "IMAGE_viewer";
	else
	if (eViewerType == SOUND_viewer)
		return "SOUND_viewer";
	else
	if (eViewerType == VIDEO_viewer)
		return "VIDEO_viewer";
	else
	if (eViewerType == BINARY_viewer)
		return "BINARY_viewer";

	return "UNKNOWN_viewer";
}

QString Viewer::toString( ViewerMode eViewerMode )
{
	if (eViewerMode == RAW_mode)
		return "RAW_mode";
	else
	if (eViewerMode == RENDER_mode)
		return "RENDER_mode";
	else
	if (eViewerMode == EDIT_mode)
		return "EDIT_mode";
	else
	if (eViewerMode == UNKNOWN_mode)
		return "UNKNOWN_mode";

	return "UNKNOWN_mode";
}

QString Viewer::toString( ReadingMode eReadingMode )
{
	if (eReadingMode == ALL)
		return "ALL";
	else
	if (eReadingMode == APPEND)
		return "APPEND";
	else
	if (eReadingMode == ON_DEMAND)
		return "ON_DEMAND";
	else
	if (eReadingMode == GET_READABILITY)
		return "GET_READABILITY";

	return "NONE";
}

} // namespace Preview
