/***************************************************************************
 *   Copyright (C) 2016 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _COMMANDLINE_H
#define _COMMANDLINE_H

#include <QLabel>
#include <QToolBar>
#include <QComboBox>

//#include "cmdcompleter.h"

// class CmdCompleter;

class CommandLine : public QToolBar
{
	Q_OBJECT
public:
	CommandLine( QWidget *pParent );
	~CommandLine();

// 	void setModelForCompleter( QAbstractItemModel *pModel );
	void setCurrentPath( const QString &sPath ) { m_sCurrentPath = sPath; m_pCommandLineLabel->setToolTip(sPath); }

	/** Shows popup list.
	 */
	void popup()     { m_comboCommandLine->showPopup(); }
	/** Sets focus in this widget.
	 */
	void setActive() { m_comboCommandLine->setFocus(); }

	/** Returns strings list from the CommandLine.
	 * @return pathes list
	 */
	QStringList & list();

private:
	QLabel          *m_pCommandLineLabel;
	QComboBox       *m_comboCommandLine;
// 	CmdCompleter    *m_completer;
	QStringList      m_initCmdsList, m_cmdsList;

	QString          m_sBackgroundColor;
	QString          m_sCurrentPath;


	void changeBackgroundColor();

// public Q_SLOTS:
// 	void slotCleanCmdLine() { m_comboCommandLine->lineEdit()->clear(); }

private Q_SLOTS:
	void slotEnterPressed( const QString &sCmd );

Q_SIGNALS:
	void signalExeCommandInTerminal( const QString &sCmd );
	void signalChangePathInTerminal( const QString &sPath );

protected:
	bool eventFilter( QObject *pObj, QEvent *pEvent );

};

#endif // _COMMANDLINE_H
