/***************************************************************************
 *   Copyright (C) 2018 by Mariusz Borowski <mariusz@nes.pl>               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along withthisprogram; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef ACTIONPARAMS_H
#define ACTIONPARAMS_H

#include <QCoreApplication>

class ActionSection
{
	Q_DECLARE_TR_FUNCTIONS(ActionSection)

public:
	explicit ActionSection() {}
	explicit ActionSection(const QString &id, const QString &name) : m_id(id), m_name(name) {}

	QString id() const { return m_id; }
	QString name() const { return m_name; }

	inline void setId(const QString &id);
	inline void setName(const QString &name);

private:
	QString m_id;
	QString m_name;
};

inline void ActionSection::setId(const QString &id)
{
	m_id = id;
}

inline void ActionSection::setName(const QString &name)
{
	m_name = name;
}

inline bool operator==(const ActionSection &a, const ActionSection &b)
{
	return a.id() == b.id();
}

inline uint qHash(const ActionSection &key, uint seed)
{
	return qHash(key.id(), seed);
}

struct ActionParams
{
	QString m_actionId;
	ActionSection m_section;
};

Q_DECLARE_METATYPE(ActionParams)


#endif // ACTIONPARAMS_H
