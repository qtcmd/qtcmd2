/***************************************************************************
 *   Copyright (C) 2005 by Mariusz Borowski <b0mar@nes.pl>                 *
 *   Copyright (C) 2009 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDir> // was <QFileSystemModel>
#include <QDebug> // need to qDebug()
#include <QVBoxLayout>
#include <QSortFilterProxyModel>

#include "panel.h"
#include "settings.h"
#include "mimetype.h"
//#include "vfsmodel.h"
#include "mainwindow.h"
#include "pluginmanager.h"
#include "vfsflatlistmodel.h"


Panel::Panel( QWidget *pParent, QWidget *pMainWnd, KeyShortcuts *pKeyShortcuts, const QString &sName )
	: QWidget(pParent), m_pFileListView(nullptr), m_pMainWnd(pMainWnd)
{
	m_pPathView = nullptr;
	m_statusBar = nullptr;
	m_complModel = nullptr;
	m_subsystemMngr = nullptr;
	m_proxyModel = nullptr;
	m_vfsFLModel = nullptr;
	m_vfsFLModelCompl = nullptr;
	m_firstListedStat = false;
	m_pKeyShortcuts = pKeyShortcuts;
	setObjectName(sName);
	setAccessibleName(sName);

	if (sName.contains(":Viewer_")) {
		m_sContentType = "Viewer";
		return;
	}
	m_pMainLayout = new QVBoxLayout(this);
	m_pMainLayout->setSpacing(0);
	m_pMainLayout->setMargin(0);

	// -- set panel sizePolicy
	QSizePolicy panelSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	panelSizePolicy.setHeightForWidth(sizePolicy().hasHeightForWidth());
	panelSizePolicy.setHorizontalStretch(0);
	panelSizePolicy.setVerticalStretch(0);
	setSizePolicy(panelSizePolicy);

	QString sPanelContentType = (sName.indexOf("left") != -1) ? "LeftPanelContentType" : "RightPanelContentType";
	Settings settings;
	m_sContentType = settings.value("filelistview/"+sPanelContentType, "FlatFileListView").toString();
}

Panel::~Panel()
{
	qDebug() << "Panel::~Panel(). DESTRUCTOR" << accessibleName();
	if (objectName().contains(":Viewer_"))
		return;

	qDebug() << "Panel::~Panel(). DESTRUCTOR for listview";
	// --- update configuration
	QString sPanelContentType = (accessibleName().indexOf("left") != -1) ? "LeftPanelContentType" : "RightPanelContentType";
	Settings settings;
	if (settings.value("filelistview/"+sPanelContentType).isNull())
		settings.setValue("filelistview/"+sPanelContentType, "FlatFileListView");

	delete m_complModel;
	delete m_pPathView;
	delete m_pFileListView;
	delete m_subsystemMngr;
	delete m_proxyModel;
	delete m_vfsFLModel;
	delete m_vfsFLModelCompl;
}

void Panel::initPanelView()
{
	// -- create path view
	m_pPathView = new PathView(this, accessibleName());
	m_pPathView->setModelForCompleter(m_complModel);
	// -- create status bar
	m_statusBar = new StatusBar(this, true); // true - showListStatus
	//connect(m_pViewMngr, static_cast<void(ViewManager::*)(const QString & , uint, bool)>(&ViewManager::signalUpdateStatusBar),  m_statusBar, &StatusBar::slotSetMessage); // viewPlugin embeded

	m_subsystemMngr = new SubsystemManager(m_pPathView->prevUri(), accessibleName(), m_pKeyShortcuts, m_pMainWnd);
	connect(m_subsystemMngr, static_cast<void(SubsystemManager::*)(const URI &, bool, bool)>(&SubsystemManager::signalPathViewChange),  this, &Panel::slotPathViewPathChange);
	connect(m_subsystemMngr, &SubsystemManager::signalCloseConnection, this, &Panel::slotCloseConnection);

	connect(m_subsystemMngr, static_cast<void(SubsystemManager::*)(const QString &, uint, bool)>(&SubsystemManager::signalUpdateStatusBar),
			m_statusBar, static_cast<void(StatusBar::*)(const QString &, uint, bool)>(&StatusBar::slotSetMessage));
	connect(m_pPathView, &PathView::signalPathChanged, m_subsystemMngr, &SubsystemManager::slotPathChanged);
	connect(m_pPathView, &PathView::signalPathChangedForCompleter, m_subsystemMngr, &SubsystemManager::slotPathChangedForCompleter);
	connect(m_pPathView, &PathView::signalRestorePath, m_subsystemMngr, &SubsystemManager::slotRestorePath);
	connect(m_pPathView, &PathView::signalOpenFileDialog, (MainWindow *)m_pMainWnd, &MainWindow::slotOpenDirectory);

	// -- create view
	m_pFileListView = new FileListView(m_subsystemMngr, accessibleName(), this);
	m_pFileListView->setKeyShortcuts(m_pKeyShortcuts);
	// TODO:  VfsModel *vflModel; // parent of VfsFlatListModel and VfsTreeListModel (inherit from VfsModel). VfsModel inherit with QAbstractItemModel.
// 	connect(m_pFileListView, static_cast<void(FileListView::*)(bool)>(&FileListView::signalDuplicateTab), (MainWindow *)m_pMainWnd, &MainWindow::slotDuplicateCurrentTab);
	connect(m_pFileListView, &FileListView::signalDuplicateTab, (MainWindow *)m_pMainWnd, &MainWindow::slotDuplicateCurrentTab);
	connect(m_pFileListView, &FileListView::signalCloseTab, (MainWindow *)m_pMainWnd, &MainWindow::slotCloseCurrentTab);
	connect(m_pFileListView, &FileListView::signalAddTab, (MainWindow *)m_pMainWnd, &MainWindow::slotAddTab);
	connect(m_pFileListView, static_cast<void(FileListView::*)(int)>(&FileListView::signalSwitchToTab), (MainWindow *)m_pMainWnd, &MainWindow::slotSwitchToTab);
	connect(m_pFileListView, &FileListView::signalUpdatePathInTermianl, (MainWindow *)m_pMainWnd, &MainWindow::slotUpdatePathInTermianl);
//	connect(m_pFileListView, &FileListView::signalUpdateColWidthInOppositePanel, (MainWindow *)m_pMainWnd, &MainWindow::slotUpdateColWidthInOppositePanel);
	setColumnWidthSynchro(true);  // connect or disconnect signal

	connect(m_pFileListView, &FileListView::signalFocusToSecondPanel, (MainWindow *)m_pMainWnd, &MainWindow::slotFocusToSecondPanel); // panel <-> panel communication
	// -- for properly updating StatusBar object
	connect(m_selectionModel, &VfsFlatListSelectionModel::signalSelectionChanged, m_pFileListView, &FileListView::slotUpdateListViewStatusBar);
	connect(m_vfsFLModel, &VfsFlatListModel::signalModelChanged, m_pFileListView, &FileListView::slotUpdateListViewStatusBar);
// 	connect(m_vfsFLModel, &VfsFlatListModel::signalSetSelected, m_selectionModel, &VfsFlatListSelectionModel::slotSetSelected);

	connect<void(FileListView::*)(uint, uint, qint64)>(m_pFileListView, &FileListView::signalUpdateOfListStatus, m_statusBar, &StatusBar::slotUpdateOfListStatus);
	connect(m_pFileListView, static_cast<void(FileListView::*)(const QString &)>(&FileListView::signalUpdateOfListStatus), m_statusBar, static_cast<void(StatusBar::*)(const QString &)>(&StatusBar::slotSetMessage));

	// cased inifinty loop for signal -> slot and in result crash after opening directory in second panel.
	//connect(m_subsystemMngr, &SubsystemManager::signalAdjustColumnSize, m_pFileListView, &FileListView::slotAdjustColumnSize);

	connect(m_subsystemMngr, &SubsystemManager::signalPanelListed, (MainWindow *)m_pMainWnd, &MainWindow::slotPanelListed);

	connect(m_pFileListView, &FileListView::signalGetPathFromOppositeActiveTab, (MainWindow *)m_pMainWnd, &MainWindow::slotGetPathFromOppositeActiveTab);
	connect(m_pFileListView, &FileListView::signalUpdateStatusBar, (MainWindow *)m_pMainWnd, &MainWindow::slotSetMsgOnStatusBar);

	connect(m_subsystemMngr, &SubsystemManager::signalGetPathFromTab, (MainWindow *)m_pMainWnd, &MainWindow::slotGetPathFromTab);
	connect(m_subsystemMngr, &SubsystemManager::signalGetRequestingPanelName, (MainWindow *)m_pMainWnd, &MainWindow::slotGetRequestingPanelName);
	connect(m_subsystemMngr, &SubsystemManager::updateItemInAllMatchingTabs, (MainWindow *)m_pMainWnd, &MainWindow::slotUpdateItemInAllMatchingTabs);
	connect(m_subsystemMngr, &SubsystemManager::signalGetPathFromOppositeActiveTab, (MainWindow *)m_pMainWnd, &MainWindow::slotGetPathFromOppositeActiveTab);
	connect(m_subsystemMngr, &SubsystemManager::signalForceUnmarkItemsInOppositeActiveTab, (MainWindow *)m_pMainWnd, &MainWindow::slotForceUnmarkItemsInOppositeActiveTab);
	connect(m_subsystemMngr, &SubsystemManager::signalOpenInTab, (MainWindow *)m_pMainWnd, &MainWindow::slotOpenInTab);
	connect(m_subsystemMngr, &SubsystemManager::signalCopyFilesFromOppositePanel, (MainWindow *)m_pMainWnd, &MainWindow::slotCopyFilesFromOppositePanel);
	connect(m_subsystemMngr, &SubsystemManager::signalUpdateListViewStatusBar, m_pFileListView, &FileListView::slotUpdateListViewStatusBar);
	connect(m_subsystemMngr, &SubsystemManager::signalRemoveFiles, (MainWindow *)m_pMainWnd, &MainWindow::slotRemoveFiles);

	connect(m_subsystemMngr, &SubsystemManager::signalAddMatchedItem, (MainWindow *)m_pMainWnd, &MainWindow::slotAddMatchedItemIntoFindFileDialog);
	connect(m_subsystemMngr, &SubsystemManager::signalSearchingFinished, (MainWindow *)m_pMainWnd, &MainWindow::slotSearchingFinished);
	connect(m_subsystemMngr, &SubsystemManager::signalUpdateRemovedOnFindFileDialog, (MainWindow *)m_pMainWnd, &MainWindow::slotUpdateRemovedFindFileDialog);

	// -- set widgets
	m_pMainLayout->addWidget(m_pPathView);
	m_pMainLayout->addWidget(m_pFileListView);
	m_pMainLayout->addWidget(m_statusBar);

	setTabOrder(m_pPathView->pathView(), m_pFileListView->filelistView());
}

void Panel::setColumnWidthSynchro( bool bOn )
{
	if (bOn)
		connect(m_pFileListView, &FileListView::signalUpdateColWidthInOppositePanel, (MainWindow *)m_pMainWnd, &MainWindow::slotUpdateColWidthInOppositePanel);
	else
		disconnect(m_pFileListView, &FileListView::signalUpdateColWidthInOppositePanel, (MainWindow *)m_pMainWnd, &MainWindow::slotUpdateColWidthInOppositePanel);
}

void Panel::setPanelView( const URI &initUri )
{
	QString sFN = "Panel::setPanelView (1):" + accessibleName();
	qDebug() << sFN;
	URI uri = initUri;
	QString sName = accessibleName();
	Settings settings;
	if (initUri.path().isEmpty()) {
		QString name;
		// m_sPanelInitPath - used by multiTab feature for setting tab name
		sName.insert(accessibleName().indexOf('_'), "Tab"); // leftPanelTab_NUM or rightPanelTab_NUM
		if (accessibleName().indexOf("left") != -1) {
			name = sName+"/Path";
			m_sPanelInitURIstr = settings.value(name, "/home").toString(); // +"/test_archives"
			//m_sPanelInitPath = m_pSettings->value(name, QDir::homePath()).toString(); // +"/test_archives"
//			qDebug() << "  -- config key name:" << name << " m_sPanelInitPath:" << m_sPanelInitPath;
		}
		else
		if (accessibleName().indexOf("right") != -1)
			m_sPanelInitURIstr = settings.value(sName+"/Path", "/").toString();
		else
			m_sPanelInitURIstr = initUri.toString();

		if (m_sPanelInitURIstr.isEmpty()) {
			qDebug() << sFN << "WARNING: read empty path for" << sName << "! set '/'";
			m_sPanelInitURIstr = "/";
		}
		uri.setUri(m_sPanelInitURIstr);
	}
	else {
		m_sPanelInitURIstr = initUri.toString();
	}
// 	URI newUri(m_sPanelInitURIstr);
	m_URI.setUri(m_sPanelInitURIstr);

	if (m_sContentType == "FlatFileListView") {
		// -- create models
		m_vfsFLModel = new VfsFlatListModel(sName);
//		m_vfsFLModel->setColumnsId(1, 2, 3, 4, 5); // TODO: Panel::initPanel. Column's id and visiblity read from config file
		bool m_bShowIconsCompl = settings.value("filelistview/ShowIcons", true).toBool();
		m_vfsFLModelCompl = new VfsFlatListModel(sName);
		m_vfsFLModelCompl->setShowIcons(m_bShowIconsCompl);
		m_proxyModel = new VfsFlatListProxyModel(this);
		m_proxyModel->setSourceModel(m_vfsFLModel);
		//QString sColumns = m_pSettings->value("filelistview/Columns", "1-true,2-true,3-true,4-true,5-true").toString();
		m_proxyModel->setColumnsId(1, 2, 3, 4, 5); // TODO: Panel::initPanel. Column's id and visiblity read from config file

		m_complModel = new VfsFlatListCompleterModel(this, m_vfsFLModelCompl); // source nodel is set inside

		m_selectionModel = new VfsFlatListSelectionModel(sName, m_vfsFLModel); // used m_vfsFLModel as source model because inside VfsFlatListSelectionModel are called fun.from VfsFlatListModel
		m_vfsFLModel->setSelectionModel(m_selectionModel); // classes: FileListView and SubsystemManager get selectionModel from VfsFlatListModel

		initPanelView(); // inside is created SubsystemManager obj.
		m_pFileListView->setVfsModel(m_proxyModel);
// 		m_vfsFLModel->setSelectionModel(m_selectionModel); // old way
// 		m_pFileListView->setSelectionModel(m_selectionModel); // old way
// 		m_subsystemMngr->setSelectionModel(m_selectionModel); // old way
		m_subsystemMngr->setVfsModelForCompleter(m_vfsFLModelCompl);
		m_selectionModel->setProxyModel(m_proxyModel);

		// -- list location
		// Must be done like below.
		// I  reason: In error directory reading case application isn't visible.
		// II reason: Method m_subsystemMngr->slotPathChanged(m_URI.toString()); can't be call directly here due to calling inside method TabPanelsWidget class, that isn't created yet.
		//Panel::slotPathViewPathChange
		//((MainWindow *)m_parentWnd)->setTabTitle(uri.toString(), sTabPanelName, tabId);
		qDebug() << sFN << "Call singleShot for slotListLocation to start delayed listing of url:" << m_URI.toString();
		m_subsystemMngr->initURI(m_URI);
		QTimer::singleShot(5, this, &Panel::slotListLocation); // slotListLocation uses m_URI
		m_firstListedStat = false;
	}
	else if (m_sContentType == "TreeFileListView") {
		qDebug() << sFN << m_sContentType << "- not implemented yet.";
	}
	else if (m_sContentType == "FileContentView") {
		qDebug() << sFN << m_sContentType << "- not implemented yet.";
	}
	else
		qDebug() << sFN << "ERROR. Incorrect type of PanelContent!";
}

bool Panel::setPanelView( FileInfoList *pFileInfoList, const QString &sTabTitle )
{
	QString sFN = "Panel::setPanelView (2): " + objectName();
	qDebug() << sFN;
	if (pFileInfoList != nullptr && pFileInfoList->isEmpty()) {
		qDebug() << sFN << "Input fileInfoList is nullptr or empty";
		return false;
	}
	m_sPanelInitURIstr = sTabTitle;

	if (m_sContentType == "FlatFileListView") {
		// -- create model
		m_vfsFLModel = new VfsFlatListModel(accessibleName(), true);
		m_vfsFLModelCompl = new VfsFlatListModel(accessibleName(), true);
		m_proxyModel = new VfsFlatListProxyModel(this, true);
		m_proxyModel->setSourceModel(m_vfsFLModel);
		//QString sColumns = m_pSettings->value("filelistview/Columns", "1-true,2-true,3-true,4-true,5-true").toString();
		m_proxyModel->setColumnsId(1, 2, 3, 4, 5, 6); // TODO: Panel::initPanel. Column's id and visiblity read from config file

		m_complModel = new VfsFlatListCompleterModel(this, m_vfsFLModelCompl); // necessary for change directory to one from found

		m_selectionModel = new VfsFlatListSelectionModel(accessibleName(), m_vfsFLModel); // because inside are called fun.from VfsFlatListModel
		m_vfsFLModel->setSelectionModel(m_selectionModel);
// 		m_complModel->setSourceModel(m_vfsFLModel); // old way
// 		m_subsystemMngr->setSelectionModel(m_selectionModel); // old way
// 		m_vfsFLModel->setSelectionModel(m_selectionModel); // old way

		if (pFileInfoList == nullptr && ! sTabTitle.isEmpty()) {
			pFileInfoList = m_vfsFLModel->inportDataModel(sTabTitle);
			if (pFileInfoList == nullptr) {
				qDebug() << sFN << "ERROR. Cannot find data model file in config location for title:" << sTabTitle << "set in configuration!";
				return false;
			}
			dynamic_cast <MainWindow *> (m_pMainWnd)->slotPanelListedFR(objectName());
		}
		initPanelView();
		m_pFileListView->setVfsModel(m_proxyModel);
		m_pPathView->setFindResultName(sTabTitle);

		if (! findFileResultMode()) // patm model is not necessary for findFileResultMode
			m_subsystemMngr->setVfsModelForCompleter(m_vfsFLModelCompl);

		m_vfsFLModel->insertItems(pFileInfoList, sTabTitle);
		m_vfsFLModelCompl->insertItems(pFileInfoList, sTabTitle);

		m_firstListedStat = false;
		m_statusBar->slotUpdateOfListStatus(0, m_vfsFLModel->rowCount(), 0);
		if (! findFileResultMode())
			m_pFileListView->slotAdjustColumnSize();
		// need to set properly subsystem be able to call commands on found files
		m_subsystemMngr->setSubsystemFromMime(pFileInfoList->first()->filePath());
		// FIXME: what if looking inside archive too, will be turned on (on the list will are: localsubsystem files and archive files)
		//        in this case will be needs to change subsystem on line (when user select file)
	}
	return true;
}

void Panel::skipPathViewConfig( bool /*bSkipConfig*/ )
{
	if (m_pPathView != nullptr)
		m_pPathView->setDontWriteConfig(true);
}

void Panel::setColumnsOnListView( bool bFindFileResultMode )
{
	if (m_pFileListView != nullptr)
		m_pFileListView->setColumns(bFindFileResultMode);
}

bool Panel::findFileResultMode() const
{
	if (m_pFileListView != nullptr)
		return m_pFileListView->findFileResultMode();

	return false;
}

// ---------------------- S L O T s -----------------------

void Panel::slotSetFileListViewActive()
{
	m_pFileListView->setActive(); // m_tvFileList->setFocus();
	m_pFileListView->slotUpdateFocusSelection(false, "");
}

void Panel::slotPathViewPathChange( const URI &uri, bool bUpdateComplModel, bool bAddPathToHistory )
{
	QString sFN = "Panel::slotPathViewPathChange: " + accessibleName();
	if (m_pFileListView->findFileResultMode())
		return;
	qDebug() << sFN << "uri:" << uri.toString() << ((!bUpdateComplModel && !bAddPathToHistory) ? "*dummy call for removing failed path" : "");
// 	qDebug() << "  -- updateComplModel, addPathToHistory" << updateComplModel << addPathToHistory;
// 	if (! m_pFileListView->isActive()) // m_pFileListView->isActiveWindow()  // PathView is not updated after start of application
// 		return;

	if (uri.isValid()) {
		if (bAddPathToHistory) {
			m_pPathView->setPath(uri);
			m_pFileListView->setActive();
		}
		else {
			URI myURI = uri;
			if (! uri.path().endsWith(QDir::separator())) {
				myURI.setPath(uri.path() + QDir::separator());
			}
			QString sUri = myURI.toString();
			if (myURI.mime().contains("remote")) {
				int id = sUri.indexOf("://")+3;
				sUri.remove(id, sUri.indexOf('@')-id+1);
			}
			m_pPathView->removePath(sUri);
		}
	}

	if (bUpdateComplModel) {
		if (uri.isValid()) {
			if (m_pPathView != nullptr) {
				if (! m_pPathView->isVisible()) {
					m_pPathView->setFindResultName(""); // show PathView and set variable (FindFileResult flag)
	// 				m_vflModel->setFindFileResultMode(false); // set in SubsystemManager::openFile
					m_proxyModel->setColumnsId(1, 2, 3, 4, 5); // TODO: Panel::initPanel. Column's id and visiblity read from config file
				}
				m_pPathView->setWritableStatus(m_subsystemMngr->dirWritableStatus());
			}
			dynamic_cast <MainWindow *> (m_pMainWnd)->setTabTitle(uri.toString(), accessibleName()); // find tabId using name and set title
		}
		else {
			qDebug() << sFN << "setSourceModel for completer";
			m_complModel->setSourceModel(m_vfsFLModelCompl);
		}
	}
	m_pFileListView->slotUpdateListViewStatusBar();
}

void Panel::slotCloseConnection()
{
	qDebug() << "Panel::slotCloseConnection.";
	QString sNewPath, sNewAbsName, sPrevMime, sPrevHost, sCurrentMime, sCurrentHost;
	URI prevUri, currUri;

	QStringList slPathList = m_pPathView->list();
	if (slPathList.count() == 1) {
		sNewPath = QDir::rootPath();
	}
	else  {
		int nCurrId = m_pPathView->pathView()->currentIndex();
		currUri = m_pPathView->currentUri();
		if (currUri.protocol() == "file") { // or: if (! currUri.host().startsWith("/"))
			sCurrentMime = MimeType::getMimeFromPath(currUri.path(), sCurrentHost, sNewAbsName); // returns: sMime, sCurrentHost, sNewAbsName
			if (sCurrentMime.isEmpty())
				sCurrentMime = currUri.mime();
		}
		else {
			sCurrentMime = currUri.mime();
			sCurrentHost = currUri.host();
		}
		//qDebug() << "--- currUri: " << currUri.toString() << "sCurrentMime" << sCurrentMime << "prevUri.mime()" << prevUri.mime() << "currUri.host()" << currUri.host() << "currUri.protocol()" << currUri.protocol();
		bool bPluginSupport;
		PluginManager *pPluginManager = PluginManager::instance();
		if (! sCurrentMime.startsWith("remote")) {
			// -- quit from archive to its parent's directory
			sPrevMime = MimeType::getMimeFromPath(currUri.toString(), sPrevHost, sNewAbsName); // also sets: sCurrentHost, sNewAbsName
			bPluginSupport = pPluginManager->isPluginRegistrered(sPrevMime, "subsystem");
			if (bPluginSupport && sPrevMime != "folder" && sPrevMime != "folder/symlink") { // this is archive
				sNewPath = sPrevHost.left(sPrevHost.lastIndexOf(QDir::separator(), -2)) + QDir::separator();
			}
		}
		else { // -- quit from remote subsystem (ftp) to the first local subsystem directory
			for (int i = nCurrId; i >= 0; --i) {
				prevUri = URI(slPathList[i]);
				if (prevUri.protocol() == "file") { // or: if (! currUri.host().startsWith("/"))
					sPrevMime = MimeType::getMimeFromPath(prevUri.path(), sPrevHost, sNewAbsName); // returns: sMime, sCurrentHost, sNewAbsName
					if (sPrevMime.isEmpty())
						sPrevMime = prevUri.mime();
				}
				else {
					sPrevMime = prevUri.mime();
					sPrevHost = prevUri.host();
				}
				//qDebug() << "- sPrevMime" << sPrevMime << "sCurrentHost" << sCurrentHost << "sPrevHost" << sPrevHost;
				if (sCurrentMime != sPrevMime) {
					bPluginSupport = pPluginManager->isPluginRegistrered(sPrevMime, "subsystem");
					if (bPluginSupport && sPrevMime != "folder" && sPrevMime != "folder/symlink") // this is archive
						sNewPath = prevUri.toString();
					else
						sNewPath = sNewAbsName;
					break;
				}
			}
		}
		if (sNewPath.isEmpty()) // not found different host than that of current path
			sNewPath = QDir::rootPath();
	}

	m_subsystemMngr->slotPathChanged(sNewPath);
}

void Panel::slotListLocation()
{
	qDebug() << "Panel::slotListLocation:" << accessibleName() << "for url:" << m_URI.toString() << "(call slotPathChanged for SubsystemManager)";
	m_firstListedStat = true;
	m_subsystemMngr->slotPathChanged(m_URI.toString());
}

/*
void setView(FileListView *view);
void Panel::setView(FileListView *view)
{
	view->setParent(this);
	m_mainLayout->addWidget(view);
}
*/

