/***************************************************************************
 *   Copyright (C) 2017 by Mariusz Borowski <mariusz@nes.pl>               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along withthisprogram; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _ACTIONREGISTER_H_
#define _ACTIONREGISTER_H_

#include <QHash>
#include <QLinkedList>
#include <QKeySequence>


class QAction;

typedef std::list <QAction*> ActionList;
typedef QHash<int, QAction*> ActionHash;
typedef QHash<QString, ActionList> Sections;


/*
 *
 * ActionRegister
 *
 *
 */
class ActionRegister
{
	explicit ActionRegister() {}

public:
	~ActionRegister() {}

	static ActionRegister* instance();

	void add(const QString &section, QAction *action);

	void remove(int actionId, const QString &section);

	QList<QString> sections() const;

	const ActionList& sectionActions(const QString &section) const;

	QAction* action(int actionId);

	void updatePrimaryShortcut(int actionId, const QKeySequence &seq);

	void updateSecondaryShortcut(int actionId, const QKeySequence &seq);

private:
	void addToSection(const QString &section, QAction *action);

	ActionHash m_actions;
	Sections m_sections;
};


#endif
