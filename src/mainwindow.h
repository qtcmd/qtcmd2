/***************************************************************************
 *   Copyright (C) 2005 by Mariusz Borowski <b0mar@nes.pl>                 *
 *   Copyright (C) 2009 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QCloseEvent>
#include <QStatusBar>
#include <QMenuBar>
#include <QUrl>

#include "panel.h"
#include "termwidget.h"
#include "helpdialog.h"
#include "commandline.h"
#include "tabpanelswidget.h"
#include "pluginmanager.h" // includes also qtcmdconfigdialog.h
#include "settings.h"
#include "keyshortcuts.h"
#include "toolbarpagesettings.h"

class QSplitter;
class IconCache;


class MainWindow: public QMainWindow
{
	Q_OBJECT
public:
	MainWindow();
	virtual ~MainWindow();

	void setTabTitle( const QString &sTabTitle, const QString &sSenderName, int nTabId=-1 );

private:
	IconCache   *m_pIconCache;

	QSplitter   *m_split;

	bool         m_bAppCloseConfirmation;
	QString      m_sVfsPlugins, m_sViewPlugins, m_sConfigPlugins;
	int          m_initWidth, m_initHeight;
	QString      m_sTmpDir;
	QString      m_sRequestingPanel; // usable for Copy, Move and Link creation

	TabPanelsWidget *m_leftTabsPanel, *m_rightTabsPanel, *m_lastTabsPanel;
	bool         m_bAppStarted, m_bInitFocusOnLeftPanel;
	int          m_rightPanelListedTabsCount, m_leftPanelListedTabsCount;

	QStatusBar  *m_pStatusBar;
	bool         m_bShowStatusBar;

	QMenuBar    *m_pMenuBar;
	bool         m_bShowMenuBar;
	QMenu       *m_pTerminalMenu;
	QAction     *m_pShowHideStatusBarAct, *n_pShowHideButtonBarAct, *m_pShowHideToolBarAct, *m_pShowHideMainMenuAct;
	QAction     *m_pShowHideTermAct, *m_pFindInTermAct, *m_pShowHideCmdLineAct;
	QAction     *m_pOpenDirAct, *m_pOpenDocAct, *m_pOpenArchAct,  *m_pCreateNewArchAct, *m_pMoveFilesToArchAct;

	QAction     *m_pFindFilesAct, *m_pConnManagerAct, *m_pFavFoldersAct, *m_pStoragesAct; //, *m_pFavDocsAct;
	QAction     *m_pQuickGetOutSubsysAct, *m_pOneDirBackwardAct, *m_pOneDirForwardAct, *m_pGoToRootDirAct, *m_pGoToHomeDirAct;
	QAction     *m_pConfigQtcmdAct;

	bool         m_bShowHideToolBar;
	QToolBar     *m_pBaseToolBar, *m_pToolsToolBar, *m_pNaviToolBar;
	QActionGroup *m_pBaseToolBarAG, *m_pToolsToolBarAG, *m_pNaviToolBarAG;

	TermWidget  *m_pTermWidget;
	CommandLine *m_pCommandLine;
	bool         m_bShowCommandLine;
	bool         m_bPathSyncBetweenTermAndFLV;

	QToolBar    *m_pButtonBar;
	QPushButton *m_btpButtonBar[10];
	bool         m_bFlatButtonBar, m_bShowButtonBar;
	Panel       *m_pLastActivePanel;

	ConfigurationDialog *m_pQtCmdConfigurationDialog;
	KeyShortcuts *m_pKS;

	QList <QActionGroup *> *m_pToolbarActionsAppLst;
	//typedef QList <QActionGroup *> ActionGroupLst;
	QMap <QString, QAction *> m_toolBarActionsMap;

	FindFileDialog   *m_pFindFileDialog;
	FileInfoList     *m_pFoundFileInfoList;
	FileInfoToRowMap *m_pFoundFileSelectionMap; // used only for found files (in FindFileDialog)
	FileInfoToRowMap *m_pHelperSelectionMap;    // holds one item helped with removing found (in FindFileDialog) item and make way to open archive from remote location (download it)

	void loadPlugins(const Settings &settings);
	void createIconCache(const Settings &settings);

	QIcon icon( const QString &sIconName ) const {
		return m_pIconCache->icon(sIconName);
	}

	void initMenuBar(const Settings &settings);
	void initToolBar(const Settings &settings);
	void initStatusBar(const Settings &settings);
	void initTabPanels();
	void initEmbededTerminal( bool bHideTerminal );
	void initCommandLine(const Settings &settings);
	void initButtonBar(const Settings &settings);
	void initFindFileDialog();

	/** Initializes all configurable key key shortcuts (not all, because some are fixed).
	 * Loads configuration of shortcuts from config file.
	 */
	void initKeyShortcuts();

	/** Returns pointer to chosen TabPanelsWidget.
	 * @param bFromOppositeActiveTab true if needs to get opposite TabPanelsWidget, otherwise current one
	 * @return pointer to chosen TabPanelsWidget
	 */
	TabPanelsWidget *tabPanelsWidget( bool bFromOppositeActiveTab=false );
	Panel *currentPanel( bool bCurrent=true );

	void removeTempDir();
	void createTrashDir( const QString &sPathToTrash );

	void sortOutTabEntriesInConfig( const QString &sSide, int nTabCount );

	/** Updates tool tip for given action with keyshortcut pointed out by given ID.
	 * @param pAction pointer to action
	 * @param nActionId ID of action
	 */
	void addKeyshortcutToActionToolTip( QAction * pAction, int nActionId );

	/** Updates toolbars checking config file.
	 */
	void updateToolBars();

protected:
	void closeEvent( QCloseEvent *pCE );
	void resizeEvent( QResizeEvent *pRE );

private Q_SLOTS:
	void slotShowAbout();
	void slotShowAboutQt();

	void slotUrlActivated( const QUrl &url );

	void slotExitTerminal();
 	void slotPathChangedInTerminal();

	void slotShowHideMenuBar();
	void slotShowHideToolBar();
	void slotShowHideStatusBar();
	void slotShowHideButtonBar();

	void slotShowHideTerminal();
	void slotShowHideCommandLine();
	void slotShowHideTerminalSearchBar();

	void slotRefreshStatusBar();

	void slotConfigureQtCommander();
	void slotConfigureQtCmdShortcuts();
	void slotConfigureToolbar();

	void slotShowFtpSessionsManagerDlg() {
		currentPanel()->openNewRemoteConnection(RemoteConnection::FTP);
	}

	void slotShowFindFilesDialog();

	void slotShowFavoriteFoldersMenu() {
		currentPanel()->showFavoriteFoldersMenu();
	}
// 	void slotShowFavoriteDocsMenu() {
// 		currentPanel()->showFavoriteDocsMenu();
// 	}
	void slotShowStoragesMenu() {
		currentPanel()->showDrivesMenu();
	}

	void slotQuickGetOutOfSubsystem() {
		currentPanel()->quickGetOutOfSubsystem();
	}
	void slotOneDirBackward() {
		currentPanel()->oneDirBackward();
	}
	void slotOneDirForward()  {
		currentPanel()->oneDirForward();
	}
	void slotGoToRootDirectory()  {
		currentPanel()->goToRootDirectory();
	}
	void slotGoToUserHomeDirectory()  {
		currentPanel()->goToUserHomeDirectory();
	}

	/** Depend on subsystem in panel (which passed name) show/hide terminal command line and option(s) in menu.
	 *  Additionaly change terminal availability (switching by Ctrl+O).
	 * @param sPanelName the name of current panel
	 */
	void slotUpdateTerminalBar( const QString &sPanelName );

	void slotOpenDocument();
	void slotOpenArchive();

	void slotCreateNewArchive()  {
		currentPanel()->createNewArchive();
	}
	void slotMoveFilesToArchive() {
		currentPanel()->moveFilesToArchive();
	}

	// buttons bar slots
	// ---
	void slotShowHelp() {
		HelpDialog::show(this, m_pIconCache);
	}
	void slotRename() {
		currentPanel()->renameFile();
	}
	void slotFileView() {
		currentPanel()->fileView();
	}
	void slotFileEdit() {
		currentPanel()->fileEdit();
	}
	void slotCopy() {
		currentPanel()->copyFile();
	}
	void slotMove() {
		currentPanel()->moveFile();
	}
	void slotMakeDir() {
		currentPanel()->makeDir();
	}
	void slotDelete() {
		currentPanel()->deleteFile();
	}
	void slotMakeLink() {
		currentPanel()->makeLink();
	}

	void slotActivateCommandLine() { m_pCommandLine->setActive(); }

	// FindFileDialog
	void slotCloseFindFilesDialog();
	void slotPauseSearch( bool bPause ) {
		currentPanel()->pauseCurrentOperation(bPause);
	}
	void slotStartFindFiles( const FindCriterion &fcFindCriterion ) {
		currentPanel()->findFiles(fcFindCriterion, m_pFoundFileSelectionMap);
	}
	void slotRemoveFoundFile( FileInfo *pFileInfo, QWidget *pParent );
	void slotFoundFilesPutInPanel();
	void slotJumpToFoundFile( FileInfo *pFileInfo );
	void slotViewOfFoundFile( FileInfo *pFileInfo, bool bEditMode );


	/** Applies settings for both list views getting data from given object.
	 * @param listViewSettings reference to container containing list view settings
	 * @param sCategory - category in opened section
	 */
	void slotApplyListViewSettings( const ListViewSettings &listViewSettings, const QString &sCategory );

	/** Applies settings for both list views getting data from given object.
	 * @param fileSystemSettings reference to container containing list view settings
	 */
	void slotApplyFileSystemSettings( const FileSystemSettings &fileSystemSettings );

	/** Applies settings of keyshortcuts for both list views getting data from given object.
	 * @param pKeyShortcuts pointer to container containing keyshortcuts
	 */
	void slotApplyKeyShortcutsSettings( KeyShortcuts *pKeyShortcuts );

	/** Applies settings for tool bar getting data from given object.
	 * @param toolBarPageSettings reference to container containing tool bar settings
	 */
	void slotApplyToolbarsSettings( const ToolBarPageSettings &toolBarPageSettings );

	void slotStopOfSearching() {
		currentPanel()->stopOfSearching();
	}

public Q_SLOTS:
	void slotAddTab();
	void slotCloseCurrentTab();
	void slotDuplicateCurrentTab();

	void slotOpenInTab(const Vfs::URI& uri, Vfs::OpenInTab openInTab);

	void slotFocusToSecondPanel();

	void slotPanelListed();
	void slotPanelListedFR( const QString &sPanelName );

	void slotGetPathFromOppositeActiveTab( QString &sPath );
	void slotGetPathFromTab( const QString &sTabObjName, QString &sPath );
	void slotGetRequestingPanelName( QString &sRequestingPanel ) { sRequestingPanel = m_sRequestingPanel; }

	void slotCopyFilesFromOppositePanel( const QString &sTargetPath, FileInfoToRowMap *pSelectionMap, bool bCopy, bool bWeighBefore );
	void slotRemoveFiles( FileInfoToRowMap *pSelectionMap, bool bWeighBefore );

	void slotUpdateItemInAllMatchingTabs( const QString &sFileName, bool bExists );

	void slotSetMsgOnStatusBar( const QString &sMsg );

	void slotForceUnmarkItemsInOppositeActiveTab( FileInfoList *pProcessedFilesLst );

	void slotSwitchToTab( int nTabId );

	void slotUpdatePathInTermianl();

	void slotUpdateColWidthInOppositePanel( int nLogicalIndex, int nOldSize, int nNewSize );

	void slotQuitApp() { qApp->closeAllWindows(); }

	void slotOpenDirectory();

	void slotAddMatchedItemIntoFindFileDialog( FileInfo *foundFileInfo, const QString &sSearchingResult );

	void slotSearchingFinished() {
		m_pFindFileDialog->slotSearchingFinished();
	}
	void slotUpdateRemovedFindFileDialog( Vfs::FileInfo *pFileInfo ) {
		m_pFindFileDialog->slotUpdateRemovedFindFileDialog(pFileInfo);
	}

};

#endif // MAINWINDOW_H
