/***************************************************************************
 *   Copyright (C) 2012 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QMouseEvent>
#include <QVBoxLayout>
#include <QTabBar>
#include <QMenu>
#include <QDir> // for QDir::separator()
#include <QDebug>
#include <QWidget>

#include "settings.h"
#include "messagebox.h"
#include "squeezelabel.h"
#include "tabpanelswidget.h"
#include "keyshortcutsappids.h"


TabPanelsWidget::TabPanelsWidget( const QString &sTabTitle, QWidget *pMainWnd, KeyShortcuts *pKeyShortcuts, QWidget *pParent )
	: QTabWidget(pParent), m_pMainWnd(pMainWnd), m_pKS(pKeyShortcuts)
{
	qDebug() << "TabPanelsWidget::TabPanelsWidget. tabTitle:" << sTabTitle;

	setTabPosition(QTabWidget::South);
	setFocusPolicy(Qt::NoFocus);
	//setTabsClosable(true);
	setObjectName(sTabTitle);
	setDocumentMode(true);
	setMovable(true);
	m_sPanelName = sTabTitle;
	m_sPanelName.replace("_TabWidget", "Panel_");
	m_sPanelName[0] = m_sPanelName[0].toUpper();
	m_currentTabId = 0;


	QString sSide = m_sPanelName.left(m_sPanelName.indexOf("Panel"));
	Settings settings;
	m_bHideTabIfOpenedOnlyOne = settings.value("mainwindow/Hide"+sSide+"TabIfOpenedOnlyOne", false).toBool();

	sSide[0] = sSide[0].toLower();
	int nTabId = 0, nTabCount = 0;
	QStringList slSettingsGroups(settings.childGroups());
	QStringList slTabNameLst = slSettingsGroups.filter(QRegExp(sSide+"PanelTab_[0-9]+"));
	QString sPanelTabName, sTabId;

	while (nTabCount < slTabNameLst.size()) {
		sPanelTabName = slTabNameLst.at(nTabCount);
		// -- get Id
		sTabId = sPanelTabName;
		sTabId.remove(sSide+"PanelTab_");
		nTabId = sTabId.toInt();
		nTabCount++;
		// -- check FileOfModelData
		if (! settings.value(sPanelTabName +"/FileOfModelData").isNull()) {
			QFileInfo fi(settings.value(sPanelTabName +"/FileOfModelData").toString());
			addTab(QString(sSide+"Panel_%1").arg(nTabId), false, nullptr, fi.completeBaseName());
		}
		else
			addTab(QString(sSide+"Panel_%1").arg(nTabId));
	}
	if (nTabCount == 0)
		addTab(sSide+"Panel_1");
	if (nTabCount <= 1 && m_bHideTabIfOpenedOnlyOne) {
		tabBar()->hide();
	}
	if (nTabCount > 0) {
		sSide = m_sPanelName.left(m_sPanelName.indexOf("_"));
		m_currentTabId = settings.value("filelistview/"+sSide+"CurrentTab", 0).toInt();
		setCurrentIndex(m_currentTabId);
	}
	m_sPanelName[0] = m_sPanelName[0].toLower();

	connect(this, &QTabWidget::currentChanged, this, &TabPanelsWidget::slotSetActiveList);
}

TabPanelsWidget::~TabPanelsWidget()
{
	// -- destroy tabs
	m_panelsList.clear();  // calls Panel's destructor too
	m_widgetsList.clear();

	m_sPanelName[0] = m_sPanelName[0].toUpper();
	QString sSide = m_sPanelName.left(m_sPanelName.indexOf("Panel"));
	Settings settings;
	settings.update("mainwindow/Hide"+sSide+"TabIfOpenedOnlyOne", m_bHideTabIfOpenedOnlyOne);

	sSide = m_sPanelName.left(m_sPanelName.indexOf("_"));
	settings.update("filelistview/"+sSide+"CurrentTab", m_currentTabId);
}


void TabPanelsWidget::addTab( const QString &sPanelName, bool bDuplicate, FileInfoList *pFileInfoList, const QString &sInFindResulTabTitle )
{
	int nTabId = m_widgetsList.size();
	qDebug() << "TabPanelsWidget::addTab() newPanelName:" << sPanelName << "duplicate:" << bDuplicate << "tabId:" << nTabId << "sInFindResulTabTitle:" << sInFindResulTabTitle;
	QString sFindResulTabTitle = sInFindResulTabTitle;
	m_pImportFileInfoList = pFileInfoList; // need for cleaning

	if (pFileInfoList != nullptr && sInFindResulTabTitle.isEmpty()) { // create panel and their content
		bool bParam;
		int id = 0;
		sFindResulTabTitle = InputTextDialog::getText(this, InputTextDialog::PUT_RESULT_TO_PANEL, QString(tr("Find result")+"_%1").arg(count()+1), bParam);
		if (sFindResulTabTitle.isEmpty())
			return;
		while (id < m_widgetsList.size()) {
			if (tabText(id) != sFindResulTabTitle)
				id++;
			else {
				MessageBox::msg(nullptr, MessageBox::WARNING, tr("Tab with given name already exists!\nPlease use other name."));
				sFindResulTabTitle = InputTextDialog::getText(this, InputTextDialog::PUT_RESULT_TO_PANEL, QString(tr("Find result")+"_%1").arg(count()+1), bParam);
				if (sFindResulTabTitle.isEmpty())
					return;
				id = 0;
			}
		}
	}
	QWidget *newTabContainer = new QWidget(m_pMainWnd);
//	newTabContainer->setFocusPolicy((Qt::FocusPolicy)(Qt::TabFocus | Qt::ClickFocus | Qt::StrongFocus | Qt::WheelFocus)); // if focus is allowed for tab then Qt doesn't want to jump to next widget
	QVBoxLayout *verticalLayout = new QVBoxLayout(newTabContainer);
	verticalLayout->setMargin(0);
	verticalLayout->setSpacing(0);
	URI uri;
	Panel *oldPanel = nullptr;
	QStringList oldPanelPathesList;
	if (bDuplicate) {
		oldPanel = m_panelsList.at(currentIndex());
		uri.setUri(oldPanel->getCurrentURI().toString());
		oldPanelPathesList = oldPanel->pathesList();
	}
	if (! m_TabURI.path().isEmpty()) {
		uri = m_TabURI;
	}

// 	int nTabId = m_widgetsList.size();
// 	qDebug() << "  -- tabId:" << nTabId;

	m_widgetsList.append(newTabContainer);
	QTabWidget::addTab(m_widgetsList.at(nTabId), QString("tab%1").arg(nTabId+1));

	bool bCloseTab = false;
	Panel *pPanel = new Panel(newTabContainer, m_pMainWnd, m_pKS, sPanelName);
	if (pFileInfoList == nullptr && sFindResulTabTitle.isEmpty()) // create panel and their content
		pPanel->setPanelView(uri);
	else {
		if (! pPanel->setPanelView(pFileInfoList, sFindResulTabTitle)) // not found "Find Result" file model
			bCloseTab = true;
	}

	if (bDuplicate)
		pPanel->setPathesList(oldPanelPathesList);

	m_panelsList.append(pPanel);
	verticalLayout->addWidget(pPanel);

	if (pFileInfoList == nullptr)
		setTabTitle(pPanel->getPanelInitURIstr(), nTabId); // set tab title with given id
	else
		setTabTitle(sFindResulTabTitle, nTabId);

	if (count() > 1 && m_bHideTabIfOpenedOnlyOne) {
		tabBar()->show();
		m_bHideTabIfOpenedOnlyOne = false;
	}

	if (bCloseTab) {
//		QMessageBox::critical(this, tr("Error tab opening")+" - QtCommander", tr("Cannot open tab containing search result with name")+" "+sPanelName); // made crash
		closeTab(nTabId);
	}

	if (bDuplicate)
		QTimer::singleShot(5, oldPanel, &Panel::slotSetFileListViewActive);
}

void TabPanelsWidget::addTab( const QString &sPanelName, const URI &uri )
{
	m_TabURI = uri;

	addTab(sPanelName);

	m_TabURI.reset();
}


void TabPanelsWidget::closeTab( int nTabId )
{
	qDebug() << "TabPanelsWidget::closeTab() tabTitle:" << m_sPanelName << "nPanelId:" << nTabId;
	if (count() == 1 || nTabId < 0 || nTabId > m_panelsList.size())
		return;
	if (m_panelsList.at(nTabId) == nullptr)
		return;

	QString sKey = m_panelsList.at(nTabId)->objectName(); // m_sPanelName: "leftPanel_"
	sKey.insert(sKey.indexOf('_'), "Tab");

	if (m_panelsList.at(nTabId)->findFileResultMode())
		m_panelsList.at(nTabId)->setColumnsOnListView(false); // prevent to export data to file
	m_panelsList.at(nTabId)->skipPathViewConfig(true);
	delete m_panelsList.at(nTabId); // calls save configuration in other class (FileListView)
	m_panelsList.remove(nTabId);
	m_widgetsList.remove(nTabId);
	removeTab(nTabId); // doesn't remove content

	if (count() == 1 && m_bHideTabIfOpenedOnlyOne)
		tabBar()->hide();

	Settings settings;
	// -- remove file containing model data (already saved)
	if (! settings.value(sKey+"/FileOfModelData").isNull()) {
		QFileInfo fi(settings.value(sKey+"/FileOfModelData").toString());
		QString sPath = QFileInfo(settings.fileName()).absolutePath() + QDir::separator();
		QString sFileName = sPath + fi.completeBaseName() + ".txt";
		QFile(sFileName).remove();
	}
	// -- remove key from configuration
	QStringList tabNameLst(settings.childGroups());
	if (tabNameLst.contains(sKey))
		settings.remove(sKey+"/");

// 	qDebug() << "  -- m_panelsList.size:" << m_panelsList.size() << "m_widgetsList.size:" << m_widgetsList.size();
}

void TabPanelsWidget::setActiveTab( int nTabId )
{
	if (nTabId < 1 || nTabId > count())
		return;
	nTabId--; // because tabs are indexing starting from 0
// 	qDebug() << "TabPanelsWidget::setActiveTab. TabId:" << nTabId;
	setCurrentIndex(nTabId);
}

void TabPanelsWidget::setTabTitle( const QString &sTabTitle, int nTabId )
{
	if (nTabId > count())
		return;
	qDebug() << "TabPanelsWidget::setTabTitle. TabTitle:" << sTabTitle << "TabId:" << nTabId << objectName();
	QString sShortTitle, sToolTip = sTabTitle;
	if (sTabTitle.endsWith(QDir::separator())) {
		int pos = sTabTitle.lastIndexOf('/', -2) + 1;
		int len = sTabTitle.length() - pos - 1;
		sShortTitle = sTabTitle.mid(pos, len);
	}
	else
		sShortTitle = sTabTitle.right(sTabTitle.length() - sTabTitle.lastIndexOf(QDir::separator()) - 1);

	if (sShortTitle.isEmpty())
		sShortTitle = QDir::rootPath();

	int nNewTabId = (nTabId >= 0) ? nTabId : currentIndex();

	sToolTip.insert(0, "<strong>"+sShortTitle+":</strong>\n");
	const int MAX_CHARACTERS = 20;
	if (sShortTitle.size() > MAX_CHARACTERS)
		sShortTitle = SqueezeLabel::squeezeLine(sShortTitle, MAX_CHARACTERS, tabBar());

	setTabText(nNewTabId, sShortTitle);
	//tabBar()->setTabText(nNewTabId, sShortTitle);
	tabBar()->setTabToolTip(nNewTabId, sToolTip);
}

Panel * TabPanelsWidget::tabPanel( int nInTabId )
{
	int nTabId = nInTabId;
	int nMaxId = m_panelsList.size()-1;
	if (nInTabId > nMaxId) {
		qDebug() << "TabPanelsWidget::tabPanel. WARNING. Passed Id:" << nInTabId << "is over the range:" << nMaxId;
		qDebug() << "TabPanelsWidget::tabPanel. -- NOTE. Will be used max available Id.";
		nTabId = nMaxId;
	}
	return m_panelsList.at(nTabId);
}

Panel * TabPanelsWidget::tabPanel( const QString &sTabObjName )
{
	Panel *pPanel = nullptr;
	for (int i = 0; i < m_panelsList.size(); ++i) {
		if (m_panelsList.at(i)->objectName() == sTabObjName) {
			pPanel = m_panelsList.at(i);
			break;
		}
	}
	return pPanel;
}

void TabPanelsWidget::slotAddTab()
{
	QString sNewPanelName = m_sPanelName + QString("%1").arg(m_widgetsList.size()+1);
	addTab(sNewPanelName);
}

void TabPanelsWidget::slotDuplicateTab()
{
	QString sNewPanelName = m_sPanelName + QString("%1").arg(m_widgetsList.size()+1);
	addTab(sNewPanelName, true);
}

void TabPanelsWidget::slotCloseTab()
{
	if (m_clickedTabId < 0)
		return;

	closeTab(m_clickedTabId);
}

void TabPanelsWidget::slotCloseOtherTabs()
{
	int allTabs = count();
	for (int i=allTabs-1; i>=0; i--) {
		if (i != currentIndex())
			closeTab(i);
	}
}

void TabPanelsWidget::slotSetActiveList( int nTabId ) // called after user switches tab
{
	if (nTabId < 0 || nTabId > m_panelsList.size()-1)
		return;
// 	qDebug() << "TabPanelsWidget::slotSetActiveList. TabId:" << nTabId << "senderName:" << sender()->objectName();
	m_currentTabId = nTabId;
	if (m_panelsList.size() > 0)
		m_panelsList.at(nTabId)->slotSetFileListViewActive();

	emit signalUpdateStatusBar();
	emit signalUpdateTerminalBar(sender()->objectName());
}

void TabPanelsWidget::slotHideTabs()
{
	if (count() > 1)
		return;

	if (tabBar()->visibleRegion().isEmpty())
		tabBar()->show();
	else
		tabBar()->hide();

	m_bHideTabIfOpenedOnlyOne = tabBar()->visibleRegion().isEmpty();
}


void TabPanelsWidget::mousePressEvent( QMouseEvent *me )
{
	// -- determine clicked tab
	int x = me->x();
	int y = me->y() - (height()-tabBar()->height());
	m_clickedTabId = tabBar()->tabAt(QPoint(x,y));
	//qDebug() << "TabPanelsWidget::mousePressEvent. m_clickedTabId:" << m_clickedTabId << "me->pos" << QPoint(x,y);
	if (y < 0)
		return;

	if (me->buttons() == Qt::MidButton) {
		if (y > 0)
			slotCloseTab();
		return;
	}
	else if (me->buttons() == Qt::RightButton) {
		// -- build menu
		QPoint mePos = mapToGlobal(me->pos());
		QMenu *menu = new QMenu(this);
		menu->addAction(m_pKS->keyDesc(DuplicateCurrentTab_APP), this, SLOT(slotDuplicateTab()), m_pKS->keyCode(DuplicateCurrentTab_APP));
// 		menu->addAction(tr("Open &new tab"),         this, SLOT(slotAddTab()));
		QAction *ont = menu->addAction(m_pKS->keyDesc(NewTabInCurrentPanel_APP), this, SLOT(slotAddTab()), m_pKS->keyCode(NewTabInCurrentPanel_APP));
		QString sActCloseTab = m_pKS->keyDesc(CloseCurrentTab_APP);
		if (m_clickedTabId != -1)
			sActCloseTab = tr("&Close tab");
		else
			m_clickedTabId = currentIndex();
		QAction *pCloseTabA = menu->addAction(sActCloseTab, this, SLOT(slotCloseTab()), m_pKS->keyCode(CloseCurrentTab_APP));
		if (count() > 1)
			menu->addAction(tr("Close &all other tabs"), this, SLOT(slotCloseOtherTabs()));
		//menu->addAction(tr("&Rename current tab"),   this, SLOT(slotRenameTab()));
		//menu->addAction(tr("Change color of current tab"),   this, SLOT(slotChangeColorTab()));
		if (count() == 1) {
			menu->addSeparator();
			QString sActionTitle = (tabBar()->visibleRegion().isEmpty()) ? tr("Show tab") : tr("Hide tab");
			QAction *hideTabs = new QAction(sActionTitle, this);
			connect(hideTabs, &QAction::triggered, this, &TabPanelsWidget::slotHideTabs);
			menu->addAction(hideTabs);
			pCloseTabA->setVisible(false);
		}
		QAction *e = menu->exec(mePos);
		if (e == ont)
			setCurrentIndex(count()-1);
		return;
	}
/*	else {
		// doubleClick: RenameTab
	}*/

	QTabWidget::mousePressEvent(me);
}

