/***************************************************************************
 *   Copyright (C) 2013 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QtGui>
#include <QDebug>
#include <QDesktopWidget>
#include <QGuiApplication>

#include "findfiledialog.h"
#include "findfilelistview.h"
// #include "../../src/contextmenucmdids.h"
#include "contextmenucmdids.h"
#include "listviewitemdelegate.h"


FindFileListView::FindFileListView( QWidget *pParent )
	: TreeView(pParent)
{
	setWindowTitle(tr("FindFileListView"));
	resize(400, 200);

	setItemsExpandable(false);
	setRootIsDecorated(false);
	setAlternatingRowColors(true);
	setEditTriggers(QAbstractItemView::NoEditTriggers);
	setSortingEnabled(true);
	sortByColumn(0, Qt::AscendingOrder);

	setItemDelegate(new ListViewItemDelegate(this));

	m_model = new FindFileListViewModel(true);
	m_proxyModel = new FindFileListViewProxyModel(this);
	m_proxyModel->setSourceModel(m_model);
	setModel(m_proxyModel);

	header()->setSectionResizeMode(QHeaderView::Interactive);
	header()->resizeSection(header()->logicalIndex(0), window()->width()/3); // NAME
	header()->resizeSection(header()->logicalIndex(1), window()->width()/7); // SIZE
	header()->resizeSection(header()->logicalIndex(2), window()->width()/2); // PATH
	header()->resizeSection(header()->logicalIndex(3), window()->width()/4); // TIME
	header()->resizeSection(header()->logicalIndex(7), window()->width()/3); // FILE TYPE

	m_pContextMenu = new ContextMenu(this);
	connect(m_pContextMenu, &ContextMenu::signalContextMenuAction, this, &FindFileListView::slotContexMenuAction);
}

FindFileListView::~FindFileListView()
{
	qDebug() << "FindFileListView::~FindFileListView";
	delete m_pContextMenu;
	delete m_proxyModel;
	delete m_model;
}

void FindFileListView::initListView( Vfs::FileInfoList *pFoundFilesList, KeyShortcuts *pKS )
{
	if (pFoundFilesList == nullptr || pFoundFilesList->isEmpty())
		return;

	m_pContextMenu->init(pKS);
	m_pContextMenu->setContextMenuType(ContextMenu::FindFileListViewMENU);
	m_model->init(pFoundFilesList);
}

void FindFileListView::addMatchedItem( Vfs::FileInfo *pFileInfo )
{
	m_model->addItem(pFileInfo);
}

void FindFileListView::setFirstAsCurrent()
{
	if (m_model->rowCount() > 0)
		setCurrentIndex(model()->index(0, 0));
}

Vfs::FileInfo *FindFileListView::currentFileInfo()
{
	QModelIndex srcIndex = static_cast <const FindFileListViewProxyModel *>(currentIndex().model())->mapToSource(currentIndex());
	return m_model->fileInfo(srcIndex);
}

void FindFileListView::showContextMenu( bool bOnCursorPos )
{
	if (m_pContextMenu == nullptr)
		return;

	QPoint position;
	QRect vr = visualRect(currentModelIndex());

	if (bOnCursorPos)
		position = QPoint(QCursor::pos().x() - 2, QCursor::pos().y() + 2);
	else // for Key_Menu
		position = mapToGlobal(QPoint(header()->sectionSize(0), vr.y() + vr.height() + header()->height() + 1));

	int y = position.y(), menuHeight = m_pContextMenu->sizeHint().height();
	if (y + menuHeight > QGuiApplication::screens().at(0)->availableSize().height()) {
// 	if (y + menuHeight > QApplication::desktop()->screenGeometry().height()) {
		position.setY( y - menuHeight );
		if (! bOnCursorPos) // correction for Key_Menu
			position.setY(y - vr.height() - 1);
	}

// 	QModelIndex index = static_cast <const FindFileListViewProxyModel *>(currentModelIndex().model())->mapToSource(currentModelIndex());
// 	FileInfo *fi = m_model->fileInfo(index);

	m_pContextMenu->showMenu(position);
}

void FindFileListView::slotContexMenuAction( int actionId )
{
	if (     actionId == CMD_FindFile_GoToFile)
		(static_cast <FindFileDialog *> (m_pParent))->slotJumpToFile(QModelIndex());
	else if (actionId == CMD_FindFile_ViewFile)
		(static_cast <FindFileDialog *> (m_pParent))->slotViewFile(false);
	else if (actionId == CMD_FindFile_EditFile)
		(static_cast <FindFileDialog *> (m_pParent))->slotViewFile(true);
	else if (actionId == CMD_FindFile_DelFile)
		(static_cast <FindFileDialog *> (m_pParent))->slotRemoveFile();
}



void FindFileListView::keyPressEvent( QKeyEvent *pKE )
{
	switch(pKE->key())
	{
		case Qt::Key_Enter:
		case Qt::Key_Return:
			(static_cast <FindFileDialog *> (m_pParent))->slotJumpToFile(QModelIndex());
			return;

		case Qt::Key_F3:
			(static_cast <FindFileDialog *> (m_pParent))->slotViewFile(false);
			return;

		case Qt::Key_F4:
			(static_cast <FindFileDialog *> (m_pParent))->slotViewFile(true);
			return;

		case Qt::Key_F8:
		case Qt::Key_Delete:
			(static_cast <FindFileDialog *> (m_pParent))->slotRemoveFile();
			return;

		default:
			break;
	}

	return QTreeView::keyPressEvent(pKE);
}

void FindFileListView::mouseReleaseEvent( QMouseEvent *pME )
{
	if (pME->button() == Qt::RightButton)
		showContextMenu(true);

	return QTreeView::mouseReleaseEvent(pME);
}

