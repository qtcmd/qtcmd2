/***************************************************************************
 *   Copyright (C) 2003 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef DRIVESMENU_H
#define DRIVESMENU_H

#include <QProcess>

#include "iconcache.h"
#include "contextmenu.h"


class DrivesMenu : public ContextMenu
{
	Q_OBJECT
public:
	DrivesMenu( QWidget *pParent );
	~DrivesMenu();

	enum Operation { OPENDIR=0, MOUNT, UNMOUNT };

	int  exec( const QPoint &globalPos );

private:
	Operation m_eCurrentOperation;

	QString  m_sMountPath;

	QProcess *m_pProcess;
	QDialog  *m_pInfoDlg;
	IconCache *m_pIconCache;

	bool m_bActionIsFinished;


	bool init();
	void showInfoDlg( const QString &sCaption, const QString &sMsg );
	void runAction();
	void runProcess( Operation eOperation, const QString &sArguments );

	QIcon icon( const QString &sIconName ) const {
		return m_pIconCache->icon(sIconName);
	}

private slots:
	void slotProcessFinished( int nExitCode, QProcess::ExitStatus exitStatus );
	void slotBreakProcess();
	void slotUnmount();
	void slotMount();

signals:
	void signalPathChanged( const QString & sPath );

};

#endif // DRIVESMENU_H
