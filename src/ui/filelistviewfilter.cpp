/***************************************************************************
 *   Copyright (C) 2012 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDebug> // need to qDebug()
#include <QKeyEvent>
#include <QLineEdit>
#include <QToolTip>
#include <QDateTime>

#include "settings.h"
#include "treeview.h"
//#include "../treeview.h"
#include "filelistviewfilter.h"


FilterItem::FilterItem()
{
	m_pattern = "";
	m_wildcard         = false;
	m_regExpression    = false;
	m_caseSensitivity  = false;
	m_startWithPattern = false;
	m_containPattern   = false;
}

QString FilterItem::toString()
{
	return QString("p=%1,re=%2,wd=%3,cs=%4,swp=%5,cp=%6").arg(m_pattern).arg(m_regExpression).arg(m_wildcard).arg(m_caseSensitivity).arg(m_startWithPattern).arg(m_containPattern);
}


FileListViewFilter::FileListViewFilter( TreeView *pTreeView, QWidget *pParent )
	: m_treeView(pTreeView), m_columnNameId(0)
{
	setupUi(this);
	setParent(pParent);
	setMinimumHeight(26);
	setMaximumHeight(26);
	m_bListFiltered = false;
	m_checkBoxAfterToggledProcessing = false;

	//QString sColumns = m_pSettings->value("filelistview/Columns", "1-true,2-true,3-true,4-true,5-true").toString();
	setColumnsId(1, 2, 3, 4, 5);  // TODO: The order of columns should be read from config file

// 	m_closeButton->setShortcut( QKeySequence( tr("Escape")) ); // commented due to override Esc grabbin when are opened filter and search widget

	connect(m_editToolButton,  &QToolButton::toggled, this, &FileListViewFilter::slotComboEditable);
	connect(m_cleanToolButton, &QToolButton::clicked, this, &FileListViewFilter::slotCleanFilter);

	// --- prepare validation regExp
	m_filterValidationRegExp = new QRegExp();
	m_filterValidationRegExp->setCaseSensitivity(Qt::CaseSensitive);
	m_filterValidationRegExp->setPatternSyntax(QRegExp::RegExp);

	// TODO: FileListViewFilter::FileListViewFilter - filters need to read from the configuration and inits map: m_filterMap
	//m_filterComboBox->addItem(".*\\.txt|>=100KB|<01-02-2012"); // test: bad date format
	//m_filterComboBox->addItem(".*\\.txt|<200 KB|<2012 12");    // test: year and hour
	m_filterComboBox->setCurrentIndex(-1);
	m_filterComboBox->setItemData(0, tr("all"), Qt::ToolTipRole);
	connect<void(QComboBox::*)(const QString &)>(m_filterComboBox, &QComboBox::textActivated,  this, &FileListViewFilter::slotComboCurrentIndexChanged);

	loadFilters(); // load filters from configuration file

	// -- toolButton menu
	m_filterMenu            = new QMenu(this);
	m_useRegExpAction       = new QAction(tr("Use regular expression for column Name"), m_filterMenu);
	m_useWildcardAction     = new QAction(tr("Use wildcard for column Name"),           m_filterMenu);
	m_caseSensitivityAction = new QAction(tr("Case sensitivity for column Name"),       m_filterMenu);
	m_filterWildcardMenu         = new QMenu(tr("Wildcard match mode"), m_filterMenu);
	m_wildcardStartingWithAction = new QAction(tr("String starting with..."), m_filterWildcardMenu);
	m_wildcardContainingAction   = new QAction(tr("String containing..."),    m_filterWildcardMenu);

	m_useRegExpAction->setObjectName("UseRegExpAction");
	m_useRegExpAction->setCheckable(true);
	m_useRegExpAction->setShortcut(Qt::ALT|Qt::Key_R);
	m_useRegExpAction->setChecked(false);  // TODO: FileListViewFilter::FileListViewFilter - read from the configuration (m_useRegExpAction is unchecked default)
	connect(m_useRegExpAction, &QAction::toggled, this, &FileListViewFilter::slotToggledInputType);

	m_useWildcardAction->setObjectName("UseWildcardAction");
	m_useWildcardAction->setCheckable(true);
	m_useWildcardAction->setShortcut(Qt::ALT|Qt::Key_W);
	m_useWildcardAction->setChecked(true); // TODO: FileListViewFilter::FileListViewFilter - read from the configuration (m_useWildcardAction is checked default)
	connect(m_useWildcardAction, &QAction::toggled, this, &FileListViewFilter::slotToggledInputType);

	m_caseSensitivityAction->setObjectName("CaseSensitivity");
	m_caseSensitivityAction->setCheckable(true);
	m_caseSensitivityAction->setShortcut(Qt::ALT|Qt::Key_S);
	if (m_useRegExpAction->isChecked())
		m_caseSensitivityAction->setChecked(true); // TODO: FileListViewFilter::FileListViewFilter - read from the configuration (m_caseSensitivity is checked default for regular expr.)
	connect(m_caseSensitivityAction, &QAction::toggled, this, &FileListViewFilter::slotToggledInputType);

	m_wildcardStartingWithAction->setObjectName("WildcardStartingWith");
	m_wildcardStartingWithAction->setCheckable(true);
	m_wildcardStartingWithAction->setChecked(false); // TODO: FileListViewFilter::FileListViewFilter - read from the configuration (m_wildcardStartsWithAction is unchecked default)
	connect(m_wildcardStartingWithAction, &QAction::toggled, this, &FileListViewFilter::slotToggledInputType);

	m_wildcardContainingAction->setObjectName("WildcardContaining");
	m_wildcardContainingAction->setCheckable(true);
	m_wildcardContainingAction->setChecked(true);    // TODO: FileListViewFilter::FileListViewFilter - read from the configuration (m_wildcardContainsAction is checked default)
	connect(m_wildcardContainingAction, &QAction::toggled, this, &FileListViewFilter::slotToggledInputType);

	m_actionsFilterGroup = new QActionGroup(m_filterMenu);
	m_actionsFilterGroup->setExclusive(true);
	m_actionsFilterGroup->addAction(m_useRegExpAction);
	m_actionsFilterGroup->addAction(m_useWildcardAction);

	m_actionsFilterWildcardGroup = new QActionGroup(m_filterWildcardMenu);
	m_actionsFilterWildcardGroup->setExclusive(true);
	m_actionsFilterWildcardGroup->addAction(m_wildcardStartingWithAction);
	m_actionsFilterWildcardGroup->addAction(m_wildcardContainingAction);


	m_filterMenu->addAction(m_useRegExpAction);
	m_filterMenu->addAction(m_useWildcardAction);
	m_filterMenu->addAction(m_caseSensitivityAction);
	m_filterMenu->addMenu(m_filterWildcardMenu);
	m_filterWildcardMenu->addAction(m_wildcardStartingWithAction);
	m_filterWildcardMenu->addAction(m_wildcardContainingAction);
	m_menuToolButton->setMenu(m_filterMenu);

//	changeRegExpParamsNameColInModel(); // sets caseSensitivity and syntaxPattern on the model used to filtering and sets toolTip for toolButton

	m_filterComboBox->installEventFilter(this);

	m_checkBoxAfterToggledProcessing = m_editToolButton->isChecked();

	// -- inits validation helper string lists
	m_weightOperStrLst << tr("bytes") << tr("KB") << tr("MB") << tr("GB") << tr("TB");
	m_dateTimeStrLst
		<< "\\d\\d\\d\\d"                // yyyy
		<< "\\d\\d\\d\\d \\d\\d"         // yyyy HH
		<< "\\d\\d\\d\\d \\d\\d:\\d\\d"  // yyyy HH:mm
		<< "\\d\\d\\d\\d-\\d\\d"                // yyyy-MM
		<< "\\d\\d\\d\\d-\\d\\d \\d\\d"         // yyyy-MM HH
		<< "\\d\\d\\d\\d-\\d\\d \\d\\d:\\d\\d"  // yyyy-MM HH:mm
		<< "\\d\\d\\d\\d-\\d\\d-\\d\\d"                // yyyy-MM-dd
		<< "\\d\\d\\d\\d-\\d\\d-\\d\\d \\d\\d"         // yyyy-MM-dd HH
		<< "\\d\\d\\d\\d-\\d\\d-\\d\\d \\d\\d:\\d\\d"  // yyyy-MM-dd HH:mm
		<< "\\d\\d\\d\\d-\\d\\d-\\d\\d \\d\\d:\\d\\d:\\d\\d";  // yyyy-MM-dd HH:mm:ss
	m_dateFormatStrLst
		<< "yyyy"
		<< "yyyy HH"
		<< "yyyy HH:mm"
		<< "yyyy-MM"
		<< "yyyy-MM HH"
		<< "yyyy-MM HH:mm"
		<< "yyyy-MM-dd"
		<< "yyyy-MM-dd HH"
		<< "yyyy-MM-dd HH:mm"
		<< "yyyy-MM-dd HH:mm:ss";
	m_compareOperStrLst << "=" << "<" << ">" << "!=" << "<>" << "<=" << ">=" << "=>" << "=<" << "==";
	setColumnNameStrLst();

//	m_permStrLst << "^[d\\-\\\\?]" << "^[d\\-\\\\?][r\\-\\\\?]"  << "^[d\\-\\\\?][r\\-\\\\?][w\\-\\\\?]" << "^[d\\-\\\\?][r\\-\\\\?][w\\-\\\\?][sx\\-\\\\?]";
//	("^[d\\-\\\\?][r\\-\\\\?]\{0,1}[w\\-\\\\?]\{0,1}[sx\\-\\\\?]\{0,1}[r\\-\\\\?]\{0,1}[w\\-\\\\?]\{0,1}[sx\\-\\\\?]\{0,1}[r\\-\\\\?]\{0,1}[w\\-\\\\?]\{0,1}[tx\\-\\\\?]\{0,1}");

	m_sRichTxtToolTip = "<HTML><UL><LI>^a.* - files and directories, that name start at a character</UL></HTML>"; // breaks line on 15th character
	m_sRichTxtToolTip  = tr("Pressing Enter saves entered pattern") + ".\n";
	m_sRichTxtToolTip += tr("You can put here values separated by | and ordered by Name,Size,DateTime,Permission,Owner,Group") + ".\n";
	m_sRichTxtToolTip += tr("Examples (regular expression)") + ":\n";
	m_sRichTxtToolTip += "^a.*\t- " + tr("files and directories, that name starts at 'a' character") + "\n";
	m_sRichTxtToolTip += ".*|<DIR>\t- " + tr("show dirs only") + "\n";
	m_sRichTxtToolTip += ".*|=0\t- " + tr("empty files") + "\n";
	m_sRichTxtToolTip += ".*|>= 200KB\t- " + tr("all files that size is great or equal 200 kilobytes") + "\n";
	m_sRichTxtToolTip += ".*|>0|=2011\t- " + tr("all files that were modified at 2011 year") + "\n";
	m_sRichTxtToolTip += ".*|<DIR>|<= 2011-01\t- " + tr("directories that were modified less or equal january 2011") + "\n";
	m_sRichTxtToolTip += ".*|*|<= 2011-01|d\t- " + tr("the same like before") + "\n";
	m_sRichTxtToolTip += ".*|*|= 2011 11|d\t- " + tr("directories that were modified 2011 year and at 11 o'clock") + "\n";
	m_sRichTxtToolTip += tr("DateTime format: 'yyyy-MM-dd hh24:mi:ss'. Not all date's members are required.\nThere must be only the first (year)") + "\n";
	m_sRichTxtToolTip += ".*|>0|*|-??x\t- " + tr("all files, that are executables by user (not all permission are necessary)") + "\n";
	// TODO: maybe help by use F1 should be better (Handled hidden button)
	/*
Pattern where values are separated by | and ordered by Name,Size,DateTime,Permission,Owner,Group.
Examples:
^a.* - files and directories, that name start at a character
.*|<DIR> - show dirs only\n
.*|=0  - empty files
.*|>= 200KB - all files that size is great or equal 200 kilobytes
.*|>0|=2011 - all files that were modified at 2011 year
.*|<DIR>|<= 2011-01 - directories that were modified less or equal january 2011
.*|*|<= 2011-01|d - the same like before
.*|*|= 2011 11|d - directories that were modified 2011 year and at 11 o'clock
DateTime format: "yyyy-MM-dd hh24:mi:ss". Not all date's members are required. There must be only the first (year).
.*|>0|*|-??x - all files, that are executables by user (not all permission are necessary)
*/

//	m_filterComboBox->setToolTip(m_sRichTxtToolTip);
	m_patternSyntax    = QRegExp::Wildcard;
	m_caseSensitivity  = Qt::CaseInsensitive;
}

FileListViewFilter::~FileListViewFilter()
{
	delete m_filterValidationRegExp;
}


void FileListViewFilter::init()
{
	m_filterComboBox->setFocus();

	if (! m_editToolButton->isChecked())
		m_editToolButton->toggle();
	m_filterComboBox->lineEdit()->clear();
	setValidationStatusAsBgColor(ValidStatusType::VS_none);

	m_filterComboBox->setCurrentIndex(0);
	slotComboCurrentIndexChanged("*");
}

void FileListViewFilter::setColumnNameStrLst( bool bFindFileResultPanel )
{
	m_bFindFileResultPanel = bFindFileResultPanel;
	m_columnNameStrLst.clear();
	if (bFindFileResultPanel)
		m_columnNameStrLst << tr("Name") << tr("Size") << tr("Path") << tr("Time") << tr("Permission") << tr("Owner") << tr("Group");
	else
		m_columnNameStrLst << tr("Name") << tr("Size") << tr("Time") << tr("Permission") << tr("Owner") << tr("Group");
}

void FileListViewFilter::setColumnsId( int colSizeId, int colTimeId, int colPermId, int colOwnerId, int colGroupId )
{
	if (colSizeId < 0 || colTimeId < 0 || colPermId < 0 || colOwnerId < 0 || colGroupId < 0) {
		qDebug() << "FileListViewFilter::setColumnsId. parameter < 0";
		return;
	}
	m_columnSizeId  = colSizeId;
	m_columnTimeId  = colTimeId;
	m_columnPermId  = colPermId;
	m_columnOwnerId = colOwnerId;
	m_columnGroupId = colGroupId;
}

void FileListViewFilter::setColumnsId( int colSizeId, int colPathId, int colTimeId, int colPermId, int colOwnerId, int colGroupId )
{
	if (colPathId < 0) {
		qDebug() << "FileListViewFilter::setColumnsId. parameter < 0";
		return;
	}
	m_columnPathId = colPathId;
	setColumnsId(colSizeId, colTimeId, colPermId, colOwnerId, colGroupId);
}

void FileListViewFilter::setVfsProxyModel( VfsFlatListProxyModel *proxyModel )
{
    m_proxyModel = proxyModel;
    m_proxyModel->setDynamicSortFilter(true);
}

void FileListViewFilter::setValidationStatusAsBgColor( ValidStatus validStatus )
{
	QLineEdit *pLineEdit = m_filterComboBox->lineEdit();
	if (pLineEdit == nullptr)
		return;

	QColor   bgColor;
	QPalette palette;

	if (validStatus == ValidStatusType::VS_valid)
		bgColor = QColor( 201,241,215 ); //old: 228, 240, 223
	else
	if (validStatus == ValidStatusType::VS_notValid)
		bgColor = QColor( 241,229,229 ); //old: 255, 229, 229
	else
		bgColor = Qt::white;

	palette.setColor(pLineEdit->backgroundRole(), bgColor);
	palette.setColor(pLineEdit->foregroundRole(), Qt::black);
	pLineEdit->setAutoFillBackground(true);
	pLineEdit->setPalette(palette);
}


void FileListViewFilter::filterItems( bool bFilter )
{
//	qDebug() << "FileListViewFilter::filterItems. filter:" << bFilter;
	m_bListFiltered = bFilter;
	if (! bFilter) {
		m_proxyModel->setFilterRegExp("");
		return;
	}
	QLineEdit *pLineEdit = m_filterComboBox->lineEdit();
	//qDebug() << "m_filterComboBox->currentText():" << m_filterComboBox->currentText(); // empty
	//QString lineEditTxt = (lineEdit != nullptr) ? lineEdit->text() : m_filterMap.key(m_filterComboBox->currentText());
	QString sPattern = m_filterComboBox->currentText();
	if (sPattern.isEmpty())
		sPattern = m_sContextMenuFilter;

	QString lineEditTxt = (pLineEdit != nullptr) ? pLineEdit->text() : m_filterMap.value(sPattern)->m_pattern;
//	qDebug() << "FileListViewFilter::filterItems. lineEditTxt:" << lineEditTxt << "m_caseSensitivity:" << m_caseSensitivity << "m_patternSyntax:" << m_patternSyntax;
	if (lineEditTxt.count('|') == 0) {
		QRegExp regExp(lineEditTxt, m_caseSensitivity, m_patternSyntax);
		m_proxyModel->setFilterRegExp(regExp);
	}
	else {
		m_proxyModel->setFilterRegExp(lineEditTxt);
	}

	m_treeView->scrollTo(m_treeView->currentIndex());
	emit signalUpdateStatusBar();
}

FileListViewFilter::ValidStatus FileListViewFilter::validation( const QString &inputTxt )
{
	ValidStatus validStatus = ValidStatusType::VS_none;
	QString columnTxt;

	// --- do validation
	if (! inputTxt.isEmpty()) {
		//qDebug() << "FileListViewFilter::validation(). inputTxt:" << inputTxt;
		bool validOK = true;
		QStringList filterStrList = inputTxt.split('|');
		for (int i=0; i<filterStrList.count(); ++i) {
			columnTxt = filterStrList[i];
			if (i != m_columnPermId) // because permission has detailed toolTip
				QToolTip::showText(m_treeView->mapToGlobal(QPoint(pos().x(), pos().y()-height()-6)), m_columnNameStrLst[i]);

			if (columnTxt.isEmpty()) {
				validOK = false;
				if (inputTxt.count('|') == m_columnPermId)
					showToolTipForFilterValue();
				break;
			}
			else if (i == m_columnNameId || (m_bFindFileResultPanel && i == m_columnPathId) || i == m_columnOwnerId || i == m_columnGroupId) {
				; // all is correct
			}
			else if (i == m_columnSizeId) {
				if (columnTxt == "*") {
					validOK = true;
				}
				else {
					m_filterValidationRegExp->setPattern("<DIR>");
					validOK = m_filterValidationRegExp->exactMatch(columnTxt);
					if (! validOK) {
						m_filterValidationRegExp->setPattern("^[=!<>]+[ ]*[0123456789]*");
						validOK = m_filterValidationRegExp->exactMatch(columnTxt);
						if (! validOK) {
							// check number with suffixes
							for (int j=0; j<m_weightOperStrLst.count(); ++j) {
								m_filterValidationRegExp->setPattern("^[=!<>]+[ ]*[0123456789]*[ ]*"+m_weightOperStrLst[j]);
								validOK = m_filterValidationRegExp->exactMatch(columnTxt);
								if (validOK)
									break;
							}
						}
						validOK = operatorValidation(columnTxt, validOK);
					}
				}
			} // col_SIZE
			else if (i == m_columnTimeId) {
				if (columnTxt == "*") {
					validOK = true;
				}
				else {
					for (int j=0; j<m_dateTimeStrLst.count(); ++j) {
						m_filterValidationRegExp->setPattern("^[=!<>]+[ ]*"+m_dateTimeStrLst[j]);
						validOK = m_filterValidationRegExp->exactMatch(columnTxt);
						if (validOK) {
							QString  sDateTime = columnTxt.right(columnTxt.length() - columnTxt.indexOf(QRegExp("\\d")));
							QDateTime dateTime = QDateTime::fromString(sDateTime, m_dateFormatStrLst[j]);
							validOK = (dateTime.isValid());
							break;
						}
						validOK = operatorValidation(columnTxt, validOK);
					}
				}
			} // col_TIME
			else if (i == m_columnPermId) {
				if (columnTxt == "*") {
					validOK = true;
				}
				else {
					showToolTipForFilterValue();
/*					if (columnTxt.length() > 8)
						qDebug() << "col_PERMISSION:" << columnTxt;*/
					// example of correct values:  d dr drw drwx -rw? -rw?r
					QString sPtrn  = "^[d\\-\\\\?]([r\\-\\\\?])\{0,1}([r\\-\\\\?][w\\-\\\\?])\{0,1}([r\\-\\\\?][w\\-\\\\?][sx\\-\\\\?])\{0,1}";
							sPtrn += "([r\\-\\\\?][w\\-\\\\?][sx\\-\\\\?][r\\-\\\\?])\{0,1}";
							sPtrn += "([r\\-\\\\?][w\\-\\\\?][sx\\-\\\\?][r\\-\\\\?][w\\-\\\\?])\{0,1}";
							sPtrn += "([r\\-\\\\?][w\\-\\\\?][sx\\-\\\\?][r\\-\\\\?][w\\-\\\\?][stx\\-\\\\?])\{0,1}";
					m_filterValidationRegExp->setPattern(sPtrn);
					//m_filterRegExp->setPattern("^[d\\-\\\\?][r\\-\\\\?]\{0,1}([r\\-\\\\?][w\\-\\\\?])\{0,1}"); // OK
					//("^[d\\-\\\\?][r\\-\\\\?]\{0,1}[w\\-\\\\?]\{0,1}[sx\\-\\\\?]\{0,1}[r\\-\\\\?]\{0,1}[w\\-\\\\?]\{0,1}[sx\\-\\\\?]\{0,1}[r\\-\\\\?]\{0,1}[w\\-\\\\?]\{0,1}[tx\\-\\\\?]\{0,1}");
					validOK = m_filterValidationRegExp->exactMatch(columnTxt);
					// FIXME: FileListViewFilter::validation. After modified 'drwxdrwxdrwx' (using BkSp or Del) to 'drwrwxrwx' (incorrect expr.) expression matched
					//if (m_filterRegExp->exactMatch("drwrwxrwx")) qDebug() << "match"; else qDebug() << "NOT match"; // test (shows 'match')
				}
			} // col_PERMISSION
		} // for
		validStatus = (validOK) ? ValidStatusType::VS_valid : ValidStatusType::VS_notValid;
	}

	setValidationStatusAsBgColor(validStatus);

	if (validStatus == ValidStatusType::VS_none) // for empty lineEdit
		validStatus = ValidStatusType::VS_valid;

	return validStatus;
}


bool FileListViewFilter::operatorValidation( const QString& inputStr, bool bPrevValidationStatus )
{
	bool validOK = bPrevValidationStatus;
	int  numId   = -1;

	if ((numId=inputStr.indexOf(QRegExp("\\d"))) != -1) {
		QString sOperator = inputStr.left(numId);
		//qDebug() << columnTxt << "numId" << numId << "sOperator(1):" << sOperator;
		sOperator.replace(" ", "");
		//qDebug() << "sOperator(2):" << sOperator;
		if (! sOperator.isEmpty()) {
			if (sOperator.length() > 2)
				validOK = false;
			else {
				validOK = false;
				for (int j=0; j<m_compareOperStrLst.count(); ++j) {
					if (sOperator == m_compareOperStrLst[j]) {
						validOK = bPrevValidationStatus;
						break;
					}
				}
			}
		}
	}
	else validOK = false; // not found number

	return validOK;
}

void FileListViewFilter::showToolTipForFilterValue()
{
	QLineEdit *lineEdit    = m_filterComboBox->lineEdit();
	int        curPosition = lineEdit->cursorPosition();
	QString    lineEditTxt = lineEdit->text().left(curPosition);
	int        pipeId      = lineEditTxt.count('|');

	if (pipeId == m_columnPermId) {
		QString sColumnName = m_columnNameStrLst[pipeId];
		int curSubPos = curPosition - (lineEditTxt.lastIndexOf('|') + 1);

		if (curSubPos < 1)
			sColumnName += ": " + tr("Direcory/File");
		else
		if (curSubPos < 4)
			sColumnName += ": " + tr("User");
		else
		if (curSubPos > 3 && curSubPos < 7)
			sColumnName += ": " + tr("Group");
		else
		if (curSubPos > 6 && curSubPos < 11)
			sColumnName += ": " + tr("Other");

		QToolTip::showText(m_treeView->mapToGlobal(QPoint(pos().x(), pos().y()-height()-6)), sColumnName);
	}
	else {
		QToolTip::showText(m_treeView->mapToGlobal(QPoint(pos().x(), pos().y()-height()-6)), m_columnNameStrLst[pipeId]);
	}
}

void FileListViewFilter::changeRegExpParamsNameColInModel()
{
//	qDebug() << "FileListViewFilter::changeRegExpParamsNameColInModel()";
	m_patternSyntax    = m_useRegExpAction->isChecked()            ? QRegExp::RegExp     : QRegExp::Wildcard;
	m_caseSensitivity  = m_caseSensitivityAction->isChecked()      ? Qt::CaseSensitive   : Qt::CaseInsensitive;
	Qt::MatchFlags  mf = m_wildcardStartingWithAction->isChecked() ? Qt::MatchStartsWith : Qt::MatchContains;

	emit signalSetRegExpParametersNameCol(m_patternSyntax, m_caseSensitivity, mf);

	if (m_actionsFilterGroup->checkedAction() == nullptr)
		return;
	QString sToolTipMsg = tr("Checked") +": "+ m_actionsFilterGroup->checkedAction()->text() + ", ";

	sToolTipMsg += (m_caseSensitivityAction->isChecked()) ? tr("Case sensitivity") : tr("Case insensitive");

	m_menuToolButton->setToolTip(sToolTipMsg);

	m_filterWildcardMenu->setDisabled(m_useRegExpAction->isChecked());

	if (m_actionsFilterWildcardGroup->checkedAction() == nullptr)
		return;
	if (m_useWildcardAction->isChecked()) {
		sToolTipMsg += ", ";
		sToolTipMsg += m_actionsFilterWildcardGroup->checkedAction()->text();
	}

	m_menuToolButton->setToolTip(sToolTipMsg);
}

void FileListViewFilter::loadFilters()
{
	qDebug() << "FileListViewFilter::loadFilters";
	m_filterMap.clear();

	FilterItem *fi = new FilterItem;
	fi->m_pattern          = "*";
	fi->m_regExpression    = false;
	fi->m_wildcard         = true;
	fi->m_caseSensitivity  = false;
	fi->m_startWithPattern = false;
	fi->m_containPattern   = true;
	m_filterMap[tr("all")] = fi;

	QMap <QString, bool> filterBoolMap;
	QString sGroup, sFilterName, sKeyName, sPattern;
	Settings settings;
	foreach(sGroup, settings.childGroups()) {
		if (sGroup.startsWith("flv_filter")) {
			sFilterName = sGroup.right(sGroup.length() - sGroup.indexOf(':') - 1);

			settings.beginGroup("flv_filter:"+sFilterName);
			//qDebug() << sFilterName << "keys" << m_pSettings->allKeys();
			filterBoolMap.clear();
			foreach(sKeyName, settings.allKeys()) {
				if (sKeyName == "pattern")
					sPattern = settings.value(sKeyName).toString();
				else
					filterBoolMap[sKeyName] = settings.value(sKeyName, false).toBool();
			}
			settings.endGroup();

			FilterItem *fi = new FilterItem;
			fi->m_pattern          = sPattern;
			fi->m_regExpression    = filterBoolMap["regExpression"];
			fi->m_wildcard         = filterBoolMap["wildcard"];
			fi->m_caseSensitivity  = filterBoolMap["caseSensitivity"];
			fi->m_startWithPattern = filterBoolMap["startWithPattern"];
			fi->m_containPattern   = filterBoolMap["containPattern"];

			m_filterMap[sFilterName] = fi;
			qDebug() << "  -- filter:" << sFilterName << fi->toString();
		}
	}
}


// ---------------------- EVENTs -----------------------


bool FileListViewFilter::eventFilter( QObject *obj, QEvent *event )
{
	QEvent::Type eventType = event->type();

	if (obj == m_filterComboBox && m_filterComboBox->isEditable()) {
		if (eventType == QEvent::KeyRelease) {
			QKeyEvent *keyEvent = static_cast <QKeyEvent *> (event);
			unsigned int key = keyEvent->key();
			unsigned int modifiers = keyEvent->modifiers();
			//qDebug() << "modifiers:" << modifiers << "key:" << key;
			if (key == Qt::Key_Left || key == Qt::Key_Right || key == Qt::Key_Home || key == Qt::Key_End)
				showToolTipForFilterValue();
			else
			if (modifiers != Qt::ControlModifier && modifiers != Qt::MetaModifier &&
				(! keyEvent->text().isEmpty() ||
					(key == Qt::Key_Delete || key == Qt::Key_Backspace || key == Qt::Key_Up || key == Qt::Key_Down || key == Qt::Key_PageUp || key == Qt::Key_PageDown)))
			{
				QString sFilter = m_filterComboBox->lineEdit()->text();
				if (validation(sFilter) == ValidStatusType::VS_valid) {
//					if (key == Qt::Key_Backspace) // test for permission validation error
//						qDebug() << "Key_Backspace and sFilter" << sFilter;
					filterItems(! sFilter.isEmpty());
				}
			}
			return false;
		}
		else
		if (eventType == QEvent::FocusOut) {  // after click in m_menuToolButton is called slotComboCurrentIndexChanged
			return true;
		}
		else
		if (eventType == QEvent::KeyPress) {
			QKeyEvent *keyEvent = static_cast <QKeyEvent *> (event);
			unsigned int key = keyEvent->key();
			if (key == Qt::Key_Enter || key == Qt::Key_Return) {
				slotSetNameForCurrentFilter(); // when enter pressed on editable combo then slotComboCurrentIndexChanged does'nt called
				return true;
			}
			else if (key == Qt::Key_Backtab || key == Qt::Key_Tab) {
				m_treeView->setFocus();
				return true;
			}
			else if (key == Qt::Key_Escape) {
				close();
				return false;
			}
		}
	}
	else if (obj == m_filterComboBox) {
		if (eventType == QEvent::KeyPress) {
			QKeyEvent *keyEvent = static_cast <QKeyEvent *> (event);
			unsigned int key = keyEvent->key();
			if (key == Qt::Key_Escape) {
				close();
				return false;
			}
		}
	}

	return QWidget::eventFilter(obj, event);
}

void FileListViewFilter::closeEvent( QCloseEvent *ce )
{
	m_treeView->setFocus(); // (workaround) restore focus, because it is jumping to second panel
	filterItems(false);
	Q_EMIT signalUpdateStatusBar();
	// TODO: FileListViewFilter::closeEvent - save filters
	// TODO: FileListViewFilter::closeEvent - save filtering configuration

	ce->accept();

	m_treeView->scrollTo(m_treeView->currentIndex(), QAbstractItemView::PositionAtTop);
}


// ---------------------- SLOTs -----------------------


void FileListViewFilter::slotSetNameForCurrentFilter()
{
	QString sFilter = m_filterComboBox->lineEdit()->text();
	if (validation(sFilter) != ValidStatusType::VS_valid)
		return;

	bool bParam;
	QString sFilterName = InputTextDialog::getText(this, InputTextDialog::MAKE_FILTER, m_filterComboBox->currentText(), bParam);
	if (sFilterName.isEmpty()) {
		qDebug() << "FileListViewFilter::slotSetNameForCurrentFilter. Empty name. Do nothing.";
		return;
	}
	FilterItem *pFI = new FilterItem;
	pFI->m_pattern          = m_filterComboBox->currentText();
	pFI->m_regExpression    = m_useRegExpAction->isChecked();
	pFI->m_wildcard         = m_useWildcardAction->isChecked();
	pFI->m_caseSensitivity  = m_caseSensitivityAction->isChecked();
	pFI->m_startWithPattern = m_wildcardStartingWithAction->isChecked();
	pFI->m_containPattern   = m_wildcardContainingAction->isChecked();
	m_filterMap[sFilterName] = pFI;
//	qDebug() << "filter:" << fi->toString();

	// --- save filter to configuration file
	QString sSection = "flv_filter:" + sFilterName + "/";
	Settings settings;
	settings.setValue(sSection + "pattern", pFI->m_pattern);
	settings.setValue(sSection + (pFI->m_regExpression ? "regExpression" : "wildcard"), true);
	settings.setValue(sSection + "caseSensitivity", pFI->m_caseSensitivity);

	if (pFI->m_wildcard) {
		settings.setValue(sSection + "startWithPattern", pFI->m_startWithPattern);
		settings.setValue(sSection + "containPattern", pFI->m_containPattern);
	}

	// -- update toolTips for all added items
	int id = 0;
	QMapIterator<QString, FilterItem *> i(m_filterMap);
	while (i.hasNext()) {
		i.next();
		m_filterComboBox->setItemData(id, i.value()->m_pattern, Qt::ToolTipRole);
		id++;
	}
}

void FileListViewFilter::slotComboEditable( bool bEditable )
{
	if (! bEditable) { // lineEdit will be turned off
// 		disconnect(m_filterComboBox->lineEdit(), &QLineEdit::returnPressed, this, &FileListViewFilter::slotSetNameForCurrentFilter); // returnPressing handled in eventFilter
		m_filterComboBox->setToolTip("");
	}

	m_filterComboBox->setEditable(bEditable);
	m_cleanToolButton->setVisible(bEditable);
	m_menuToolButton->setEnabled(bEditable);

	if (bEditable) { // lineEdit turned on
// 		connect(m_filterComboBox->lineEdit(), &QLineEdit::returnPressed, this, &FileListViewFilter::slotSetNameForCurrentFilter); // returnPressing handled in eventFilter
		m_filterComboBox->setToolTip(m_sRichTxtToolTip);
	}
	int currentIdx = m_filterComboBox->currentIndex();

	// -- reload filters
	int id = 0;
	m_filterComboBox->clear();
	QMapIterator<QString, FilterItem *> i(m_filterMap);
	while (i.hasNext()) {
		i.next();
		//qDebug() << i.key() << ": " << i.value();
		if (! bEditable) {
			m_filterComboBox->addItem(i.key());
			m_filterComboBox->setItemData(id, i.value()->m_pattern, Qt::ToolTipRole);
		}
		else {
			m_filterComboBox->addItem(i.value()->m_pattern);
			m_filterComboBox->setItemData(id, i.key(), Qt::ToolTipRole);
		}
		id++;
	}

	// -- restore filter index and validation
	m_filterComboBox->setCurrentIndex(currentIdx);

	if (bEditable)
		validation(m_filterComboBox->currentText());
	else {
		// set toolTip for current filter
		QMapIterator<QString, FilterItem *> it(m_filterMap);
		while (it.hasNext()) {
			it.next();
			if (m_filterComboBox->currentText() == it.key()) {
				m_filterComboBox->setToolTip(it.value()->m_pattern);
				break;
			}
		}
	}
}

void FileListViewFilter::applyGivenFilter( const QString &sPattern )
{
	qDebug() << "FileListViewFilter::applyGivenFilter. sPattern:" << sPattern;
	if (sPattern.isEmpty()) {
		qDebug() << "FileListViewFilter::applyGivenFilter. WARNING. Passed empty pattern!";
		return;
	}
	if (sPattern == "*") {
		qDebug() << "FileListViewFilter::applyGivenFilter. Remove filter.";
		m_sContextMenuFilter = tr("all");
		FilterItem *fi = m_filterMap[tr("all")];
		QString sFilter = fi->m_pattern;
		m_filterComboBox->setCurrentText(sFilter);
		filterItems(true);
		m_bListFiltered = false;
		return;
	}
	m_patternSyntax   = QRegExp::Wildcard;
	m_caseSensitivity = Qt::CaseInsensitive;

	// -- calculate all semi-automatic defined filters
	int n = 1;
	QMapIterator<QString, FilterItem *> it(m_filterMap);
	while (it.hasNext()) {
		it.next();
		if (it.key().startsWith(tr("FWE_")))
			n++;
	}
	m_sContextMenuFilter = tr("FWE_")+QString("%1").arg(n); // Filter With Extension
	m_filterComboBox->setCurrentText(sPattern);
	// -- default settings of filter
	QMap <QString, bool> filterBoolMap;
	FilterItem *fi = new FilterItem;
	fi->m_pattern          = sPattern; // context menu pattern, i.e.: "*.txt"
	fi->m_regExpression    = false;
	fi->m_wildcard         = true;
	fi->m_caseSensitivity  = false;
	fi->m_startWithPattern = false;
	fi->m_containPattern   = true;
	// setting from config.file
// 	fi->m_regExpression    = filterBoolMap["regExpression"];
// 	fi->m_wildcard         = filterBoolMap["wildcard"];
// 	fi->m_caseSensitivity  = filterBoolMap["caseSensitivity"];
// 	fi->m_startWithPattern = filterBoolMap["startWithPattern"];
// 	fi->m_containPattern   = filterBoolMap["containPattern"];
	m_filterMap[m_sContextMenuFilter] = fi;
	qDebug() << "FileListViewFilter::applyGivenFilter. Filter:" << m_sContextMenuFilter << fi->toString();

	filterItems(true);
}


void FileListViewFilter::slotComboCurrentIndexChanged( const QString &currentText )
{
// 	qDebug() << "FileListViewFilter::slotComboCurrentIndexChanged() currentText:" << currentText << "edit mode:" << m_editToolButton->isChecked();
	QMapIterator<QString, FilterItem *> i(m_filterMap);
	QString sFilter = currentText;
	if (m_editToolButton->isChecked()) { // editable
		if (validation(sFilter) == ValidStatusType::VS_valid) {
			FilterItem *fi = nullptr;
			while (i.hasNext()) {
				i.next();
				if (i.value()->m_pattern == sFilter) {
					fi = i.value();
					break;
				}
			}
			if (fi != nullptr) {
				m_checkBoxAfterToggledProcessing = false;
				m_useRegExpAction->setChecked(fi->m_regExpression);
				m_useWildcardAction->setChecked(fi->m_wildcard);
				m_caseSensitivityAction->setChecked(fi->m_caseSensitivity);
				m_wildcardStartingWithAction->setChecked((fi->m_regExpression) ? false : fi->m_startWithPattern);
				m_wildcardContainingAction->setChecked((fi->m_regExpression) ? false : fi->m_containPattern);
				m_checkBoxAfterToggledProcessing = true;
// 				qDebug() << "filter:" << fi->toString();
			}
		}
	} else { // not editable
		FilterItem *fi = m_filterMap[currentText];
		sFilter = fi->m_pattern;
		if (validation(sFilter) == ValidStatusType::VS_valid) {
			m_checkBoxAfterToggledProcessing = false;
			m_useRegExpAction->setChecked(fi->m_regExpression);
			m_useWildcardAction->setChecked(fi->m_wildcard);
			m_caseSensitivityAction->setChecked(fi->m_caseSensitivity);
			m_wildcardStartingWithAction->setChecked((fi->m_regExpression) ? false : fi->m_startWithPattern);
			m_wildcardContainingAction->setChecked((fi->m_regExpression) ? false : fi->m_containPattern);
			m_checkBoxAfterToggledProcessing = true;
// 			qDebug() << "filter:" << fi->toString();
		}
		// set toolTip for selected filter
		while (i.hasNext()) {
			i.next();
			if (m_filterComboBox->currentText() == i.key()) {
				m_filterComboBox->setToolTip(i.value()->m_pattern);
				break;
			}
		}
	}
	changeRegExpParamsNameColInModel();
	filterItems(! sFilter.isEmpty());
}

void FileListViewFilter::slotCleanFilter()
{
	if (m_filterComboBox->isEditable()) {
		m_filterComboBox->lineEdit()->clear();
		filterItems(false);
		setValidationStatusAsBgColor(ValidStatusType::VS_none);
	}
}

void FileListViewFilter::slotToggledInputType( bool bToggled )
{
	if (! m_checkBoxAfterToggledProcessing)
		return;
//	qDebug() << "FileListViewFilter::slotToggledInputType";

	QString senderName = sender()->objectName();
//	QPoint  toolTipPos = m_treeView->mapToGlobal(QPoint(pos().x(), pos().y()-height()-6));

	if (senderName == "UseRegExpAction" && bToggled) {
		m_caseSensitivityAction->setChecked(true);
//		QToolTip::showText(toolTipPos, tr("Use regular expression and 'Case sensitivity'"));
	}
	else
	if (senderName == "UseWildcardAction" && bToggled) {
		m_wildcardContainingAction->setChecked(true);
//		m_caseSensitivityAction->setChecked(false);
//		QToolTip::showText(toolTipPos, tr("Use Wildcard"));
	}

	if (m_useRegExpAction->isChecked()) {
		if (senderName == "WildcardStartingWith" && bToggled) {
			m_wildcardStartingWithAction->setChecked(false);
		}
		if (senderName == "WildcardContaining" && bToggled) {
			m_wildcardContainingAction->setChecked(false);
		}
	}

	// --- sets caseSensitivity, syntaxPattern and wildcardMatch for the proxyModel used to filtering and sets toolTip for toolButton
	changeRegExpParamsNameColInModel();

	// --- update view after parameter changed
	QString sFilter = m_filterComboBox->lineEdit()->text();
	if (validation(sFilter) == ValidStatusType::VS_valid) {
		filterItems(! sFilter.isEmpty());
	}
}

