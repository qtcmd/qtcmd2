 /***************************************************************************
 *   Copyright (C) 2005 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDebug>
#include <QMenu>
#include <QRegExp>
#include <QKeyEvent>
#include <QTreeView>
#include <QDateTime>
#include <QCheckBox>
#include <QLineEdit>
#include <QTabWidget>
#include <QCompleter>
#include <QCloseEvent>
#include <QFileDialog>
#include <QFileSystemModel>

#include "settings.h"
#include "messagebox.h"
#include "findfiledialog.h"
#include "systeminfohelper.h" // for getOwners()


FindFileDialog::FindFileDialog( QWidget *pParent )
	: QWidget(pParent)
{
	m_pDialog = new QDialog(pParent);
	setupUi(m_pDialog);

	m_bFindPause = false;
	m_StartPathInSearch = START_UNKNOWN;

	m_pListViewOfFound->setParent(this); // needed when child (m_pFoundFilesListView) calls method from parent (this)

	connect(m_inputNameComboBox->lineEdit(),     &QLineEdit::returnPressed, this, &FindFileDialog::slotStartFinding);
	connect(m_inputLocationComboBox->lineEdit(), &QLineEdit::returnPressed, this, &FindFileDialog::slotStartFinding);

	connect(m_pRespectTimeChkBox,    &QCheckBox::toggled, this, &FindFileDialog::slotEnableTimeRange);
	connect(m_pFileSizeChkBox,       &QCheckBox::toggled, this, &FindFileDialog::slotEnableFileSizeParam);
	connect(m_pSelectDirLocationBtn, &QCheckBox::clicked, this, &FindFileDialog::slotSelectDirLocation);

	connect(m_pFindBtn,       &QPushButton::clicked, this, &FindFileDialog::slotStartFinding);
	connect(m_pStopBtn,       &QPushButton::clicked, this, &FindFileDialog::slotFindStop);
	connect(m_pPauseBtn,      &QPushButton::clicked, this, &FindFileDialog::slotPause);
	connect(m_pClearListBtn,  &QPushButton::clicked, this, &FindFileDialog::slotClearList);
	connect(m_pPutInPanelBtn, &QPushButton::clicked, this, &FindFileDialog::slotPutInPanel);
	connect(m_pListViewOfFound, &FindFileListView::doubleClicked, this, &FindFileDialog::slotJumpToFile);

	m_pCloseBtn->setShortcut(Qt::Key_Escape);
	m_pDialog->installEventFilter(this); // due to: setupUi(m_pDialog) there is need to handle event by event filter
}

FindFileDialog::~FindFileDialog()
{
	delete m_pDialog;
}

void FindFileDialog::init( const URI &uri, FileInfoList *pFoundFilesList, KeyShortcuts *pKS )
{
	setWindowTitle( tr("Find file(s)")+" - QtCommander" );

	m_pListViewOfFound->initListView(pFoundFilesList, pKS);

	Settings settings;
	// -- init first Tab
	m_inputNameComboBox->clear();
	QString sInputNameList = settings.value("vfs/FindName").toString();
	if (! sInputNameList.isEmpty()) {
		m_initNameList = sInputNameList.split("||");
		m_inputNameComboBox->addItems(m_initNameList);
	}
	m_inInFindNameLastId = m_inputNameComboBox->currentIndex();
	int nFindNameLastId = settings.value("vfs/FindNameLastId").toInt();
	if (nFindNameLastId < 0 || nFindNameLastId > m_initNameList.size()-1)
		nFindNameLastId = 0;
	bool bFindNameCaseSensitive = settings.value("vfs/FindNameCaseSensitive", false).toBool();

	m_inputLocationComboBox->clear();
	QString sInputLocationList = settings.value("vfs/FindLocation").toString();
	if (! sInputLocationList.isEmpty()) {
		m_initLocationList = sInputLocationList.split('|');
		m_inputLocationComboBox->addItems(m_initLocationList);
	}
	// ---- init start path for searching
	QString sStartPathInSeach = settings.value("vfs/StartPathInFindFiles", "current").toString();
	if (sStartPathInSeach == "current")
		m_StartPathInSearch = START_CURRENT;
	else
	if (sStartPathInSeach == "last")
		m_StartPathInSearch = START_LAST;
	if (m_StartPathInSearch == START_UNKNOWN) // use default
		m_StartPathInSearch = START_CURRENT;

// 	qDebug() << "FindFileDialog::init. uri.path:" << uri.path();
// 	m_bUseCurrentPathAsStart = settings.value("vfs/UsePathAsStartInSearch", "last").toBool();
	if (m_StartPathInSearch == START_LAST) {
		int nIdOfLastUsedPath = settings.value("vfs/IdLastUsedPathInSearch", -1).toInt();
		if (nIdOfLastUsedPath == -1)
			m_StartPathInSearch = START_CURRENT;
		else { // START_LAST
			QString sStartPath = m_inputLocationComboBox->itemText(nIdOfLastUsedPath);
			if (sStartPath.isEmpty())
				qDebug() << "FindFileDialog::init. Internal error. Cannot find path with given Id:" << nIdOfLastUsedPath;
			else  {
				m_inputLocationComboBox->setCurrentIndex(nIdOfLastUsedPath);
				m_inFindLocationLastId = nIdOfLastUsedPath;
			}
		}
	}
	else
	if (m_StartPathInSearch == START_CURRENT) {
		int idx = -1;
		// check if current path is present on loaded list of paths
		if ((idx=m_inputLocationComboBox->findText(uri.path())) == -1) { // path not found, just add it
			m_inputLocationComboBox->addItem(uri.path());
			idx = m_inputLocationComboBox->findText(uri.path());
		}
		else { // found path
			m_inFindLocationLastId = idx;
		}
		m_inputLocationComboBox->setCurrentIndex(idx);
	}
/*
	if (! m_bUseCurrentPathAsStart) {
		int nFindLocationLastId = settings.value("vfs/FindLocationLastId").toInt();
		if (nFindLocationLastId < 0 || nFindLocationLastId > m_initLocationList.size()-1)
			nFindLocationLastId = idx;
		m_inFindLocationLastId = nFindLocationLastId;
	}
	m_inputLocationComboBox->setCurrentIndex((m_bUseCurrentPathAsStart && idx != -1) ? idx : m_inFindLocationLastId);
// 	m_inputLocationComboBox->lineEdit()->setText(uri.path());
*/
 	m_pNameCaseSensitiveChkBox->setChecked(bFindNameCaseSensitive);
	setTabOrder(m_inputNameComboBox, m_inputLocationComboBox);
	m_inputNameComboBox->setCurrentIndex(nFindNameLastId);
	m_inputNameComboBox->setFocus();

	QString sCurrentPath = m_inputLocationComboBox->currentText();
	if (sCurrentPath.startsWith('/')) {
		QCompleter *completer = new QCompleter(this);
		completer->setMaxVisibleItems(15);
		// Unsorted QFileSystemModel
		QFileSystemModel *fsModel = new QFileSystemModel(completer);
		fsModel->setRootPath(sCurrentPath);
		fsModel->setFilter(QDir::AllDirs);
		completer->setModel(fsModel);
		m_inputLocationComboBox->setCompleter(completer);
	}

	// --- second Tab // NOT YET SUPPORTED
	m_pTabWidget->widget(1)->setEnabled(false);
/*	QString sContainsNameList = settings->value("vfs/FindTextInFile").toString();
	if (! sContainsNameList.isEmpty()) {
		QStringList slContainsNameStrLst = sContainsNameList.split('|');
		m_inputLocationComboBox->addItems(slContainsNameStrLst);
	}
	bool bContainsNameCaseSensitive = settings->value("vfs/ContainsNameCaseSensitive", false).toBool();
	m_pTextCaseSensitiveChkBox->setChecked(bContainsNameCaseSensitive);
	bool bIncludeBinaryFile = settings->value("vfs/IncludeBinaryFile", false).toBool();
	m_pIncludeBinaryChkBox->setChecked(bIncludeBinaryFile);
	bool bContainsRegExpr = settings->value("vfs/ContainsRegExpr", false).toBool();
	m_pTextRegExpChkBox->setChecked(bContainsRegExpr);
*/
	// --- init third Tab
	m_pFirstDateTimeEdit->setDateTime(QDateTime::currentDateTime());
	m_pSecondDateTimeEdit->setDateTime(QDateTime::currentDateTime());

// 	QStringList slAllUsersGroupList = ;
	m_pOwnerComboBox->addItems(getAllUsers());
	m_pGroupComboBox->addItems(getAllGroups());

	// --- init forth Tab
	m_pFindRecursiveChkBox->setChecked(true);

	m_pSearchInSelectedChkBox->setEnabled(false); // NOT YET SUPPORTED
	m_pClearListBtn->setEnabled(false);
	m_pPutInPanelBtn->setEnabled(false);

	slotSearchingFinished();
}

void FindFileDialog::saveSettings()
{
	// --- save names and locations to the config file
	QString sName, sNameList;
	Settings settings;

// 	m_inputNameComboBox->saveSettings();
	if (m_initNameList != comboStrList(m_inputNameComboBox) && m_inputNameComboBox->count() > 0 && ! m_inputNameComboBox->lineEdit()->text().isEmpty()) {
		foreach(sName, comboStrList(m_inputNameComboBox)) {
			if (! sName.isEmpty())
				sNameList += sName + "||";
		}
		sNameList.remove(sNameList.length()-1, 2);
		settings.setValue("vfs/FindName", sNameList);
	}
	if (m_inInFindNameLastId != m_inputNameComboBox->currentIndex() && ! m_inputNameComboBox->lineEdit()->text().isEmpty()) {
		settings.setValue("vfs/FindNameLastId", m_inputNameComboBox->currentIndex());
	}
	// 	m_inputLocationComboBox->saveSettings(); // save list of entries
	if (m_initNameList != comboStrList(m_inputLocationComboBox) && m_inputLocationComboBox->count() > 0) {
		sNameList = "";
		foreach(sName, comboStrList(m_inputLocationComboBox)) {
			if (! sName.isEmpty())
				sNameList += sName + "|";
		}
		sNameList.remove(sNameList.length()-1, 1);
		settings.setValue("vfs/FindLocation", sNameList);
	}
	if (m_StartPathInSearch == START_LAST)
		settings.setValue("vfs/IdLastUsedPathInSearch", m_inputLocationComboBox->currentIndex());
}


QStringList & FindFileDialog::comboStrList( QComboBox *pComboBox )
{
	m_strList.clear();
	if (pComboBox == nullptr)
		return m_strList;

	QString sTemplate;
	for (int i=0; i<pComboBox->count(); i++) {
		sTemplate = pComboBox->itemText(i);
		while (sTemplate.endsWith('|')) // clean template with one or more | characters
			sTemplate.remove(sTemplate.length()-1, 1);
		m_strList.append(sTemplate);
	}
	return m_strList;
}


void FindFileDialog::slotSearchingFinished()
{
	m_bFindPause = false;

	m_pFindBtn->setEnabled(true);
	m_pStopBtn->setEnabled(false);
	m_pPauseBtn->setEnabled(false);

	for (int i=0; i<4; i++) {
		if (i != 1) // second tab not yet supported
			m_pTabWidget->widget(i)->setEnabled(true);
	}
	m_pTabWidget->setCurrentIndex(0);

	m_pStatusInfoLab->setText(tr("Ready."));
// 	m_pFoundFilesListView->setFirstAsCurrent();

	m_pClearListBtn->setEnabled( (m_pListViewOfFound->rowCount() > 0) );
	m_pPutInPanelBtn->setEnabled( (m_pListViewOfFound->rowCount() > 0) );
}


void FindFileDialog::updateSearchingStatus( const QString &sMatched, const QString &sFiles, const QString &sDirectories )
{
// 	m_pStatusDetailsLab->setText(QString(tr("Matched: %1 from %2 files and %3 directories")).arg(nMatches).arg(nFiles).arg(nDirectories));
	m_pStatusDetailsLab->setText(QString(tr("Matched: %1 from %2 files and %3 directories")).arg(sMatched).arg(sFiles).arg(sDirectories));
}

void FindFileDialog::slotStartFinding()
{
	// --- check and update the comboBoxes
	QString sCurrentComboText = m_inputNameComboBox->lineEdit()->text();
	if (sCurrentComboText.isEmpty()) {
		m_pTabWidget->setCurrentIndex(0);
		MessageBox::msg(nullptr, MessageBox::CRITICAL, tr("Not given file name for searching!"));
		m_inputNameComboBox->setFocus();
		return;
	}
	else {
		if (m_inputNameComboBox->findText(sCurrentComboText) < 0) {
			m_inputNameComboBox->insertItem(0, sCurrentComboText);
		}
	}
	m_inputNameComboBox->setCurrentIndex(m_inputNameComboBox->findText(sCurrentComboText));
	sCurrentComboText = m_inputLocationComboBox->lineEdit()->text();
	if (sCurrentComboText.isEmpty()) {
		m_pTabWidget->setCurrentIndex(0);
		MessageBox::msg(nullptr, MessageBox::CRITICAL, tr("Not given start path for searching!"));
		m_inputLocationComboBox->setFocus();
		return;
	}
	else {
		// simple path validation
		if (sCurrentComboText.contains(QRegExp(".*~/")))
			sCurrentComboText.replace(QRegExp(".*~/"), QDir::homePath()+"/");
		if (sCurrentComboText.contains(QRegExp("/[\\.\\.]+/")))
			sCurrentComboText.replace(QRegExp("/[\\.\\.]+/"), "/");
		if (sCurrentComboText.contains(QRegExp("[/]+")))
			sCurrentComboText.replace(QRegExp("[/]+"), "/");

		m_inputLocationComboBox->setCurrentText(sCurrentComboText);
		if (m_inputLocationComboBox->findText(sCurrentComboText) < 0)
			m_inputLocationComboBox->insertItem(0, sCurrentComboText);
		if (! QDir(sCurrentComboText).exists()) {
			MessageBox::msg(nullptr, MessageBox::CRITICAL, tr("Given path doesn't exists!!"));
			return;
		}
		m_inputLocationComboBox->setCurrentText(sCurrentComboText);
	}

	// --- enables and disables buttons and tabs
	m_pStopBtn->setEnabled(true);
	m_pPauseBtn->setEnabled(true);
	m_pFindBtn->setEnabled(false);
	m_pPutInPanelBtn->setEnabled(false);
// 	m_pTabWidget->setEnabled(false); // if searching files contents will be supported then can to uncomment it
	for (uint i=0; i<4; i++)
		m_pTabWidget->widget(i)->setEnabled(false);

	// ---initialization find criterion obj.
	m_FindCriterion.nameLst           = m_inputNameComboBox->lineEdit()->text();
	m_FindCriterion.location          = m_inputLocationComboBox->lineEdit()->text();
	m_FindCriterion.caseSensitivity   = m_pNameCaseSensitiveChkBox->isChecked() ? Qt::CaseSensitive : Qt::CaseInsensitive;
	m_FindCriterion.patternSyntax     = m_pNameRegularExpression->isChecked() ? QRegExp::RegExp : QRegExp::Wildcard;
	m_FindCriterion.containText       = m_pContainsComboBox->lineEdit()->text();
	m_FindCriterion.caseSensitiveText = m_pTextCaseSensitiveChkBox->isChecked();
	m_FindCriterion.includeBinFiles   = m_pIncludeBinaryChkBox->isChecked();
	m_FindCriterion.regExpText        = m_pTextRegExpChkBox->isChecked();
	m_FindCriterion.checkTime         = m_pRespectTimeChkBox->isChecked();
	m_FindCriterion.firstTime         = m_pFirstDateTimeEdit->dateTime();
	m_FindCriterion.secondTime        = m_pSecondDateTimeEdit->dateTime();
	m_FindCriterion.checkFileSize     = (m_pFileSizeChkBox->isChecked()) ? (FindCriterion::CheckFileSize)m_pFileSizeQualifiersComboBox->currentIndex() : FindCriterion::NONE;

	qint64 nFileSize = (m_FindCriterion.checkFileSize!=FindCriterion::NONE) ? m_pFileSizeSpinBox->value() : -1;
	if (nFileSize >= 0) {
		uint suffixesId = m_pFileSizeSuffixesComboBox->currentIndex();
		if (suffixesId == 1)
			nFileSize *= 1024; // for KB
		if (suffixesId == 2)
			nFileSize *= 1073741824; // for MB
		if (suffixesId == 3) {
			nFileSize *= 1073741824; // for GB
			nFileSize *= 1024;
		}
		m_FindCriterion.fileSize = nFileSize;
	}

	int nUID = (int) getUserId(m_pOwnerComboBox->currentText());
	int nGID = (int) getGroupId(m_pGroupComboBox->currentText());
// 	QStringList slList = QStringList() << m_pGroupComboBox->currentText(); // get Id for current (in combo) user name
// 	int nGID = (int) getCurrentUserGroupId(); // get Id for current (in combo) group name

	m_FindCriterion.checkFileOwner     = m_pFileOwnerChkBox->isChecked();
	m_FindCriterion.checkFileGroup     = m_pFileGroupChkBox->isChecked();
	m_FindCriterion.ownerId            = (m_FindCriterion.checkFileOwner) ? nUID : -1;
	m_FindCriterion.groupId            = (m_FindCriterion.checkFileGroup) ? nGID : -1;
	m_FindCriterion.owner              = (m_FindCriterion.checkFileOwner) ? m_pOwnerComboBox->currentText() : "";
	m_FindCriterion.group              = (m_FindCriterion.checkFileGroup) ? m_pGroupComboBox->currentText() : "";
	m_FindCriterion.filesType          = (FindCriterion::FilesType)m_pFiltersComboBox->currentIndex();
	m_FindCriterion.findRecursive      = m_pFindRecursiveChkBox->isChecked();
	m_FindCriterion.searchIntoSelected = m_pSearchInSelectedChkBox->isChecked();
	m_FindCriterion.stopIfXMatched     = m_pStopMatchesChkBox->isChecked();
	m_FindCriterion.stopAfterXMaches   = (m_FindCriterion.stopIfXMatched) ? m_pMatchesNumSpinBox->value() : -1;

	// --- make an destination action
	m_pStatusInfoLab->setText(tr("Searching")+"...");
	slotClearList();
	Q_EMIT FindFileDialog::signalStartFind(m_FindCriterion);
	m_pListViewOfFound->setFocus();
}

void FindFileDialog::slotFindStop()
{
	slotSearchingFinished();
	Q_EMIT FindFileDialog::signalStop();
}

void FindFileDialog::slotPause()
{
	m_bFindPause = ! m_bFindPause;

	m_pStatusInfoLab->setText(m_bFindPause ? tr("Pause.") : tr("Searching")+"...");
	m_pPauseBtn->setText(m_bFindPause ? tr("Co&ntinue") : tr("&Pause"));

	m_pPutInPanelBtn->setEnabled(m_bFindPause);
	m_pClearListBtn->setEnabled(m_bFindPause);

	for (int i=0; i<4; i++) {
		if (i != 1) // second tab not yet supported
			m_pTabWidget->widget(i)->setEnabled(m_bFindPause);
	}
	Q_EMIT FindFileDialog::signalPause(m_bFindPause);
}

void FindFileDialog::slotClearList()
{
	m_pStatusDetailsLab->clear();
	m_pListViewOfFound->clear();
	m_pClearListBtn->setEnabled(false);
	m_pPutInPanelBtn->setEnabled(false);
}

void FindFileDialog::slotPutInPanel()
{
	hide();
	saveSettings();
	Q_EMIT FindFileDialog::signalPutInPanel();
}

void FindFileDialog::slotEnableTimeRange( bool bEnable )
{
	betweenLab->setEnabled(bEnable);
	m_pFirstDateTimeEdit->setEnabled(bEnable);
	andLab->setEnabled(bEnable);
	m_pSecondDateTimeEdit->setEnabled(bEnable);
}

void FindFileDialog::slotEnableFileSizeParam( bool bEnable )
{
	m_pFileSizeGrpBox->setEnabled(bEnable);
}

void FindFileDialog::slotSelectDirLocation()
{
	QString sDirName = QFileDialog::getExistingDirectory(this, tr("Select start path for finding")+ " - QtCommander", QDir::rootPath());
	if (! sDirName.isEmpty()) {
		m_inputLocationComboBox->addItem(sDirName);
		m_inputLocationComboBox->setCurrentIndex(m_inputLocationComboBox->findText(sDirName));
	}
}

void FindFileDialog::slotJumpToFile( const QModelIndex & )
{
	if (! m_pFindBtn->isEnabled() || m_pListViewOfFound->rowCount() == 0)
		return;

	emit signalJumpToFile(m_pListViewOfFound->currentFileInfo());
}

void FindFileDialog::slotRemoveFile()
{
	if (! m_pFindBtn->isEnabled() || m_pListViewOfFound->rowCount() == 0)
		return;

	emit signalRemoveFile(m_pListViewOfFound->currentFileInfo(), nullptr);
}

void FindFileDialog::slotViewFile( bool bEditMode )
{
	if (! m_pFindBtn->isEnabled() || m_pListViewOfFound->rowCount() == 0)
		return;

	emit signalViewFile(m_pListViewOfFound->currentFileInfo(), bEditMode);
}


void FindFileDialog::closeEvent( QCloseEvent *pCE )
{
	slotFindStop();
	saveSettings();
	// --- close current dialog
	emit signalClose();
	pCE->ignore();
}

bool FindFileDialog::eventFilter( QObject *pObj, QEvent *pEvent )
{
	if (pEvent->type() == QEvent::Close) {
		QCloseEvent *pCloseEvent = static_cast<QCloseEvent *>(pEvent);
		closeEvent(pCloseEvent);
		return true;
	}
	return QObject::eventFilter(pObj, pEvent);
}

