/***************************************************************************
 *   Copyright (C) 2005 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _FINDFILEDIALOG_H
#define _FINDFILEDIALOG_H

#include <QDialog>
#include <QKeyEvent>
#include <QCloseEvent>

#include "ui_findfiledlg.h"

#include "findcriterion.h"
#include "keyshortcuts.h"
#include "uri.h"

/**
	@author Piotr Mierzwiński
*/
class FindFileDialog : public QWidget, public Ui::FindFileDlg
{
	Q_OBJECT
public:
	FindFileDialog( QWidget *pParent );
	~FindFileDialog();

	/** Initialize FindFiles dialog.
	 * @param uri current url in tab which calling FindFiles dialog.
	 * @param pFoundFilesList pointer to container holding found files
	 * @param pKS pointer to container holding key shortcuts (helped in navigating on list of found)
	 */
	void init( const URI &uri, FileInfoList *pFoundFilesList, KeyShortcuts *pKS );

	/** Shows this dialog.
	 */
	void show() { m_pDialog->show(); }

	/** Hides this dialog.
	 */
	void hide() { m_pDialog->hide(); }

	void updateSearchingStatus( const QString &sMatched, const QString &sFiles, const QString &sDirectories );

	void addMatchedItem( FileInfo *pFileInfo ) {
		m_pListViewOfFound->addMatchedItem(pFileInfo);
	}

	void getFoundFilesList( FileInfoList *pFoundFilesList ) {
        m_pListViewOfFound->getFoundFilesList(pFoundFilesList); }

private:
	QDialog *m_pDialog;
	FindCriterion m_FindCriterion;

	bool m_bFindPause;

	QStringList m_strList, m_initNameList, m_initLocationList;
	int m_inInFindNameLastId, m_inFindLocationLastId;
	//bool m_bUseCurrentPathAsStart, m_bUseLastUsedPathAsStart;

	enum {
		START_UNKNOWN = 0,
		START_CURRENT,
		START_LAST
	} StartPathInSearch;
	int m_StartPathInSearch;

	/** Returns strings list from the PathView for given QComboBox object.
	 * @return pathes list
	 */
	QStringList & comboStrList( QComboBox *pComboBox );

	void saveSettings();

private slots:
	void slotStartFinding();
	void slotFindStop();
	void slotPause();

	void slotClearList();

	void slotPutInPanel();
	void slotEnableTimeRange( bool bEnable );
	void slotEnableFileSizeParam( bool bEnable );
	void slotSelectDirLocation();

public slots:
	void slotSearchingFinished();
	void slotJumpToFile( const QModelIndex & );
	void slotRemoveFile();
	void slotViewFile( bool bEditMode );
	void slotUpdateRemovedFindFileDialog( Vfs::FileInfo *pFileInfo ) {
        m_pListViewOfFound->updateRemoved(pFileInfo);
	}

protected:
	void closeEvent( QCloseEvent * pCE );
	bool eventFilter( QObject *pObj, QEvent *pEvent );

signals:
	void signalClose();
	void signalStop();
	void signalPause( bool bPause );
	void signalJumpToFile( Vfs::FileInfo *pFileInfo );
	void signalRemoveFile( Vfs::FileInfo *pFileInfo, QWidget *pParent );
	void signalViewFile( Vfs::FileInfo *pFileInfo, bool bEditMode );
	void signalStartFind( const FindCriterion & );
	void signalPutInPanel();

};

#endif // _FINDFILEDIALOG_H
