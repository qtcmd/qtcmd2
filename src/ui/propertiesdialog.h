/***************************************************************************
 *   Copyright (C) 2005 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _PROPERTIESDIALOG_H
#define _PROPERTIESDIALOG_H

#include <QDialog>
#include <QCloseEvent>
#include <QButtonGroup>

#include "ui_propertiesdlg.h"

#include "uri.h"
#include "fileinfo.h"
#include "fileattributes.h"

using namespace Vfs;

/**
	@author Piotr Mierzwiński
*/
class PropertiesDialog : public QWidget, Ui::PropertiesDlg
{
	Q_OBJECT
public:
	PropertiesDialog( QWidget *pParent );
	~PropertiesDialog();

	void init( FileInfoToRowMap *pSelectionMap, const URI &uri, bool bReadOnly );
	void show() { m_pDialog->show(); }
	void hide() { m_pDialog->hide(); }

	bool readOnlyMode() const { return m_bForbiddenChanges; }

	/**
	 * @brief Returns status the attributes to change.
	 * @return true if any attributes should be changed, otherwise false
	 */
	bool changeTheAttributes();

	FileAttributes *changedAtrributes() { return m_changedAtrributes; }

private:
	QDialog *m_pDialog;
	QWidget *m_pParent;

	void enableButtonGroup( QButtonGroup *pButtonGroup );
	void setDescrPermissions();

	/**
	 * @brief Returns changed permissions.
	 * @return permissions (decimal value)
	 */
	int permissions() const;

	enum ChangedAttib { PERMISSIONS=0, OWNER_AND_GROUP, ACCESS_MODFIED_TIME, FILE_NAME };

	/**
	 * @brief Returns type of given change
	 * @param changedAttrib type of change, @see ChangedAttib
	 * @return index of changed type, @see ChangesFor
	 */
	FileAttributes::ChangesFor changesFor( ChangedAttib changedAttrib );

private:
	bool m_bForbiddenChanges;
	QString m_sLoggedUserName, m_sAllSelectedItem;
	QMap <FileInfo *, QString> m_itemsMap;
	bool m_bOKbuttonCloseWnd;

	FileInfoToRowMap *m_selectionMap;
	FileInfo *m_selectedFI;
	URI m_URI;

	FileAttributes *m_changedAtrributes;


	enum Permmissions { USER_PERM=0, GROUP_PERM, OTHER_PERM };

	QButtonGroup *m_pClassButtonGroup;
	QButtonGroup *m_pReadButtonGroup, *m_pWriteButtonGroup, *m_pExecButtonGroup, *m_pSpecialButtonGroup;

	QStringList m_slInputNames;

	bool m_bSelectedAtLeastOneDir;
	bool m_bArchiveSelected;

	bool n_bReadForAll, m_bWriteForAll, m_bExecForAll;

private slots:
	void slotStopWeighing();
	void slotStartWeighing();

	void slotOkPressed();
	void slotActivatedName( const QString &sFullName );
	void slotCheckGroupButton( int nButtonId );

	void slotItemActivatedChangesForComboBox( int index );
	void slotToggledChangesForChkBox( bool state );

	void slotInitAllTabs( const QString &sInpFileName );
	void slotInitAdvPermissions();
	void slotInitAdvOwnerAndGroup();
	void slotInitAdvAccessAndModifiedTime();
	void slotInitPermissonsForClass( int nPermClass );

	void slotToggledUsesExtView( bool bState );

	void slotCheckUncheckReadForAll();
	void slotCheckUncheckWriteForAll();
	void slotCheckUncheckExecForAll();


public slots:
	void slotSetWeighingResult( qint64 nWeight, qint64 nRealWeight, uint nFiles, uint nDirs );
	void slotShowErrorInfoForWeighing( bool errorOccured ) { m_pErrorDuringWeighing->setVisible(errorOccured); m_pRefreshBtn->setEnabled(true); }

protected:
	void closeEvent( QCloseEvent *e );
	bool eventFilter( QObject *pObj, QEvent *pEvent );

signals:
	void signalClose();
	void signalStartWeighing( FileInfo *pFileInfo, bool bStartWeigh );
	void signalOkPressed( FileInfo *pFileInfo, FileAttributes *pChangedAttrib, bool bChangesForAllSelected );

};

#endif // _PROPERTIESDIALOG_H
