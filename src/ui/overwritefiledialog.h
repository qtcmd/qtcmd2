/***************************************************************************
 *   Copyright (C) 2005 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _OVERWRITETHEFILEDIALOG_H
#define _OVERWRITETHEFILEDIALOG_H

#include <QButtonGroup>

#include "fileinfo.h"
#include "ui_overwritefiledlg.h"

/**
	@author Piotr Mierzwiński
*/
class OverwriteFileDialog : public QDialog, Ui::OverwriteFileDlg
{
//	Q_OBJECT
public:
	OverwriteFileDialog();

	static int show( const Vfs::FileInfo &targetFleInfo, const Vfs::FileInfo &sourceFleInfo );

	enum FileOverwriteBtns { Yes=1, No, DiffSize, Rename, All, Update, None, Cancel };

private:
	QButtonGroup *m_pButtonGroup;

};

#endif // _OVERWRITETHEFILEDIALOG_H
