/***************************************************************************
 *   Copyright (C) 2017 by Piotr Mierzwi?ski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QTextBrowser>
#include <QApplication>
#include <QPushButton>
#include <QLocale>
#include <QDebug>
#include <QFile>

#include "helpdialog.h"


HelpDialog::HelpDialog()
{
	setupUi(this);
	setWindowTitle(tr("Help")+" - QtCommander 2");

	connect(m_pTextBrowser, &QTextBrowser::anchorClicked, this, &HelpDialog::slotAnchorClicked);
}

void HelpDialog::show( QWidget *pParent, IconCache *pIconCache )
{
	HelpDialog *pDlg = new HelpDialog;
	Q_CHECK_PTR( pDlg );

	pDlg->setParent(pParent);
	pDlg->m_pIconCache = pIconCache;
	pDlg->setWindowFlags(Qt::Dialog);

 	pDlg->m_pBackButton->setIcon(pDlg->icon("go/previous"));
 	pDlg->m_pForwardButton->setIcon(pDlg->icon("go/next"));
 	pDlg->m_pHomeButton->setIcon(pDlg->icon("go/home"));


	QString sLang = QLocale().name().left(2); // pl, en, ru, etc.
	QString sHelpPath =  qApp->applicationDirPath() + "/";
	QString sGlobalLocation = "../share/doc/qtcmd2/";
	QString sDevLocation    = "../doc/";

	if (QFile::exists(sHelpPath + sGlobalLocation + sLang))
		sHelpPath += sGlobalLocation;
	else
	if (QFile::exists(sHelpPath + sDevLocation + sLang))
		sHelpPath += sDevLocation;
	else {
		qDebug() << "HelpDialog::show."
				 << QString(tr("Cannot found documentation directory for local langauge '%1'. Following locations doesn't exist:")).arg(sLang)
				 << "\n\t" << sHelpPath+sGlobalLocation+sLang << "\n\t" << sHelpPath+sDevLocation+sLang
				 << "Try to use language: \"en\"";
		// -- check if exists documentation for "en" langauge
		if (QFile::exists(sHelpPath + sGlobalLocation + "en") || QFile::exists(sHelpPath + sDevLocation + "en"))
			sLang = "en";
		else {
			qDebug() << "HelpDialog::show."
					 << QString(tr("Cannot found documentation directory for local langauge '%1'. Following locations don't  exist:")).arg(sLang)
					 << "\n\t" << sHelpPath+sGlobalLocation+"en" << "\n\t" << sHelpPath+sDevLocation+"en";
			return;
		}
	}
	sHelpPath += sLang;
	pDlg->m_sHelpPath = sHelpPath + "/";

	//qDebug("HelpDialog::show. sHelpPath=%s locale:%s", sHelpPath.toLatin1().data(), sLang.toLatin1().data());

	if (QFile::exists(sHelpPath +"/index.html"))
		pDlg->m_pTextBrowser->setSource(QUrl( sHelpPath +"/index.html" ));
	else
		qDebug("HelpDialog::loadFile. Cannot found file %s/index.html", sHelpPath.toLatin1().data());

	pDlg->resize(pParent->width()*0.66, pParent->height()-24);
	pDlg->exec(); // show dialog
	delete pDlg;  pDlg = 0;
}

void HelpDialog::slotAnchorClicked( const QUrl &link )
{
	QString sNewLink = link.toString();
	if (link.toString().contains("#")) {
		sNewLink.remove(QRegExp("#.*"));
		// thanks that work only link making back to top of document, all others inside document don't work
		m_pTextBrowser->setSource(QUrl(sNewLink));
	}
	else
		m_pTextBrowser->setSource(QUrl(m_sHelpPath + sNewLink));
}

