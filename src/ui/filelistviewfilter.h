/***************************************************************************
 *   Copyright (C) 2012 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _FILELISTVIEWFILTER_H
#define _FILELISTVIEWFILTER_H

#include "ui_filelistviewfilterdlg.h"
#include "vfsflatlistproxymodel.h"
#include "vfsflatlistmodel.h"
#include "inputtextdialog.h"
#include "treeview.h"

#include <QMenu>
#include <QTimer>

class QKeyEvent;
class TreeView;

/*
namespace ValidStatusType {
	enum ValidStatusEnum { VS_none=0, VS_valid, VS_notValid };
}
typedef ValidStatusType::ValidStatusEnum ValidStatus; */


class FilterItem
{
public:
	FilterItem();

	QString m_pattern;
	bool    m_regExpression;
	bool    m_wildcard;
	bool    m_caseSensitivity;
	bool    m_startWithPattern;
	bool    m_containPattern;

	QString toString();

};

/**
	@author Piotr Mierzwiński <piom@nes.pl>
 */
class FileListViewFilter : public QWidget, Ui::FileListViewFilterDlg
{
	Q_OBJECT
public:
	FileListViewFilter( TreeView *pTreeView, QWidget *pParent );
	~FileListViewFilter();

	/** Inits dialog.
	 * Set focus on the QComboBox object, toggled it to editable and clean lineEdit into QComboBox.
	 */
	void init();

	/** Sets proxyModel model for filtering.
	 * @param proxyModel proxy model to filtering
	 */
	void setVfsProxyModel( VfsFlatListProxyModel *proxyModel );

	/** Sets id for columns. Id equal < 0 means that column doesn't exist.
	 * @param colSizeId  column 'Size' id
	 * @param colTimeId  column 'Time' id
	 * @param colPermId  column 'Permission' id
	 * @param colOwnerId column 'Owner' id
	 * @param colGroupId column 'Group' id
	 **/
	void setColumnsId( int colSizeId, int colTimeId, int colPermId, int colOwnerId, int colGroupId );

	/** Sets id for columns. Id equal < 0 means that column doesn't exist.
	 * @param colSizeId  column 'Size' id
	 * @param colPathId  column 'Path' id
	 * @param colTimeId  column 'Time' id
	 * @param colPermId  column 'Permission' id
	 * @param colOwnerId column 'Owner' id
	 * @param colGroupId column 'Group' id
	 **/
	void setColumnsId( int colSizeId, int colPathId, int colTimeId, int colPermId, int colOwnerId, int colGroupId );

	/** Sets toolTip on the toolButton (changing matching type: regExp/wildcards)
	 * and emits siganal: signalSetRegExpParametersNameCol, that sets parameters for regExp obj.
	 */
    void changeRegExpParamsNameColInModel();

	void setColumnNameStrLst( bool bFindFileResultPanel=false ); // used by FindFileDialog for update list

	bool listFiltered() const { return m_bListFiltered; }

	/** Applies given filter.
	 * @param sPattern string describing filter
	 */
	void applyGivenFilter( const QString &sPattern );

	/** Returns status of list be filtered.
	 * @return true if list is filtered already, otherwise not filtered at all
	 */
	bool filterApplied() const { return m_bListFiltered; }

private:
	TreeView              *m_treeView;

	QMenu                 *m_filterMenu, *m_filterWildcardMenu;
	QAction               *m_useRegExpAction, *m_useWildcardAction, *m_caseSensitivityAction;
	QAction               *m_wildcardStartingWithAction, *m_wildcardContainingAction;
	bool                   m_checkBoxAfterToggledProcessing;
	QActionGroup          *m_actionsFilterGroup, *m_actionsFilterWildcardGroup;
	QRegExp               *m_filterValidationRegExp;
	QStringList            m_weightOperStrLst, m_dateTimeStrLst, m_dateFormatStrLst, m_compareOperStrLst;//, m_permStrLst;
	QStringList            m_columnNameStrLst;
	QString                m_sRichTxtToolTip;

	VfsFlatListProxyModel *m_proxyModel;

	QRegExp::PatternSyntax m_patternSyntax;
	Qt::CaseSensitivity    m_caseSensitivity;

	const int m_columnNameId;
	int m_columnSizeId, m_columnTimeId, m_columnPermId, m_columnOwnerId, m_columnGroupId, m_columnPathId;

	InputTextDialog *getFilterName;
// 	QMap <QString, QString>  m_filterMap1;

	/** Type for possible validation status.
	 */
	struct ValidStatusType {
		enum ValidStatusEnum { VS_none=0, VS_valid, VS_notValid };
	};
    typedef ValidStatusType::ValidStatusEnum ValidStatus;

	QMap <QString, FilterItem *>  m_filterMap;

	bool m_bFindFileResultPanel;
	bool m_bListFiltered;

	QString m_sContextMenuFilter;

	/** Perform validation for given parameter.
	 * Note. Correctness of pattern (reg exp or wild cards) is not validated.
	 * @param inputTxt string to validation
	 * @return validStatus (@see ValidStatusType)
	 */
	ValidStatus validation( const QString &inputTxt );

	/** Validation of operator in given string
	 * @param inputStr string contains: operator(s), spaces, digits
	 * @param bPrevValidationStatus previous validation status
	 * @return value of bPrevValidation if operator valid otherwise false
	 */
	bool        operatorValidation( const QString &inputStr, bool bPrevValidationStatus );

	/** Shows detailed toolTip for permission value wrote in filter.
	 *  Detailed means showing name of group, example: Permission: User
	 */
	void        showToolTipForFilterValue();

	/** Performs filtering items on the parent list view (from contructor) using pattern from line edit (into comboBox).
	 * @param bFilter if true then perform filtering otherwise clear filter on the list view.
	 */
	void        filterItems( bool bFilter );

	/** Sets background color of line edit (into comboBox) according to given parameter.
	 * - value VS_valid: sets color to green,
	 * - value VS_notValid: sets color to red,
	 * - value VS_none: sets color to white,
	 * @param validStatus validation status (see: ValidStatusEnum)
	 */
	void        setValidationStatusAsBgColor( ValidStatus validStatus );

	/** Loads filters from configuration file.
	 */
	void        loadFilters();

private slots:
	/** Slot turn on/off edit mode for m_filterComboBox object.
	 * @param bEditable if true then set object on edit mode otherwise set to no editable
	 */
	void        slotComboEditable( bool bEditable );

	/** Slots called when comboFilter has been changed.
	 * @param currentText new text in combo
	 */
	void        slotComboCurrentIndexChanged( const QString &currentText );

	void        slotToggledInputType( bool bToggled );
	void        slotCleanFilter();

	/** Shows input dialog and gets name for current filter definition.
	 * Name and filter definition sets on the map.
	 */
	void        slotSetNameForCurrentFilter();

protected:
	/** Used for stops m_blinkTimer and reseting current object.
	 *  @param ce - close dialog event.
	 */
	void        closeEvent( QCloseEvent *ce );

	/** Catch events from QComboBox (keyPress and keyRelease)
	 */
	bool        eventFilter( QObject *obj, QEvent *event );

signals:
	void        signalSetRegExpParametersNameCol( QRegExp::PatternSyntax ps, Qt::CaseSensitivity cs, Qt::MatchFlags mf );
	void        signalUpdateStatusBar();

//private:
//	Ui::FileListViewFindItemDlg ui;

};

#endif // _FILELISTVIEWFINDITEM_H
