/***************************************************************************
 *   Copyright (C) 2013 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _FINDFILELISTVIEW_H
#define _FINDFILELISTVIEW_H

#include <QWidget>
#include <QMouseEvent>
#include <QtDesigner/QDesignerExportWidget>

#include "treeview.h"
#include "fileinfo.h"
#include "contextmenu.h"
#include "findfilelistviewmodel.h"
#include "vfsflatlistselectionmodel.h"


class FindFileDialog;

class QDESIGNER_WIDGET_EXPORT FindFileListView : public TreeView
{
	Q_OBJECT // necessary for designer (even if there is no any slots/signals)
public:
	FindFileListView( QWidget *pParent );
	~FindFileListView();

	void addMatchedItem( FileInfo *pFileInfo );

	int  rowCount() const { return m_model->rowCount(); }

	void clear() { m_model->clear(); }

	void setFirstAsCurrent();

	void getFoundFilesList( FileInfoList *pFoundFilesList ) { m_model->getList(pFoundFilesList); }
	void initListView( FileInfoList *pFoundFilesList, KeyShortcuts *pKS );

	void updateRemoved( FileInfo *pFileInfo ) { m_model->removeItem(pFileInfo); }

	FileInfo *currentFileInfo();

	void setParent( QWidget *pParent ) { m_pParent = pParent; }

private:
	FindFileListViewModel      *m_model;
	FindFileListViewProxyModel *m_proxyModel;

	ContextMenu *m_pContextMenu;;
	QWidget     *m_pParent;


	QModelIndex currentModelIndex() const { return model()->index(currentIndex().row(), 0); }
	void showContextMenu( bool bOnCursorPos );

private slots:
	void slotContexMenuAction( int actionId );

protected:
	void keyPressEvent( QKeyEvent *pKE );
	void mouseReleaseEvent ( QMouseEvent *pME );

};

#endif
