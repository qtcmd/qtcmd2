/***************************************************************************
 *   Copyright (C) 2013 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDebug>
#include <QToolTip>

#include "settings.h"
#include "mimetype.h"
#include "findfilelistviewmodel.h"


FindFileListViewModel::FindFileListViewModel( bool bRoundFileSize )
	: m_bBytesRound(bRoundFileSize)
{
	qDebug() << "FindFileListViewModel::FindFileListViewModel.";
	m_headerItems << tr("Name") <<  tr("Size") << tr("Path") << tr("Time") << tr("Permission") << tr("Owner") << tr("Group") << tr("File type");
	m_bShowIcons = true;
	m_bBoldDir = true;

	Settings settings;
	m_hiddenFgColor.setNamedColor(settings.value("filelistview/HiddenFileColor", "#A0A0A0").toString());
	m_executableFgColor.setNamedColor(settings.value("filelistview/ExecutableFileColor", "#008000").toString());
	m_symlinkColor.setNamedColor(settings.value("filelistview/SymlinkColor", "#808080").toString());
	m_brokenSymlinkColor.setNamedColor(settings.value("filelistview/BrokenSymlinkColor", "#800000").toString());

	m_listOfFoundFiles = new FileInfoList;
	m_pIconCache = IconCache::instance();
}

FindFileListViewModel::~FindFileListViewModel()
{
	qDebug() << "FindFileListViewModel::~FindFileListViewModel()";
	delete m_listOfFoundFiles;  m_listOfFoundFiles = NULL;
}
/*
virtual Qt::ItemFlags flags( const QModelIndex &index ) const;
Qt::ItemFlags FindFileListViewModel::flags( const QModelIndex &index ) const
{
	if (! index.isValid())
		return 0;

	return Qt::ItemIsEditable | Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}*/

QVariant FindFileListViewModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
	if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
		return m_headerItems.at(section);

	return QAbstractItemModel::headerData(section, orientation, role);
}

QModelIndex FindFileListViewModel::parent( const QModelIndex &/*index*/ ) const
{
	return QModelIndex();
}

int FindFileListViewModel::columnCount( const QModelIndex &parent ) const
{
	if (parent.column() > 0)
		return 0;

	return m_headerItems.size();
}

int FindFileListViewModel::rowCount( const QModelIndex &parent ) const
{
	if (parent.column() > 0)
		return 0;

	return m_listOfFoundFiles->count();
}


FileInfo * FindFileListViewModel::fileInfo( const QModelIndex &index ) const
{
	Q_ASSERT(index.isValid());
	return m_listOfFoundFiles->at(index.row()); // only after first sorting (QDir obj) work correct!!!
}


QVariant FindFileListViewModel::data( const QModelIndex &index, int role ) const
{
	// Qt4 engine call this every time when (mouse) cursor is over the item's list!
	if (! index.isValid())
		return QVariant();

	if (role != Qt::ToolTipRole)
		QToolTip::hideText();

	int idxOfColumn = index.column();
	FileInfo *fi = fileInfo(index);

	if (role == Qt::DisplayRole || role == Qt::EditRole || role == Qt::ToolTipRole) {
		switch (idxOfColumn) {
			case 0: return fi->fileName();
			case 1: {
				if      (fi->isDir())
					return fi->dirLabel();
				else if (fi->isSymLink())
					return (m_bBytesRound) ? FileInfo::bytesRound(fi->symLinkTarget().size()) : QString::number(fi->symLinkTarget().size());
				else
					return (m_bBytesRound) ? FileInfo::bytesRound(fi->size())                 : QString::number(fi->size());
			}
			case 2: return fi->filePath();
			case 3: return fi->lastModified().toString("yyyy-MM-dd HH:mm:ss");
			case 4: return fi->permissions();
			case 5: return fi->owner();
			case 6: return fi->group();
			case 7: return fi->mimeInfo();
			default:
				break;
// 				Q_ASSERT(false);
		}
	}

	if (role == Qt::ForegroundRole) {
		if (fi->isHidden())
			return m_hiddenFgColor;
		if (fi->isSymLink() && fi->mimeInfo() == "symlink/broken")
			return m_brokenSymlinkColor;
		if (! fi->isDir() && fi->isSymLink())
			return m_symlinkColor;
		if (! fi->isDir() && fi->permissions().contains('x'))
			return m_executableFgColor;
	}
	if (idxOfColumn == 0) {
		if (role == Qt::DecorationRole && m_pIconCache != NULL && m_bShowIcons) {
			return m_pIconCache->icon(fi->mimeInfo());
		}
		if (role == Qt::FontRole) {
			if (m_bBoldDir && fi->isDir()) {
				QFont fontBold;
				fontBold.setBold(true);
				return fontBold;
			}
		}
		else
		if (role == Qt::StatusTipRole)
			return fi->symLinkTarget();
	}

	return QVariant();
}

QModelIndex FindFileListViewModel::index( int row, int column, const QModelIndex &parent ) const
{
	if ( ! hasIndex(row, column, parent) )
		return QModelIndex();

	if (row >= 0 && row < m_listOfFoundFiles->count()) {
		return createIndex(row, column);
	}
	return QModelIndex();
}

bool FindFileListViewModel::hasChildren( const QModelIndex &parent ) const
{
	if (parent.isValid())
		return false;

	return true;
}


void FindFileListViewModel::clear()
{
	beginResetModel();
	m_listOfFoundFiles->clear();
	endResetModel();
}

void FindFileListViewModel::init( FileInfoList *pListOfFoundFiles )
{
	if (pListOfFoundFiles == NULL || pListOfFoundFiles->isEmpty())
		return;

	beginResetModel();
 	m_listOfFoundFiles->clear();
	for (int i = 0; i < pListOfFoundFiles->count(); ++i) {
		m_listOfFoundFiles->append(pListOfFoundFiles->at(i));
	}
	endResetModel();
}

void FindFileListViewModel::addItem( FileInfo *pFileInfo )
{
	if ( pFileInfo == NULL)
		return;

	beginResetModel();
	m_listOfFoundFiles->append( pFileInfo );
	endResetModel();
}

void FindFileListViewModel::removeItem( FileInfo *pFileInfo )
{
	if ( pFileInfo == NULL)
		return;

	FileInfo *pFI;
	beginResetModel();
	for (int i = 0; i < m_listOfFoundFiles->count(); ++i) {
		pFI = m_listOfFoundFiles->at(i);
		if ( pFI->absoluteFileName() == pFileInfo->absoluteFileName())
			m_listOfFoundFiles->removeAt(i);
	}
	endResetModel();
}

void FindFileListViewModel::getList( FileInfoList *pNewListOfFoundFiles )
{
	if (pNewListOfFoundFiles != NULL)
		pNewListOfFoundFiles->clear();
	if (pNewListOfFoundFiles == NULL || m_listOfFoundFiles == NULL || m_listOfFoundFiles->count() == 0)
		return;

	for (int i = 0; i < m_listOfFoundFiles->count(); ++i) {
		pNewListOfFoundFiles->append(m_listOfFoundFiles->at(i));
	}
}

// -----------------------------------------

void FindFileListViewProxyModel::initSortKey( char *k, const FileInfo &fi, bool asc, int sortColumn ) const
{
	if (asc) {
		k[0] = fi.fileName() == ".." ? '0' : '1';
		k[1] = fi.isDir() ? '0' : '1';
	} else {
		k[0] = fi.fileName() == ".." ? '1' : '0';
		k[1] = fi.isDir() ? '1' : '0';
	}
	k[2] = fi.isHidden() ? '0' : '1';
	if ((1 == sortColumn && ! fi.isDir()) || 2 == sortColumn)
		k[2] = '0';
}

bool FindFileListViewProxyModel::lessThan( const QModelIndex &left, const QModelIndex &right ) const
{
	char lf[] = "0000";
	char rt[] = "0000";
	const FileInfo &lfi = sourceModel()->fileInfo(left);
	const FileInfo &rfi = sourceModel()->fileInfo(right);

	bool isAscOrder = sortOrder() == Qt::AscendingOrder;
	int  col = sortColumn();
	initSortKey(lf, lfi, isAscOrder, col);
	initSortKey(rt, rfi, isAscOrder, col);

	QVariant ld = sourceModel()->data(left);
	QVariant rd = sourceModel()->data(right);
	qint64  lfiSize = lfi.isSymLink() ? lfi.symLinkTarget().length() : lfi.size();
	qint64  rfiSize = rfi.isSymLink() ? rfi.symLinkTarget().length() : rfi.size();
	bool isLessThan = (1 == col ? lfiSize < rfiSize : QString::localeAwareCompare(ld.toString(), rd.toString()) < 0);
	if (isLessThan) {
		lf[3] = '0';
		rt[3] = '1';
	} else {
		lf[3] = '1';
		rt[3] = '0';
	}

	return qstrcmp(lf, rt) < 0;
}

