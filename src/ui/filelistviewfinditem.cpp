/***************************************************************************
 *   Copyright (C) 2011 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
// #include <QDebug> // need to qDebug()
#include <QKeyEvent>

#include "settings.h"
#include "treeview.h"
#include "filelistview.h"
#include "filelistviewfinditem.h"

#define CLOSE_TIMEOUT  6000


FileListViewFindItem::FileListViewFindItem( TreeView *pTreeView, QWidget *pParent )
	: m_treeView(pTreeView), m_keyEvent(NULL)
{
	setupUi(this);
	setParent(pParent);
	setMinimumHeight(26);
	setMaximumHeight(26);

	Settings settings;
	m_bMatchCaseSensitive = settings.value("filelistview/FileListViewFindItemCaseSensitive", true).toBool();
	m_caseSensitiveChkBox->setChecked(m_bMatchCaseSensitive);

// 	m_closeButton->setShortcut( QKeySequence( tr("Escape")) ); // commented due to override Esc grabbin when are opened filter and search widget

	connect(m_nextButton, &QPushButton::clicked, this, &FileListViewFindItem::slotNextMatching);
	connect(m_prevButton, &QPushButton::clicked, this, &FileListViewFindItem::slotPrevMatching);
	connect(m_caseSensitiveChkBox, &QCheckBox::toggled, this, &FileListViewFindItem::slotSetMatchCaseSensitive);
	connect(this, &FileListViewFindItem::signalKeyPressed, (FileListView *)pParent, &FileListView::slotFindItemKeyPressed);

	m_lineEdit->installEventFilter(this);

	// support for auto closing
	m_closeTimer = new QTimer( this );
	connect(m_closeTimer, &QTimer::timeout, this, &FileListViewFindItem::close);
}

FileListViewFindItem::~FileListViewFindItem()
{
	delete m_closeTimer;
}


void FileListViewFindItem::init( QChar firstChar )
{
	m_keyEvent = NULL;
	m_movementDirection = CM_none;
	updateMatchStatus(MS_none);

	m_lineEdit->setFocus();
	if (firstChar != 0)
		m_lineEdit->setText(QString(firstChar));

	m_closeTimer->start(CLOSE_TIMEOUT);
}


void FileListViewFindItem::keyPressed( QKeyEvent *keyEvent )
{
	unsigned int key = keyEvent->key();
	QString     text = keyEvent->text();

	m_keyEvent = NULL;
	m_closeTimer->start(CLOSE_TIMEOUT);

	switch ( key )
	{
		case Qt::Key_Enter:
		case Qt::Key_Return:
		case Qt::Key_Escape:
		case Qt::Key_Tab:
			m_keyEvent = keyEvent;
			close();
			break;

		case Qt::Key_Left:
		case Qt::Key_Right:
			m_movementDirection = CM_behind;
			break;

		case Qt::Key_Up:
			m_movementDirection = CM_up;
			break;

		case Qt::Key_Down:
			m_movementDirection = CM_down;
			break;

		case Qt::Key_Home:
			m_movementDirection = CM_home;
			break;

		case Qt::Key_End:
			m_movementDirection = CM_end;
			break;

		case Qt::Key_R: // clear (Remove) serching string
		{
			if (keyEvent->modifiers() == Qt::CTRL) {
				m_lineEdit->clear();
				updateMatchStatus(MS_none);
				m_movementDirection = CM_none;
			}
		}
		break;

		default:
		{
			if (text.isEmpty()) {
				if (keyEvent->modifiers() == Qt::NoModifier) {
					if (key == Qt::Key_AltGr) // right Alt
						break;
					m_keyEvent = keyEvent;
					close();
					break;
				}
				else {
					m_movementDirection = CM_behind;
					break;
				}
			}
			m_movementDirection = CM_none;
		}
		break;
	}
}


// Below method base to QAbstractItemView::keyboardSearch, that come from Qt4.3.3 (modified by author)
void FileListViewFindItem::matchItem()
{
	if (m_treeView->model()->rowCount() < 1)
		return;

	const QString searchString = m_lineEdit->text();
	if (searchString.isEmpty()) {
		updateMatchStatus(MS_none);
		return;
	}

	MatchStatus matchStatus = MS_none;
	QModelIndex start = m_treeView->currentIndex().isValid() ? m_treeView->model()->index(m_treeView->currentIndex().row(), 0) : m_treeView->model()->index(0, 0); // always get first column

//	qDebug() << "FileListViewFindItem::matchItem; search:" << searchString << "m_movementDirection:" << m_movementDirection << " m_bMatchCaseSensitive" << m_bMatchCaseSensitive; // << "model:" << m_myTreeView->model(); //QSortFilterProxyModel

	// match with case sensitive and wraparound (only if simple matching)
	Qt::MatchFlags matchFlags = Qt::MatchFlags(Qt::MatchStartsWith | Qt::MatchWrap);
	if (m_movementDirection != CM_none) { // if movements into matching items
		matchFlags = Qt::MatchFlags(Qt::MatchStartsWith);
	}
	if (m_bMatchCaseSensitive)
		matchFlags |= Qt::MatchCaseSensitive;

	QModelIndex parent;
	int newRow, prevMatchSize;
	int matchSize = m_treeView->model()->match(start, Qt::DisplayRole, searchString, -1, matchFlags).size();
	// Up/Down cursor movements into matching items
	if (m_movementDirection == CM_up || m_movementDirection == CM_down) {
		parent = start.parent();
		newRow = (start.row() < m_treeView->model()->rowCount(parent) - 1) ? start.row() + (int)m_movementDirection : 0;
		if (newRow < 0)
			newRow = 0;
		if (m_movementDirection == CM_up) // due to below row will be decreasing
			newRow += 1;
		start = m_treeView->model()->index(newRow, start.column(), parent);
	}
	int  hits = 1;
	// Home/End cursor movements into matching items
	if (m_movementDirection == CM_home || m_movementDirection == CM_end) {
		hits = -1; // collect all match
		if (m_movementDirection == CM_home)
			start = m_treeView->model()->index(0, 0);
	}

	int down_fixRow = start.row() - 1;
	if (down_fixRow < 0)
		down_fixRow = 0;
	// search from start with wraparound or without
	QModelIndex newIndex;
	QModelIndex current = start;
	QModelIndexList match, previous;
	do {
		// match item(s) searching from current
		match = m_treeView->model()->match(current, Qt::DisplayRole, searchString, hits, matchFlags);
		if (match == previous) {
			// matchStatus correction
			if (m_movementDirection == CM_down) {
				current = m_treeView->model()->index(down_fixRow, start.column(), start.parent());
				match = m_treeView->model()->match(current, Qt::DisplayRole, searchString, hits, matchFlags);
				if (match != previous) {
					matchStatus = MS_match;
					break;
				}
			}
			matchStatus = MS_notMatch;
			break;
		}
		else {
			if (m_movementDirection == CM_up) {
				parent = current.parent();
				newRow = current.row() - 1;
				if (newRow >= 0) {
					current = m_treeView->model()->index(newRow, current.column(), parent);
					prevMatchSize = matchSize;
					match = m_treeView->model()->match(current, Qt::DisplayRole, searchString, -1, matchFlags);
					matchSize = match.size();
					//qDebug() << "newRow:" << newRow << "<< match.size():" << matchSize;
					if (prevMatchSize == matchSize)
						continue;
				}
			}
		}
		previous = match;
		if (match.value(0).isValid() && (match.value(0).flags() & Qt::ItemIsEnabled)) {
			newIndex = (m_movementDirection == CM_end) ? match.last() : match.first();
			m_treeView->setCurrentIndex(newIndex);
			matchStatus = MS_match;
			break;
		}
		current = current.sibling((match.value(0).row()+1) % m_treeView->model()->rowCount(), 0);
	} while (current != start && match.value(0).isValid());

	updateMatchStatus(matchStatus);
}

void FileListViewFindItem::updateMatchStatus( MatchStatus matchStatus )
{
	QColor   bgColor;
	QPalette palette;

	if ( matchStatus == MS_match )
		bgColor = QColor( 201,241,215 ); //old: 228, 240, 223
	else
	if ( matchStatus == MS_notMatch )
		bgColor = QColor( 241,229,229 ); //old: 255, 229, 229
	else
		bgColor = Qt::white;

	palette.setColor(m_lineEdit->backgroundRole(), bgColor);
	palette.setColor(m_lineEdit->foregroundRole(), Qt::black);
	m_lineEdit->setPalette(palette);
}


bool FileListViewFindItem::eventFilter( QObject *obj, QEvent *event )
{
	QEvent::Type eventType = event->type();

	if (obj == m_lineEdit) {
		if (eventType == QEvent::FocusOut) {
            m_treeView->clearSelection();
			close();
		}
		else
		if (eventType == QEvent::KeyPress) {
			keyPressed(static_cast<QKeyEvent *>(event));
		}
		else
		if (eventType == QEvent::KeyRelease) {
			if (m_movementDirection < CM_behind) {
				if (static_cast<QKeyEvent *>(event)->modifiers() == Qt::NoModifier)
					matchItem();
			}
		}
		return false;
	}

	return QWidget::eventFilter(obj, event);
}

void FileListViewFindItem::closeEvent( QCloseEvent *ce )
{
// 	qDebug() << "FileListViewFindItem::closeEvent;";// m_pKeyEvent:" << (int *)m_keyEvent;
	m_treeView->setFocus(); // (workaround) restore focus, because it is jumping to second panel

	m_closeTimer->stop();
	m_lineEdit->clear();

	emit signalKeyPressed( m_keyEvent ); // send keyEvent to parent

	ce->accept();
}


// ---------------------- SLOTs -----------------------

void FileListViewFindItem::slotNextMatching()
{
	m_closeTimer->start(CLOSE_TIMEOUT);
	m_movementDirection = CM_down;
	matchItem();
}

void FileListViewFindItem::slotPrevMatching()
{
	m_closeTimer->start(CLOSE_TIMEOUT);
	m_movementDirection = CM_up;
	matchItem();
}

void FileListViewFindItem::slotSetMatchCaseSensitive( bool bMatchCaseSensitive )
{
	m_closeTimer->start(CLOSE_TIMEOUT);
	m_movementDirection   = CM_none;
	m_bMatchCaseSensitive = bMatchCaseSensitive;
	m_caseSensitiveChkBox->setText(bMatchCaseSensitive ? tr("a != A") : tr("a == A"));
	m_caseSensitiveChkBox->setToolTip(bMatchCaseSensitive ? tr("Case sensitive on") : tr("Case sensitive off"));

	Settings settings;
	settings.setValue("filelistview/FileListViewFindItemCaseSensitive", m_caseSensitiveChkBox->isChecked());

	if (! m_lineEdit->text().isEmpty())
		matchItem();
}


