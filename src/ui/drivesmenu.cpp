/***************************************************************************
 *   Copyright (C) 2003 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QtWidgets>

#include "fileinfo.h"
#include "messagebox.h"
#include "drivesmenu.h"
#include "systeminfohelper.h" // for getting capacity and free space on device


DrivesMenu::DrivesMenu( QWidget *pParent )
	: ContextMenu(pParent), m_pInfoDlg(NULL)
{
	m_pIconCache = IconCache::instance();

	m_pProcess = new QProcess;
	connect<void(QProcess::*)(int, QProcess::ExitStatus)>(m_pProcess, &QProcess::finished, this, &DrivesMenu::slotProcessFinished);
}


DrivesMenu::~DrivesMenu()
{
	delete m_pProcess;
	if (m_pInfoDlg != NULL)
		delete m_pInfoDlg;
}


bool DrivesMenu::init()
{
	QFile   file;
	QString sLine;
	QString sMtabFileBuffer, sFStabFileBufferUUID, sFStabFileBuffer;

	// -- read /etc/mtab to buffer
	file.setFileName("/etc/mtab");
	if (file.open(QIODevice::ReadOnly)) {
		QTextStream stream(&file);
		sMtabFileBuffer = stream.readAll(); // read entire stream
		file.close();
	}
	else {
		MessageBox::msg(this, MessageBox::WARNING,
						tr("Cannot open file")+": /etc/mtab\n"+tr("Cannot get the information about mounted devices"));
	}

	// -- read /etc/fstab to buffer (support UUID)
	file.setFileName("/etc/fstab");
	if (! file.open(QIODevice::ReadOnly)) {
		MessageBox::msg(this, MessageBox::CRITICAL, tr("Cannot open file")+": /etc/fstab");
		return false;
	}
	else {
		QTextStream stream(&file);
		do {
			sLine = stream.readLine(); // read line by line
			if (sLine.indexOf("swap") < 0 && ! sLine.startsWith("#") && ! sLine.isEmpty()) {
				if (sLine.startsWith("UUID=", Qt::CaseInsensitive) || sLine.startsWith("/dev", Qt::CaseInsensitive))
					sFStabFileBufferUUID += sLine + "\n";
				else
					sFStabFileBuffer += sLine + "\n";
			}
		}
		while (! sLine.isNull());
		file.close();
	}
	// -- matching UUIDs
	if (sFStabFileBufferUUID.size() > 0) {
		// -- read directory /dev/disk/by-uuid
		QDir dir("/dev/disk/by-uuid");
		dir.setFilter(QDir::Files | QDir::NoDotAndDotDot);

		QFileInfo fileInfo;
		QFileInfoList fiList = dir.entryInfoList();
		for (int i = 0; i < fiList.size(); i++) {
			fileInfo = fiList.at(i);
			if (fileInfo.isSymLink()) {
				//qDebug() << "fileName= " << fileInfo.fileName() << ", symlinkTarget= " << fileInfo.symLinkTarget();
				sFStabFileBufferUUID.replace(fileInfo.fileName(), fileInfo.symLinkTarget());
				sFStabFileBufferUUID.replace("UUID=", "", Qt::CaseInsensitive);
			}
		}
	}
	QStringList slFStab = sFStabFileBufferUUID.split("\n");
 	slFStab += sFStabFileBuffer.split("\n");

	//qDebug() << "sFStabFileBuffer=\n" << sFStabFileBuffer;

	// --- create menu
	enum mtabCols { DEV=0, PATH, FS };
	QIcon mountIcon;
	QStringList slFStabLine;
	bool bIsMounted = false;
	QMenu *pSubMenu = NULL;
	char cCharId = 'a'; // id for menu items
	QString sPrefix;
	qint64 nFreeBytes, nAllBytes;
	QString sFreeBytes, sTotalBytes;
	QString sMenuTitle, sStatusTip, sPathMount;
	QAction *a1, *a2;

	clearMenu(); // menu (current object)

	for (int i=0; i< slFStab.count()-1; i++) {
		sLine = slFStab.at(i);
		slFStabLine = sLine.split(QRegExp("[\\s][\\t]*")); // split by space and tab
		if (slFStabLine.count() < 2) // prevent to CRASH for incorrect entries in fstab file
			continue;
		bIsMounted  = sMtabFileBuffer.indexOf(slFStabLine[DEV])+1;
		mountIcon = icon(bIsMounted ? "drive/mounted" : "drive/notmounted");
		sPathMount = slFStabLine[PATH];
		if (sPathMount.startsWith("/proc") || sPathMount.startsWith("/sys") || sPathMount.startsWith("/dev") || sPathMount.startsWith("/run")
			|| slFStabLine[DEV] == "tmpfs" || slFStabLine[DEV] == "sunrpc")
			continue;

		if (cCharId == 'z'+1) // TODO add support to character-id for more than 25 devices (maybe to use digits)
			cCharId = 'a';
		sPrefix = "&"+QString((QChar)cCharId)+"  ";
		cCharId++;

		if (bIsMounted) {
			// try to get device capacity and free space on it
			Vfs::getStatFS(sPathMount, nFreeBytes, nAllBytes);
			sTotalBytes = Vfs::FileInfo::bytesRound(nAllBytes);
			sFreeBytes  = Vfs::FileInfo::bytesRound(nFreeBytes);
			sMenuTitle = sPrefix+slFStabLine[PATH]+"\t("+ sFreeBytes +" / "+ sTotalBytes +")";
			sStatusTip = tr("Directory mounted on")+" <strong>"+slFStabLine[DEV]+"</strong> "+tr("using")+" <strong>"+slFStabLine[FS]+"</strong> "+tr("file system"); // toolTip showin on status
			// create menu items
			if (slFStabLine[PATH] != "/" && slFStabLine[PATH] != "/usr" && slFStabLine[PATH] != "/var") {
				pSubMenu = new QMenu(this);
				pSubMenu->setTitle(sMenuTitle);
				pSubMenu->setIcon(mountIcon);
				pSubMenu->setStatusTip(sStatusTip); // can't catch it in hovered slot, probably this is QMenu and not QAction
				a1 = pSubMenu->addAction(tr("Open"));
				a1->setData("open "+slFStabLine[PATH]); // command processed in exec()
				a1->setStatusTip(sStatusTip);
				a2 = pSubMenu->addAction(tr("Unmount"));
				a2->setData("unmount "+slFStabLine[PATH]); // command processed in exec()
				a2->setStatusTip(sStatusTip);
				addMenu(pSubMenu);
			}
			else {
// 				a1 = addAction(mountIcon, sMenuTitle, slFStabLine[DEV]+" "+slFStabLine[FS]); // icon, title, toolTip
				a1 = addAction(mountIcon, sMenuTitle); // icon, title
				a1->setStatusTip(sStatusTip);
				a1->setData("open "+slFStabLine[PATH]); // command processed in exec()
			}
		}
		else {
// 			a1 = addAction(mountIcon, sPrefix+slFStabLine[PATH], slFStabLine[DEV]+" "+slFStabLine[FS]); // icon, title, toolTip
			a1 = addAction(mountIcon, sPrefix+slFStabLine[PATH]); // icon, title
			a1->setStatusTip(sStatusTip);
			a1->setData("mount "+slFStabLine[PATH]); // command processed in exec()
		}
	}

	return true;
}


void DrivesMenu::showInfoDlg( const QString &sCaption, const QString &sMsg  )
{
	if (m_pInfoDlg != NULL) {
		delete m_pInfoDlg;  m_pInfoDlg = NULL;
	}
	m_pInfoDlg = new QDialog;
	// dialog should be unresizeable
	m_pInfoDlg->setMinimumSize(320, 140);
	m_pInfoDlg->setMaximumSize(320, 140);
	m_pInfoDlg->setWindowTitle(sCaption+" - QtCommander");

	QLabel *pLab = new QLabel(sMsg, m_pInfoDlg);
	pLab->setGeometry(20, 30, 250, 40);

	QPushButton *btn = new QPushButton(tr("&Break"), m_pInfoDlg);
	btn->move(m_pInfoDlg->width()/2-btn->width()/2, m_pInfoDlg->height()-btn->height()-15);
	connect(btn, &QAbstractButton::clicked, this, &DrivesMenu::slotBreakProcess);

	m_bActionIsFinished = false;
	m_pInfoDlg->exec();
	while (! m_bActionIsFinished)
		m_pInfoDlg->exec();
}

int DrivesMenu::exec( const QPoint &globalPos )
{
	if (! init()) // read information from /etc/fstab and build the menu
		return -1;

// 	QAction *pAction = QMenu::exec(parentWidget()->mapToGlobal(position));
	QAction *pAction = QMenu::exec(globalPos);
	if (pAction == NULL)
		return -1;

	enum mtabCols { COMMAND=0, PATH };
	QString sCommand = pAction->data().toString();
	QStringList slCommand = sCommand.split(QRegExp("[\\s][\\t]*")); // split by space and tab

	if (sCommand.startsWith("open")) {
		qDebug() << "DrivesMenu::exec. Open dir:" << slCommand.at(PATH);
		emit signalPathChanged(slCommand.at(PATH));
	}
	else
	if (sCommand.startsWith("mount")) {
		qDebug() << "DrivesMenu::exec. Mount dir:" << slCommand.at(PATH);
		m_sMountPath = slCommand.at(PATH);
		m_eCurrentOperation = MOUNT;
		runAction();
	}
	else
	if (sCommand.startsWith("unmount")) {
		qDebug() << "DrivesMenu::exec. Unmount dir:" << slCommand.at(PATH);
		m_sMountPath = slCommand.at(PATH);
		m_eCurrentOperation = UNMOUNT;
		runAction();
	}

	return 0;
}


void DrivesMenu::runProcess( Operation eOperation, const QString &sArguments )
{
	QString sProcessName = (eOperation == MOUNT) ? "mount" : "umount";

#if QT_VERSION >= QT_VERSION_CHECK(5, 15, 0)
	QStringList slArgs = QStringList(sArguments); // because usually argument is only one and it is path
	qDebug() << "DrivesMenu::runProcess. Start:" << sProcessName << slArgs;
	m_pProcess->start(sProcessName, slArgs);
#else
	QString sProcess = sProcessName+" "+sArguments;
	qDebug() << "DrivesMenu::runProcess. Start:" << sProcess;
	m_pProcess->start(sProcess);
#endif
}


void DrivesMenu::runAction()
{
	if (m_eCurrentOperation == MOUNT)
		QTimer::singleShot(0, this, &DrivesMenu::slotMount);
	else
	if (m_eCurrentOperation == UNMOUNT)
		QTimer::singleShot(0, this, &DrivesMenu::slotUnmount);

	// --- show an info dialog
	QString sAction = (m_eCurrentOperation == MOUNT) ? tr("Mounting") : tr("Unmounting");
	QString sMsg = tr("Please wait.")+"\n"+sAction+" "+tr("is still processed")+"...";

	showInfoDlg(sAction+" - QtCommander", sMsg);
}


void DrivesMenu::slotUnmount()
{
	runProcess(UNMOUNT, m_sMountPath);
}


void DrivesMenu::slotMount()
{
	runProcess(MOUNT, m_sMountPath);
}



void DrivesMenu::slotProcessFinished( int nExitCode, QProcess::ExitStatus exitStatus )
{
	//qDebug() << "DrivesMenu::slotProcessFinished. Value of nExitCode:" << nExitCode << "exitStatus:" << exitStatus;
	m_bActionIsFinished = true;
	if (m_pInfoDlg) {
		delete m_pInfoDlg;  m_pInfoDlg = NULL;
	}

	if (exitStatus == QProcess::NormalExit && nExitCode == 0)
		emit signalPathChanged(m_sMountPath);
	else { // error occures
		QString sError;
		if (nExitCode == 32) // try mount
			sError = tr("No medium found.");
		else
		if (nExitCode == 1 || nExitCode == 2) // try umount
			sError = tr("Device is busy.");
// 		else
// 		if (nExitCode == 2) // umount
// 			sError = tr("Device is not mounted (according to mtab)");
		else
		if (nExitCode != 0)
			sError = tr("Error no.")+" "+QString::number(nExitCode);

		QString sAction = (m_eCurrentOperation == MOUNT) ? tr("mount") : tr("umount");
		MessageBox::msg(this, MessageBox::CRITICAL, tr("Cannot not")+" "+sAction+" "+tr("device for directory")+" "+m_sMountPath+"\n\n"+sError);
		qDebug() << "DrivesMenu::slotProcessFinished. Value of nExitCode:" << nExitCode;
	}
}


void DrivesMenu::slotBreakProcess()
{
	if (m_pProcess->exitStatus() == QProcess::CrashExit) { // condition maybe not necessary (not tested) :/
		m_pProcess->kill();
		m_bActionIsFinished = true;
		delete m_pInfoDlg; m_pInfoDlg = 0;
	}
}
