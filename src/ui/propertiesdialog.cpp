/***************************************************************************
 *   Copyright (C) 2005 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <sys/stat.h> // for: S_ISUID, S_ISGID, S_ISVTX

#include <QDebug>

#include <QDir>
#include <QIcon>
#include <QStyle>
#include <QLineEdit>
#include <QFileInfo>
#include <qurlinfo.h>

#include "propertiesdialog.h"
#include "systeminfohelper.h" // for getOwners()
#include "pluginfactory.h"
#include "pluginmanager.h"
#include "messagebox.h"
#include "settings.h"
#include "viewer.h"


PropertiesDialog::PropertiesDialog( QWidget *pParent )
	: QWidget(pParent)
{
	m_pDialog = new QDialog(pParent);
// 	m_pDialog->setMaximumWidth(550);
	setupUi(m_pDialog);
	m_pParent = pParent;
	m_bOKbuttonCloseWnd = true;
	m_bArchiveSelected = false;

// 	m_pOkBtn->setIcon(QIcon::fromTheme("dialog-ok")); // dialog-ok-apply
// 	m_pCancelBtn->setIcon(QIcon::fromTheme("dialog-cancel"));
	m_pCancelBtn->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogCancelButton));
	m_pOkBtn->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogOkButton));

	connect(m_pCancelBtn, &QPushButton::released, this, &PropertiesDialog::signalClose);
	connect(m_pOkBtn, &QCheckBox::clicked, this, &PropertiesDialog::slotOkPressed);

	connect(m_pNameComboBox, static_cast<void (QComboBox::*)(const QString &)>(&QComboBox::textActivated), this, &PropertiesDialog::slotInitAllTabs);
//	connect(m_pNameComboBox, static_cast<void (QComboBox::*)(const QString &)>(&QComboBox::activated), this, &PropertiesDialog::slotInitAllTabs); // deprecated in 5.15

	connect(m_pRefreshBtn, &QCheckBox::clicked, this, &PropertiesDialog::slotStartWeighing);
	connect(m_pStopBtn, &QCheckBox::clicked, this, &PropertiesDialog::slotStopWeighing);

	connect(m_pRestorePerm, &QCheckBox::clicked, this, &PropertiesDialog::slotInitAdvPermissions);
	connect(m_pRestoreOwnGrp, &QCheckBox::clicked, this, &PropertiesDialog::slotInitAdvOwnerAndGroup);
	connect(m_pRestoreTime, &QCheckBox::clicked, this, &PropertiesDialog::slotInitAdvAccessAndModifiedTime);

 	connect(m_pChangePermChkBox, &QCheckBox::toggled, this, &PropertiesDialog::slotToggledChangesForChkBox);
	connect(m_pChangeOwnerGroupChkBox, &QCheckBox::toggled, this, &PropertiesDialog::slotToggledChangesForChkBox);
	connect(m_pChangeTimeChkBox, &QCheckBox::toggled, this, &PropertiesDialog::slotToggledChangesForChkBox);

	connect(m_pChangePermComboBox, static_cast<void (QComboBox::*)(int)>(&QComboBox::activated), this, &PropertiesDialog::slotItemActivatedChangesForComboBox);
	connect(m_pChangeOwnerGroupComboBox, static_cast<void (QComboBox::*)(int)>(&QComboBox::activated), this, &PropertiesDialog::slotItemActivatedChangesForComboBox);
	connect(m_pChangeTimeComboBox, static_cast<void (QComboBox::*)(int)>(&QComboBox::activated), this, &PropertiesDialog::slotItemActivatedChangesForComboBox);

	connect(m_usesExtViewChkBox, &QCheckBox::toggled, this, &PropertiesDialog::slotToggledUsesExtView);

	// --- build buttons groups
	m_pClassButtonGroup = new QButtonGroup;
	m_pClassButtonGroup->addButton(m_pClassUserChkBox,  USER_PERM);
	m_pClassButtonGroup->addButton(m_pClassGroupChkBox, GROUP_PERM);
	m_pClassButtonGroup->addButton(m_pClassOtherChkBox, OTHER_PERM);
	m_pClassButtonGroup->setExclusive(false);
	connect(m_pClassButtonGroup, static_cast<void (QButtonGroup::*)(int)>(&QButtonGroup::idClicked), this, &PropertiesDialog::slotInitPermissonsForClass);
//	connect(m_pClassButtonGroup, static_cast<void (QButtonGroup::*)(int)>(&QButtonGroup::buttonClicked), this, &PropertiesDialog::slotInitPermissonsForClass); // deprecated in 5.15

	m_pReadButtonGroup = new QButtonGroup;
	m_pReadButtonGroup->addButton(m_pReadUserChkBox,  USER_PERM);
	m_pReadButtonGroup->addButton(m_pReadGroupChkBox, GROUP_PERM);
	m_pReadButtonGroup->addButton(m_pReadOtherChkBox, OTHER_PERM);
	m_pReadButtonGroup->setExclusive(false);
	connect(m_pReadButtonGroup, static_cast<void (QButtonGroup::*)(int)>(&QButtonGroup::idClicked), this, &PropertiesDialog::slotCheckGroupButton);
//	connect(m_pReadButtonGroup, static_cast<void (QButtonGroup::*)(int)>(&QButtonGroup::buttonClicked), this, &PropertiesDialog::slotCheckGroupButton); // deprecated in 5.15

	m_pWriteButtonGroup = new QButtonGroup;
	m_pWriteButtonGroup->addButton(m_pWriteUserChkBox,  USER_PERM);
	m_pWriteButtonGroup->addButton(m_pWriteGroupChkBox, GROUP_PERM);
	m_pWriteButtonGroup->addButton(m_pWriteOtherChkBox, OTHER_PERM);
	m_pWriteButtonGroup->setExclusive(false);
	connect(m_pWriteButtonGroup, static_cast<void (QButtonGroup::*)(int)>(&QButtonGroup::idClicked), this, &PropertiesDialog::slotCheckGroupButton);
// 	connect(m_pWriteButtonGroup, static_cast<void (QButtonGroup::*)(int)>(&QButtonGroup::buttonClicked), this, &PropertiesDialog::slotCheckGroupButton); // deprecated in 5.15

	m_pExecButtonGroup = new QButtonGroup;
	m_pExecButtonGroup->addButton(m_pExeUserChkBox,  USER_PERM);
	m_pExecButtonGroup->addButton(m_pExeGroupChkBox, GROUP_PERM);
	m_pExecButtonGroup->addButton(m_pExeOtherChkBox, OTHER_PERM);
	m_pExecButtonGroup->setExclusive(false);
	connect(m_pExecButtonGroup, static_cast<void (QButtonGroup::*)(int)>(&QButtonGroup::idClicked), this, &PropertiesDialog::slotCheckGroupButton);
// 	connect(m_pExecButtonGroup, static_cast<void (QButtonGroup::*)(int)>(&QButtonGroup::buttonClicked), this, &PropertiesDialog::slotCheckGroupButton); // deprecated in 5.15

	m_pSpecialButtonGroup = new QButtonGroup;
	m_pSpecialButtonGroup->addButton(m_pSuidChkBox,  USER_PERM);
	m_pSpecialButtonGroup->addButton(m_pSgidChkBox, GROUP_PERM);
	m_pSpecialButtonGroup->addButton(m_pSvtxChkBox, OTHER_PERM);
	m_pSpecialButtonGroup->setExclusive(false);
	connect(m_pSpecialButtonGroup, static_cast<void (QButtonGroup::*)(int)>(&QButtonGroup::idClicked), this, &PropertiesDialog::slotCheckGroupButton);
// 	connect(m_pSpecialButtonGroup, static_cast<void (QButtonGroup::*)(int)>(&QButtonGroup::buttonClicked), this, &PropertiesDialog::slotCheckGroupButton); // deprecated in 5.15

	n_bReadForAll  = false;
	m_bWriteForAll = false;
	m_bExecForAll  = false;
	connect(m_pReadForAllBtn,  &QPushButton::released, this, &PropertiesDialog::slotCheckUncheckReadForAll);
	connect(m_pWriteForAllBtn, &QPushButton::released, this, &PropertiesDialog::slotCheckUncheckWriteForAll);
	connect(m_pExeForAllBtn,   &QPushButton::released, this, &PropertiesDialog::slotCheckUncheckExecForAll);

	m_pChangePermChkBox->setObjectName("ChangePermChkBox");
	m_pChangeOwnerGroupChkBox->setObjectName("ChangeOwnerGroupChkBox");
	m_pChangeTimeChkBox->setObjectName("ChangeTimeChkBox");

	m_pChangePermComboBox->setObjectName("ChangesForPermComboBox");
	m_pChangeOwnerGroupComboBox->setObjectName("ChangesForOwnGrpComboBox");
	m_pChangeTimeComboBox->setObjectName("ChangesForTimeComboBox");

	// -- need to enabled, becouse Qt4 default disabled GroupBox with properties checkable
	enableButtonGroup(m_pReadButtonGroup);
	enableButtonGroup(m_pWriteButtonGroup);
	enableButtonGroup(m_pExecButtonGroup);

	// -- error info after weighing
	m_pErrorDuringWeighing->setVisible(false);
	QPalette paletteErr;
	paletteErr.setColor(m_pErrorDuringWeighing->foregroundRole(), QColor(255,0,0)); // txtColor
	m_pErrorDuringWeighing->setPalette(paletteErr);

	QPalette paletteAdvPerm;
 	paletteAdvPerm.setColor(m_pAdvPermStrLab->backgroundRole(), Qt::white);
 	paletteAdvPerm.setColor(m_pAdvPermStrLab->foregroundRole(), Qt::black);
	m_pAdvPermStrLab->setPalette(paletteAdvPerm);
	m_pAdvPermOctalLab->setPalette(paletteAdvPerm);
	m_pAdvPermStrLab->setAutoFillBackground(true);
	m_pAdvPermOctalLab->setAutoFillBackground(true);

	m_pDialog->installEventFilter(this); // due to: setupUi(m_pDialog) there is need to handle event by event filter
// 	m_pDialog->setFixedSize(550,570);
// 	m_pDialog->adjustSize();
}

PropertiesDialog::~PropertiesDialog()
{
	delete m_pDialog;
}

void PropertiesDialog::enableButtonGroup( QButtonGroup *pButtonGroup )
{
	for (int i=0; i<pButtonGroup->buttons().count(); i++)
		pButtonGroup->button(i)->setEnabled(true);
}


void PropertiesDialog::init( FileInfoToRowMap *pSelectionMap, const URI &uri, bool bReadOnly )
{
	m_selectionMap = pSelectionMap;
	m_bForbiddenChanges = bReadOnly;
	if (bReadOnly) {
		m_pChangeTimeChkBox->setVisible(false);
		m_pChangePermChkBox->setVisible(false);
		m_pChangeOwnerGroupChkBox->setVisible(false);
		m_pChangePermComboBox->setVisible(false);
		m_pChangeOwnerGroupComboBox->setVisible(false);
		m_pChangeTimeComboBox->setVisible(false);
		m_pRefreshBtn->setEnabled(false);
	}
	else {
		m_pChangePermComboBox->setEnabled(false);
		m_pChangeOwnerGroupComboBox->setEnabled(false);
		m_pChangeTimeComboBox->setEnabled(false);
	}

	m_pDialog->setWindowTitle(tr("File properties")+" - QtCommander");
	m_sLoggedUserName = getCurrentUserName(); //sCurrentLoggedUserName;  (it's make sense only for localsubsystem)
	m_pCancelBtn->setShortcut(Qt::Key_Escape);
	m_sAllSelectedItem = tr("ALL SELECTED");

	m_pNameComboBox->setFocus(); // set focus and highlight the text
	if (m_pNameComboBox->lineEdit())
		connect(m_pNameComboBox->lineEdit(), &QLineEdit::returnPressed, this, &PropertiesDialog::slotOkPressed);
	m_pNameComboBox->clear();
	if (pSelectionMap->count() > 1)
		m_pNameComboBox->addItem(m_sAllSelectedItem);

    FileInfoToRowMap::const_iterator it = pSelectionMap->constBegin();
	while (it != pSelectionMap->constEnd()) {
		m_itemsMap.insert(it.key(), it.key()->fileName());
		m_pNameComboBox->addItem(it.key()->absoluteFileName());
		m_slInputNames << it.key()->absoluteFileName();
		++it;
	}
	if (m_pNameComboBox->count() == 1 && bReadOnly) {
// 		m_pNameComboBox->lineEdit()->home(false); // doesn't work
		m_pNameComboBox->lineEdit()->deselect();  // doesn't work
	}

	m_URI = uri;
	if (! uri.isValid())
		m_URI.setUri(pSelectionMap->constBegin().key()->absoluteFileName());
	slotInitAllTabs(QFileInfo(m_pNameComboBox->itemText(0)).fileName());

	m_pTabWidget->setCurrentIndex(0);
}


FileAttributes::ChangesFor PropertiesDialog::changesFor( ChangedAttib changedAttrib )
{
	if (changedAttrib == FILE_NAME) {
		int offset = (m_pNameComboBox->count() > 1) ? 1 : 0; // if selected more than 1 item then m_pNameComboBox contains more items than m_slInputNames
		for (int i = 0; i < m_pNameComboBox->count(); ++i) {
			if (m_pNameComboBox->itemText(i) == m_sAllSelectedItem)
				continue;
			if (m_pNameComboBox->itemText(i) != m_slInputNames[i-offset])
				return (FileAttributes::ChangesFor)0; // fake value, because this is only change status - different than NONE means changed files name
		}
	}
	else
	if (changedAttrib == PERMISSIONS) {
		if (m_pChangePermChkBox->isChecked())
			return (m_pChangePermComboBox->count() == 1 ? FileAttributes::FILES_ONLY : (FileAttributes::ChangesFor)m_pChangePermComboBox->currentIndex());
	}
	else
	if (changedAttrib == OWNER_AND_GROUP) {
		if (m_pChangeOwnerGroupChkBox->isChecked() && (m_pOwnerListWidget->currentItem() != nullptr || m_pGroupListWidget->currentItem() != nullptr))
			return (m_pChangeOwnerGroupComboBox->count() == 1 ? FileAttributes::FILES_ONLY : (FileAttributes::ChangesFor)m_pChangeOwnerGroupComboBox->currentIndex());
	}
	else
	if (changedAttrib == ACCESS_MODFIED_TIME) {
		if (m_pChangeTimeChkBox->isChecked())
			return (m_pChangeTimeComboBox->count() == 1 ? FileAttributes::FILES_ONLY : (FileAttributes::ChangesFor)m_pChangeTimeComboBox->currentIndex());
	}

	return FileAttributes::NONE;
}


bool PropertiesDialog::changeTheAttributes()
{
	bool bAnyAttributesToChange = (
		changesFor(FILE_NAME) != FileAttributes::NONE ||
		changesFor(PERMISSIONS) != FileAttributes::NONE ||
		changesFor(OWNER_AND_GROUP) != FileAttributes::NONE ||
		changesFor(ACCESS_MODFIED_TIME) != FileAttributes::NONE
	);

	return bAnyAttributesToChange;
}


int PropertiesDialog::permissions() const
{
	int nPerm = 0;

	if (m_pReadUserChkBox->isChecked() )  nPerm += QUrlInfo::ReadOwner;
	if (m_pReadGroupChkBox->isChecked())  nPerm += QUrlInfo::ReadGroup;
	if (m_pReadOtherChkBox->isChecked())  nPerm += QUrlInfo::ReadOther;
	if (m_pWriteUserChkBox->isChecked() ) nPerm += QUrlInfo::WriteOwner;
	if (m_pWriteGroupChkBox->isChecked()) nPerm += QUrlInfo::WriteGroup;
	if (m_pWriteOtherChkBox->isChecked()) nPerm += QUrlInfo::WriteOther;
	if (m_pExeUserChkBox->isChecked() )   nPerm += QUrlInfo::ExeOwner;
	if (m_pExeGroupChkBox->isChecked())   nPerm += QUrlInfo::ExeGroup;
	if (m_pExeOtherChkBox->isChecked())   nPerm += QUrlInfo::ExeOther;

	if (m_pSuidChkBox->isChecked()) nPerm += S_ISUID;
	if (m_pSgidChkBox->isChecked()) nPerm += S_ISGID;
	if (m_pSvtxChkBox->isChecked()) nPerm += S_ISVTX;

	return nPerm;
}

void PropertiesDialog::setDescrPermissions()
{
	QString sPerm = m_selectedFI->permissions();
	sPerm.remove(0, 1);
	if (sPerm.count('-') == 9) {
		sPerm = tr("Not known");
		m_pPermissionsLab->setText(sPerm);
		return;
	}
	QString sRead, sWrite, sExe, sDescPerm;
// 012345678
// rwxrwxrwx
// 	qDebug() << "sPerm" << sPerm;
	if (sPerm.count('r') == 3)
		sRead = tr("read for all") + "\n";
	else
	if (sPerm[0] == 'r' && sPerm[3] == 'r')
		sRead = tr("read for owner and group") + "\n";
	else
	if (sPerm[0] == 'r' && sPerm[6] == 'r')
		sRead = tr("read for owner and others") + "\n";
	else
	if (sPerm[3] == 'r' && sPerm[6] == 'r')
		sRead = tr("read for group and others") + "\n";
	else
	if (sPerm[0] == 'r')
		sRead = tr("read for owner") + "\n";
	else
	if (sPerm[3] == 'r')
		sRead = tr("read for group") + "\n";
	else
	if (sPerm[6] == 'r')
		sRead = tr("read others") + "\n";

	if (sPerm.count('w') == 3)
		sWrite = tr("write for all") + "\n";
	else
	if (sPerm[1] == 'w' && sPerm[4] == 'w')
		sWrite = tr("write for owner and group") + "\n";
	else
	if (sPerm[1] == 'w' && sPerm[7] == 'w')
		sWrite = tr("write for owner and others") + "\n";
	else
	if (sPerm[4] == 'w' && sPerm[7] == 'w')
		sWrite = tr("write for group and others") + "\n";
	else
	if (sPerm[1] == 'w')
		sWrite = tr("write for owner") + "\n";
	else
	if (sPerm[4] == 'w')
		sWrite = tr("write for group") + "\n";
	else
	if (sPerm[7] == 'w')
		sWrite = tr("write others") + "\n";

	if (sPerm.count('x') == 3)
		sExe = tr("executing for all") + "\n";
	else
	if (sPerm[2] == 'x' && sPerm[5] == 'x')
		sExe = tr("executing for owner and group") + "\n";
	else
	if (sPerm[2] == 'x' && sPerm[8] == 'x')
		sExe = tr("executing for owner and others") + "\n";
	else
	if (sPerm[5] == 'x' && sPerm[8] == 'x')
		sExe = tr("executing for group and others") + "\n";
	else
	if (sPerm[2] == 'x')
		sExe = tr("executing for owner") + "\n";
	else
	if (sPerm[2] == 'S')
		sExe = tr("sicky bit for user") + "\n";
	else
	if (sPerm[5] == 'x')
		sExe = tr("executing for group") + "\n";
	else
	if (sPerm[5] == 'S')
		sExe = tr("sicky bit for group") + "\n";
	else
	if (sPerm[8] == 'x')
		sExe = tr("executing others") + "\n";
	else
	if (sPerm[8] == 'S')
		sExe = tr("sicky bit for others") + "\n";

	sDescPerm = sRead + sWrite + sExe;
	sDescPerm.remove(sDescPerm.length()-1, 1); // remove last NL
	m_pPermissionsLab->setText(sDescPerm);
}



void PropertiesDialog::slotInitAllTabs( const QString &sInpFileName )
{
// 	qDebug() << "PropertiesDlg::slotInitAllTabs. sFileName:" << sInpFileName;
	QString sTypeInfo, sLocation, sMime;
	FileInfo *pFileInfo = nullptr;
	bool bAllSelected = (m_pNameComboBox->count() > 1 && m_pNameComboBox->currentIndex() == 0);
	bool isDir = false;

	if (bAllSelected) {
		// -- there is/are dir/dirs in selected items
		bool isFile = false;
		m_bSelectedAtLeastOneDir = false;
		FileInfoToRowMap::const_iterator it = m_selectionMap->constBegin();
		while (it != m_selectionMap->constEnd()) {
			if (it.key()->isDir()) {
				m_bSelectedAtLeastOneDir = true;
				isDir = true;
			}
			else
				isFile = true;
			if (isDir && isFile)
				break;
			++it;
		}
		if (isDir && isFile)
			sTypeInfo = tr("files and directories");
		else
		if (isDir)
			sTypeInfo = tr("directories");
		else {
			sTypeInfo = tr("files");
		}

		if (m_URI.protocol() != "file")
			sLocation = m_URI.protocolWithSuffix() + m_URI.host();
		else
		if (! m_URI.host().isEmpty())
			sLocation = m_URI.host() + QDir::separator();
		sLocation += m_itemsMap.key(QFileInfo(m_pNameComboBox->itemText(1)).fileName())->filePath();

		sMime = "?";
		m_pChangeRecursiveChkBox->setChecked(false);
	}
	else { // single file or directory
		pFileInfo = m_itemsMap.key(QFileInfo(sInpFileName).fileName());
		if (pFileInfo->isDir() && ! pFileInfo->isSymLink()) {
			sTypeInfo = tr("directory");
			m_bSelectedAtLeastOneDir = true;
		}
		else {
			if (pFileInfo->isSymLink())
				sTypeInfo = tr("symlink");
			else
				sTypeInfo = tr("file");
		}
		sMime = pFileInfo->mimeInfo();

		if (m_URI.protocol() != "file")
			sLocation = m_URI.protocolWithSuffix() + m_URI.host() + "/";
		else
		if (! m_URI.host().isEmpty())
			sLocation = m_URI.host() + QDir::separator();
		sLocation += pFileInfo->filePath();

		bool bPluginSupport = PluginFactory::instance()->isPluginAvailable(sMime, "subsystem");
		m_bArchiveSelected = (bPluginSupport && sMime != "folder" && sMime != "folder/symlink");
	}
	m_selectedFI = pFileInfo;

	// --- init first tab
	m_pNameComboBox->setEditable(! bAllSelected);
	if (! bAllSelected)
		m_pNameComboBox->lineEdit()->setText(pFileInfo->fileName());
	if (m_bForbiddenChanges) {
		m_bSelectedAtLeastOneDir = false; // just in case set to false
		if (! bAllSelected)
			m_pNameComboBox->lineEdit()->setReadOnly(true);
	}
	m_pLocationLab->setText(sLocation);
	m_pTypeLab->setText(sTypeInfo);
	m_pMimeLab->setText(sMime);
	// update "Uses internal view"

	Settings settings;
	QString sMimeLst = settings.value("textViewer/AdditionalMimeTypes", "").toString();
	bool bChecked = (sMimeLst.split(':').indexOf(sMime) != -1);
	// -- update for standard mime
	bool bPluginSupport = PluginFactory::instance()->isPluginAvailable(sMime, "subsystem"); // is this archive/compressed file
	if (sMime.startsWith("text/") || sMime.startsWith("audio/") || sMime.startsWith("image/") || sMime.startsWith("video/")
		|| bPluginSupport) {
		bChecked = true;
		m_usesExtViewChkBox->setEnabled(false); // user can't disable it, because this is built in
	}
	m_usesExtViewChkBox->setChecked(bChecked);
	if (! sMime.startsWith("application/") && // exmples: application/x-shellscript:application/x-perl:application/javascript:application/x-desktop
		! sMime.startsWith("text/") && ! sMime.startsWith("audio/") && ! sMime.startsWith("image/") && ! sMime.startsWith("video/"))
		m_usesExtViewChkBox->hide();
	// maybe we need to add toolTip with info: "to view will be forced internal view, if there is no supported RAW mode then will be shown view in BIN mode"

	if (! bAllSelected) {
		setDescrPermissions();
		m_pLastModifiedTimeLab->setText(pFileInfo->lastModified().toString("dddd, d MMMM yyyy, hh:mm:ss"));
	}
	else {
		m_pPermissionsLab->setText("?");
// 		m_pOwnerAndGroupLab->setText("?"); // set inside: slotInitAdvOwnerAndGroup
		m_pLastModifiedTimeLab->setText("?");
	}
// 	isDir ? m_pRefreshBtn->show() : m_pRefreshBtn->hide();
// 	isDir ? m_pStopBtn->show()    : m_pStopBtn->hide();
	slotStartWeighing();

	// --- init second tab
	slotInitAdvPermissions();
	slotInitAdvOwnerAndGroup();
	slotInitAdvAccessAndModifiedTime();

	m_pChangeRecursiveChkBox->setVisible(false);
	if (! m_bForbiddenChanges) {
		if (! bAllSelected) {
			isDir = pFileInfo->isDir();
		}
		m_pChangePermChkBox->setChecked(false);
		m_pChangeOwnerGroupChkBox->setChecked(false);
		m_pChangeTimeChkBox->setChecked(false);

		QStringList dirOptions = (QStringList() << tr("files and directories") << tr("files only") << tr("directories only"));
		QComboBox *comboBoxTab[] = { m_pChangePermComboBox, m_pChangeOwnerGroupComboBox, m_pChangeTimeComboBox };
		for (int i = 0; i < 3; ++i) {
			comboBoxTab[i]->setEditable(false);
			comboBoxTab[i]->clear();
			if (isDir)
				comboBoxTab[i]->addItems(dirOptions);
			else
				comboBoxTab[i]->addItem(tr("files only"));
		}
	}
}

void PropertiesDialog::slotInitAdvPermissions()
{
	m_pClassUserChkBox->setChecked(false);
	m_pClassGroupChkBox->setChecked(false);
	m_pClassOtherChkBox->setChecked(false);

	if (m_selectedFI != nullptr) {
		int nPerm = permissionsFromString(m_selectedFI->permissions());
		m_pReadButtonGroup->button( USER_PERM)->setChecked(nPerm & QUrlInfo::ReadOwner);
		m_pWriteButtonGroup->button(USER_PERM)->setChecked(nPerm & QUrlInfo::WriteOwner);
		m_pExecButtonGroup->button( USER_PERM)->setChecked(nPerm & QUrlInfo::ExeOwner);
		m_pReadButtonGroup->button( GROUP_PERM)->setChecked(nPerm & QUrlInfo::ReadGroup);
		m_pWriteButtonGroup->button(GROUP_PERM)->setChecked(nPerm & QUrlInfo::WriteGroup);
		m_pExecButtonGroup->button( GROUP_PERM)->setChecked(nPerm & QUrlInfo::ExeGroup);
		m_pReadButtonGroup->button( OTHER_PERM)->setChecked(nPerm & QUrlInfo::ReadOther);
		m_pWriteButtonGroup->button(OTHER_PERM)->setChecked(nPerm & QUrlInfo::WriteOther);
		m_pExecButtonGroup->button( OTHER_PERM)->setChecked(nPerm & QUrlInfo::ExeOther);

		if (nPerm & S_ISUID)  m_pSuidChkBox->setChecked(true);
		if (nPerm & S_ISGID)  m_pSgidChkBox->setChecked(true);
		if (nPerm & S_ISVTX)  m_pSvtxChkBox->setChecked(true);

		m_pAdvPermStrLab->setText(m_selectedFI->permissions());
		m_pAdvPermOctalLab->setText(QString("%1").arg(nPerm, 0, 8));

		slotCheckGroupButton(USER_PERM);
		slotCheckGroupButton(GROUP_PERM);
		slotCheckGroupButton(OTHER_PERM);

// 		slotInitPermissonsForClass(USER_PERM);
// 		slotInitPermissonsForClass(GROUP_PERM);
// 		slotInitPermissonsForClass(OTHER_PERM);
	}
	else { // reset all
		m_pReadButtonGroup->button( USER_PERM)->setChecked(false);
		m_pWriteButtonGroup->button(USER_PERM)->setChecked(false);
		m_pExecButtonGroup->button( USER_PERM)->setChecked(false);
		m_pReadButtonGroup->button( GROUP_PERM)->setChecked(false);
		m_pWriteButtonGroup->button(GROUP_PERM)->setChecked(false);
		m_pExecButtonGroup->button( GROUP_PERM)->setChecked(false);
		m_pReadButtonGroup->button( OTHER_PERM)->setChecked(false);
		m_pWriteButtonGroup->button(OTHER_PERM)->setChecked(false);
		m_pExecButtonGroup->button( OTHER_PERM)->setChecked(false);

		m_pSuidChkBox->setChecked(false);
		m_pSgidChkBox->setChecked(false);
		m_pSvtxChkBox->setChecked(false);

		m_pAdvPermStrLab->setText("??????????");
		m_pAdvPermOctalLab->setText("????");

		m_pChangePermChkBox->setChecked(false);
	}
}

void PropertiesDialog::slotInitAdvOwnerAndGroup()
{
	QStringList slList;
	QList <QListWidgetItem *> foundItemsLst;

	slList = getAllUsers();
	m_pOwnerListWidget->clear();
	m_pOwnerListWidget->addItems(slList);

    slList = getAllGroups();
	m_pGroupListWidget->clear();
	m_pGroupListWidget->addItems(slList);

	QString sOwner, sGroup;
	if (m_selectedFI != nullptr) {
		sOwner = m_selectedFI->owner();
		sGroup = m_selectedFI->group();

		QListWidgetItem *pLWitem = nullptr;
		QString sOwnGrp = sOwner+", "+sGroup;
		if (sOwnGrp == ", ")
			sOwnGrp = tr("Not known");
		m_pOwnerAndGroupLab->setText(sOwnGrp); // set first tab
		// - update owner
		foundItemsLst = m_pOwnerListWidget->findItems(sOwner, Qt::MatchExactly);
		if (! foundItemsLst.isEmpty()) {
			if (foundItemsLst.first() == nullptr) // not found sOwner
				m_pOwnerListWidget->insertItem(0, sOwner);

			pLWitem = m_pOwnerListWidget->findItems(sOwner, Qt::MatchExactly).first();
			m_pOwnerListWidget->setCurrentItem(pLWitem);
			m_pOwnerListWidget->scrollToItem(pLWitem, QAbstractItemView::PositionAtTop); // vertical policy must be 'prefered'
		}
		// - update group
		foundItemsLst = m_pGroupListWidget->findItems(sGroup, Qt::MatchExactly);
		if (! foundItemsLst.isEmpty()) {
			if (foundItemsLst.first() == nullptr) // not found sGroup
				m_pGroupListWidget->insertItem(0, sGroup);

			pLWitem = foundItemsLst.first();
			m_pGroupListWidget->setCurrentItem(pLWitem);
			m_pGroupListWidget->scrollToItem(pLWitem, QAbstractItemView::PositionAtTop); // vertical policy must be 'prefered'
		}
	}
	else {
		m_pOwnerAndGroupLab->setText("?"); // set first tab
		m_pOwnerListWidget->scrollToTop(); // vertical policy must be 'prefered'
		m_pOwnerListWidget->setCurrentItem(nullptr);
		m_pGroupListWidget->scrollToTop(); // vertical policy must be 'prefered'
		m_pGroupListWidget->setCurrentItem(nullptr);
		m_pChangeOwnerGroupChkBox->setChecked(false);
// 		sOwner = getCurrentUserName(); // get current logged user name
// 		sGroup = getCurrentGroupName();// get current logged user's sGroup name
	}
//	bool enableOwnerChanges = (sOwner == m_sLoggedUserName || m_sLoggedUserName == "root");
}

void PropertiesDialog::slotInitAdvAccessAndModifiedTime()
{
	if (m_selectedFI != nullptr) {
		if (m_bForbiddenChanges)
			m_pLastAccessTimeEdit->setDateTime(m_selectedFI->lastModified());
		else // localsubsystem
			m_pLastAccessTimeEdit->setDateTime(QFileInfo(m_selectedFI->absoluteFileName()).lastRead());

		m_pLastModifiedTimeEdit->setDateTime(m_selectedFI->lastModified());
	}
	else {
// 		QDateTime emptyDateTime(QDate(1970,1,1)); // year == 1 means 1752  - deprecated in 5.15
		QDateTime emptyDateTime(QDate().startOfDay());
		m_pLastAccessTimeEdit->setDateTime(emptyDateTime);
		m_pLastModifiedTimeEdit->setDateTime(emptyDateTime);

		m_pChangeTimeChkBox->setChecked(false);
	}
}


void PropertiesDialog::slotStartWeighing()
{
	m_pWeightLab->setText(tr("Weighing, please wait ..."));
	bool bAllSelected = (m_pNameComboBox->count() > 1 && m_pNameComboBox->currentIndex() == 0);
	FileInfo *fileInfo = nullptr;
	if (! bAllSelected)
		fileInfo = m_itemsMap.key(QFileInfo(m_pNameComboBox->itemText(m_pNameComboBox->currentIndex())).fileName());

	emit signalStartWeighing(fileInfo, true); // true - start weigh, false - stop weigh, fileInfo == nullptr - all selected.
}

void PropertiesDialog::slotStopWeighing()
{
	m_pWeightLab->setText(tr("Weighing stopped"));
	emit signalStartWeighing(nullptr, false);
}

void PropertiesDialog::slotSetWeighingResult( qint64 nWeight, qint64 nRealWeight, uint nFiles, uint nDirs )
{
	bool bExtInfo = true;
	if (nWeight < 0) {
		m_pWeightLab->setText(tr("Cannot weight item"));
		m_pRefreshBtn->setEnabled(false);
		m_pStopBtn->setEnabled(false);
		return;
	}

	QString sWeight = FileInfo::bytesRound(nWeight);
	if (nWeight > 1024)
		sWeight += QString(tr(" (%1 bytes)")).arg(nWeight);
	QString sRealWeigtht = FileInfo::bytesRound(nRealWeight);
	if (nRealWeight > 1024)
		sRealWeigtht += QString(tr(" (%1 bytes)")).arg(nRealWeight);
// 	QString sSizeInfo = formatNumber(nWeight, BKBMBGBformat)+" ("+formatNumber(nWeight, THREEDIGITSformat)+")";
	if (nWeight == 0 && nDirs == 0 && nFiles == 0) {
		sWeight = "? B (? B)";
		if (bExtInfo)
			sRealWeigtht = "? B (? B)";
	}

	m_pWeightLab->setText(tr("Weighing, please wait ..."));
	bool bWeighingFihished = (nDirs > 0 || nFiles > 0 || m_bForbiddenChanges);
	if (bWeighingFihished) {
		if (m_bArchiveSelected)
			m_pWeightLab->setText(FileInfo::bytesRound(m_selectedFI->size()));
		else {
			m_pWeightLab->setText(sWeight);
		}
		realWeightLabel->setVisible(bExtInfo);
		containsLabel->setVisible(bExtInfo);
		if (bExtInfo) {
			if (m_bArchiveSelected) {
				m_pRealWeightLab->setText(sWeight);
				m_pRealWeightLab->setToolTip(tr("Weight of extracted archive"));
			}
			else {
				m_pRealWeightLab->setText(sRealWeigtht);
			}
			QString sDirInfo  = (nDirs == 1 && nFiles == 0) ? tr("1 directory") : QString(tr("%1 directories")).arg(nDirs);
			QString sFileInfo = (nDirs == 0 && nFiles == 1) ? tr("1 file")      : QString(tr("%1 files")).arg(nFiles);
			QString sFullInfo;
			if (nDirs > 0  && nFiles > 0)
				sFullInfo = sDirInfo + tr(" and ") + sFileInfo;
			if (nDirs > 0  && nFiles == 0)
				sFullInfo = sDirInfo;
			if (nDirs == 0 && nFiles > 0)
				sFullInfo = sFileInfo;
			m_pFilesAmountLab->setText(sFullInfo);
		}
	}

	if (! m_bForbiddenChanges)
		m_pRefreshBtn->setEnabled(bWeighingFihished);
	m_pStopBtn->setVisible(! bWeighingFihished);
}


void PropertiesDialog::slotInitPermissonsForClass( int nPermClass )
{
	bool bIsChecked = m_pClassButtonGroup->button(nPermClass)->isChecked();

	m_pReadButtonGroup->button(nPermClass)->setChecked(bIsChecked);
	m_pWriteButtonGroup->button(nPermClass)->setChecked(bIsChecked);
	m_pExecButtonGroup->button(nPermClass)->setChecked(bIsChecked);

	bool isDir = (m_selectedFI != nullptr) ? m_selectedFI->isDir() : false;
	m_pAdvPermStrLab->setText(permissionsToString(permissions(), isDir));
	m_pAdvPermOctalLab->setText(QString("%1").arg(permissions(), 0, 8));
}

void PropertiesDialog::slotCheckGroupButton( int nButtonId )
{
	if (nButtonId < 0 || nButtonId > 2)
		return;

	// -- check for class
	bool r = m_pReadButtonGroup->button(nButtonId)->isChecked();
	bool w = m_pWriteButtonGroup->button(nButtonId)->isChecked();
	bool x = m_pExecButtonGroup->button(nButtonId)->isChecked();

	m_pClassButtonGroup->button(nButtonId)->setChecked(r && w && x);

	bool isDir = (m_selectedFI != nullptr) ? m_selectedFI->isDir() : false;
	m_pAdvPermStrLab->setText(permissionsToString(permissions(), isDir));
	m_pAdvPermOctalLab->setText(QString("%1").arg(permissions(), 0, 8));
}

void PropertiesDialog::slotActivatedName( const QString &sFullName )
{
	if (m_pNameComboBox->count() > 1) {
		if (m_pNameComboBox->currentIndex() > 0) // not bAllSelected
			m_pNameComboBox->setCurrentIndex(m_pNameComboBox->findText(sFullName));
	}
}


void PropertiesDialog::slotItemActivatedChangesForComboBox( int index )
{
	if (sender() == nullptr) {
		qDebug() << "PropertiesDlg::slotItemActivatedChangesForComboBox. Empty sender";
		return;
	}
	QString sSenderName = sender()->objectName();
	int nCurrentIdx;

	if (sSenderName == "ChangesForPermComboBox") {
		// align values all in Changes...ComboBox
		nCurrentIdx = m_pChangePermComboBox->currentIndex();
		m_pChangeOwnerGroupComboBox->setCurrentIndex(nCurrentIdx);
		m_pChangeTimeComboBox->setCurrentIndex(nCurrentIdx);
	}
	else
	if (sSenderName == "ChangesForOwnGrpComboBox") {
		// align values all in Changes...ComboBox
		nCurrentIdx = m_pChangeOwnerGroupComboBox->currentIndex();
		m_pChangePermComboBox->setCurrentIndex(nCurrentIdx);
		m_pChangeTimeComboBox->setCurrentIndex(nCurrentIdx);
	}
	else
	if (sSenderName == "ChangesForTimeComboBox") {
		// align values all in Changes...ComboBox
		nCurrentIdx = m_pChangeTimeComboBox->currentIndex();
		m_pChangePermComboBox->setCurrentIndex(nCurrentIdx);
		m_pChangeOwnerGroupComboBox->setCurrentIndex(nCurrentIdx);
	}
}


void PropertiesDialog::slotToggledChangesForChkBox( bool state )
{
		if (sender() == nullptr) {
		qDebug() << "PropertiesDlg::slotToggledChangesForChkBox. Empty sender";
		return;
	}
	QString sSenderName = sender()->objectName();

	QComboBox *comboBoxTab[] = { m_pChangePermComboBox, m_pChangeOwnerGroupComboBox, m_pChangeTimeComboBox };

	if (sSenderName == "ChangePermChkBox")
		comboBoxTab[PERMISSIONS]->setEnabled(state);
	else
	if (sSenderName == "ChangeOwnerGroupChkBox")
		comboBoxTab[OWNER_AND_GROUP]->setEnabled(state);
	else
	if (sSenderName == "ChangeTimeChkBox")
		comboBoxTab[ACCESS_MODFIED_TIME]->setEnabled(state);

	bool bChangesForAllSelected = (m_selectedFI == nullptr);
	bool isDir = false;
	if (bChangesForAllSelected) {
		FileInfoToRowMap::const_iterator it = m_selectionMap->constBegin();
		while (it != m_selectionMap->constEnd()) {
			if (it.key()->isDir()) {
				m_bSelectedAtLeastOneDir = true;
				isDir = true;
				break;
			}
			++it;
		}
	}
	else {
		isDir = m_selectedFI->isDir();
	}

	m_pChangeRecursiveChkBox->setVisible(state && m_bSelectedAtLeastOneDir && ! m_bForbiddenChanges && isDir);
}

void PropertiesDialog::slotToggledUsesExtView( bool bState )
{
	if (! m_usesExtViewChkBox->isEnabled()) // because signal is sent even from disabled checkBox
		return;

	QString sMime = m_pMimeLab->text();
	// - get list of mimes for textviewer_plugin
	QStringList slMimes = PluginManager::instance()->getAllRegistreredPluginsInfo(PluginData::MIME_TYPE, "viewer");
	QStringList slNames = PluginManager::instance()->getAllRegistreredPluginsInfo(PluginData::NAME, "viewer");
	int nTxtViewerId = slNames.indexOf("textviewer_plugin");
	QString sNewMimeLst;
	if (nTxtViewerId > -1 && slMimes.size() > 0)
		sNewMimeLst = slMimes.at(nTxtViewerId);

	QString sOldMimeLst = sNewMimeLst;

	// - update list of mimes with new mime
	bool bFoundMime = (sNewMimeLst.split(':').indexOf(sMime) != -1);
	if (bState) {
		if (! bFoundMime) {
			sNewMimeLst += ":"+sMime;
		}
	}
	else {
		if (bFoundMime) {
			int id = sNewMimeLst.indexOf(sMime);
			sNewMimeLst.remove(id, sMime.length()+1);
			if (sNewMimeLst.endsWith(":"))
				sNewMimeLst.remove(sNewMimeLst.length()-1, 1);
		}
	}

	Settings settings;
	settings.setValue("textViewer/AdditionalMimeTypes", sNewMimeLst);
	PluginFactory::instance()->updateMimeConfig("textviewer", sOldMimeLst, sNewMimeLst);
}


void PropertiesDialog::slotOkPressed()
{
	bool bChangesForAllSelected = (m_selectedFI == nullptr);
	//QString sLoggedUserName  = SystemInfo::getUser();       // get current logged user name
	//QString sLoggedGroupName = SystemInfo::getUser(false);  // get current logged user's group name

	FileAttributes *changedAttrib = new FileAttributes;

	changedAttrib->changeAttribNameFor = changesFor(FILE_NAME);
	changedAttrib->changePermFor       = changesFor(PERMISSIONS);
	changedAttrib->changeOwnGrpFor     = changesFor(OWNER_AND_GROUP);
	changedAttrib->changeAccModTimeFor = changesFor(ACCESS_MODFIED_TIME);

	// -- because all "changesFor" are aligned, we can get value from any object
	if      (changedAttrib->changePermFor == FileAttributes::FILES_AND_DIRS)
		changedAttrib->sChangesForMsg = tr("Files and directories");
	else if (changedAttrib->changePermFor == FileAttributes::FILES_ONLY)
		changedAttrib->sChangesForMsg = tr("Files only");
	else if (changedAttrib->changePermFor == FileAttributes::DIRS_ONLY)
		changedAttrib->sChangesForMsg = tr("Directories only");

	// -- init files name
	for (int i = 1; i < m_pNameComboBox->count(); ++i) {
		if (m_pNameComboBox->itemText(i) != m_slInputNames[i-1]) {
			changedAttrib->slOriginalFilesName << m_slInputNames[i-1];
			changedAttrib->slRenamedFiles      << m_pNameComboBox->itemText(i);
		}
	}
	// -- init access and modified time
	if (m_pChangeTimeChkBox->isChecked()) {
		changedAttrib->dtLastAccessTime   = m_pLastModifiedTimeEdit->dateTime();
		changedAttrib->dtLastModifiedTime = m_pLastAccessTimeEdit->dateTime();
	}
	// -- init permissions
	if (m_pChangePermChkBox->isChecked())
		changedAttrib->nPermissions = permissions();
	else
		changedAttrib->nPermissions = 0;

	// -- init owner and group
	if (m_pChangeOwnerGroupChkBox->isChecked()) {
		QListWidgetItem *pOwner = m_pOwnerListWidget->currentItem();
		QListWidgetItem *pGroup = m_pGroupListWidget->currentItem();
		if (pOwner == nullptr && pGroup == nullptr) {
// 			MessageBox::information(m_pParent, tr("Setting attributes"),
// 									tr("You have set option \"Change owner and group for:\"")+"\n"+tr("and no neither owner nor group is selected."));
			bool bDummy = false;
			int nResult = MessageBox::yesNo(m_pParent, tr("Setting attributes"),
											tr("You have set option \"Change owner and group for:\"")+"\n"+tr("and no neither owner nor group is selected."),
											tr("Do you want to continue?"),
											bDummy,
											MessageBox::No // default focused button
										);
			if (nResult == MessageBox::No || nResult == 0) {
				m_bOKbuttonCloseWnd = false;
				return;
			}
		}
		else {
			if (pOwner != nullptr)
				changedAttrib->sOwner = pOwner->text();
			if (pGroup != nullptr)
				changedAttrib->sGroup = pGroup->text();
		}
	}
	// -- init recursive status
	changedAttrib->bRecursiveChanges = m_pChangeRecursiveChkBox->isChecked();

	m_bOKbuttonCloseWnd = true;
	bool bAnyChangeFor = (m_pChangePermChkBox->isChecked() || m_pChangeOwnerGroupChkBox->isChecked() || m_pChangeTimeChkBox->isChecked());
	if (! bAnyChangeFor) {
		bool bDummy = false;
		int nResult = MessageBox::yesNo(m_pParent, tr("Setting attributes"),
										tr("No option \"Change ... for\" is checked.")+"\n"+tr("Changes will be not applied."),
										tr("Are you sure you want to leave properties window?"),
										bDummy,
										MessageBox::No // default focused button
									);
		if (nResult == MessageBox::No || nResult == 0) {
			m_bOKbuttonCloseWnd = false;
			return;
		}
	}

	emit signalOkPressed(m_selectedFI, changedAttrib, bChangesForAllSelected); // will be caught in FileListView::slotSetAttributes
}



void PropertiesDialog::closeEvent( QCloseEvent *e )
{
	emit signalClose();
	e->ignore();
}

bool PropertiesDialog::eventFilter( QObject *pObj, QEvent *pEvent )
{
	if (pEvent->type() == QEvent::Close) {
		if (! m_bOKbuttonCloseWnd)
			return QObject::eventFilter(pObj, pEvent);

		QCloseEvent *pCloseEvent = static_cast<QCloseEvent *>(pEvent);
		closeEvent(pCloseEvent);
		return true;
	}
	return QObject::eventFilter(pObj, pEvent);
}


void PropertiesDialog::slotCheckUncheckReadForAll()
{
	n_bReadForAll = (m_pReadUserChkBox->isChecked() && m_pReadGroupChkBox->isChecked() && m_pReadOtherChkBox->isChecked());
	n_bReadForAll = ! n_bReadForAll;
	m_pReadUserChkBox->setChecked(n_bReadForAll);
	m_pReadGroupChkBox->setChecked(n_bReadForAll);
	m_pReadOtherChkBox->setChecked(n_bReadForAll);
}

void PropertiesDialog::slotCheckUncheckWriteForAll()
{
	m_bWriteForAll = (m_pWriteUserChkBox->isChecked() && m_pWriteGroupChkBox->isChecked() && m_pWriteOtherChkBox->isChecked());
	m_bWriteForAll = ! m_bWriteForAll;
	m_pWriteUserChkBox->setChecked(m_bWriteForAll);
	m_pWriteGroupChkBox->setChecked(m_bWriteForAll);
	m_pWriteOtherChkBox->setChecked(m_bWriteForAll);
}

void PropertiesDialog::slotCheckUncheckExecForAll()
{
	m_bExecForAll = (m_pExeUserChkBox->isChecked() && m_pExeGroupChkBox->isChecked() && m_pExeOtherChkBox->isChecked());
	m_bExecForAll = ! m_bExecForAll;
	m_pExeUserChkBox->setChecked(m_bExecForAll);
	m_pExeGroupChkBox->setChecked(m_bExecForAll);
	m_pExeOtherChkBox->setChecked(m_bExecForAll);
}

