/***************************************************************************
 *   Copyright (C) 2005 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QToolTip>
#include <QMetaObject>

#include "overwritefiledialog.h"


OverwriteFileDialog::OverwriteFileDialog()
{
	setupUi(this);

	m_pButtonGroup = new QButtonGroup;
	m_pButtonGroup->addButton(m_pYesBtn, Yes);
	m_pButtonGroup->addButton(m_pNoBtn, No);
	m_pButtonGroup->addButton(m_pDiffSizeBtn, DiffSize);
	m_pButtonGroup->addButton(m_pRenameBtn, Rename);
	m_pButtonGroup->addButton(m_pAllBtn, All);
	m_pButtonGroup->addButton(m_pUpdateBtn, Update);
	m_pButtonGroup->addButton(m_pNoneBtn, None);
	m_pButtonGroup->addButton(m_pCancelBtn, Cancel);

	connect<void(QButtonGroup::*)(int)>(m_pButtonGroup, &QButtonGroup::idClicked,  this, &OverwriteFileDialog::done); // signal is overloaded
}


int OverwriteFileDialog::show( const Vfs::FileInfo &targetFleInfo, const Vfs::FileInfo &sourceFleInfo )
{
	OverwriteFileDialog *pDlg = new OverwriteFileDialog;
	Q_CHECK_PTR( pDlg );

	pDlg->setWindowTitle(tr("File overwriting")+" - QtCommander");

	pDlg->m_pTargetFileNameLabValue->setText("\""+targetFleInfo.fileName()+"\"");
	pDlg->m_pSourceDateLabValue->setText(sourceFleInfo.lastModified().toString("yyyy-MM-dd HH:mm:ss"));
	pDlg->m_pTargetDateLabValue->setText(targetFleInfo.lastModified().toString("yyyy-MM-dd HH:mm:ss") );
	pDlg->m_pSourceSizeLabValue->setText(QString::number(sourceFleInfo.size()));
	pDlg->m_pTargetSizeLabValue->setText(QString::number(targetFleInfo.size()));

	pDlg->m_pSourceSizeLabValue->setToolTip(Vfs::FileInfo::bytesRound(sourceFleInfo.size()));
	pDlg->m_pTargetSizeLabValue->setToolTip(Vfs::FileInfo::bytesRound(targetFleInfo.size()));

	// --- show dialog
	int nResult = pDlg->exec();

	delete pDlg;  pDlg = 0;

	return nResult;
}
