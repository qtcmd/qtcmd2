/***************************************************************************
 *   Copyright (C) 2015 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef BOOKMARKS_H
#define BOOKMARKS_H

#include <QMap>

#include "contextmenu.h"

/**
 * @author Piotr Mierzwiński
 **/
/** @short Class handles context menu for files list panel.
*/

class Bookmarks : public ContextMenu
{
public:
	Bookmarks( QWidget *pParent );

	/** Rebuild ContextMenu using Bookmarks saved in config file.
	 * @param pKS pointer to KeyShortcuts object
	 */
	void    init( KeyShortcuts *pKS );

	QString pathFromBookmarks( int nId ) const { return m_id2pathMap.value(nId); }
// 	void    setMaxBookmarks( uint nMax ) { m_nMaxBookmarks = nMax; }
	int     bookmarksOptionId() const { return m_nBookmarksOptionId; }
	void    addToTheBookmarks( const QString &sAbsPath );

private:
	QMap <int, QString> m_id2pathMap;

// 	uint m_nMaxBookmarks;
	uint m_nBookmarksOptionId;

};

#endif // BOOKMARKS_H
