/***************************************************************************
 *   Copyright (C) 2017 by Piotr Mierzwi?ski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef HELPDIALOG_H
#define HELPDIALOG_H

#include <QDialog>

#include "iconcache.h"
#include "ui_helpdlg.h"

class IconCache;
class QPushButton;
class QTextBrowser;

/** @short Class provides help window for application
*/
/**
	@author Piotr Mierzwiński
*/
class HelpDialog : public QDialog, public Ui::HelpDlg
{
	Q_OBJECT
public:
	HelpDialog();

	static void show( QWidget *pParent, IconCache *pIconCache );

private:
	IconCache *m_pIconCache;
	QString    m_sHelpPath;

	QIcon icon( const QString &sIconName ) const {
		return m_pIconCache->icon(sIconName);
	}

private slots:
	void slotAnchorClicked( const QUrl &link );

};

#endif // HELPDIALOG_H
