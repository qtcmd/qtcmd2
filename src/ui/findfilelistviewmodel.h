/***************************************************************************
 *   Copyright (C) 2013 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _FINDFILELISTVIEWMODEL_H
#define _FINDFILELISTVIEWMODEL_H

#include <QAbstractItemModel>
#include <QSortFilterProxyModel>

#include "fileinfo.h"
#include "iconcache.h"
// class QIcon;
// class IconCache;

using namespace Vfs;

/**
	@author Mariusz Borowski <b0mar@nes.pl>
	@author Piotr Mierzwinski <piom@nes.pl>
*/
class FindFileListViewModel : public QAbstractItemModel // remove when will be support for TREE and FLAT list (use VfsModel)
{
//	Q_OBJECT
public:
	FindFileListViewModel( bool bRoundFileSize );
	virtual ~FindFileListViewModel();

	virtual QVariant    headerData( int section, Qt::Orientation orientation, int role = Qt::DisplayRole ) const;

	virtual QModelIndex parent( const QModelIndex &index ) const;

	virtual int         columnCount( const QModelIndex &parent = QModelIndex() ) const;

	virtual int         rowCount( const QModelIndex &parent = QModelIndex() ) const;

	virtual QVariant    data( const QModelIndex &index, int role = Qt::DisplayRole ) const;

	virtual QModelIndex index( int row, int column = 0, const QModelIndex &parent = QModelIndex() ) const;

	virtual bool        hasChildren( const QModelIndex &parent = QModelIndex() ) const;

	FileInfo           *fileInfo( const QModelIndex& index ) const;


	/** Clear model.
	 */
    void                clear();

	/** Inits model by items from given list.
	 * @param pListOfFoundFiles list of found items
	 */
	void                init( FileInfoList *pListOfFoundFiles );

	/** Incremental adding items to model.
	 * @param pFileInfo item to adding
	 */
	void                addItem( FileInfo *pFileInfo );
	void                removeItem( FileInfo *pFileInfo );

	void                getList( FileInfoList *pNewListOfFoundFiles );

private:
	QList<QString>    m_headerItems;
	FileInfoList     *m_listOfFoundFiles;

	IconCache        *m_pIconCache;

	bool              m_bBoldDir;
	bool              m_bShowIcons;
	bool              m_bBytesRound;

	QColor            m_hiddenFgColor;
	QColor            m_executableFgColor;
	QColor            m_symlinkColor;
	QColor            m_brokenSymlinkColor;

};



class FindFileListViewProxyModel : public QSortFilterProxyModel
{
public:
	FindFileListViewProxyModel( QObject *parent = 0 ) : QSortFilterProxyModel(parent) {}

	FindFileListViewModel *sourceModel() const { return static_cast <FindFileListViewModel *> (QSortFilterProxyModel::sourceModel()); }

private:
	void initSortKey( char *k, const FileInfo &fi, bool asc, int sortColumn ) const;

protected:
	virtual bool lessThan ( const QModelIndex &left, const QModelIndex &right ) const;

};

#endif // FINDFILELISTVIEWMODEL_H

