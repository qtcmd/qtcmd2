/***************************************************************************
 *   Copyright (C) 2015 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDebug>

#include "settings.h"
#include "bookmarks.h"
#include "messagebox.h"
#include "inputtextdialog.h"
#include "contextmenucmdids.h"


Bookmarks::Bookmarks( QWidget *pParent )
	: ContextMenu(pParent)
{
// 	m_nMaxBookmarks = m_pSettings->value("filelistview/maxBookmarks", 10).toUInt();
	m_nBookmarksOptionId = -1;
}

void Bookmarks::init( KeyShortcuts *pKS )
{
	Q_ASSERT(pKS != nullptr);
	clearMenu();
	m_id2pathMap.clear();

// 	if (m_pSettings->value("filelistview/maxBookmarks").isNull())
// 		m_pSettings->setValue("filelistview/maxBookmarks", m_nMaxBookmarks);

	Settings settings;
	QString sBookmarks = settings.value("filelistview/bookmarks").toString();
	QStringList slBookmarksLst = sBookmarks.split('|', Qt::SkipEmptyParts);

	QChar chr('0');
	QChar chrLetter('a');
	char chrId = chr.toLatin1();
	QString sId, sEntry, sName, sPath;
	int nEntryId = CMD_BookmarksMenuInit;
	QAction *pAction;
	foreach (sEntry, slBookmarksLst) {
		sName = sEntry.left(sEntry.indexOf('='));
		sPath = sEntry.right(sEntry.length() - (sName.length()+1));
		m_id2pathMap.insert(nEntryId, sPath);
		sId = QString("&%1  ").arg(QChar(chrId));
		pAction = addAction(sId + sName, nEntryId, pKS->keyCode(CMD_NoOption));
		pAction->setStatusTip(sPath);
		nEntryId++;
		chrId++;
		if (chrId == ':')
			chrId = chrLetter.toLatin1();
	}

	m_nBookmarksOptionId = CMD_BookmarksMenuInit + (nEntryId - CMD_BookmarksMenuInit) + 1;

	//int nOptionId = CMD_BookmarksMenuInit + m_nMaxInBookmark + 1;
	addSeparator();
	addAction( tr("&Add current path to the bookmarks"), m_nBookmarksOptionId, pKS->keyCode(CMD_NoOption));
	addAction( tr("&Manage the bookmkars"), m_nBookmarksOptionId+1, pKS->keyCode(CMD_NoOption));

	setEnableAction(m_nBookmarksOptionId+1, false);
}

void Bookmarks::addToTheBookmarks( const QString &sAbsPath )
{
	qDebug() << "Bookmarks::putToTheBookmarks. path:" << sAbsPath;
	bool bPostCreatingAction = false;
	QString sInputName = sAbsPath.right(sAbsPath.length() - sAbsPath.lastIndexOf('/',-2) - 1);
	sInputName.remove('/');
	QString sName = InputTextDialog::getText(this, InputTextDialog::BOOKMARK_NAME, sInputName, bPostCreatingAction, sAbsPath);
	if (sName.isEmpty())
		return;

	Settings settings;
	QString sBookmarks = settings.value("filelistview/bookmarks").toString();
	if (! sBookmarks.isEmpty()) {
		if (! sBookmarks.endsWith('|'))
			sBookmarks += "|";
	}
	QStringList slBookmarksLst = sBookmarks.split('|', Qt::SkipEmptyParts);
	int nPathId = slBookmarksLst.indexOf(QRegExp(".*"+sAbsPath));
	int nResult = MessageBox::No;
	if (nPathId != -1) {
		QString sPathName = slBookmarksLst.at(nPathId).left(slBookmarksLst.at(nPathId).indexOf('='));
		bool bDummyChkBox = false;
		nResult = MessageBox::yesNo(this, tr("Add path to the bookmarks"),
			tr("Path already stored with name:")+" "+sPathName,
			tr("Do you want to add it again?")+"\n\n"+tr("Note. Its name you can change using \"Paths manager\""),
			bDummyChkBox, MessageBox::No // default focused button
		);
	}
	if (nResult == 0) // user closed dialog
		return;

	if (nResult == MessageBox::Yes || nPathId == -1) {
		sBookmarks += sName+"="+sAbsPath+"|";
		settings.setValue("filelistview/bookmarks", sBookmarks);
		qDebug() << "Bookmarks::putToTheBookmarks. path stored.";
	}
}

