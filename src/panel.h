/***************************************************************************
 *   Copyright (C) 2005 by Mariusz Borowski <b0mar@nes.pl>                 *
 *   Copyright (C) 2009 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef PANEL_H
#define PANEL_H

#include <QWidget>

#include "pathview.h"
#include "statusbar.h"
#include "keyshortcuts.h"
#include "filelistview.h"

class QVBoxLayout;

class IconCache;
class FileListView;

/**
  @author Mariusz Borowski
  @author Piotr Mierzwiński
  */
class Panel : public QWidget
{
	Q_OBJECT
public:
	Panel( QWidget *pParent, QWidget *pMainWnd, KeyShortcuts *pKeyShortcuts, const QString &sName );
	~Panel();

	void setPanelView( const URI &initUri );
	bool setPanelView( FileInfoList *pFileInfoList, const QString &sTabTitle );

	//FileListView *getView() const { return m_pFileListView; }
	void getIconCache();

	QString getPanelInitURIstr() const { return m_sPanelInitURIstr; }
	URI     getCurrentURI() const { return m_subsystemMngr->uri(); }
	void    changePath( const QString &sNewPath ) { m_subsystemMngr->slotPathChanged(sNewPath); }

	void setFileListViewFocusPolicy( Qt::FocusPolicy focusPolicy ) { m_pFileListView->filelistView()->setFocusPolicy(focusPolicy); }
//	Qt::FocusPolicy fileListViewFocusPolicy() const { return m_pFileListView->filelistView()->focusPolicy(); }
	bool getFileListViewFocus() const { return m_pFileListView->filelistView()->hasFocus(); }
	bool getFileListViewPastFocus() const { return m_pFileListView->hadFocus(); }
	void setFileListViewFocus()   { m_pFileListView->filelistView()->setFocus();   }
	void clearFileListViewFocus() { m_pFileListView->filelistView()->clearFocus(); }

	void openInOppositeActiveTab( const URI &uri ) { m_subsystemMngr->openInOppositeActiveTab(uri); }
	void openInCurrentActiveTab( const URI &uri ) { m_subsystemMngr->openInCurrentActiveTab(uri); }

	void skipPathViewConfig( bool bSkipConfig );   // used by Mainwindow::slotCloseTab

	QStringList pathesList() const { return m_pPathView->list(); }
	void setPathesList( const QStringList &itemsList ) { m_pPathView->setItemsList(itemsList); }

	bool firstListedStat() const { return m_firstListedStat; }

	void copyFilesTo( const QString &sTargetPath, FileInfoToRowMap *pSelectionMap, bool bCopy, bool bWeighBefore ) {
		m_subsystemMngr->copyFilesTo(sTargetPath, pSelectionMap, bCopy, bWeighBefore);
	}
	void startRemoveFiles( FileInfoToRowMap *pSelectionMap, bool bWeighBefore ) {
		m_subsystemMngr->startRemoveFiles(pSelectionMap, bWeighBefore);
	}

	void updateStatusBar() { m_pFileListView->slotUpdateListViewStatusBar(); }

	bool findFileResultMode() const;
	void setColumnsOnListView( bool bFindFileResultMode );

	void setColumnWidth( int nLogicalIndex, int nNewSize ) {
		m_pFileListView->setColumnWidth(nLogicalIndex, nNewSize);
	}

	void updateItemOnLV( const QString &sFileName, bool bExists ) {
		m_subsystemMngr->updateItemOnLV(sFileName, bExists);
	}

	void setFileListViewActive( const QString &sCode ) {
		m_pFileListView->slotUpdateFocusSelection(false, sCode);
	}

	void setKeyShortcuts( KeyShortcuts *pKeyShortcuts ) {
		m_pKeyShortcuts = pKeyShortcuts;
	}

	void forceUnmarkItems( FileInfoList *pProcessedFilesLst ) {
		m_subsystemMngr->forceUnmarkItems(pProcessedFilesLst);
	}

	void openNewRemoteConnection(Vfs::RemoteConnection eRemoteConnection) {
		m_pFileListView->openNewRemoteConnection(eRemoteConnection);
	}

	void showFavoriteFoldersMenu() {
		m_pFileListView->showBookmarksMenu();
	}

	void showDrivesMenu() {
		m_pFileListView->showDrivesMenu();
	}

	void renameFile() {
		m_pFileListView->slotInitRename();
	}

	void quickGetOutOfSubsystem() {
		m_pFileListView->quickGetOutOfSubsystem(); // always returns false
	}
	void oneDirBackward() {
		m_pFileListView->oneDirBackward(m_pPathView->prevUri());
	}
	void oneDirForward() {
		m_pFileListView->oneDirForward(m_pPathView->nextUri());
	}
	void goToRootDirectory() {
		m_pFileListView->goToRootDirectory();
	}
	void goToUserHomeDirectory() {
		m_pFileListView->goToUserHomeDirectory();
	}

	void fileView() {
		m_pFileListView->slotViewSelected(true, false);
	}
	void fileEdit() {
		m_pFileListView->slotViewSelected(true, true);
	}
	void copyFile() {
		m_pFileListView->copyMoveFile(true);
	}
	void moveFile() {
		m_pFileListView->copyMoveFile(false);
	}
	void makeDir() {
		m_pFileListView->makeDir();
	}
	void deleteFile() {
		m_pFileListView->deleteFile();
	}
	void makeLink() {
		m_pFileListView->makeLink();
	}
	void createNewArchive()  {
		m_pFileListView->createAnArchive(true);
	}
	void moveFilesToArchive() {
		m_pFileListView->createAnArchive(false);
	}

	/** Applies settings for list view getting data from given object.
	 * @param listViewSettings reference to container containing list view settings
	 * @param sCategory - category in opened section
	 */
	void applyListViewSettings( const ListViewSettings &listViewSettings, const QString &sCategory ) {
		m_pFileListView->applyListViewSettings(listViewSettings, sCategory);
	}

	/** Applies settings for susbystem manager.
	 */
	void applyFileSystemSettings( const FileSystemSettings &fileSystemSettings ) {
		m_subsystemMngr->applySettings(fileSystemSettings);
	}

	void showFindFilesDialog( FindFileDialog *pFindFileDialog, FileInfoToRowMap *pFileSelectionMap ) {
		m_subsystemMngr->showFindFilesDialog(pFindFileDialog, pFileSelectionMap);
	}
	void stopOfSearching() {
		m_pFileListView->slotBreakCurrentOperation();
	}
	void findFiles( const FindCriterion &fcFindCriterion, FileInfoToRowMap *pFoundFileSelectionMap ) {
		m_subsystemMngr->findFiles(fcFindCriterion, pFoundFileSelectionMap);
	}
	void pauseCurrentOperation( bool bPause ) {
		m_subsystemMngr->pauseCurrentOperation(bPause);
	}
	void jumpToFoundFile( FileInfo *pFileInfo ) {
		m_subsystemMngr->jumpToFoundFile(pFileInfo);
	}
	void removeFiles( FileInfoToRowMap *pSelectionMap, QWidget *pParent ) {
		m_subsystemMngr->removeFiles(pSelectionMap, pParent);
	}
	bool viewOfFoundFile( FileInfo *pFileInfo, bool bEditMode ) {
		return m_subsystemMngr->viewOfFoundFile(pFileInfo, bEditMode);
	}
	void setColumnWidthSynchro( bool bOn );

	QString getName() const {
		return m_pFileListView->objectName();
	}

	QString columnsWidthStr() const {
		return m_pFileListView->columnsWidthStr();
	}

private:
	void initPanelView();

	QString                    m_sContentType;

	QVBoxLayout               *m_pMainLayout;
	QString                    m_sPanelInitURIstr;
	PathView                  *m_pPathView;
	FileListView              *m_pFileListView;
	QWidget                   *m_pMainWnd;
	IconCache                 *m_pIconCache;
	StatusBar                 *m_statusBar;

	VfsFlatListModel          *m_vfsFLModel;
	VfsFlatListModel          *m_vfsFLModelCompl;
	VfsFlatListProxyModel     *m_proxyModel;
	VfsFlatListCompleterModel *m_complModel;
	VfsFlatListSelectionModel *m_selectionModel;

	SubsystemManager          *m_subsystemMngr;

	KeyShortcuts              *m_pKeyShortcuts;

	URI                        m_URI;
	bool                       m_firstListedStat;

private slots:
	void slotListLocation();

public slots:
	void slotPathViewPathChange( const URI &uri, bool bUpdateComplModel, bool bAddPathToHistory );

	void slotPopUpPathView()  { m_pPathView->popup();     }
	void slotActivePathView() { m_pPathView->setActive(); }
	void slotCloseConnection();

	void slotSetFileListViewActive();

};

#endif
