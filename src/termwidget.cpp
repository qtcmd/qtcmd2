/***************************************************************************
 *   Copyright (C) 2016 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#define DONT_START_SHELL 0
#define START_SHELL      1

#include <unistd.h>

#include <QDebug>
#include <QClipboard>
#include <QApplication>

// #include "mimetype.h"
// #include "systeminfohelper.h"
#include "settings.h"
#include "termwidget.h"


TermWidget::TermWidget( const QString &sWorkDir, QWidget *pParent, const QString &sShell )
	: QTermWidget(DONT_START_SHELL, pParent)
{
 	setObjectName("TermWidget");
	setAccessibleName("TermWidget");

	if (! sWorkDir.isEmpty()) {
		setWorkingDirectory(sWorkDir);
		n_sCurrentPath = sWorkDir;
	}
	QString sShellProg = sShell;
	if (! sShell.isEmpty())
		setShellProgram(sShell);
	else
		sShellProg = "bash"; // default setting in QTermWidget
	qDebug() << "TermWidget::TermWidget. objectName:" << objectName() << "WorkDir:" << sWorkDir << "shell:" << sShellProg;
	m_sShell = sShell;

	setFlowControlWarningEnabled(true);
	setMonitorActivity(true);

	loadSettings();
	setProperties();

	startShellProgram(); // call only when is uses: DONT_START_SHELL
// 	setFocus();
}

TermWidget::~TermWidget()
{
	qDebug() << "TermWidget::~TermWidget." << accessibleName();
	Settings settings;
	settings.update("Terminal/ColorScheme", m_sColorScheme);
	settings.update("Terminal/KeyBindings", m_sKeyBindings);
	settings.update("Terminal/HistorySize", QString("%1").arg(m_nHistorySize));
	settings.update("Terminal/Font", m_sFont);
}

void TermWidget::loadSettings()
{
	qDebug() << "TermWidget::loadSettings";
	Settings settings;
	m_sColorScheme = settings.value("Terminal/ColorScheme", "WhiteOnBlack").toString();
	QStringList slColorSchemes = availableColorSchemes();
	if (slColorSchemes.indexOf(m_sColorScheme) == -1)
		m_sColorScheme = "WhiteOnBlack";

	m_sKeyBindings = settings.value("Terminal/KeyBindings", "linux").toString();
	QStringList slKeyBindings = availableKeyBindings();
	if (slKeyBindings.indexOf(m_sKeyBindings) == -1)
		m_sKeyBindings = "linux";

	m_nHistorySize = settings.value("Terminal/HistorySize", 5000).toUInt();
	if (m_nHistorySize < 100)
		m_nHistorySize = 5000;

	m_sFont = settings.value("Terminal/Font", "Monospace,10").toString();
	n_sFontFamily = m_sFont.left(m_sFont.indexOf(','));
	m_nFontSize = m_sFont.right(m_sFont.length() - m_sFont.indexOf(',')).toUInt();
	if (m_nFontSize < 10 || m_nFontSize > 20)
		m_nFontSize = 10;
	m_font = qvariant_cast<QFont>(settings.value("Terminal/Font", n_sFontFamily));

//     m_font = qvariant_cast<QFont>(m_pSettings->value("Terminal/Font", "Monospace"));
	m_font.setPointSize(m_nFontSize);
	m_font.setBold(false);
}

void TermWidget::setProperties()
{
	qDebug() << "TermWidget::setProperties";
// 	QFont default_font = QApplication::font();
// 	default_font.setFamily(DEFAULT_FONT);
// 	default_font.setPointSize(10);
// 	default_font.setStyleHint(QFont::TypeWriter);

    QFont font = QApplication::font();
#ifdef Q_WS_MAC
    font.setFamily("Monaco");
#elif defined(Q_WS_QWS)
    font.setFamily("fixed");
#else
    font.setFamily(n_sFontFamily);
#endif
// 	font.setFamily("Oxygen mono");
	font.setPointSize(m_nFontSize);
	font.setBold(false);

	setTerminalFont(m_font);

	setHistorySize(m_nHistorySize);
	setColorScheme(m_sColorScheme);
    setKeyBindings(m_sKeyBindings);
	qDebug() << "TermWidget::setProperties. HistorySize:" << QString("%1").arg(m_nHistorySize) << "ColorScheme:" << m_sColorScheme << "KeyBindings:" << m_sKeyBindings;

    setScrollBarPosition(QTermWidget::ScrollBarRight);
}


void TermWidget::slotPathChanged( const QString &sPath )
{
	qDebug() << "TermWidget::slotPathChanged. Path:" << sPath;
	n_sCurrentPath = sPath;
	// TODO check if incoming path comes from local subsystem and this is not inside of archive or remote location
	changeDir("\""+sPath+"\"");
	emit signalPathChangedInTerminal(); // caught in MainWindow
}


void TermWidget::slotExeCommandInTerminal( const QString &sCmd )
{
	qDebug() << "TermWidget::slotExeCommandInTerminal. Execute cmd:" << sCmd;
	if (sCmd.trimmed() == "exit")
		return;

	sendText(sCmd+"\n");
//	"pwd | xsel -b"
// 	sendText("pwd | xsel\n"); // doesn't work
// 	sendText("pwd | xclip\n"); // doesn't work
	sendText("pwd | xclip -selection clipboard\n"); // a bit hacky, but works
// 	sendText("echo $PWD | xclip -selection clipboard\n"); // works
 	QTimer::singleShot(15, this, &TermWidget::slotGetCurrentPath);
}


bool TermWidget::shellIsWorking() // cannot be const due to getShellPID
{
	QString strCmd;
	strCmd.setNum(getShellPID());
	strCmd.prepend("ps -j ");
	strCmd.append(" | tail -1 | awk '{ print $5 }' | grep -q \\+ 2> /dev/null");
	int retval = system(strCmd.toStdString().c_str());

	return (! retval);
}

void TermWidget::slotGetCurrentPath()
{
	// a bit hacky, but works (sorry, I wasn't able to find better solution)
	QString sPath = QApplication::clipboard()->text();
	// TODO send commend to terminal cleaning current line  (Ctrl+U)
// 	sPath = selectedText(false); // doesn't work'
	n_sCurrentPath = sPath.remove("\n");
	qDebug() << "TermWidget::slotGetCurrentPath. Set CurrentPath:" << n_sCurrentPath;
	emit signalPathChangedInTerminal(); // caught in MainWindow
}

