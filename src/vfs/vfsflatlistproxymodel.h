/***************************************************************************
 *   Copyright (C) 2010 by Mariusz Borowski <b0mar@nes.pl>                 *
 *   Copyright (C) 2010 by Piotr Mierzwi�ski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef VFSFLATLISTPROXYMODEL_H
#define VFSFLATLISTPROXYMODEL_H

#include <QSortFilterProxyModel>

#include "vfsflatlistmodel.h"


/**
	@author Mariusz Borowski <b0mar@nes.pl>  // sorting
	@author Piotr Mierzwinski <piom@nes.pl>  // filtering
*/
class VfsFlatListProxyModel : public QSortFilterProxyModel
{
	Q_OBJECT
public:
	VfsFlatListProxyModel( QObject *parent=nullptr, bool bFindFileResultPanel=false );

	VfsFlatListModel *sourceModel() const { return static_cast <VfsFlatListModel *> (QSortFilterProxyModel::sourceModel()); }

	/**
	 * @brief Sets id for columns. Id equal < 0 means that column doesn't exist.
	 *
	 * @param colSizeId  column 'Size' id
	 * @param colTimeId  column 'Time' id
	 * @param colPermId  column 'Permission' id
	 * @param colOwnerId column 'Owner' id
	 * @param colGroupId column 'Group' id
	 **/
	void setColumnsId( int colSizeId, int colTimeId, int colPermId, int colOwnerId, int colGroupId );

	void setColumnsId( int colSizeId, int colPathId, int colTimeId, int colPermId, int colOwnerId, int colGroupId );

	/** Sets visibility of "hidden" items (where name starting with dot except double dots).
	 * @param bShowHidden if true then shows "hidden" items otherwise hide them
	 */
	void setShowHiddenFiles( bool bShowHidden ) { m_bShowHiddenFiles = bShowHidden; }

protected:
	virtual bool lessThan ( const QModelIndex &left, const QModelIndex &right ) const;

	virtual bool filterAcceptsRow( int sourceRow, const QModelIndex &sourceParent ) const;

private:
	void initSortKey( char *k, const FileInfo &fi, bool asc, int sortColumn ) const;

	/**
	 * @brief Check whether filter for name's column string matches to given name.
	 *
	 * @param sCurrentName name of current listed item
	 * @return true if name from filter contains into gived name otherwise false
	 **/
	bool containsName( const QString &sCurrentName ) const;

	/**
	 * @brief Preparing data to comparing by size. Calls filterCompare().
	 *
	 * @param currentSize size of current listed item
	 * @return return false if item matches otherwise true
	 **/
	bool sizeInRange( qint64 currentSize ) const;

	/**
	 * @brief Preparing data to comparing by date. Calls filterCompare().
	 *
	 * @param currentDateTime time of current listed item
	 * @return return false if item matches otherwise true
	 **/
	bool dateInRange( const QDateTime &currentDateTime ) const;

	/**
	 * @brief Compares permission from filter with current listed item.
	 *
	 * @param sCurrentPerm permission string (example: drwx)
	 * @return true if permission matched otherwise false
	 **/
	bool containsPerm( const QString &sCurrentPerm ) const;

	/**
	 * @brief Check whether filter for owner's' column string matches to given owner.
	 *
	 * @param sCurrentOwner owner of current listed item
	 * @return true if owner matches otherwise false
	 **/
	bool containsOwner( const QString &sCurrentOwner ) const;

	/**
	 * @brief Check whether filter for group's column string matches to given group.
	 *
	 * @param sCurrentGroup group of current listed item
	 * @return true if group matches otherwise false
	 **/
	bool containsGroup( const QString &sCurrentGroup ) const;

	/**
	 * @brief Check whether filter for path's column string matches to given path.
	 *
	 * @param sCurrentPath path of current listed item
	 * @return true if path matches otherwise false
	 **/
	bool containsPath( const QString &sCurrentPath ) const;

	/**
	 * @brief Helper function used to compare given values using given operator.
	 *
	 * @param currentValue listed value
	 * @param filterValue value from filter
	 * @param sCmpOperator compare operator
	 * @param topRangeForFilterValue top range value for filter (usable only for time comparnig)
	 * @return false if compare returns true otherwise true.
	 **/
	bool filterCompare( qint64 currentValue, qint64 filterValue, const QString& sCmpOperator, qint64 topRangeForFilterValue=0 ) const;


	const int m_columnNameId;
	int m_columnSizeId, m_columnTimeId, m_columnPermId, m_columnOwnerId, m_columnGroupId, m_columnPathId;
	QStringList m_timeFormatStrLst;

	Qt::CaseSensitivity    m_filterCaseSensitivity;
	QRegExp::PatternSyntax m_filterPatternSyntax;
	Qt::MatchFlags         m_filterWildcardMatch;

	bool m_bFindFileResultPanel;
	bool m_bShowHiddenFiles;


public slots:
	void slotSetRegExpParametersNameCol( QRegExp::PatternSyntax ps, Qt::CaseSensitivity cs, Qt::MatchFlags mf );

};

// ========================== VfsFlatListCompleterModel class declaration ==========================

class VfsFlatListCompleterModel : public QSortFilterProxyModel
{
public:
	VfsFlatListCompleterModel( QObject *pParent, VfsFlatListModel *pVfsFlatListModel );

	QVariant data( const QModelIndex &index, int role=Qt::EditRole ) const;

private:
	FileInfo *fileInfo( const QModelIndex &index ) const;

};


#endif // VFSFLATLISTPROXYMODEL_H
