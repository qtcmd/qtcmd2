/***************************************************************************
 *   Copyright (C) 2008 by Mariusz Borowski <b0mar@nes.pl>                 *
 *   Copyright (C) 2009 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDebug> // need to QDebug()

#include "mimetype.h"
#include "vfstreelistmodel.h"
#include "iconcache.h"


VfsTreeListModel::VfsTreeListModel(Subsystem * subsystem, IconCache * pIconCache)
	: m_subsystem(subsystem), m_rootNode(0), m_pIconCache(pIconCache)
{
	qDebug() << "VfsTreeListModel::VfsTreeListModel";
	Q_ASSERT(m_subsystem != 0);
	m_headerItems << tr("Name") << tr("Ext.") << tr("Size") << tr("Time") << tr("Permission") << tr("Owner") << tr("Group") << tr("File type");
	Q_ASSERT(NUMBER_OF_COLUMNS == m_headerItems.size());
	setRootNode(m_subsystem->root().filePath());
}

VfsTreeListModel::~VfsTreeListModel()
{
    if (m_rootNode)
        delete m_rootNode;
}

QVariant VfsTreeListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return m_headerItems.at(section);

    return QAbstractItemModel::headerData(section, orientation, role);
}

int VfsTreeListModel::columnCount(const QModelIndex & parent) const
{
    if (parent.column() > 0)
        return 0;

    return NUMBER_OF_COLUMNS;
}

int VfsTreeListModel::rowCount(const QModelIndex & parent) const
{
    if (parent.column() > 0)
        return 0;

    SubsystemTreeListNode *node = parent.isValid() ? static_cast<SubsystemTreeListNode*>(parent.internalPointer()) : m_rootNode;
    Q_ASSERT(node != 0);
    if (node->m_itemInfo.isDir() && ! node->m_populated)
        populate(node);

    return node->m_children.count();
}

QModelIndex VfsTreeListModel::parent(const QModelIndex & index) const
{
    if ( ! index.isValid() )
        return QModelIndex();

    SubsystemTreeListNode *childNode = static_cast<SubsystemTreeListNode*>(index.internalPointer());
    SubsystemTreeListNode *parentNode = childNode->m_parent;
    Q_ASSERT(parentNode != 0);
    if (parentNode == m_rootNode)
        return QModelIndex();

    SubsystemTreeListNode *p = parentNode->m_parent;
    Q_ASSERT(p != 0);
    const QVector<SubsystemTreeListNode*> children = p->m_children;
    Q_ASSERT(children.count() > 0);
    int row = children.indexOf(parentNode);
    Q_ASSERT(row >= 0);
    return createIndex(row, 0, parentNode);
}

QVariant VfsTreeListModel::data( const QModelIndex & index, int role ) const
{
	if (! index.isValid())
		return QVariant();

	SubsystemTreeListNode *node = static_cast<SubsystemTreeListNode*>(index.internalPointer());
	Q_ASSERT(node != 0);
	SubsystemFileInfo *pFileInfo = &node->m_itemInfo;

	// Qt4 engine call this every time when (mouse) cursor is over the item's list!

	if (role == Qt::DisplayRole || role == Qt::EditRole) {
		//qDebug() << "--- Display or Edit role, fileName=" << pFileInfo->fileName();
		switch (index.column()) {
			//case 0: return pFileInfo->isDir() ? "\0"+pFileInfo->fileName() : pFileInfo->fileName();
			case 0: return pFileInfo->fileName();
			case 1: return pFileInfo->fileExtension();
			case 2: {
				if (pFileInfo->isDir())
					return QLatin1String("<DIR>");
				else
					return pFileInfo->size();
			}
			case 3: return pFileInfo->lastModified();
			case 4: return pFileInfo->permission();
			case 5: return pFileInfo->owner();
			case 6: return pFileInfo->group();
			default:
				Q_ASSERT(false);
		}
	}

	if (index.column() == 0) {
		if (role == Qt::DecorationRole && m_pIconCache != NULL) {
			//qDebug() << "mimeinfo=" << pFileInfo->mimeInfo() << "mimeicon=" << MimeType::getMimeIcon(pFileInfo->mimeInfo());
			/*
			if (pFileInfo->isDir()) {
				//if (pFileInfo->isReadable())
				//	return m_pIconCache->icon("folder/locked");
				//else
					return m_pIconCache->icon("folder");
			}
			else {
				return m_pIconCache->icon(MimeType::getMimeIcon(pFileInfo->mimeInfo()));
			}
			*/
			return m_pIconCache->icon(MimeType::getMimeIcon(pFileInfo->mimeInfo()));
		}
	}

	return QVariant();
}

QModelIndex VfsTreeListModel::index(int row, int column, const QModelIndex & parent) const
{
    if (column < 0 || column >= NUMBER_OF_COLUMNS || row < 0 || parent.column() > 0)
        return QModelIndex();
    if ( ! hasIndex(row, column, parent) )
        return QModelIndex();

    SubsystemTreeListNode *p = parent.isValid() ? static_cast<SubsystemTreeListNode*>(parent.internalPointer()) : m_rootNode;
    Q_ASSERT(p != 0);
    if ( ! p->m_populated && p->m_itemInfo.isDir() )
        populate(p);

    if (row >= 0 && row < p->m_children.count()) {
        SubsystemTreeListNode *node = p->m_children.at(row);
        Q_ASSERT(node);
        return createIndex(row, column, node);
    }
    return QModelIndex();
}

bool VfsTreeListModel::hasChildren(const QModelIndex & parent) const
{
    if (parent.column() > 0)
        return false;

    SubsystemTreeListNode *p = parent.isValid() ? static_cast<SubsystemTreeListNode*>(parent.internalPointer()) : m_rootNode;

    return p->m_itemInfo.isDir() && rowCount(parent) > 0;
}


void VfsTreeListModel::populate(SubsystemTreeListNode *parent) const
{
	qDebug("VfsTreeListModel::populate(), file=%s", qPrintable(parent->m_itemInfo.fileName()));
    if (parent->m_itemInfo.fileName() == "..")
        return;

	// generate tree structure
	SubsystemFileInfoList lst = m_subsystem->listDirectory(parent->m_itemInfo.filePath());
    for (SubsystemFileInfoList::const_iterator it = lst.begin(); it != lst.end(); ++it)
    {
        //if ((*it).fileName() == "..")
          //  continue;
        SubsystemTreeListNode *node = new SubsystemTreeListNode(parent);
        node->m_itemInfo = *it;
        parent->m_children.append(node);
		//qDebug() << "node=" << node->m_itemInfo.fileName();
    }
    parent->m_populated = true;
    qDebug("VfsTreeListModel::populate(): %s, children: %d", qPrintable(parent->m_itemInfo.filePath()), parent->m_children.count());
}

void VfsTreeListModel::setRootNode(const QString & sPath)
{
	if (sPath == "/..") // PM
		return;
	qDebug("VfsTreeListModel::setRootNode(), path=%s", qPrintable(sPath));
	SubsystemTreeListNode *node = new SubsystemTreeListNode;
	if (node) {
//		if (path == "..") {
//			qDebug("VfsTreeListModel::setRootNode(), path= %s", qPrintable(m_prevNode->m_itemInfo.filePath()));
//		}
		node->m_itemInfo = m_subsystem->fileInfo(sPath);
		Q_ASSERT(node->m_itemInfo.isDir());
		populate(node);
		if (m_rootNode)
			delete m_rootNode;
		m_rootNode = node;
		reset();
	} else {
		qDebug("VfsTreeListModel:setRootNode(), create root node from %s failed!", qPrintable(sPath));
	}
}

/*void VfsTreeListModel::sort( int column, Qt::SortOrder order )
{
	qDebug() << "sort";
	//QAbstractItemModel::sort(column, order); // pusta
	QDir::SortFlags sort = QDir::DirsFirst | QDir::IgnoreCase;
	if (order == Qt::DescendingOrder)
		sort |= QDir::Reversed;
	//tr("Name") << tr("Ext.") << tr("Size") << tr("Time") << tr("Permission") << tr("Owner") << tr("Group");
	switch (column) {
		case 0: // name
		case 1: // ext
			sort |= QDir::Name;
			break;
		case 2: // size
			sort |= QDir::Size;
			break;
		case 3: // time
			sort |= QDir::Time;
			break;
		case 4: // permision
		case 5: // owner
		case 6: // group
			sort |= QDir::Name;
			break;
		default:
			break;
	}
}
*/

QString VfsTreeListModel::vfsSubsystemName( const QString & sFullFileName )
{
	QString sMimeType = MimeType::getFileMime(sFullFileName);
	QString sMimeIcon = MimeType::getMimeIcon(sMimeType);
	QString sMimeIcon2 = MimeType::getMimeIcon(sMimeType, true);
	// x-cd-image - for iso files
	// package-x-generic - for archive files
	QString sSubFileSystemName;

	// TODO find plugin related to mimetype
	qDebug() << "VfsTreeListModel::fileHasSubsystemSubsystem. File:" << sFullFileName << "has mimetype:" << sMimeType << "and mimeicon:" << sMimeIcon << "and mimeicon full:" << sMimeIcon2;
	qDebug() << "--" << (sSubFileSystemName.isEmpty() ? "Not found subfilesystem" : QString("Found subfilesystem: "+sSubFileSystemName));

	return sSubFileSystemName;
}

