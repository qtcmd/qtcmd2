/***************************************************************************
 *   Copyright (C) 2010 by Mariusz Borowski <b0mar@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _VFSMODEL_H
#define _VFSMODEL_H

#include <QDir>
#include <QAbstractItemModel>

#include "fileinfo.h"

class IconCache;

/**
    @author Piotr Mierzwinski <piom@nes.pl>
*/
class VfsModel : public QAbstractItemModel
{
public:
	VfsModel();
	virtual ~VfsModel() {}

	virtual QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;
	virtual int columnCount(const QModelIndex& parent = QModelIndex()) const;
	virtual int rowCount(const QModelIndex& parent = QModelIndex()) const;
	virtual QModelIndex parent(const QModelIndex& child) const;
	virtual QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const;

	virtual void setFileInfoList( const FileInfoList &fileInfoList );
    virtual void clearSelectionMap();

};

#endif // _VFSMODEL_H
