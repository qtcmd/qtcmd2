/***************************************************************************
 *   Copyright (C) 2017 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDebug>

#include "vfsflatlistselectionmodel.h"


VfsFlatListSelectionModel::VfsFlatListSelectionModel( const QString &sName, VfsFlatListModel *pSourceModel )
{
	m_sourceModel = pSourceModel;
	m_proxyModel  = nullptr;
	m_weighOfSelected = 0;

	m_selectionMap  = new FileInfoToRowMap;
	m_listOfSeleted = new FileInfoList;

	setObjectName(sName);
}

VfsFlatListSelectionModel::~VfsFlatListSelectionModel()
{
	m_selectionMap->clear();
	delete m_selectionMap;
	m_listOfSeleted->clear();
	delete m_listOfSeleted;
}


QVariant VfsFlatListSelectionModel::data( const QModelIndex &index, int role ) const
{
	if (role == Qt::ForegroundRole) {
		FileInfo *pFI = m_sourceModel->fileInfoPtr(index);

		if (m_selectionMap->contains(pFI))
			return selectedItemColor();

		if (pFI->isDir() && ! pFI->isSymLink() && ! pFI->isHidden())
			return commonItemColor();
		if (! pFI->isSymLink() && ! pFI->isHidden() && ! pFI->permissions().contains('x'))
			return commonItemColor();
		if (pFI->isHidden())
			return hiddenFileColor();
		if (pFI->isSymLink() && pFI->mimeInfo() == "symlink/broken")
			return brokenSymlinkColor();
		if (! pFI->isDir() && pFI->isSymLink())
			return symlinkColor();
		if (! pFI->isDir() && pFI->permissions().contains('x'))
			return executableFileColor();
	}

	return QVariant();
}


void VfsFlatListSelectionModel::getSelectionDetails( uint &files, uint &dirs, uint &symlinks ) const
{
	files = 0; dirs = 0; symlinks = 0;

	FileInfoToRowMap::const_iterator it = m_selectionMap->constBegin();
	while (it != m_selectionMap->constEnd()) {
		if (it.key()->isSymLink())
			symlinks++;
		else if (it.key()->isDir())
			dirs++;
		else
			files++;

		++it;
	}
}

QString VfsFlatListSelectionModel::selectedItemsMsg( bool bSelectedWordInMsg, bool bAddSumInfo ) const
{
	uint files = 0, dirs = 0, symlinks = 0;
	getSelectionDetails(files, dirs, symlinks);

	QString msg;
		 if (files > 0 && dirs == 0 && symlinks == 0)
		msg = QString(tr("%1 file(s)")).arg(files);
	else if (files == 0 && dirs > 0 && symlinks == 0)
		msg = QString(tr("%1 directory(ies)")).arg(dirs);
	else if (files == 0 && dirs == 0 && symlinks > 0)
		msg = QString(tr("%1 symlink(s)")).arg(symlinks);
	else if (files > 0 && dirs > 0 && symlinks == 0)
		msg = QString(tr("%1 file(s) and %2 directory(ies)")).arg(files).arg(dirs);
	else if (files > 0 && dirs > 0 && symlinks > 0)
		msg = QString(tr("%1 file(s), %2 directory(ies) and %3 symlink(s)")).arg(files).arg(dirs).arg(symlinks);
	else if (files > 0 && dirs == 0 && symlinks > 0)
		msg = QString(tr("%1 file(s) and %2 symlink(s)")).arg(files).arg(symlinks);
	else if (files == 0 && dirs > 0 && symlinks > 0)
		msg = QString(tr("%1 directory(ies) and %2 symlink(s)")).arg(dirs).arg(symlinks);

	if (bSelectedWordInMsg)
		msg += " selected";
	if (bAddSumInfo)
		msg += " (<b>"+QString(tr("%1 items")).arg(files + dirs + symlinks) + "</b>)";
	//msg += ".";

	return msg;
}

FileInfoList *VfsFlatListSelectionModel::listOfSeleted() const
{
	m_listOfSeleted->clear();

	FileInfoToRowMap::const_iterator it = m_selectionMap->constBegin();
	while (it != m_selectionMap->constEnd()) {
		m_listOfSeleted->append(it.key());
		++it;
	}

	return m_listOfSeleted;
}


// --- SELECTION METHODS

void VfsFlatListSelectionModel::clear()
{
	m_weighOfSelected = 0;

	if (m_selectionMap->size() > 0)
		m_selectionMap->clear();

	m_sourceModel->flmEndResetModel(); // to redraw ListView
	emit signalSelectionChanged();
}
/*
bool VfsFlatListSelectionModel::removeSelection( FileInfoList *pFileInfoList )
{
	if (pFileInfoList == nullptr)
		return false;
	int rows = pFileInfoList->size();
	if (rows == 0)
		return false;
	qDebug() << "VfsFlatListSelectionModel::removeSelection. processedItems size" << rows;

	for (int i=rows - 1; i >= 0; --i)
		setSelected(pFileInfoList->at(i), false);

	return true;
}
*/
FileInfo *VfsFlatListSelectionModel::firstMarked() const
{
	if (m_selectionMap->size() > 0)
		return m_selectionMap->constBegin().key();

// 	qDebug() << "VfsFlatListSelectionModel::firstMarked. WARNING. Empty selection list";
	return nullptr;
}

int VfsFlatListSelectionModel::markIfNoOneMarked( const QModelIndex &index )
{
	int nNumOfSelected = m_selectionMap->size();
	if (nNumOfSelected == 0) {
// 		QModelIndex currentIndex = static_cast <const VfsFlatListProxyModel *>(index.model())->mapToSource(index);
// 		QModelIndex currentIndex = currentModelIndex(); // was: m_tvFileList->currentIndex();
		if (index.data().toString() == "..")
			return 0;
		else {
			setSelected(index, true);
			return 1;
		}
	}

	return nNumOfSelected;
}

bool VfsFlatListSelectionModel::selected( const QModelIndex &index ) const
{
	Q_ASSERT(index.isValid() && index.row() < index.model()->rowCount());
	if (m_sourceModel == nullptr)
		return false;

// 	qDebug() << "VfsFlatListSelectionModel::selected. incoming index.model:" << index.model() << "item name:" << index.data().toString() << "row:" << index.row();
	QModelIndex srcIndex = static_cast <const VfsFlatListProxyModel *>(index.model())->mapToSource(index);
// 	qDebug() << "VfsFlatListSelectionModel::selected. srcIndex.model:" << srcIndex.model() << "name:" << srcIndex.data().toString() << "row:" << srcIndex.row();

	FileInfo *pFI = m_sourceModel->fileInfoPtr(srcIndex);
// 	qDebug() << "VfsFlatListSelectionModel::selected. FileInfo name" << pFI->fileName() << "selected:" << m_selectionMap->contains(pFI);
	return (m_selectionMap->contains(pFI));
}

bool VfsFlatListSelectionModel::setSelected( const QModelIndex &index, bool bMark )
{
	if (! index.isValid()) {
		qDebug() << "VfsFlatListSelectionModel::setSelected. Not found given index:" << index;
		return false;
	}
	if (index.data().toString() == "..")
		return true;

// 	qDebug() << "VfsFlatListSelectionModel::setSelected. indexMmodel:" << index.model() << "name:" << index.data().toString() << "mark:" << bMark;
	FileInfo *pFI = m_sourceModel->fileInfoPtr(index);
// 	qDebug() << "VfsFlatListSelectionModel::setSelected. matching FileInfoName" << pFI->fileName();

	if (m_selectionMap->contains(pFI) == bMark) // doesn't mark already marked
		return true;

	if (bMark)
		m_selectionMap->insert(pFI, index.row());
	else
		m_selectionMap->remove(pFI);

    m_weighOfSelected += m_sourceModel->fileSize(pFI) * (bMark ? 1 : -1);

	emit signalSelectionChanged();

	return true;
}

bool VfsFlatListSelectionModel::setSelected( FileInfo *pFileInfo, bool bMark, bool bLookingForName, bool bForceUnmark, bool updateWeigh )
{
	if (pFileInfo == nullptr) {
		qDebug() << "VfsFlatListSelectionModel::setSelected. Empty given FileInfo";
		return false;
	}
	if (pFileInfo->fileName() == "..")
		return true;

	FileInfo *pFI = pFileInfo;
	bool bAlreadyMarked;
	int row = -1;

	if ((bAlreadyMarked=m_selectionMap->contains(pFileInfo)) == bMark && ! bForceUnmark) // doesn't mark yet marked
		return true;

	if (bLookingForName) {
		bool bCheckPath = m_sourceModel->findFileResultMode();
// 		qDebug() << "-- looking for name:" << pFileInfo->fileName();
		row = m_selectionMap->value(pFileInfo, -1);
		//row = m_fileInfoList.indexOf(pFileInfo); // old
		if (row == -1)
			pFI = m_sourceModel->fileInfoPtr(pFileInfo->fileName(), (bCheckPath ? pFileInfo->location() : ""));
			//pFileInfo = fileInfoPtr(pFileInfo->fileName(), (bCheckPath ? pFileInfo->location() : "")); // old. Why input pointer is overwrote?
		if (pFileInfo == nullptr) {
			qDebug() << "VfsFlatListSelectionModel::setSelected. Not found item name from given fileInfo";
			return false;
		}
	}

	if (bMark)
		m_selectionMap->insert(pFI, row);
	else
		m_selectionMap->remove(pFI);

	if ((m_weighOfSelected == 0 && ! bMark) || ! updateWeigh) { // prevent for decrease less than 0
		emit signalSelectionChanged();
		return true;
	}

	m_weighOfSelected += m_sourceModel->fileSize(pFI) * (bMark ? 1 : -1);

	emit signalSelectionChanged();
	return true;
}

void VfsFlatListSelectionModel::setSelected( FileInfoList *pFileInfoList, bool bMark, bool bLookingForName, bool bForceUnmark, bool bUpdateWeigh )
{
	for (int id = 0; id < pFileInfoList->count(); ++id) {
		qDebug() << "VfsFlatListSelectionModel::setSelected (FI list)." << "file:" << pFileInfoList->at(id)->fileName() << " mark as:" << bMark << " in obj.:" << objectName();
		setSelected(pFileInfoList->at(id), bMark, bLookingForName, bForceUnmark, bUpdateWeigh);
	}
}

void VfsFlatListSelectionModel::inverseIndexSelection( const QModelIndex &index )
{
	if (index.data().toString() == "..")
		return;

	QModelIndex srcIndex = static_cast <const VfsFlatListProxyModel *>(index.model())->mapToSource(index);
// 	qDebug() << "VfsFlatListSelectionModel::inverseIndexSelection. srcIndex - model:" << srcIndex.model() << "name:" << srcIndex.data().toString() << "row:" << srcIndex.row();

	FileInfo *pFI = m_sourceModel->fileInfoPtr(srcIndex);
	bool bItemMarked = m_selectionMap->contains(pFI);
// 	qDebug() << "VfsFlatListSelectionModel::inverseIndexSelection. FI name:" << pFI->fileName();
	if (bItemMarked)
		m_selectionMap->remove(pFI);
	else
		m_selectionMap->insert(pFI, srcIndex.row());

    m_weighOfSelected += m_sourceModel->fileSize(pFI) * (bItemMarked ? -1 : 1);
// 	qDebug() << "VfsFlatListSelectionModel::inverseIndexSelection. FI name:" << pFI->fileName() << "selected:" << m_selectionMap->contains(pFI);
	emit signalSelectionChanged();
}

void VfsFlatListSelectionModel::inverseSelection()
{
	if (m_proxyModel == nullptr || m_sourceModel == nullptr) {
		qDebug() << "VfsFlatListSelectionModel::inverseIndexSelection. proxyModel or sourceModel not set";
		return;
	}
	//const VfsFlatListProxyModel *pProxyModel = static_cast <const VfsFlatListProxyModel *>(m_tvFileList->currentIndex().model());
	FileInfoList fileInfoList = m_sourceModel->fileInfoList(); // all items - not filtered and not sorted
	bool bMarkDirs = m_sourceModel->selectDirs();
	QModelIndex srcIndex, proxyIndex;
	bool bItemMarked;
	FileInfo *pFI;

	m_sourceModel->flmBeginResetModel();

	for (int row = 0; row < m_proxyModel->rowCount(); ++row) {
		proxyIndex = m_proxyModel->index(row, 0);
		srcIndex   = static_cast <const VfsFlatListProxyModel *>(proxyIndex.model())->mapToSource(proxyIndex);

		pFI = fileInfoList[srcIndex.row()];

		if (! bMarkDirs) {
			if (pFI->isDir())
				continue;
		}
		if (pFI->fileName() == "..")
			continue;

		bItemMarked = m_selectionMap->contains(pFI);
		if (bItemMarked)
			m_selectionMap->remove(pFI);
		else
			m_selectionMap->insert(pFI, srcIndex.row());

        m_weighOfSelected += m_sourceModel->fileSize(pFI) * (bItemMarked ? -1 : 1);
	}

	m_sourceModel->flmEndResetModel();
	emit signalSelectionChanged();
}

void VfsFlatListSelectionModel::selectAll( bool bMark )
{
	m_weighOfSelected = 0;
	m_selectionMap->clear();

	if (! bMark) {
		m_sourceModel->flmEndResetModel();
		emit signalSelectionChanged();
		return;
	}
	// -- mark all
	FileInfoList fileInfoList = m_sourceModel->fileInfoList();
	bool bMarkDirs = m_sourceModel->selectDirs();
	QModelIndex srcIndex, proxyIndex;
	FileInfo *pFI;

	m_sourceModel->flmBeginResetModel();

	for (int row = 0; row < m_proxyModel->rowCount(); ++row) {
		proxyIndex = m_proxyModel->index(row, 0);
		srcIndex   = static_cast <const VfsFlatListProxyModel *>(proxyIndex.model())->mapToSource(proxyIndex);

		pFI = fileInfoList[srcIndex.row()];

		if (! bMarkDirs) {
			if (pFI->isDir())
				continue;
		}
		if (pFI->fileName() == "..")
			continue;

		m_selectionMap->insert(pFI, srcIndex.row());
		m_weighOfSelected += m_sourceModel->fileSize(pFI);
	}

	m_sourceModel->flmEndResetModel();
	emit signalSelectionChanged();
}

void VfsFlatListSelectionModel::selectWithExtention( bool bMark, const QString &sExtention )
{
	m_weighOfSelected = 0;

	QString sFileName, sFoundExtension;
	QModelIndex srcIndex, proxyIndex;
	QStringList lst;
	bool bItemMarked;
	FileInfo *pFI;

	bool bMarkDirs = m_sourceModel->selectDirs();
	m_sourceModel->flmBeginResetModel();

	FileInfoList fileInfoList = m_sourceModel->fileInfoList();

	for (int row = 0; row < m_proxyModel->rowCount(); ++row) {
		proxyIndex = m_proxyModel->index(row, 0);
		srcIndex   = static_cast <const VfsFlatListProxyModel *>(proxyIndex.model())->mapToSource(proxyIndex);

		pFI = fileInfoList[srcIndex.row()];

		if (! bMarkDirs) {
			if (pFI->isDir())
				continue;
		}
		sFileName = pFI->fileName();
		if (sFileName == "..")
			continue;
		// -- get extention string // TODO check for files with tar.gz and only .gz
		lst = sFileName.split(".");
		sFoundExtension = (lst.size() > 1) ? lst.at(lst.size()-1) : "";
		// -- mark/unmark item
		if (sExtention == sFoundExtension) {
			bItemMarked = m_selectionMap->contains(pFI);

			if (bMark) {
				if (! bItemMarked)
					m_selectionMap->insert(pFI, srcIndex.row());

//				m_selectionWeigh += m_sourceModel->fileSize(pFI);
			}
			else {
				if (bItemMarked)
					m_selectionMap->remove(pFI);
			}
            m_weighOfSelected += m_sourceModel->fileSize(pFI) * (bMark ? 1 : -1);
		}
	}

	m_sourceModel->flmEndResetModel();
	emit signalSelectionChanged();
}

void VfsFlatListSelectionModel::selectByPattern( bool bMark, const QString &sPatterns, bool bUseRegExp )
{
	QRegExp regExp;
	regExp.setCaseSensitivity(bUseRegExp ? Qt::CaseSensitive : Qt::CaseInsensitive);
	regExp.setPatternSyntax(bUseRegExp ? QRegExp::RegExp : QRegExp::Wildcard);

	QStringList slPattersList = sPatterns.split('|');
	foreach(QString sPattern, slPattersList ) {
		regExp.setPattern(sPattern);
		selectByPattern_Processing(bMark, regExp);
	}
}

void VfsFlatListSelectionModel::selectByPattern_Processing( bool bMark, const QRegExp &pattern )
{
	QModelIndex srcIndex, proxyIndex;
	QString sFileName;
	bool bItemMarked;
	FileInfo *pFI;

	bool bMarkDirs = m_sourceModel->selectDirs();
	m_sourceModel->flmBeginResetModel();

	FileInfoList fileInfoList = m_sourceModel->fileInfoList(); // TODO: Instead of FileInfoList fileInfoList pass here pointer to FileInfoList

	for (int row = 0; row < m_proxyModel->rowCount(); ++row) {
		proxyIndex = m_proxyModel->index(row, 0);
		srcIndex   = static_cast <const VfsFlatListProxyModel *>(proxyIndex.model())->mapToSource(proxyIndex);

		pFI = fileInfoList[srcIndex.row()];

		if (! bMarkDirs) {
			if (pFI->isDir())
				continue;
		}
		sFileName = pFI->fileName();
		if (sFileName == "..")
			continue;
		// -- mark/unmark item
		if (pattern.exactMatch(sFileName)) {
			bItemMarked = m_selectionMap->contains(pFI);

			if (bMark) {
				if (! bItemMarked)
					m_selectionMap->insert(pFI, srcIndex.row());

//				m_selectionWeigh += m_sourceModel->fileSize(pFI);
			}
			else {
				if (bItemMarked)
					m_selectionMap->remove(pFI);
			}
			m_weighOfSelected += m_sourceModel->fileSize(pFI) * (bMark ? 1 : -1);
		}
	}

	m_sourceModel->flmEndResetModel();
	emit signalSelectionChanged();
}

void VfsFlatListSelectionModel::selectRange( const QModelIndex &fromIn, const QModelIndex &toIn, TreeView *pTreeView )
{
	if (! fromIn.isValid() || ! toIn.isValid())
		return;
// 	qDebug() << "VfsFlatListSelectionModel::selectRange. fromIn:" << fromIn << "row:" << fromIn.row() << "toIn:" << toIn << "row:" << toIn.row();

	int row;
	const VfsFlatListProxyModel *pProxyModel = static_cast <const VfsFlatListProxyModel *>(fromIn.model());
	QModelIndex newId = pProxyModel->mapToSource(fromIn);
	QModelIndex to    = pProxyModel->mapToSource(toIn);

	if (fromIn.row() > toIn.row()) { // Swap
		QModelIndex temp = newId;
		newId = to;
		to = temp;
	}

	FileInfoList fileInfoList = m_sourceModel->fileInfoList(); // FIXME after usage of filter all items are seleted not only filtered
	FileInfo *pFI;

	while (newId != to) {
		row = newId.row();
		if (row < 0)
			break;
// 		qDebug() << "VfsFlatListSelectionModel::selectRange. newId:" << newId << "row:" << row;
		pFI = fileInfoList[row];

// 		qDebug() << "mark/unmark item:" << m_fileInfoList[row]->fileName();
		if (m_selectionMap->contains(pFI)) // ItemMarked?
			m_selectionMap->remove(pFI);
		else
			m_selectionMap->insert(pFI, row);

		newId = pTreeView->indexBelow(pProxyModel->mapFromSource(newId));

		if (newId.row() != -1)
			newId = pProxyModel->mapToSource(newId);
	}
	// mark/unmark last item
	row = newId.row();
	if (newId.row() != -1) {
// 		qDebug() << "VfsFlatListSelectionModel::selectRange. newId:" << newId << "row:" << row;
		pFI = fileInfoList[row];

		if (m_selectionMap->contains(pFI)) // ItemMarked?
			m_selectionMap->remove(pFI);
		else
			m_selectionMap->insert(pFI, row);

		pTreeView->setCurrentIndex(newId);
	}

	m_sourceModel->flmEndResetModel();
	emit signalSelectionChanged();
}

