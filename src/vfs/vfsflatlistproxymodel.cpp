/***************************************************************************
 *   Copyright (C) 2010 by Mariusz Borowski <b0mar@nes.pl>                 *
 *   Copyright (C) 2010 by Piotr Mierzwi�ski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDebug> // need to qDebug()
#include <QDateTime>
#include <QDir>

#include "vfsflatlistproxymodel.h"


VfsFlatListProxyModel::VfsFlatListProxyModel( QObject *parent, bool bFindFileResultPanel )
	: QSortFilterProxyModel(parent), m_columnNameId(0),
	  m_columnSizeId(1), m_columnTimeId(2), m_columnPermId(3), m_columnOwnerId(4), m_columnGroupId(5) // default ids
{
	m_timeFormatStrLst << "yyyy" << "yyyy HH" << "yyyy HH:mm" << "yyyy-MM" << "yyyy-MM HH" << "yyyy-MM HH:mm" << "yyyy-MM-dd" << "yyyy-MM-dd HH" << "yyyy-MM-dd HH:mm" << "yyyy-MM-dd HH:mm:ss";
	m_bFindFileResultPanel = bFindFileResultPanel;
	m_columnPathId = -1;

	m_bShowHiddenFiles = true;
}

void VfsFlatListProxyModel::setColumnsId( int colSizeId, int colTimeId, int colPermId, int colOwnerId, int colGroupId )
{
	if (colSizeId < 0 || colTimeId < 0 || colPermId < 0 || colOwnerId < 0 || colGroupId < 0) {
		qDebug() << "VfsFlatListProxyModel::setColumnsId. parameter < 0";
		return;
	}
	m_columnSizeId  = colSizeId;
	m_columnTimeId  = colTimeId;
	m_columnPermId  = colPermId;
	m_columnOwnerId = colOwnerId;
	m_columnGroupId = colGroupId;
}

void VfsFlatListProxyModel::setColumnsId( int colSizeId, int colPathId, int colTimeId, int colPermId, int colOwnerId, int colGroupId )
{
	if (colPathId < 0) {
		qDebug() << "VfsFlatListProxyModel::setColumnsId. parameter < 0";
		return;
	}
	m_columnPathId = colPathId;
	setColumnsId(colSizeId, colTimeId, colPermId, colOwnerId, colGroupId);
}


void VfsFlatListProxyModel::initSortKey( char *k, const FileInfo &fi, bool asc, int sortColumn ) const
{
	if (asc) {
		k[0] = fi.fileName() == ".." ? '0' : '1';
		k[1] = fi.isDir() ? '0' : '1';
	} else {
		k[0] = fi.fileName() == ".." ? '1' : '0';
		k[1] = fi.isDir() ? '1' : '0';
	}
	k[2] = fi.isHidden() ? '0' : '1';
	if ((1 == sortColumn && ! fi.isDir()) || 2 == sortColumn)
		k[2] = '0';
}

bool VfsFlatListProxyModel::lessThan( const QModelIndex &left, const QModelIndex &right ) const
{
	char lf[] = "0000";
	char rt[] = "0000";
	const FileInfo &lfi = sourceModel()->fileInfo(left);
	const FileInfo &rfi = sourceModel()->fileInfo(right);
// 	const FileInfo &lfi = static_cast<VfsFlatListModel*>(sourceModel())->fileInfo(left);
// 	const FileInfo &rfi = static_cast<VfsFlatListModel*>(sourceModel())->fileInfo(right);

	bool isAscOrder = sortOrder() == Qt::AscendingOrder;
	int  col = sortColumn();
	initSortKey(lf, lfi, isAscOrder, col);
	initSortKey(rt, rfi, isAscOrder, col);

	QVariant ld = sourceModel()->data(left);
	QVariant rd = sourceModel()->data(right);
	qint64  lfiSize = lfi.isSymLink() ? lfi.symLinkTarget().length() : lfi.size();
	qint64  rfiSize = rfi.isSymLink() ? rfi.symLinkTarget().length() : rfi.size();
	bool isLessThan = (col == 1 ? lfiSize < rfiSize : QString::localeAwareCompare(ld.toString(), rd.toString()) < 0);
	if (isLessThan) {
		lf[3] = '0';
		rt[3] = '1';
	} else {
		lf[3] = '1';
		rt[3] = '0';
	}

	return qstrcmp(lf, rt) < 0;
}


bool VfsFlatListProxyModel::filterAcceptsRow( int sourceRow, const QModelIndex &sourceParent ) const
{
	QModelIndex indexName  = sourceModel()->index(sourceRow, m_columnNameId,  sourceParent);
	QModelIndex indexSize  = sourceModel()->index(sourceRow, m_columnSizeId,  sourceParent);
	QModelIndex indexTime  = sourceModel()->index(sourceRow, m_columnTimeId,  sourceParent);
	QModelIndex indexPerm  = sourceModel()->index(sourceRow, m_columnPermId,  sourceParent);
	QModelIndex indexOwner = sourceModel()->index(sourceRow, m_columnOwnerId, sourceParent);
	QModelIndex indexGroup = sourceModel()->index(sourceRow, m_columnGroupId, sourceParent);

	const FileInfo &fi = (m_columnSizeId > 0) ? sourceModel()->fileInfo(indexSize) : FileInfo();

	bool bResult;
	if (m_bFindFileResultPanel) {
		QModelIndex indexPath  =  sourceModel()->index(sourceRow, m_columnPathId, sourceParent);
		bResult = (containsName(sourceModel()->data(indexName).toString())
			&& ((m_columnSizeId > 0)  ? sizeInRange(fi.isDir() ? -1 : fi.size()) : true)
			&& ((m_columnPathId > 0)  ? containsPath(sourceModel()->data(indexPath).toString())   : true)
			&& ((m_columnTimeId > 0)  ? dateInRange(sourceModel()->data(indexTime).toDateTime())  : true)
			&& ((m_columnPermId > 0)  ? containsPerm(sourceModel()->data(indexPerm).toString())   : true)
			&& ((m_columnOwnerId > 0) ? containsOwner(sourceModel()->data(indexOwner).toString()) : true)
			&& ((m_columnGroupId > 0) ? containsGroup(sourceModel()->data(indexGroup).toString()) : true)
		);
	}
	else {
		if (! m_bShowHiddenFiles) {
			QString sName = sourceModel()->data(indexName).toString();
			bResult = (sName.startsWith(".") && sName != "..");
			bResult = ! bResult;
		}
		else
			bResult = (containsName(sourceModel()->data(indexName).toString())
				&& ((m_columnSizeId > 0)  ? sizeInRange(fi.isDir() ? -1 : fi.size()) : true)
				&& ((m_columnTimeId > 0)  ? dateInRange(sourceModel()->data(indexTime).toDateTime())  : true)
				&& ((m_columnPermId > 0)  ? containsPerm(sourceModel()->data(indexPerm).toString())   : true)
				&& ((m_columnOwnerId > 0) ? containsOwner(sourceModel()->data(indexOwner).toString()) : true)
				&& ((m_columnGroupId > 0) ? containsGroup(sourceModel()->data(indexGroup).toString()) : true)
			);
	}

	return bResult;
}

bool VfsFlatListProxyModel::containsName( const QString &sCurrentName ) const
{
	QString sPattern = filterRegExp().pattern();
	if (sPattern.isEmpty())
		return true;

	QString sFilterValue;
	if ((unsigned int)sPattern.count('|') > 0)
		sFilterValue = sPattern.left(sPattern.indexOf('|'));
	else
		sFilterValue = sPattern;

	QRegExp regExp(sFilterValue);
	regExp.setCaseSensitivity(m_filterCaseSensitivity);
	regExp.setPatternSyntax(m_filterPatternSyntax);

	if (m_filterWildcardMatch == Qt::MatchStartsWith && m_filterPatternSyntax == QRegExp::Wildcard) {
		QString startCurrName = sCurrentName.left(sPattern.length());
		return startCurrName.contains(regExp);
	}

	return sCurrentName.contains(regExp);
}

bool VfsFlatListProxyModel::sizeInRange( qint64 currentSize ) const
{
	QString sPattern = filterRegExp().pattern();
	if (sPattern.isEmpty())
		return true;

	if (sPattern.count('|') < m_columnSizeId)
		return true;

	QStringList columnLst    = sPattern.split('|');
	QString     sFilterValue = columnLst[m_columnSizeId];

	if (sFilterValue == "*")
		return true;
	if (currentSize < 0 && sFilterValue != "<DIR>")
		return false;
	if (currentSize < 0 && sFilterValue == "<DIR>") // filtering dirs.
		return true;

	int     nDigitId     = sFilterValue.indexOf(QRegExp("\\d"));
	QString sCmpOperator = sFilterValue.left(nDigitId).replace(' ', "");
	sFilterValue = sFilterValue.right(sFilterValue.length() - nDigitId).replace(QRegExp("[A-Z ]*"), "");

	const quint64 kb = 1024;
	const quint64 mb = 1024 * kb;
	const quint64 gb = 1024 * mb;
	const quint64 tb = 1024 * gb;

	qlonglong filterValue = sFilterValue.toLongLong();
	if (sPattern.contains(tr("KB")))
		filterValue *= kb;
	else
	if (sPattern.contains(tr("MB")))
		filterValue *= mb;
	else
	if (sPattern.contains(tr("GB")))
		filterValue *= gb;
	else
	if (sPattern.contains(tr("TB")))
		filterValue *= tb;

	return ! filterCompare(currentSize, filterValue, sCmpOperator);
}


bool VfsFlatListProxyModel::dateInRange( const QDateTime &currentDateTime ) const
{
	QString sPattern = filterRegExp().pattern();
	if (sPattern.isEmpty())
		return true;

	if (sPattern.count('|') < m_columnTimeId)
		return true;

	QStringList columnLst    = sPattern.split('|');
	QString     sFilterValue = columnLst[m_columnTimeId];
	if (sFilterValue == "*")
		return true;
	int     nDigitId = sFilterValue.indexOf(QRegExp("\\d"));
	QString sCmpOperator = sFilterValue.left(nDigitId).replace(' ', "");
	sFilterValue = sFilterValue.right(sFilterValue.length() - nDigitId);

	bool bTimeValid = false;
	qint64 filterValue = 0, topRangeForFilterValue = 0;
	QString sTimeFormat, rightPartTimeFormat;
	QDateTime dateTime, topRangeForFilter;
	for (int j=0; j<m_timeFormatStrLst.count(); ++j) {
		sTimeFormat = m_timeFormatStrLst[j];
		dateTime = QDateTime::fromString(sFilterValue, sTimeFormat);
		topRangeForFilter = dateTime;
		if (sCmpOperator.contains('=')) {
			rightPartTimeFormat = sTimeFormat.right(2);
			if (rightPartTimeFormat == "yy")
				topRangeForFilter = topRangeForFilter.addYears(1);
			else if (rightPartTimeFormat == "MM")
				topRangeForFilter = topRangeForFilter.addMonths(1);
			else if (rightPartTimeFormat == "dd")
				topRangeForFilter = topRangeForFilter.addDays(1);
			else if (rightPartTimeFormat == "HH")
				topRangeForFilter = topRangeForFilter.addSecs(3600);
			else if (rightPartTimeFormat == "mm")
				topRangeForFilter = topRangeForFilter.addSecs(60);
		}
		if ((bTimeValid=dateTime.isValid())) {
			filterValue = dateTime.toTime_t();
			topRangeForFilterValue = topRangeForFilter.toTime_t();
			break;
		}
	}

	return ! (filterCompare(currentDateTime.toTime_t(), filterValue, sCmpOperator, topRangeForFilterValue) && bTimeValid);
}

bool VfsFlatListProxyModel::filterCompare( qint64 currentValue, qint64 filterValue, const QString &sCmpOperator, qint64 topRangeForFilterValue ) const
{
	//qDebug() << "VfsFlatListProxyModel::filterCompare. filterRegExp():" << filterRegExp() << "currentValue:" << currentValue << "filterValue:" << filterValue << "sCmpOperator:" << sCmpOperator;
	if (sCmpOperator == "=" || sCmpOperator == "==") {
		if (topRangeForFilterValue > 0) { // time (without seconds) arrive
			if (currentValue >= filterValue && currentValue < topRangeForFilterValue)
				return false;
		}
		else
		if (currentValue == filterValue)  return false;
		//return !(currentValue == filterValue);
	}
	else
	if (sCmpOperator == "<") {
		if (currentValue < filterValue)   return false;
	}
	else
	if (sCmpOperator == ">") {
		if (currentValue > filterValue)   return false;
	}
	else
	if (sCmpOperator == "!=" || sCmpOperator == "<>") {
		if (currentValue != filterValue)  return false;
	}
	else
	if (sCmpOperator == "<=" || sCmpOperator == "=<") {
		if (topRangeForFilterValue > 0) { // time (without seconds) arrive
			if (currentValue <= topRangeForFilterValue)
				return false;
		}
		else
		if (currentValue <= filterValue)  return false;
	}
	else
	if (sCmpOperator == ">=" || sCmpOperator == "=>") {
		if (currentValue >= filterValue)  return false;
	}

	return true;
}


bool VfsFlatListProxyModel::containsPerm( const QString &sCurrentPerm ) const
{
	QString sPattern = filterRegExp().pattern();
	if (sPattern.isEmpty())
		return true;

	if (sPattern.count('|') < m_columnPermId)
		return true;

	QStringList columnLst    = sPattern.split('|');
	QString     sFilterValue = columnLst[m_columnPermId];

	if (sFilterValue == "*")
		return true;

	sFilterValue.replace("?", ".");
	sFilterValue.insert(0, "^");
	if (! sCurrentPerm.contains(QRegExp(sFilterValue)))
		return false;

	return true;
}

bool VfsFlatListProxyModel::containsOwner( const QString &sCurrentOwner ) const
{
	QString sPattern = filterRegExp().pattern();
	if (sPattern.isEmpty())
		return true;

	if (sPattern.count('|') < m_columnOwnerId)
		return true;

	QStringList columnLst    = sPattern.split('|');
	QString     sFilterValue = columnLst[m_columnOwnerId];

	if (sFilterValue != sCurrentOwner)
		return false;

	return true;
}

bool VfsFlatListProxyModel::containsGroup( const QString &sCurrentGroup ) const
{
	QString sPattern = filterRegExp().pattern();
	if (sPattern.isEmpty())
		return true;

	if (sPattern.count('|') < m_columnGroupId)
		return true;

	QStringList columnLst    = sPattern.split('|');
	QString     sFilterValue = columnLst[m_columnGroupId];

	if (sFilterValue != sCurrentGroup)
		return false;

	return true;
}

bool VfsFlatListProxyModel::containsPath( const QString &sCurrentPath ) const
{
	QString sPattern = filterRegExp().pattern();
	if (sPattern.isEmpty())
		return true;

	if (sPattern.count('|') < m_columnPathId)
		return true;

	QStringList columnLst    = sPattern.split('|');
	QString     sFilterValue = columnLst[m_columnPathId];

	if (sFilterValue != sCurrentPath)
		return false;

	return true;
}


void VfsFlatListProxyModel::slotSetRegExpParametersNameCol( QRegExp::PatternSyntax ps, Qt::CaseSensitivity cs, Qt::MatchFlags mf )
{
	m_filterPatternSyntax   = ps;
	m_filterCaseSensitivity = cs;
	m_filterWildcardMatch   = mf;
}

// ========================== VfsFlatListCompleterModel class implementation ==========================

VfsFlatListCompleterModel::VfsFlatListCompleterModel( QObject *pParent, VfsFlatListModel *pVfsFlatListModel )
	: QSortFilterProxyModel(pParent)
{
	setSourceModel(pVfsFlatListModel);
}

FileInfo * VfsFlatListCompleterModel::fileInfo( const QModelIndex &index ) const
{
	Q_ASSERT(index.isValid());
	FileInfoList fileInfoList = (static_cast <VfsFlatListModel *> (QSortFilterProxyModel::sourceModel()))->fileInfoList();
	if (index.row() >= fileInfoList.size())
		return nullptr;

 	return fileInfoList.at(index.row()); // only after first sorting (QDir obj) work correct!!!
}

QVariant VfsFlatListCompleterModel::data( const QModelIndex &index, int role ) const
{
	if (index.column() == 0 && role == Qt::EditRole) {
		FileInfo *fi = fileInfo(index);
		if (fi != nullptr && fi->fileName() != ".." && fi->isDir())
			return QVariant(fi->absoluteFileName()); // fi->fileName
		else
			return QVariant();
	}

	return QSortFilterProxyModel::data(index, role); // if inherited by QSortFilterProxyModel
}

