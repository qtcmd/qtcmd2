/***************************************************************************
 *   Copyright (C) 2005 by Mariusz Borowski <b0mar@nes.pl>                 *
 *   Copyright (C) 2009 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef SUBSYSTEMMANAGER_H
#define SUBSYSTEMMANAGER_H

#include "vfsflatlistselectionmodel.h"
#include "vfsflatlistproxymodel.h"
#include "vfsflatlistmodel.h"
#include "multiprogressdialog.h"
#include "progressdialog.h"
#include "fileattributes.h"
#include "subsystemconst.h"
#include "findfiledialog.h"
#include "pluginmanager.h"
#include "findcriterion.h"
//#include "fileviewer.h"
//#include "vfsmodel.h"

class FileViewer;

/**
	@author Piotr Mierzwinski <piom@nes.pl>
*/
class SubsystemManager : public QObject
{
	Q_OBJECT
public:
	SubsystemManager( const URI &prevUri, const QString &sPanelName, KeyShortcuts *pKeyShortcuts, QWidget *pParent );
	~SubsystemManager();

	void          initURI( const URI &uri );

	URI           uri() const  { return m_URI;        }
	QString       path() const { return m_URI.path(); }

	void          openFile( FileInfo *pFileInfo );

	void          setVfsModel( VfsFlatListModel *pVfsModel );
// 	void          setSelectionModel( VfsFlatListSelectionModel *pSelectionModel ) { m_selectionModel = pSelectionModel; }
	void          setVfsModelForCompleter( VfsFlatListModel *pVfsModel ) { m_vfsModelCompl = pVfsModel; }

	void          updateContextMenu( ContextMenu *pContextMenu, KeyShortcuts *pKS ) { m_subsystem->updateContextMenu(pContextMenu, pKS); }

	void          createNewEmptyFile( const QModelIndex &currentIndex, bool bCreateFile, QWidget *pParent );
	void          removeFiles( FileInfoToRowMap *pSelectionMap, QWidget *pParent );
	void          setAttributes( FileInfoToRowMap *pSelectionMap, FileAttributes *pChangedAttrib, bool bWeighBefore );

	void          findFiles( const FindCriterion &fcFindCriterion, FileInfoToRowMap *pFoundFileSelectionMap );
	void          makeLink( QWidget *pParent );
	void          editSymLink( const QModelIndex &currentIndex, QWidget *pParent );

	void          weighItem( FileInfo *pFileInfo, bool bUpdateModelAfterWeighing=true );
	void          weighSelectedItems( FileInfoToRowMap *pSelectionMap );

	Vfs::WritableStatus dirWritableStatus() { return m_subsystem->dirWritableStatus(); }

	void          openInOppositeActiveTab( const URI &uri );
	void          openInCurrentActiveTab( const URI &uri );

	void          openSelectedItemsWith( const QString &sAbsProgrammName, FileInfoToRowMap *pSelectionMap );

	void          setBreakOperation() { m_subsystem->setBreakOperation(); }

	void          openSelectedItemsWith( const QString &sApplicationName, QWidget *pParent, bool bSelectApplication=true, bool bOpenFoundItem=false );

	void          cancelVfsOperation();

	void          jumpToFoundFile( FileInfo *pFileInfo );

	bool          setSubsystemFromMime( const QString &sNewPathWrk ); // used by "put found files into panel"

	bool          handledOperation( Vfs::SubsystemCommand sc ) { return m_subsystem->handledOperation(sc); }

	void          copyFiles( FileInfoToRowMap *pSelectionMap, bool bCopy, QWidget *pParent );
	void          copyFilesTo( const QString &sTargetPath, FileInfoToRowMap *pSelectionMap, bool bCopy, bool bWeighBefore );

	void          startRemoveFiles( FileInfoToRowMap *pSelectionMap, bool bWeighBeforeRemoving );

	void          createFileView( FileInfoToRowMap *pSelectionMap, QModelIndex &currentIndex, bool bViewInWindow, bool bOpenInEditMode, QWidget *pParent );

	void          showFindFilesDialog( FindFileDialog *pFindFileDialog, FileInfoToRowMap *pFileSelectionMap );

	void          setPrevDirName( const QString &sPrevDirName ) { m_sPrevDirName = sPrevDirName; }

	void          updateItemOnLV( const QString &sFileName, bool bExists );

	bool          subsystemAvailableForSelected( FileInfoToRowMap *pSelectionMap );

	/** Depending on URI.path extract selected archive(s) or file(s) to opposite panel.
	 * @param pSelectionMap map containing selected items (all not being archives handled in plugins will be skipped)
	 * @param pParent pointer to parent (FileListView is allowed only)
	 */
	void          extract( FileInfoToRowMap *pSelectionMap, QWidget *pParent );

	/** Invokes extracting of archive(s).
	 * @param whereExtract points where archive should be extracted (available are EXTRACT_HERE, EXTRACT_TO)
	 * @param pSelectionMap map containing selected items (all not being archives handled in plugins will be skipped)
	 * @param pParent pointer to parent (FileListView is allowed only)
	 * Note. Method can be called only from FileListView due to dynamic case inside.
	 */
	void extractArchive( Vfs::WhereExtract whereExtract, FileInfoToRowMap *pSelectionMap, QWidget *pParent );
	// NOTE: SelectionMap is always available through pointer (m_vfsModel) to VfsFlatListModel, so this is multiplying information

	/** Invokes extracting of archive(s).
	 * @param whereArchive selects location whene archive will be created. available are: ARCHIVE_HERE, ARCHIVE_TO
	 * @param bMoveToArchive if TRUE then all archived files will be removed after operation finished
	 * @param pSelectionMap map containing selected items
	 * @param pParent pointer to parent (FileListView is allowed only)
	 * Note. Method can be called only from FileListView due to dynamic case inside.
	 */
	void createAnArchive( WhereArchive whereArchive, bool bMoveToArchive, FileInfoToRowMap *pSelectionMap, QWidget *pParent );

	void openCurrentItemInTab( const QModelIndex &currentIndex, Vfs::OpenInTab openInTab );

	void          forceUnmarkItems( FileInfoList *pProcessedFilesLst ) {
		m_selectionModel->setSelected(pProcessedFilesLst, false, true, true);
	}

	void pauseCurrentOperation( bool bPause ) {
		switchSubsystem(PluginManager::instance(), m_URI.mime()); // making sure, that operation will be invoked on properly subsystem
		m_subsystem->setPauseOperation(bPause);
	}
	bool viewOfFoundFile( FileInfo *pFileInfo, bool bEditMode );

	void applySettings( const FileSystemSettings &fileSystemSettings );

private:
	Subsystem    *m_subsystem;

	/// dir name used to selection item after leaving directory
	QString       m_sPrevDirName;
	QString       m_sPrevSubsysFileName;

	QString       name() const { return m_sPanelName; }
	URI           m_prevURI, m_URI;
	URI           m_remoteURI; // used for leaving archive opened from remote location

	VfsFlatListModel *m_vfsModel, *m_vfsModelCompl;
	VfsFlatListSelectionModel *m_selectionModel;

	/// Flag for opened archives
	bool          m_OpenFile;

	QString       m_sPanelName;
	QString       m_newlyCreatedEmpyFileName;

	QWidget      *m_pMainParent; // MainWindow obj. (helpful for positioning dialogs created here)

// 	ProgressDialog *m_progressDlg;
	QVector <ProgressDialog *> m_progressDlgPtrTable;
	int           m_progressDlgCounter;
	MultiProgressDialog *m_pMultiProgressDlg;

	bool          m_bShowMsgCantRmFileAgain;
	bool          m_bShowMsgCantSetAttrAgain;
	bool          m_bShowMsgCantCpAgain;
	bool          m_bShowMsgCantMvAgain;

	bool          m_bOperationCompleted;
//	bool          m_bPostCreatingAction;
	bool          m_bOpenInOppositeActiveTab;
	bool          m_bUpdateModelAfterWeighing;
	bool          m_bFileRenamed;
	bool          m_bExtractingFromLocalSubsys, m_bArchivingFromLocalSubsys;
	bool          m_bWeighBeforeArchExtracting; // usable only when Extracting from no local subsystem
	bool          m_bOpenArchInRemoteLocation;
	bool          m_bMoveToArchive;
	//bool          m_bDeleteWithoutConfirmation; // if TRUE then always skip confirmation

	QMap <QString, QString> m_absFileNameToAssociatedAppMap;
	QString       m_sTmpDir, m_sMimeWhenOpStopped;

	FileInfo     *m_ffdItemRemoved; // used by FindFileDialog for update list
	FileInfoList *m_pSelectedItemToView;
	bool          m_bTopOpenWithAppUseAsDefault;

	FileViewer   *m_pFileViewer;
	int           m_nViewerCounter;

	QMap <FileInfo *, QString> m_FileInfoToMime_map;
	QString       m_sTargetPathToExtracting; /// used as target path for extracting archive from remote location
	QString       m_sOpenedArchFromRemoteLocation; /// used for removing downloaded archive from tmp directory after leaving this archive
	QString       m_sDwnArchForExtractFromRemoteLocation; /// used for removing downloaded archive from tmp directory after extracting this archive
	QString       m_sTargetPathToArchiving; /// used for archiving to obtain target path in archiving next fun.
	QString       m_sArchiverExt;  // selected archiver type (extenstion) - used in archiving
	QString       m_sArchiverMime; // detected mime for selected archvier - used to switching to properly archive subsystem when archiving
	FileInfoToRowMap *m_selectedForDownloadBeforExtracting; /// holds one item required to download before extracting - prevents before downloading all files
	FileInfoToRowMap *m_pHelperSelectionMap; /// holds one item helped with removing found (in FindFileDialog) item and make way to open archive from remote location (download it)
	uint          m_nTotalProcessed, m_nCounterOfProcessed; // usuful in extracting multiply archives in one time
	uint          m_nTotalWeighedItems; // used in weighing before archiving
	qint64        m_nTotalWeight; // used in weighing before archiving
	bool          m_bSingleArchive;

	FileInfoToRowMap *m_pFileSelectionMap;

	Vfs::WhereExtract m_whereExtractArchive;
	Vfs::SubsystemStatus m_lastStatus;

	KeyShortcuts *m_pKeyShortcuts;

	/** Creates new instance of ProgressDialog class.
	 * Pointer newly created object is added to table m_progressDlgPtrTable to be able to manage with all created later.
	 * Newly created object is coonected with current Subsystem (pointen in m_subsystem)
	 * Signal "signalShowFileOverwriteDlg" sent by Subsystem is also reconnected with current class.
	 * @param bWeighBefore status saying that processed file/directory will be weigh before main operation
	 * @param subSysCmd Subsystem coomand that will be processed
	 * @param sTargetPath target path for processed operation (required only for Copy and similar operations)
	 */
	void showProgressDialog( bool bWeighBefore, Vfs::SubsystemCommand subSysCmd, const QString &sTargetPath=QString() );
	void removeProgressDlg( bool bCheckWhetherCanToClose=false );
	void showMultiProgressDialog ( bool bWeighBefore, Vfs::SubsystemCommand subSysCmd );
	void removeMultiProgressDlg( bool bCheckWhetherCanToClose=false );
	void openSelectedItemsWithSelectedApp( bool bOpenFromLocalsubsystem );

	/** Reconnects Subsystem, set subsystem pointer and invoke aligning of context menu.
	 * @param pPluginManager pointer to the PluginManager object, where has been created instance of it
	 * @param sMime mime related with new files subsystem
	 * @return TRUE if system has been reconncted otherwise FALSE.
	 */
	bool switchSubsystem( PluginManager *pPluginManager, const QString &sMime );
	void connectProgressDialogWithSubsystem( bool bMultiProgress=false );

	/** Invokes extracting selected files.
	 * @param pSelectionMap map containing selected items (all not being archives handled in plugins will be skipped)
	 * @param pParent pointer to parent (FileListView is allowed only)
	 * @param bMove if TRUE then extracted files will be also removed from archive
	 * Note. Method can be called only from FileListView due to dynamic case inside.
	 */
	void extractFilesTo( FileInfoToRowMap *pSelectionMap, bool bMove, QWidget *pParent );
	void extractNextArchive();
	void preArchiveNextItem();
	void archiveNextItem( const QString &sTargetArchiveName="" );

public slots:
	void slotActivated( const QModelIndex &index );
	void slotPathChanged( const QString &sInPathIn );
	void slotPathChangedForCompleter( const QString &sInPath );
	void slotOpenConnection();
	void slotCloseConnection();
	void slotRestorePath();
	void slotRefreshCurrentDir();
	void slotRenameFile( FileInfo *pFileInfo, const QString &sInNewName );
	void slotShowFileOverwriteDlg( const FileInfo &sourceFileInfo, FileInfo &targetFileInfo, int &nResult );
	void slotShowConnectionDlg( URI &uri, const QString &sProtocol, const QString &sNoPassUser, bool &bRetStat );

	void slotReadFileToViewer( const QString &sHost, FileInfo *pFileInfo, QByteArray &baBuffer, int &nReadBlockSize, int nReadingMode, const QString &sName );
	void slotUpdateItemOnLV( const QString &sFileName, bool bExists );

	void slotSubsystemStatusChanged( Vfs::SubsystemStatus eStatus, const QString &sTargetPanel );

private slots:
	void slotProgressDialogCancel( int nDlgId );
	void slotProgressDialogOperationInBackground();
	void slotCloseView();

	void slotMultiProgressDialogCancel();
	void slotDataTransferProgress( long long nBytesDone, long long nBytesTotal, uint nBytesTransfered, bool bSetTotalBytes );

	void slotGetWeighingResult( qint64 &totalWeight, uint &totalItems );

signals:
	void signalUpdateFocusSelection( bool bInitSelectionFlag, const QString &sPrevDirName );
	void signalPathViewChange( const URI &uri, bool updateComplModel, bool addPathToHis );
	void signalCloseConnection();
	void singalUpdateContextMenu();
	void signalAdjustColumnSize();

	void signalUpdateStatusBar( const QString &sMessage );
	void signalUpdateStatusBar( const QString &sMessage, uint nMiliSec, bool bShowPrevious );
	void signalUpdateListViewStatusBar();

	void signalPanelListed();

	void signalWeighingResult( qint64 weight, qint64 realWeight, uint files, uint dirs );
	void signalWeighStopAnimation();
	void signalClosePropertiesDlg();
	void signalShowErrorWeighingInfoOnPropertiesDlg( bool bShowErrorInfo );

	void signalAddMatchedItem( FileInfo *pFoundFileInfo, const QString &sSearchingResult );
	void signalSearchingFinished();
	void signalUpdateRemovedOnFindFileDialog( FileInfo *ffdItemRemoved );

	void signalGetPathFromTab( const QString &sTabObjName, QString &sPath );
	void signalGetPathFromOppositeActiveTab( QString &sPath );

	void signalGetRequestingPanelName( QString &sRequestingPanel );
// 	void signalResortItems();

// 	void signalReadyRead();

	void updateItemInAllMatchingTabs( const QString &sFileName, bool bExists );

	void signalForceUnmarkItemsInOppositeActiveTab( FileInfoList *pProcessedFilesList );

	void signalOpenInTab( const URI &uri, Vfs::OpenInTab openInTab );

	void signalCopyFilesFromOppositePanel( const QString &sTargetPath, FileInfoToRowMap *pSelectionMap, bool bCopy, bool bWeighBefore );
	void signalRemoveFiles( FileInfoToRowMap *pSelectionMap, bool bWeighBefore );

	//void signalStopOfSearching();

};

#endif // SUBSYSTEMMANAGER_H
