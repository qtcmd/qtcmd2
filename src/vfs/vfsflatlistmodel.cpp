/***************************************************************************
 *   Copyright (C) 2010 by Mariusz Borowski <b0mar@nes.pl>                 *
 *   Copyright (C) 2010 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDebug> // need to qDebug()
#include <QDir>
#include <QToolTip>
#include <QFileInfo>

#include "mimetype.h"
#include "settings.h"
#include "iconcache.h"
#include "vfsflatlistmodel.h"
#include "vfsflatlistselectionmodel.h"


VfsFlatListModel::VfsFlatListModel( const QString &sName, bool bFindFileResultMode )
{
	setObjectName(sName);

	m_bBoldDirs = true;
	m_bShowIcons = true;
	m_bBytesRound = true;
	m_bSelectDirs = false;
	m_updateDataFlag = false;
	setColumns(bFindFileResultMode);

	qDebug() << "VfsFlatListModel::VfsFlatListModel. objectName:" << objectName();
	m_selectionModel = nullptr;
	m_FindResultFileInfoList = nullptr;
	m_pIconCache = IconCache::instance();

	m_selectionWeigh = 0;
// 	m_bFollowByLink = false;
}

VfsFlatListModel::~VfsFlatListModel()
{
	exportDataModel();
	qDebug() << "VfsFlatListModel::~VfsFlatListModel(). Called for" << objectName();
}


void VfsFlatListModel::setColumns( bool bFindFileResultMode )
{
	qDebug() << "VfsFlatListModel::setColumns. bFindFileResultMode:" << bFindFileResultMode << "objectName:" << objectName();
	m_bFindFileResultMode = bFindFileResultMode;
	m_headerItems.clear();
	QStringList slAllColumns, slColumn;
	QString sColumns, sColumn;
	Settings settings;

	if (bFindFileResultMode) {
		slAllColumns << tr("Name") <<  tr("Size") << tr("Path") << tr("Time") << tr("Permission") << tr("Owner") << tr("Group") << tr("File type");
		sColumns = settings.value("filelistview/FL_Columns", "0-170,1-70,2-107,3-70,4-55,5-55,6-55,7-55").toString();
	}
	else {
		slAllColumns << tr("Name") <<  tr("Size") << tr("Time") << tr("Permission") << tr("Owner") << tr("Group") << tr("File type");
		sColumns = settings.value("filelistview/CL_Columns", "0-170,1-70,2-107,3-70,4-55,5-55,6-55").toString();
	}

	QStringList slColumns = sColumns.split(',');
	for (int i=0; i<slAllColumns.count(); i++) {
		sColumn = slColumns[i];
		slColumn = sColumn.split('-');
		//if (slColumn[1] != "0") // needs to add all and later show or hide proper column
			m_headerItems << slAllColumns[i];
	}
}


Qt::ItemFlags VfsFlatListModel::flags( const QModelIndex &index ) const
{
	if (! index.isValid())
		return Qt::NoItemFlags;

	return Qt::ItemIsEditable | Qt::ItemIsEnabled | Qt::ItemIsSelectable;
//	return Qt::ItemIsEditable | Qt::ItemIsEnabled;
}



QVariant VfsFlatListModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
	if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
		return m_headerItems.at(section);

	return QAbstractItemModel::headerData(section, orientation, role);
}

QModelIndex VfsFlatListModel::parent( const QModelIndex &index ) const
{
	return QModelIndex();
}

int VfsFlatListModel::columnCount( const QModelIndex &parent ) const
{
	if (parent.column() > 0)
		return 0;

	return m_headerItems.size();
}

int VfsFlatListModel::rowCount( const QModelIndex &parent ) const
{
	if (parent.column() > 0)
		return 0;

	return m_fileInfoList.count();
}


FileInfo & VfsFlatListModel::fileInfo( const QModelIndex &index ) const
{
	Q_ASSERT(index.isValid());
	if (! index.isValid())
		return *m_fileInfoList.at(0);

	return *m_fileInfoList.at(index.row()); // only after first sorting (QDir obj) works correct!!!
}

FileInfo * VfsFlatListModel::fileInfoPtr( const QModelIndex &index )
{
	Q_ASSERT(index.isValid());
	if (! index.isValid())
		return m_fileInfoList.at(0);

// 	qDebug() << "VfsFlatListModel::fileInfoPtr. index row:" << index.row() << "model:" << index.model();
// 	qDebug() << "VfsFlatListModel::fileInfoPtr. m_fileInfoList:" << m_fileInfoList;
// 	qDebug() << "VfsFlatListModel::fileInfoPtr. list size:" << m_fileInfoList.size();
	return m_fileInfoList.at(index.row()); // only after first sorting (QDir obj) works correct!!!
}

FileInfo * VfsFlatListModel::fileInfoPtr( const QString &sFileName, const QString &sFilePath )
{
	if (sFileName.isEmpty())
		return nullptr;

	FileInfo *pFileInfo = nullptr;
	for (int id = 0; id < m_fileInfoList.count(); ++id) {
		if (! sFilePath.isEmpty()) { // check pathName and fileName
			if (m_fileInfoList.at(id)->location() == sFilePath) {
				if (m_fileInfoList.at(id)->fileName() == sFileName) {
					pFileInfo = m_fileInfoList.at(id);
					break;
				}
			}
		}
		else
		if (m_fileInfoList.at(id)->fileName() == sFileName) {
			pFileInfo = m_fileInfoList.at(id);
			break;
		}
	}
	return pFileInfo;
}

QVariant VfsFlatListModel::data( const QModelIndex &index, int role ) const
{
	// Qt4/5 event loop calls this every time when (mouse) cursor is over the item's list!
	if (! index.isValid())
		return QVariant();

	if (role != Qt::ToolTipRole)
		QToolTip::hideText();

	int idxOfColumn = index.column();
	const FileInfo fi = fileInfo(index);

	if (role == Qt::DisplayRole || role == Qt::EditRole || role == Qt::ToolTipRole) {
		if (m_bFindFileResultMode) {
			switch (idxOfColumn) {
				case 0: return fi.fileName();
				case 1: {
					if      (fi.isDir())
						return fi.dirLabel();
					else if (fi.isSymLink())
						return (m_bBytesRound) ? FileInfo::bytesRound(fi.symLinkTarget().size()) : QString::number(fi.symLinkTarget().size());
					else
						return (m_bBytesRound) ? FileInfo::bytesRound(fi.size())                 : QString::number(fi.size());
				}
				case 2: return fi.filePath();
				case 3: return fi.lastModified().toString("yyyy-MM-dd HH:mm:ss");
				case 4: return fi.permissions();
				case 5: return fi.owner();
				case 6: return fi.group();
				case 7: return fi.mimeInfo();
				default:
					Q_ASSERT(false);
			}
		}
		else {
			switch (idxOfColumn) {
				case 0: return fi.fileName();
				case 1: {
					if      (fi.isDir())
						return fi.dirLabel();
					else if (fi.isSymLink())
						return (m_bBytesRound) ? FileInfo::bytesRound(fi.symLinkTarget().size()) : QString::number(fi.symLinkTarget().size());
					else
						return (m_bBytesRound) ? FileInfo::bytesRound(fi.size())                 : QString::number(fi.size());
				}
				case 2: return fi.lastModified().toString("yyyy-MM-dd HH:mm:ss");
				case 3: return fi.permissions();
				case 4: return fi.owner();
				case 5: return fi.group();
				case 6: return fi.mimeInfo();
				default:
					Q_ASSERT(false);
			}
		}
	}
	// handled in VfsFlatListSelectionModel::data
/*	if (role == Qt::ForegroundRole) {
			if (fi.isHidden())
				return m_hiddenFgColor;
			if (fi.isSymLink() && fi.mimeInfo() == "symlink/broken")
				return m_brokenSymlinkColor;
			if (! fi.isDir() && fi.isSymLink())
				return m_symlinkColor;
			if (! fi.isDir() && fi.permissions().contains('x'))
				return m_executableFgColor;
	}*/
	if (idxOfColumn == 0) {
		if (role == Qt::DecorationRole && m_pIconCache != nullptr && m_bShowIcons) {
			return m_pIconCache->icon(fi.mimeInfo()); // was MimeType::getMimeIcon(fi.mimeInfo())
		}
		if (role == Qt::FontRole) {
			if (m_bBoldDirs && fi.isDir()) {
				QFont fontBold;
				fontBold.setBold(true);
				return fontBold;
			}
		}
		else
		if (role == Qt::StatusTipRole)
			return fi.symLinkTarget();
	}
	else
	if (idxOfColumn == 1) {
		if (role == Qt::TextAlignmentRole && fi.isDir()) {
			return Qt::AlignCenter;
		}
		if (role == Qt::FontRole) {
			if (m_bBoldDirs && fi.isDir()) {
				QFont fontBold;
				fontBold.setBold(true);
				return fontBold;
			}
		}
	}

	if (m_selectionModel == nullptr)
		return QVariant();

	return m_selectionModel->data(index, role); // to handle selection
}

bool VfsFlatListModel::setData( const QModelIndex &index, const QVariant &value, int role )
{
	if (role != Qt::EditRole || ! m_updateDataFlag) {
		m_setDataValue = value;
		m_setDataIndex = index;
		return false;
	}
	FileInfo *pItem = fileInfoPtr(index);
	bool result = pItem->setData(index.column(), value, m_bFindFileResultMode);
// 	qDebug() << "VfsFlatListModel::setData" << "item" << pItem->fileName() << "value:" << value << objectName();
	if (result) {
		m_fileInfoList[index.row()] = pItem;
		emit dataChanged(index, index);
		m_updateDataFlag = false;
	}

	return result;
}

bool VfsFlatListModel::setRowData( const QModelIndex &idx, FileInfo *fileInfo, int role )
{
	for (int column=0; column < columnCount(); ++column) {
		m_updateDataFlag = true;
		QModelIndex child = index(idx.row(), column);

		if (m_bFindFileResultMode) {
			if (column == 0)
				setData(child, fileInfo->fileName()); // Qt::EditRole is default
			else
			if (column == 1)
				setData(child, fileSize(fileInfo));
			else
			if (column == 2)
				setData(child, fileInfo->filePath());
			else
			if (column == 3)
				setData(child, fileInfo->lastModified());
			else
			if (column == 4)
				setData(child, fileInfo->permissions());
			else
			if (column == 5)
				setData(child, fileInfo->owner());
			else
			if (column == 6)
				setData(child, fileInfo->group());
			else
			if (column == 7)
				setData(child, fileInfo->mimeInfo());
		}
		else {
			if (column == 0)
				setData(child, fileInfo->fileName());
			else
			if (column == 1)
				setData(child, fileSize(fileInfo));
			else
			if (column == 2)
				setData(child, fileInfo->lastModified());
			else
			if (column == 3)
				setData(child, fileInfo->permissions());
			else
			if (column == 4)
				setData(child, fileInfo->owner());
			else
			if (column == 5)
				setData(child, fileInfo->group());
			else
			if (column == 6)
				setData(child, fileInfo->mimeInfo());
		}
	}
	return true;
}

bool VfsFlatListModel::updateRowsData( int column, const QStringList &listOfItemsToModify, FileInfoList *pFileInfoList )
{
	if (pFileInfoList == nullptr)
		return false;
	int rows = pFileInfoList->size();
	if (rows == 0 || listOfItemsToModify.size() == 0 || column < 0) {
		qDebug() << "VfsFlatListModel::updateRowsData. Incorrect input values";
		return false;
	}
// 	qDebug() << "VfsFlatListModel::updateRowsData. ItemsToModify:" << listOfItemsToModify.count() << objectName();

	Qt::MatchFlags matchFlags = Qt::MatchExactly;
	int hits = 1;
	QModelIndex start, current;
	QModelIndexList matchLst;

	setUpdateData(true); // triggers updating data in model (setData)
	for (int row=0; row < rows; ++row) {
		start = index(0, column);
		matchLst = match(start, Qt::DisplayRole, listOfItemsToModify.at(row), hits, matchFlags); // should find exacly one item
		if (matchLst.size() == 0) {
			qDebug() << "VfsFlatListModel::updateRowsData. Not found item with value:" << listOfItemsToModify.at(row);
			continue;
		}
		current = matchLst.at(row);
		setRowData(current, pFileInfoList->at(row)); // set all columns for this child
	}

	return true;
}

bool VfsFlatListModel::updateRowData( int column, const QString &itemNameToMofify, FileInfo *pFileInfo )
{
	if (pFileInfo == nullptr || itemNameToMofify.isEmpty() || column < 0) {
		qDebug() << "VfsFlatListModel::updateRowData. Incorrect input values";
		return false;
	}
// 	qDebug() << "VfsFlatListModel::updateRowData. ItemsToModify: 1" << objectName();

	QStringList listOfItemsToModify(itemNameToMofify);
	FileInfoList *fil = new FileInfoList;
	fil->append(pFileInfo);
	updateRowsData(0, listOfItemsToModify, fil);
	delete fil;

	return true;
}

/*
bool VfsFlatListModel::insertRows( int position, int rows, const QModelIndex &parent )
{
	if (position < 0 || position > m_fileInfoList.size() || rows <= 0)
		return false;

	beginInsertRows(parent, position, position + rows - 1);
	QVector<QVariant> data(columnCount());
	for (int i=0; i < rows; ++i) {
		FileInfo *item = new FileInfo(data);
		m_fileInfoList.insert(position, item);
	}
	endInsertRows();

	return true;
}
*/
bool VfsFlatListModel::insertItems( FileInfoList *pFileInfoList, int nPosition, bool bNoDuplicateNames )
{
	if (nPosition < 0 || nPosition > m_fileInfoList.size() || pFileInfoList == nullptr)
		return false;
	int rows = pFileInfoList->size();
	if (rows == 0)
		return false;

// 	qDebug() << "VfsFlatListModel::insertItems. rows:" << rows << "in:" << objectName();
	// -- updating items
	QVector <int> idToRevmoves;
	const int fiRows = rows;
	FileInfo *pFileInfo;
	for (int row=0; row < fiRows; ++row) {
		pFileInfo = pFileInfoList->at(row);
		if (fileInfoPtr(pFileInfo->fileName()) != nullptr) {
			updateRowData(1, pFileInfo->fileName(), pFileInfo);
			idToRevmoves.append(row);
			rows--;
		}
	}
	// commented due to in SubsystemManager::slotSubsystemStatusChanged method m_vfsModel->setSelected have nothing to unselect
// 	if (idToRevmoves.size() > 0) {
// 		for (int i = 0; i < idToRevmoves.size(); ++i) {
// 			fileInfoList->removeAt(idToRevmoves[i]);
// 		}
// 	}
	if (rows == 0)
		return true;
	// -- inserting items
	beginInsertRows(QModelIndex(), nPosition, nPosition + rows - 1); // mi, first, last
	setUpdateData(true); // triggers updating data in model (used in setData)
	for (int row=0; row < pFileInfoList->size(); ++row) {
// 		fileInfo = pFileInfoList->at(row);
//		qDebug() << "  -- insert:" << fileInfo->fileName();
// 		if (bNoDuplicateNames) {
// 			if (fileInfoPtr(fileInfo->fileName()) != nullptr)
// 				continue;
// 		}
		m_fileInfoList.insert(nPosition, pFileInfoList->at(row));
	}
	endInsertRows(); // calls sorting

	emit signalModelChanged(); // for update ListViewStatusBar
	return true;
}

void VfsFlatListModel::insertItems( FileInfoList *pFileInfoList, const QString &sFindResultName )
{
	m_sFindResultName = sFindResultName;
	insertItems(pFileInfoList, 0);
}


bool VfsFlatListModel::removeItems( FileInfoList *pFileInfoList )
{
	if (pFileInfoList == nullptr)
		return false;

	int fiRows = pFileInfoList->size();
// 	int smRows = m_selectionMap->size();
	qDebug() << "VfsFlatListModel::removeItems. ProcessedItems:" << fiRows << " objectName:" << objectName();
	if (fiRows == 0)
		return false;

// 	emit signalSetSelected(pFileInfoList, false); // one caught by SelectionModel
	m_selectionModel->setSelected(pFileInfoList, false, true, true);
	setUpdateData(true); // triggers updating of data in model

	FileInfo *pFIsel, *pFIrem;
	qSort(*pFileInfoList);
	//std::sort(&pFileInfoList); // no matching funciton for call to 'sort'
	beginResetModel();
	for (int i = fiRows - 1; i >= 0; --i) {
		pFIsel = pFileInfoList->at(i);
// 		if (smRows > 0) {
// 			setSelected(pFIsel, false, true, true); // in LocalSubsystemWatcher is created new FileInfo obj., so need to find it by name and unselect by force
// 		}
		if (! m_fileInfoList.removeOne(pFIsel)) { // item must be found in loop (other reason: create QMap <QString fname, FileInfo *ptr> for both panels)
			pFIrem = nullptr;
			for (int i = 0; i < rowCount(); ++i) {
				if (m_fileInfoList.at(i)->fileName() == pFIsel->fileName()) {
					pFIrem = m_fileInfoList.at(i);
					break;
				}
			}
			if (pFIrem != nullptr) {
				m_fileInfoList.removeOne(pFIrem);
			}
		}
	}
	endResetModel();
	emit signalModelChanged(); // for update ListViewStatusBar

	return true;
}

/*
bool VfsFlatListModel::insertColumns( int position, int columns, const QModelIndex &parent )
{
	FileInfo item = fileInfo(parent);
	bool success;

	beginInsertColumns(parent, position, position + columns - 1);
	success = item.insertColumns(position, columns);
	endInsertColumns();

	return success;
}

bool VfsFlatListModel::removeColumns( int position, int columns, const QModelIndex &parent )
{
	FileInfo item = fileInfo(parent);
	bool success;

	beginRemoveColumns(parent, position, position + columns - 1);
	success = item.removeColumns(position, columns);
	endRemoveColumns();

	if (columnCount() == 0)
		removeRows(0, rowCount());

	return success;
}
*/

bool VfsFlatListModel::setDataPost()
{
	return setData( m_setDataIndex, m_setDataValue );
}


QModelIndex VfsFlatListModel::index( int row, int column, const QModelIndex &parent ) const
{
	if ( ! hasIndex(row, column, parent) )
		return QModelIndex();

	if (row >= 0 && row < m_fileInfoList.count()) {
		return createIndex(row, column);
	}
	return QModelIndex();
}

bool VfsFlatListModel::hasChildren( const QModelIndex &parent ) const
{
	if (parent.isValid())
		return false;

	return true;
}


FileInfoList * VfsFlatListModel::inportDataModel( const QString &sFindResultName )
{
	m_sFindResultName = sFindResultName;
// 	if (! m_bFindFileResultMode)
// 		return nullptr;

	Settings settings;
	QString sPath = QFileInfo(settings.fileName()).absolutePath() + QDir::separator();
	QString sInFileName = sPath + m_sFindResultName + ".txt";

	QFile fname(sInFileName);
	if (fname.open(QFile::ReadOnly)) {
		m_FindResultFileInfoList = new FileInfoList;
		QTextStream inStream(&fname);
		QString sLine;
		QDateTime dt;
		do {
			sLine = inStream.readLine();
			QStringList slLine = sLine.split('|');
			if (slLine.count() < 10)
				continue;

			FileInfo *fi = new FileInfo(
				 slLine[0], // fileName
				 slLine[1], // filePath
				 slLine[2].toLongLong(), // size
				(slLine[3] == "true"),  // isDir
				(slLine[4] == "true"),  // isSymLink
				 dt.fromString(slLine[5], "yyyy-MM-dd HH:mm:ss"),
				 slLine[6], // permissions
				 slLine[7], // owner
				 slLine[8], // group
				 slLine[9],  // mimeInfo
				 slLine[10]  // symLinkTarget
			);
			m_FindResultFileInfoList->append(fi);
		} while (! sLine.isNull());
	}
	else // cannot open file
		return nullptr;

	qDebug() << "VfsFlatListModel::inportDataModel. imported items:" << m_FindResultFileInfoList->count();
	return m_FindResultFileInfoList;
}

void VfsFlatListModel::exportDataModel()
{
	if (! m_bFindFileResultMode) // TODO: one might to check if something changed or it's newly created model
		return;

	// TODO export only when subsystem was modified or was newly created
	Settings settings;
	QString sPath = QFileInfo(settings.fileName()).absolutePath() + QDir::separator();
	QString sOutFileName = sPath + m_sFindResultName + ".txt";

	QFile fname(sOutFileName);
	if (fname.open(QFile::WriteOnly | QFile::Truncate)) {
		QTextStream out(&fname);
		QString sOutLine;
		for (int row = 0; row < rowCount(); ++row) {
			sOutLine +=
				  m_fileInfoList[row]->fileName() + "|"
				+ m_fileInfoList[row]->filePath() + "|"
				+ QString("%1").arg(m_fileInfoList[row]->size()) + "|"
				+ (m_fileInfoList[row]->isDir()     ? "true" : "false") + "|"
				+ (m_fileInfoList[row]->isSymLink() ? "true" : "false") + "|"
				+ m_fileInfoList[row]->lastModified().toString("yyyy-MM-dd HH:mm:ss") + "|"
				+ m_fileInfoList[row]->permissions() + "|"
				+ m_fileInfoList[row]->owner() + "|"
				+ m_fileInfoList[row]->group() + "|"
				+ m_fileInfoList[row]->mimeInfo() + "|"
				+ m_fileInfoList[row]->symLinkTarget() + "\n";
			out << sOutLine;
			sOutLine = "";
		}
	}
}


qint64 VfsFlatListModel::fileSize( FileInfo *pFI, bool bFollowByLink )
{
	if (pFI->isSymLink()) {
		if (bFollowByLink)
			return pFI->size();
		else
			return QFileInfo(pFI->absoluteFileName()).symLinkTarget().length();
	}

	return pFI->size();
}

