/***************************************************************************
 *   Copyright (C) 2018 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
// #include <QDebug> // need to qDebug()

#include "filelistview.h"
#include "listviewitemdelegate.h"

ListViewItemDelegate::ListViewItemDelegate( TreeView *pTreeView )
{
	m_pModel = nullptr;
	m_selectionModel = nullptr;
	m_pTreeView = pTreeView;

	m_returnPressed = false;
	m_pIconCache = IconCache::instance();
}

ListViewItemDelegate::ListViewItemDelegate( QObject *pParent ) : QStyledItemDelegate(pParent) // was QItemDelegate
{
// 	qDebug() << "ListViewItemDelegate::ListViewItemDelegate" << parent;
	m_pModel = nullptr;
	m_selectionModel = nullptr;
	m_pTreeView = nullptr;

	setParentObj(pParent);
// 	if (parent != nullptr) {
// 		FileListView *pFLV = (FileListView *)parent;
// 		m_pModel    = pFLV->vfsModel();
// 		m_pSelModel = pFLV->vfsSelectionModel();
// 	}
	m_returnPressed = false;
	m_pIconCache = IconCache::instance();
}

QWidget* ListViewItemDelegate::createEditor( QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index ) const
{
	if (index.column() != 0) // other columns are not editable
		return nullptr;

	QLineEdit *pEditor = new QLineEdit(parent);
//	pEditor->setFrame(true); // by default turned on
	pEditor->setFocus();
	return pEditor;
}

void ListViewItemDelegate::setEditorData( QWidget *editor, const QModelIndex &index ) const
{
	if (index.column() != 0) // other columns are not editable
		return;

	QString value = index.model()->data(index, Qt::EditRole).toString();

	QLineEdit *pLineEdit = static_cast<QLineEdit*>(editor);
	pLineEdit->setText(value);
}

void ListViewItemDelegate::setModelData( QWidget *editor, QAbstractItemModel *model, const QModelIndex &index ) const
{
	if (index.column() != 0) // other columns are not editable
		return;

	QLineEdit *pLineEdit = static_cast<QLineEdit*>(editor);
	QString value = pLineEdit->text();

	model->setData(index, value, Qt::EditRole);
}

void ListViewItemDelegate::updateEditorGeometry( QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index ) const
{
	if (index.column() != 0) // other columns are not editable
		return;

	QRect rect = option.rect;
	QSize iconSize = ((QIcon &)(index.model()->data(index, Qt::DecorationRole).data_ptr())).actualSize(QSize(128,128)); // returns: 64x64 (real icon's size)
	//qDebug() << ((QLineEdit *)editor)->sizeHint() << iconSize << iconSize.isValid();
	if (iconSize.isValid())
		rect.setX(rect.x() + iconSize.width()/2);

	QString sStyleName = QApplication::style()->objectName();
// 	qDebug() << "ListViewItemDelegate::updateEditorGeometry. styleName:" << sStyleName;
	if (sStyleName == "oxygen") { // correct for to small hight
		rect.setHeight(rect.height()+4);
		rect.setY(rect.y() - 2);
	}
	else
	if (sStyleName == "breeze") {
		rect.setX(rect.x() - option.font.pointSize());
	}

	editor->setGeometry(rect);
}

void ListViewItemDelegate::setParentObj( QObject *pParent )
{
	if (pParent != nullptr) {
		FileListView *pFLV = (FileListView *)pParent;
		m_pModel    = pFLV->vfsModel();
		m_selectionModel = pFLV->vfsSelectionModel();
	}
}


bool ListViewItemDelegate::eventFilter( QObject *editor, QEvent *event )
{
	QEvent::Type eventType = event->type();
	m_returnPressed = false;

	if (eventType == QEvent::KeyPress) {
		QKeyEvent *pKeyEvent = (QKeyEvent *)event;
		int key = pKeyEvent->key();

		if (key == Qt::Key_Return || key == Qt::Key_Enter) {
			//qDebug() << "Return/Enter";
			m_returnPressed = true;
		}
	}

	return QStyledItemDelegate::eventFilter(editor, event); // was: QItemDelegate
}

bool ListViewItemDelegate::editorEvent( QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index )
{
	if (index.column() != 0) // other columns are not editable )yet)
		QStyledItemDelegate::editorEvent(event, model, option, index);;// was: QItemDelegate

	QEvent::Type eventType = event->type();
	if (eventType >= 2 && eventType <= 5)
		emit closeEditor(0);

	return QStyledItemDelegate::editorEvent(event, model, option, index); // was: QItemDelegate
}

void ListViewItemDelegate::paint( QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index ) const
{
    Q_ASSERT(index.isValid());

	if (option.state & QStyle::State_Selected) {  // highlighted item (with focus)
		if (m_selectionModel == nullptr)
			QStyledItemDelegate::paint(painter, option, index); // was: QItemDelegate
		else {
			const VfsFlatListProxyModel *pProxyModel = static_cast <const VfsFlatListProxyModel *>(index.model());
			QModelIndex idx = (pProxyModel == nullptr) ? index : pProxyModel->mapToSource(index);
			FileInfo fi = m_pModel->fileInfo(idx);
			QColor color;

			if (m_selectionModel->selected(index))
				color = m_selectionModel->selectedItemUnderCursorColor();
			else { // set proper file color
				//m_selectionModel->itemUnderCursorColor();
				if (fi.isHidden())
					color = m_selectionModel->hiddenFileColor();
				else if (fi.isSymLink() && fi.mimeInfo() == "symlink/broken")
					color = m_selectionModel->brokenSymlinkColor();
				else if (! fi.isDir() && fi.isSymLink())
					color = m_selectionModel->symlinkColor();
				else if (! fi.isDir() && fi.permissions().contains('x'))
					color = m_selectionModel->executableFileColor();
				else
					color = m_selectionModel->itemUnderCursorColor();
					//color = painter->pen().color(); // default color
			}

			QRect rect = option.rect;
			QFont font = painter->font();
			font.setFamily(option.font.family());
			font.setBold(painter->font().bold());
			int nCellWidth = m_pTreeView->header()->sectionSize(m_pTreeView->header()->logicalIndex(index.column()))-10; // -10 is margin size

			int flags = option.displayAlignment;
			if (index.column() == 0) {
				if (m_pModel->showIcons())
					rect.setX(3+16+6);
				font.setBold(fi.isDir());
				if (m_pModel->showIcons())
					nCellWidth -= (16 + option.rect.x()+3); // 16 is icon width
			}
			else
			if (index.column() == 1) {
				//if (index.model()->data(index, Qt::DisplayRole).toString() == "<DIR>") // FileInfo: m_dirLabel = "<DIR>";
				if (fi.isDir())
					flags = option.decorationAlignment;
				else
					rect.setX(rect.x()+3);
				font.setBold(fi.isDir());
			}
			else
			if (index.column()  > 1) {
 				font.setBold(false);
				rect.setX(rect.x()+3);
			}
// 			painter->setFont(font);
			painter->setRenderHint(QPainter::Antialiasing, true);
			painter->setPen(color);
// 			if (option.state & QStyle::State_Selected) // doesn't highlight cells
				painter->fillRect(option.rect, m_selectionModel->cursorColor());
			painter->setFont(font);
			painter->drawText(rect, flags, squeezeString(index.model()->data(index, Qt::DisplayRole).toString(), nCellWidth, font, painter->device()));
			if (index.column() == 0 && m_pModel->showIcons()) {
				QString sMime = m_pModel->fileInfo(idx).mimeInfo();
				//qDebug() << "sMime:" << sMime << "name:" << m_pModel->fileInfo(idx).fileName();
				painter->drawImage(option.rect.x()+3, option.rect.y()+5, m_pIconCache->icon(sMime).pixmap(QSize(16,16)).toImage()); // for QItemDelegate
			}
		}
	}
	else
		QStyledItemDelegate::paint(painter, option, index); // was: QItemDelegate
}


QString ListViewItemDelegate::squeezeString( const QString &sStr, uint nCellWidthInPixels, const QFont &font, QPaintDevice *pDevice ) const
{
	QString sNewStr = sStr;
	uint nTextNewWidthInPixels;
	uint nTextWidthInPixels = QFontMetrics(font, pDevice).horizontalAdvance(sStr, sStr.length());
	uint nThreeDotsWidth    = QFontMetrics(font, pDevice).horizontalAdvance("...", 3);
// 	qDebug() << sStr << "nCellWidthInPixels:" << nCellWidthInPixels << "nTextWidthInPixels:" << nTextWidthInPixels << "nThreeDotsWidth:" << nThreeDotsWidth;
	if (nTextWidthInPixels <= nCellWidthInPixels)
		return sNewStr;

	while (true) {
		sNewStr = sNewStr.left(sNewStr.length()-1);
		nTextNewWidthInPixels = QFontMetrics(font, pDevice).horizontalAdvance(sNewStr, sNewStr.length()) + nThreeDotsWidth;
		if (sNewStr.length() == 1 || nCellWidthInPixels >= nTextNewWidthInPixels) {
			sNewStr += "...";
			break;
		}
	}
// 	qDebug() << "sNewStr:" << sNewStr;

	return sNewStr;
}

