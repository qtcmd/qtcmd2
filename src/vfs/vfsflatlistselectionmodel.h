/***************************************************************************
 *   Copyright (C) 2017 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef VFSFLATLISTSELECTIONMODEL_H
#define VFSFLATLISTSELECTIONMODEL_H

#include <QAbstractItemModel>

#include "fileinfo.h"
#include "vfsflatlistmodel.h"
#include "vfsflatlistproxymodel.h"


using namespace Vfs;
/**
	@author Piotr Mierzwiński <piom@nes.pl>
 */
class VfsFlatListSelectionModel : public QAbstractItemModel
{
	Q_OBJECT
public:
	VfsFlatListSelectionModel( const QString &sName, VfsFlatListModel *pSourceModel );
	~VfsFlatListSelectionModel();

 	virtual QModelIndex	index( int row, int column, const QModelIndex &parent = QModelIndex() ) const  { return QModelIndex(); }
	virtual QModelIndex	parent( const QModelIndex &index ) const  { return QModelIndex(); }
	virtual int         rowCount( const QModelIndex &parent ) const  { return 0; }
    virtual int         columnCount( const QModelIndex &parent ) const  { return 0; }
    virtual QVariant    data( const QModelIndex &index, int role ) const;

	/** Sets source model for selection model.
	  * @oaram pSourceModel source model
	 */
	void                setSourceModel( VfsFlatListModel *pSourceModel ) { m_sourceModel = pSourceModel; }
// 	VfsFlatListModel *sourceModel() const { return m_sourceModel; }

	/** Sets source model for selection model. Used to makrs filtered items.
	  * @oaram pProxyModel source model
	 */
	void                setProxyModel( VfsFlatListProxyModel *pProxyModel ) { m_proxyModel = pProxyModel; }
// 	VfsFlatListProxyModel *proxyModel() const { return m_proxyModel; }

	/** Returns map of selection like <FileInfo *, int>
	 * @return pointer to FileInfoToRowMap object
	 */
	FileInfoToRowMap   *selectionMap() const { return m_selectionMap; }

	FileInfoList       *listOfSeleted() const;

	/** Returns number of selected items.
	 * @return number of selected items.
	 */
 	int                 count() const { return m_selectionMap->size(); }

 	/** Returns number of selected items.
	 * @return number of selected items.
	 */
 	int                 size()  const { return m_selectionMap->size(); }

	/** Returns weigh of selected items.
	 * @return weigh in bytes.
	 */
	qint64              weighOfSelected() const { return m_weighOfSelected; }

	/** Sets in given parameters number of selected items.
	 * @param files selected files
	 * @param dirs selected dirs
	 * @param symlinks selected symbolic links
	 */
	void                getSelectionDetails( uint &files, uint &dirs, uint &symlinks ) const;

	/** Method returns detailed information about selected items (files, dirs and symlinks), including sum of all.
	 * @param bSelectedWordInMsg if TRUE then appends " selected" word to the returned message
	 * @param bAddSumInfo if TRUE then adds info with sum of selected items to the returned message
	 * @return message about selected items
	 */
	QString             selectedItemsMsg( bool bSelectedWordInMsg=true, bool bAddSumInfo=true ) const;

	/** Returns first marked item from selectionMap.
	 * @return FileInfo pointer or NULL if empty selectionMap
	 */
	FileInfo           *firstMarked() const;

	/** Marks given item if no one marked.
	 * @param index index to mark
	 * @return number of marked.
	 */
	int                 markIfNoOneMarked( const QModelIndex &index );

	/** Returns selection status for given index.
	 * @param index checked index
	 * @return true if item selected, otherwise false.
	 */
	bool                selected( const QModelIndex &index ) const;

	/** Selects or deselects given index.
	 * @param index index to set
	 * @param bMark status of selection for given index
	 */
	bool                setSelected( const QModelIndex &index, bool bMark );

	/** Selects or deselects given FileInfo item.
	 * @param pFileInfo FileInfo class pointer for item to mark/unmark
	 * @param bMark status of selection for FileInfo item
	 * @param bLookingForName if pointer of item has not been found then start look it for by name
	 * @param bForceUnmark if true then do force unmarking otherwise consider only bSelected value
	 * @param updateWeigh  if true then update weigh on status otherwise skip it
	 */
	bool                setSelected( FileInfo *pFileInfo, bool bMark, bool bLookingForName=false, bool bForceUnmark=false, bool updateWeigh=true );

	/** Selects or deselects given FileInfo list items.
	 * @param pFileInfoList list pointers of FileInfo class with items to mark/unmark
	 * @param bMark status of selection for items on given list
	 * @param bLookingForName if pointer of item has not been found then start look it for by name
	 * @param bForceUnmark if true then do force unmarking otherwise consider only bSelected value
	 * @param bUpdateWeigh  if true then update weigh on status otherwise skip it
	 */
	void                setSelected( FileInfoList *pFileInfoList, bool bMark, bool bLookingForName=false, bool bForceUnmark=false, bool bUpdateWeigh=true );

	/** Inverses selection on internal map selection.
	 * @param index index to set
	 */
	void                inverseIndexSelection( const QModelIndex &index );

	/** Inverses selection all items.
	 */
	void                inverseSelection();

	/** Selects not selected or deselect all selected items.
	 * @param bMark if true then select items otherwise deselect items.
	 */
	void                selectAll( bool bMark );

	/** Selects not selected or deselect all selected items with given extention (suffix: .extention).
	 * @param bMark if true then select items otherwise deselect items.
	 * @param sExtention string describing extention
	 */
	void                selectWithExtention( bool bMark, const QString &sExtention );

	/** Selects not selected or deselect all selected items accoring to passed pattern(s).
	 * @param bMark if true then try to select otherwise deselect
	 * @param sPatterns one or more patterns
	 * @param bUseRegExp if true then pattern(s) will be processing like regular expression(s) otherwise like wildcards
	 */
	void                selectByPattern( bool bMark, const QString &sPatterns, bool bUseRegExp );

	/** Selects given range of items.
	 * @param fromIn first item to select
	 * @param toIn last item to select
	 * @param pTreeView TreeVew pointer, helps in going through items tree
	 */
	void               selectRange ( const QModelIndex &fromIn, const QModelIndex &toIn, TreeView *pTreeView );

	/** Clear all selections.
	 */
	void               clear();


	QColor             cursorColor() const { return m_cursorColor; } // used in ListViewItemDelegate
	void               setColorOfCursor( const QColor &color ) { m_cursorColor = color; }

	QColor             itemUnderCursorColor() const { return m_itemUnderCursorColor; } // used in ListViewItemDelegate
	void               setColorOfItemUnderCursor( const QColor &color ) { m_itemUnderCursorColor = color; }

	QColor             selectedItemUnderCursorColor() const  { return m_selectedItemUnderCursorColor; }
	void               setColorOfSelectedItemUnderCursor( const QColor &color ) { m_selectedItemUnderCursorColor = color; }

	QColor             commonItemColor() const { return m_commonItemColor; }
	void               setColorOfCommonItem( const QColor &color ) { m_commonItemColor = color; }

	QColor             selectedItemColor() const { return m_selecttedItemColor; }
	void               setColorOfSelectedItem ( const QColor &color ) { m_selecttedItemColor = color; }

	QColor             hiddenFileColor() const { return m_hiddenFgColor; }
	void               setColorOfHiddenFile( const QColor &color ) { m_hiddenFgColor = color; }

	QColor             executableFileColor() const { return m_executableFgColor; }
	void               setColorOfExecutableFile( const QColor &color ) { m_executableFgColor = color; }

	QColor             symlinkColor() const { return m_symlinkColor; }
	void               setColorOfSymlink( const QColor &color ) { m_symlinkColor = color; }

	QColor             brokenSymlinkColor() const { return m_brokenSymlinkColor; }
	void               setColorOfBrokenSymlink( const QColor &color ) { m_brokenSymlinkColor = color; }
// 	bool               removeSelection( FileInfoList *fileInfoList );

// public slots:
// 	void slotSetSelected( FileInfoList *pFileInfoList, bool bSelect ) {
// 		setSelected(pFileInfoList, bSelect, true, true);
// 	}

private:
	VfsFlatListModel      *m_sourceModel;
	VfsFlatListProxyModel *m_proxyModel;

	FileInfoToRowMap *m_selectionMap;
	FileInfoList *m_listOfSeleted;

	qint64 m_weighOfSelected;

	QColor m_cursorColor;
	QColor m_commonItemColor;
	QColor m_selecttedItemColor;
	QColor m_itemUnderCursorColor;
	QColor m_hiddenFgColor;
	QColor m_executableFgColor;
	QColor m_symlinkColor;
	QColor m_brokenSymlinkColor;
	QColor m_selectedItemUnderCursorColor;

	void selectByPattern_Processing( bool bMark, const QRegExp &pattern );

Q_SIGNALS:
	void signalSelectionChanged();

};

#endif // VFSFLATLISTSELECTIONMODEL_H
