/***************************************************************************
 *   Copyright (C) 2010 by Mariusz Borowski <b0mar@nes.pl>                 *
 *   Copyright (C) 2010 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _VFSFLATLISTMODEL_H
#define _VFSFLATLISTMODEL_H

#include <QColor>
#include <QAbstractItemModel>

#include "treeview.h"
#include "fileinfo.h"
//#include "vfsmodel.h"

class QIcon;
class IconCache;
class VfsFlatListSelectionModel;

using namespace Vfs;
/**
	@author Piotr Mierzwiński <piom@nes.pl>
	@author Mariusz Borowski <b0mar@nes.pl>
 */
//class VfsFlatListModel : public VfsModel // base class inherit from QAbstractItemModel
class VfsFlatListModel : public QAbstractItemModel // remove when support for TREE and FLAT list (use VfsModel) have be implemented
{
	Q_OBJECT
public:
	VfsFlatListModel( const QString &sName, bool bFindFileResultMode=false );
	virtual ~VfsFlatListModel();

	virtual QVariant      headerData( int section, Qt::Orientation orientation, int role = Qt::DisplayRole ) const;
	//virtual bool          setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role = Qt::EditRole);

	virtual QModelIndex   parent( const QModelIndex &index ) const;

	virtual int           columnCount( const QModelIndex &parent = QModelIndex() ) const;

	virtual int           rowCount( const QModelIndex &parent = QModelIndex() ) const;

	virtual QVariant      data( const QModelIndex& index, int role = Qt::DisplayRole ) const;
	virtual bool          setData( const QModelIndex &index, const QVariant &value, int role = Qt::EditRole );

	virtual bool          updateRowsData( int column, const QStringList &listOfItemsToModify, FileInfoList *pFileInfoList );
	virtual bool          updateRowData( int column, const QString &itemNameToMofify, FileInfo *pFileInfo );

	virtual QModelIndex   index( int row, int column = 0, const QModelIndex &parent = QModelIndex() ) const;

	virtual Qt::ItemFlags flags( const QModelIndex &index ) const;

	virtual bool          hasChildren( const QModelIndex &parent = QModelIndex() ) const; // don't remove (speeds up showing items after reading directory)

// 	virtual bool          insertRows( int position, int rows, const QModelIndex &parent = QModelIndex() );

// 	virtual bool          insertColumns( int position, int columns, const QModelIndex &parent = QModelIndex() );
// 	virtual bool          removeColumns( int position, int columns, const QModelIndex &parent = QModelIndex() );


	void                insertItems( FileInfoList *pFileInfoList, const QString &sFindResultName );
	bool                insertItems( FileInfoList *pFileInfoList, int nPosition=0, bool bNoDuplicateNames=false );
	bool                removeItems( FileInfoList *pFileInfoList );

	void                setSelectionModel( VfsFlatListSelectionModel *pSelectionModel ) { m_selectionModel = pSelectionModel; }
	VfsFlatListSelectionModel *selectionModel() const { return m_selectionModel; }

	/** Returns information type FileInfo about file poited out by index.
	 * @param index index of item in current model
	 */
	FileInfo            & fileInfo( const QModelIndex &index ) const;
	FileInfo            * fileInfoPtr( const QModelIndex &index );
	FileInfo            * fileInfoPtr( const QString &sFileName, const QString &sFilePath="" );

	qint64              fileSize( FileInfo *pFI, bool bFollowByLink=false );
//	IconCache          *iconCache() const { return m_pIconCache; }

	bool                bytesRound() const { return m_bBytesRound; }
	void                setBytesRound( bool round ) { m_bBytesRound = round; }

	bool                showIcons() const { return m_bShowIcons; }
	void                setShowIcons( bool showIcons ) { m_bShowIcons = showIcons; }

	bool                selectDirs() const { return m_bSelectDirs; }
	void                setSelectDirs( bool bSelectDirs ) { m_bSelectDirs = bSelectDirs; }

	bool                boldDirs() const { return m_bBoldDirs; }
	void                seBoldDirs( bool bBold ) { m_bBoldDirs = bBold; }


	FileInfoList        fileInfoList() const { return m_fileInfoList; } // TODO change this into pointer
	//FileInfoList       *fileInfoListPtr() const { return (dynamic_cast <FileInfoList *>(&m_fileInfoList)); }

	void                setFileInfoList( const FileInfoList &fileInfoList ) { m_fileInfoList = fileInfoList; }


	void                beginResetVfsModel() { beginResetModel(); } // workaround for SubsystemManager, becouse target method is protected
	void                endResetVfsModel()   { endResetModel();   } // workaround for SubsystemManager, becouse target method is protected

	void                setUpdateData( bool updateData ) { m_updateDataFlag = updateData; }

	/** Calls setData method with saved agruments: QModelIndex and QVariant.
	  * Useful when the setData method is called before (for example, for remote subsystem).
	  * @return true if set data successful, otherwise false.
	  */
	bool                setDataPost();

	bool                setRowData( const QModelIndex &idx, FileInfo *fileInfo, int role = Qt::EditRole );

// 	void                setFollowByLink( bool bFollowByLink ) { m_bFollowByLink = bFollowByLink; }

	bool                findFileResultMode() const { return m_bFindFileResultMode; }

	FileInfoList       *inportDataModel( const QString &sFindResultName );
	void                setColumns( bool bFindFileResultMode );
	void                exportDataModel();

	void                flmBeginResetModel() { beginResetModel(); }
	void                flmEndResetModel() { endResetModel(); }

Q_SIGNALS:
	void                signalModelChanged();
// 	void                signalSetSelected( FileInfoList *pFileInfoList, bool bSelect );

private:
	QList<QString>      m_headerItems;
	FileInfoList        m_fileInfoList;
	VfsFlatListSelectionModel *m_selectionModel;

	IconCache          *m_pIconCache;
	bool                m_bFindFileResultMode;
	bool                m_updateDataFlag;

	bool                m_bBytesRound;
	bool                m_bShowIcons;
	bool                m_bBoldDirs;
	bool                m_bSelectDirs;

	QVariant            m_setDataValue;
	QModelIndex         m_setDataIndex;

	qint64              m_selectionWeigh;

	QString             m_sFindResultName;
	FileInfoList       *m_FindResultFileInfoList;

// 	bool                m_bFollowByLink;

};


#endif // _VFSFLATLISTMODEL_H
