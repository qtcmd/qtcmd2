/***************************************************************************
 *   Copyright (C) 2018 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef LISTVIEWITEMDELEGATE_
#define LISTVIEWITEMDELEGATE_

#include <QPainter>
#include <QStyledItemDelegate> // was: QItemDelegate

#include "treeview.h"
#include "vfsflatlistselectionmodel.h"

class IconCache;

class ListViewItemDelegate : public QStyledItemDelegate // was: QItemDelegate
{
public:
	ListViewItemDelegate( TreeView *pTreeView );
	explicit ListViewItemDelegate( QObject *pParent );

	QWidget *createEditor( QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index ) const;
	void setEditorData( QWidget *editor, const QModelIndex &index ) const;
	void setModelData( QWidget *editor, QAbstractItemModel *model, const QModelIndex &index ) const;
	void updateEditorGeometry( QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index ) const;
	void paint( QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index ) const;

	bool returnPressed() const { return m_returnPressed; }

	void setParentObj( QObject *parent );

private:
	bool m_returnPressed;
	IconCache *m_pIconCache;
	TreeView *m_pTreeView;

	VfsFlatListModel *m_pModel;
	VfsFlatListSelectionModel *m_selectionModel;

	QString squeezeString( const QString &sStr, uint nCellWidthInPixels, const QFont &font, QPaintDevice *pDevice ) const;

protected:
	bool eventFilter( QObject *editor, QEvent *event );
	bool editorEvent( QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index );

};

#endif // LISTVIEWITEMDELEGATE_
