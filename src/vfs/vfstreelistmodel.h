/***************************************************************************
 *   Copyright (C) 2008 by Mariusz Borowski <b0mar@nes.pl>                 *
 *   Copyright (C) 2009 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef VFSTREELISTMODEL_H
#define VFSTREELISTMODEL_H

#include "vfsmodel.h"

class QIcon;
class IconCache;


class VfsTreeListNode
{
public:
	VfsTreeListNode(VfsTreeListNode *parent = 0) : m_parent(parent), m_populated(false) {}
	virtual ~VfsTreeListNode() { qDeleteAll(m_children); }

	VfsTreeListNode *m_parent;
	bool m_populated;
	VfsFileInfo m_itemInfo;
	QVector<VfsTreeListNode*> m_children;

};


/**
	@author Mariusz Borowski <b0mar@nes.pl>
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class VfsTreeListModel : public VfsModel // inherit from QAbstractItemModel
{
//	Q_OBJECT
public:
	VfsTreeListModel( Subsystem *subsystem );
	virtual ~VfsTreeListModel();

	virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

	virtual int columnCount(const QModelIndex & parent = QModelIndex()) const;

	virtual int rowCount(const QModelIndex & parent = QModelIndex()) const;

	virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

	virtual QModelIndex index(int row, int column = 0, const QModelIndex & parent = QModelIndex()) const;

	virtual QModelIndex parent(const QModelIndex & index) const;

    //virtual bool hasChildren(const QModelIndex & parent = QModelIndex()) const;
    bool hasChildren(const QModelIndex & parent = QModelIndex()) const;

	//virtual void sort( int column, Qt::SortOrder order = Qt::AscendingOrder );

	void setRootNode(const QString &path);

	/** Returns current (opened) vfs name;
	@return vfs name (example: local_fs);
	*/
	QString getCurrentVfsName() const { return m_vfs->vfsName(); }

	/** Returns subfilesystem name related to the passed file.
	@param sFullFileName file name that contains absolute path.
	@return name of subfilesystem or enpty string (if not found subfilesystem)
	*/
	QString vfsSubsystemName( const QString &sFullFileName );


	/** Execute file view for gived file name.
	@param sFullFileName file name that contains absolute path.
	@return view status (error of view).
	*/
	Vfs::VfsError fileView( const QString &sFullFileName ) {  return m_vfs->fileView(sFullFileName);  }

	/** Detects file is executable.
	@param sFullFileName file name that contains absolute path.
	@return TRUE if file is executable otherwise FALSE.
	*/
	bool isExecutableFile( const QString &sFullFileName ) {  return m_vfs->isExecutableFile(sFullFileName); }

	/** Run procedure that run gived file.
	@param sFullFileName file name that contains absolute path.
	*/
	void runFile( const QString &sFullFileName ) {  m_vfs->runFile(sFullFileName);  }


private:
	void populate(VfsTreeListNode * parent) const;

	static const int NUMBER_OF_COLUMNS = 7;
	QList<QString>  m_headerItems;
	Vfs  *m_subsystem;
	mutable VfsTreeListNode *m_rootNode;

	IconCache *m_pIconCache;
	QDir::SortFlags m_sort;

};

#endif // VFSTREELISTMODEL_H
