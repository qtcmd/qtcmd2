/***************************************************************************
 *   Copyright (C) 2005 by Mariusz Borowski <b0mar@nes.pl>                 *
 *   Copyright (C) 2009 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "settings.h"
#include "pathview.h" // only for PathView::cleanPath
#include "mimetype.h"
#include "fileviewer.h"
#include "messagebox.h"
#include "pluginfactory.h"
#include "inputtextdialog.h"
#include "systeminfohelper.h"
#include "subsystemmanager.h"
#include "overwritefiledialog.h"
#include "vfsflatlistproxymodel.h"
#include "contextmenucmdids.h"
#include "connectiondialog.h"

#include <QDir>
#include <QDebug>
#include <QProcess>
#include <QFileDialog>
#include <QApplication>

static bool s_bCloseProgressDlgWhenCopyFinished;
static bool s_bCloseProgressDlgWhenMoveFinished;
static bool s_bCloseProgressDlgWhenDeleteFinished;
static bool s_bCloseProgressDlgWhenExtractFinished;
static bool s_bCloseProgressDlgWhenDownloadFinished;
static bool s_bCloseProgressDlgWhenUploadFinished;
static bool s_bCloseProgressDlgWhenArchivFinished;
static bool s_bAlwaysOverwriteWhenNewArchive;
static bool s_bAlwaysOverwriteWhenAddToArch;
static bool s_bAlwaysOverwriteInCaseCpMvDwExt;
static bool s_bWeighBeforeArchivng;
static bool s_bWeighBeforeRemoving;
static bool s_bAlwaysAskBeforeRemoving;
static bool s_bOpenDirAfterCreationIt;


SubsystemManager::SubsystemManager( const URI &prevUri, const QString &sPanelName, KeyShortcuts *pKeyShortcuts, QWidget *pParent )
	: m_sPanelName(sPanelName), m_pMainParent(pParent)
{
	m_OpenFile = false;
	m_pFileViewer = nullptr;
	m_subsystem = nullptr;
	m_vfsModel = nullptr;
	m_vfsModelCompl = nullptr;
	m_selectionModel = nullptr;
	//m_bPostCreatingAction = false;
	m_bOperationCompleted = false;
	m_bOpenInOppositeActiveTab = false;
	m_bUpdateModelAfterWeighing = false;
	m_bMoveToArchive = false;
	m_bSingleArchive = false;

	m_ffdItemRemoved = nullptr;
	m_pMultiProgressDlg = nullptr;
	m_pFileSelectionMap = nullptr;

	Settings settings;
	s_bCloseProgressDlgWhenCopyFinished    = settings.value("vfs/CloseProgressDlgWhenCopyFinished", false).toBool();
	s_bCloseProgressDlgWhenMoveFinished    = settings.value("vfs/CloseProgressDlgWhenMoveFinished", false).toBool();
	s_bCloseProgressDlgWhenDeleteFinished  = settings.value("vfs/CloseProgressDlgWhenDeleteFinished", false).toBool();

	s_bCloseProgressDlgWhenArchivFinished  = settings.value("vfs/CloseProgressDlgWhenArchivingFinished", false).toBool();
	s_bCloseProgressDlgWhenExtractFinished = settings.value("vfs/CloseProgressDlgWhenExtractingFinished", false).toBool();

	s_bCloseProgressDlgWhenDownloadFinished = settings.value("vfs/CloseProgressDlgWhenDownloadFinished", false).toBool();
	s_bCloseProgressDlgWhenUploadFinished   = settings.value("vfs/CloseProgressDlgWhenUploadFinished", false).toBool();

	s_bAlwaysOverwriteWhenNewArchive  = settings.value("vfs/AlwaysOverwriteWhenNewArchive", true).toBool(); // TODO: implement AlwaysOverwriteExistingArchive for value "false"
	s_bAlwaysOverwriteWhenAddToArch   = settings.value("vfs/AlwaysOverwriteWhenAddToArch", true).toBool(); // TODO: implement AlwaysOverwriteExistingArchive for value "false"
	s_bAlwaysOverwriteInCaseCpMvDwExt = settings.value("vfs/AlwaysOverwriteInCaseCpMvDwExt", true).toBool();
	s_bAlwaysAskBeforeRemoving        = settings.value("vfs/AskBeforeRemoving", true).toBool();

	s_bOpenDirAfterCreationIt   = settings.value("vfs/OpenDirAfterCreationIt", true).toBool();
	s_bWeighBeforeRemoving      = settings.value("vfs/WeighBeforeRemoving", true).toBool();
	s_bWeighBeforeArchivng      = settings.value("vfs/WeighBeforeArchiving", true).toBool();
//	s_bAlwaysSavePermission     = settings.value("vfs/AlwaysSavePermission", true).toBool();

	m_bTopOpenWithAppUseAsDefault = settings.value("filelistview/TopOpenWithAppUseAsDefault", true).toBool();
	m_sTmpDir = settings.value("vfs/TempDir", QDir::tempPath()+"/qtcmd2/").toString();

	m_bShowMsgCantRmFileAgain  = true;
	m_bShowMsgCantSetAttrAgain = true;
	m_bShowMsgCantCpAgain = true;
	m_bShowMsgCantMvAgain = true;
	m_bFileRenamed = false;
	m_bExtractingFromLocalSubsys = true;
	m_bArchivingFromLocalSubsys  = false;
	m_bWeighBeforeArchExtracting = true;
	m_bOpenArchInRemoteLocation  = false;

	m_pSelectedItemToView = new FileInfoList;

	m_lastStatus = UNKNOWN_STAT;
	m_prevURI = prevUri;
	setObjectName(sPanelName);
	m_nViewerCounter = 0;
	m_progressDlgCounter = 0;
	m_nTotalProcessed = 0;
	m_nCounterOfProcessed = 0; // used in m_pMultiProgressDlg
	m_selectedForDownloadBeforExtracting = new FileInfoToRowMap;
	m_pHelperSelectionMap = new FileInfoToRowMap;

	m_pKeyShortcuts = pKeyShortcuts;
	qDebug() << "SubsystemManager::SubsystemManager." << m_sPanelName;
}

SubsystemManager::~SubsystemManager()
{
	qDebug() << "SubsystemManager::~SubsystemManager." << m_sPanelName;
	foreach(ProgressDialog *pDlg, m_progressDlgPtrTable) {
		delete pDlg;
	}
	m_progressDlgPtrTable.clear();
	m_selectedForDownloadBeforExtracting->clear();
	delete m_selectedForDownloadBeforExtracting;
	m_pHelperSelectionMap->clear();
	delete m_pHelperSelectionMap;
	delete m_pMultiProgressDlg;

	Settings settings;
	settings.update("vfs/CloseProgressDlgWhenCopyFinished", s_bCloseProgressDlgWhenCopyFinished);
	settings.update("vfs/CloseProgressDlgWhenMoveFinished", s_bCloseProgressDlgWhenMoveFinished);
	settings.update("vfs/CloseProgressDlgWhenDeleteFinished", s_bCloseProgressDlgWhenDeleteFinished);

	settings.update("vfs/CloseProgressDlgWhenArchivingFinished", s_bCloseProgressDlgWhenArchivFinished);
	settings.update("vfs/CloseProgressDlgWhenExtractingFinished", s_bCloseProgressDlgWhenExtractFinished);

	settings.update("vfs/CloseProgressDlgWhenDownloadFinished", s_bCloseProgressDlgWhenDownloadFinished);
	settings.update("vfs/CloseProgressDlgWhenUploadFinished", s_bCloseProgressDlgWhenUploadFinished);

	settings.update("vfs/AlwaysOverwriteWhenNewArchive", s_bAlwaysOverwriteWhenNewArchive);
	settings.update("vfs/AlwaysOverwriteWhenAddToArch", s_bAlwaysOverwriteWhenAddToArch);
	settings.update("vfs/AlwaysOverwriteInCaseCpMvDwExt", s_bAlwaysOverwriteInCaseCpMvDwExt);
	settings.update("vfs/AskBeforeRemoving", s_bAlwaysAskBeforeRemoving);

	settings.update("vfs/OpenDirAfterCreationIt", s_bOpenDirAfterCreationIt);
	settings.update("vfs/WeighBeforeRemoving", s_bWeighBeforeRemoving);
//	settings.update("vfs/AlwaysSavePermission", s_bAlwaysSavePermission);

	delete m_pSelectedItemToView;
	m_FileInfoToMime_map.clear();
}

void SubsystemManager::setVfsModel( VfsFlatListModel *pVfsModel )
{
	m_vfsModel = pVfsModel;
	m_selectionModel = m_vfsModel->selectionModel();
}

void SubsystemManager::applySettings( const FileSystemSettings &fileSystemSettings )
{
	s_bCloseProgressDlgWhenCopyFinished    = fileSystemSettings.closeProgressDlgWhenCopyFinished;
	s_bCloseProgressDlgWhenMoveFinished    = fileSystemSettings.closeProgressDlgWhenMoveFinished;
	s_bCloseProgressDlgWhenDeleteFinished  = fileSystemSettings.closeProgressDlgWhenDeleteFinished;

	s_bCloseProgressDlgWhenArchivFinished  = fileSystemSettings.closeProgressDlgWhenArchivFinished;
	s_bCloseProgressDlgWhenExtractFinished = fileSystemSettings.closeProgressDlgWhenExtractFinished;

	s_bCloseProgressDlgWhenDownloadFinished = fileSystemSettings.closeProgressDlgWhenDwonloadFinished;
	s_bCloseProgressDlgWhenUploadFinished   = fileSystemSettings.closeProgressDlgWhenUploadFinished;

	s_bAlwaysOverwriteInCaseCpMvDwExt = fileSystemSettings.alwaysOverwriteInCaseCpMvDwExt;
	s_bAlwaysOverwriteWhenNewArchive  = fileSystemSettings.alwaysOverwriteWhenArchiving;
	s_bAlwaysOverwriteWhenNewArchive  = fileSystemSettings.alwaysOverwriteWithNewArchive;
	s_bAlwaysAskBeforeRemoving        = fileSystemSettings.alwaysAskBeforeRemoving;

	s_bOpenDirAfterCreationIt         = fileSystemSettings.openDirAfterCreationIt;
	s_bWeighBeforeRemoving            = fileSystemSettings.alwaysWeighBeforeRemove;
// 	fileSystemSettings.alwaysSavePermission  // loaded in LocalSubsystem
}



void SubsystemManager::initURI( const URI &uri ) // called in Panel in case of creating FlatFileListView
{
	QString sMime = MimeType::getMimeFromPath(uri.toString());
	if (sMime.isEmpty())
		sMime = uri.mime();
	qDebug() << "SubsystemManager::initURI. PanelName:" << m_sPanelName << "input uri:" << uri.toString() << "Mime:" << sMime << "uri.protocol:" << uri.protocol();

	m_URI = uri;
	m_URI.setMime(sMime); // init mime for URI
	//m_URI.setMime(MimeType::getFileMime(uri.path())); // FIXME: MimeType::getFileMime for / returns octet-stream
	if (m_URI.mime().contains("octet-stream"))
		m_URI.setMime("folder");
}


void SubsystemManager::createNewEmptyFile( const QModelIndex &currentIndex, bool bCreateFile, QWidget *pParent )
{
	qDebug() << "SubsystemManager::createNewEmptyFile.";
	if (m_vfsModel->findFileResultMode())
		return;

	FileInfo *pFileInfo = m_vfsModel->fileInfoPtr(currentIndex);
	QString sInputName = pFileInfo->fileName();
	if (sInputName == "..")
		sInputName = "";

// 	slotBreakCurrentOperation(); // in this moment called in FileListView

	QString sName;
	bool bOpenFileAfterCreationIt = false;
	if (bCreateFile)
		sName = InputTextDialog::getText(pParent, InputTextDialog::MAKE_FILE, sInputName, bOpenFileAfterCreationIt);
	else
		sName = InputTextDialog::getText(pParent, InputTextDialog::MAKE_DIR,  sInputName, s_bOpenDirAfterCreationIt);

	if (sName.isEmpty())
		return;

	if (m_URI.mime().startsWith("remote/")) {
		if (sName.contains('/')) {
			MessageBox::msg(pParent, MessageBox::WARNING, tr("Sorry. Recursive operations are not allowed!.\nWhole path will be removed."), sName);
			sName = sName.right(sName.length()-sName.lastIndexOf('/')-1);
		}
	}

	// -- simple parser
	QString sFileName = QDir::cleanPath(sName);
	QString sep       = QDir::separator();
	QString sTildeSep = "~"+sep;
	QString sCurrentPath;
	QString sHost = m_URI.host();
	if (sHost.isEmpty())
		sHost = "/";

	sFileName.replace(QRegExp("\\s+"+sHost), sHost);
	if (sFileName.contains(sTildeSep))
		sCurrentPath = QDir::homePath()+sep;

	switchSubsystem(PluginManager::instance(), m_URI.mime()); // making sure, that operation will be invoked on properly subsystem
	// -- update path
	if (m_URI.mime() == "folder") {
		if (! sFileName.startsWith(m_subsystem->root()->filePath()))
			sCurrentPath = m_URI.path();
	} else {                             // non LocalSubsystem
		if (! sFileName.startsWith(m_URI.protocolWithSuffix()))
			sCurrentPath = m_URI.path();
	}
	if (bCreateFile) {
		if (sFileName.endsWith(sep))
			sFileName = sFileName.length()-1;
	}
	else { // dir
		if (! sFileName.endsWith(sep))
			sFileName += sep;
	}
	if (bCreateFile)
		m_newlyCreatedEmpyFileName = sFileName; // used for opening view window

// 	qDebug() << "SubsystemManager::createNewEmptyFile. -- reconnect signalShowFileOverwriteDlg - current name:" << m_sPanelName;
	disconnect(m_subsystem, &Subsystem::signalShowFileOverwriteDlg, this, &SubsystemManager::slotShowFileOverwriteDlg);
	connect(m_subsystem, &Subsystem::signalShowFileOverwriteDlg, this, &SubsystemManager::slotShowFileOverwriteDlg);

	bCreateFile ? m_subsystem->makeFile(sCurrentPath, sFileName, m_sPanelName) : m_subsystem->makeDir(sCurrentPath, sFileName, m_sPanelName);
}


void SubsystemManager::weighItem( FileInfo *pFileInfo, bool bUpdateModelAfterWeighing )
{
	qDebug() << "SubsystemManager::weighItem. Name:" << pFileInfo->fileName();
	m_bUpdateModelAfterWeighing = bUpdateModelAfterWeighing;
	QString sMime = pFileInfo->mimeInfo();
	if (! m_URI.host().isEmpty() && ! m_URI.mime().startsWith("remote/")) // inside of archive
		sMime = m_URI.mime();

	bool bPluginSupport = PluginFactory::instance()->isPluginAvailable(sMime, "subsystem");
	if (! bPluginSupport) { // this is NOT compressed file
		switchSubsystem(PluginManager::instance(), sMime); // making sure, that operation will be invoked on properly subsystem. For regular file will be returned error, but we can ignore it
	}
	m_sTargetPathToExtracting = pFileInfo->absoluteFileName(); // required for switching to properly subsystem in slotSubsystemStatusChanged()
	switchSubsystem(PluginManager::instance(), sMime); // making sure, that operation will be invoked on properly subsystem
	m_subsystem->weighItem(pFileInfo, m_URI, m_sPanelName);
}

void SubsystemManager::weighSelectedItems( FileInfoToRowMap *pSelectionMap )
{
	qDebug() << "SubsystemManager::weighSelectedItems.";
	m_bUpdateModelAfterWeighing = false;
	switchSubsystem(PluginManager::instance(), m_URI.mime()); // making sure, that operation will be invoked on properly subsystem
	m_subsystem->weigh(pSelectionMap, m_URI, m_sPanelName, false);
}


void SubsystemManager::removeFiles( FileInfoToRowMap *pSelectionMap, QWidget *pParent )
{
	qDebug() << "SubsystemManager::removeFiles.";
	// very abbreviated checking if one given correct class pointer in pParent parameter
	if (pParent != NULL) { // pParent is NULL for calls from FindFileDialog
		if (! pParent->objectName().startsWith("leftPanel_") && ! pParent->objectName().startsWith("rightPanel_")) {
			qDebug() << "SubsystemManager::removeFiles. Internal ERROR. Incorrect class passed in pParent parameter!";
			return;
		}
	}
// 	slotBreakCurrentOperation(); // in this moment called in FileListView
	int nNumOfSelected = (pParent == NULL) ? 1 : dynamic_cast <FileListView *> (pParent)->numberOfSelectedItems();  // if no one selected then select current item, except ".."
	if (nNumOfSelected == 0) // if item ".."
		return;

	//Settings settings;
	//bool bDeleteConfirmation  = settings.value("vfs/DeleteConfirmation", true).toBool();
	//bool bWeighBeforeRemoving = settings.value("vfs/WeighBeforeRemoving", true).toBool();

	int nResult;
//	if (m_sDeleteConfirmation && ! m_bDeleteWithoutConfirmation) {
	if (s_bAlwaysAskBeforeRemoving) {
		QString sFImsg, sInfo;
		if (nNumOfSelected > 1) {
			sFImsg = m_selectionModel->selectedItemsMsg();
			sInfo = tr("them");
		}
		else { // one item selected
			FileInfo *pFileInfo = pSelectionMap->begin().key();
			sFImsg = "\""+pFileInfo->fileName()+"\"";
			sInfo = (pFileInfo->isDir() && ! pFileInfo->isSymLink()) ? tr("directory") : tr("file");
		}
		nResult = MessageBox::yesNo(pParent, tr("Removing ..."), sFImsg,
									tr("Are you sure you want to remove")+" "+sInfo+"?", s_bWeighBeforeRemoving,
									MessageBox::Yes, // default focused button
									tr("Weigh before removing")
		);
		if (nResult == 0) { // user closed dialog
			if (pParent != NULL)
				dynamic_cast <FileListView *> (pParent)->selectAll(false); // deselect all selected
			return;
		}
	}
	else
		nResult = MessageBox::Yes;

//	Settings settings;
//	settings.update("vfs/WeighBeforeRemoving", m_sWeighBeforeRemoving);
//	if (! s_bDeleteWithoutConfirmation)
//		settings.update("vfs/DeleteConfirmation", m_sDeleteConfirmation);

	// -- maybe user abandon removing
	if (nResult != MessageBox::Yes) { // unmark all selected, thanks that in next operation user will avoid mistake taking previous marked items
		m_selectionModel->selectAll(false); // deselect all selected
		return;
	}
	if (pParent == NULL) {
		m_ffdItemRemoved = NULL;
		if (pSelectionMap->count() == 1)
			m_ffdItemRemoved = pSelectionMap->begin().key();
	}
	startRemoveFiles(pSelectionMap, s_bWeighBeforeRemoving);
}

void SubsystemManager::startRemoveFiles( FileInfoToRowMap *pSelectionMap, bool bWeighBeforeRemoving ) // called also from opposite panel (moving files to archvive)
{
	switchSubsystem(PluginManager::instance(), m_URI.mime()); // making sure, that operation will be invoked on properly subsystem
	showProgressDialog(bWeighBeforeRemoving, Vfs::REMOVE); // removed in slotSubsystemStatusChanged
	m_subsystem->remove(pSelectionMap, m_URI, bWeighBeforeRemoving, m_sPanelName);
}

void SubsystemManager::setAttributes( FileInfoToRowMap *pSelectionMap, FileAttributes *pChangedAttrib, bool bWeighBefore )
{
	qDebug() << "SubsystemManager::setAttributes";
	bool bSingleFile = false;
	if (pSelectionMap->size() == 1) {
		FileInfo *pFileInfo = pSelectionMap->begin().key();
		bSingleFile = (pFileInfo->isSymLink() || ! pFileInfo->isDir());
	}
	switchSubsystem(PluginManager::instance(), m_URI.mime()); // making sure, that operation will be invoked on properly subsystem
	if (! bSingleFile) {
		showProgressDialog(bWeighBefore, Vfs::SET_ATTRIBUTES_CMD); // removed in slotSubsystemStatusChanged
		ProgressDialog *pProgressDlg = m_progressDlgPtrTable.at(m_progressDlgCounter-1);
		pProgressDlg->setAttributesChangeForInfo(pChangedAttrib->sChangesForMsg);
	}
	m_subsystem->setAttributes(pSelectionMap, m_URI, pChangedAttrib, bWeighBefore, m_sPanelName);
}

void SubsystemManager::findFiles( const FindCriterion &fcFindCriterion, FileInfoToRowMap *pFoundFileSelectionMap )
{
	URI uri(fcFindCriterion.location);
	qDebug() << "SubsystemManager::startFindFiles. mime:" << uri.mime() << "path:" << uri.path();
 	switchSubsystem(PluginManager::instance(), uri.mime()); // making sure, that operation will be invoked on properly subsystem
	m_subsystem->searchFiles(pFoundFileSelectionMap, uri, fcFindCriterion, m_sPanelName);
}

void SubsystemManager::copyFiles( FileInfoToRowMap *pSelectionMap, bool bCopy, QWidget *pParent )
{
	if (! m_URI.host().isEmpty() && ! m_URI.mime().startsWith("remote/")) { // inside of archive
		extractFilesTo(pSelectionMap, ! bCopy, pParent);
		return;
	}

	QString sFN = "SubsystemManager::copyFiles.";
	if (pSelectionMap == NULL) {
		qDebug() << sFN << "ERROR. pSelectionMap is NULL or empty!";
		return;
	}
	if (pParent == NULL) {
		qDebug() << sFN << "ERROR. pParent is NULL!";
		return;
	}
	// very abbreviated checking if one given correct class pointer in pParent parameter
	if (! pParent->objectName().startsWith("leftPanel_") && ! pParent->objectName().startsWith("rightPanel_")) {
		qDebug() << sFN << "Internal ERROR. Incorrect class passed in pParent parameter!";
		return;
	}

	qDebug() << sFN << (bCopy ? "Copy" : "Move") << "selected item(s)";
	QString sInputName;
	emit signalGetPathFromOppositeActiveTab(sInputName);
	if (sInputName.startsWith("://")) { // findFileResultMode (target panel)
		MessageBox::msg(pParent, MessageBox::WARNING, tr("Sorry. You can't copy or move file(s).\nTarget file system is read only!"));
		return;
	}
// 	slotBreakCurrentOperation(); // in this moment called in FileListView
	int nNumOfSelected = dynamic_cast <FileListView *> (pParent)->numberOfSelectedItems();  // if no one selected then selects current item, except ".."
	if (nNumOfSelected == 0) // if item ".."
		return;

	bool bWeighBefore;
	InputTextDialog::GetTextMode gtm = (bCopy ? InputTextDialog::COPY : InputTextDialog::MOVE);
	URI inURI(sInputName);
	if (inURI.mime().startsWith("remote/")) {
		gtm = InputTextDialog::UPLOAD;
		sInputName = inURI.protocolWithSuffix()+inURI.host()+inURI.path();
	}
	if (sInputName.startsWith(QDir::rootPath()) && ! inURI.mime().startsWith("remote/") && ! inURI.host().isEmpty()) { // archive
		gtm = InputTextDialog::ADD_TO_ARCH;
	}
	QString sInputLabel = bCopy ? "<b>"+tr("Copy")+"</b> " : "<b>"+tr("Move")+"</b> ";
	if (gtm == InputTextDialog::ADD_TO_ARCH)
		sInputLabel = bCopy ? "<b>"+tr("Add")+"</b> " : "<b>"+tr("Archive and remove")+"</b> ";
	if (nNumOfSelected > 1) {
		sInputLabel += m_selectionModel->selectedItemsMsg(false);
	}
	else { // one item selected
		FileInfo *pFileInfo = pSelectionMap->begin().key();
		QString sType = (pFileInfo->isDir() && ! pFileInfo->isSymLink()) ? tr("directory") : tr("file");
		sInputLabel += "(" + sType + "): ";
		sInputLabel += "\""+pFileInfo->fileName()+"\"";
	}
	QString sTargetPath = InputTextDialog::getText(pParent, gtm, sInputName, bWeighBefore, sInputLabel);
	if (sTargetPath.isEmpty()) {
		m_selectionModel->selectAll(false); // deselect all selected
		return;
	}

	if (! m_URI.mime().startsWith("remote/")) { // copy/move from relative path  (local filesystem or archive)
		if (! sTargetPath.startsWith(QDir::rootPath()) && ! URI(sTargetPath).mime().startsWith("remote/")) {
			QString sHost = m_URI.host();
			QString sCurrentPath = m_URI.path();
			if (! sHost.isEmpty()) // archive subsystem
				sCurrentPath.insert(0, sHost);
			sTargetPath.insert(0, sCurrentPath);
			sTargetPath =  QDir::cleanPath(sTargetPath); // resolve all ".."
			sTargetPath += QDir::separator();
		}
		else { // upload to ftp/add to archive or just copy/move file from right to left panel. This operation must be executed by target subsystem
			emit signalCopyFilesFromOppositePanel(sTargetPath, pSelectionMap, bCopy, bWeighBefore); // caught by Panel and sent to MainWindow and MainWindow calls method for opposite panel
			return;
		}
	}

	qDebug() << sFN << "CopyMode:" << bCopy << ", selected:" << pSelectionMap->size() << "items, targetPath:" << sTargetPath << "currentPanel:" << m_sPanelName;
	QString sTargetMime = URI(sTargetPath).mime();
	QString sMime = m_URI.mime();
	if (sMime == sTargetMime)
		sMime = sTargetMime;
	PluginManager *pPluginManager = PluginManager::instance();
	if (pPluginManager->isNewSubsystem(sMime) || m_subsystem == NULL) {
		qDebug() << sFN << "Get subsystem for Mime:" << sMime;
		switchSubsystem(pPluginManager, sMime);
	}
	copyFilesTo(sTargetPath, pSelectionMap, bCopy, bWeighBefore);
}

void SubsystemManager::copyFilesTo( const QString &sTargetPath, FileInfoToRowMap *pSelectionMap, bool bCopy, bool bWeighBefore ) // method invoked from opposite panel
{
	Vfs::SubsystemCommand subSysCmd = bCopy ? Vfs::COPY : Vfs::MOVE;
	URI inURI(sTargetPath);
	if (sTargetPath.startsWith(QDir::rootPath()) && ! inURI.mime().startsWith("remote/") && ! inURI.host().isEmpty()) // archive
		subSysCmd = (bCopy ? Vfs::COPY_TO_ARCHIVE : Vfs::MOVE_TO_ARCHIVE);

	switchSubsystem(PluginManager::instance(), m_URI.mime()); // making sure, that operation will be invoked on properly subsystem
	showProgressDialog(bWeighBefore, subSysCmd, sTargetPath);
	m_nTotalWeight = 0;  m_nTotalWeighedItems = 0;
	m_subsystem->copyFiles(pSelectionMap, sTargetPath, ! bCopy, bWeighBefore, m_sPanelName);
}

void SubsystemManager::makeLink( QWidget *pParent )
{
	// very abbreviated checking if one given correct class pointer in pParent parameter
	if (! pParent->objectName().startsWith("leftPanel_") && ! pParent->objectName().startsWith("rightPanel_")) {
		qDebug() << "SubsystemManager::makeLink. Internal ERROR. Incorrect class passed in pParent parameter!";
		return;
	}
	qDebug() << "SubsystemManager::makeLink.";
	// -- check if remote and archive subsystem in panel beside
	QString sTargetPath;
	QString sWarnMsgP1 = tr("Sorry. You can't create link with path:");
	QString sWarnMsgP2 = tr("Target filesystem doesn't support links!");
	emit signalGetPathFromOppositeActiveTab(sTargetPath);
	QString sMime = MimeType::getMimeFromPath(sTargetPath);
	if (sTargetPath.contains("://") || ! sMime.startsWith("folder")) { // findFileResultMode or remote subsystem (in target panel)
		MessageBox::msg(pParent, MessageBox::WARNING, sWarnMsgP1 +"\n\n"+sTargetPath+"\n\n"+ sWarnMsgP2+"\n");
		return;
	}

// 	slotBreakCurrentOperation(); // in this moment called in FileListView
	int nNumOfSelected = dynamic_cast <FileListView *> (pParent)->numberOfSelectedItems();  // if no one selected then selects current item, except ".."
	if (nNumOfSelected == 0) // if item ".."
		return;

	bool bHardLink = false;
	QString sTargetLinkName, sInputName, sLinkName;
	if (nNumOfSelected == 1) {
		FileInfo *pFI = m_selectionModel->firstMarked();
		sInputName = pFI->fileName();
		if (sInputName == "..")
			sInputName = "";

		QString sFileType = pFI->isDir() ? tr("directory") : tr("file");
		QString sInpPath = tr("<p><b>Source name</b> (target")+" "+ sFileType +" "+tr("for new link):");
		sInpPath += "<br>&nbsp;&nbsp;&nbsp;"+ m_URI.path() + sInputName + "<br>";
		QString sNewLinkLabel = tr("<b>New link name:</b>");
		sTargetLinkName = sTargetPath + sInputName;
		sLinkName = InputTextDialog::getText(pParent, InputTextDialog::MAKE_LINK, sTargetLinkName, bHardLink, sNewLinkLabel, QStringList(sInpPath));
		if (sLinkName.isEmpty())
			return;
		// -- check for remote and archive subsystem for target path
		QString sTargetFile = m_URI.path() + sInputName;
		if (! sLinkName.startsWith(QDir::rootPath()))
			sLinkName.insert(0, m_URI.path());
		if (sLinkName == sTargetFile) {
			MessageBox::msg(pParent, MessageBox::WARNING, tr("Cannot create link in the same location with the same name as original")+" "+ sFileType +"!");
			return;
		}
		sMime = MimeType::getMimeFromPath(sLinkName.left(sLinkName.lastIndexOf('/')));
		if (sLinkName.contains("://") || ! sMime.startsWith("folder")) { // findFileResultMode or remote subsystem (in target panel)
			MessageBox::msg(pParent, MessageBox::WARNING, sWarnMsgP1 +"\n\n"+QFileInfo(sLinkName).absolutePath()+"\n\n"+ sWarnMsgP2+"\n");
			return;
		}
		sTargetLinkName = sLinkName;
	}
	else {
		int nResult = MessageBox::yesNo(pParent, tr("Create link"),
									tr("Do you want to create links for all selected files?"), "",
										bHardLink, MessageBox::No, // default focused button
									tr("Hard links")
								);
		if (nResult == MessageBox::No || nResult == 0) {
			m_selectionModel->selectAll(false); // deselect all selected
			return;
		}
		sTargetLinkName = sTargetPath;
	}

	switchSubsystem(PluginManager::instance(), m_URI.mime()); // making sure, that operation will be invoked on properly subsystem
	disconnect(m_subsystem, &Subsystem::signalShowFileOverwriteDlg, this, &SubsystemManager::slotShowFileOverwriteDlg);
	connect(m_subsystem, &Subsystem::signalShowFileOverwriteDlg, this, &SubsystemManager::slotShowFileOverwriteDlg);
	m_subsystem->makeLink(m_selectionModel->selectionMap(), sTargetLinkName, bHardLink, m_sPanelName);
}

void SubsystemManager::editSymLink( const QModelIndex &currentIndex, QWidget *pParent )
{
	qDebug() << "SubsystemManager::editSymLink.";
	FileInfo *pFileInfo = m_vfsModel->fileInfoPtr(currentIndex);
	if (! pFileInfo->isSymLink())
		return;

	QString	sMime = MimeType::getMimeFromPath(m_URI.path());
	if (m_URI.path().contains("://") || ! sMime.startsWith("folder")) { // findFileResultMode or remote subsystem (in current panel)
		MessageBox::msg(pParent, MessageBox::WARNING, tr("Cannot edit symbolic link in this filesystem"));
		return;
	}
// 	slotBreakCurrentOperation(); // in this moment called in FileListView

	QString sSymLink = pFileInfo->absoluteFileName();
	QString sSymLinkTarget = pFileInfo->symLinkTarget();
	QString sMsg = tr("Modify target of symbolic link to:");
	bool bFake = false;

	QString sSymLinkNewTarget = InputTextDialog::getText(pParent, InputTextDialog::EDIT_LINK, sSymLinkTarget, bFake, sMsg);
	if (sSymLinkNewTarget.isEmpty() || sSymLinkNewTarget == sSymLinkTarget)
		return;

	switchSubsystem(PluginManager::instance(), m_URI.mime()); // making sure, that operation will be invoked on properly subsystem
// 	m_subsystemMngr->editSymLink(m_vfsModel->fileInfoPtr(index), sSymLinkNewTarget);
	m_subsystem->editSymLink(pFileInfo, sSymLinkNewTarget, m_sPanelName);
}

// NOTE. before calling this method, please make sure that m_subsystem is pointing to properly subsystem
void SubsystemManager::showProgressDialog( bool bWeighBefore, Vfs::SubsystemCommand subSysCmd, const QString &sTargetPath )
{
	ProgressDialog *pProgressDlg = new ProgressDialog(m_pMainParent, true, m_progressDlgCounter); // true - set modal dialog
	m_progressDlgPtrTable.append(pProgressDlg);
	m_progressDlgCounter++;

	connect(pProgressDlg, &ProgressDialog::signalCancel, this, &SubsystemManager::slotProgressDialogCancel);
	connect(pProgressDlg, &ProgressDialog::signalBackground, this, &SubsystemManager::slotProgressDialogOperationInBackground);

	connectProgressDialogWithSubsystem();

	bool bCloseProgressDlgWhenFinished = false;
	if (subSysCmd == Vfs::COPY)
		bCloseProgressDlgWhenFinished = s_bCloseProgressDlgWhenCopyFinished;
	else if (subSysCmd == Vfs::MOVE)
		bCloseProgressDlgWhenFinished = s_bCloseProgressDlgWhenMoveFinished;
	else if (subSysCmd == Vfs::REMOVE)
		bCloseProgressDlgWhenFinished = s_bCloseProgressDlgWhenDeleteFinished;
	else if (subSysCmd == Vfs::EXTRACT)
		bCloseProgressDlgWhenFinished = s_bCloseProgressDlgWhenExtractFinished;
	else if (subSysCmd == Vfs::ARCHIVE)
		bCloseProgressDlgWhenFinished = s_bCloseProgressDlgWhenArchivFinished;

	pProgressDlg->setTotalVisible(bWeighBefore);
	pProgressDlg->showProgress(subSysCmd, bCloseProgressDlgWhenFinished, sTargetPath);
}

// NOTE. before calling this method, please make sure that m_subsystem is pointing to properly subsystem
void SubsystemManager::removeProgressDlg( bool bCheckWhetherCanToClose )
{
	if (m_progressDlgPtrTable.size() == 0)
		return;

	ProgressDialog *pProgressDlg = m_progressDlgPtrTable.at(m_progressDlgCounter-1);
// 	qDebug() << "SubsystemManager::removeProgressDlg. CheckWhetherCanToClose:" << bCheckWhetherCanToClose << "Will be make connections with:" << m_subsystem;
	if (bCheckWhetherCanToClose) {
		bool bCloseProgressDlgWhenFinished = false;
		if (pProgressDlg->cmdInProgress() == Vfs::COPY)
			bCloseProgressDlgWhenFinished = s_bCloseProgressDlgWhenCopyFinished;
		else if (pProgressDlg->cmdInProgress() == Vfs::MOVE)
			bCloseProgressDlgWhenFinished = s_bCloseProgressDlgWhenMoveFinished;
		else if (pProgressDlg->cmdInProgress() == Vfs::REMOVE)
			bCloseProgressDlgWhenFinished = s_bCloseProgressDlgWhenDeleteFinished;
		else if (pProgressDlg->cmdInProgress() == Vfs::EXTRACT)
			bCloseProgressDlgWhenFinished = s_bCloseProgressDlgWhenExtractFinished;
		else if (pProgressDlg->cmdInProgress() == Vfs::ARCHIVE)
			bCloseProgressDlgWhenFinished = s_bCloseProgressDlgWhenArchivFinished;

		if (! bCloseProgressDlgWhenFinished) {
// 			if (m_lastSubsysStat == Vfs::REMOVE) // fix: if current solution in FileListView::slotInitSelection wouldn't works
// 				emit signalUpdateFocusSelection(false, "^"); // set highlight on properly item
			return;
		}
	}

	disconnect(m_subsystem, &Subsystem::signalGetWeighingResult, this, &SubsystemManager::slotGetWeighingResult);
	disconnect(m_subsystem, &Subsystem::signalShowFileOverwriteDlg, this, &SubsystemManager::slotShowFileOverwriteDlg);

	if (pProgressDlg != NULL) {
		pProgressDlg->timerStop();
		delete pProgressDlg;  pProgressDlg = NULL;
		m_progressDlgCounter--;
		m_progressDlgPtrTable.remove(m_progressDlgCounter);
// 		m_tvFileList->raise();
// 		m_tvFileList->setFocus();
	}
// 	if (m_lastSubsysStat == Vfs::REMOVE) // fix: if current solution in FileListView::slotInitSelection wouldn't works
// 		emit signalUpdateFocusSelection(false, "^"); // set highlight on properly item
}

// NOTE. before calling this method, please make sure that m_subsystem is pointing to properly subsystem
void SubsystemManager::showMultiProgressDialog( bool bWeighBefore, Vfs::SubsystemCommand subSysCmd )
{
// 	qDebug() << "SubsystemManager::showMultiProgressDialog.";
	if (m_pMultiProgressDlg == NULL)
		return;

	connectProgressDialogWithSubsystem(true);

	bool bCloseProgressDlgWhenFinished = false;
	if (subSysCmd == Vfs::COPY)
		bCloseProgressDlgWhenFinished = s_bCloseProgressDlgWhenCopyFinished;
	else if (subSysCmd == Vfs::MOVE)
		bCloseProgressDlgWhenFinished = s_bCloseProgressDlgWhenMoveFinished;
	else if (subSysCmd == Vfs::REMOVE)
		bCloseProgressDlgWhenFinished = s_bCloseProgressDlgWhenDeleteFinished;
	else if (subSysCmd == Vfs::EXTRACT)
		bCloseProgressDlgWhenFinished = s_bCloseProgressDlgWhenExtractFinished;
	else if (subSysCmd == Vfs::ARCHIVE)
		bCloseProgressDlgWhenFinished = s_bCloseProgressDlgWhenArchivFinished;

	if (! m_pMultiProgressDlg->isVisible())
		m_pMultiProgressDlg->showDialog(subSysCmd, bCloseProgressDlgWhenFinished);
}

void SubsystemManager::removeMultiProgressDlg( bool bCheckWhetherCanToClose )
{
// 	qDebug() << "SubsystemManager::removeMultiProgressDlg.";
	if (m_pMultiProgressDlg == NULL)
		return;

// 	qDebug() << "SubsystemManager::removeMultiProgressDlg. CheckWhetherCanToClose:" << bCheckWhetherCanToClose << "Will be make connections with:" << m_subsystem;
	if (bCheckWhetherCanToClose) {
		bool bCloseProgressDlgWhenFinished = false;
		if (m_pMultiProgressDlg->lastSubsysCmd() == Vfs::COPY)
			bCloseProgressDlgWhenFinished = s_bCloseProgressDlgWhenCopyFinished;
		else if (m_pMultiProgressDlg->lastSubsysCmd() == Vfs::MOVE)
			bCloseProgressDlgWhenFinished = s_bCloseProgressDlgWhenMoveFinished;
		else if (m_pMultiProgressDlg->lastSubsysCmd() == Vfs::REMOVE)
			bCloseProgressDlgWhenFinished = s_bCloseProgressDlgWhenDeleteFinished;
		else if (m_pMultiProgressDlg->lastSubsysCmd() == Vfs::EXTRACT)
			bCloseProgressDlgWhenFinished = s_bCloseProgressDlgWhenExtractFinished;
		else if (m_pMultiProgressDlg->lastSubsysCmd() == Vfs::ARCHIVE)
			bCloseProgressDlgWhenFinished = s_bCloseProgressDlgWhenArchivFinished;

		if (! bCloseProgressDlgWhenFinished) {
// 			if (m_lastSubsysStat == Subsystem::REMOVE) // fix: if current solution in FileListView::slotInitSelection wouldn't works
// 				emit signalUpdateFocusSelection(false, "^"); // set highlight on properly item
			return;
		}
	}
	disconnect(m_subsystem, &Subsystem::signalShowFileOverwriteDlg, this, &SubsystemManager::slotShowFileOverwriteDlg);
	disconnect(m_subsystem, &Subsystem::signalDataTransferProgress, this, &SubsystemManager::slotDataTransferProgress);
	//disconnect(m_subsystem, &Subsystem::signalTotalProgress, m_pMultiProgressDlg, &MultiProgressDlg::slotUpdateTotalProgress);

	if (m_pMultiProgressDlg != NULL) {
		delete m_pMultiProgressDlg;  m_pMultiProgressDlg = NULL;
	}
}


void SubsystemManager::showFindFilesDialog( FindFileDialog *pFindFileDialog, FileInfoToRowMap *pFileSelectionMap )
{
	if (m_vfsModel->findFileResultMode() || ! handledOperation(Vfs::FIND)) // find operation doesn't work in findFileResult mode
		return;

	m_pFileSelectionMap = pFileSelectionMap;
	pFindFileDialog->show();
}

bool SubsystemManager::viewOfFoundFile( FileInfo *pFileInfo, bool bEditMode )
{
	qDebug() << "SubsystemManager::slotFoundFileView. Incomming fileName:" << pFileInfo->fileName() << " EditMode:" << bEditMode;

	if (! bEditMode) {
		if (pFileInfo->fileName() == ".." || pFileInfo->isDir()) {
			openFile(pFileInfo); // here is used m_pHelperSelectionMap, initialized inside
			//slotCloseFindFilesDlg();
			return false; // so call slotCloseFindFilesDialog()
		}
		else {
			QString sApplicationName;
			m_pFileSelectionMap->clear(); // used in openSelectedItemsWith
			m_pFileSelectionMap->insert(pFileInfo, 0); // fake index

			if (m_bTopOpenWithAppUseAsDefault) {
				Settings settings;
				QString sValue = settings.value("appAssociationWithMime/"+pFileInfo->mimeInfo()).toString();
				sApplicationName = sValue.right(sValue.length()-sValue.lastIndexOf('|')-1);
			}
			openSelectedItemsWith(sApplicationName, NULL, false, true); // use default application
		}
	}
	else {
		qDebug() << "SubsystemManager::slotFoundFileView. edit file not yet supported";
	}
	return true;
}

void SubsystemManager::openSelectedItemsWith( const QString &sApplicationName, QWidget *pParent, bool bSelectApplication, bool bOpenFoundItem )
{
	qDebug() << "SubsystemManager::openSelectedItemsWith.";
// 	slotBreakCurrentOperation(); // in this moment called in FileListView
	// very abbreviated checking if one given correct class pointer in pParent parameter
	if (pParent != NULL) { // pParent is NULL for calls from FindFileDialog
		if (! pParent->objectName().startsWith("leftPanel_") && ! pParent->objectName().startsWith("rightPanel_")) {
			qDebug() << "SubsystemManager::openSelectedItemsWith. Internal ERROR. Incorrect class passed in pParent parameter!";
			return;
		}
	}
	QString sSelectedAppName = sApplicationName;
	sSelectedAppName = sSelectedAppName.remove('&'); // aplication name is taking from pupup menu, where is set underlined letter
	int nNumOfSelected = (pParent == NULL) ? 1 : dynamic_cast <FileListView *> (pParent)->numberOfSelectedItems();  // if no one selected then selects current item, except ".."
	if (nNumOfSelected == 0) // if item ".."
		return;

	if (bSelectApplication) {
		QFileDialog fd(m_pMainParent, tr("Open current file with..."), "/usr");
		fd.setFileMode(QFileDialog::ExistingFile);
		sSelectedAppName = "";
		if (fd.exec()) {
			sSelectedAppName = fd.selectedFiles().at(0);
			qDebug() << "SubsystemManager::openSelectedItemsWith. Selected application:" << sSelectedAppName;
			// -- save selected application name in configuration file with relation to their mime
			if (nNumOfSelected == 1 || bOpenFoundItem) {
				FileInfo *pFI = bOpenFoundItem ? m_pFileSelectionMap->constBegin().key() : m_selectionModel->firstMarked();
				QString sMime = pFI->mimeInfo();
				Settings settings;
				if (settings.value("appAssociationWithMime/"+sMime).isNull())
					settings.setValue("appAssociationWithMime/"+sMime, sSelectedAppName);
				else {
					QString sOldValue = settings.value("appAssociationWithMime/"+sMime).toString();
					QString sNewApp = sOldValue;
					if (sOldValue.indexOf(sSelectedAppName) < 0) { // new application to associate
						if (sOldValue.count('|')+2 > MAX_OPENWITH_BY_MIME) // if override limit the remove last one
							sOldValue = sOldValue.left(sOldValue.lastIndexOf('|'));
						sNewApp = sSelectedAppName+ "|" +sOldValue;
					}
					settings.setValue("appAssociationWithMime/"+sMime, sNewApp);
				}
			}
		}
		if (sSelectedAppName.isEmpty())
			return;
	}

	if (! bOpenFoundItem) {
		//int nNumOfSelected = numberOfSelectedItems(); // if no one selected then selects current item, except ".."
		if (nNumOfSelected == 0) // maybe current item name matches to the ".."
			return;
	}
	openSelectedItemsWith(sSelectedAppName, (bOpenFoundItem ? m_pFileSelectionMap : m_selectionModel->selectionMap()));
}


void SubsystemManager::cancelVfsOperation()
{
	removeProgressDlg();
	removeMultiProgressDlg();
	if (m_bOperationCompleted)
		return;

	qDebug() << "SubsystemManager::cancelVfsOperation";
	//switchSubsystem(PluginManager::instance(), m_URI.mime()); // making sure, that operation will be invoked on properly subsystem
	//Vfs::SubsystemStatus status = m_subsystem->status();
/*
//	if ( ! m_subsystem->operationFinished() ) {
//		m_subsystem->slotPause( TRUE );

		QString sOperation;
		VFS::Operation eOperation = m_subsystem->operation();
		if ( eOperation == VFS::Copy )
			sOperation = tr("copying"); // it's true for 'moving' too
			else
				if ( eOperation == VFS::Remove )
					sOperation = tr("removing");
				else
					if ( eOperation == VFS::SetAttributs )
						sOperation = tr("setting attributs");

				int nResult = MessageBox::yesNo( this,
													 tr("Break")+" "+sOperation+" - QtCommander",
													 tr("Do you really want to break an operation")+" ?",
													 MessageBox::Yes // default is the 'Yes' button
					);
				if (nResult == 0) { // user closed dialog
					return;
				}
				if ( nResult == MessageBox::Yes )
					m_pVFS->breakOperation();
				else { // "No" or Esc
					m_pProgressDlg->timerStart(); // continue a timer
					m_pVFS->slotPause( FALSE ); // continue an eOperation
				}
	}
	else // an eOperation has been finished
		return true; //removeProgressDlg();

	return false; */
}


void SubsystemManager::openSelectedItemsWith( const QString &sAbsProgrammName, FileInfoToRowMap *pSelectionMap )
{
	qDebug() << "SubsystemManager::openSelectedItemsWith. Let's associate every selected file to properly application, which will open this file";
	QString sAssociatedApp;
	m_absFileNameToAssociatedAppMap.clear();
	FileInfoToRowMap::const_iterator it = pSelectionMap->constBegin();
	while (it != pSelectionMap->constEnd()) {
		if (sAbsProgrammName.isEmpty()) {
			if (findExecutable("xdg-open").isEmpty())
				MessageBox::msg(m_pMainParent, MessageBox::WARNING, "Cannot found xdg-open application!");
			else {
				Settings settings;
				sAssociatedApp = settings.value("appAssociationWithMime/"+it.key()->mimeInfo(), "xdg-open").toString();
			}
		}
		else
			sAssociatedApp = sAbsProgrammName;
		m_absFileNameToAssociatedAppMap.insert(it.key()->absoluteFileName(), sAssociatedApp);
		qDebug() << "SubsystemManager::openSelectedItemsWith. File:" << it.key()->absoluteFileName() << "will be open by" << sAssociatedApp;
		++it;
	}

	if (m_vfsModel->findFileResultMode()) {
		// one might assume that host for all selected items is the same
		QString sHost = pSelectionMap->constBegin().key()->absoluteFileName();
		while (! QFileInfo(sHost).exists()) {
			sHost = sHost.left(sHost.lastIndexOf('/'));
		}
		m_URI.setHost(sHost);
	}
	switchSubsystem(PluginManager::instance(), m_URI.mime()); // making sure, that operation will be invoked on properly subsystem
	m_subsystem->openWith(pSelectionMap, m_URI, m_sPanelName);
}

void SubsystemManager::openSelectedItemsWithSelectedApp( bool bOpenFromLocalsubsystem )
{
	switchSubsystem(PluginManager::instance(), m_URI.mime()); // making sure, that operation will be invoked on properly subsystem
	int numOfProcessedItems = m_subsystem->processedFiles()->size();
	if (numOfProcessedItems > 0) {
		FileInfo *pFI;
		QString sAssociatedApp;
		typedef QPair <QString, QString> AppAndArg;
		QList <AppAndArg> sRunList;
		for (int i = 0; i < numOfProcessedItems; ++i) {
			pFI = m_subsystem->processedFiles()->at(i);
			sAssociatedApp = m_absFileNameToAssociatedAppMap[pFI->absoluteFileName()];
			if (bOpenFromLocalsubsystem)
				sRunList << AppAndArg(sAssociatedApp, pFI->absoluteFileName());
			else
				sRunList << AppAndArg(sAssociatedApp, m_sTmpDir + pFI->fileName());

			m_selectionModel->setSelected(pFI, false); // deselect all selected
		}
		// loop through all associated files (so all selected)
		QString sCmd;
		for (int i=0; i<sRunList.size(); i++) {
			int nNumOfCmds = sRunList[i].first.count('|'); // returns number of commands - 1;
			if (nNumOfCmds > 0)
				qDebug() << "SubsystemManager::openSelectedItemsWithSelectedApp. Found more than one associated application:" << sRunList[i].first
						 << "Will be choosen default (selected as first by user in \"Open with\" option)";
			sCmd = sRunList[i].first.split('|').at(nNumOfCmds);
			qDebug() << "SubsystemManager::openSelectedItemsWithSelectedApp. Execute:" << sCmd +" "+ sRunList[i].second;
			//if (sRunList[i].first != "[itv]") { // isn't internal view.  Commeted because here itv is never met
				/*bool ok = */QProcess::startDetached(sCmd, QStringList() << sRunList[i].second);
// 				qDebug() << "SubsystemManager::openSelectedItemsWithSelectedApp." << ok;
			//}
		}
		emit signalUpdateFocusSelection(false, m_subsystem->processedFiles()->at(0)->fileName()); // set focus on given item name
	}
	else {
		if (m_selectionModel->size() == 1)
			m_selectionModel->setSelected(m_selectionModel->firstMarked(), false);
	}
}

bool SubsystemManager::switchSubsystem( PluginManager *pPluginManager, const QString &sMime )
{
	QString sFN = "SubsystemManager::switchSubsystem.";
	if (pPluginManager == NULL) {
		qDebug() << sFN << "Internal ERROR - pPluginManager is NULL";
		return false;
	}
	if (m_subsystem != NULL) {
		disconnect(m_subsystem, &Subsystem::signalStatusChanged, this, &SubsystemManager::slotSubsystemStatusChanged);
// 		disconnect(m_subsystem, &Subsystem::signalShowFileOverwriteDlg(const FileInfo &, FileInfo &, int &)),  this, SLOT(slotShowFileOverwriteDlg(const FileInfo &, FileInfo &, int &)) );
	}
	Vfs::Subsystem *pSubsystem = pPluginManager->subsystem(sMime);
	bool bReconnected = false;
	if (pSubsystem == NULL) {
		qDebug() << sFN << "ERROR. Cannot switch to subsystem related with mime:" << sMime << "Try to switch to local subsystem";
		pSubsystem = pPluginManager->subsystem("folder");
		if (pSubsystem == NULL) {
			qDebug() << sFN << "FATAL ERROR. Cannot switch to local subsystem";
			return false;
		}
	}
	bReconnected = (m_subsystem != pSubsystem);
	m_subsystem = pSubsystem;
	qDebug() << sFN << "Switched to" << m_subsystem << "Mime:" << sMime << "PanelName:" << objectName(); // m_sPanelName
	connect(m_subsystem, &Subsystem::signalStatusChanged, this, &SubsystemManager::slotSubsystemStatusChanged);
	// below commented, because ProgressDialog crashed during copying OverwriteFileDialog is called for second panel and ProgressDialog for first (in second ProgressDialog doesn't exist)
	//connect(m_subsystem, &Subsystem::signalShowFileOverwriteDlg,  this, &SubsystemManager::slotShowFileOverwriteDlg);
	if (m_FileInfoToMime_map.size() == 0) // map used in extracting entire archive, so updated context menu is not required
		emit singalUpdateContextMenu(); // caugh by slotUpdateContextMenu in FileListView

	return bReconnected;
}

void SubsystemManager::connectProgressDialogWithSubsystem( bool bMultiProgress )
{
// 	qDebug() << "SubsystemManager::connectProgressDialogWithSubsystem";
	if (m_subsystem == NULL)
		return;

	if (bMultiProgress) {
		//connect(m_subsystem, &Subsystem::signalTotalProgress(int, long long)), m_pMultiProgressDlg, SLOT(slotUpdateTotalProgress(int, long long)));
		connect(m_subsystem, &Subsystem::signalDataTransferProgress, // update progress (%) in every single item
				this, &SubsystemManager::slotDataTransferProgress); // m_pMultiProgressDlg required processedFileInfo
		connect(m_subsystem, &Subsystem::signalOperationFinished, m_pMultiProgressDlg, &MultiProgressDialog::slotUpdateLabelOfCancelBtnWithDone);
		// below reconnectiom must be here, because ProgressDialog crashed during copying OverwriteFileDialog is called for second panel and ProgressDialog for first (in second ProgressDialog doesn't exist)
		// and here connection will be made in the same panels
		// 		qDebug() << "SubsystemManager::showProgressDialog. -- reconnect signalShowFileOverwriteDlg - current name:" << m_sPanelName << "subSysCmd" << subSysCmd;
// 		disconnect(m_subsystem, &Subsystem::signalShowFileOverwriteDlg, this, &SubsystemManager::slotShowFileOverwriteDlg);
		connect(m_subsystem, &Subsystem::signalShowFileOverwriteDlg, this, &SubsystemManager::slotShowFileOverwriteDlg);
	}
	else { // single progress dialog
		if (m_progressDlgCounter < 1)
			return;

		ProgressDialog *pProgressDlg = m_progressDlgPtrTable.at(m_progressDlgCounter-1);

		disconnect(m_subsystem, &Subsystem::signalNameOfProcessedFile, pProgressDlg, &ProgressDialog::slotSetFilesName);
		connect(m_subsystem, &Subsystem::signalNameOfProcessedFile, pProgressDlg, &ProgressDialog::slotSetFilesName);

		disconnect(m_subsystem, &Subsystem::signalCounterProgress, pProgressDlg, &ProgressDialog::slotSetCounter);
		connect(m_subsystem, &Subsystem::signalCounterProgress, pProgressDlg, &ProgressDialog::slotSetCounter);

		disconnect(m_subsystem, &Subsystem::signalTotalProgress, pProgressDlg, &ProgressDialog::slotSetTotalProgress);
		connect(m_subsystem, &Subsystem::signalTotalProgress, pProgressDlg, &ProgressDialog::slotSetTotalProgress);

		disconnect(m_subsystem, &Subsystem::signalDataTransferProgress, pProgressDlg, &ProgressDialog::slotDataTransferProgress);
		connect(m_subsystem, &Subsystem::signalDataTransferProgress, pProgressDlg, &ProgressDialog::slotDataTransferProgress);

		disconnect(m_subsystem, &Subsystem::signalTimerStartStop, pProgressDlg, &ProgressDialog::slotTimerStartStop);
		connect(m_subsystem, &Subsystem::signalTimerStartStop, pProgressDlg, &ProgressDialog::slotTimerStartStop);

		disconnect(m_subsystem, &Subsystem::signalOperationFinished, pProgressDlg, &ProgressDialog::slotUpdateLabelOfCancelBtnWithDone);
		connect(m_subsystem, &Subsystem::signalOperationFinished, pProgressDlg, &ProgressDialog::slotUpdateLabelOfCancelBtnWithDone);
	}

	// below reconnections must be here, because ProgressDialog crashed during copying OverwriteFileDialog is called for second panel and ProgressDialog for first (in second ProgressDialog doesn't exist)
	// and here connection will be made in the same panels
	//qDebug() << "SubsystemManager::connectProgressDialogWithSubsystem. -- reconnect signalShowFileOverwriteDlg - current name:" << m_sPanelName << "subSysCmd" << subSysCmd;
	disconnect(m_subsystem, &Subsystem::signalShowFileOverwriteDlg, this, &SubsystemManager::slotShowFileOverwriteDlg);
	connect(m_subsystem, &Subsystem::signalShowFileOverwriteDlg, this, &SubsystemManager::slotShowFileOverwriteDlg);
	// usable in archive subsystem. Sent from inside of this subsystem to weigh directory in LocalSubsystem
	disconnect(m_subsystem, &Subsystem::signalGetWeighingResult, this, &SubsystemManager::slotGetWeighingResult);
	connect(m_subsystem, &Subsystem::signalGetWeighingResult, this, &SubsystemManager::slotGetWeighingResult);

	//disconnect(m_subsystem, &Subsystem::signalSetOperation(Operation)), ProgressDialog, &ProgressDialog::slotSetOperation(Operation)));
	//connect(m_subsystem, &Subsystem::signalSetOperation(Operation)), ProgressDialog, &ProgressDialog::slotSetOperation(Operation)));
	qDebug() << "\"SubsystemManager::connectProgressDialogWithSubsystem.\" Made new connections with:" << m_subsystem << " progressDlgId:" << m_progressDlgCounter;
}


void SubsystemManager::createFileView( FileInfoToRowMap *pSelectionMap, QModelIndex &currentIndex, bool bViewInWindow, bool bOpenInEditMode, QWidget *pParent )
{
	QString sFN = "SubsystemManager::createFileView.";
	qDebug() << sFN;
	m_nViewerCounter++;
	QString sName = m_sPanelName+QString(":Viewer_%1").arg(m_nViewerCounter);
	m_pFileViewer = new FileViewer(pSelectionMap, m_URI.host(), currentIndex.row(), bViewInWindow, bOpenInEditMode, sName, pParent, m_pKeyShortcuts, this);

	//connect(pFileViewer, &FileViewer::signalUpdateItemOnLV, this, &SubsystemManager::slotUpdateItemOnLV);
	// signal from FileView will trigger reading file in current subsystem
	//connect(m_pFileViewer, &FileViewer::signalCheckReadyRead, this, &SubsystemManager::slotCheckReadyRead);
	connect(m_pFileViewer, &FileViewer::signalClose, this, &SubsystemManager::slotCloseView); // handles closing button in embeded view
	connect(m_pFileViewer, &FileViewer::signalReadFile, this, &SubsystemManager::slotReadFileToViewer);
	connect(m_pFileViewer, &FileViewer::signalUpdateItemOnLV, this, &SubsystemManager::slotUpdateItemOnLV);
	// no need to disconnect above connections because if obj. of FileView will be destroyed then also connections will be removed

	QString sMime = m_URI.mime();
	FileInfo *pFI = pSelectionMap->constBegin().key(); // first one from selected
	bool bPluginSupport = PluginFactory::instance()->isPluginAvailable(pFI->mimeInfo(), "subsystem");
	if (bPluginSupport) // this is compressed file
		sMime = pFI->mimeInfo();

	switchSubsystem(PluginManager::instance(), sMime); // making sure, that operation will be invoked on properly subsystem
	connect(m_subsystem, &Subsystem::signalTotalProgress, m_pFileViewer->statusBar(), &StatusBar::slotSetProgress);

/*	FileInfo *pInitFI = pSelectionMap->key(currentIndex.row());
	QString sInitFileName = pInitFI->absoluteFilePath();
	if (m_subsystem->canOpenFileForReading(sInitFileName)) // it should works on all subsystems without any delay
		pFileViewer->slotReadyRead();
	*/
// 	m_pFileViewer->openCurrentFile();

	// -- gathering items to unselect
	//FileInfo *fi;
	m_pSelectedItemToView->clear();
	FileInfoToRowMap::const_iterator it = pSelectionMap->constBegin();
	while (it != pSelectionMap->constEnd()) {
// 		qDebug() << it.key() << ": " << it.value() << endl;
		//fi = it.key();
		m_pSelectedItemToView->append(it.key()); // append FileInfo
		++it;
	}
	//	pFileView->setFilesAssociation(m_pFilesAssociation);
}

void SubsystemManager::jumpToFoundFile( FileInfo *pFileInfo )
{
	m_sPrevDirName = pFileInfo->fileName();
	slotPathChanged(pFileInfo->filePath());
}

bool SubsystemManager::subsystemAvailableForSelected( FileInfoToRowMap *pSelectionMap )
{
	if (pSelectionMap == NULL || pSelectionMap->size() == 0) {
		qDebug() << "SubsystemManager::subsystemAvailableForSelected. ERROR. pSelectionMap is NULL or empty!";
		return false;
	}
	bool bFound = false;
	PluginFactory *pPlugiFactory = PluginFactory::instance();
	FileInfoToRowMap::const_iterator it = pSelectionMap->constBegin();
	while (it != pSelectionMap->constEnd()) {
	//foreach (FileInfo *pFI, pSelectionMap) { // would work if pSelectionMap would be a class, struct or union type, but here it's only typedef
		if (! it.key()->isDir()) {
			if (pPlugiFactory->isPluginAvailable(it.key()->mimeInfo(), "subsystem")) {
				bFound = true;
				break;
			}
		}
		++it;
	}

	return bFound;
}

void SubsystemManager::extract( FileInfoToRowMap *pSelectionMap, QWidget *pParent )
{
	if (! m_URI.host().isEmpty() && ! m_URI.mime().startsWith("remote/")) // inside of archive
		extractFilesTo(pSelectionMap, false, pParent); // false -> COPY
	else
		extractArchive(Vfs::EXTRACT_TO, pSelectionMap, pParent);
}

void SubsystemManager::extractFilesTo( FileInfoToRowMap *pSelectionMap, bool bMove, QWidget *pParent )
{
	QString sFN = "SubsystemManager::extractFilesTo.";
	if (pSelectionMap == NULL) {
		qDebug() << sFN << "ERROR. pSelectionMap is NULL or empty!";
		return;
	}
	if (pParent == NULL) {
		qDebug() << sFN << "ERROR. pParent is NULL!";
		return;
	}
	// very abbreviated checking if one given correct class pointer in pParent parameter
	if (! pParent->objectName().startsWith("leftPanel_") && ! pParent->objectName().startsWith("rightPanel_")) {
		qDebug() << sFN << "Internal ERROR. Incorrect class passed in pParent parameter!";
		return;
	}
// 	slotBreakCurrentOperation(); // in this moment called in FileListView
	int nNumOfSelected = dynamic_cast <FileListView *> (pParent)->numberOfSelectedItems();  // if no one selected then selects current item, except ".."
	if (nNumOfSelected == 0) // maybe focusBar matches to item ".."
		return;

	QString sInputLabel = "<b>"+tr("Extract: ")+"</b>";
	if (bMove)
		sInputLabel = "<b>"+tr("Extract and remove: ")+"</b>";
	if (nNumOfSelected > 1) {
		sInputLabel += m_selectionModel->selectedItemsMsg(false);
	}
	else { // one item selected
		FileInfo *pFileInfo = pSelectionMap->begin().key();
		QString sType = (pFileInfo->isDir() && ! pFileInfo->isSymLink()) ? tr("directory") : tr("file");
		sInputLabel += "(" + sType + "): ";
		sInputLabel += "\""+pFileInfo->fileName()+"\"";
	}

	bool bFake = false;
	QString sTargetPath;
	emit signalGetPathFromOppositeActiveTab(sTargetPath);
	m_sTargetPathToExtracting = InputTextDialog::getText(pParent, InputTextDialog::EXTRACT_FILES, sTargetPath, bFake, sInputLabel);
	if (m_sTargetPathToExtracting.isEmpty())
		return;

	bool bWeighBeforeArchExtracting = true;
	switchSubsystem(PluginManager::instance(), m_URI.mime()); // making sure, that operation will be invoked on properly subsystem
	showProgressDialog(bWeighBeforeArchExtracting, Vfs::EXTRACT); // removed in slotSubsystemStatusChanged
	m_subsystem->extractTo(Vfs::EXTRACT_FILES, bMove, pSelectionMap, m_sTargetPathToExtracting, m_sPanelName);
}

void SubsystemManager::extractArchive( Vfs::WhereExtract whereExtract, FileInfoToRowMap *pSelectionMap, QWidget *pParent )
{
	QString sFN = "SubsystemManager::extractArchive.";
	if (pSelectionMap == NULL) {
		qDebug() << sFN << "ERROR. pSelectionMap is NULL or empty!";
		return;
	}
	if (pParent == NULL) {
		qDebug() << sFN << "ERROR. pParent is NULL!";
		return;
	}
	// very abbreviated checking if one given correct class pointer in pParent parameter
	if (! pParent->objectName().startsWith("leftPanel_") && ! pParent->objectName().startsWith("rightPanel_")) {
		qDebug() << sFN << "Internal ERROR. Incorrect class passed in pParent parameter!";
		return;
	}
// 	slotBreakCurrentOperation(); // in this moment called in FileListView
	m_FileInfoToMime_map.clear();

	if (pSelectionMap->size() == 0)
		dynamic_cast <FileListView *> (pParent)->numberOfSelectedItems();  // if no one selected then selects current item, except ".."

	// obtaining items which are archives handled by internal plugins
	PluginFactory *pPlugiFactory = PluginFactory::instance();
	QString sMime;
	if (pSelectionMap->size() > 0) {
		FileInfoToRowMap::const_iterator it = pSelectionMap->constBegin();
		while (it != pSelectionMap->constEnd()) {
			if (! it.key()->isDir()) {
				sMime = it.key()->mimeInfo();
				if (pPlugiFactory->isPluginAvailable(sMime, "subsystem"))
					m_FileInfoToMime_map.insert(it.key(), sMime);
				else
					m_selectionModel->setSelected(it.key(), false);
			}
			else
				m_selectionModel->setSelected(it.key(), false);
			++it;
		}
	}

	Settings settings;
	if (pSelectionMap->size() == 0) {
		MessageBox::msg(m_pMainParent, MessageBox::INFORMATION, tr("No archives selected or selected archives are not handled by internal plugins!"));
		return;
	}
	else
	if (pSelectionMap->size() > 1) {
		bool bConfirmIfExtractingMoreThanOneArch = settings.value("vfs/ConfirmIfExtractingMoreThanOneArch", true).toBool(); // value is cached when application starts
		if (bConfirmIfExtractingMoreThanOneArch) {
			bool bConfirmIfExtractingMoreThanOneArch_inv = ! bConfirmIfExtractingMoreThanOneArch;
			int nResult = -1;
			nResult = MessageBox::yesNo(pParent, tr("Extract archives"), tr("Selected more than one item."),
					tr("Do you want to extract all of them?")+"\n"+tr("Answer 'No' causes of extracting only current one."),
					bConfirmIfExtractingMoreThanOneArch_inv, MessageBox::No, // default focused button
					tr("Don't ask me again.")
				);
			if (nResult == 0) { // user closed dialog
				dynamic_cast <FileListView *> (pParent)->selectAll(false); // deselect all selected
				return;
			}
			bool bConfirmIfExtractingMoreThanOneArch = ! bConfirmIfExtractingMoreThanOneArch_inv;
			settings.update("vfs/ConfirmIfExtractingMoreThanOneArch", bConfirmIfExtractingMoreThanOneArch);

			if (nResult == MessageBox::No) // need to unselect all other
				dynamic_cast <FileListView *> (pParent)->selectAll(false);
			if (pSelectionMap->count() == 0)
				dynamic_cast <FileListView *> (pParent)->numberOfSelectedItems();  // if no one selected then selects current item, except ".."
			if (pSelectionMap->count() == 0) // item ".." is highlighted
				return;
			// check if current item is supported by internal plugin (subsystem class)
			FileInfo *pCurrentFI = pSelectionMap->constBegin().key();
			sMime = pCurrentFI->mimeInfo();
			if (pPlugiFactory->isPluginAvailable(sMime, "subsystem"))
				m_FileInfoToMime_map.insert(pCurrentFI, sMime);
			else {
				MessageBox::msg(m_pMainParent, MessageBox::INFORMATION, pCurrentFI->fileName() +"\n\n"+
								tr("No archive selected or selected archive is not handled by internal plugins!"));
				dynamic_cast <FileListView *> (pParent)->selectAll(false);
				return;
			}
		}
	}
	QString sTargetPath = m_URI.path();
	if (whereExtract == Vfs::EXTRACT_TO)
		emit signalGetPathFromOppositeActiveTab(sTargetPath);

	QString sInitFileName;
	if (pSelectionMap->size() == 1) {
		sInitFileName = pSelectionMap->constBegin().key()->fileName();
		qDebug() << sFN << sInitFileName << "extract to" << sTargetPath;
	}
	else { // > 1
		qDebug() << sFN << "selected:" << pSelectionMap->size() << "archives" << "extract to" << sTargetPath;
		sInitFileName = m_selectionModel->selectedItemsMsg(false, false);
	}

	if (whereExtract == Vfs::EXTRACT_TO) {
		bool bPersistPath = settings.value("archive/PersistPathInExtractingArch").toBool();
		m_sTargetPathToExtracting = InputTextDialog::getText(pParent, InputTextDialog::EXTRACT_ARCHIVE, sTargetPath, bPersistPath, sInitFileName);
		if (m_sTargetPathToExtracting.isEmpty())
			return;
	}
	else
		m_sTargetPathToExtracting = sTargetPath;

	m_nTotalProcessed = pSelectionMap->size();
	m_whereExtractArchive = whereExtract;
	m_nCounterOfProcessed = 0; // used in m_pMultiProgressDlg

	if (pSelectionMap->size() > 1) {
		m_pMultiProgressDlg = new MultiProgressDialog(m_pMainParent);
		m_pMultiProgressDlg->init(sTargetPath, m_selectionModel->selectionMap());
		m_pMultiProgressDlg->slotUpdateCounterOfProcessed(m_nTotalProcessed, true);
		connect(m_pMultiProgressDlg, &MultiProgressDialog::signalCancel, this, &SubsystemManager::slotMultiProgressDialogCancel);
	}
	extractNextArchive();
}

void SubsystemManager::extractNextArchive()
{
	QString sFN ="SubsystemManager::extractNextArchive.";
	if (m_FileInfoToMime_map.size() == 0) {
		qDebug() << sFN << "Empty map used as queue for extracting!";
		return;
	}
	FileInfo *pFileInfo = m_FileInfoToMime_map.constBegin().key(); // get first item from map
	if (pFileInfo == NULL) {
		qDebug() << sFN << "Internal ERROR. Null FileInfo pointer found in m_extractingFItoMimeMap!";
		return;
	}
	Settings settings;
	bool bWeighBeforeArchExtracting = settings.value("archive/WeighBeforeArchExtracting", true).toBool();

	qDebug() << sFN << "try with:" << pFileInfo->fileName() << "WeighBeforeArchExtracting:" << bWeighBeforeArchExtracting;
	QString sMime = m_URI.mime();
	if (sMime.startsWith("remote/")) {
		m_bExtractingFromLocalSubsys = false;
		m_bWeighBeforeArchExtracting = bWeighBeforeArchExtracting;
		switchSubsystem(PluginManager::instance(), sMime); // switching is needs to be done before showProgressDialog due to provide correct connections with subsystem
		showProgressDialog(true, Vfs::COPY, m_sTmpDir);
		m_selectedForDownloadBeforExtracting->clear();
		m_selectedForDownloadBeforExtracting->insert(m_selectionModel->firstMarked(), m_selectionModel->selectionMap()->constBegin().value());
		// Note. m_selectionModel->selectionMap() contains only archives, which will be filtered in extractArchiveTo
		//       Processed item is unmarked so in next call will be taken next item
		m_subsystem->copyFiles(m_selectedForDownloadBeforExtracting, m_sTmpDir, false, true, m_sPanelName); // download/extract file
	}
	else { // local subsystem
		m_bExtractingFromLocalSubsys = true;
		switchSubsystem(PluginManager::instance(), pFileInfo->mimeInfo());
		if (m_pMultiProgressDlg != NULL) // multiple archives
			showMultiProgressDialog(bWeighBeforeArchExtracting, Vfs::EXTRACT); // removed in slotSubsystemStatusChanged
		else // single archive
			showProgressDialog(bWeighBeforeArchExtracting, Vfs::EXTRACT, m_sTargetPathToExtracting);
		m_subsystem->extractTo(Vfs::EXTRACT_ARCHIVE, false, m_selectionModel->selectionMap(), m_sTargetPathToExtracting, m_sPanelName); // false -> COPY
	}
	// next call of current function should appear when current extracting will be finished, so from slotSubsystemStatusChanged (EXTRACTED subsystem status)
}

void SubsystemManager::createAnArchive( Vfs::WhereArchive whereArchive, bool bMoveToArchive, FileInfoToRowMap *pSelectionMap, QWidget *pParent )
{
	QString sFN = "SubsystemManager::createAnArchive.";
	if (pSelectionMap == NULL) {
		qDebug() << sFN << "ERROR. pSelectionMap is NULL or empty!";
		return;
	}
	if (pParent == NULL) {
		qDebug() << sFN << "ERROR. pParent is NULL!";
		return;
	}
	// very abbreviated checking if one given correct class pointer in pParent parameter
	if (! pParent->objectName().startsWith("leftPanel_") && ! pParent->objectName().startsWith("rightPanel_")) {
		qDebug() << sFN << "Internal ERROR. Incorrect class passed in pParent parameter!";
		return;
	}

// 	slotBreakCurrentOperation(); // in this moment called in FileListView
	if (pSelectionMap->size() == 0)
		dynamic_cast <FileListView *> (pParent)->numberOfSelectedItems();  // if no one selected then selects current item, except ".."

	const int nNumOfSelected = pSelectionMap->size();
	m_bMoveToArchive = bMoveToArchive;

	QString sTargetPath = m_URI.path();
	if (whereArchive == Vfs::ARCHIVE_TO)
		emit signalGetPathFromOppositeActiveTab(sTargetPath);

	Settings settings;
	int nArchSeparately = -1;
	if (nNumOfSelected > 1) {
		bool bForMultiSelCreateOnlyOneArchive = settings.value("vfs/ForMultiSelCreateOnlyOneArchive", false).toBool(); // value is cached when application starts
		if (! bForMultiSelCreateOnlyOneArchive) {
			nArchSeparately = MessageBox::yesNo(pParent, tr("Creating archives..."), tr("Selected more than one item."),
										tr("Do you want to archive each of them separately?")+"\n"+tr("Answer 'No' causes of creating only one archive."),
										bForMultiSelCreateOnlyOneArchive, MessageBox::No, // default focused button
										tr("Always create only one archive")
			);
			if (nArchSeparately == 0) { // user closed dialog
				if (pParent != NULL) {
					dynamic_cast <FileListView *> (pParent)->selectAll(false); // deselect all selected (FIXME: better would be to create signal/slot connection)
				}
				return;
			}
			Settings settings;
			settings.update("vfs/ForMultiSelCreateOnlyOneArchive", bForMultiSelCreateOnlyOneArchive);
		}
		else
			nArchSeparately = MessageBox::Yes;
	}

	m_bSingleArchive = false;
	QString sInitFileName, sSelectedFilesMsg;
	if (nNumOfSelected == 1 || nArchSeparately == MessageBox::No) {
		if (nNumOfSelected == 1) {
			sInitFileName = pSelectionMap->constBegin().key()->fileName();
			sTargetPath += sInitFileName;
		}
		else { // all selected directories and/or files in one archive
			sInitFileName = tr("New_Archive");
			sTargetPath += sInitFileName;
		}
		m_bSingleArchive = true;
		sSelectedFilesMsg = sInitFileName;
		qDebug() << sFN << "InitFileName:" << sInitFileName << "archive name without ext.:" << sTargetPath;
	}
	m_FileInfoToMime_map.clear();
	if (nArchSeparately == MessageBox::Yes) {
		FileInfoToRowMap::const_iterator it = pSelectionMap->constBegin();
		while (it != pSelectionMap->constEnd()) {
			m_FileInfoToMime_map.insert(it.key(), it.key()->fileName()); // uses in preArchiveNextItem to point file to weigh
			sTargetPath += it.key()->fileName()+"|";
			++it;
		}
		sTargetPath = sTargetPath.left(sTargetPath.length()-1);
	}
	if (nNumOfSelected > 1)
		sSelectedFilesMsg = m_selectionModel->selectedItemsMsg(false, false);
	if (nArchSeparately == MessageBox::Yes)
		sSelectedFilesMsg += " "+tr("to multi archives.");
	qDebug() << sFN << "Selected:" << sSelectedFilesMsg;

	sSelectedFilesMsg.insert(0, "<b>"+tr("Add")+" </b>");
	//bool bAlwaysOverwrite  = settings.value("vfs/AlwaysOverwriteWithNewArchive", true).toBool(); // TODO: implement AlwaysOverwriteExistingArchive for value "false"
	// -- get supported archives type
	PluginManager *pPluginManager = PluginManager::instance();
	QStringList slArchivesExt = pPluginManager->getAllRegistreredPluginsInfo(PluginData::FILE_EXT, "subsystem");

	InputTextDialog::GetTextMode gtm = (bMoveToArchive) ? InputTextDialog::MOVE_TO_ARCH : InputTextDialog::ADD_TO_ARCH;
	if (whereArchive == Vfs::ARCHIVE_TO) {
// 		bool bPersistPath = m_pSettings->value("archive/PersistPathInExtractingArch").toBool();
		m_sTargetPathToArchiving = InputTextDialog::getText(pParent, gtm, sTargetPath, s_bAlwaysOverwriteWhenNewArchive, sSelectedFilesMsg, slArchivesExt);
		if (m_sTargetPathToArchiving.isEmpty()) {
			dynamic_cast <FileListView *> (pParent)->selectAll(false);
			return;
		}
	}

	if (nNumOfSelected > 1) {
		m_nTotalProcessed = nNumOfSelected;
		m_nCounterOfProcessed = 0; // used in m_pMultiProgressDlg

		if (nArchSeparately == MessageBox::Yes) {
			m_pMultiProgressDlg = new MultiProgressDialog(m_pMainParent);
			m_pMultiProgressDlg->init(sTargetPath, m_selectionModel->selectionMap());
			m_pMultiProgressDlg->slotUpdateCounterOfProcessed(m_nTotalProcessed, true);
			connect(m_pMultiProgressDlg, &MultiProgressDialog::signalCancel, this, &SubsystemManager::slotMultiProgressDialogCancel);
		}
	}

	QString sArchiveType = m_sTargetPathToArchiving.split("||").at(1);
	m_sTargetPathToArchiving = m_sTargetPathToArchiving.split("||").at(0);

	if (m_sTargetPathToArchiving.startsWith("./")) { // create archive here (in current path)
		m_sTargetPathToArchiving.remove(0, m_sTargetPathToArchiving.lastIndexOf(QDir::separator())+1); // +1 because m_URI.toString() contains ending separator
		m_sTargetPathToArchiving.prepend(m_URI.toString()); // update with current path
	}
	if (m_bSingleArchive) // to be able get new archive name in archiveNextItem
		m_FileInfoToMime_map.insert(pSelectionMap->constBegin().key(), QFileInfo(m_sTargetPathToArchiving).baseName());

	// -- find properly archiver mime
//	findProperlyArchiverMime(sArchiveType);
	QString sMultiArchiveExt;
	QStringList slArchivesMimes;
	slArchivesExt = pPluginManager->getAllRegistreredPluginsInfo((PluginData::FILE_EXT|PluginData::MIME_TYPE), "subsystem");
	int id = slArchivesExt.indexOf(sArchiveType);
	m_sArchiverExt = "";
	if (id > 0) {
		for (int i=id; i>=0; --i) {
			if (slArchivesExt.at(i).contains('/')) { // every mime type contains slash
				m_sArchiverMime = slArchivesExt.at(i);
				if (m_sArchiverMime.contains(':')) { // multi mime
					sMultiArchiveExt = m_sArchiverMime.split(':').at(0);
					sMultiArchiveExt = sMultiArchiveExt.right(sMultiArchiveExt.length()-sMultiArchiveExt.lastIndexOf('-')); // "-tar"
					slArchivesMimes = m_sArchiverMime.split(':').filter(QRegExp(sMultiArchiveExt));
					m_sArchiverMime = slArchivesMimes.at(id-i-1);
				}
				m_sArchiverExt = slArchivesExt.at(id);
				break;
			}
		}
	}
	qDebug() << sFN << "MoveToArchive:" << bMoveToArchive << " Selected archive type:" << sArchiveType << "ArchiverMime:" << m_sArchiverMime << "TargetArchiveName:" << m_sTargetPathToArchiving;

	if (! m_sArchiverExt.isEmpty()) {
		sTargetPath = sTargetPath.left(sTargetPath.lastIndexOf('/'));
		//bool bWeighBeforeArchiving = settings.value("vfs/WeighBeforeArchiving", true).toBool();
		QString sMime = (s_bWeighBeforeArchivng) ? m_URI.mime() : m_sArchiverMime;
		switchSubsystem(PluginManager::instance(), sMime); // if choosed weighing then switch to localsubsystem
		m_nTotalWeight = 0;  m_nTotalWeighedItems = 0;

		if (m_pMultiProgressDlg != NULL) // multiple archives
			showMultiProgressDialog(s_bWeighBeforeArchivng, Vfs::ARCHIVE); // removed in slotSubsystemStatusChanged
		else // single archive
			showProgressDialog(s_bWeighBeforeArchivng, Vfs::ARCHIVE, sTargetPath);

		if (m_bSingleArchive) {
			m_pHelperSelectionMap->clear();
			FileInfo *pFileInfo;
			FileInfoToRowMap::const_iterator it = pSelectionMap->constBegin();
			while (it != pSelectionMap->constEnd()) {
				pFileInfo = it.key();
				m_pHelperSelectionMap->insert(pFileInfo, 0); // fake index
// 				weighItem(pFileInfo, false); // false - don't update DataModel (m_bUpdateModelAfterWeighing = false). When weighing will be finished then in slotSubsystemStatusChanged will be called archiveNextItem
				++it;
			}
			m_sTargetPathToExtracting = sTargetPath; // required for switching to properly subsystem in slotSubsystemStatusChanged()
			m_bUpdateModelAfterWeighing = false;
			weighSelectedItems(pSelectionMap);
			m_bArchivingFromLocalSubsys = true; // archiving will start after weighing will be finished
		}
		else
			preArchiveNextItem();
/*
		m_bArchivingFromLocalSubsys = true; // should be set on true before weighing, because operation will be done in LocalSubsystem
		if (s_bWeighBeforeArchiving)
			weighItem(m_FileInfoToMime_map.constBegin().key(), false); // false - don't update DataModel (m_bUpdateModelAfterWeighing = false)
		if (! s_bWeighBeforeArchiving)
			archiveNextItem();*/
	}
	else
		qDebug() << sFN << "ERROR. Cannot find archiver mime for:" << sArchiveType;
}

void SubsystemManager::preArchiveNextItem()
{
	QString sFN ="SubsystemManager::preArchiveNextItem.";
	if (m_FileInfoToMime_map.size() == 0) {
		qDebug() << sFN << "Empty map used as queue for archiving!";
		return;
	}
	FileInfo *pFileInfo = m_FileInfoToMime_map.constBegin().key(); // get first item from map
	if (pFileInfo == NULL) {
		qDebug() << sFN << "Internal ERROR. Null FileInfo pointer found in m_FileInfoToMime_map!";
		return;
	}
	m_pHelperSelectionMap->clear();
	m_pHelperSelectionMap->insert(pFileInfo, 0); // fake index

	//Settings settings;
	//bool bWeighBeforeArchiving = settings.value("vfs/WeighBeforeArchiving", true).toBool();
	qDebug() << sFN << "try with:" << pFileInfo->fileName() << "bWeighBeforeArchiving:" << s_bWeighBeforeArchivng;
	QString sMime = m_URI.mime();
	if (sMime.startsWith("remote/")) {
		qDebug() << sFN << "Archiving from remote/inside archive location is not yet supported.";
		// here is required to download all items before archiving may start
	}
	else { // local subsystem
		m_bArchivingFromLocalSubsys = true; // should be set on true before weighing, because operation will be done in LocalSubsystem
		if (s_bWeighBeforeArchivng)
			weighItem(pFileInfo, false); // false - don't update DataModel (m_bUpdateModelAfterWeighing = false). When weighing will be finished then in slotSubsystemStatusChanged will be called archiveNextItem
		else
			archiveNextItem();
	}
	// next call of current function should appear when current archiving will be finished, so from slotSubsystemStatusChanged (ARCHIVED subsystem status)
}

void SubsystemManager::archiveNextItem( const QString &sTargetArchiveName )
{
	QString sFN ="SubsystemManager::archiveNextItem.";
	FileInfo *pFileInfo = m_FileInfoToMime_map.constBegin().key(); // get first item from map
	QString sTargetFileName = m_FileInfoToMime_map[pFileInfo];
	QString sTargetPath = m_sTargetPathToArchiving.left(m_sTargetPathToArchiving.lastIndexOf('/'));
	bool bWeighBeforeArchArchiving = false; // this parameter is not used in copyFiles by any archiver subsystem // m_pSettings->value("archive/WeighBeforeArchArchiving", true).toBool();
	if (sTargetArchiveName.isEmpty()) {
		sTargetFileName = sTargetPath+"/"+sTargetFileName+"."+m_sArchiverExt;
		if (m_bSingleArchive)
			sTargetFileName += "|"; // to be able to recognize in archive subsystem if this is single archive
	}
	else
		sTargetFileName = sTargetArchiveName+"."+m_sArchiverExt;
	//m_bArchivingFromLocalSubsys = true;
	qDebug() << sFN << "TargetFileName:" << sTargetFileName;
	switchSubsystem(PluginManager::instance(), m_sArchiverMime);
	connectProgressDialogWithSubsystem((m_pMultiProgressDlg != NULL)); // reconnent all connections due to subsystem could be changed
	m_subsystem->copyFiles(m_pHelperSelectionMap, sTargetFileName, m_bMoveToArchive, bWeighBeforeArchArchiving, m_sPanelName); // m_pHelperSelectionMap contains always one item
}

void SubsystemManager::openCurrentItemInTab( const QModelIndex &currentIndex, Vfs::OpenInTab openInTab )
{
// 	slotBreakCurrentOperation(); // in this moment called in FileListView
	FileInfo fi = m_vfsModel->fileInfo(currentIndex);
	QString sFileName = fi.fileName();
//	if (sFileName == "..")
//		return;
	URI pbURI;

	if (openInTab == Vfs::OPPOSITE_ACTIVE_TAB) {
		QString sLocation = m_vfsModel->findFileResultMode() ? fi.absoluteFileName() : m_URI.toString() + sFileName;
		pbURI.setUri(sLocation);
	}
	else {
		pbURI.setUri(m_URI.toString() + sFileName);
	}

	if (pbURI.mime().isEmpty())
		pbURI.setMime(fi.mimeInfo());

	emit signalOpenInTab(pbURI, openInTab);
}

void SubsystemManager::openInOppositeActiveTab( const URI &uri ) // called from opposite panel
{
	QString sFN = "SubsystemManager::openInOppositeActiveTab";
	// --- detects subsystem plugin's support for given uri
	QString sMime = uri.mime(); // fi.mimeInfo();
	if (sMime.contains(':')) {
		sMime.remove(sMime.lastIndexOf(':'), sMime.lastIndexOf(':')); // remove iconName string
	}
	qDebug() << sFN << "sMime:" << sMime;

	PluginManager *pPluginManager = PluginManager::instance();
	bool bPluginSupport = pPluginManager->isPluginRegistrered(sMime, "subsystem");
	qDebug() << sFN << "PanelName:" << m_sPanelName << " uri:" << uri.toString() << " Mime:" << sMime << " PluginSupport:" << bPluginSupport;
	if (bPluginSupport || sMime == "folder/up") {
		m_bOpenInOppositeActiveTab = true;
		slotPathChanged(uri.toString());
	}
	// TODO add to openInOppositeActiveTab support for file view
}

void SubsystemManager::openInCurrentActiveTab( const URI &uri )
{
	QString sFN = "SubsystemManager::openInCurrentActiveTab.";
	qDebug() << sFN << uri.toString();
	if (uri.toString().endsWith("/")) {
		m_bOpenInOppositeActiveTab = false;
		slotPathChanged(uri.toString());
	}
	else {
		m_OpenFile = true;
		//openFile();
	}
}

// ==================
// --- S L O T S ----

void SubsystemManager::slotActivated( const QModelIndex &miIndex )
{
	Q_ASSERT(miIndex.isValid());
	QString sFN = "SubsystemManager::slotActivated.";
	m_sMimeWhenOpStopped = "";
	if (m_subsystem != NULL) {
		// -- switch to previous subsystem after broke "connecting" operation in remote subsystem
		SubsystemStatus subsysStat = m_subsystem->status();
		qDebug() << sFN << "Subsystem last status:" << Vfs::toString(subsysStat);
		if (subsysStat == Vfs::CLOSING    || subsysStat == Vfs::OPR_STOPPED || // OPR_STOPPED occurs after pressing Esc
			subsysStat == Vfs::CONNECTING || subsysStat == Vfs::LOGIN)
		{
			if (subsysStat == Vfs::CONNECTING || subsysStat == Vfs::LOGIN) // ongoing operation, need to break this operation
				m_subsystem->close();
			QString sNewPathWrk = m_prevURI.toString();
// 			qDebug() << sFN << ">> sNewPathWrk:" << sNewPathWrk;
			QString sNewAbsName, sCurrentHost;
			QString sMime = MimeType::getMimeFromPath(sNewPathWrk, sCurrentHost, sNewAbsName); // also sets: sCurrentHost, sNewAbsName
			if (sMime.isEmpty())
				sMime = m_prevURI.mime();
			// -- get subsystem for current or new mime
			PluginManager *pPluginManager = PluginManager::instance();
			if (pPluginManager->isNewSubsystem(sMime) || m_subsystem == NULL) {
				qDebug() << sFN << "-- get subsystem for mime:" << sMime;
				switchSubsystem(pPluginManager, sMime);
			}
			m_URI = m_prevURI;
			m_URI.setHost(sCurrentHost); // shows correct path in path view
			m_URI.setMime(sMime);
			m_sMimeWhenOpStopped = sMime;
// 			qDebug() << ">> subsystem:" << m_subsystem << "status:" << Vfs::toString(subsysStat);
		}
	}

	QModelIndex proxyModelFileName = static_cast <const VfsFlatListProxyModel *>(miIndex.model())->mapToSource(miIndex.model()->index(miIndex.row(), 0));
	qDebug() << sFN << "Current m_URI:" << m_URI.toString() << " proxyModelFileName:" << proxyModelFileName.data().toString();
	FileInfo *pFI = m_vfsModel->fileInfoPtr(proxyModelFileName); // miIndex need to be cast here due to calling signal from QTreeView
	QString sMime = MimeType::getMimeFromPath(m_URI.toString()); // for remote subsystem sMime is empty
	if (sMime != "folder" && sMime != "folder/symlink" && m_URI.protocol() == "file") { // this is archive (in local subsystem)
		if (pFI->fileName() == "..")
			m_sPrevDirName = QFileInfo(m_URI.host()).fileName();
	}
	openFile(pFI);
}

void SubsystemManager::openFile( FileInfo *pFileInfo )
{
	Q_ASSERT(pFileInfo);
	QString sFN = "SubsystemManager::openFile."; // this->metaObject()->className() :: Q_FUNC_INFO   or  typeid(*this).name() "::" __func__ ". ";
	QString sMime = m_sMimeWhenOpStopped;
	QString sCurrentAbsPath = m_vfsModel->findFileResultMode() ? pFileInfo->filePath() : m_URI.path();
	QString sFileOrDirName  = pFileInfo->fileName();
	QString sNewAbsName     = m_vfsModel->findFileResultMode() ? pFileInfo->absoluteFileName() : sCurrentAbsPath;
// 	bool bLocalSubsystem    = m_URI.host().isEmpty();
	if (sNewAbsName.endsWith(".."))
		sNewAbsName = sCurrentAbsPath;
	if (! m_vfsModel->findFileResultMode()) {
		if (! sNewAbsName.endsWith("/"))
			sNewAbsName += "/";
		sNewAbsName += sFileOrDirName;
	}
	qDebug() << sFN << "NewAbsName:"<< sNewAbsName << " isDir:"<< pFileInfo->isDir() << " fileName:"<< pFileInfo->fileName() << " CurrentAbsPath:"<< sCurrentAbsPath << " FileOrDirName:"<< sFileOrDirName;
	// making sure, that operation will be invoked on properly subsystem
	if (! m_vfsModel->findFileResultMode())
		switchSubsystem(PluginManager::instance(), m_URI.mime()); // also set ContextMenu to FilesPanelMENU

	if (pFileInfo->isDir()) {
		if (m_vfsModel->findFileResultMode()) {
			URI uri;
			uri.setUri(sNewAbsName);
			uri.setPath(sNewAbsName);
			emit signalOpenInTab(uri, Vfs::NEW_ACTIVE_TAB);
			return;
		}
		m_OpenFile = false;
		if (sCurrentAbsPath == "/" && sFileOrDirName == "..") { // if root dir and go up
			if (m_subsystem->canUpThanRootDir()) {
				if (m_sOpenedArchFromRemoteLocation.isEmpty())
					slotCloseConnection();
				else { // quit from archive opened from remote location
					QFile::remove(m_sOpenedArchFromRemoteLocation);
					m_URI = m_remoteURI;
					m_remoteURI.reset();
					qDebug() << sFN << "Open m_URI:" << m_URI.toString() << "removed downloaded archive:" << m_sOpenedArchFromRemoteLocation;
					m_sOpenedArchFromRemoteLocation = "";
					switchSubsystem(PluginManager::instance(), m_URI.mime());
					m_subsystem->init(m_pMainParent);
					m_subsystem->open(m_URI, m_sPanelName);
				}
				return;
			}
			else
				return;
		}
		// try change directory
		if (! m_subsystem->canChangeDirTo(sNewAbsName)) {
			MessageBox::msg(m_pMainParent, MessageBox::CRITICAL, tr("Cannot open directory"), sNewAbsName);
			return;
		}
		// get prev name from path
		m_sPrevDirName = "."; // used to setting focus on last/previous directory name
		if (sFileOrDirName == "..") {
			if (sCurrentAbsPath.endsWith(QDir::separator())) {
				int pos = sCurrentAbsPath.lastIndexOf('/', -2) + 1;
				int len = sCurrentAbsPath.length() - pos - 1;
				m_sPrevDirName = sCurrentAbsPath.mid(pos, len);
			}
			else
				m_sPrevDirName = sCurrentAbsPath.right(sCurrentAbsPath.length() - sCurrentAbsPath.lastIndexOf(QDir::separator()) - 1);
		}
		// TODO: if m_URI would be set after successfuly finished LIST op.then would be no need to set it here and restore if an error appears
		// save previous path as current path
		if (sMime == "folder")
			m_URI.setPrevLocalPath(m_URI.path());
		// set current path
		m_URI.setPath(PathView::cleanPath(sNewAbsName));
		// get mime for new path and update with it current URI
		// TODO in getMimeFromPath check is this inside of archive (host different than / or protocol different than file) if yes then there is no sense to call getFileMime() which make checking in LocalSubsystem and shows error
		sMime = MimeType::getMimeFromPath(m_URI.host() + m_URI.path());
		if (sMime.isEmpty()) // or: if (m_URI.protocol() != "file")  or: if (! uri.host().startsWith(QDir::rootPath()))
			sMime = m_URI.mime();
		m_URI.setMime(sMime);
		qDebug() << sFN << "Open new m_URI:" << m_URI.toString();
		m_subsystem->open(m_URI, m_sPanelName);
	}
	else { // this is file (check it to find if subsystem is supported)
		sMime = pFileInfo->mimeInfo();
		if (sMime.contains(':'))
			sMime.remove(sMime.lastIndexOf(':'), sMime.lastIndexOf(':')); // remove iconName string
		qDebug() << sFN << "Try to open file with mime:" << sMime;
		PluginManager *pPluginManager = PluginManager::instance();
		if (pPluginManager == NULL) {
			qDebug() << sFN << "-- Internal ERROR. Cannot get instance for PluginManager!";
			return;
		}
		if (pPluginManager->isPluginRegistrered(sMime, "subsystem")) {
			qDebug() << sFN << "Found registrered plugin for mime:" << sMime << "in class subsystem";
			if (m_URI.mime().startsWith("remote/")) { // need to download file into the temporary directory
				bool bFake = false;
				int nResult = MessageBox::yesNo(m_pMainParent, tr("Opening archive ..."),
												m_URI.protocolWithSuffix() + m_URI.host() + sNewAbsName,
											tr("Do you want to download and open this archive")+"?", bFake,
											MessageBox::No // default focused button
				);
				if (nResult == 0) { // user closed dialog
					return;
				}
				if (nResult == MessageBox::Yes) {
					m_bOpenArchInRemoteLocation = true;
					m_pHelperSelectionMap->clear();
					m_pHelperSelectionMap->insert(pFileInfo, 0); // fake index
					showProgressDialog(true, Vfs::COPY, m_sTmpDir);
					m_subsystem->copyFiles(m_pHelperSelectionMap, m_sTmpDir, false, true, m_sPanelName);
				}
				return;
			}
			if (m_vfsModel->findFileResultMode()) {
				URI uri;
				uri.setUri(sNewAbsName); // for set mime and protocol
				uri.setHost(sNewAbsName);
				uri.setPath("/");
				emit signalOpenInTab(uri, Vfs::NEW_ACTIVE_TAB);
				return;
			}
			if (pPluginManager->isNewSubsystem(sMime)) { // if new subsystem then set as current
				switchSubsystem(pPluginManager, sMime);
				m_sPrevSubsysFileName = sFileOrDirName; // usable after get out from subsystem (eg. remote, archive)
				m_sPrevDirName = m_sPrevSubsysFileName; // used to set foucs on last/previous archive name
			}
			m_OpenFile = true;
			QString sNewHost, sNewPath;
			QString sMime = MimeType::getMimeFromPath(sNewAbsName, sNewHost, sNewPath); // also sets: sNewHost, sNewPath
			if (sMime.isEmpty())
				sMime = m_URI.mime();
			m_URI.setHost(sNewHost);
			m_URI.setPath(sNewPath);
			m_URI.setMime(sMime);
			qDebug() << sFN << "Open m_URI:" << m_URI.toString();
			m_subsystem->init(m_pMainParent);
			m_subsystem->open(m_URI, m_sPanelName);
		}
		else // try to run file (EXCEPTION: mounted Microsoft Windows file system, eg. fat, ntfs)
		if (MimeType::getParentMime(sMime) == "application" && m_subsystem->isExecutableFile(sNewAbsName) != Vfs::CANNOT_EXECUTE) {
			m_subsystem->executeApplication(sNewAbsName);
			// TODO; VfsFlatListModel::slotActivated. run file, if remote subsystem then download/extract before
		}
		else { // try to view file (or run in external viewer)
			if (m_selectionModel->size() == 0)
				m_selectionModel->setSelected(pFileInfo, true);
			openSelectedItemsWith("", m_selectionModel->selectionMap());
		}
	}
}

void SubsystemManager::slotPathChanged( const QString &sInPathIn )
{
	QString sFN = "SubsystemManager::slotPathChanged.";
	QString sInPath = sInPathIn;
	if (sInPathIn == "/|/") {
		bool bRenmote = (m_URI.mime().startsWith("remote/"));
		bool bArchive = (! m_URI.host().isEmpty() && ! m_URI.mime().startsWith("remote/")); // inside of archive
		if (bRenmote) {
			sInPath = m_URI.protocol()+"://"+m_URI.user()+":"+m_URI.password()+"@"+m_URI.host()+":"+QString("%1").number(m_URI.port())+"/";
		}
		else if (bArchive) {
			sInPath = m_URI.host()+"/";
		}
		else  // local subsystem
			sInPath = "/";
		qDebug() << sFN << "GoToRootDirectoryInCurrentSubsystem" << sInPath;
	}
	QString sSenderName = (sender() != NULL) ? sender()->objectName() : "?";
	qDebug() << sFN << "Sender:" << sSenderName << "Panel:" << m_sPanelName << "Incomming path:" << sInPath;
	QString sNewPath = sInPath.isEmpty() ? m_URI.toString() : sInPath; // prevent for empty sInPath
	if (sNewPath.startsWith("~/"))
		sNewPath.replace("~/", QDir::homePath()+QDir::separator());
	if (sNewPath.contains('~'))
		sNewPath.replace(QRegExp(".*~/"), QDir::homePath()+QDir::separator());
	if (sInPath.isEmpty())
		qDebug() << sFN << "Given empty path. New path is:" << sNewPath;
	if (sNewPath.contains(QRegExp("^ +/")))
		sNewPath.replace(QRegExp("^ +/"), "/");
	// correct if user accidentally append remote path to local subsystem path
	if (sNewPath.contains("://") && sNewPath.startsWith('/')) {
		int dblSlashId = sNewPath.indexOf("://");
		int lastStartingSlashId = sNewPath.lastIndexOf('/', dblSlashId) + 1;
		sNewPath = sNewPath.right(sNewPath.length()- lastStartingSlashId);
	}

	switchSubsystem(PluginManager::instance(), m_URI.mime()); // making sure, that operation will be invoked on properly subsystem

	if (sInPath == "..") { // used by Backspace handled in FileListView
		if (m_URI.path() == "/" && sNewPath == "..") { // if root dir and up
			if (m_subsystem->canUpThanRootDir()) {
				if (m_sOpenedArchFromRemoteLocation.isEmpty()) {
					sNewPath = m_URI.toString();
					qDebug() << sFN << "root dir and up. sNewPath:" << sNewPath;
					// get prev name from path
					if (sNewPath.endsWith(QDir::separator())) {
						int pos = sNewPath.lastIndexOf('/', -2) + 1;
						int len = sNewPath.length() - pos - 1;
						m_sPrevDirName = sNewPath.mid(pos, len);
					}
					else
						m_sPrevDirName = sNewPath.right(sNewPath.length() - sNewPath.lastIndexOf(QDir::separator()) - 1);
					//qDebug() << sFN << "-- (1) m_sPrevDirName:" << m_sPrevDirName;
					sNewPath += "..";
					sNewPath = PathView::cleanPath(sNewPath);
					//qDebug() << sFN << "-- sNewPath" << sNewPath << "m_prevURI:" << m_prevURI.toString();
					slotCloseConnection();
				}
				else { // quit from archive opened from remote location
					QFile::remove(m_sOpenedArchFromRemoteLocation);
					m_URI = m_remoteURI;
					m_remoteURI.reset();
					qDebug() << sFN << "Open m_URI:" << m_URI.toString() << "removed downloaded archive:" << m_sOpenedArchFromRemoteLocation;
					m_sOpenedArchFromRemoteLocation = "";
					switchSubsystem(PluginManager::instance(), m_URI.mime());
					m_subsystem->init(m_pMainParent);
					m_subsystem->open(m_URI, m_sPanelName);
				}
				return;
			}
			else
				return;
		}
		sNewPath = m_URI.toString();
		//qDebug() << sFN << "(2) sNewPath:" << sNewPath;
		// get previous name from path
		if (sNewPath.endsWith(QDir::separator())) {
			int pos = sNewPath.lastIndexOf('/', -2) + 1;
			int len = sNewPath.length() - pos - 1;
			m_sPrevDirName = sNewPath.mid(pos, len);
		}
		else
			m_sPrevDirName = sNewPath.right(sNewPath.length() - sNewPath.lastIndexOf(QDir::separator()) - 1);
		//qDebug() << sFN << "-- (2) m_sPrevDirName:" << m_sPrevDirName;
		sNewPath += "..";
		sNewPath = PathView::cleanPath(sNewPath);
		//qDebug() << sFN << "(3) sNewPath:" << sNewPath;
	} // if (sInPath == "..")

	URI uri(sNewPath); // note that for archive subsys.host will be always empty, for local subsys. mime s always "folder"
// 	qDebug() << sFN << "-- uri_mime" << uri.mime() << "host:" << uri.host() << "sNewPath" << sNewPath;
	if (sNewPath == m_URI.protocol()+":"+QDir::separator()) { // for path=remote_host_or_archive/../  (PathView)
		slotCloseConnection(); // leave current remote/archive subsystem
		return;
	}

	QString sNewAbsPath  = sNewPath;
	QString sCurrentHost = m_URI.host();
	QString sMime = MimeType::getMimeFromPath(sNewPath, sCurrentHost, sNewAbsPath); // also sets: sCurrentHost, sNewAbsPath
	if (sMime.isEmpty()) { // remote subsystem
		sMime = uri.mime();
		sCurrentHost = uri.host();
		sNewAbsPath  = uri.path();
	}
	//qDebug() << sFN << "-- before get subsystem - sMime:" << sMime << "uri.mime()" << uri.mime() << "sCurrentHost:" << sCurrentHost << "sNewAbsPath:" << sNewAbsPath;
	// -- get subsystem for mime
	bool bSubsystemChanged = false;
	PluginManager *pPluginManager = PluginManager::instance();
	if (pPluginManager->isNewSubsystem(sMime) || m_subsystem == NULL)
		bSubsystemChanged = switchSubsystem(pPluginManager, sMime);
	else
	if (! pPluginManager->isPluginRegistrered(sMime, "subsystem")) {
		qDebug() << sFN << "Cannot found registrered plugin related to mime:" << sMime;
		MessageBox::msg(m_pMainParent, MessageBox::WARNING, "Cannot found plugin related to given subsystem!", sNewPath);
		return;
	}
	qDebug() << sFN << "Mime:" << sMime << " SubsystemChanged:" << bSubsystemChanged << " subsystem:" << m_subsystem;

	if (! m_subsystem->canChangeDirTo(sInPath)) {
		qDebug() << sFN << "-- Cannot open location:" << sInPath;
		// -- check difference for mimes, if subsystem has different than current the use subsystem related to current mime
		if (m_subsystem->pluginData()->mime_type != sMime) {
			bSubsystemChanged = switchSubsystem(pPluginManager, sMime);
			qDebug() << sFN << "-- different mime, need to use subsystem related to current mime:" << m_subsystem;
		}
	}

	QString sPrevPath = (m_URI.mime() == "folder") ? m_URI.path() : "";
	if (bSubsystemChanged || sMime.startsWith("remote")) {
		m_URI = uri;
		// URI contructor sets mime to "folder" if uri points to archive and contains path within
		if (! sMime.startsWith("remote") && ! uri.host().isEmpty()) // archive
			m_URI.setMime(sMime);
	}
	m_URI.setPath(sNewAbsPath);
	if (m_URI.mime() != "folder") // no local subsystem
		m_URI.setHost(sCurrentHost);

	if (sMime == "folder" && ! sPrevPath.isEmpty())
		m_URI.setPrevLocalPath(sPrevPath);

	qDebug() << sFN << "Call" << m_subsystem << "open URI:" << m_URI.toString() << " URI.host:" << m_URI.host() << " URI.mime:" << m_URI.mime() << " prevURI:" << m_prevURI.toString();

	disconnect(m_subsystem, &Subsystem::signalShowConnectionDlg,  this, &SubsystemManager::slotShowConnectionDlg);
	connect(m_subsystem, &Subsystem::signalShowConnectionDlg,  this, &SubsystemManager::slotShowConnectionDlg);

	m_subsystem->open(m_URI, m_sPanelName);
} // slotPathChanged

void SubsystemManager::slotPathChangedForCompleter( const QString &sInPath )
{
	qDebug() << "SubsystemManager::slotPathChangedForCompleter. sInPath:" << sInPath;
	if (sInPath.isEmpty())
		return;
	QString sPath = sInPath;
	if (sPath.startsWith("~/"))
		sPath.replace("~/", QDir::homePath()+QDir::separator());
	if (sPath.contains('~'))
		sPath.replace(QRegExp(".*~/"), QDir::homePath()+QDir::separator());
	URI uri(sPath);
	if (uri.mime().isEmpty()) {
		qDebug() << "SubsystemManager::slotPathChangedForCompleter. Probably came invalid path due to empty mime.";
		return;
	}
	QString sMime = uri.mime(); // correct only for local subsystem, because checked is only path without host
	if (! uri.host().isEmpty())
		sMime = MimeType::getFileMime(sPath, false, false, true);
	if (! m_URI.host().isEmpty() && ! m_URI.mime().startsWith("remote/")) { // inside of archive
		sMime = m_URI.mime();
		uri.setMime(sMime);
	}
	switchSubsystem(PluginManager::instance(), sMime); // making sure, that operation will be invoked on properly subsystem
// 	qDebug() << "SubsystemManager::slotPathChangedForCompleter. -- invoke m_subsystem->open uri(sPath):" << uri.toString() << "host:" << uri.host() << "sMime:" << sMime << "subsystem:" << m_subsystem;
	m_subsystem->open(uri, m_sPanelName, true);
}

bool SubsystemManager::setSubsystemFromMime( const QString &sNewPathWrk )
{
	QString sFN = "SubsystemManager::setSubsystemFromMime.";
	qDebug() << sFN << "Incoming path:" << sNewPathWrk;
	URI uri(sNewPathWrk);
	QString sMime = MimeType::getMimeFromPath(sNewPathWrk);
	if (sMime.isEmpty())
		sMime = uri.mime();
	// -- get subsystem for current or new mime
	PluginManager *pPluginManager = PluginManager::instance();
	if (pPluginManager->isNewSubsystem(sMime) || m_subsystem == NULL) {
		qDebug() << sFN << "-- get subsystem for mime:" << sMime;
		switchSubsystem(pPluginManager, sMime);
	}

	return m_subsystem->canChangeDirTo(sNewPathWrk);
}


void SubsystemManager::slotSubsystemStatusChanged( Vfs::SubsystemStatus eStatus, const QString &sTargetPanel )
{
	QString sFN = "SubsystemManager::slotSubsystemStatusChanged.";
	bool bFocusInCurrentPanel = (m_sPanelName == sTargetPanel); // sTargetPanel is RequestingPanel for CREATED_LINK
// 	if (bFocusInCurrentPanel)
// 		qDebug() << sFN << "(0) Status:" << Vfs::toString(eStatus) << "current:" << m_sPanelName << "target:" << sTargetPanel << "cmd:" << Vfs::toString(m_subsystem->currentCommand());

	if (eStatus == Vfs::CONNECTING   || eStatus == Vfs::CONNECTED ||
		eStatus == Vfs::CLOSING      || eStatus == Vfs::UNCONNECTED ||
		eStatus == Vfs::LOGIN        || eStatus == Vfs::LISTING ||
		eStatus == Vfs::CHANGING_DIR || eStatus == Vfs::CHANGED_DIR ||
		eStatus == Vfs::CREATING_DIR || eStatus == Vfs::CREATING_FILE ||
		eStatus == Vfs::RENAMING     || eStatus == Vfs::REMOVING ||
		eStatus == Vfs::READING_FILE || eStatus == Vfs::OPENING_WITH ||
		eStatus == Vfs::FINDING      || eStatus == Vfs::SETTING_ATTRIBUTES ||
		eStatus == Vfs::COPYING      || eStatus == Vfs::MOVING ||
		eStatus == Vfs::ARCHIVING    || eStatus == Vfs::EXTRACTING ||
		eStatus == Vfs::COMPRESSING  || eStatus == Vfs::DECOMPRESSING ||
		eStatus == Vfs::COMPRESSED   || eStatus == Vfs::DECOMPRESSED ||
		eStatus == Vfs::DOWNLOADING  || eStatus == Vfs::UPLOADING ||
		eStatus == Vfs::WEIGHING     || eStatus == Vfs::CREATING_LINK)
	{
		if (bFocusInCurrentPanel) {
			emit signalUpdateStatusBar(Vfs::toString(eStatus), 0, true);
			if (m_pMultiProgressDlg != NULL) {
				FileInfo *pFileInfo = (m_bArchivingFromLocalSubsys) ? m_FileInfoToMime_map.constBegin().key() : m_subsystem->processedFileInfo();
				m_pMultiProgressDlg->slotUpdateFileStatus(pFileInfo, Vfs::toString(eStatus));
			}
		}
		if (eStatus == Vfs::COMPRESSED) {
			emit signalUpdateListViewStatusBar();
			if (m_bArchivingFromLocalSubsys) // creating an archive is processig
				eStatus = Vfs::ARCHIVED; // make possible properly processing operation
			else
				return;
		}
		else
			return;
	}

	QString sTargetPanelName = sTargetPanel.left(sTargetPanel.indexOf(':')); // discard view suffix from name. For LINK_CREATED this is RequestingPanel name
	bool bStatForFileView = (eStatus == Vfs::READY_TO_READ || eStatus == Vfs::NOT_READY_TO_READ || eStatus == Vfs::EXTRACTED_TO_VIEW || eStatus == Vfs::DOWNLOADED_TO_VIEW || eStatus == Vfs::READ_FILE);
	if (bStatForFileView) // required correction for bFocusInCurrentPanel, because name of view obj.has suffix
		bFocusInCurrentPanel = (m_sPanelName == sTargetPanelName);
	bool bOpenFileToViewInternally = (bStatForFileView && bFocusInCurrentPanel);
	if (bStatForFileView)
		if (! bFocusInCurrentPanel)
			return;

	bool bOtherTabInTheSamePanel = (m_sPanelName.left(m_sPanelName.indexOf('_')) == sTargetPanel.left(sTargetPanel.indexOf('_'))); // if this is panel from which user invoked an operation
	QString sRequestingPanelPath; // NOTE: for COPIED this is target path and sRequestingPanel is target panel
	//URI uriForPath;
	if (! sTargetPanelName.isEmpty()) {// empty is only for stat.: WATCHER_REQ_...
		Q_EMIT signalGetPathFromTab(sTargetPanelName, sRequestingPanelPath);
// 		emit signalGetUriFromTab(sTargetPanelName, uriForPath);
// 		sRequestingPanelPath = uriForPath.path();
	}
	bool bMatchingPaths = (sRequestingPanelPath == m_URI.toString());
	// -- prerequisits need refresh
	bool bWatcherReq = (eStatus == Vfs::WATCHER_REQ_ADD || eStatus == Vfs::WATCHER_REQ_DEL || eStatus == Vfs::WATCHER_REQ_REN || eStatus == Vfs::WATCHER_REQ_CHATR);
	QString sSenderName = ((sender() != NULL) ? sender()->objectName() : "?"); // WATCHER_REQ_... sends by this, the path is required to reread
	if (bWatcherReq) // correction for WATCHER_REQ_... is required
		bMatchingPaths = (sSenderName == m_URI.toString());

	bool bTheSameDirInBothActiveTabs = m_subsystem->theSameDirInBothPanels(); // which means panels (precisely left and right active tab)
	QString sRequestingPanel;
	emit signalGetRequestingPanelName(sRequestingPanel); // usable only for COPIED, MOVED and CREATED_LINK
	bool bFocusInRequestingPanel = (m_sPanelName == sRequestingPanel); // usable only for COPIED,MOVED and CREATED_LINK
	if (eStatus == Vfs::MOVED) { // needs correction for MatchingPaths
		Q_EMIT signalGetPathFromTab(sRequestingPanel, sRequestingPanelPath); // if sRequestingPanel is empty the is gettig current
// 		emit signalGetUriFromTab(sRequestingPanel, uriForPath); // if sRequestingPanel is empty the is gettig current
// 		sRequestingPanelPath = uriForPath.path();
		bMatchingPaths = (sRequestingPanelPath == m_URI.toString());
	}
	if (! bWatcherReq)
		m_lastStatus = UNKNOWN_STAT;

	qDebug() << sFN << "(1) Incoming status:" << Vfs::toString(eStatus) << "current obj.:" << m_sPanelName  << "target:" << sTargetPanel << "currentURI:" << m_URI.toString() << "ReqPanelPath:" << sRequestingPanelPath
			<< "FocusInCurrentPanel:" << bFocusInCurrentPanel << " MatchingPaths:" << bMatchingPaths << " OtherTabInTheSamePanel:" << bOtherTabInTheSamePanel << "RequestingPanel:" << sRequestingPanel << " SenderName:" << sSenderName
			<< "cmd:" << Vfs::toString(m_subsystem->currentCommand());

	if ((eStatus != Vfs::COPIED && eStatus != Vfs::MOVED && ! bOpenFileToViewInternally)) { // COPIED and MOVE are exception because there is need update both of panels
		if ( !(m_subsystem->currentCommand() == Vfs::MOVE && eStatus == Vfs::ERROR_OCCURED)) {
			if (! bFocusInCurrentPanel && eStatus == Vfs::OPEN_WITH)
				return;
			if (! bFocusInCurrentPanel && ! bMatchingPaths && (eStatus == Vfs::LISTED || eStatus == Vfs::VIEW_COMPL || eStatus == Vfs::WEIGHED)) // don't list already listed (in other tab) dir.
				return;
		}
	}

	if (eStatus == Vfs::LISTED || eStatus == Vfs::VIEW_COMPL || eStatus == Vfs::LISTED_COMPL || eStatus == Vfs::ERROR_OCCURED) { // errors and listing operations can be handled only if current tab
		if (! bFocusInCurrentPanel && eStatus != Vfs::ERROR_OCCURED) // (m_sPanelName != sTargetPanel)
			return;
		if (m_subsystem->error() == Vfs::CANNOT_OPEN) {
			if (m_sPanelName != sTargetPanel)
				return;
		}
	}
	else
	if (eStatus == Vfs::CREATED_DIR || eStatus == Vfs::CREATED_FILE || eStatus == Vfs::REMOVED || eStatus == Vfs::FOUND ||
		eStatus == Vfs::EXTRACTED || eStatus == Vfs::ARCHIVED ||
		eStatus == Vfs::RENAMED || bWatcherReq || eStatus == Vfs::SET_ATTRIBUTES || eStatus == Vfs::EDITED_LINK || eStatus == Vfs::DOWNLOADED)
	{
		if (! bFocusInCurrentPanel) {
			if (bOtherTabInTheSamePanel) {
				if (! bMatchingPaths)
					return;
			}
			else // tab in second panel
			if (! bTheSameDirInBothActiveTabs && ! bWatcherReq && ! bMatchingPaths)
				return;
		}
		if (bWatcherReq) {
			if (m_URI.toString() != sSenderName)
				return;
		}
	}
	else
	if (eStatus == Vfs::UNKNOWN_STAT) { // operation could be interrupted, so just update status bar
		if (m_sPanelName == sTargetPanel)
			return;
	}

	// -- get subsystem for m_URI protocol (usable for remote/local filesystem)
	m_bOperationCompleted = true; // used by cancelVfsOperation
	PluginManager *pPluginManager = PluginManager::instance();
	bool bSubsystemChanged = false;
	if (! m_vfsModel->findFileResultMode() && eStatus != Vfs::ARCHIVED && eStatus != Vfs::VIEW_COMPL && eStatus != Vfs::READ_FILE) { // in "findFile" mode m_URI.mime is empty
		if (pPluginManager->isNewSubsystem(m_URI.mime()) && eStatus != Vfs::ERROR_OCCURED && eStatus != Vfs::LOGGED_IN) {
			qDebug() << sFN << "Found new subsysten AND no error AND not logged in. Switch to subsystem related with m_URI.mime()";
			bSubsystemChanged = switchSubsystem(pPluginManager, m_URI.mime());
		}
	}
	Vfs::SubsystemCommand eCurrendCmd = m_subsystem->currentCommand();
	qDebug() << sFN << "(2) subsystem->error:" << Vfs::toString(m_subsystem->error()) << "subsystem->status:" << Vfs::toString(m_subsystem->status()) << "subsystem->command:" << Vfs::toString(eCurrendCmd);
	qDebug() << sFN << "    URI protocol:" << m_URI.protocol() << "subsystem:" << m_subsystem << "SubsystemChanged:" << bSubsystemChanged;
	FileInfoList *pProcessedFilesLst = m_subsystem->processedFiles();
	int  nProcessedFiles = pProcessedFilesLst->size();
	bool bProcessedFiles = (nProcessedFiles > 0);
	bool bNeedToUpdateModel = (bProcessedFiles && bMatchingPaths);
	bool bRemoteLocationOrArchive = (! m_URI.host().isEmpty());

	// -- prevent before empty processedFiles() list
	if (eStatus == Vfs::COPIED || eStatus == Vfs::MOVED || eStatus == Vfs::REMOVED || eStatus == Vfs::RENAMED || eStatus == Vfs::SET_ATTRIBUTES ||
		eStatus == Vfs::CREATED_DIR || eStatus == Vfs::CREATED_FILE || eStatus == Vfs::CREATED_LINK || eStatus == Vfs::EDITED_LINK || eStatus == Vfs::ARCHIVED ||
		bWatcherReq)
	{
		if (! bProcessedFiles)
			return;
	}
// 	eStatus != Vfs::LISTED && eStatus != Vfs::LISTED_COMPL && eStatus != Vfs::OPEN_WITH && ! bWatcherReq
	ProgressDialog *pProgressDlg = NULL;
	if (m_progressDlgPtrTable.size() > 0)
		pProgressDlg = m_progressDlgPtrTable.at(m_progressDlgCounter-1);

	Settings settings;
	if (eStatus == Vfs::LOGGED_IN) {
		m_URI = m_subsystem->currentURI();
		qDebug() << sFN << "Handle stat:" << Vfs::toString(eStatus) << "current URI from subsystem:" << m_URI.toString();
	}
	else
	if (eStatus == Vfs::OPEN_WITH) {
		if (m_subsystem->operationFinished()) {
			openSelectedItemsWithSelectedApp((m_subsystem->pluginData()->name == "localsubsystem_plugin")); // for other subsystems will be used temporary directory
			return;
		}
	}
	else if (eStatus == Vfs::LISTED) {
		if (eCurrendCmd == Vfs::OPEN || eCurrendCmd == Vfs::LIST) {
			m_prevURI = m_URI;
			QString sMime = MimeType::getMimeFromPath(m_URI.toString());
			if (sMime.isEmpty())
				sMime = m_URI.mime();
			// -- get subsystem for current or new mime (usable for running application with opened archive)
			bool bSubsystemChanged = false;
			PluginManager *pPluginManager = PluginManager::instance();
			if (pPluginManager->isNewSubsystem(sMime)) {
				bSubsystemChanged = switchSubsystem(pPluginManager, sMime); // possible update m_subsystem
			}
			m_URI = m_subsystem->currentURI();
			qDebug() << sFN << "Handle stat:" << Vfs::toString(eStatus) << "with command:" << Vfs::toString(eCurrendCmd) << "m_URI:" << m_URI.toString() << "mime:" << sMime << "subsystem:" << m_subsystem << "subsystemChanged:" << bSubsystemChanged << m_sPanelName;
			QString sNewAbsName = m_URI.path();
			m_selectionModel->clear();
			if (m_OpenFile) { // opened archive (level 0) or remote FS
				if (m_URI.host() == m_URI.path())
					sNewAbsName = QDir::separator();
			}
			m_URI.setPath(PathView::cleanPath(sNewAbsName));
			qDebug() << sFN << "-- update data model for list view and for completer";
			FileInfoList fil = pPluginManager->subsystem(sMime)->listDirectory();
			m_vfsModel->beginResetVfsModel();
			m_vfsModel->setFileInfoList(fil);
			m_vfsModel->endResetVfsModel();
			m_vfsModelCompl->beginResetVfsModel();
			m_vfsModelCompl->setFileInfoList(fil);
			m_vfsModelCompl->endResetVfsModel();
			//if (sMime != "folder") // for archives needs to update size of column
			//	emit signalAdjustColumnSize(); // after first start column are not resized, because files are loaded when filelistview will be visible [not truth in newer ver.of Qt]

			emit signalPathViewChange(m_URI, true, true); // signal caught by Panel::slotPathViewPathChange (sets pathview [incl.background color], tab title, completer model)

			bool backTab = false;
			if (m_bOpenInOppositeActiveTab) {
				backTab = true;
			}
			else if (bFocusInCurrentPanel) {
				qDebug() << sFN << "LISTED (1)" << m_sPanelName << "- set focus on:" << m_sPrevDirName << " (FocusInCurrentPanel)  uri:" << m_URI.toString();
				emit signalUpdateFocusSelection(false, m_sPrevDirName); // highlight given item name
			}
			if (backTab) {
				qDebug() << sFN << "LISTED (2)" << m_sPanelName << "- call BackTab";
				if (m_bOpenInOppositeActiveTab) {
					emit signalUpdateFocusSelection(true, "<|"); // << call BackTab
					m_bOpenInOppositeActiveTab = false;
				}
			}
			else
				emit signalPanelListed(); // increases counter of listed panels in MainWindow (caught by Panel)
// 			qDebug() << sFN << "End of handle for OPEN or LIST with status: LISTED";
		}
	}
	else if (eStatus == Vfs::LISTED_COMPL) {
		if (m_subsystem->currentCommand() == Vfs::LIST) {
			URI uri = m_subsystem->currentURI();
			qDebug() << sFN << "Handle stat:" << Vfs::toString(eStatus) << "subsystem->command:" << Vfs::toString(m_subsystem->currentCommand()) << "uri:" << uri.toString();
			QString sMime = MimeType::getMimeFromPath(uri.toString());
			if (sMime.isEmpty())
				sMime = uri.mime();
			qDebug() << sFN << "-- update data model";
			m_vfsModelCompl->beginResetVfsModel();
			m_vfsModelCompl->setFileInfoList(pPluginManager->subsystem(sMime)->listDirectory());
			m_vfsModelCompl->endResetVfsModel();
			emit signalPathViewChange(URI(), true, false); // signal caught by Panel::slotPathViewPathChange (sets pathview (path and color), tabTile, completer model)
		}
	}
	else if (bWatcherReq) {
		qDebug() << sFN << "Handle stat:" << Vfs::toString(eStatus) << "NeedToUpdateModel:" << bNeedToUpdateModel << " items:" << nProcessedFiles;
		if (bMatchingPaths) {
			if (eStatus == Vfs::WATCHER_REQ_ADD)
				m_vfsModel->insertItems(pProcessedFilesLst, 1, true);
			else
			if (eStatus == Vfs::WATCHER_REQ_DEL) {
				if (bFocusInCurrentPanel)
					emit signalUpdateFocusSelection(false, "^-"); // try to set the focus on previous item on the list
				m_vfsModel->removeItems(pProcessedFilesLst);
				//qDebug() << sFN << "Handle stat:" << Vfs::toString(eStatus) << "before signalUpdateFocusSelection, bFocusInCurrentPanel:" << bFocusInCurrentPanel << "currentCommand:" << Vfs::toString(m_subsystem->currentCommand()) << "status:" << Vfs::toString(m_subsystem->status()) << "m_sPanelName:" << m_sPanelName << "sTargetPanel:" << sTargetPanel;
				if (bNeedToUpdateModel) {
					if (m_lastStatus == Vfs::REMOVED)
						emit signalUpdateFocusSelection(false, "^"); // try to set the focus on previous item on the list
				}
			}
			else // WATCHER_REQ_REN and WATCHER_REQ_CHATR
				m_vfsModel->updateRowsData(0, m_subsystem->listOfItemsToModify(), pProcessedFilesLst);
			if (eStatus == Vfs::WATCHER_REQ_ADD && m_bFileRenamed) {
				m_bFileRenamed = false;
				QString sLastCreatedFileName = pProcessedFilesLst->at(nProcessedFiles-1)->fileName();
				emit signalUpdateFocusSelection(false, sLastCreatedFileName); // highlight given item name
			}
		}
	}
	else if (eStatus == Vfs::RENAMED) {
		qDebug() << sFN << "Handle stat:" << Vfs::toString(eStatus) << "NeedToUpdateModel:" << bNeedToUpdateModel;
		if (bNeedToUpdateModel) {
			m_vfsModel->updateRowsData(0, m_subsystem->listOfItemsToModify(), pProcessedFilesLst);
			if (bFocusInCurrentPanel) {
				m_bFileRenamed = true;
				QString sLastCreatedFileName = pProcessedFilesLst->at(nProcessedFiles-1)->fileName();
				emit signalUpdateFocusSelection(false, sLastCreatedFileName); // highlight given item name
			}
		}
	}
	else if (eStatus == Vfs::EDITED_LINK) {
		qDebug() << sFN << "Handle stat:" << Vfs::toString(eStatus) << "NeedToUpdateModel:" << bNeedToUpdateModel;
		if (bNeedToUpdateModel) {
			//m_vfsModel->updateRowsData(0, m_subsystem->listOfItemsToModify(), pProcessedFilesLst); // didn't work correctly (size and TargetLinkName were not updated properly)
			m_vfsModel->removeItems(pProcessedFilesLst);
			m_vfsModel->insertItems(pProcessedFilesLst, 1, true);
		}
	}
	else if (eStatus == Vfs::CREATED_DIR || eStatus == Vfs::CREATED_FILE) {
		qDebug() << sFN << "Handle stat:" << Vfs::toString(eStatus) << "NeedToUpdateModel:" << bNeedToUpdateModel << " items:" << nProcessedFiles << "postCreatingAction:" << s_bOpenDirAfterCreationIt;
		if (bProcessedFiles) {
			FileInfo *pFI = pProcessedFilesLst->at(0);
			if (pFI->filePath()+"/" != m_URI.toString() && ! m_URI.mime().startsWith("remote/"))
				return;
			if (bNeedToUpdateModel) {
				m_vfsModel->insertItems(pProcessedFilesLst);
// 				emit signalResortItems(); // workaround (not required now)
			}
			QString sLastCreatedFileName = pProcessedFilesLst->at(nProcessedFiles-1)->fileName();
			if (! s_bOpenDirAfterCreationIt) {
				if (bFocusInCurrentPanel)
					emit signalUpdateFocusSelection(false, sLastCreatedFileName); // highlight given item name
			}
			if (s_bOpenDirAfterCreationIt && bMatchingPaths && bFocusInCurrentPanel) {
				if (eStatus == Vfs::CREATED_FILE) {
					m_selectionModel->setSelected(pProcessedFilesLst->at(nProcessedFiles-1), true); // needed to selectionMap in vfsModel
					openSelectedItemsWith("", m_selectionModel->selectionMap());
				}
				else
				if (eStatus == Vfs::CREATED_DIR) {
					m_prevURI = m_URI;
					m_URI.setPath(path() + sLastCreatedFileName);
					slotPathChanged(m_URI.toString());
					//if (bFocusInCurrentPanel)
						emit signalUpdateFocusSelection(false, sLastCreatedFileName); // highlight given item name
				}
			}
		}
	}
	else if (eStatus == Vfs::REMOVED) {
		m_lastStatus = eStatus;
		qDebug() << sFN << "Handle stat:" << Vfs::toString(eStatus) << "with command:" << Vfs::toString(eCurrendCmd) << "NeedToUpdateModel:" << bNeedToUpdateModel << " items:" << nProcessedFiles;
		if (bNeedToUpdateModel) {
			if (bRemoteLocationOrArchive) {
				emit signalUpdateFocusSelection(false, "^-"); // try to set the focus on previous item on the list
				m_vfsModel->removeItems(pProcessedFilesLst);
			}
			if (bFocusInCurrentPanel && pProgressDlg != NULL) {
				pProgressDlg->timerStop();
				if (m_subsystem->operationFinished())
					removeProgressDlg(true);
			}
		}
		if (m_ffdItemRemoved != NULL)
			emit signalUpdateRemovedOnFindFileDialog(m_ffdItemRemoved);
		if (bFocusInCurrentPanel && bNeedToUpdateModel) {
			if (bRemoteLocationOrArchive) // in localsubsystem focus is handled by WATCHER_REQ_DEL
				emit signalUpdateFocusSelection(false, "^"); // try to set the focus on previous item on the list
		}
	}
	else if (eStatus == Vfs::SET_ATTRIBUTES) {
		qDebug() << sFN << "Handle stat:" << Vfs::toString(eStatus) << "NeedToUpdateModel:" << bNeedToUpdateModel << "name:" << m_subsystem->listOfItemsToModify().at(nProcessedFiles-1) << " items:" << nProcessedFiles;
		emit signalClosePropertiesDlg();
		m_selectionModel->setSelected(pProcessedFilesLst->at(nProcessedFiles-1), false, true, true, false);
		if (bNeedToUpdateModel)
			m_vfsModel->updateRowData(0, m_subsystem->listOfItemsToModify().at(nProcessedFiles-1), pProcessedFilesLst->at(nProcessedFiles-1));
	}
	else if (eStatus == Vfs::COPIED || eStatus == Vfs::UPLOADED) {
		qDebug() << sFN << "Handle stat:" << Vfs::toString(eStatus) << "NeedToUpdateModel:" << bNeedToUpdateModel << " items:" << nProcessedFiles;
		if (bMatchingPaths) {
			if (eStatus == Vfs::UPLOADED || bRemoteLocationOrArchive) {
				m_vfsModel->insertItems(pProcessedFilesLst, 1, true);
				m_selectionModel->setSelected(pProcessedFilesLst, false, true, true, false); // required to newly put item wasn't selected
			}
		}
		if (bFocusInCurrentPanel) {
			if (pProgressDlg != NULL) {
				pProgressDlg->timerStop();
				if (m_subsystem->operationFinished())
					removeProgressDlg(true);
			}
			if (eStatus == Vfs::UPLOADED)
				emit signalForceUnmarkItemsInOppositeActiveTab(pProcessedFilesLst); // required for UPLOADED due to below "else" block is never reached
		}
		else
		if (bFocusInRequestingPanel) {
			qDebug() << sFN << "  try to unmark item:" << m_selectionModel->firstMarked()->fileName();
			m_selectionModel->setSelected(m_selectionModel->firstMarked(), false);
		}
	}
	else if (eStatus == Vfs::MOVED) {
		qDebug() << sFN << "Handle stat:" << Vfs::toString(eStatus) << "NeedToUpdateModel:" << bNeedToUpdateModel << " items:" << nProcessedFiles;
		if (! bMatchingPaths && bOtherTabInTheSamePanel && ! sRequestingPanel.isEmpty()) {
			if (bRemoteLocationOrArchive)
				m_vfsModel->insertItems(pProcessedFilesLst, 1, true);
			m_selectionModel->setSelected(pProcessedFilesLst, false, true, true); // required to newly put item wasn't selected
		}
		if (bFocusInCurrentPanel) {
			if (pProgressDlg != NULL) {
				pProgressDlg->timerStop();
				if (m_subsystem->operationFinished())
					removeProgressDlg(true);
			}
		}
		else {
			if (bFocusInRequestingPanel || bMatchingPaths) { // this is panel where user call MOVE or panel with the same path like panel where was called MOVE
				m_selectionModel->setSelected(pProcessedFilesLst, false, true, true);
				if (m_subsystem->postMovedFiles()->size() > 0)  // try remove even panel is hidden, so nothing can remove
					m_vfsModel->removeItems(m_subsystem->postMovedFiles());
				if (bFocusInRequestingPanel)
					emit signalUpdateFocusSelection(false, "^"); // set focus on given item name
			}
		}
		if (sRequestingPanel.isEmpty() && bMatchingPaths) {
			if (m_subsystem->postMovedFiles()->size() > 0)  // try remove even panel is hidden, so nothing can remove
				m_vfsModel->removeItems(m_subsystem->postMovedFiles());
			if (bFocusInRequestingPanel)
				emit signalUpdateFocusSelection(false, "^"); // set focus on given item name
		}
	}
	else if (eStatus == Vfs::CREATED_LINK) {
		if (bMatchingPaths) {
			qDebug() << sFN << "Handle stat:" << Vfs::toString(eStatus) << " unmarked items:" << nProcessedFiles;
			m_selectionModel->setSelected(pProcessedFilesLst, false, true, true); // force unmark
		}
	}
	else if (eStatus == Vfs::DOWNLOADED) {
		qDebug() << sFN << "Handle stat:" << Vfs::toString(eStatus);
		if (! m_bExtractingFromLocalSubsys) { // remote or archive
			qDebug() << sFN << " - extracting from local subsystem. Target directory:" << m_sTargetPathToExtracting;
			removeProgressDlg(false); // remove download progress dialog
			FileInfo *pFileInfo = m_selectionModel->firstMarked();
			switchSubsystem(PluginManager::instance(), pFileInfo->mimeInfo());
			showProgressDialog(m_bWeighBeforeArchExtracting, Vfs::EXTRACT);
			m_subsystem->extractTo(Vfs::EXTRACT_ARCHIVE, false, m_selectionModel->selectionMap(), m_sTargetPathToExtracting, m_sPanelName); // false -> COPY
			m_bExtractingFromLocalSubsys = true;
			m_sDwnArchForExtractFromRemoteLocation = Subsystem::absFileNameInLocalFS(pFileInfo, m_sTmpDir);
			if (m_pMultiProgressDlg != NULL)
				m_pMultiProgressDlg->slotUpdateFileStatus(m_subsystem->processedFileInfo(), Vfs::toString(eStatus));
		}
		else if (m_bOpenArchInRemoteLocation) {
			qDebug() << sFN << " - try to open archive located at remote location. Archive downloaded to:" << m_sTmpDir;
			removeProgressDlg(false); // remove download progress dialog
			m_bOpenArchInRemoteLocation = false;
			FileInfo *pFileInfo = m_pHelperSelectionMap->constBegin().key(); // get first item from map
			switchSubsystem(PluginManager::instance(), pFileInfo->mimeInfo());
			m_prevURI = m_URI;
			m_remoteURI = m_URI;
			m_URI.reset();
			m_OpenFile = true;
			QString sNewHost, sNewPath;
			QString sMime = MimeType::getMimeFromPath(Subsystem::absFileNameInLocalFS(pFileInfo, m_sTmpDir), sNewHost, sNewPath); // also sets: sNewHost, sNewPath
			m_URI.setProtocol("file");
			m_URI.setHost(sNewHost);
			m_URI.setPath(sNewPath);
			m_URI.setMime(sMime);
			qDebug() << sFN << " - open m_URI:" << m_URI.toString();
			m_sOpenedArchFromRemoteLocation = m_URI.host();
			m_subsystem->init(m_pMainParent);
			m_subsystem->open(m_URI, m_sPanelName);
		}
		else {
			qDebug() << sFN << " - unmark items in:" << m_sPanelName << "and update status bar";
			m_selectionModel->setSelected(pProcessedFilesLst, false, true, true);
		}
	}
	else if (eStatus == Vfs::ARCHIVED) {
		qDebug() << sFN << "Handle stat:" << Vfs::toString(eStatus) << "with command:" << Vfs::toString(eCurrendCmd) << "NeedToUpdateModel:" << bNeedToUpdateModel << " items:" << nProcessedFiles;
		if (bNeedToUpdateModel) {
			if (eCurrendCmd == Vfs::COPY_TO_ARCHIVE || eCurrendCmd == Vfs::MAKE_ARCHIVE)
				emit signalForceUnmarkItemsInOppositeActiveTab(pProcessedFilesLst);
			if (m_bArchivingFromLocalSubsys) // creating an archive is still processig
				m_selectionModel->setSelected(pProcessedFilesLst, false, true, true);
			else
				m_vfsModel->insertItems(pProcessedFilesLst, 1, true);
			if (bFocusInCurrentPanel) {
				if (pProgressDlg != NULL || m_pMultiProgressDlg != NULL) {
					if (pProgressDlg != NULL)
						pProgressDlg->timerStop();
					if (m_subsystem->operationFinished()) { // after creating second archive here is false
						removeProgressDlg(true);
						if (m_bMoveToArchive) {
							//bool bWeighBeforeRemoving = settings.value("vfs/WeighBeforeRemoving", true).toBool();
							// switch to source subsystem and call removing item(s)
							m_pHelperSelectionMap->clear();
							foreach(FileInfo *pFI, *pProcessedFilesLst) { // was used m_subsystem->postMovedFiles(); // commented because when selected 2 here is only 1
								m_pHelperSelectionMap->insert(pFI, 0); // usage fake index
							}
							if (m_bArchivingFromLocalSubsys) { // creating an archive is processig so remove file/dir in current panel
								//m_bDeleteWithoutConfirmation = true;
								bool bAskBeforeRemoving = s_bAlwaysAskBeforeRemoving;
								s_bAlwaysAskBeforeRemoving = false;
								removeFiles(m_pHelperSelectionMap, NULL);
								//m_bDeleteWithoutConfirmation = false;
								s_bAlwaysAskBeforeRemoving = bAskBeforeRemoving;
							}
							else
								emit signalRemoveFiles(m_pHelperSelectionMap, s_bWeighBeforeRemoving); // invoke removing files in opposite panel. Thanks that ProgressDlg will be visible and removeable
						}
						emit signalUpdateListViewStatusBar(); // helpful if file has been overwritten
					}
				}
			}
			if (m_bArchivingFromLocalSubsys) { // creating an archive is processig
				FileInfo *pFileInfo = m_FileInfoToMime_map.constBegin().key(); // get first item from map
				if (pFileInfo == NULL) {
					qDebug() << sFN << "Internal ERROR. FileInfo is NULL (got from in m_FileInfoToMime_map)!";
					return;
				}
				m_FileInfoToMime_map.take(pFileInfo); // drop item, because it's not yet required
				if (m_pMultiProgressDlg != NULL) {
					m_pMultiProgressDlg->slotUpdateCounterOfProcessed(++m_nCounterOfProcessed, false);
					m_pMultiProgressDlg->slotUpdateFileStatus(m_subsystem->processedFileInfo(), Vfs::toString(eStatus));
				}
				if (m_FileInfoToMime_map.size() > 0) {
					preArchiveNextItem(); // launch weighing (for next item) if required or just call archiveNextItem
					return;
				}
				else {
					if (m_pMultiProgressDlg != NULL)
						m_pMultiProgressDlg->slotUpdateLabelOfCancelBtnWithDone();
					if (m_progressDlgPtrTable.size() > 0)
						m_progressDlgPtrTable.at(m_progressDlgCounter-1)->slotUpdateLabelOfCancelBtnWithDone();
					if ((uint)m_FileInfoToMime_map.size() == 0) { // archiving finished
						removeMultiProgressDlg(true);
					}
				}
			}
			m_bArchivingFromLocalSubsys = false;
			switchSubsystem(PluginManager::instance(), "folder"); // need to use to be in subsystem used by last archive due to pending operation on it
		}
	}
	else if (eStatus == Vfs::EXTRACTED) {
		qDebug() << sFN << "Handle stat:" << Vfs::toString(eStatus) << "; unmark items and update status bar";
		if (m_FileInfoToMime_map.size() > 0) { // extract archive(s)
			FileInfo *pFileInfo = m_FileInfoToMime_map.constBegin().key(); // get first item from map
			if (pFileInfo == NULL) {
				qDebug() << sFN << "Internal ERROR. FileInfo NULL pointer in m_FileInfoToMime_map!";
				return;
			}
			if (! m_sDwnArchForExtractFromRemoteLocation.isEmpty()) {
				QFile::remove(m_sDwnArchForExtractFromRemoteLocation);
				qDebug() << sFN << " - removed archives:" << m_sDwnArchForExtractFromRemoteLocation << "downloaded to be able extract it";
				m_sDwnArchForExtractFromRemoteLocation = "";
			}
			switchSubsystem(PluginManager::instance(), pFileInfo->mimeInfo()); // need to use be in subsystem used by last archive due to pending operation on it
			if (m_pMultiProgressDlg != NULL) {
				//qDebug() << sFN << "getProcessedItem from:" << m_subsystem << "FileInfo:" << pFileInfo->mimeInfo();
				m_pMultiProgressDlg->slotUpdateCounterOfProcessed(++m_nCounterOfProcessed, false);
				m_pMultiProgressDlg->slotUpdateFileStatus(m_subsystem->processedFileInfo(), Vfs::toString(eStatus));
			}
			m_FileInfoToMime_map.take(pFileInfo); // drop item, because it's not yet required
			m_selectionModel->setSelected(pFileInfo, false);
			if (pProgressDlg != NULL) {
				pProgressDlg->timerStop();
				if (m_subsystem->operationFinished()) {
					removeProgressDlg(true);
				}
			}
			else
			if (m_pMultiProgressDlg != NULL) {
				if (m_nTotalProcessed == m_nCounterOfProcessed)
					removeMultiProgressDlg(true);
			}
			if (bMatchingPaths && m_whereExtractArchive == Vfs::EXTRACT_HERE) { // need to update current ListView manualy, because LocalSubsystemWatcher wasn't able to send signal (signal was already sent from this panel for EXTRACT)
				if (m_subsystem->postMovedFiles() != NULL && m_subsystem->postMovedFiles()->size() > 0)
					m_vfsModel->insertItems(m_subsystem->postMovedFiles(), 1, true);
			}
			if (m_FileInfoToMime_map.size() > 0)
				extractNextArchive();
		}
		else { // extracted files
			if (m_subsystem->currentCommand() != Vfs::EXTRACT_AND_REMOVE)
				m_selectionModel->setSelected(pProcessedFilesLst, false);
			if (pProgressDlg != NULL) {
				pProgressDlg->timerStop();
				//qDebug() << sFN << "extracted_files - m_subsystem:" << m_subsystem;
				if (m_subsystem->operationFinished())
					removeProgressDlg(true);
			}
			if (m_subsystem->currentCommand() == Vfs::EXTRACT_AND_REMOVE) {
				//bool bWeighBeforeRemoving = settings.value("vfs/WeighBeforeRemoving", true).toBool();
				startRemoveFiles(m_selectionModel->selectionMap(), s_bWeighBeforeRemoving);
			}
			emit signalUpdateListViewStatusBar();
		}
	}
	else if (eStatus == DECOMPRESSED_TO_VIEW) {
		qDebug() << sFN << "Handle stat:" << Vfs::toString(eStatus) << "; unmark items and update status bar";
		if (m_pSelectedItemToView->size() > 0) { // extract archive(s)
			FileInfo *pFileInfo = m_pSelectedItemToView->at(0); // get first item from map
			if (pFileInfo == NULL) {
				qDebug() << sFN << "Internal ERROR. FileInfo NULL pointer in m_pSelectedItemToView!";
				return;
			}
			switchSubsystem(pPluginManager, "folder"); // we need LocalSubsystem to be able to read extracted file localy
			qDebug() << sFN << "-- switched to:" << m_subsystem << "to be able to read extracted file";
			m_pFileViewer->readExtractedFile();
		}
	}
	else if (eStatus == Vfs::WEIGHED) {
		qint64 totalWeight, realWeight;
		uint nFiles, nDirs;
		if (m_bUpdateModelAfterWeighing)
			emit signalWeighStopAnimation();
		else {
			QString sMime = MimeType::getFileMime(m_sTargetPathToExtracting);
			bool bPluginSupport = PluginFactory::instance()->isPluginAvailable(sMime, "subsystem");
			if (! bPluginSupport) { // this is NOT compressed file
				switchSubsystem(PluginManager::instance(), sMime); // making sure, that operation will be invoked on properly subsystem. For regular file will be returned error, but we can ignore it
			}
		}
		m_subsystem->getWeighingResult(totalWeight, realWeight, nFiles, nDirs);
		switchSubsystem(PluginManager::instance(), m_URI.mime()); // restore current subsystem
		qDebug() << sFN << "Handle stat:" << Vfs::toString(eStatus) << "totalWeigh:" << totalWeight << FileInfo::bytesRound(totalWeight) << "files:" << nFiles << "dirs:" << nDirs;
		if (m_bUpdateModelAfterWeighing) {
			if (bProcessedFiles) {
				FileInfo *prfi = pProcessedFilesLst->at(0);
				FileInfo *pFI = m_vfsModel->fileInfoPtr(prfi->fileName());
				pFI->setData(Vfs::COL_SIZE, totalWeight);
				pFI->setDirLabel(FileInfo::bytesRound(totalWeight));
				m_vfsModel->updateRowData(Vfs::COL_NAME, pFI->fileName(), pFI);
				m_selectionModel->setSelected(pFI, true, true);
				emit signalUpdateFocusSelection(false, pFI->fileName()); // set focus on last item
			}
			else
				qDebug() << sFN << "-- empty processedFiles list in subsystem";
		}
		else {
			if (m_bArchivingFromLocalSubsys) { // creating an archive is processig
				m_nTotalWeight = totalWeight;
				m_nTotalWeighedItems = (nFiles + nDirs);
				archiveNextItem(); // subsystem will get the weight result
			}
			else {
				emit signalWeighingResult(totalWeight, realWeight, nFiles, nDirs); // caught by PropertiesDialog (created in FileListView)
				// progress dialog is updated by signals sending from Subsystem class
				emit signalUpdateListViewStatusBar();
			}
		}
	}
	else if (eStatus == Vfs::FOUND) {
		qDebug() << sFN << "Handle stat:" << Vfs::toString(eStatus) << "NeedToUpdateModel:" << bNeedToUpdateModel << " items:" << nProcessedFiles;
		if (bProcessedFiles) {
			FileInfo *pFI = pProcessedFilesLst->at(nProcessedFiles-1);
			emit signalAddMatchedItem(pFI, m_subsystem->searchingResult()); // caught by FindFileDialog in FileListView
		}
		if (m_subsystem->operationFinished()) { // or bProcessedFiles == false
			emit signalSearchingFinished(); // caught by FindFileDialog in FileListView
		}
		emit signalUpdateListViewStatusBar();
	}
	else if (eStatus == Vfs::OPR_STOPPED) {
		m_bArchivingFromLocalSubsys = false;
		qDebug() << "Handle stat:" << Vfs::toString(eStatus) << sFN << "with command:" << Vfs::toString(m_subsystem->currentCommand());
		if (m_subsystem->currentCommand() == Vfs::WEIGH) { // restore string <DIR>
			FileInfo *fi = pProcessedFilesLst->at(0);
			fi->setDirLabel(); // default set "<DIR>"
			//if (bMatchingPath)
				m_vfsModel->updateRowData(Vfs::COL_NAME, fi->fileName(), fi);
			m_selectionModel->setSelected(fi, false);
		}
		else
		if (m_subsystem->currentCommand() == Vfs::CONNECT) {
			if (bRemoteLocationOrArchive) {
				if (m_URI.protocol() != "file")
					m_subsystem->close(true); // force close the connection attempt
			}
		}
		return;
	}
	else if (eStatus == Vfs::OPR_PAUSED) {
		qDebug() << sFN << "Handle stat:" << Vfs::toString(eStatus) << "with command:" << Vfs::toString(m_subsystem->currentCommand());
		if (m_subsystem->currentCommand() == Vfs::FIND) {
// 			emit signalSearchingPaused();
		}
	}
	else if (eStatus == Vfs::EXTRACTED_TO_VIEW || eStatus == Vfs::DOWNLOADED_TO_VIEW) {
		if (m_subsystem->currentCommand() == Vfs::READ_FILE_TO_VIEW) { // extracted file is ready to view
			qDebug() << sFN << "Handle stat:" << Vfs::toString(eStatus) << "with command:" << Vfs::toString(m_subsystem->currentCommand());
			switchSubsystem(pPluginManager, "folder"); // we need LocalSubsystem to be able to read extracted file localy
			qDebug() << sFN << "-- switched to:" << m_subsystem << "to be able to read extracted file";
			m_pFileViewer->readExtractedFile();
		}
	}
	else if (eStatus == Vfs::READ_FILE) {  // file has been read
		qDebug() << sFN << "Handle stat:" << Vfs::toString(eStatus) << "wuth command:" << Vfs::toString(m_subsystem->currentCommand());
		if (m_subsystem->currentCommand() == Vfs::READ_FILE_TO_VIEW) {
			//qDebug() << sFN << "Handle command:" << Vfs::toString(m_subsystem->currentCommand()) << "try to unmark item:" << m_selectionModel->firstMarked()->fileName();
			//if (m_bFileWasExtracted) { // this is required only when file was extracted/downloaded previously
				if (! m_vfsModel->findFileResultMode())
					switchSubsystem(pPluginManager, m_URI.mime()); // we need LocalSubsystem to read extracted file
				//qDebug() << sFN << "-- back to previous subsystem related with mime:" << m_URI.mime() << "which is:" << m_subsystem;
				m_selectionModel->setSelected(m_selectionModel->firstMarked(), false);
			//}
		}
		else if (m_subsystem->status() == Vfs::VIEW_COMPL) {
			qDebug() << sFN << "Handle command:" << Vfs::toString(m_subsystem->currentCommand()) << "try to unmark item:" << m_selectionModel->firstMarked()->fileName();
			m_selectionModel->setSelected(m_selectionModel->firstMarked(), false);
		}
	}
	else if (eStatus == Vfs::NOT_READY_TO_READ) {
		//QString sOpeningFileName = pProcessedFilesLst->at(0).fileName();
		QApplication::restoreOverrideCursor();
		if (! m_subsystem->operationFinished())
			MessageBox::msg(m_pMainParent, MessageBox::CRITICAL, tr("Cannot view following file:")+"\n\n\""+m_subsystem->processedFileName()+"\"\n\n"+ tr("Probably due to read permission issue."));
		m_selectionModel->setSelected(m_pSelectedItemToView, false);
		if (m_nViewerCounter > 0)
			m_nViewerCounter--;
		delete m_pFileViewer; //m_pFileViewer->close();
		return;
	}
	else if (eStatus == Vfs::ERROR_OCCURED) { // bFocusInCurrentPanel
		// prevent to display error multiple times. This can happen because one subsystem might be connected with multi tabs.
		if (! bFocusInCurrentPanel) {
			if (bOtherTabInTheSamePanel) {
				if (! bMatchingPaths)
					return;
			}
			else // tab in second panel
			if (! bTheSameDirInBothActiveTabs && ! bWatcherReq && ! bMatchingPaths)
				return;
		}
		m_bOperationCompleted = true; // used by cancelVfsOperation
		const Vfs::SubsystemError errorCode = m_subsystem->error();
		m_bArchivingFromLocalSubsys = false;
		qDebug() << sFN << "Handle stat:" << Vfs::toString(eStatus) << "with command:" << Vfs::toString(m_subsystem->currentCommand()) << "errorCode" << Vfs::toString(errorCode) << "NeedToUpdateModel:" << bNeedToUpdateModel << " items:" << nProcessedFiles << "m_prevURI:" << m_prevURI.toString();

		if (eCurrendCmd == Vfs::SET_ATTRIBUTES_CMD)
			emit signalClosePropertiesDlg();
		if (errorCode == Vfs::CANNOT_RENAME) {
			qDebug() << sFN << "-- errorCode: CANNOT_RENAME";
			if (bProcessedFiles) {
				FileInfo *fi = pProcessedFilesLst->at(nProcessedFiles-1);
				QString sDirOrFile = fi->isDir() ? tr("directory") : tr("file");
				QString sOldName = m_subsystem->listOfItemsToModify().at(nProcessedFiles-1);
				MessageBox::msg(m_pMainParent, MessageBox::CRITICAL, tr("Cannot rename")+" "+sDirOrFile, sOldName+"\n\tto\n"+fi->fileName()+"\n");
			}
			return;
		}
		else if (errorCode == Vfs::CANNOT_CREATE_DIR || errorCode == Vfs::CANNOT_CREATE_FILE) {
			qDebug() << sFN << "-- errorCode: CANNOT_CREATE_DIR or CANNOT_CREATE_FILE";
			QString sFileName, sTypeOfFile = (errorCode == Vfs::CANNOT_CREATE_DIR) ? tr("directory") : tr("file");
			if (bProcessedFiles) {
				FileInfo *pFI = pProcessedFilesLst->at(nProcessedFiles-1);
				sFileName = pFI->absoluteFileName();
			}
			else {
				sFileName = m_subsystem->processedFileName();
			}
			QString sMsg = tr("Cannot create following") + " " + sTypeOfFile + ":";
			MessageBox::msg(m_pMainParent, MessageBox::CRITICAL, sFileName, sMsg, eCurrendCmd);
			m_newlyCreatedEmpyFileName = "";
			return;
		}
		else if (errorCode == Vfs::CANNOT_REMOVE) {
			qDebug() << sFN << "-- errorCode: CANNOT_REMOVE, operationFinished:" << m_subsystem->operationFinished() << "showMsgCantRmFileAgain:" << m_bShowMsgCantRmFileAgain << "processedFile:" << m_subsystem->processedFileName();
			if (pProgressDlg != NULL)
				pProgressDlg->timerStop();
			if (m_subsystem->operationFinished())
				removeProgressDlg(true);
			qDebug() << sFN << "-- command:" << Vfs::toString(eCurrendCmd) << "reqPanel:" << sRequestingPanel << " ProcessedFiles:" << nProcessedFiles;
			if (eCurrendCmd == Vfs::MOVE) {
				if (bProcessedFiles) { // update model in panel beside, because copying finished success
					if (bMatchingPaths)
						m_vfsModel->insertItems(pProcessedFilesLst, 1, true);
					m_selectionModel->setSelected(pProcessedFilesLst, false, true, true, false);
				}
				else {
					qDebug() << sFN << "deselect in" << m_sPanelName << "items:" << nProcessedFiles;
					m_selectionModel->setSelected(pProcessedFilesLst, false, true, true); // no effect due to item(s) is/are unmarked in first calling of this fun.
				}
			}
			FileInfo *pFI = NULL;
			QString sMsg = tr("Cannot remove all selected file(s) or/and directory(ies)");
			if (bProcessedFiles) {
				if (m_selectionModel->count() > 0) {
					pFI = m_selectionModel->selectionMap()->begin().key();
					m_selectionModel->setSelected(pFI, false, false); // last false -> use map to find item
				}
				else { // get item(s) from pProcessedFilesLst
					FileInfoList *pFileList = m_subsystem->postMovedFiles(); // supported only by local Subsystem
					if (pFileList == NULL)
						pFileList = m_subsystem->processedFiles();
					int nPostMovedFiles = pFileList->count();
					if (nPostMovedFiles > 0) {
						pFI = pFileList->at(nPostMovedFiles-1);
						m_selectionModel->setSelected(pFI, false, true); // last true -> find name on list
					}
				}
				if (pFI != NULL) {
					QString sFileType = (pFI->isDir() ? tr("directory") : tr("file"));
					if (pFI->isSymLink())
						sFileType = tr("symbolic link");
					sMsg = pFI->absoluteFileName() + "\n\n" + tr("Cannot remove")+" "+sFileType+"!";
				}
			}
			else {
				if (m_selectionModel->count() > 0) {
					qDebug() << sFN << "Some files are selected, but cannot find processed files.";
					//pFI = m_selectionModel->selectionMap()->begin().key();
					//m_selectionModel->setSelected(pFI, false, false); // last false -> use map to find item
				}
			}
			if (eCurrendCmd != Vfs::MOVE || m_URI.mime().startsWith("remote/")) // showing error message is handled in local Subsystem class
				MessageBox::msg(m_pMainParent, MessageBox::CRITICAL, sMsg);
			return;
		}
		else if (errorCode == Vfs::CANNOT_SET_PERM || errorCode == Vfs::CANNOT_SET_MA_TIME || errorCode == Vfs::CANNOT_SET_OWNER_GROUP) {
			// NOTE. For local subsystem this code probably will be never called due to error handle in LocalSubsystem class through met.: showMsgCannotDo (tested for 2 sel.files). Exists just in case.
			if (bMatchingPaths) {
				qDebug() << sFN << "-- errorCode: CANNOT_SET_ATTRIBUTE, operationFinished:" << m_subsystem->operationFinished();
				if (pProgressDlg != NULL)
					pProgressDlg->timerStop();
				QString sMsg, sFI = m_subsystem->processedFileName();
				if (errorCode == Vfs::CANNOT_SET_PERM)
					sMsg = "Cannot set permissions";
				else if (errorCode == Vfs::CANNOT_SET_MA_TIME)
					sMsg = "Cannot set last access time or last modification time";
				else if (errorCode == Vfs::CANNOT_SET_OWNER_GROUP)
					sMsg = "Cannot set owner and/or group";
				MessageBox::msg(m_pMainParent, MessageBox::CRITICAL, sMsg, sFI);
				if (m_subsystem->operationFinished())
					removeProgressDlg(true);
			}
			return;
		}
		else if (errorCode == Vfs::ALREADY_EXISTS) {
			qDebug() << sFN << "--- errorCode: ALREADY_EXISTS";
			QString fileName = m_subsystem->processedFileName();
			QString dirOrFile = tr("Directory or file");
			MessageBox::msg(m_pMainParent, MessageBox::INFORMATION, dirOrFile + " " + tr("already exists."), fileName);
			return;
		}
		else if (errorCode == Vfs::CANNOT_OPEN || errorCode == Vfs::CANNOT_STAT) {
// 			qDebug() << "cmd" << Vfs::toString(currendCmd);
			if (eCurrendCmd == Vfs::WEIGH || eCurrendCmd == Vfs::REMOVE) {
				if (eCurrendCmd == Vfs::WEIGH) {
					emit signalWeighStopAnimation();
					emit signalWeighingResult(-1, -1, 0, 0); // caugh by PropertiesDlg
					emit signalShowErrorWeighingInfoOnPropertiesDlg(true);
				}
				QString sLocation;
				if (eCurrendCmd == Vfs::WEIGH)
					sLocation = m_subsystem->processedFileName();
				else {
					if (bProcessedFiles)
						sLocation = pProcessedFilesLst->at(0)->absoluteFileName();
					if (sLocation.isEmpty())
						sLocation = m_URI.toString();
				}
				MessageBox::msg(m_pMainParent, MessageBox::CRITICAL, sLocation, tr("Cannot open or read following location:"), eCurrendCmd);
				return;
			}
			else if (eCurrendCmd == Vfs::FIND) {
				QString sMsg;
				if (errorCode == Vfs::CANNOT_OPEN)
					sMsg = tr("Cannot open location.");
				else // most likely CANNOT_STAT
					sMsg = tr("Cannot check file. No such file or directory.");
				MessageBox::msg(m_pMainParent, MessageBox::CRITICAL, sMsg, m_subsystem->processedFileName(), eCurrendCmd);
				return;
			}
			else {
				MessageBox::msg(m_pMainParent, MessageBox::CRITICAL, tr("Cannot open location. Let's try to use parent's path."), m_URI.toString());
				emit signalPathViewChange(m_URI, false, false); // signal caught by Panel::slotPathViewPathChange (removes current URI from path view)
				// -- find existng (in file system) path
				QString sCurrentPath = m_URI.toString();
				QString sNewFixedPath = sCurrentPath.left(sCurrentPath.lastIndexOf(QDir::separator(), -2)) + QDir::separator();
				m_prevURI.setUri(sNewFixedPath); // returned URI will be empty if path doesn't exist in file system and this is not inside of archive
				while (m_prevURI.toString().isEmpty()) {
					sNewFixedPath = sNewFixedPath.left(sNewFixedPath.lastIndexOf(QDir::separator(), -2)) + QDir::separator();
					m_prevURI.setUri(sNewFixedPath);
				}
				slotPathChanged(m_prevURI.toString());
				return;
			}
		}
		else if (errorCode == Vfs::CANNOT_READ) {
			emit signalWeighStopAnimation();
			removeProgressDlg();
			QString sLocation;
			if (bProcessedFiles)
				sLocation = pProcessedFilesLst->at(0)->absoluteFileName();
			if (sLocation.isEmpty())
				sLocation = m_URI.toString();
			if (eCurrendCmd == Vfs::WEIGH) {
				MessageBox::msg(m_pMainParent, MessageBox::CRITICAL, tr("Cannot read location."), sLocation, eCurrendCmd);
				emit signalWeighingResult(-1, -1, 0, 0);
			}
			else
			if (eCurrendCmd == Vfs::COPY || eCurrendCmd == Vfs::MOVE) {
				QString sFileType = tr("file or directory");
				QString sMsg = tr("Cannot read following");
				if (m_subsystem->postMovedFiles()->size() > 0) {
					FileInfo *pFI = m_subsystem->postMovedFiles()->at(m_subsystem->postMovedFiles()->count()-1);
					// unmarking is not possible due to currect panel is different than source
					//m_selectionModel->setSelected(pFI, false, true); // last true -> find name on list
					sLocation = pFI->absoluteFileName();
					sFileType = pFI->isDir() ? tr("directory") : tr("file");
				}
				else
					sLocation = m_subsystem->processedFileName();
				sMsg += " " + sFileType + ":";
				MessageBox::msg(m_pMainParent, MessageBox::CRITICAL, sLocation, sMsg, eCurrendCmd);
				//emit signalUpdateListViewStatusBar();
			}
			else
			if (eCurrendCmd == Vfs::OPEN)
				MessageBox::msg(m_pMainParent, MessageBox::CRITICAL, tr("Cannot open location."), m_subsystem->processedFileName());
			else
				MessageBox::msg(m_pMainParent, MessageBox::CRITICAL, tr("Cannot read location."), sLocation);
			return;
		}
		else if (errorCode == Vfs::CANNOT_WRITE) {
			if (m_subsystem->operationFinished() || eCurrendCmd == Vfs::CREATE_LINK || eCurrendCmd == Vfs::COPY || eCurrendCmd == Vfs::MOVE || eCurrendCmd == Vfs::EXTRACT) {
				if (m_subsystem->operationFinished()) { // when appears an error for last file
					emit signalWeighStopAnimation();
					removeProgressDlg();
				}
				QString sLocation;
				FileInfo *pFI = NULL;
				if (bProcessedFiles) {
					pFI = pProcessedFilesLst->at(0);
					sLocation = pFI->absoluteFileName();
				}
				if (sLocation.isEmpty()) {
					if (eCurrendCmd == Vfs::MOVE) {
						if (m_subsystem->postMovedFiles()->size() > 0)
							pFI = m_subsystem->postMovedFiles()->at(m_subsystem->postMovedFiles()->count()-1);
						sLocation = m_URI.toString();
					}
					else if (eCurrendCmd == Vfs::COPY || eCurrendCmd == Vfs::EXTRACT) {
						sLocation = m_subsystem->processedFileName(); // retested for remote FS in local pProcessedFilesLst is not empty
					}
				}
				if (pFI == NULL)
					pFI = m_subsystem->processedFileInfo();
				QString sFileType, sMsg = tr("Cannot write to following location:");
				if (pFI != NULL) {
					sMsg = tr("Cannot")+" "+(eCurrendCmd == Vfs::COPY ? tr("copy") : tr("create") );
					if (eCurrendCmd == Vfs::CREATE_LINK)
						sFileType = tr("link");
					else
						sFileType = pFI->isDir() ? tr("directory") : tr("file");
					sMsg += " "+sFileType+" "+tr("to selected location.");
					sLocation = pFI->fileName();
					m_selectionModel->setSelected(pFI, false);
				}
				if (eCurrendCmd != Vfs::CREATE_LINK) // showing error message is handled in Subsystem class
					MessageBox::msg(m_pMainParent, MessageBox::CRITICAL, sLocation, sMsg, eCurrendCmd);
				return;
			}
		}
		else if (errorCode == Vfs::CANNOT_CONNECT || errorCode == Vfs::CANNOT_LOGIN) {
			URI errURI = m_subsystem->currentURI();
			if (errorCode == Vfs::CANNOT_CONNECT)
				MessageBox::msg(m_pMainParent, MessageBox::CRITICAL, tr("Cannot connect.")+"\n"+tr("Please check URL or IP."), errURI.toString());
			else
			if (errorCode == Vfs::CANNOT_LOGIN)
				MessageBox::msg(m_pMainParent, MessageBox::CRITICAL, tr("Cannot login.")+"\n"+tr("Please check credentials."), errURI.toString());

			if (m_URI.toString() != m_prevURI.toString())
				m_URI = m_prevURI;
			else
				m_prevURI = m_URI;
			qDebug() << sFN << "--- errorCode:" << Vfs::toString(eStatus) << "prevURI:" << m_prevURI.toString() << "URI:" << m_URI.toString() << m_subsystem;
			emit signalPathViewChange(errURI, false, false); // signal caught by Panel::slotPathViewPathChange (remove failed path from path view)
			slotPathChanged(m_prevURI.toString());
		}
		else
		if (errorCode == Vfs::CANNOT_EXTRACT) {
			if (pProgressDlg != NULL)
				pProgressDlg->timerStop();
			MessageBox::msg(m_pMainParent, MessageBox::CRITICAL, tr("Cannot extract all input files"), QString(tr("Selected %1 files.").arg(nProcessedFiles)));
			if (! m_subsystem->operationFinished()) {
				if (pProgressDlg != NULL)
					pProgressDlg->timerStart();
			}
			return;
		}
		else
		if (errorCode == Vfs::NOT_SUPPORTED || errorCode == Vfs::INTERNAL_ERROR) {
			emit signalWeighStopAnimation();
			if (pProgressDlg != NULL)
				pProgressDlg->timerStop();
			removeProgressDlg(false);
			QString sMsg = (errorCode == Vfs::NOT_SUPPORTED) ? tr("Not supported operation!") +"\n\n"+ Vfs::toString(eCurrendCmd) : tr("Internal error.")+"\n"+tr("Please check console logs.");
			MessageBox::msg(m_pMainParent, MessageBox::CRITICAL, sMsg, (errorCode == Vfs::NOT_SUPPORTED ? m_URI.toString() : m_prevURI.toString()));
			return;
		}
		else
		if (errorCode == Vfs::NO_FREE_SPACE) {
			if (pProgressDlg != NULL)
				pProgressDlg->timerStop();
			removeProgressDlg(false);
			QString sMsg;
			if (eCurrendCmd == Vfs::OPEN_WITH_CMD)
				sMsg = tr("Cannot open selected file(s)") + "\n\n" + tr("No enough free space in working directory!");
			if (eCurrendCmd == Vfs::COPY || eCurrendCmd == Vfs::MOVE)
				sMsg = tr("Cannot copy selected file(s)") + "\n\n" + tr("No enough free space in target directory!");
			if (! sMsg.isEmpty())
				MessageBox::msg(m_pMainParent, MessageBox::CRITICAL, sMsg);
			return;
		}
		else
		if (errorCode == Vfs::SKIP_FILE) {
			qDebug() << sFN << "-- errorCode: SKIP_FILE. Deselect item:" << m_subsystem->processedFileInfo()->fileName();
			m_selectionModel->setSelected(m_subsystem->processedFileInfo(), false, false); // last false -> use map to find item
			return;
		}
		else
		if (errorCode == Vfs::CMD_NOT_FOUND) {
			if (eCurrendCmd == Vfs::PROG_EXISTS) {
				MessageBox::msg(m_pMainParent, MessageBox::WARNING, m_subsystem->processedFileName());
				m_URI = m_prevURI; // because after failed open archive m_URI has incorrect value
				if (pProgressDlg != NULL)
					pProgressDlg->timerStop();
				removeProgressDlg(false);
				emit signalUpdateListViewStatusBar();
				return;
			}
		}
		else
		if (errorCode == Vfs::CANCELED) {
			qDebug() << sFN << "Error code: Vfs::CANCELED. Do nothing. eCurrendCmd:" << Vfs::toString(eCurrendCmd);
			if (eCurrendCmd == Vfs::CONNECT || m_vfsModel->rowCount() == 0)
				slotPathChanged(m_prevURI.toString());
			return;
		}
		else
		if (errorCode == Vfs::CANNOT_EDIT_LINK) {
			QString sMsg = "\n"+ tr("Cannot edit target of symbolic link.");
			QString sTarget = "<b>"+tr("Target:")+"</b> "+ m_subsystem->processedFileName();
			MessageBox::msg(m_pMainParent, MessageBox::CRITICAL, sMsg, sTarget, eCurrendCmd);
			return;
		}
		else
		if (errorCode == Vfs::CANNOT_COPY_TO_THESAME) {
			if (pProgressDlg != NULL)
				pProgressDlg->timerStop();
			QString sMsg = tr("Skiping file due to this is operation on the same file!");
			MessageBox::msg(m_pMainParent, MessageBox::CRITICAL, sMsg, m_subsystem->processedFileName(), eCurrendCmd);
			if (! m_subsystem->operationFinished()) {
				if (pProgressDlg != NULL)
					pProgressDlg->timerStart();
			}
			return;
		}
		else
		if (errorCode == Vfs::CANNOT_ARCHIVE) {
			if (pProgressDlg != NULL)
				pProgressDlg->timerStop();
			QString sMsg;
			if (m_subsystem->currentCommand() == Vfs::COPY_TO_ARCHIVE || m_subsystem->currentCommand() == Vfs::MOVE_TO_ARCHIVE)
				sMsg = tr("Cannot add file(s) to archive!");
			else
				sMsg = tr("Cannot create archive!");
			MessageBox::msg(m_pMainParent, MessageBox::CRITICAL, sMsg, m_subsystem->processedFileName(), eCurrendCmd);
			if (! m_subsystem->operationFinished()) {
				if (pProgressDlg != NULL)
					pProgressDlg->timerStart();
			}
			else {
				emit signalUpdateListViewStatusBar();
			}
			return;
		}
		if (errorCode == Vfs::CANNOT_COMPRESS || errorCode == Vfs::CANNOT_DECOMPRESS) {
			if (pProgressDlg != NULL)
				pProgressDlg->timerStop();
			QString sMsg;
			if (errorCode == Vfs::CANNOT_COMPRESS)
				sMsg = tr("Cannot compress file!");
			else
				sMsg = tr("Cannot decompress file!");
			MessageBox::msg(m_pMainParent, MessageBox::CRITICAL, sMsg, m_subsystem->processedFileName(), eCurrendCmd);
			if (! m_subsystem->operationFinished()) {
				if (pProgressDlg != NULL)
					pProgressDlg->timerStart();
			}
			return;
		}
		qDebug() << sFN << "--- errorCode: Other error. ErrorCode:" << Vfs::toString(errorCode);
		if (m_URI.toString() != m_prevURI.toString()) {
			qDebug() << sFN << "-- For new subsystem use m_prevURI:" << m_prevURI.toString();
			m_URI = m_prevURI;
			if (errorCode == Vfs::CANNOT_OPEN || errorCode == Vfs::CANCELED || errorCode == Vfs::CANNOT_CONNECT) {
				qDebug() << sFN << "--- errorCode: CANNOT_OPEN or CANCELED or CANNOT_CONNECT -> restore previous uri";
				slotPathChanged(m_URI.toString());
			}
		}
	}
} // slotSubsystemStatusChanged

void SubsystemManager::slotRestorePath()
{
	QString sSenderName = (sender() != NULL) ? sender()->objectName() : "?";
	qDebug() << "SubsystemManager::slotRestorePath. invoked from" << sSenderName;// << "try chanage path to:" << m_URI.toString();
	if (sSenderName != "?") {
		QString sPathViewUri = sSenderName.split(':').at(2);
		if (sPathViewUri != m_URI.toString())
			emit signalPathViewChange(m_URI, false, true); // signal caught by Panel::slotPathViewPathChange
	}
	else
		emit signalPathViewChange(m_URI, false, true); // signal caught by Panel::slotPathViewPathChange

	switchSubsystem(PluginManager::instance(), m_URI.mime()); // making sure, that operation will be invoked on properly subsystem
	if (m_subsystem->currentURI().toString() != m_URI.toString())
		slotPathChangedForCompleter(m_URI.toString()); // update completer for new path
}

void SubsystemManager::slotOpenConnection()
{
	QString sFN = "SubsystemManager::slotOpenConnection.";
	URI uri("ftp://"); // for tests only
	Q_ASSERT(uri.isValid());

	QString sMime = uri.mime();
	qDebug() << sFN << "protocol:" << uri.protocol() << "mime:" << uri.mime();

	PluginManager *pPluginManager = PluginManager::instance();
	if (! pPluginManager->isPluginRegistrered(sMime, "subsystem")) {
		qDebug() << sFN << "Cannot found plugin related to mime:" << sMime;
		return;
	}
	if (pPluginManager->isNewSubsystem(sMime)) {
		qDebug() << sFN << "Get instance for new subsystem related to mime:" << sMime;
		switchSubsystem(pPluginManager, uri.mime());
	}
	m_sPrevSubsysFileName = ""; // current selected will be good  // usable after get out from subsystem (eg. remote, archive)
	m_OpenFile = true;

	disconnect(m_subsystem, &Subsystem::signalShowConnectionDlg,  this, &SubsystemManager::slotShowConnectionDlg);
	connect(m_subsystem, &Subsystem::signalShowConnectionDlg,  this, &SubsystemManager::slotShowConnectionDlg);

	m_subsystem->init(m_pMainParent);
	m_subsystem->open(uri, m_sPanelName);
}

void SubsystemManager::slotCloseConnection()
{
	qDebug() << "SubsystemManager::slotCloseConnection. m_URI:" << m_URI.toString();
	qDebug() << "SubsystemManager::slotCloseConnection. host:"<< m_URI.host() << "mime:"<< m_URI.mime() << "protocol:"<< m_URI.protocol();

	if (! m_URI.host().isEmpty()) { // call for non local subsystem
		if (m_URI.protocol() != "file") {
			switchSubsystem(PluginManager::instance(), m_URI.mime()); // making sure, that operation will be invoked on properly subsystem
			m_subsystem->close();
		}
		emit signalCloseConnection(); // caugh by: Panel::slotCloseConnection
	}
	else {
		URI uri("/");
		m_subsystem->init(m_pMainParent);
		m_subsystem->open(uri, m_sPanelName);
	}
}

void SubsystemManager::slotRefreshCurrentDir()
{
	qDebug() << "SubsystemManager::slotRefreshCurrentDir. Call m_subsystem->open with m_URI:" << m_URI.toString();
	switchSubsystem(PluginManager::instance(), m_URI.mime()); // making sure, that operation will be invoked on properly subsystem
	m_subsystem->open(m_URI, m_sPanelName);  // change directory and emit signalStatusChanged(LISTED) from Subsystem
}

void SubsystemManager::slotRenameFile( FileInfo *pFileInfo, const QString &sInNewName )
{
	if (sInNewName.isEmpty())
		return;

	QString sInOldName = pFileInfo->fileName();
	QString sCurrentPath = "."; sCurrentPath += QDir::separator();
	QString sOldName = (sInOldName.startsWith(sCurrentPath) || sInOldName.startsWith(QDir::separator())) ? sInOldName : m_URI.path()+sInOldName;
	QString sNewName = (sInNewName.startsWith(sCurrentPath) || sInNewName.startsWith(QDir::separator())) ? sInNewName : m_URI.path()+sInNewName;

	qDebug() << "SubsystemManager::slotRenameFile. oldName:" << sOldName << "newName:" << sNewName << "call m_subsystem->rename";
	switchSubsystem(PluginManager::instance(), m_URI.mime()); // making sure, that operation will be invoked on properly subsystem
	m_subsystem->rename(pFileInfo, sNewName, m_sPanelName);
}

void SubsystemManager::slotProgressDialogCancel( int nDlgId ) // called when progress dialog is closed
{
	ProgressDialog *pProgressDlg = m_progressDlgPtrTable.at(nDlgId);
	if (nDlgId < 0 || nDlgId > m_progressDlgPtrTable.size()-1) {
		qDebug() << "SubsystemManager::slotProgressDialogCancel. DlgId out of range";
		return;
	}

	Settings settings;
	if (pProgressDlg->cmdInProgress() == Vfs::COPY) {
		s_bCloseProgressDlgWhenCopyFinished = pProgressDlg->isCloseAfterFinished();
		settings.update("vfs/CloseProgressDlgWhenCopyFinished", s_bCloseProgressDlgWhenCopyFinished);
	}
	else
	if (pProgressDlg->cmdInProgress() == Vfs::MOVE) {
		s_bCloseProgressDlgWhenMoveFinished = pProgressDlg->isCloseAfterFinished();
		settings.update("vfs/CloseProgressDlgWhenMoveFinished", s_bCloseProgressDlgWhenMoveFinished);
	}
	else
	if (pProgressDlg->cmdInProgress() == Vfs::REMOVE) {
		s_bCloseProgressDlgWhenDeleteFinished = pProgressDlg->isCloseAfterFinished();
		settings.update("vfs/CloseProgressDlgWhenDeleteFinished", s_bCloseProgressDlgWhenDeleteFinished);
	}
	else
	if (pProgressDlg->cmdInProgress() == Vfs::EXTRACT) {
		s_bCloseProgressDlgWhenExtractFinished = pProgressDlg->isCloseAfterFinished();
		settings.update("vfs/CloseProgressDlgWhenExtractingFinished", s_bCloseProgressDlgWhenExtractFinished);
	}
	else
	if (pProgressDlg->cmdInProgress() == Vfs::ARCHIVE) {
		s_bCloseProgressDlgWhenArchivFinished = pProgressDlg->isCloseAfterFinished();
		settings.update("vfs/CloseProgressDlgWhenArchivingFinished", s_bCloseProgressDlgWhenArchivFinished);
	}
	cancelVfsOperation();
}

void SubsystemManager::slotMultiProgressDialogCancel()
{
	Settings settings;
	if (m_pMultiProgressDlg->lastSubsysCmd() == Vfs::COPY) {
		s_bCloseProgressDlgWhenCopyFinished = m_pMultiProgressDlg->isCloseAfterFinished();
		settings.update("vfs/CloseProgressDlgWhenCopyFinished", s_bCloseProgressDlgWhenCopyFinished);
	}
	else
	if (m_pMultiProgressDlg->lastSubsysCmd() == Vfs::MOVE) {
		s_bCloseProgressDlgWhenMoveFinished = m_pMultiProgressDlg->isCloseAfterFinished();
		settings.update("vfs/CloseProgressDlgWhenMoveFinished", s_bCloseProgressDlgWhenMoveFinished);
	}
	else
	if (m_pMultiProgressDlg->lastSubsysCmd() == Vfs::REMOVE) {
		s_bCloseProgressDlgWhenDeleteFinished = m_pMultiProgressDlg->isCloseAfterFinished();
		settings.update("vfs/CloseProgressDlgWhenDeleteFinished", s_bCloseProgressDlgWhenDeleteFinished);
	}
	else
	if (m_pMultiProgressDlg->lastSubsysCmd() == Vfs::EXTRACT) {
		s_bCloseProgressDlgWhenExtractFinished = m_pMultiProgressDlg->isCloseAfterFinished();
		settings.update("vfs/CloseProgressDlgWhenExtractingFinished", s_bCloseProgressDlgWhenExtractFinished);
	}
	else
	if (m_pMultiProgressDlg->lastSubsysCmd() == Vfs::ARCHIVE) {
		s_bCloseProgressDlgWhenArchivFinished = m_pMultiProgressDlg->isCloseAfterFinished();
		settings.update("vfs/CloseProgressDlgWhenArchivingFinished", s_bCloseProgressDlgWhenArchivFinished);
	}
	cancelVfsOperation();
}

void SubsystemManager::slotDataTransferProgress( long long nBytesDone, long long nBytesTotal, uint nBytesTransfered, bool bSetTotalBytes )
{
	if (m_FileInfoToMime_map.size() > 0) {
		FileInfo *pFileInfo = m_FileInfoToMime_map.constBegin().key(); // get first item from map
		QString sMime = (m_bArchivingFromLocalSubsys) ? m_sArchiverMime : pFileInfo->mimeInfo();
		switchSubsystem(PluginManager::instance(), sMime); // need to use be in subsystem used by last archive due to pending operation on it
		qDebug() << "SubsystemManager::slotDataTransferProgress" << m_pMultiProgressDlg;
		m_pMultiProgressDlg->slotUpdateFileProgress(m_subsystem->processedFileInfo(), nBytesDone, nBytesTotal);
	}
}

void SubsystemManager::slotProgressDialogOperationInBackground()
{
	ProgressDialog *pProgressDlg = m_progressDlgPtrTable.at(m_progressDlgCounter-1);
	// need to detach progress window from main window (let's be single window)
	pProgressDlg->setModal(false); // made detach
	// (maybe copying to do in thread or copying can do asynchronus - current version), and minimalize when copying finished
	// after finished checks opened directory in panel that operation has been called if there is different then do nothing
}

void SubsystemManager::slotShowFileOverwriteDlg( const FileInfo &sourceFileInfo, FileInfo &targetFileInfo, int &nResult )
{
	qDebug() << "SubsystemManager::slotShowFileOverwriteDlg. for:" << m_sPanelName;
	ProgressDialog *pProgressDlg = m_progressDlgPtrTable.at(m_progressDlgCounter-1);

	if (pProgressDlg != NULL)
		pProgressDlg->timerStop();
// 	else
// 		qDebug() << "SubsystemManager::slotShowFileOverwriteDlg. -- not called pProgressDlg->timerStop(), because there is not pProgressDlg";
	if (nResult != OverwriteFileDialog::Rename) // force to show only rename dlg.
		nResult = OverwriteFileDialog::show(targetFileInfo, sourceFileInfo);

	if (nResult == OverwriteFileDialog::Rename) {
		bool bP1; // dummy
		QString sNewFileName = InputTextDialog::getText(m_pMainParent, InputTextDialog::RENAME_IN_OVR, targetFileInfo.fileName(), bP1);
		if (! sNewFileName.isEmpty()) {
			QString sep = QDir::separator();
			if (sNewFileName.at(0) != '/')
				sNewFileName.insert(0, targetFileInfo.filePath()+sep);
			QString sFileName = sNewFileName.right(sNewFileName.length() - sNewFileName.lastIndexOf(QDir::separator()) - 1);
			QString sPathName = sNewFileName.left(sNewFileName.length() - sFileName.length() - 1);
			targetFileInfo.setData(0, sFileName, true);
			targetFileInfo.setData(2, sPathName, true);
// 			sTargetFileName = sNewFileName;
			if (m_URI.path() == sPathName) {
				;// TODO rename in current dir: need to replace name inside selected list sSourceFileName to sTargetFileName
// 				Q3ValueList<Q3ListViewItem *>::iterator it;
// 				it = m_pListView->selectedItemsList()->find( m_pListView->findName(sSourceFileName) );
// 				(*it)->setText( 0, FileInfoExt::fileName(sTargetFileName) );
			}
		}
		else
			nResult = OverwriteFileDialog::No; // force skipping file
	}

	if (nResult != OverwriteFileDialog::Cancel) {
		if (pProgressDlg != NULL)
			pProgressDlg->timerStop(false);
// 		else
// 			qDebug() << "SubsystemManager::slotShowFileOverwriteDlg. -- not called pProgressDlg->timerStop(false), (noCancel) because there is not pProgressDlg - for:" << m_sPanelName;
	}
}

void SubsystemManager::slotShowConnectionDlg( URI &uri, const QString &sProtocol, const QString &sNoPassUser, bool &bRetStat )
{
	bRetStat = ConnectionDialog::showDlg(m_pMainParent, uri, sProtocol, sNoPassUser);
}

void SubsystemManager::slotCloseView()
{
	qDebug() << "SubsystemManager::slotCloseView.";
	if (m_pFileViewer != NULL) {
		delete m_pFileViewer;  m_pFileViewer = NULL;
	}
}

void SubsystemManager::slotUpdateItemOnLV( const QString &sFileName, bool bExists )
{
	emit updateItemInAllMatchingTabs(sFileName, bExists);
}

void SubsystemManager::updateItemOnLV( const QString &sFileName, bool bExists ) // method invoked from opposite panel
{
	qDebug() << "SubsystemManager::updateItemOnLV. Update/create new item with name:" << sFileName << "where item exists:" << bExists << "for:" << objectName();
	if (bExists) {
		m_vfsModel->updateRowData(0, QFileInfo(sFileName).fileName(), m_subsystem->fileInfo(sFileName));
	}
	else {
		FileInfoList *fil = new FileInfoList;
		fil->append(m_subsystem->fileInfo(sFileName));
		m_vfsModel->insertItems(fil); // update data in model
		delete fil;
	}
}

void SubsystemManager::slotReadFileToViewer( const QString &sHost, FileInfo *pFileInfo, QByteArray &baBuffer, int &nReadBlockSize, int nReadingMode, const QString &sName )
{
	QString sFN = "SubsystemManager::slotReadFileToViewer.";
	PluginManager *pPluginManager = PluginManager::instance();
	if (m_vfsModel->findFileResultMode()) {
		QString sMime = MimeType::getMimeFromPath(sHost);
		if (pPluginManager->isNewSubsystem(sMime))
			switchSubsystem(pPluginManager, sMime);
	}
	if (pFileInfo == NULL) {
		qDebug() << sFN << "Internal ERROR. pFileInfo is NULL";
		return;
	}
	bool bPluginSupport = PluginFactory::instance()->isPluginAvailable(pFileInfo->mimeInfo(), "subsystem");
	if (bPluginSupport) { // this is compressed file, decompress it into configured tmp directory
		if (nReadingMode != Viewer::GET_READABILITY) {
			QString sFileName = pFileInfo->fileName();
			sFileName = m_sTmpDir + sFileName.left(sFileName.lastIndexOf('.'));
			if (QFileInfo(sFileName).exists()) {
				nReadBlockSize = 100240; // file decompressed, start load it to view
			}
			return;
		}
		nReadBlockSize = 1; // to avoid "Cannot read file" from FileViewer::prepareToView
		URI inURI(pFileInfo->absoluteFileName());
		inURI.setHost(pFileInfo->absoluteFileName());
		switchSubsystem(pPluginManager, pFileInfo->mimeInfo());
		qDebug() << sFN << "Call open (decompress file) using" << m_subsystem;
		m_subsystem->open(inURI, m_sPanelName+QString(":Viewer_%1").arg(m_nViewerCounter)); // let subsystem knows that this is view
	}
	else {
		qDebug() << sFN << "Call readFileToViewer in " << m_subsystem;
		m_subsystem->readFileToViewer(sHost, pFileInfo, baBuffer, nReadBlockSize, nReadingMode, sName);
	}
	if (pFileInfo->size() == 0)
		nReadBlockSize = 0;
	qDebug() << sFN << "nReadBlockSize:" << nReadBlockSize;
}

void SubsystemManager::slotGetWeighingResult( qint64 &totalWeight, uint &totalItems )
{
	totalWeight = m_nTotalWeight;
	totalItems  = m_nTotalWeighedItems;
	QString sSenderName = (sender() != NULL) ? sender()->objectName() : "?";
	qDebug() << "\"SubsystemManager::slotGetWeighingResult.\" Invoked by:" << sSenderName << "Returns totalWeight:" << totalWeight << "totalItems:" << totalItems;
	/*	QString sMime = "folder";
	PluginManager *pPluginManager = PluginManager::instance();
	if (pPluginManager->isNewSubsystem(sMime))
		switchSubsystem(pPluginManager, sMime);
	m_subsystem->getWeighingResult(weight, realWeight, files, dirs);*/
}


