/***************************************************************************
 *   Copyright (C) 2007 by Mariusz Borowski <b0mar@nes.pl>                 *
 *   Copyright (C) 2009 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDebug> // need to qDebug()
#include <QKeyEvent>
#include <QHeaderView>
#include <QVBoxLayout>
#include <QApplication> // temp.solution for shortcut Ctrl+Q -> quitApp
#include <QDesktopWidget>
#include <QFileDialog>

#include "settings.h"
#include "mimetype.h"
#include "mimeinfo.h"
#include "messagebox.h"
#include "filelistview.h"
#include "pluginmanager.h"
#include "pluginfactory.h"
#include "inputtextdialog.h"
#include "vfsflatlistmodel.h"
#include "contextmenucmdids.h"
#include "configurationdialog.h"
#include "listviewitemdelegate.h"
#include "keyshortcutsappids.h"


FileListView::FileListView( SubsystemManager *pSubsystemMngr, const QString &sName, QWidget *pParent )
	: QWidget(pParent), m_flmModel(0), m_bInitSelectionFlag(false), m_subsystemMngr(pSubsystemMngr)
{
	qDebug() << "FileListView::FileListView." << sName;
	IconCache *pIconCache = IconCache::instance();

	Q_ASSERT(pSubsystemMngr != nullptr);
	Q_ASSERT(pIconCache != nullptr);

	setAccessibleName(sName);
	setObjectName(sName);
	setParent(pParent);

	m_sTabName = sName;
	m_sTabName.insert(m_sTabName.indexOf('_'), "Tab");
	m_sLastSelectedItemName = "";
	m_lineEditDelegateVisible = false;
	m_contextMenuVisible = false; // helps to select item when context menu visible
	m_bMouseBtnPressed = false;
	m_bShiftPressed = false;
	m_bCtrlPressed = false;

	m_pPropertiesDlg = nullptr;
	m_selectionModel = nullptr;
	m_vfsModel = nullptr;
	m_pKS = nullptr;

	m_pContextMenu = new ContextMenu(this);
	connect(m_pContextMenu, &Bookmarks::signalContextMenuAction, this, &FileListView::slotContexMenuAction);

	m_pBookmarks = new Bookmarks(this);
	connect(m_pBookmarks, &Bookmarks::signalContextMenuAction, this, &FileListView::slotContexMenuAction);
	connect(m_pBookmarks, &Bookmarks::hovered, this, &FileListView::slotMenuEntryHovered);

	m_pDrivesMenu = new DrivesMenu(this);
// 	connect(m_pDrivesMenu, static_cast<void(DrivesMenu::*)(int)>(&DrivesMenu::signalContextMenuAction), this, &FileListView::slotContexMenuAction);
	connect(m_pDrivesMenu, &DrivesMenu::signalPathChanged, this, &FileListView::slotDrivesMenuPathChanged);
	connect(m_pDrivesMenu, &Bookmarks::hovered, this, &FileListView::slotMenuEntryHovered);

	// -- build gui
	QVBoxLayout *pMainLayout = new QVBoxLayout(this);
	pMainLayout->setContentsMargins(0, 0, 0, 0);

	m_tvFileList = new TreeView(this);
	m_tvFileList->setItemsExpandable(false);
	m_tvFileList->setRootIsDecorated(false);
	m_tvFileList->installEventFilter(this);
	m_tvFileList->setAlternatingRowColors(m_bAlternatingRowColors);
	m_tvFileList->setEditTriggers(QAbstractItemView::NoEditTriggers);
	m_tvFileList->setSortingEnabled(true); // sorting is always enabled
// 	m_tvFileList->sortByColumn(0, Qt::AscendingOrder); // already set in setVfsModel using configuratio

	m_FileListViewFindItem = new FileListViewFindItem(this);
	m_FileListViewFindItem->setListView(m_tvFileList);
	m_FileListViewFindItem->setAutoHide(true);
	m_FileListViewFindItem->setVisible(false);
	connect(m_FileListViewFindItem, &FileListViewFindItem::signalKeyPressed, this, &FileListView::slotFindItemKeyPressed);

	m_pFileListViewFilter = new FileListViewFilter(m_tvFileList, this);
	m_pFileListViewFilter->setVisible(false);
	connect(m_pFileListViewFilter, &FileListViewFilter::signalUpdateStatusBar, this, &FileListView::slotUpdateListViewStatusBar);

	pMainLayout->addWidget(m_tvFileList);
	pMainLayout->addWidget(m_FileListViewFindItem);
	pMainLayout->addWidget(m_pFileListViewFilter);

	m_pListViewItemDelegate = new ListViewItemDelegate(m_tvFileList);
	connect(m_pListViewItemDelegate, &ListViewItemDelegate::closeEditor, this, &FileListView::slotCloseEditor);
	connect(m_pListViewItemDelegate, &ListViewItemDelegate::commitData,  this, &FileListView::slotCommitEditor);

	m_pWeighAnimTimer = new QTimer(this);
	connect(m_pWeighAnimTimer, &QTimer::timeout, this, &FileListView::slotWeighStartAnimation);

	loadConfiguration();
// 	updateView(); // called in setVfsModel
}

FileListView::~FileListView()
{
	qDebug() << "FileListView::~FileListView." << accessibleName();
	slotBreakCurrentOperation();
	// -- save sorting parameters before dropping list view obj.
	QString sValue;
	QString sSortOrder = (m_tvFileList->header()->sortIndicatorOrder() == Qt::AscendingOrder) ? "asc" : "desc";
	QString sCurrentSorting = QString("%1 %2").arg(m_tvFileList->header()->sortIndicatorSection()).arg(sSortOrder);
	Settings settings;
	if (settings.value(m_sTabName+"/Sorting").isNull())
		settings.setValue(m_sTabName+"/Sorting", sCurrentSorting);
	else {
		sValue = settings.value(m_sTabName+"/Sorting").toString().toLower();
		if (sValue != sCurrentSorting)
			settings.setValue(m_sTabName+"/Sorting", sCurrentSorting);
	}

	delete m_FileListViewFindItem;
	delete m_pFileListViewFilter;
	delete m_pContextMenu;
	delete m_pBookmarks;
	delete m_tvFileList;  m_tvFileList = nullptr;
	delete m_pPropertiesDlg;

	saveConfiguration(); // TODO remove after settigs dialog will be implemented
}


void FileListView::loadConfiguration()
{
	// -- get system colors
	bool bSystesmAltRowColor = m_tvFileList->alternatingRowColors();
	QPalette sysPalette   = m_tvFileList->palette();
	QColor systemItemCol  = sysPalette.windowText().color();
	QColor systemBgCol    = sysPalette.window().color();
	QColor systemSecBgCol = sysPalette.alternateBase().color();
	QColor systemCursorCol = sysPalette.highlight().color();
	// -- load colors from configuration
	Settings settings;
	m_bSetAttribConfirmation = settings.value("vfs/SetAttribConfirmation", true).toBool();
	m_bWeighBeforeSettingAttrib = settings.value("vfs/WeighBeforeSettingAttrib", true).toBool();
	m_bWeighBeforeCopying   = settings.value("vfs/WeighBeforeCopying",  true).toBool();
	m_bWeighBeforeMoving    = settings.value("vfs/WeighBeforeMoving",   true).toBool();

	QString sScheme = settings.flvSchemeName();
	qDebug() << "FileListView::loadConfiguration. Used SchemeName:" << sScheme;

	n_sItemColor            = settings.value(sScheme+"/ItemColor", systemItemCol).toString(); // "#000000"
	m_sItemColorUnderCursor = settings.value(sScheme+"/ItemColorUnderCursor", systemItemCol).toString(); // "#000000"
//	n_bItemColorFromSystem  = settings.value(sScheme+"/ItemColorFromSystem", true).toBool();
	m_bAlternatingRowColors = settings.value(sScheme+"/AlternatingRowColors", bSystesmAltRowColor).toBool();
	m_sBackgroundColor      = settings.value(sScheme+"/BackgroundColor", systemBgCol).toString(); // "#FFFFFF"
// 	settings.update(sScheme+"/BackgroundColorOfSelectedItem", systemBgCol);
	m_sSecondBackgroundColor = settings.value(sScheme+"/SecondBackgroundColor", systemSecBgCol).toString(); // #FFEBDC"
	m_sCursorColor          = settings.value(sScheme+"/CursorColor", systemCursorCol).toString(); // "#00008B"
	m_sColorOfSelectedItem  = settings.value(sScheme+"/ColorOfSelectedItem", "#FF0000").toString();
	m_sColorOfSelectedItemUnderCurs = settings.value(sScheme+"/ColorOfSelectedItemUnderCursor", "#FF0000").toString();
	m_sHiddenFileColor      = settings.value(sScheme+"/HiddenFileColor",     "#A0A0A0").toString();
	m_sExecutableFileColor  = settings.value(sScheme+"/ExecutableFileColor", "#008000").toString();
	m_sSymlinkColor         = settings.value(sScheme+"/SymlinkColor",        "#808080").toString();
	m_sBrokenSymlinkColor   = settings.value(sScheme+"/BrokenSymlinkColor",  "#800000").toString();

	m_bShowIcons  = settings.value("filelistview/ShowIcons",  true).toBool();
	m_bBoldedDir  = settings.value("filelistview/BoldedDir",  true).toBool();
	m_bBytesRound = settings.value("filelistview/BytesRound", true).toBool();
	m_bSelectDirs = settings.value("filelistview/SelectDirs", true).toBool();
	m_bShowHiddenFiles = settings.value("filelistview/ShowHidden", true).toBool();

	m_bTopOpenWithAppUseAsDefault      = settings.value("filelistview/TopOpenWithAppUseAsDefault", true).toBool();
	m_bViewCurrentIfMoreSelected       = settings.value("filelistview/ViewCurrentIfMoreSelected", false).toBool();
	m_bUseExtViewerIfIntIsNotAvailable = settings.value("filelistview/UseExtViewerIfIntIsNotAvailable", false).toBool();
	m_bAlwaysOpenExtViewIfFewMarked    = settings.value("filelistview/AlwaysOpenExtViewIfFewMarked", false).toBool();
	m_nTheWayOfInvokingSearchBar       = settings.value("filelistview/TheWayOfInvokingSearchBarOnListView", START_SEARCH_WHEN_TYPING).toInt();

	// to be able get colors for default scheme in config dialog
	int nSchemeId = settings.value("filelistviewStyles/SchemeCurrentId", 0).toInt();
	if (nSchemeId == 0) { // DefaultScheme
		settings.update(sScheme+"/ItemColor", systemItemCol.name()); // to be able to read in Settings dialog
		settings.update(sScheme+"/ItemColorUnderCursor", systemItemCol.name());
		settings.update(sScheme+"/AlternatingRowColors", bSystesmAltRowColor);
		settings.update(sScheme+"/BackgroundColor", systemBgCol.name());
		settings.update(sScheme+"/SecondBackgroundColor", systemSecBgCol.name());
		settings.update(sScheme+"/CursorColor", systemCursorCol.name());
	}

	m_bSyncColWidthInBothPanels = settings.value("filelistview/SynchronizeColumns", true).toBool();

	// -- load font settings
	QFont defaultFont = QApplication::font();
	QString sDefaultFont = defaultFont.family()+","+QString("%1").arg(defaultFont.pointSize())+","+defaultFont.styleName();
	m_sTreeViewFont = settings.value("filelistview/Font", "").toString(); // Noto Sans Adlam
	if (m_sTreeViewFont.isEmpty()) // use default in system
		m_sTreeViewFont = sDefaultFont; // DejaVu Sans
}

void FileListView::saveConfiguration()
{
	Settings settings;
	// TODO: Move this to MainWindow, because the most likely is called for each opened panel
	settings.update("vfs/SetAttribConfirmation", m_bSetAttribConfirmation);

	settings.update("filelistview/ShowIcons", m_bShowIcons);
	settings.update("filelistview/BoldedDir", m_bBoldedDir);
	settings.update("filelistview/BytesRound", m_bBytesRound);
	settings.update("filelistview/SelectDirs", m_bSelectDirs);
	settings.update("filelistview/ShowHidden", m_bShowHiddenFiles);

	settings.update("filelistview/TopOpenWithAppUseAsDefault", m_bTopOpenWithAppUseAsDefault);
	settings.update("filelistview/ViewCurrentIfMoreSelected", m_bViewCurrentIfMoreSelected);
	settings.update("filelistview/UseExtViewerIfIntIsNotAvailable", m_bUseExtViewerIfIntIsNotAvailable);
	settings.update("filelistview/AlwaysOpenExtViewIfFewMarked", m_bAlwaysOpenExtViewIfFewMarked);
// 	settings.update("filelistview/TheWayOfInvokingSearchBarOnListView", m_nTheWayOfInvokingSearchBar);
// 	settings.update("filelistview/HiddenFileColor", m_sHiddenFileColor);
// 	settings.update("filelistview/ExecutableFileColor", m_sExecutableFileColor);
// 	settings.update("filelistview/SymlinkColor", m_sSymlinkColor);
// 	settings.update("filelistview/BrokenSymlinkColor", m_sBrokenSymlinkColor);
}

void FileListView::updateView() // called by slotApplyConfiguration
{
	qDebug() << "FileListView::updateView. Panel:" << objectName();
	QPalette palette; // = m_tvFileList->palette();
 	//palette.setColor(QPalette::WindowText, QColor(n_sItemColor)); // moved to SelectionModel
 	//palette.setColor(QPalette::Text, QColor(m_sItemColorUnderCursor)); // moved to ListViewItemDelegate
	palette.setColor(QPalette::Window, QColor(m_sBackgroundColor));
	palette.setColor(QPalette::AlternateBase, QColor(m_sSecondBackgroundColor));
	m_tvFileList->setAlternatingRowColors(m_bAlternatingRowColors);
	m_tvFileList->setPalette(palette);

	QString sFontFamily = m_sTreeViewFont.section(',', 0,0);
	QString sFontSize   = m_sTreeViewFont.section(',', 1,1).simplified();
	QString sFontStyle  = m_sTreeViewFont.section(',', 2,2).simplified().toLower();
	bool bOk;
	uint nFontSize = sFontSize.toInt(&bOk);
	if (! bOk)
		nFontSize = 10; // default size
	int nFontWeight = (sFontStyle == "bold") ? QFont::Bold : QFont::Normal;
	m_tvFileList->setFont(QFont(sFontFamily, nFontSize, nFontWeight, (sFontStyle == "italic")));

	m_vfsModel->seBoldDirs(m_bBoldedDir);
	m_vfsModel->setShowIcons(m_bShowIcons);
	m_vfsModel->setBytesRound(m_bBytesRound);
	m_vfsModel->setSelectDirs(m_bSelectDirs);
	m_flpModel->setShowHiddenFiles(m_bShowHiddenFiles);

	m_selectionModel->setColorOfCursor(QColor(m_sCursorColor));
	m_selectionModel->setColorOfCommonItem(QColor(n_sItemColor));
	m_selectionModel->setColorOfSelectedItem(QColor(m_sColorOfSelectedItem));
 	m_selectionModel->setColorOfSelectedItemUnderCursor(QColor(m_sColorOfSelectedItemUnderCurs));
	m_selectionModel->setColorOfItemUnderCursor(QColor(m_sItemColorUnderCursor));

	m_selectionModel->setColorOfHiddenFile(QColor(m_sHiddenFileColor));
	m_selectionModel->setColorOfExecutableFile(QColor(m_sExecutableFileColor));
	m_selectionModel->setColorOfSymlink(QColor(m_sSymlinkColor));
	m_selectionModel->setColorOfBrokenSymlink(QColor(m_sBrokenSymlinkColor));
}

void FileListView::setColumnWidth( int nLogicalIndex, int nNewSize )
{
	m_vfsModel->beginResetVfsModel();
	m_tvFileList->header()->resizeSection(m_tvFileList->header()->logicalIndex(nLogicalIndex), nNewSize);
	m_vfsModel->endResetVfsModel();
}


void FileListView::setVfsModel( QAbstractItemModel *pModel )
{
	Q_ASSERT(pModel != 0);

	m_vfsModel = static_cast<const VfsFlatListProxyModel *>(pModel)->sourceModel();
	m_subsystemMngr->setVfsModel(m_vfsModel);
	//connect(m_tvFileList,  static_cast<void(TreeView::*)(const QModelIndex &)>(&TreeView::activated), m_subsystemMngr, &SubsystemManager::slotActivated);
	connect(m_tvFileList, static_cast<void(QTreeView::*)(const QModelIndex &)>(&QTreeView::doubleClicked), m_subsystemMngr, &SubsystemManager::slotActivated);
	connect(m_subsystemMngr, &SubsystemManager::signalUpdateFocusSelection, this, &FileListView::slotUpdateFocusSelection); // don't need to make conversion if function is not overloaded
	connect(m_subsystemMngr, &SubsystemManager::signalWeighStopAnimation, this, &FileListView::slotWeighStopAnimation);
	connect(m_subsystemMngr, &SubsystemManager::singalUpdateContextMenu, this, &FileListView::slotUpdateContextMenu);
 	//connect(m_subsystemMngr, &SubsystemManager::signalResortItems, this, &FileListView::slotResortView); // signal doesn't exist yet

	m_flmModel = static_cast <VfsFlatListModel *> (pModel);

	m_tvFileList->setModel(m_flmModel);
	m_selectionModel = m_vfsModel->selectionModel();  // here m_vfsModel points out on VfsFlatListModel

	m_flpModel = static_cast <VfsFlatListProxyModel *> (pModel);
	m_pFileListViewFilter->setVfsProxyModel(m_flpModel);
	connect(m_pFileListViewFilter, &FileListViewFilter::signalSetRegExpParametersNameCol, m_flpModel, &VfsFlatListProxyModel::slotSetRegExpParametersNameCol);

	updateView(); // by use configuration

	m_pFileListViewFilter->changeRegExpParamsNameColInModel(); // needed for update regExp obj.parameters in proxyModel
	slotAdjustColumnSize();
	connect(m_tvFileList->header(), &QHeaderView::sectionResized, this, &FileListView::slotSectionResized); // thanks that connect is done one time

	// -- set sorting
	Settings settings;
	QString sSorting = settings.value(m_sTabName+"/Sorting", "0 asc").toString().toLower(); // default sorts by name with ascending order
	if (sSorting.isEmpty() || ! sSorting.contains(' '))
		return;

	int nSortColumn = sSorting.split(' ').at(0).toInt();
	QString sSortOrder = sSorting.split(' ').at(1);
	if (sSortOrder != "asc" && sSortOrder != "desc")
		sSortOrder = "asc";
	if (nSortColumn > m_tvFileList->header()->count()-1 || nSortColumn < 0)
		nSortColumn = 0;

	Qt::SortOrder sortOrder = (sSortOrder == "asc") ? Qt::AscendingOrder : Qt::DescendingOrder;
	m_tvFileList->sortByColumn(nSortColumn, sortOrder);
// 	qDebug() << "FileListView::setVfsModel. sorting in" << m_sTabName << ":" << nSortColumn << sSortOrder << "col.count:" << m_vfsModel->columnCount();

	QItemSelectionModel *selectionModel= m_tvFileList->selectionModel();
	connect(selectionModel, &QItemSelectionModel::selectionChanged, this, &FileListView::slotSelectionChanged);

// 	if (m_pListViewItemDelegate == nullptr) {
// 		m_pListViewItemDelegate = new ListViewItemDelegate(this);
// 		m_tvFileList->setItemDelegate(m_pListViewItemDelegate);
// 	}
	m_pListViewItemDelegate->setParentObj(this);
	m_tvFileList->setItemDelegate(m_pListViewItemDelegate);

	// -- show/hide columns, and getting width of all columns
	QString sColumns, sColumn;
	// TODO. Needs to use actual columns width for list view, they needs firstly initialize using config file
	if (m_vfsModel->findFileResultMode())
		sColumns = settings.value("filelistview/FL_Columns", "0-170,1-70,2-107,3-70,4-55,5-55,6-55,7-55").toString();
	else
		sColumns = settings.value("filelistview/CL_Columns", "0-170,1-70,2-107,3-70,4-55,5-55,6-55").toString();
	QStringList slColumn, slColumns = sColumns.split(',');
	for (int i=0; i<slColumns.count(); i++) {
		sColumn = slColumns[i];
		slColumn = sColumn.split('-');
		showHideSection(i, (slColumn[1] != "0"));
		if (m_vfsModel->findFileResultMode())
			m_ntFindFilesCol[i] = m_tvFileList->header()->sectionSize(i);
		else
			m_ntListViewCol[i] = m_tvFileList->header()->sectionSize(i);;
	}
}

void FileListView::slotAdjustColumnSize()
{
	QHeaderView *pHeader = m_tvFileList->header();
	pHeader->setSectionResizeMode(QHeaderView::Interactive);
	pHeader->resizeSection(pHeader->logicalIndex(0), m_tvFileList->window()->width()/4);
 	pHeader->resizeSection(pHeader->logicalIndex(1), m_tvFileList->window()->width()/16);
 	pHeader->resizeSection(pHeader->logicalIndex(2), m_tvFileList->window()->width()/9);

	if (m_vfsModel->findFileResultMode())
		pHeader->resizeSection(pHeader->logicalIndex(2), window()->width()/2);

	if (m_vfsModel->findFileResultMode()) {
		m_pFileListViewFilter->setColumnsId(1, 2, 3, 4, 5, 6);  // TODO: The order of columns need to read from config file
		m_pFileListViewFilter->setColumnNameStrLst(true);
	}
}

QModelIndex FileListView::currentModelIndex() const
{
	QModelIndex currentMI = m_tvFileList->model()->index(m_tvFileList->currentIndex().row(), 0);
	const VfsFlatListProxyModel *pProxyModel = static_cast <const VfsFlatListProxyModel *>(currentMI.model());
	return (pProxyModel == nullptr) ? currentMI : pProxyModel->mapToSource(currentMI);
// 	return m_tvFileList->model()->index(m_tvFileList->currentIndex().row(), 0);
}
/*
QModelIndex currentProxyModelIndex() const;
QModelIndex FileListView::currentProxyModelIndex() const
{
	return (static_cast <const VfsFlatListProxyModel *>(currentModelIndex().model())->mapToSource(currentModelIndex()));
}
*/

QString FileListView::currentFileName() const
{
	return currentModelIndex().data().toString();
}

QString FileListView::currentFileExtention() const
{
	QString fileName = currentFileName();
	QStringList lst = fileName.split(".");
	QString fileExtension;

	if (lst.size() > 1)
		fileExtension = lst.at(lst.size()-1); // old: lst.at(1);

	return fileExtension;
}

void FileListView::showContextMenu( bool bOnCursorPos )
{
	slotBreakCurrentOperation();
	// below commented due to QLineEdit has custom support of ContextMenu
// 	m_tvFileList->closePersistentEditor(currentModelIndex()); // if more inbox edit handled then need to close all
	m_contextMenuVisible = true;

	QPoint globalPos;
	QRect vr = m_tvFileList->visualRect(currentModelIndex());

	if (bOnCursorPos)
		globalPos = QPoint(QCursor::pos().x() - 2, QCursor::pos().y() + 2);
	else // for Key_Menu
		globalPos = mapToGlobal(QPoint( m_tvFileList->header()->sectionSize(0), vr.y() + vr.height() + m_tvFileList->header()->height() + 1 ));

	int y = globalPos.y(), menuHeight = m_pContextMenu->sizeHint().height();
	if (y + menuHeight > QApplication::desktop()->height()) {
		globalPos.setY( y - menuHeight );
		if (! bOnCursorPos) // correction for Key_Menu
			globalPos.setY(y - vr.height() - 1);
	}
	QString sPathInSecondPanel;
	emit signalGetPathFromOppositeActiveTab(sPathInSecondPanel);
	URI uri2P(sPathInSecondPanel);
	bool bLocalPathInOppositePanel = (uri2P.host().isEmpty()); // remote or archive

	// --- shows/hides item menu depends to clicked item
	FileInfo fi = m_vfsModel->fileInfo(currentModelIndex());
	bool bMkSymlinkVisible = (fi.fileName() != ".." && bLocalPathInOppositePanel);
	m_pContextMenu->setVisibleAction(CMD_EditSymlink, fi.isSymLink());  // toggler depends on current context
	m_pContextMenu->setVisibleAction(CMD_Makelink, bMkSymlinkVisible);

	bool bNotDblDots = (fi.fileName() != "..");
	m_pContextMenu->setVisibleAction(CMD_CopyTo, bNotDblDots);
	m_pContextMenu->setVisibleAction(CMD_MoveTo, bNotDblDots);
	m_pContextMenu->setVisibleAction(CMD_Remove, bNotDblDots);
	m_pContextMenu->setVisibleAction(CMD_Rename, bNotDblDots);
	m_pContextMenu->setVisibleAction(CMD_ShowProperties, bNotDblDots);

	// --- detects plugin's support for current item
	QString sMime = fi.mimeInfo();
	PluginFactory *pPlugiFactory = PluginFactory::instance();
	bool bPluginSupport = pPlugiFactory->isPluginAvailable(sMime, "subsystem");
	//qDebug() << "-- input mime" << sMime << "archiveSupported" << bPluginSupport;
	bool isSupportedArchive = (bPluginSupport && sMime != "folder" && sMime != "folder/symlink" && ! sMime.startsWith("remote/") && sMime != "symlink/broken");
	bool isSupportedArchiveForSelected = false;
	if (m_selectionModel->size() > 0)
		isSupportedArchiveForSelected = m_subsystemMngr->subsystemAvailableForSelected(m_selectionModel->selectionMap());
	bool isRemoteSubsystem = (m_subsystemMngr->uri().protocol() != "file"); // or sMime.startsWith("remote/")
	bool isDir = (fi.isDir() || (fi.isSymLink() && fi.isDir()));
	bool bInsideRemoteLoc = (! fi.filePath().startsWith(QDir::rootPath()));
	bool bInsideArchive = (bInsideRemoteLoc && ! m_subsystemMngr->uri().mime().startsWith("remote/") && ! m_subsystemMngr->uri().host().isEmpty());
	bool bDoubleDot = (fi.fileName() == "..");

	m_pContextMenu->setVisibleAction(CMD_ExtractHere, (isSupportedArchive || isSupportedArchiveForSelected) && ! bInsideRemoteLoc && ! bDoubleDot);
	m_pContextMenu->setVisibleAction(CMD_ExtractTo, (isSupportedArchive || isSupportedArchiveForSelected) && ! bInsideArchive && bLocalPathInOppositePanel && ! bDoubleDot);
	m_pContextMenu->setVisibleAction(CMD_Archive_SEP, (m_pContextMenu->isActionVisible(CMD_ExtractHere) || m_pContextMenu->isActionVisible(CMD_ExtractTo)));
	m_pContextMenu->setVisibleAction(CMD_OpenInOppositeActiveTab, isDir || isSupportedArchive);
	m_pContextMenu->setVisibleAction(CMD_OpenInNewTab, isDir || isSupportedArchive);
	m_pContextMenu->setVisibleAction(CMD_Edit, ! isDir && ! isSupportedArchive && ! isRemoteSubsystem && ! bInsideArchive);
	m_pContextMenu->setVisibleAction(CMD_AddToArchive, bNotDblDots && ! bInsideRemoteLoc && ! bInsideArchive && bLocalPathInOppositePanel);
	m_pContextMenu->setVisibleAction(CMD_MoveToArchive, bNotDblDots && ! bInsideRemoteLoc && ! bInsideArchive && bLocalPathInOppositePanel);

	// -- update OpenWith menu (only when one item is selected)
	if (m_selectionModel->size() == 1) {
		sMime = m_vfsModel->fileInfo(currentModelIndex()).mimeInfo();
		Settings settings;
		QString sValue = settings.value("appAssociationWithMime/"+sMime).toString();
		QStringList slAppsList = sValue.split('|');
		foreach(QString sAppName, slAppsList)
			m_pContextMenu->insertAction(CMD_OpenWithSep, sAppName, m_pContextMenu->openWithNextActionId(), CMD_OpenWith_MENU, MAX_OPENWITH_BY_MIME); // before CMD_OpenWithSep will be inserted action
	}

	// -- visibility of "Filter With Extension" and "Remove filter"
	bool isFile = ((! isDir && !fi.isSymLink()) || (bNotDblDots && !isDir));
	bool hasExt = ! QFileInfo(fi.fileName()).suffix().isEmpty() && ! fi.isHidden() && bNotDblDots;
	m_sFilterWithExtension = QFileInfo(fi.fileName()).suffix();
	m_pContextMenu->setVisibleAction(CMD_FilterWithExtension, (isFile && hasExt));
	m_pContextMenu->setVisibleAction(CMD_RemoveFilter, isListFiltered());
	if (isListFiltered())
		m_pContextMenu->setVisibleAction(CMD_FilterWithExtension, false);

	// --- disable not supported actions
	if (m_vfsModel->findFileResultMode()) {
		m_pContextMenu->setVisibleAction(CMD_CreateNew_MENU, false);
		m_pContextMenu->setVisibleAction(CMD_Select_MENU, false);
	}

	if (sPathInSecondPanel.startsWith("://")) { // findFileResultMode in second panel
		m_pContextMenu->setVisibleAction(CMD_CopyTo, false);
		m_pContextMenu->setVisibleAction(CMD_MoveTo, false);
	}
	m_pContextMenu->init(m_pKS);
	// -- update application list for Open with -> Select
	m_pContextMenu->updateOpenWithSelect(sMime);
	m_pContextMenu->showMenu(globalPos);

	m_contextMenuVisible = false;
}

void FileListView::showBookmarksMenu()
{
	slotBreakCurrentOperation();

	QPoint globalPos = mapToGlobal(QPoint( 1, m_tvFileList->header()->height() + 1 ));
	int y = globalPos.y(), menuHeight = m_pBookmarks->sizeHint().height();
	if (y + menuHeight > QApplication::desktop()->height()) {
		globalPos.setY(y - menuHeight);
	}
	m_pBookmarks->init(m_pKS);
	m_pBookmarks->showMenu(globalPos);
	// - restore status bar
	slotUpdateListViewStatusBar();
}

void FileListView::showDrivesMenu()
{
	slotBreakCurrentOperation();

	QPoint globalPos = mapToGlobal(QPoint( 1, m_tvFileList->header()->height() + 1 ));
	int y = globalPos.y(), menuHeight = m_pDrivesMenu->sizeHint().height();
	if (y + menuHeight > QApplication::desktop()->height()) {
		globalPos.setY(y - menuHeight);
	}
	m_pDrivesMenu->exec(globalPos);
	// - restore status bar
	slotUpdateListViewStatusBar();
}

void FileListView::createAnArchive( bool bMove )
{
	FileInfo fi = m_vfsModel->fileInfo(currentModelIndex());
	bool bInsideRemoteLoc = (! fi.filePath().startsWith(QDir::rootPath()));
	bool bInsideArchive = (bInsideRemoteLoc && ! m_subsystemMngr->uri().mime().startsWith("remote/") && ! m_subsystemMngr->uri().host().isEmpty());

	if (fi.fileName() != ".." && ! bInsideRemoteLoc && ! bInsideArchive) {
		slotBreakCurrentOperation();
		m_subsystemMngr->createAnArchive(Vfs::ARCHIVE_TO, bMove, m_selectionModel->selectionMap(), this); // ARCHIVE_HERE is not yet supported
	}
}

// ---------------------- E V E N T s -----------------------

bool FileListView::eventFilter( QObject *obj, QEvent *event )
{
	QEvent::Type eventType = event->type();
//	qDebug() << "eventType" << eventType;

	if (obj == m_tvFileList) {
		if (eventType == QEvent::FocusOut) {
			m_bHadFocus = true;
			if (! m_contextMenuVisible)
				m_tvFileList->clearSelection(); // remove focus
			m_bInitSelectionFlag = false;
			if (! currentFileName().isEmpty()) {
				m_sLastSelectedItemName = currentFileName();
				m_lastSelectedModelIndex = currentModelIndex();
			}
			if (currentModelIndex().isValid())
				m_currentIndex = currentModelIndex();
		}
		else
		if (eventType == QEvent::FocusIn) {
			m_bHadFocus = false;
			if (! m_bInitSelectionFlag) {
// 				if (! currentFileName().isEmpty()) // currentFileName() is empty when returns back from modal dialog
					slotUpdateFocusSelection(false, currentFileName());
			}
		}
		// void keyPressEvent( QKeyEvent *pKeyEvent ) doesn't work correct -> method: keyboardSearch doesn't work after Backspace pressed
		else if (eventType == QEvent::KeyPress) {
			m_bMouseBtnPressed = false;
			if (m_lineEditDelegateVisible)
				return QWidget::eventFilter(obj, event);
			if (keyPressed((QKeyEvent *)event))
				return true;
		}
		else if (eventType == QEvent::KeyRelease) {
			m_bMouseBtnPressed = false;
			m_bShiftPressed = false;
			m_bCtrlPressed = false;
		}
		else if (eventType == QEvent::MouseButtonPress) {
			// use QEvent::ContextMenu made overloading handle of Key_Menu and pressing it calls support ContextMenu event
			m_bMouseBtnPressed = true;
			QMouseEvent *me = (QMouseEvent *)event;
			if (me->button() == Qt::RightButton)
				showContextMenu(true);
			else
			if (me->button() == Qt::LeftButton) {
				if (m_bShiftPressed) {
					QString sSelectedName = currentFileName();
					m_selectionModel->selectRange(m_previousModelIndex, m_tvFileList->currentIndex(), m_tvFileList);
					slotUpdateFocusSelection(false, sSelectedName);
					return true;
				}
				else if (m_bCtrlPressed) { // inverse selection
					m_selectionModel->inverseIndexSelection(m_tvFileList->currentIndex());
					return true;
				}
			}
		}
		return false;
	}

	return QWidget::eventFilter(obj, event);
}

bool FileListView::keyPressed( QKeyEvent *pKeyEvent )
{
	int key    = pKeyEvent->key();
	int keyMod = pKeyEvent->modifiers();
	QKeySequence ks = m_pKS->ke2ks(pKeyEvent);

// 	qDebug() << "key:" << key;
	m_bShiftPressed = (keyMod == Qt::SHIFT && key == Qt::Key_Shift);
	if (m_bShiftPressed)
		m_previousModelIndex = m_tvFileList->currentIndex();

    m_bCtrlPressed = (keyMod == Qt::CTRL && key == Qt::Key_Control);

// 	if (key == Qt::Key_Tab) {
// 		qDebug() << "FileListView::keyPressed. Key_Tab";
// 		emit signalUpdatePathInTermianl();
// 	}
// 	else
	if (key == Qt::Key_Backtab) {
		if (keyMod == (Qt::CTRL|Qt::SHIFT))
			setActive();
// 		else // MoveCursorToPathView_APP
	}
	if (ks.matches(m_pKS->keyCode(MoveCursorToPathView_APP))) {
		slotBreakCurrentOperation();
		QTimer::singleShot(0, parent(), SLOT(slotActivePathView()));
		return false;
	}

	else if (ks.matches(m_pKS->keyCode(InvertSelection_APP))) {
		slotBreakCurrentOperation();
		QString sSelectedName = currentFileName();
		m_currentItemBeforeMark = m_tvFileList->currentIndex();
		m_selectionModel->inverseSelection();
		slotUpdateFocusSelection(false, sSelectedName);
	}
	else if (key == Qt::Key_Plus) {
		if (m_vfsModel->findFileResultMode())
			return false;
		m_currentItemBeforeMark = m_tvFileList->currentIndex();
		selectByPattern(true);
	}
	else if (key == Qt::Key_Minus) {
		if (m_vfsModel->findFileResultMode())
			return false;
		m_currentItemBeforeMark = m_tvFileList->currentIndex();
		selectByPattern(false);
	}
	else if (key == Qt::Key_Insert) {
		if (m_vfsModel->findFileResultMode())
			return false;
	}

	if (keyMod == Qt::NoModifier || keyMod == Qt::SHIFT) {
		if (key == Qt::Key_Tab) {
// 			qDebug() << "FileListView::keyPressed. Key_Tab";
			emit signalUpdatePathInTermianl();
	//		return false;
		}
		if (m_nTheWayOfInvokingSearchBar == START_SEARCH_WHEN_TYPING) {
			QString sKey = pKeyEvent->text();
			if (! sKey.isEmpty() && sKey.length() == 1 && key != Qt::Key_Space) {
				QChar typedChar(sKey.at(0));
				if (typedChar.isPrint()) {
					slotBreakCurrentOperation();
					m_tvFileList->closePersistentEditor(currentModelIndex());
					m_FileListViewFindItem->init(typedChar);
					m_FileListViewFindItem->setVisible(true);
				}
			}
		}
	}

// 	if (keyMod == Qt::CTRL) {
		if (ks.matches(m_pKS->keyCode(QuitApplication_APP))) {
			slotBreakCurrentOperation();
			//qApp->closeAllWindows(); // supported globally from main menu
			return false;
		}
		else if (ks.matches(m_pKS->keyCode(QuickItemSearch_APP))) {
			slotBreakCurrentOperation();
			m_tvFileList->closePersistentEditor(currentModelIndex());
			m_FileListViewFindItem->init();
			m_FileListViewFindItem->setVisible(true);
		}
		else if (ks.matches(m_pKS->keyCode(QuickItemFiltering_APP))) {
			slotBreakCurrentOperation();
			m_tvFileList->closePersistentEditor(currentModelIndex());
			m_pFileListViewFilter->init();
			m_pFileListViewFilter->setVisible(true);
		}
		else if (ks.matches(m_pKS->keyCode(NewTabInCurrentPanel_APP))) {
			emit signalAddTab();
		}
		else if (ks.matches(m_pKS->keyCode(RefreshDirectory_APP))) {
			if (m_vfsModel->findFileResultMode())
				return false;
			m_sLastSelectedItemName = currentFileName();
			m_lastSelectedModelIndex = currentModelIndex();
			slotBreakCurrentOperation();
			m_subsystemMngr->setPrevDirName(currentFileName());
			m_subsystemMngr->slotRefreshCurrentDir();
		}
		else if (ks.matches(m_pKS->keyCode(SelectAllItems_APP))) {
			selectAll(true);
		}
		else if (ks.matches(m_pKS->keyCode(SelectFilesWithTheSameExtention_APP))) {
			selectWithTheSameExtention(true);
		}
		else if (ks.matches(m_pKS->keyCode(OpenCurrentFileOrDirInLeftPanel_APP)) || ks.matches(m_pKS->keyCode(OpenCurrentFileOrDirInRightPanel_APP))) {
// 		else if (key == Qt::Key_Right || key == Qt::Key_Left) {
			slotBreakCurrentOperation();
			m_subsystemMngr->openCurrentItemInTab(currentModelIndex(), Vfs::OPPOSITE_ACTIVE_TAB);
			return true;
		}
		else if (ks.matches(m_pKS->keyCode(CloseCurrentTab_APP))) {
			slotBreakCurrentOperation();
			//QTimer::singleShot(0, this, SIGNAL(signalCloseTab()));
			QTimer::singleShot(0, this, &FileListView::signalCloseTab);
// 			emit signalCloseTab(); // made CRASH, because function try to return to non existnig object
		}
		else if (ks.matches(m_pKS->keyCode(DuplicateCurrentTab_APP))) {
 			emit signalDuplicateTab();
 		}
		else if (ks.matches(m_pKS->keyCode(QuickGoOutFromCurrentSubSystem_APP))) {
			return quickGetOutOfSubsystem();
		}
		else if (ks.matches(m_pKS->keyCode(ShowBookmarks_APP))) {
			showBookmarksMenu();
		}
		else if (ks.matches(m_pKS->keyCode(MakeALink_APP))) {
			slotBreakCurrentOperation();
			m_subsystemMngr->makeLink(this);
		}
		else if (ks.matches(m_pKS->keyCode(EditCurrentLink_APP))) {
			slotBreakCurrentOperation();
			m_subsystemMngr->editSymLink(currentModelIndex(), this);
		}
		else if (ks.matches(m_pKS->keyCode(GoToUserHomeDirectory_APP))) {
			goToUserHomeDirectory();
		}
		else if (ks.matches(m_pKS->keyCode(GoToRootDirectory_APP))) {
			goToRootDirectory();
		}
// 		else if (ks.matches(m_pKS->keyCode(OneDirForward_APP))) { // handled globally
// 			oneDirBackward();
// 		}
// 		else if (ks.matches(m_pKS->keyCode(OneDirBackward_APP))) { // handled globally
// 			oneDirForward();
// 		}
// 	} // keyMod == Qt::CTRL
// 	else
	if (keyMod == (Qt::CTRL|Qt::SHIFT)) {
	//if (keyMod == (Qt::ControlModifier|Qt::ShiftModifier)) {
		if      (key == Qt::Key_N) {
			slotBreakCurrentOperation();
			m_subsystemMngr->slotCloseConnection();
		}
		if (ks.matches(m_pKS->keyCode(DeselectAllFiles_APP))) {
			selectAll(false);
		}
		if (ks.matches(m_pKS->keyCode(DeselectFilesWithTheSameExtention_APP))) {
			selectWithTheSameExtention(false);
		}
	}
	else
	if (keyMod == (Qt::CTRL|Qt::ALT)) {
		if (ks.matches(m_pKS->keyCode(ShowStoragesList_APP))) {
			showDrivesMenu();
		}
		else if (ks.matches(m_pKS->keyCode(ShowConnectionManager_APP))) {
			openNewRemoteConnection(RemoteConnection::FTP);
		}
	}
	if (keyMod == Qt::AltModifier) {
		bool bCreateArch;
		if (ks.matches(m_pKS->keyCode(ShowHistory_APP))) {
// 			breakLastOperation();
			if (! m_vfsModel->findFileResultMode())
				QTimer::singleShot(0, parent(), SLOT(slotPopUpPathView()));
		}
		else if (key == Qt::Key_Up) {
// 			breakLastOperation();
			if (! m_vfsModel->findFileResultMode())
				QTimer::singleShot(0, parent(), SLOT(slotActivePathView()));
		}
		else if ((bCreateArch=ks.matches(m_pKS->keyCode(CreateNewArchive_APP))) || ks.matches(m_pKS->keyCode(MoveFilesToArchive_APP))) {
			createAnArchive(bCreateArch);
		}
		if (key > Qt::Key_0 && key <= Qt::Key_9) {
			int nTabId = key - Qt::Key_0;
			emit signalSwitchToTab(nTabId);
		}
		else if (key == Qt::Key_Return) {
			showPropertiesDialog();
		}
	}
	else
	if (keyMod == Qt::ShiftModifier) {
		if (ks.matches(m_pKS->keyCode(ShowContextMenu_APP))) {
			showContextMenu(false);
		}
		else if (ks.matches(m_pKS->keyCode(MakeAnEmptyFile_APP))) {
			slotBreakCurrentOperation();
			m_subsystemMngr->createNewEmptyFile(currentModelIndex(), true, this);
		}
		else if (ks.matches(m_pKS->keyCode(QuickRename_APP)) || key == Qt::Key_F6) {
			slotInitRename();
		}
	}
	else
	if (keyMod == Qt::NoModifier) {
		bool bCopy;
		if      (key == Qt::Key_Backspace) { // go to dir up
			if (m_vfsModel->findFileResultMode())
				return false;
			slotBreakCurrentOperation();
			m_subsystemMngr->slotPathChanged("..");
		}
		else if (ks.matches(m_pKS->keyCode(SelectUnselectItem_APP))) {
			slotBreakCurrentOperation();
			// weighed dir handles
			QModelIndex srcIndex = m_tvFileList->currentIndex(); // model points out on VfsFlatListProxyModel
			if (m_selectionModel->selected(srcIndex)) {
				FileInfo *pFI = m_vfsModel->fileInfoPtr(srcIndex);
				if (! pFI->isSymLink() && pFI->isDir()) {
					pFI->setDirLabel();
					m_vfsModel->updateRowData(Vfs::COL_NAME, pFI->fileName(), pFI);
				}
			}
			// inverses selection, and moves cursor down
			m_selectionModel->inverseIndexSelection(srcIndex);
			int nextRow = srcIndex.row() + 1;
			if (nextRow < m_vfsModel->rowCount())
				m_tvFileList->setCurrentIndex(srcIndex.sibling(nextRow, 0));
		}
		else if (key == Qt::Key_Enter || key == Qt::Key_Return) {
			if (m_lineEditDelegateVisible)
				return false;
			if (m_selectionModel->size() > 1)
				slotOpenSelected();
			else { // only one item selected
				slotBreakCurrentOperation();
				m_sLastSelectedItemName = currentFileName();
				m_lastSelectedModelIndex = currentModelIndex();
				m_subsystemMngr->slotActivated(m_tvFileList->currentIndex());
			}
		}
		else if (key == Qt::Key_Menu) {
			showContextMenu(false);
		}
		else if (ks.matches(m_pKS->keyCode(QuickRename_APP))) {
			slotInitRename();
		}
		else if (ks.matches(m_pKS->keyCode(ViewFile_APP))) {
			slotViewSelected(true, false);
		}
		else if (ks.matches(m_pKS->keyCode(EditFile_APP))) {
			slotViewSelected(true, true);
		}
// 		else if (key == Qt::Key_F5 || key == Qt::Key_F6) {
		else if ((bCopy=ks.matches(m_pKS->keyCode(CopyFiles_APP))) || ks.matches(m_pKS->keyCode(MoveFiles_APP))) {
			copyMoveFile(bCopy);
		}
		else if (ks.matches(m_pKS->keyCode(MakeDirectory_APP))) {
			makeDir();
		}
		else if (ks.matches(m_pKS->keyCode(DeleteFiles_APP)) || key == Qt::Key_Delete) {
			deleteFile();
		}
		else if (ks.matches(m_pKS->keyCode(MakeALink_APP))) {
			makeLink();
		}
		else if (ks.matches(m_pKS->keyCode(WeighCurrentDirectory_APP))) {
			weighCurrentItem();
		}
		else if (key == Qt::Key_Escape) {
			if (m_pFileListViewFilter->isVisible())
				m_pFileListViewFilter->close();
			else
				slotBreakCurrentOperation();
		}
	} // keyMod == Qt::NoModifier

	return false; // KeyEvent not to be "eaten"
}

void FileListView::weighCurrentItem()
{
// 	qDebug() << "FileListView::weighCurrentItem()";
	FileInfo *pFI = m_vfsModel->fileInfoPtr(currentModelIndex());
	if (! pFI->isDir()) {
		m_selectionModel->inverseIndexSelection(m_tvFileList->currentIndex());
		return;
	}
	if (pFI->fileName() == ".." || pFI->absoluteFileName() == "/dev" || pFI->absoluteFileName() == "/sys" || pFI->absoluteFileName() == "/proc")
		return;

	slotBreakCurrentOperation();
	emit signalUpdateStatusBar(Vfs::toString(Vfs::WEIGHING));

	if (pFI->dirLabel() != "<DIR>") {
		pFI->setDirLabel();
		if (m_vfsModel->updateRowData(Vfs::COL_NAME, pFI->fileName(), pFI)) {
//			if (m_selectionModel->selected(currentModelIndex()))
				m_selectionModel->setSelected(currentModelIndex(), false);
		}
	}
	else {
		m_weighFI = pFI;
		m_nFraneCounter = 0;
		m_pWeighAnimTimer->start(250); // emits signal every 250 ms
// 		fi->setDirLabel("+.^.+");
// 		m_vfsModel->updateRowData(0, fi->fileName(), fi);
		m_subsystemMngr->weighItem(pFI);
	}
}


// ---------------------- S L O T s -----------------------

void FileListView::slotUpdateFocusSelection( bool bInitSelectionFlag, const QString &sItemName )
{
// 	qDebug() << "## FileListView::slotUpdateFocusSelection. In. focusPolicy:" << m_tvFileList->focusPolicy() << "  m_bMouseBtnPressed:" << m_bMouseBtnPressed;
	if (m_tvFileList->focusPolicy() == Qt::NoFocus || m_bMouseBtnPressed) // don't try to set focus on the listview if focus doesn't allow for it
		return;

	bool bBackTab = (bInitSelectionFlag && sItemName.startsWith("<|"));
// 	qDebug() << "## FileListView::slotUpdateFocusSelection." << accessibleName() << "bInitSelectionFlag:" << bInitSelectionFlag << "sItemName" << sItemName << "m_lastSelectedItemName:" << m_sLastSelectedItemName << "backTab" << bBackTab;
// 	qDebug() << "## FileListView::slotUpdateFocusSelection. focusPolicy" << m_tvFileList->focusPolicy() << "hasFocus" << m_tvFileList->hasFocus() << "backTab:" << bBackTab;
	if (sItemName == "^-") { // try to set the focus on previous item on the list
// 		qDebug() << "FileListView::slotUpdateFocusSelection (1). try to set the focus on previous item on the list";
		int nIdxRow = m_tvFileList->currentIndex().row();
		if (nIdxRow > m_tvFileList->model()->rowCount())
			nIdxRow = m_tvFileList->model()->rowCount();
		m_currentIndex = m_tvFileList->model()->index(nIdxRow, 0);
		if (m_currentIndex.isValid()) {
			m_tvFileList->setCurrentIndex(m_currentIndex);
			m_sLastSelectedItemName = m_currentIndex.data().toString();
			m_lastSelectedModelIndex = m_currentIndex;
		}
		return;
	}
	else
	if (sItemName == "^") {
// 		qDebug() << "FileListView::slotUpdateFocusSelection (2). try to set the focus on previous item on the list";
		int nIdxCol = 0, nIdxRow = m_currentIndex.row();
		m_currentIndex = m_flpModel->index(nIdxRow, nIdxCol);
		QString sItem = m_currentIndex.data().toString();
		if (m_currentIndex.isValid() && (sItem != ".."  || m_flpModel->rowCount() == 1)) {
			m_tvFileList->setCurrentIndex(m_currentIndex);
			m_sLastSelectedItemName = sItem;
			m_lastSelectedModelIndex = m_currentIndex;
		}
		else { // looks for previous valid index (probably this code is never run) - run in moment of loosing focus (eg.when user invokes copy operation)
// 			qDebug() << "FileListView::slotUpdateFocusSelection. end ^11  sItem:" << sItem << "nIdxRow:" << nIdxRow;
			if (! m_currentIndex.isValid()) { // try to set the focus on previous item on the list
// 				qDebug() << "FileListView::slotUpdateFocusSelection. ! m_currentIndex.isValid";
				nIdxCol = 0, nIdxRow = m_lastSelectedModelIndex.row();
				m_currentIndex = m_flpModel->index(nIdxRow, nIdxCol);
				while (! m_currentIndex.isValid() && nIdxRow >= 0) {
					nIdxRow--;
					m_currentIndex = m_flpModel->index(nIdxRow, nIdxCol);
				}
				m_tvFileList->setCurrentIndex(m_currentIndex);
				m_sLastSelectedItemName = m_currentIndex.data().toString();
				m_lastSelectedModelIndex = m_currentIndex;
			}
			else {
// 				qDebug() << "FileListView::slotUpdateFocusSelection. m_currentIndex.isValid";
				QModelIndex newId = m_tvFileList->indexAbove(m_currentIndex);
				nIdxRow = m_currentIndex.row();
				if (newId.isValid()) {
					m_tvFileList->setCurrentIndex(newId);
					m_sLastSelectedItemName = newId.data().toString();
					m_lastSelectedModelIndex = newId;
				}
				else {
					if (nIdxRow > m_tvFileList->model()->rowCount() || nIdxRow < 0)
						nIdxRow = m_tvFileList->model()->rowCount();
					m_currentIndex = m_tvFileList->model()->index(nIdxRow, 0);
					if (m_currentIndex.isValid()) {
						m_tvFileList->setCurrentIndex(m_currentIndex);
						m_sLastSelectedItemName = m_currentIndex.data().toString();
						m_lastSelectedModelIndex = m_currentIndex;
					}
				}
			}
		}
		m_tvFileList->scrollTo(m_currentIndex);
		if (nIdxRow >= 0) {
// 			qDebug() << "-- current index name:" << m_currentIndex.data().toString();
			return;
		}
	}
// 	qDebug() << "FileListView::slotUpdateFocusSelection (3)";
	if (! bInitSelectionFlag && sItemName != ".") { // special case when panel loses focus and should back to list
		;//qDebug() << "## FileListView::slotUpdateFocusSelection. Panel lose focus and should back to list. objectName:" << objectName();
		//m_currentIndex.row() << m_currentIndex.data().toString();
		//if (sItemName.isEmpty()) {
/*			m_tvFileList->setCurrentIndex(m_currentIndex); // seems to be correct
			m_tvFileList->setFocus();
			m_tvFileList->scrollTo(m_currentIndex);*/
		//}
// 		m_bInitSelectionFlag = true;
// 		if (! bInitSelectionFlag && ! sItemName.isEmpty()) {
// 			m_tvFileList->scrollTo(m_currentIndex);
// 		}
		//return;
	}
	else
	if (((m_tvFileList->focusPolicy() == Qt::NoFocus || ! m_tvFileList->hasFocus()) && sItemName.isEmpty()) && ! bBackTab)
		return;

// 	qDebug() << "FileListView::slotUpdateFocusSelection (4)";
	if (! bInitSelectionFlag) {
		if (! sItemName.isEmpty() || (sItemName.isEmpty() && ! m_sLastSelectedItemName.isEmpty())) {
			//m_tvFileList->keyboardSearch(sPrevDirName); // doesn't work after "leave the current subsystem and jump to previous"
// 			qDebug() << "## (0) bInitSelectionFlag = false && sItemName";
			Qt::MatchFlags matchFlags = Qt::MatchFlags(Qt::MatchExactly | Qt::MatchWrap | Qt::MatchCaseSensitive);
			QString sItemToMatching = (sItemName.isEmpty() ? m_sLastSelectedItemName : sItemName);
			QModelIndexList match = m_tvFileList->model()->match(m_tvFileList->model()->index(0,0), Qt::DisplayRole, sItemToMatching, 1, matchFlags);
			if (match.count() != 0) {
				if (m_vfsModel->findFileResultMode()) { // if there are several items with the same name below prevent to jump on the first
					m_tvFileList->setCurrentIndex(m_tvFileList->currentIndex().isValid() ? m_tvFileList->currentIndex() : m_currentItemBeforeMark);
					m_currentIndex = m_tvFileList->currentIndex();
// 					qDebug() << "## (1) m_currentIndex" << m_currentIndex << m_currentIndex.data().toString();
				}
				else {
					m_currentIndex = match.first();
					m_tvFileList->setCurrentIndex(m_currentIndex);
// 					qDebug() << "## (2) (match.first) m_currentIndex" << m_currentIndex << m_currentIndex.data().toString();
				}
				m_tvFileList->setFocus();
// 				qDebug() << "## (2.1), hasFocus" << m_tvFileList->hasFocus() << "focusPolicy" << m_tvFileList->focusPolicy() << accessibleName();
			}
			else {
				m_tvFileList->setCurrentIndex(m_tvFileList->model()->index(0,0));
				m_currentIndex = m_tvFileList->model()->index(0,0);
// 				qDebug() << "## (3), hasFocus" << m_tvFileList->hasFocus() << "focusPolicy" << m_tvFileList->focusPolicy() << accessibleName();
			}
		}
		else {
			m_tvFileList->setCurrentIndex(m_tvFileList->model()->index(0,0));
			m_currentIndex = m_tvFileList->model()->index(0,0);
// 			qDebug() << "## (4), hasFocus" << m_tvFileList->hasFocus() << "focusPolicy" << m_tvFileList->focusPolicy() << accessibleName();
		}
		m_bInitSelectionFlag = true;
		if (! bInitSelectionFlag && ! sItemName.isEmpty()) {
			m_tvFileList->scrollTo(m_currentIndex);
		}
	}
	else { // bInitSelectionFlag is true
// 		qDebug() << "## (0) bInitSelectionFlag = true";
		if (sItemName.isEmpty()) {
			m_sLastSelectedItemName = currentFileName();
			m_lastSelectedModelIndex = currentModelIndex();
// 			qDebug() << "FileListView::slotUpdateFocusSelection. (1) remember currentFileName";
		}
		else {
// 			qDebug() << "FileListView::slotUpdateFocusSelection. (2) backTab";
			if (bBackTab)
				QTimer::singleShot(0, this, &FileListView::signalFocusToSecondPanel);
			else { // useful for "openCurrentItemInTab(currentModelIndex(), Vfs::OPPOSITE_ACTIVE_TAB)"
				if (sItemName.startsWith("<|")) { // this condition is requried the following code has been performed only for "openCurrentItemInTab(currentModelIndex(), Vfs::OPPOSITE_ACTIVE_TAB)"
					QKeyEvent keyTab(QEvent::KeyPress, Qt::Key_Tab, Qt::NoModifier); // doesn't work if panel is asychronous listed
					parent()->event(&keyTab);
				}
			}
		}
	}
}

void FileListView::slotFindItemKeyPressed( QKeyEvent *pKeyEvent )
{
	// this slot is called in FileListViewFindItem::closeEvent
	if (pKeyEvent == nullptr)
		return;

	switch (pKeyEvent->key())
	{
		case Qt::Key_Enter:
		case Qt::Key_Return:
			m_subsystemMngr->slotActivated(m_tvFileList->currentIndex());
			break;
		case Qt::Key_Insert:
			m_selectionModel->inverseIndexSelection(m_tvFileList->currentIndex());
			break;
		default:
			break;
	}
}

void FileListView::slotCloseEditor( QWidget *pEditor, QAbstractItemDelegate::EndEditHint )
{
// 	qDebug() << "FileListView::slotCloseEditor";
	if (pEditor == nullptr) {
		m_tvFileList->closePersistentEditor(currentModelIndex());
		return;
	}
	pEditor->close();
	m_tvFileList->setFocus();
	m_tvFileList->closePersistentEditor(currentModelIndex());
	m_lineEditDelegateVisible = false;
}

void FileListView::slotCommitEditor( QWidget *pEditor )
{
// 	qDebug() << "FileListView::slotCommitEditor";
	if (pEditor == nullptr || ! m_pListViewItemDelegate->returnPressed())
		return;

	QLineEdit *pLineEdit = static_cast<QLineEdit*>(pEditor);
	QString sNewItemName = pLineEdit->text();

	if (! sNewItemName.isEmpty()) {
		slotCloseEditor(pEditor, QAbstractItemDelegate::NoHint);
		m_subsystemMngr->slotRenameFile(m_vfsModel->fileInfoPtr(currentModelIndex()), sNewItemName);
	}
	// after leaving slotCommitEditor is called VfsFlatListModel::setData
}

void FileListView::slotOpenSelected()
{
	QString sFN = "FileListView::slotOpenSelected.";
	slotBreakCurrentOperation();
	int nSelectionSize = m_selectionModel->size();
	qDebug() << sFN << "SelectionSize:" << nSelectionSize;

	if (nSelectionSize > 1) {
		int nResult = MessageBox::No;
		if (! m_bAlwaysOpenExtViewIfFewMarked)
			nResult = MessageBox::yesNo(this, tr("View files"), tr("Selected more than one item."),
				tr("Do you want to view all of them?")+"\n"+tr("Answer 'No' (default) means view only current one."),
				m_bAlwaysOpenExtViewIfFewMarked, MessageBox::No, // default focused button
				tr("Always open external view if few marked")+"\n"+tr("Note. If you check it, then this message will not appears next time.")
				//tr("Don't ask for open external viewer if few marked")
			);
		if (nResult == 0) { // user closed dialog
			selectAll(false); // deselect all selected
			return;
		}
		if (currentFileName() == ".." && nResult == MessageBox::No) {
			m_sLastSelectedItemName = currentFileName();
			m_lastSelectedModelIndex = currentModelIndex();
			m_subsystemMngr->slotActivated(m_tvFileList->currentIndex());
			return;
		}
		if (nResult == MessageBox::No)
			selectAll(false); // deselect all selected
		if (nSelectionSize == 0)
			numberOfSelectedItems();  // if no one selected then selects current item, except ".."

		slotBreakCurrentOperation();
		m_subsystemMngr->openSelectedItemsWith("", this, false); // use default application to open all selected
	}
	else { // only one item selected
		numberOfSelectedItems(); // if no one selected then selects current item, except ".."
		FileInfoToRowMap *pSelectionMap = m_selectionModel->selectionMap();
		if (pSelectionMap->constBegin().key()->isDir()) {
			m_sLastSelectedItemName = currentFileName();
			m_lastSelectedModelIndex = currentModelIndex();
			m_subsystemMngr->slotActivated(currentModelIndex());
		}
		else {
			QString sApplicationName;
			if (m_bTopOpenWithAppUseAsDefault) {
				QString sMime  = m_vfsModel->fileInfo(currentModelIndex()).mimeInfo();
				Settings settings;
				QString sValue = settings.value("appAssociationWithMime/"+sMime).toString();
				sApplicationName = sValue.right(sValue.length()-sValue.lastIndexOf('|')-1);
			}
			slotBreakCurrentOperation();
			m_subsystemMngr->openSelectedItemsWith(sApplicationName, this, false); // use default application
		}
	}
}

void FileListView::slotViewSelected( bool bViewInWindow, bool bOpenInEditMode )
{
	if (m_lineEditDelegateVisible)
		return;

	if (bOpenInEditMode) {
		// -- checking for not editable item
		FileInfo fi = m_vfsModel->fileInfo(currentModelIndex());
		QString sMime = m_subsystemMngr->uri().mime();

		PluginFactory *pPlugiFactory = PluginFactory::instance();
		bool bPluginSupport = pPlugiFactory->isPluginAvailable(sMime, "subsystem");
		bool isSupportedArchive = (bPluginSupport && sMime != "folder" && sMime != "folder/symlink" && ! sMime.startsWith("remote/"));
		bool isRemoteSubsystem = (m_subsystemMngr->uri().protocol() != "file"); // or sMime.startsWith("remote/")
		bool isDir = (fi.isDir() || (fi.isSymLink() && fi.isDir()));

		if (isDir || isSupportedArchive || isRemoteSubsystem)
			return;
	}

	QString sFN = "FileListView::slotViewSelected.";
	slotBreakCurrentOperation();
	FileInfoToRowMap *pSelectionMap = m_selectionModel->selectionMap();
	qDebug() << sFN << "selectionSize:" << pSelectionMap->size();
	int nResult = MessageBox::No;
	QString sCurrentFileName = currentFileName();
	QString sAlwaysViewCurrentMsg = tr("Always view only current one and save selection.")+"\n"+tr("Note. When you check it, this message will not appear again.");

	if (pSelectionMap->count() == 1) {
		qDebug() << sFN << sCurrentFileName << pSelectionMap->constBegin().key()->fileName();
		if (sCurrentFileName != pSelectionMap->constBegin().key()->fileName() && ! m_bViewCurrentIfMoreSelected) {
			nResult = MessageBox::yesNo(this, tr("View files"), tr("Selected is other than the current one."),
										tr("Do you want to view selected one?")+"\n"+tr("Answer 'No' (default) means view only current one."),
										m_bViewCurrentIfMoreSelected, MessageBox::No, // default focused button
										sAlwaysViewCurrentMsg
									);
			if (nResult == 0) { // user closed dialog
				selectAll(false); // deselect all selected
				return;
			}
			if (nResult == MessageBox::No)
				selectAll(false); // deselect all selected
		}
	}
	else
	if (pSelectionMap->count() > 1 && ! m_bViewCurrentIfMoreSelected) {
		nResult = MessageBox::yesNo(this, tr("View files"), tr("Selected more than one item."),
										tr("Do you want to view all of them?")+"\n"+tr("Answer 'No' (default) means view only current one."),
										m_bViewCurrentIfMoreSelected, MessageBox::No, // default focused button
										sAlwaysViewCurrentMsg
									);
		if (nResult == 0) { // user closed dialog
			selectAll(false); // deselect all selected
			return;
		}
	}
	// -- check if file is placed inside of archive, if yes then extract it to tmp directory

	// -- try with internal viewer
	QModelIndex indexToView, currentIndex = currentModelIndex();
	if (pSelectionMap->count() == 0)
		numberOfSelectedItems();  // if no one selected then selects current item, except ".."
	if (sCurrentFileName == ".." && nResult == MessageBox::No) {
		m_sLastSelectedItemName = sCurrentFileName;
		m_lastSelectedModelIndex = currentModelIndex();
		m_subsystemMngr->slotActivated(m_tvFileList->currentIndex());
		return;
	}
	if (pSelectionMap->count() == 1)  { // try to view current one, if this is directory then open it
		FileInfo *pFI = pSelectionMap->constBegin().key();
		if (nResult == MessageBox::No)  {
			// not sure what is purpose this code (first two rows). Seems that code unmark and mark current item
			m_selectionModel->setSelected(currentIndex, false); // pFI
			numberOfSelectedItems();  // if no one selected then selects current item, except ".."
			indexToView = currentIndex;
		}
		if (pFI->isDir()) {
			m_sLastSelectedItemName = sCurrentFileName;
			m_lastSelectedModelIndex = currentModelIndex();
			//m_subsystemMngr->slotActivated(currentModelIndex());
			m_subsystemMngr->openFile(pFI);
		}
		else {
			QString sMime = pFI->mimeInfo();
			if (pFI->isSymLink())
				sMime = MimeType::getFileMime(pFI->symLinkTarget());
			if (PluginFactory::instance()->isPluginAvailable(sMime, "viewer"))
				m_subsystemMngr->createFileView(pSelectionMap, indexToView, bViewInWindow, bOpenInEditMode, this);
			else {
				nResult = MessageBox::No;
				if (! m_bUseExtViewerIfIntIsNotAvailable) {
					nResult = MessageBox::yesNo(this, tr("View files"),
								tr("Cannot find plugin related with mime:")+"\n\t"+sMime+"\n\n"+tr("Internal viewer is not available."),
								tr("Do you want to view in external viewer?"),
								m_bUseExtViewerIfIntIsNotAvailable, MessageBox::Yes, // default focused button
								tr("Always use external viewer if internal is not available")
							);
				}
				if (nResult == 0) { // user closed dialog
					selectAll(false); // deselect all selected
					return;
				}
				if (m_bUseExtViewerIfIntIsNotAvailable || nResult == MessageBox::Yes) // use external viewer
					slotOpenSelected();
				if (nResult == MessageBox::No)
					m_selectionModel->setSelected(currentIndex, false);

				return;
			}
		}
	}
	else { // selected more than one item
		// -- unselect all directories
		FileInfoToRowMap::const_iterator it = pSelectionMap->constBegin();
		while (it != pSelectionMap->constEnd()) {
			if (it.key()->isDir())
				m_selectionModel->setSelected(it.key(), false);
			++it;
		}
		if (nResult == MessageBox::No) { // open current one
			FileInfoToRowMap::const_iterator it = pSelectionMap->constBegin();
			while (it != pSelectionMap->constEnd()) {
				m_selectionModel->setSelected(it.key(), false);
				++it;
			}
			numberOfSelectedItems();  // if no one selected then selects current item, except ".."
			if (pSelectionMap->constBegin().key()->isDir() || sCurrentFileName == "..") {
				m_sLastSelectedItemName = sCurrentFileName;
				m_lastSelectedModelIndex = currentModelIndex();
				m_subsystemMngr->openFile(pSelectionMap->constBegin().key());
				return;
			} else {
				indexToView = currentIndex;
			}
		}
		m_subsystemMngr->createFileView(pSelectionMap, indexToView, bViewInWindow, bOpenInEditMode, this);
	}
}

void FileListView::slotInitRename()
{
	if (currentFileName() == "..")
		return;

	slotBreakCurrentOperation();
	m_lineEditDelegateVisible = true;
	QModelIndex currenIndex = m_tvFileList->model()->index(m_tvFileList->currentIndex().row(), 0);
	m_tvFileList->edit(currenIndex); // flag for QModelIndex must be editable  (currentModelIndex() as parameter shows error)
}


void FileListView::selectAll( bool mark )
{
	slotBreakCurrentOperation();
	QString sSelectedName = currentFileName();
	m_selectionModel->selectAll(mark);
	slotUpdateFocusSelection(false, sSelectedName);
}


void FileListView::selectWithTheSameExtention( bool mark )
{
	slotBreakCurrentOperation();
	QString sSelectedName = currentFileName();
	m_selectionModel->selectWithExtention(mark, currentFileExtention());
	slotUpdateFocusSelection(false, sSelectedName);
}

void FileListView::selectByPattern( bool mark )
{
	slotBreakCurrentOperation();
	bool bUseRegExp;
	QString sPattern = InputTextDialog::getText(this, (mark ? InputTextDialog::PATTERN_SELECT : InputTextDialog::PATTERN_UNSELECT), "*", bUseRegExp);
	if (sPattern.isEmpty())
		return;

	QString sSelectedName = currentFileName();
	m_selectionModel->selectByPattern(mark, sPattern, bUseRegExp);
	slotUpdateFocusSelection(false, sSelectedName);
}


void FileListView::openNewRemoteConnection( Vfs::RemoteConnection /*eRemoteConnection*/ )
{
	slotBreakCurrentOperation();
	QTimer::singleShot(20, m_subsystemMngr, &SubsystemManager::slotOpenConnection);
}

void FileListView::makeLink()
{
	QString sPathInSecondPanel;
	emit signalGetPathFromOppositeActiveTab(sPathInSecondPanel);
	URI uri2P(sPathInSecondPanel);
	bool bLocalPathInOppositePanel = (uri2P.host().isEmpty()); // remote or archive
	FileInfo fi = m_vfsModel->fileInfo(currentModelIndex());
	if ((fi.fileName() != ".." && bLocalPathInOppositePanel)) { // is create symlink possible?
		slotBreakCurrentOperation();
		m_subsystemMngr->makeLink(this);
	}
}


void FileListView::addCurrentPathToTheTheBookmarks()
{
	QString sAbsPath = m_subsystemMngr->path();
	if (! m_subsystemMngr->uri().host().isEmpty())
		sAbsPath.insert(0, m_subsystemMngr->uri().host());
	m_pBookmarks->addToTheBookmarks(sAbsPath);
}

void FileListView::bookmarksNeedToChangePath( int nId )
{
	QString sNewPath = m_pBookmarks->pathFromBookmarks(nId);
// 	qDebug() << "FileListView::bookmarksNeedToChangePath. sNewPath:" << sNewPath;
	if (! sNewPath.isEmpty())
		m_subsystemMngr->slotPathChanged(sNewPath);
}

void FileListView::showHideSection( int nId, bool bShow )
{
	QHeaderView *pHeader = m_tvFileList->header();
	int nLogicalId = pHeader->logicalIndex(nId);
	if (bShow) {
		if (pHeader->isSectionHidden(nLogicalId))
			pHeader->showSection(nLogicalId); // in actual column is not visible, because has width 0, so need to resise it
	}
	else { // hide section
		if (! pHeader->isSectionHidden(nLogicalId))
			pHeader->hideSection(nLogicalId);
	}
}


bool FileListView::quickGetOutOfSubsystem()
{
	if (m_vfsModel->findFileResultMode())
		return false;

	slotBreakCurrentOperation();
	m_subsystemMngr->slotCloseConnection();
	return false;
}


void FileListView::oneDirForward( const URI &nextURI )
{
	slotBreakCurrentOperation();
	QString sPath = nextURI.toString();
	m_subsystemMngr->slotPathChanged(sPath);
}

void FileListView::oneDirBackward( const URI &prevURI )
{
	slotBreakCurrentOperation();
	QString sPath = prevURI.toString();
	m_subsystemMngr->slotPathChanged(sPath);
}


void FileListView::slotMenuEntryHovered( QAction *pAction )
{
	if (pAction->statusTip().isEmpty())
		slotUpdateListViewStatusBar();
	else
		emit signalUpdateOfListStatus(pAction->statusTip());
}

void FileListView::slotContexMenuAction( int nActionId )
{
	//qDebug() << "FileListView::slotContexMenuAction. nActionId" << nActionId;
	if      (nActionId == CMD_Open)
		slotOpenSelected();
	else if (nActionId == CMD_OpenWithSelected) {
		slotBreakCurrentOperation();
		m_subsystemMngr->openSelectedItemsWith("", this); // select application to open file
	}
	else if (nActionId >= CMD_OpenWithSelectedMinId) {
		slotBreakCurrentOperation();
		m_subsystemMngr->openSelectedItemsWith(m_pContextMenu->actionText(nActionId), this, false); // nActionId
	}
	else if (nActionId == CMD_OpenInOppositeActiveTab) {
		slotBreakCurrentOperation();
		m_subsystemMngr->openCurrentItemInTab(currentModelIndex(), Vfs::OPPOSITE_ACTIVE_TAB);
	}
	else if (nActionId == CMD_OpenInNewTab) {
		slotBreakCurrentOperation();
		m_subsystemMngr->openCurrentItemInTab(currentModelIndex(), Vfs::NEW_TAB); // NEW_ACTIVE or NEW_NOACTIVE (depends on configuration)
	}
	else if (nActionId == CMD_View)
		slotViewSelected(true, false);
	else if (nActionId == CMD_Edit)
		slotViewSelected(true, true);
	else if (nActionId == CMD_Rename)
		slotInitRename();
	else if (nActionId == CMD_Remove) {
		slotBreakCurrentOperation();
		m_subsystemMngr->removeFiles(m_selectionModel->selectionMap(), this);
	}
	else if (nActionId == CMD_CopyTo || nActionId == CMD_MoveTo) {
		slotBreakCurrentOperation();
		m_subsystemMngr->copyFiles(m_selectionModel->selectionMap(), (nActionId == CMD_CopyTo), this);
	}
	else if (nActionId == CMD_CreateNewDirectory) {
		slotBreakCurrentOperation();
		m_subsystemMngr->createNewEmptyFile(currentModelIndex(), (nActionId == CMD_CreateNewTxtFile), this);
	}
	else if (nActionId == CMD_CreateNewTxtFile) {
		slotBreakCurrentOperation();
		m_subsystemMngr->createNewEmptyFile(currentModelIndex(), true, this);
	}
	else if (nActionId == CMD_SelectAll)
		selectAll(true);
	else if (nActionId == CMD_UnSelectAll)
		selectAll(false);
	else if (nActionId == CMD_SelectTemplate)
		selectByPattern(true);
	else if (nActionId == CMD_UnSelectTemplate)
		selectByPattern(false);
	else if (nActionId == CMD_SelectWithExt)
        selectWithTheSameExtention(true);
	else if (nActionId == CMD_UnSelectWithExt)
        selectWithTheSameExtention(false);
	else if (nActionId == CMD_ShowProperties)
		showPropertiesDialog();
	else if (nActionId == CMD_FilterWithExtension)
		filterWithExtension();
	else if (nActionId == CMD_RemoveFilter)
		removeFilter();
	else if (nActionId == CMD_ExtractTo) {
		slotBreakCurrentOperation();
		m_subsystemMngr->extract(m_selectionModel->selectionMap(), this);
	}
	else if (nActionId == CMD_ExtractHere) {
		slotBreakCurrentOperation();
		m_subsystemMngr->extractArchive(Vfs::EXTRACT_HERE, m_selectionModel->selectionMap(), this);
	}
	else if (nActionId == CMD_AddToArchive || nActionId == CMD_MoveToArchive) {
		createAnArchive((nActionId == CMD_MoveToArchive));
	}
	else if (nActionId == CMD_Makelink) {
		slotBreakCurrentOperation();
		m_subsystemMngr->makeLink(this);
	}
	else if (nActionId == CMD_EditSymlink) {
		slotBreakCurrentOperation();
		m_subsystemMngr->editSymLink(currentModelIndex(), this);
	}
	else if (nActionId >= CMD_BookmarksMenuInit && nActionId < m_pBookmarks->bookmarksOptionId())
		bookmarksNeedToChangePath(nActionId);
	else if (nActionId == m_pBookmarks->bookmarksOptionId())
		addCurrentPathToTheTheBookmarks();
	else if (nActionId == m_pBookmarks->bookmarksOptionId()+1)
		;//showUrlsManager();
}

void FileListView::slotUpdateContextMenu()
{
	m_pContextMenu->init(m_pKS);
	m_pContextMenu->setContextMenuType(ContextMenu::FilesPanelMENU);
	m_subsystemMngr->updateContextMenu(m_pContextMenu, m_pKS); // update for subsystem
}

void FileListView::slotUpdateListViewStatusBar()
{
	int rowCount = (m_pFileListViewFilter->listFiltered()) ? m_flpModel->rowCount() : m_vfsModel->rowCount();
	if (rowCount > 0 && ! m_pFileListViewFilter->listFiltered())  {
		if (m_vfsModel->index(0,0).data().toString() == "..")
			rowCount--;
	}
// 	qDebug() << "FileListView::slotUpdateListViewStatusBar." << objectName() << "selectionSize:" << m_selectionModel->size() << "rowCount:" << rowCount << "selectionWeigh:" << m_vfsModel->selectionWeigh();
	emit signalUpdateOfListStatus(m_selectionModel->size(), rowCount, m_selectionModel->weighOfSelected());
	m_bMouseBtnPressed = false; // thanks that after double click on folder/archive focus is present on the list
}

void FileListView::slotWeighStartAnimation()
{
	QStringList strList;
// 	strList << "-." << "\\" << "/" << ".-";
	strList << "< -. >" << "< \\  >" << "<  / >" << "< .- >"; // depended to font failly, fox fixed font should be with signle space
	if (m_nFraneCounter > strList.size()-1)
		m_nFraneCounter = 0;
	m_weighFI->setDirLabel(strList[m_nFraneCounter]);
	m_vfsModel->updateRowData(Vfs::COL_NAME, m_weighFI->fileName(), m_weighFI);
	m_nFraneCounter++;
}

void FileListView::slotWeighStopAnimation()
{
	m_pWeighAnimTimer->stop();
}


void FileListView::filterWithExtension()
{
// 	qDebug() << "FileListView::filterWithExtension. ext:" << m_sFilterWithExtension;
	m_pFileListViewFilter->applyGivenFilter("*."+m_sFilterWithExtension);
}

void FileListView::removeFilter()
{
// 	qDebug() << "FileListView::removeFilter.";
	m_pFileListViewFilter->applyGivenFilter("*");
}


void FileListView::showPropertiesDialog()
{
	slotBreakCurrentOperation();

	if (m_pPropertiesDlg != nullptr) {
		delete m_pPropertiesDlg;  m_pPropertiesDlg = nullptr;
	}
	int nNumOfSelected = numberOfSelectedItems(); // if no one selected then selects current item, except ".."
	if (nNumOfSelected == 0) // maybe focusBar matches to item ".."
		return;

	m_pPropertiesDlg = new PropertiesDialog(this);
	connect(m_pPropertiesDlg, &PropertiesDialog::signalOkPressed, this, &FileListView::slotSetAttributes);
	connect(m_pPropertiesDlg, &PropertiesDialog::signalClose, this, &FileListView::slotClosePropertiesDlg);
	connect(m_pPropertiesDlg, &PropertiesDialog::signalStartWeighing, this, &FileListView::slotStartWeighing);

	m_subsystemMngr->disconnect(SIGNAL(signalWeighingResult(qint64, qint64, uint, uint)));
	m_subsystemMngr->disconnect(SIGNAL(signalClosePropertiesDlg()));
	m_subsystemMngr->disconnect(SIGNAL(signalShowErrorWeighingInfoOnPropertiesDlg(bool)));
	connect(m_subsystemMngr, &SubsystemManager::signalWeighingResult, m_pPropertiesDlg, &PropertiesDialog::slotSetWeighingResult);
	connect(m_subsystemMngr, &SubsystemManager::signalClosePropertiesDlg, this, &FileListView::slotClosePropertiesDlg);
	connect(m_subsystemMngr, &SubsystemManager::signalShowErrorWeighingInfoOnPropertiesDlg, m_pPropertiesDlg, &PropertiesDialog::slotShowErrorInfoForWeighing);

	// --- detects if this is inside of subsytem which is readonly (assumption is: all remote and archive)
	QString sMime = m_subsystemMngr->uri().mime();
	bool isRemoteSubsystem = (m_subsystemMngr->uri().protocol() != "file"); // or sMime.startsWith("remote/")
	bool bPluginSupport = PluginFactory::instance()->isPluginAvailable(sMime, "subsystem");
	bool isArchive = (bPluginSupport && sMime != "folder" && sMime != "folder/symlink");
	bool bSubsystemReadOnly = (isArchive || isRemoteSubsystem);
// 	qDebug() << "host:" << m_subsystemMngr->lastURI().host() << "URI mime" << sMime << "archive" << isArchive << "remote:" << isRemoteSubsystem;

	m_pPropertiesDlg->init(m_selectionModel->selectionMap(), m_subsystemMngr->uri(), bSubsystemReadOnly);
	m_pPropertiesDlg->show();
}

void FileListView::slotClosePropertiesDlg()
{
	qDebug() << "FileListView::slotClosePropertiesDlg.";
	if (m_pPropertiesDlg != nullptr) {
		slotBreakCurrentOperation();
		selectAll(false);
		delete m_pPropertiesDlg; m_pPropertiesDlg = nullptr;
	}
}

void FileListView::slotSetAttributes( FileInfo *pFileInfo, FileAttributes *pChangedAttrib, bool bChangesForAllSelected )
{
	slotBreakCurrentOperation();
	m_pPropertiesDlg->hide();
	if (m_pPropertiesDlg->readOnlyMode() || ! m_pPropertiesDlg->changeTheAttributes()) {
		selectAll(false);
		slotClosePropertiesDlg();
		return;
	}

	int nNumOfSelected = numberOfSelectedItems(); // if no one selected then selects current item, except ".."
	if (nNumOfSelected == 0) // maybe focusBar matches to item ".."
		return;

	bool bSingleFile = false;
	int nResult;
	if (m_bSetAttribConfirmation) {
		QString sFImsg, sInfo;
		if (nNumOfSelected > 1 && bChangesForAllSelected) {
			sFImsg = m_selectionModel->selectedItemsMsg();
			sInfo = tr("them");
		}
		else { // one item selected
// 			FileInfo *selFI = m_selectionModel->selectionMap()->begin().key();
			sFImsg = "\""+pFileInfo->fileName()+"\"";
			sInfo = (pFileInfo->isDir() && ! pFileInfo->isSymLink()) ? tr("directory") : tr("file");
			bSingleFile = (pFileInfo->isSymLink() || ! pFileInfo->isDir());
		}
		nResult = MessageBox::yesNo(this, tr("Setting attributes"), sFImsg,
										tr("Are you sure you want to set attributes for")+" "+sInfo+"?",
										m_bWeighBeforeSettingAttrib,
										MessageBox::Yes, // default focused button
										bSingleFile ? "" : tr("Weigh before setting atributes")
									);
		if (nResult == 0) { // user closed dialog
			selectAll(false); // deselect all selected
			return;
		}
	}
	else
		nResult = MessageBox::Yes;

	if (nResult == MessageBox::Yes) {
		if (! bChangesForAllSelected) { // select only one item
			selectAll(false); // deselect all selected
			m_selectionModel->setSelected(pFileInfo, true, true);
		}
		m_subsystemMngr->setAttributes(m_selectionModel->selectionMap(), pChangedAttrib, m_bWeighBeforeSettingAttrib);
	}
	else {
		selectAll(false); // deselect all selected
		slotClosePropertiesDlg();
		return;
	}

	if (! bSingleFile) {
		Settings settings;
		settings.update("vfs/WeighBeforeSettingAttrib", m_bWeighBeforeSettingAttrib);
	}
}

void FileListView::slotDrivesMenuPathChanged( const QString &sPath )
{
	if (! sPath.isEmpty())
		m_subsystemMngr->slotPathChanged(sPath);
}

void FileListView::slotBreakCurrentOperation()
{
	if (m_pWeighAnimTimer->isActive())
		m_pWeighAnimTimer->stop();

	m_subsystemMngr->setBreakOperation();
}

void FileListView::slotStartWeighing( FileInfo *pFileInfo, bool bStartWeigh )
{
	qDebug() << "FileListView::slotStartWeighing. FileInfo:" << pFileInfo << "StartWeigh:" << bStartWeigh;
	if (! bStartWeigh) {
		slotBreakCurrentOperation();
	}
	else
	if (pFileInfo != nullptr) {
		qDebug() << "FileListView::slotStartWeighing. Weigh current item";
		FileInfo *pFI = m_vfsModel->fileInfoPtr(currentModelIndex());
		QString sAbsoluteFilePath = pFI->absoluteFileName();
		if (pFI->fileName() == ".." || sAbsoluteFilePath == "/dev" || sAbsoluteFilePath == "/sys" || sAbsoluteFilePath == "/proc")
			return;
		m_subsystemMngr->weighItem(pFileInfo, false); // false - don't update data model
	}
	else {
		qDebug() << "FileListView::slotStartWeighing. Weigh all selected";
		m_subsystemMngr->weighSelectedItems(m_selectionModel->selectionMap());
	}
}

void FileListView::slotSelectionChanged( const QItemSelection &, const QItemSelection & )
{
	if (! currentModelIndex().isValid())
		return;

	FileInfo fi = m_vfsModel->fileInfo(currentModelIndex());
	QString sMsg = fi.fileName();
	if (sMsg == "..")
		sMsg = tr("Subdirectory");
	else
	if (fi.isSymLink())
		sMsg += " -> " + fi.symLinkTarget();

	emit signalUpdateStatusBar(sMsg);
}

void FileListView::applyListViewSettings( const ListViewSettings &listViewSettings, const QString &sCategory )
{
	//qDebug() << "FileListView::applyListViewSettings. category:" << sCategory << "Panel:" << objectName();
	if (sCategory == tr("Colors")) {
		m_bAlternatingRowColors = listViewSettings.showSecondBackgroundColor;
		n_sItemColor            = listViewSettings.itemColor.name();
		m_sItemColorUnderCursor = listViewSettings.itemColorUnderCursor.name();
		m_sColorOfSelectedItem  = listViewSettings.colorOfSelectedItem.name();
		m_sColorOfSelectedItemUnderCurs = listViewSettings.colorOfSelectedItemUnderCursor.name();
	// 	m_sBackgroundColorOfSelectedItem = listViewSettings.backgroundColorOfSelectedItem.name();
	// 	m_sBackgroundColorOfSelectedItemUnderCursor = listViewSettings.backgroundColorOfSelectedItemUnderCursor.name();
		m_sBackgroundColor      = listViewSettings.backgroundColor.name();
		m_sSecondBackgroundColor = listViewSettings.secondBackgroundColor.name();
		m_sCursorColor          = listViewSettings.cursorColor.name();
		m_sHiddenFileColor      = listViewSettings.hiddenFileColor.name();
		m_sExecutableFileColor  = listViewSettings.executableFileColor.name();
		m_sSymlinkColor         = listViewSettings.symlinkColor.name();
		m_sBrokenSymlinkColor   = listViewSettings.brokenSymlinkColor.name();
	}
	else
	if (sCategory == tr("Columns")) {
		//qDebug() << "FileListView::applyListViewSettings. sCategory:" << sCategory;
		// all of this done by updateView()
	 	//showHideSection(EXTNAMEcol, (listViewSettings.columnsWidthTab[EXTNAMEcol] > 0)); // first col.is always visible
		showHideSection(SIZEcol,  (listViewSettings.columnsWidthTab[SIZEcol]  > 0));
		showHideSection(TIMEcol,  (listViewSettings.columnsWidthTab[TIMEcol]  > 0));
		showHideSection(PERMcol,  (listViewSettings.columnsWidthTab[PERMcol]  > 0));
		showHideSection(OWNERcol, (listViewSettings.columnsWidthTab[OWNERcol] > 0));
		showHideSection(GROUPcol, (listViewSettings.columnsWidthTab[GROUPcol] > 0));
		showHideSection(FTYPEcol, (listViewSettings.columnsWidthTab[FTYPEcol] > 0));
		m_bSyncColWidthInBothPanels = listViewSettings.synchronizeColWidthInBothPanels;
		// need to resize all columns, becasue seems that after any column becomes visible, gets width = 0
		int nWidth = 0;
		m_vfsModel->beginResetVfsModel();
		const int nMaxCol = m_vfsModel->findFileResultMode() ? FIND_FILES_COLUMNS : LIST_VIEW_COLUMNS;
		for (int i=0; i<nMaxCol; i++) {
			if (m_vfsModel->findFileResultMode())
				nWidth = m_ntFindFilesCol[i];
			else
				nWidth = m_ntListViewCol[i];
			if (listViewSettings.columnsWidthTab[i] > 0) { // if column is visible then resize it
				if (nWidth == 0)
					nWidth = listViewSettings.columnsWidthTab[i];
				m_tvFileList->header()->resizeSection(m_tvFileList->header()->logicalIndex(i), nWidth);
			}
		}
		m_vfsModel->endResetVfsModel();
	}
	else
	if (sCategory == tr("Fonts")) {
		//qDebug() << "FileListView::applyListViewSettings. listViewSettings.fontFamily:" << listViewSettings.fontFamily; // DEBUG
		QString sFontStyle = "regular";
		if (listViewSettings.fontBold)
			sFontStyle = "bold";
		if (listViewSettings.fontItalic)
			sFontStyle = "italic";
		m_sTreeViewFont = listViewSettings.fontFamily+","+QString("%1").arg(listViewSettings.fontSize)+","+sFontStyle;
	}
	else
	if (sCategory == tr("Other settings")) {
		m_bShowIcons  = listViewSettings.showIcons;
		m_bBoldedDir  = listViewSettings.boldedDir;
		m_bBytesRound = listViewSettings.bytesRound;
		m_bSelectDirs = listViewSettings.selectDirs;
		m_bShowHiddenFiles = listViewSettings.showHiddenFiles;
	}
	updateView();
	m_flpModel->setFilterRegExp("");
	//Qt::FocusReason
// 	m_tvFileList->clearFocus();
// 	QTimer::singleShot(0, this, &FileListView::signalFocusToSecondPanel);
// 	emit signalFocusToSecondPanel();
// 	m_tvFileList->setFocus(); // set focus in opposite panel
// 	emit signalFocusToSecondPanel(); // display two focuses
}


/*void FileListView::slotResortView()
{
	qDebug() << "--------- resort in" << objectName();
	m_tvFileList->sortByColumn(0);
}*/


void FileListView::slotSectionResized( int logicalIndex, int oldSize, int newSize )
{
	if (m_bSyncColWidthInBothPanels)
		Q_EMIT signalUpdateColWidthInOppositePanel(logicalIndex, oldSize, newSize);

	if (m_vfsModel->findFileResultMode())
		m_ntFindFilesCol[logicalIndex] = newSize;
	else
		m_ntListViewCol[logicalIndex] = newSize;
}

QString FileListView::columnsWidthStr() const
{
	int nWidth;
	QString sColumns, sCol, sWidth;
	const int nMaxCol = m_vfsModel->findFileResultMode() ? FIND_FILES_COLUMNS : LIST_VIEW_COLUMNS;

	for (int i=0; i<nMaxCol; i++) {
		if (m_vfsModel->findFileResultMode())
			nWidth = m_ntFindFilesCol[i];
		else
			nWidth = m_ntListViewCol[i];
		sCol = QString("%1").arg(i);
		sWidth = QString("%1").arg(nWidth);
		sColumns += sCol + "-" + sWidth + ",";
	}
	sColumns = sColumns.left(sColumns.length()-1);
	return sColumns;
}
