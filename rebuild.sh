#!/bin/bash
# Script handles following parameters:
# first param. : -mt/-nomt - selection between multi-thread build or one-thread build
# second param.: path where application will be installed
# third.param.:  -ins - application will be installed in set (in second param.) path
# or
# second param.: -ins - application will be installed in default path
# third param.:  path where application will be installed

BUILD_TYPE="-mt"
#BUILD_TYPE="-mt -ins"
INSTALL_PATH="$HOME/install_test" # default installation path

[ "$1" != "" ] && BUILD_TYPE="$1"
if [ "$BUILD_TYPE" != "-mt" -a "$BUILD_TYPE" != "-nomt" ]; then
	echo "Incorrect first parameter. Available are following: \"-mt\" and \"-nomt\"."
	exit 1
fi
INSTALL=0
if echo "$2" | grep ^\/ >/dev/null; then
	INSTALL_PATH="$2"
	[ "$3" == "-ins" ] && INSTALL=1
elif [ "$2" == "-ins" ]; then
	INSTALL=1
	if echo "$3" | grep ^\/ >/dev/null; then
		INSTALL_PATH="$3"
	fi
fi
#[ "$2" != "" ] && INSTALL_PATH="$2"

rm -rf build;  mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=$INSTALL_PATH ../
[ $? -ne 0 ] && exit $?

if [ "$BUILD_TYPE" == "-mt" ]; then
	make -j`grep 'core id' /proc/cpuinfo | wc -l`
else
	make
fi


if [ $INSTALL -eq 1 ]; then
	if [ ! -d "$INSTALL_PATH" ]; then
		mkdir "$INSTALL_PATH"
		if [ $? -ne 0 ]; then
			echo "Cannot install due to cannot create installation directory!"
			exit 1
		fi
	fi
	make install
	strip $INSTALL_PATH/bin/qtcmd2
	strip $INSTALL_PATH/lib/*.so
	strip $INSTALL_PATH/lib/qtcmd2/plugins/*.so
fi

exit 0
