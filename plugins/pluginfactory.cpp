/***************************************************************************
 *   Copyright (C) 2007 by Mariusz Borowski <b0mar@nes.pl>                 *
 *   Copyright (C) 2015 by Piotr Mierzwiñski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDebug> // needed for qDebug()
#include <QFileInfo>

#include "configurationdialog.h"
#include "preview/viewer.h"
#include "subsystem.h"

#include "pluginfactory.h"


PluginFactory::PluginFactory()
{
	qDebug() << "PluginFactory::PluginFactory. PRIVATE (empty)";
}

PluginFactory::~PluginFactory()
{
	qDebug() << "PluginFactory::~PluginFactory. DESTRUCTOR";// m_mimeToObjectAndClassRelation.count:" << m_mimeToObjectAndClassRelation.count();

	QString sPluginClass;
	void *currPtr, *prevPtr = NULL;

	QList < ObjClass > list = m_subsysMimeToObjAndClassRelation.values();
	std::sort(list.begin(), list.end());
	Vfs::Subsystem *pSubsystem;

	for (int i=0; i<list.size(); ++i) {
		currPtr      = list.at(i).first;
		sPluginClass = list.at(i).second;
		//qDebug() << QString("list.at(%1)  ptr: %2  plugin_class: %3  mime: %4").arg(i).arg((int)list.at(i).first).arg(list.at(i).second).arg(m_mimeToObjectAndClassRelation.key(list.at(i)));
		if (currPtr != prevPtr) {
			if (sPluginClass == "subsystem") {
				pSubsystem = (Vfs::Subsystem *)currPtr;
				//qDebug() << "PluginFactory::~PluginFactory. remove plugin and PluginData obj.for:" << pSubsystem->pluginData()->name;
				delete pSubsystem;
			}
// 			else
// 			if (sPluginClass == "viewer") {
				//delete (Preview::Viewer *)currPtr; // delete is not necessary, because every plugin object is removed in destructor of FileViewer class (removed in QSplitter)
// 			}
// 			else
// 			if (sPluginClass == "other") {
// 				delete (Other::Other *)currPtr;
// 			}
		}
		prevPtr = currPtr;
	}
	// -- removing all objects of PluginData (in this moment includes Viewers and Subsystems)
	QMap <QString, PluginData *>::const_iterator it = m_mimeToPluginData.constBegin();
	while (it != m_mimeToPluginData.end()) {
		qDebug() << "PluginFactory::~PluginFactory. Remove PluginData obj.for:" << (it.value())->name;
		delete it.value();
		++it;
	}
	m_mimeToPluginData.clear();
	m_subsysMimeToObjAndClassRelation.clear();
}

PluginFactory *PluginFactory::instance()
{
	static PluginFactory instance;
	return &instance;
}


int PluginFactory::size( const QString &sPluginClass ) const
{
	if (sPluginClass.isEmpty())
		return m_mimeToPluginData.size();

	int nSize = 0;
	QMap <QString, PluginData *>::const_iterator it = m_mimeToPluginData.constBegin();
	while (it != m_mimeToPluginData.end()) {
		if ((it.value())->plugin_class == sPluginClass)
			nSize++;
		++it;
	}

	return nSize;
}


QString PluginFactory::load( const QString &sAbsPathToPlugin )
{
	QString sFN = "PluginFactory::load.";
	if (! QFile(sAbsPathToPlugin).exists()) {
		qDebug() << sFN << "-- ERROR: File doesn't exist:" << sAbsPathToPlugin;
		return "";
	}

	QString sPluginClass;

	m_lib.setFileName(sAbsPathToPlugin);
	if (m_lib.isLoaded()) {
		qDebug() << sFN << "Plugin" << m_lib.fileName() << "already loaded.";
		return "";
	}
	// -- check supported plugins class (now there are: 'subsystem' and 'view')
	PluginClass plugin_class = (PluginClass) m_lib.resolve("plugin_class");
	if (plugin_class) {
		sPluginClass = plugin_class();
		if (sPluginClass != "subsystem" && sPluginClass != "viewer" && sPluginClass != "config") {
			qDebug() << sFN << "-- ERROR: Unsupported plugin class:" << sPluginClass << "Plugin might not be created.";
			m_lib.unload(); // because resolve is loading plugin
			return "";
		}
	}
	qDebug() << sFN << "Plugin" << QFileInfo(sAbsPathToPlugin).fileName() << "of class:" << sPluginClass << "loaded successfully.";

	PluginData *pPluginData;
	QString sShortPluginData = sPluginClass;
	QString sMime = "";

	if (sPluginClass == "viewer") {
		// -- create an data object and get its pointer
		t_func createObj = (t_func) m_lib.resolve("create_plugin_data");
		if (! createObj) {
			qDebug() << sFN << QString("Can't resolve 'create_plugin_data' symbol (%1)!").arg(m_lib.errorString());
			return "";
		}
		QObject *pObject = createObj(0);
		if (! pObject) {
			qDebug() << sFN << "Can't create object PluginData for plugin:" << QFileInfo(m_lib.fileName()).fileName();
			return "";
		}
		pPluginData = (PluginData *)(pObject);
		sMime = pPluginData->mime_type;
		m_mimeToPluginData.insert(sMime, pPluginData);
		m_mimeToPluginLocation.insert(sMime, sAbsPathToPlugin); // need to obj.creation in the future

		sShortPluginData += "|" + sMime;
		return sShortPluginData;
	}

/*	m_mimeToPluginLocation.insertMulti(sPluginClass, sAbsPathToPlugin);
	// insert() to m_mimeToPluginLocation will be overwrite existing item, but using insertMulti for subsystem class there will be several the same entries.
	// In this moment Subsystem doesn't require multi instances of object, so we don't need to create new one object.
*/
// 	QString sShortPluginData = sPluginClass;
// 	if (sPluginClass == "viewer") { // viewer can't be created here due to is going to be destroyed only in destructor of fileviewer class
// 		sShortPluginData += "|" + sMime;
// 		return sShortPluginData;
// 	}

	void *pObject = createObject(sMime, sPluginClass); // for loaded plugin

	if (sPluginClass == "subsystem" && pObject != NULL) {
		Vfs::Subsystem *pSubsystem = (Vfs::Subsystem *)(pObject);
		pPluginData = pSubsystem->pluginData();
		if (pPluginData != NULL) {
			sShortPluginData += "|" + pPluginData->mime_type;
			m_mimeToPluginData.insert(pPluginData->mime_type, pPluginData);
			m_mimeToPluginLocation.insert(pPluginData->mime_type, sAbsPathToPlugin); // need to obj.creation in the future
		}
	}

	if (sPluginClass == "config" && pObject != NULL) {
	}

	return sShortPluginData; // loading finished success (pObject != NULL);
}

bool PluginFactory::unload( const QString &sInpMime, const QString &sPluginClass )
{
	QString sFN = "PluginFactory::unload";
	if (! sInpMime.isEmpty()) {
		// -- look for available plugin
		if (! isPluginAvailable(sInpMime, sPluginClass)) // if plugin found then set in m_sLastSearchingPluginPath its path and m_sLastMatchingMime
			return false;
		QString sAbsPathToPlugin = m_sLastSearchingPluginPath;
		if (sAbsPathToPlugin.isEmpty()) {
			qDebug() << sFN << "Got empty path to plugin for mime:" << sInpMime << "and class:" << sPluginClass;
			return false;
		}
		if (! QFile(sAbsPathToPlugin).exists()) {
			qDebug() << sFN << "-- ERROR. File doesn't exist:" << sAbsPathToPlugin;
			return false;
		}
		m_lib.setFileName(sAbsPathToPlugin);
	}

	if (! m_lib.isLoaded()) {
		qDebug() << sFN << "Plugin" << m_lib.fileName() << "not loaded.";
		return true;
	}
	bool bUnloaded = m_lib.unload();
	if (bUnloaded)
		qDebug() << sFN << "Plugin:" << m_lib.fileName() << "unloaded.";
	else
		qDebug() << sFN << "-- ERROR. Can't unload plugin:" << m_lib.fileName();

	return bUnloaded;
}

void *PluginFactory::createObject( const QString &sInpMime, const QString &sPluginClass )
{
	QString sFN = "PluginFactory::createObject. -- WARNING -- :";
	QString sPluginMime = sInpMime;
	if (! sPluginMime.isEmpty()) {
		// -- look for available plugin
		if (! isPluginAvailable(sPluginMime, sPluginClass)) // if plugin found then set in m_sLastSearchingPluginPath its path and m_sLastMatchingMime
			return NULL;
		QString sAbsPathToPlugin = m_sLastSearchingPluginPath;
		if (sAbsPathToPlugin.isEmpty()) {
			qDebug() << sFN << "Got empty path to plugin for mime:" << sPluginMime << "and class:" << sPluginClass;
			return NULL;
		}
		sPluginMime = m_sLastMatchingMime;
		if (sPluginMime.isEmpty()) {
			qDebug() << sFN << "Cannot find matching mime in available plugins collection for mime:" << sPluginMime;
			return NULL;
		}
		if (! QFile(sAbsPathToPlugin).exists()) {
			qDebug() << sFN << "File doesn't exist:" << sAbsPathToPlugin;
			return NULL;
		}
		m_lib.setFileName(sAbsPathToPlugin);
	}
	else
	if (sPluginClass == "viewer") {
		qDebug() << sFN << "Empty input mime for plugin_class: viewer. Can't create object.";
		return NULL;
	}

	if (m_lib.fileName().isEmpty()) {
		qDebug() << sFN << "File name doesn't set. Can't create object.";
		return NULL;
	}

	// -- create an object and get its pointer
	t_func createObj = (t_func) m_lib.resolve("create_plugin");
	if (! createObj) {
		qDebug() << sFN << QString("Can't resolve 'create_plugin' symbol (%1)!").arg(m_lib.errorString());
		return NULL;
	}
	QObject *pObject = createObj(0);
	if (pObject == NULL) {
		qDebug() << sFN << "Can't create object of plugin. Plugin name:" << QFileInfo(m_lib.fileName()).fileName();
		return NULL;
	}
	else {
		sFN.remove(" -- WARNING -- :");
		qDebug() << sFN << "Object from plugin has been created.";
	}

	// -- get pluginData
	PluginData *pPluginData = NULL;
	if (sPluginClass == "subsystem") {
		Vfs::Subsystem *pSubsystem = (Vfs::Subsystem *)(pObject);
		pPluginData = pSubsystem->pluginData();
		sPluginMime = pPluginData->mime_type;
	}
	else
	if (sPluginClass == "viewer") {
		Preview::Viewer *pViewer = (Preview::Viewer *)(pObject);
		//pPluginData = m_mimeToPluginData[sPluginMime]; // original
		pPluginData = m_mimeToPluginData.values(sPluginMime).first(); // ??? NEEDS tests
		pViewer->setPluginData(pPluginData);
	}
	else
	if (sPluginClass == "config") {
		ConfigurationDialog *pConfiguration = (ConfigurationDialog *)(pObject);
		pPluginData = pConfiguration->pluginData();
		sPluginMime = pPluginData->mime_type;
	}

	if (pPluginData == NULL) {
		qDebug() << sFN << "-- WARNING -- Cannot get PluginData object.";
	}
	else {
		qDebug() << sFN << "Got PluginData object with name:" << pPluginData->name << ", version:" << pPluginData->version << ", class:" << pPluginData->plugin_class
					<< ", mime_type:" << sPluginMime << ", protocol:" << pPluginData->protocol
					<< ", description:" << pPluginData->description;
	}

	// Does plugin support multi-mime?
	if (sPluginMime.indexOf(':') != -1) { // yes (for example: TarSubsystem), so need to insert all relations to plugin
		QStringList slMimeList = sPluginMime.split(':');
		foreach(const QString &sMime, slMimeList) {
			qDebug() << sFN << "Set object relation plugin class:" << sPluginClass << "with mime:" << sMime;
			m_subsysMimeToObjAndClassRelation.insert(sMime, ObjClass(pObject, sPluginClass));
		}
	}
	else { // Doesn't, only set relation one time.
		qDebug() << sFN << "Set object relation plugin class:" << sPluginClass << "with mime:" << sPluginMime;// << "object:" << (int)object;
		m_subsysMimeToObjAndClassRelation.insert(sPluginMime, ObjClass(pObject, sPluginClass));
	}
	//qDebug() << "-> registrering plugin" << (*it).fileName() << "supported mime:" << sMime;
	// TODO: For viewer the map: m_mimeToObjectAndClassRelation will be grow in every time when user will be open the viewer.
	// Viewer object (loaded from plugin) will be destroyed in destructor of FileViewer class, but this map will not change.
	// Hence there is need some solution to clean this map from every not used entries related to viewer.
	// 1. after closing last one view clean all viewer entries
	// 2. after closing one viewer remove entry from this map

	return pObject;
}

void *PluginFactory::getObject( const QString &sInpMime, const QString &sPluginClass, int nId ) const
{
	if (sInpMime.isEmpty()) {
		qDebug() << "PluginFactory::getObject. Internal ERROR! Empty input mime!";
		return NULL;
	}
	if (sPluginClass.isEmpty()) {
		qDebug() << "PluginFactory::getObject. Internal ERROR! Empty input plugin class!";
		return NULL;
	}
	void *pObject = NULL;

	if (nId == 0) {
		// -- check gobal matching to class-mime (example: 'text/')
		if (sInpMime.endsWith('/')) {
			QMap <QString, ObjClass>::const_iterator it = m_subsysMimeToObjAndClassRelation.constBegin();
			while (it != m_subsysMimeToObjAndClassRelation.end()) {
				if (sInpMime.startsWith(it.key())) {
					pObject = it.value().first;
					break;
				}
				++it;
			}
		}
		else { // -- check simple matching of mime
			//pObject = m_mimeToObjectAndClassRelation[sMime].first;
			QMap <QString, ObjClass>::const_iterator it = m_subsysMimeToObjAndClassRelation.constBegin();
			// here will be found also multi-mime plugins, because every single mime is assigned with ObjClass of plugin
			while (it != m_subsysMimeToObjAndClassRelation.end()) {
				if (sPluginClass == it.value().second) {
					if (sInpMime == it.key()) {
						pObject = it.value().first;
						break;
					}
				}
				++it;
			}
			//qDebug() << "sMime" << sMime << "pObject" << (Vfs::Subsystem *)pObject;
			// -- check matching to multi-mime (plugin handles multiple mimes)
			if (pObject == NULL) {
				QStringList slMimeList = sInpMime.split(':');
				foreach(QString sMime, slMimeList) {
					//pObject = m_mimeToObjectAndClassRelation[sMime].first;
					QMap <QString, ObjClass>::const_iterator it = m_subsysMimeToObjAndClassRelation.constBegin();
					while (it != m_subsysMimeToObjAndClassRelation.end()) {
						if (sPluginClass == it.value().second) {
							if (sInpMime == it.key()) {
								pObject = it.value().first;
								break;
							}
						}
						++it;
					}
					//if (pObject != NULL)
					//	break;
				}
			}
		}
	}
	else
	if (nId > 0) {
		if (m_subsysMimeToObjAndClassRelation.count() < nId) {
			qDebug() << "Passed Id:" << nId << "exceeds the range.";
			return NULL;
		}
		int c=0;
		QMap <QString, ObjClass>::const_iterator it = m_subsysMimeToObjAndClassRelation.constBegin();
		while (it != m_subsysMimeToObjAndClassRelation.end()) {
			if (sInpMime == it.key() || (sInpMime.endsWith('/') && sInpMime.startsWith(it.key()))) {
				if (nId == c) {
					if (sPluginClass == it.value().second) {
						pObject = it.value().first;
						break;
					}
				}
				++c;
			}
			++it;
		}
	}

	if (pObject == NULL)
		qDebug() << "PluginFactory::getObject. ERROR. Cannot get pointer to object related with mime:" << sInpMime << "and class:" << sPluginClass << "Id:" << nId;

	return pObject;
}


bool PluginFactory::isPluginAvailable( const QString &sInpMime, const QString &sInpClass )
{
	QString sFN = "PluginFactory::isPluginAvailable.";
	qDebug() << sFN << "Looking for mime:" << sInpMime << "and class:" << sInpClass;
	m_sLastMatchingMime = "";
	m_sLastSearchingPluginPath = "";
	// Check if comes mime that matches to class-mime (example: 'text/') plugin.
	QString sMime, sMultiMime, sAbsPathToPlugin;
	if (sInpClass.isEmpty()) {
		QMap <QString, QString>::const_iterator it = m_mimeToPluginLocation.constBegin();
		while (it != m_mimeToPluginLocation.end()) {
			if (sInpMime.startsWith(it.key())) {
				sMime = sInpMime.left(sInpMime.indexOf('/')+1);
				//sAbsPathToPlugin = m_mimeToPluginLocation[sMime]; // original
				sAbsPathToPlugin = m_mimeToPluginLocation.values(sMime).first(); // ??? NEEDS tests
				break;
			}
			++it;
		}
		// checking for multi-mime plugin
		if (sAbsPathToPlugin.isEmpty()) {
			QMap <QString, QString>::const_iterator it = m_mimeToPluginLocation.constBegin();
			while (it != m_mimeToPluginLocation.end()) {
				sMultiMime = it.key();
				if (sMultiMime.contains(':')) { // multi-mime
					QStringList slMimeList = sMultiMime.split(':');
					foreach(sMime, slMimeList) {
						if (sMime == sInpMime) {
							//sAbsPathToPlugin = m_mimeToPluginLocation[sMultiMime]; // original
							sAbsPathToPlugin = m_mimeToPluginLocation.values(sMultiMime).first(); // ??? NEEDS tests
							if (! sAbsPathToPlugin.isEmpty())
								break;
						}
					}
				}
				++it;
			}
		}
		if (sAbsPathToPlugin.isEmpty()) {
			qDebug() << sFN << "ERROR. Cannot determine path for plugin realted to mime:" << sInpMime;
			return false;
		}
		else {
			qDebug() << sFN << "Found first matching plugin:" << sAbsPathToPlugin;
			m_sLastSearchingPluginPath = sAbsPathToPlugin;
			m_sLastMatchingMime = sMime;
		}
	}
	else { // sInpClass not empty
		bool bFound = false;
		// simple check for PluginData
		const PluginData *pPluginData = m_mimeToPluginData.value(sInpMime);
		if (pPluginData != NULL) {
			bFound = (sInpClass == pPluginData->plugin_class);
			sMultiMime = sInpMime; // to be able take correct path for plugin from m_mimeToPluginLocation map
		}
		if (! bFound) { // checking single meta-mime
			QMap <QString, PluginData *>::const_iterator it = m_mimeToPluginData.constBegin();
			while (it != m_mimeToPluginData.end()) {
				sMime = it.key();
				if (! sMime.contains(':') && ! sMime.endsWith('/')) {
					if (sInpMime.indexOf(QRegExp(sMime, Qt::CaseSensitive, QRegExp::Wildcard)) == 0) { // or matching sth like this: text/*
						pPluginData = m_mimeToPluginData.value(sMime);
						if (pPluginData != NULL) {
							if (sInpClass == pPluginData->plugin_class) {
								sMultiMime = sMime; // to be able take correct path for plugin from m_mimeToPluginLocation map
								bFound = true;
								break;
							}
						}
					}
				}
				++it;
			}
		}
		if (! bFound) { // checking multi-mime and meta-mime inside multi-mime for PluginData
			QString sMetaMime;
			QStringList slMimeList;
			QMap <QString, PluginData *>::const_iterator it = m_mimeToPluginData.constBegin();
			while (it != m_mimeToPluginData.end()) {
				sMultiMime = it.key();
				if (sMultiMime.contains(':')) {
					slMimeList = sMultiMime.split(':');
					foreach(sMime, slMimeList) {
						if (sMime == sInpMime || sInpMime.indexOf(QRegExp(sMime, Qt::CaseSensitive, QRegExp::Wildcard)) == 0) { // or matching sth like this: text/*
							pPluginData = m_mimeToPluginData.value(sMultiMime);
							if (pPluginData != NULL) {
								if (sInpClass == pPluginData->plugin_class) {
									bFound = true;
									break;
								}
							}
						}
					}
				}
				else // check for meta mime
				if (sMultiMime.endsWith('/')) {
					sMetaMime = sInpMime.left(sInpMime.indexOf('/')+1);
					if (sMultiMime == sMetaMime) {
						pPluginData = m_mimeToPluginData.value(sMultiMime);
						if (pPluginData != NULL) {
							if (sInpClass == pPluginData->plugin_class) {
								bFound = true;
								break;
							}
						}
					}
				}
				if (bFound)
					break;
				++it;
			}
		}
		if (bFound) {
			m_sLastSearchingPluginPath = m_mimeToPluginLocation.value(sMultiMime);
			m_sLastMatchingMime = sMultiMime;
			qDebug() << sFN << "Found matching plugin:" << m_sLastSearchingPluginPath;
		}
		else {
			qDebug() << sFN << "WARNING. Cannot find PluginData object for mime:" << sInpMime << "in class:" << sInpClass;
			return false;
		}
	}

	return true;
}


QStringList PluginFactory::getPluginsDataInfo( const QString &sPluginClass ) const
{
	QStringList slPluginData;
	if (sPluginClass.isEmpty())
		return slPluginData;

	PluginData *pPD;
	QString sPluginData;
	QMap <QString, PluginData *>::const_iterator it = m_mimeToPluginData.constBegin();
	while (it != m_mimeToPluginData.end()) {
		pPD = it.value();
		if (pPD->plugin_class == sPluginClass) {
			sPluginData = pPD->plugin_class + "|" + pPD->name + "|" + pPD->version + "|" + pPD->description + "|" + pPD->mime_type + "|" + pPD->protocol + "|" + pPD->file_ext;
			slPluginData << sPluginData;
		}
		++it;
	}

	return slPluginData;
}


void PluginFactory::updateMimeConfig( const QString &sPluginName, const QString &sOldMimeLst, const QString &sNewMimeLst )
{
	QString sFN = "PluginFactory::updateMimeConfig.";
	qDebug() << sFN << "PluginName:" << sPluginName << " OldMimeLst:" << sOldMimeLst << " NewMimeLst:" << sNewMimeLst;

	QList <PluginData *> pPluginDataLst = m_mimeToPluginData.values(sOldMimeLst);
	foreach (PluginData *pPluginData, pPluginDataLst) {
		if (pPluginData->name.startsWith(sPluginName)) {
			pPluginData->mime_type = sNewMimeLst;
			m_mimeToPluginData.remove(sOldMimeLst);
			m_mimeToPluginData.insert(sNewMimeLst, pPluginData);
			qDebug() << sFN << "Updated m_mimeToPluginData";
			break;
		}
	}
	QList <QString > sPluginLocationLst = m_mimeToPluginLocation.values(sOldMimeLst);
	foreach (const QString &sPluginLocation, sPluginLocationLst) {
		if (sPluginLocation.contains("/"+sPluginName+"/")) {
			m_mimeToPluginLocation.remove(sOldMimeLst);
			m_mimeToPluginLocation.insert(sNewMimeLst, sPluginLocation);
			qDebug() << sFN << "Updated m_mimeToPluginLocation";
			break;
		}
	}
	if (sPluginName.endsWith("subsystem")) {
		ObjClass objClass = m_subsysMimeToObjAndClassRelation.value(sOldMimeLst);
		m_subsysMimeToObjAndClassRelation.remove(sOldMimeLst);
		// Does plugin support multi-mime?
		if (sNewMimeLst.indexOf(':') != -1) { // yes (for example: TarSubsystem), so need to insert all relations to plugin
			QStringList slMimeList = sNewMimeLst.split(':');
			foreach (const QString &sMime, slMimeList) {
				qDebug() << sFN << "Set object relation plugin:" << sPluginName << "with mime:" << sMime;
				m_subsysMimeToObjAndClassRelation.insert(sMime, objClass);
			}
		}
		else { // Doesn't, only set relation one time.
			qDebug() << sFN << "Set object relation plugin:" << sPluginName << "with mime:" << sNewMimeLst;
			m_subsysMimeToObjAndClassRelation.insert(sNewMimeLst, objClass);
		}
	}
}

