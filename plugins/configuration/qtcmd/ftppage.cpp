/***************************************************************************
 *   Copyright (C) 2007 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "settings.h"
#include "ftppage.h"


FtpPage::FtpPage()
{
	setupUi(this);

	init();
}

void FtpPage::init()
{
	Settings settings;

	m_pStandByConnectionChkBox->setChecked(settings.value("FTP/StandByConnection", true).toBool());
	int  nNumOfTimeToRetryIfBusy = settings.value("FTP/NumOfRetriesIfBusy", 10).toInt();
	if (nNumOfTimeToRetryIfBusy < 0)
		nNumOfTimeToRetryIfBusy = 0;
	m_pNumOfRetriesIfBusySpinBox->setValue(nNumOfTimeToRetryIfBusy);

	m_pCloseProgressDlgAftterDownlFinished-> setChecked(settings.value("vfs/s_bCloseProgressDlgWhenDownloadFinished",   false).toBool());
	m_pCloseProgressDlgAftterUploadFinished-> setChecked(settings.value("vfs/s_bCloseProgressDlgWhenUploadFinished",   false).toBool());
}

int FtpPage::numOfTimeToRetryIfBusy()
{
	return m_pNumOfRetriesIfBusySpinBox->value();
}

void FtpPage::setDefaults()
{
	m_pStandByConnectionChkBox->setChecked(true);
	m_pNumOfRetriesIfBusySpinBox->setValue(10);

	m_pCloseProgressDlgAftterDownlFinished-> setChecked(false);
	m_pCloseProgressDlgAftterUploadFinished->setChecked(false);
}


void FtpPage::slotSave()
{
	Settings settings;

	settings.setValue("FTP/StandByConnection", m_pStandByConnectionChkBox->isChecked());
	settings.setValue("FTP/NumOfRetriesIfBusy", m_pNumOfRetriesIfBusySpinBox->value());

	settings.setValue("vfs/s_bCloseProgressDlgWhenDownloadFinished",  m_pCloseProgressDlgAftterDownlFinished->isChecked());
	settings.setValue("vfs/s_bCloseProgressDlgWhenUploadFinished",  m_pCloseProgressDlgAftterUploadFinished->isChecked());
}

