/***************************************************************************
 *   Copyright (C) 2018 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _QTCMDCONFIGURATIONDIALOG_H
#define _QTCMDCONFIGURATIONDIALOG_H

#include <QDialog>

#include "fileslistcolorspage.h"
#include "configurationdialog.h"

#include "fontchooserpage.h"
#include "fileslistcolumnspage.h"
#include "fileslistothersettings.h"

#include "lfspage.h"
#include "ftppage.h"
#include "archivespage.h"
#include "filesystemsothersettings.h"

#include "keyshortcutspage.h"
#include "toolbarpage.h"

#include "plugindata.h"
#include "otherappsettings.h"
#include "filesystemsettings.h"

#include "ui_qtcmdconfigurationdlg.h"

/**
	@author Piotr Mierzwiński
*/
class QtCmdConfigurationDialog : public ConfigurationDialog, public Ui::QtCmdConfigurationDlg
{
    Q_OBJECT
private:
	PluginData       *m_pPluginData;

	QDialog          *m_pDialog;

	FontChooserPage        *m_pFontPage;
	FilesListColorsPage    *m_pColorsPage;
	FilesListColumnsPage   *m_pColumnsPage;
	FilesListOtherSettings *m_pFilesListOtherSettings;

	LfsPage          *m_pLfsPage;
	FtpPage          *m_pFtpPage;
	ArchivesPage     *m_pArchivesPage;

	FileSystemsOtherSettings *m_pFileSystemsOtherSettings;
	QWidget          *m_pEmptyPage;

	KeyShortcutsPage *m_pKeyshortcutsPage;
	ToolBarPage      *m_pToolBarPage;

	int m_nListViewPagesCount, m_nFileSystemsPagesCount, m_nOtherPagesCount; // need to be treated as const

	void showCurrentPage( const QString &sSectionName );

public:
	QtCmdConfigurationDialog();
	~QtCmdConfigurationDialog();

	PluginData *pluginData() { return m_pPluginData; }

	void init( QWidget *pParent, KeyShortcuts *pKeyShortcuts, QList <QActionGroup *> *pActionGroupLst );

	/** Shows this window.
	 * Method is called in MainWindow to show configuration dialog
	 * @param sSectionName name of section to activate
	 */
	void raiseWindow( const QString &sSectionName );

private Q_SLOTS:
	//"Cancel" button is handled inside ui file
	void slotOkButtonPressed();
	void slotApplyButtonPressed();
	void slotSaveButtonPressed();
	void slotDefaultsButtonPressed();

	void slotSectionChanged( int nItemId );
	void slotShowPageInSection( int nItemId );
// 	void slotSetKeyShortcuts( KeyShortcuts *pKeyShortcuts );

};

#endif // _QTCMDCONFIGURATIONDIALOG_H
