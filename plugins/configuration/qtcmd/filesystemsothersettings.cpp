/***************************************************************************
 *   Copyright (C) 2007 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "settings.h"
#include "filesystemsothersettings.h"


FileSystemsOtherSettings::FileSystemsOtherSettings()
{
	setupUi(this);

	init();
}

void FileSystemsOtherSettings::init()
{
	Settings settings;

	m_pAlwaysSavePermissionChkBox->setChecked(settings.value("vfs/AlwaysSavePermission", true).toBool());
	m_pAlwaysOverwriteInCaseCpMvRmChkBox->setChecked(settings.value("vfs/AlwaysOverwriteInCaseCpMvRm", false).toBool());
	m_pAskBeforeDeleteChkBox->setChecked(settings.value("vfs/AskBeforeDelete", true).toBool());
	m_pOpenDirAfterCreationItChkBox->setChecked(settings.value("vfs/AlwaysGetInToDir", false).toBool());
}

void FileSystemsOtherSettings::setDefaults()
{
	m_pAskBeforeDeleteChkBox->setChecked(true);
	m_pAlwaysOverwriteInCaseCpMvRmChkBox->setChecked(true);
	m_pAlwaysSavePermissionChkBox->setChecked(true);
	m_pOpenDirAfterCreationItChkBox->setChecked(false);

// 	m_pCloseProgressDlgAfterFinished->setChecked(false);
}


void FileSystemsOtherSettings::slotSave()
{
	Settings settings;

	settings.value("vfs/AlwaysOverwriteInCaseCpMvRm", m_pAlwaysOverwriteInCaseCpMvRmChkBox->isChecked());
	settings.setValue("vfs/AskBeforeDelete", m_pAskBeforeDeleteChkBox->isChecked());
	settings.setValue("vfs/AlwaysSavePermission", m_pAlwaysSavePermissionChkBox->isChecked());
	settings.setValue("vfs/AlwaysGetInToDir", m_pOpenDirAfterCreationItChkBox->isChecked());
}

