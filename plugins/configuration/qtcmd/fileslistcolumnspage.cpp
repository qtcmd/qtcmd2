/***************************************************************************
 *   Copyright (C) 2007 by Piotr Mierzwiñski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QtGui>
#include <QButtonGroup>

#include "fileslistcolumnspage.h"
#include "settings.h"


FilesListColumnsPage::FilesListColumnsPage()
{
	setupUi(this);

	enum ColumnNames {  // TODO use ColumnNames (id) from some namespace
		NAMEcol=0, SIZEcol, TIMEcol, PERMcol, OWNERcol, GROUPcol, FTYPEcol
		//NAMEcol=0, EXTNAMEcol, TIMEcol, PERMcol, OWNERcol, GROUPcol
	};
	m_pColumnsButtonGroup = new QButtonGroup;
	m_pColumnsButtonGroup->setExclusive(false);
	m_pColumnsButtonGroup->addButton(m_pNameChkBox,  NAMEcol);
	//m_pColumnsButtonGroup->addButton(m_pNameExtChkBox, EXTNAMEcol); // not yet supported TODO add support for extention column
	m_pColumnsButtonGroup->addButton(m_pSizeChkBox,  SIZEcol);
	m_pColumnsButtonGroup->addButton(m_pTimeChkBox,  TIMEcol);
	m_pColumnsButtonGroup->addButton(m_pPermChkBox,  PERMcol);
	m_pColumnsButtonGroup->addButton(m_pOwnerChkBox, OWNERcol);
	m_pColumnsButtonGroup->addButton(m_pGroupChkBox, GROUPcol);
	m_pColumnsButtonGroup->addButton(m_pFileTypeChkBox, FTYPEcol);

	init();
}

FilesListColumnsPage::~FilesListColumnsPage()
{
	delete m_pColumnsButtonGroup;
}


void FilesListColumnsPage::init()
{
	Settings settings;
	const QStringList slAllColumns = QString("0-170,1-0,2-70,3-107,4-70,5-55,6-55").split(','); // with ext.column TODO support for extention column
	QString sColumns = settings.value("filelistview/CL_Columns", "0-170,1-70,2-107,3-70,4-55,5-55,6-55").toString();

	// --- read info from config file
	bool bColumnsSynchronization = settings.value("filelistview/SynchronizeColumns", true).toBool();
	QStringList slColumns;
	if (sColumns.isEmpty() )
		slColumns = slAllColumns;
	else
		slColumns = sColumns.split(',');

// 	bool bShowExtention = true;
	int nColumnWidth;
	QCheckBox *pColumnVisible;
	for (int i=0; i<slAllColumns.count(); i++) {
		//pColumnVisible = (QCheckBox *)m_pShowColumnsGrpBox->find(i);
		pColumnVisible = (QCheckBox *)m_pColumnsButtonGroup->button(i);
		if (slColumns[i].section('-', 0, 0 ) == QString("%1").arg(i)) { // current column is visible
			nColumnWidth = (slColumns[i].section('-', -1 )).toInt();
			if (nColumnWidth > 0 && nColumnWidth < 10) {
				nColumnWidth = (slAllColumns[i].section('-', -1 )).toInt();
			}
		}
		else
			nColumnWidth = 0;

// 		if (i == 1 && nColumnWidth < 1)
// 			bShowExtention = false;

		m_ntInitColumnsWidth[i] = nColumnWidth;
		m_ntAlltColumnsWidth[i] = (slAllColumns[i].section('-', -1 )).toInt();
		pColumnVisible->setChecked( (nColumnWidth > 0) );
	}

	m_pSynchronizeColsWidthChkBox->setChecked(bColumnsSynchronization);
}

int FilesListColumnsPage::columnWidth( int nLogicId )
{
	if (nLogicId < 0 || nLogicId > MAX_COLUMNS_IN_CFL+1)
		return 0;
	if (m_pColumnsButtonGroup->button(nLogicId) == nullptr)
		return 0;

	int nColumnWidth;
	if (m_pColumnsButtonGroup->button(nLogicId)->isChecked()) {
		nColumnWidth = m_ntInitColumnsWidth[nLogicId];
		if (nColumnWidth == 0) // if new column turned on then take default width
			nColumnWidth = m_ntAlltColumnsWidth[nLogicId];
	}
	else
		nColumnWidth = 0; // hide column

	return nColumnWidth;
}

void FilesListColumnsPage::setDefaults()
{
	m_pNameChkBox->setChecked(true);
// 	m_pNameExtChkBox->setChecked(false); // with ext.column TODO support for extention column
	m_pSizeChkBox->setChecked(true);
	m_pTimeChkBox->setChecked(true);
	m_pPermChkBox->setChecked(true);
	m_pOwnerChkBox->setChecked(true);
	m_pGroupChkBox->setChecked(true);
	m_pFileTypeChkBox->setChecked(true);

	m_pSynchronizeColsWidthChkBox->setChecked(true);
//	m_pSortColBtnGroup->button(0)->setChecked(true);

	m_pNameChkBox_2->setChecked(true);
	m_pSizeChkBox_2->setChecked(true);
	m_pPathChkBox->setChecked(true);
	m_pTimeChkBox_2->setChecked(false);
	m_pPermChkBox_2->setChecked(false);
	m_pOwnerChkBox_2->setChecked(false);
	m_pGroupChkBox_2->setChecked(false);
	m_pFileTypeChkBox_2->setChecked(false);
}


void FilesListColumnsPage::slotSave()
{
	//qDebug() << "FilesListColumnsPage::slotSave";
	Settings settings;
	settings.setValue("filelistview/SynchronizeColumns", m_pSynchronizeColsWidthChkBox->isChecked());
	QCheckBox *pColumnVisibility;
	QString sColumns;
	int nColumnWidth;

	for (uint nLogicId=0; nLogicId < MAX_COLUMNS_IN_CFL; nLogicId++) {
		pColumnVisibility = (QCheckBox *)m_pColumnsButtonGroup->button(nLogicId);
		if (pColumnVisibility->isChecked()) {
			nColumnWidth = m_ntInitColumnsWidth[nLogicId];
			if (nColumnWidth == 0) // if new column turned on then take default width
				nColumnWidth = m_ntAlltColumnsWidth[nLogicId];
		}
		else
			nColumnWidth = 0; // hide column
		sColumns.append(QString("%1-%2,").arg(nLogicId).arg(nColumnWidth));
	}
	sColumns = sColumns.left(sColumns.length()-1);
	settings.setValue("filelistview/CL_Columns", sColumns); // Common List Columns
}
