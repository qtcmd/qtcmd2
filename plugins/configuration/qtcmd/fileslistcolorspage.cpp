/***************************************************************************
 *   Copyright (C) 2007 by Piotr Mierzwi?ski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QtGui>
#include <QLineEdit>
#include <QCheckBox>
#include <QColorDialog>
#include <QInputDialog>
#include <QTreeWidgetItemIterator>
#include <QTreeWidgetItem>

#include "messagebox.h"
#include "fileslistcolorspage.h"
#include "settings.h"


FilesListColorsPage::FilesListColorsPage()
{
	setupUi(this);

	connect(m_pNewSchemeBtn,    &QPushButton::clicked, this, &FilesListColorsPage::slotNew);
	connect(m_pDeleteSchemeBtn, &QPushButton::clicked, this, &FilesListColorsPage::slotDelete);
	connect(m_pSaveSchemeBtn,   &QPushButton::clicked, this, &FilesListColorsPage::slotSave);
	connect<void(QComboBox::*)(int)>(m_pSchemeComboBox, &QComboBox::activated, this, &FilesListColorsPage::slotChangedScheme);

	init();
}


void FilesListColorsPage::init()
{
// 	QPalette sysPalette = m_tvFileList->palette();
// 	QColor systemBgCol    = sysPalette.window().color();
// 	QColor systemSecBgCol = sysPalette.alternateBase().color();
// 	QColor systemItemCol  = sysPalette.windowText().color();
// 	QColor systemCursorCol = sysPalette.highlight().color();
	m_pIconCache = IconCache::instance();

	m_pNewSchemeBtn->setText("");
	m_pNewSchemeBtn->setIcon(icon("document/new"));
	m_pDeleteSchemeBtn->setText("");
	m_pDeleteSchemeBtn->setIcon(icon("edit/delete"));
	m_pSaveSchemeBtn->setText("");
	m_pSaveSchemeBtn->setIcon(icon("document/save"));

	m_pTreeWidget->header()->hide();

	// --- load settings (into conatainer: ColorsScheme)
	m_cslSchemesList.clear();
	ColorsScheme colorsScheme;
	QString sScheme, sSchemeName, sColor;
	Settings settings;
	int nId = 0;

	// --  load schemes in order of appearing in config file
	while ((sScheme = settings.flvSchemeName(true, nId)) != "") {
		sSchemeName = sScheme;
		colorsScheme.name = sSchemeName.remove("flvStyle_");

		//colorsScheme.bShowGrid = false;// = settings.readBoolEntry("/qtcmd/"+sScheme+"/ShowGrid", false);
		colorsScheme.bAlternatingRowColors =  settings.value(sScheme+"/AlternatingRowColors", true).toBool();

		sColor = settings.value(sScheme+"/ItemColor", "#000000").toString();
		colorsScheme.itemColor = QColor(sColor);

		sColor = settings.value(sScheme+"/ItemColorUnderCursor", "#0000EA").toString();
		colorsScheme.itemColorUnderCursor = QColor(sColor);

		sColor = settings.value(sScheme+"/ColorOfSelectedItem", "#FF0000").toString();
		colorsScheme.colorOfSelectedItem = QColor(sColor);

		sColor = settings.value(sScheme+"/ColorOfSelectedItemUnderCursor", "#FF0000").toString();
		colorsScheme.colorOfSelectedItemUnderCursor = QColor(sColor);

// 		sColor = settings.value(sScheme+"/BackgroundColorOfSelectedItem", "#FFFFFF").toString();
// 		colorsScheme.backgroundColorOfSelectedItem = QColor(sColor);

// 		sColor = settings.value(sScheme+"/BackgroundColorOfSelectedItemUnderCursor", "#FFFFFF").toString();
// 		colorsScheme.backgroundColorOfSelectedItemUnderCursor = QColor(sColor);

		sColor = settings.value(sScheme+"/BackgroundColor", "#FFFFFF").toString();
		colorsScheme.backgroundColor = QColor(sColor);

		sColor = settings.value(sScheme+"/SecondBackgroundColor", "#FFEBDC").toString();
		colorsScheme.secondBackgroundColor = QColor(sColor);

		sColor = settings.value(sScheme+"/CursorColor", "#00008B").toString();
		colorsScheme.cursorColor = QColor(sColor);

		sColor = settings.value(sScheme+"/HiddenFileColor", "#A0A0A0").toString();
		colorsScheme.hiddenFileColor = QColor(sColor);

		sColor = settings.value(sScheme+"/ExecutableFileColor", "#008000").toString();
		colorsScheme.executableFileColor = QColor(sColor);

		sColor = settings.value(sScheme+"/SymlinkColor", "#808080").toString();
		colorsScheme.symlinkColor = QColor(sColor);

		sColor = settings.value(sScheme+"/BrokenSymlinkColor", "#800000").toString();
		colorsScheme.brokenSymlinkColor = QColor(sColor);

// 		sColor = settings.value(sScheme+"/GridColor", "#A9A9A9").toString();
// 		colorsScheme.gridColor = QColor(sColor);

		m_cslSchemesList.append(colorsScheme);
		m_pSchemeComboBox->addItem(settings.flvSchemeName(false, nId));
		++nId;
	}
// 	m_nAmountOfSchemes = m_pSchemeComboBox->count();
	m_pSchemeComboBox->setCurrentIndex(m_pSchemeComboBox->findText(settings.flvSchemeName(false)));
	m_bAlwaysAskBeforeDeleteScheme = settings.value("filelistviewStyles/AlwaysAskBeforeDeleteScheme", true).toBool();

	// --- expand all branches
	nId = 0;
	int nTopLvlItems = m_pTreeWidget->model()->rowCount();
	m_nAllChildren = 0;
	if (nTopLvlItems > 0) {
		QTreeWidgetItemIterator it(m_pTreeWidget);
		while (nId < nTopLvlItems) {
			if ((*it)->parent() == nullptr) {
				m_nAllChildren += (*it)->childCount();
				(*it)->setExpanded(true);
				nId++;
			}
			++it;
		}
	}
	m_nAllChildren += nTopLvlItems;

	// --- set first column as half width of view
	m_pTreeWidget->header()->resizeSection(m_pTreeWidget->header()->logicalIndex(0), (m_pTreeWidget->window()->width()/2)+20);
	m_pTreeWidget->header()->setDefaultAlignment(Qt::AlignCenter);
	m_pTreeWidget->setRootIsDecorated(false);
	//m_pTreeWidget->setItemsExpandable(false);

	// --- add all widgets
	nId = 0;
	int nSchemeId = m_pSchemeComboBox->currentIndex(); //m_pSchemeComboBox->findText(m_pSchemeComboBox->currentText());
	if (nSchemeId < 0)
		nSchemeId = 0;

	QCheckBox *pChkBox = new QCheckBox(this);
	pChkBox->setText(tr("Second Background color"));
	pChkBox->setFocusPolicy(Qt::NoFocus);
	pChkBox->setChecked(m_cslSchemesList[nSchemeId].bAlternatingRowColors);
	connect(pChkBox, &QCheckBox::stateChanged, this, &FilesListColorsPage::slotShowSecondBgColor);

	QString sBtnName = "Color_";
	QPalette palette;
	QPushButton *pPushBtn;
	QTreeWidgetItemIterator it2(m_pTreeWidget);
	while (nId < m_nAllChildren) {
		if ((*it2)->parent() == nullptr && nId != CursorColor) {
			++nId;
			++it2;
			continue;
		}
		//(*it2)->setTextAlignment(1, Qt::AlignHCenter | Qt::AlignVCenter); // doesn't work for widget
		//(*it2)->setTextAlignment(2, Qt::AlignHCenter | Qt::AlignVCenter); // doesn't work for widget

		pPushBtn = new QPushButton(this);
		pPushBtn->setObjectName(sBtnName+QString::number(nId));
		pPushBtn->setMaximumHeight(pPushBtn->height()-4);
		pPushBtn->setMaximumWidth(pPushBtn->width()/2);
		pPushBtn->setFocusPolicy(Qt::NoFocus);
// 		pPushBtn->setFlat(true);
// 		pPushBtn->setAutoFillBackground(true);
		connect(pPushBtn, &QPushButton::clicked, this, &FilesListColorsPage::slotChangeColor);

		m_pTreeWidget->setItemWidget((*it2), 1, pPushBtn);

		if (nId == SecondBackgroundColor) {
			(*it2)->setText(0, "");
			m_pTreeWidget->setItemWidget((*it2), 0, pChkBox);
		}
		++nId;
		++it2;
	}

	slotChangedScheme(m_pSchemeComboBox->currentIndex());
}

bool FilesListColorsPage::isSecondBackgroundColor()
{
	return m_cslSchemesList[m_pSchemeComboBox->currentIndex()].bAlternatingRowColors;
}

void FilesListColorsPage::drawPreview()
{
// 	int nCurrSchemeId = m_pSchemeComboBox->currentIndex();
	bool bSecondBgCol = m_cslSchemesList[m_pSchemeComboBox->currentIndex()].bAlternatingRowColors;
// 	bool bShowGrid    = m_pShowGridChkBox->isChecked();

	const int nFontHeight = 12;
	const int nTopMargin  = 2;
	const int nCursorHeight = nFontHeight + 10;
	const int nPreviewWidth = m_pPreviewLabel->width()-4, nPreviewHeight = m_pPreviewLabel->height()-2;
	const int nMaxLineNum   = (nPreviewHeight-nTopMargin) / nFontHeight; // == 7

	QPixmap screen(nPreviewWidth, nPreviewHeight);
	screen.fill(getColor(BackgroundColor));

	QPainter painter(&screen);
	painter.fillRect(0,0, nPreviewWidth, nPreviewHeight, painter.brush());

	if (bSecondBgCol) { // draws two-colors background
		QColor color;
		for (int i=0; i<nMaxLineNum; i++) {
			color = i%2 ? getColor(SecondBackgroundColor) : getColor(BackgroundColor);
			painter.fillRect(0, nTopMargin+(i*nCursorHeight), nPreviewWidth, nCursorHeight, color);
		}
	}
	// -- draws a grid
// 	if (bShowGrid) {
// 		painter.setPen(getColor(GridColor));
// 		for (int i=0; i<nMaxLineNum; i++)
// 			painter.drawLine(0, nTopMargin+(i*nCursorHeight), nPreviewWidth, nTopMargin+(i*nCursorHeight));
// 	}
	int nXpos = 2, nYpos = nTopMargin;
	// -- draws the first line
	nYpos += nFontHeight + nTopMargin;
	painter.setPen(getColor(ItemColor));
	painter.drawText(nXpos, nYpos+2, tr("TextNoCursor"));

	// -- draws a second line
	painter.fillRect(0, nTopMargin+(1*nCursorHeight), nPreviewWidth, nTopMargin+(1*nCursorHeight), getColor(CursorColor));

	nYpos += nTopMargin+(1*nCursorHeight);
	painter.setPen(getColor(ItemColorUnderCursor));
	painter.drawText(nXpos, nYpos, tr("TextUnderCursor"));

	// -- draws the third line
// 	nYpos+=1;
// 	painter.fillRect(0, nTopMargin+(2*nCursorHeight), nPreviewWidth, nTopMargin+(1*nCursorHeight), getColor(BackgroundColorOfSelectedItem));
// 	nYpos += nCursorHeight;
// 	painter.setPen(getColor(ColorOfSelectedItem));
// 	painter.drawText(nXpos, nYpos, tr("Selection"));

	// -- draws the fourth line
// 	painter.fillRect(0, nTopMargin+(3*nCursorHeight), nPreviewWidth, nTopMargin+(1*nCursorHeight), getColor(BackgroundColorOfSelectedItemUnderCursor));

	nYpos = (4*nCursorHeight);
	painter.setPen(getColor(ColorOfSelectedItem));
	painter.drawText(nXpos, nYpos-2, tr("SelectedText"));

 	painter.fillRect(0, nYpos+2, nPreviewWidth, nTopMargin+(1*nCursorHeight), getColor(CursorColor));
	nYpos -= nTopMargin;
	nYpos += (nCursorHeight-nTopMargin);
	painter.setPen(getColor(ColorOfSelectedItemUnderCursor));
	painter.drawText(nXpos, nYpos+2, tr("SelUnderCursor"));

	nYpos += (nCursorHeight-nTopMargin);
	painter.setPen(getColor(ItemColor));
	painter.drawText(nXpos, nYpos+2, tr("TextNoCursor"));

	m_pPreviewLabel->setPixmap(screen);
}

void FilesListColorsPage::updateCurrentScheme( int nColorId, QColor color )
{
	int nSchemeId = m_pSchemeComboBox->currentIndex();

	if (nColorId == ItemColor)
		m_cslSchemesList[nSchemeId].itemColor = color;
	else
	if (nColorId == ItemColorUnderCursor)
		m_cslSchemesList[nSchemeId].itemColorUnderCursor = color;
	else
	if (nColorId == ColorOfSelectedItem)
		m_cslSchemesList[nSchemeId].colorOfSelectedItem = color;
	else
	if (nColorId == ColorOfSelectedItemUnderCursor)
		m_cslSchemesList[nSchemeId].colorOfSelectedItemUnderCursor = color;
	else
// 	if (nColorId == BackgroundColorOfSelectedItem)
// 		m_cslSchemesList[nSchemeId].backgroundColorOfSelectedItem = color;
// 	else
// 	if (nColorId == BackgroundColorOfSelectedItemUnderCursor)
// 		m_cslSchemesList[nSchemeId].backgroundColorOfSelectedItemUnderCursor = color;
// 	else
	if (nColorId == BackgroundColor)
		m_cslSchemesList[nSchemeId].backgroundColor = color;
	else
	if (nColorId == SecondBackgroundColor)
		m_cslSchemesList[nSchemeId].secondBackgroundColor = color;
	else
	if (nColorId == CursorColor)
		m_cslSchemesList[nSchemeId].cursorColor = color;
	else
	if (nColorId == HiddenFileColor)
		m_cslSchemesList[nSchemeId].hiddenFileColor = color;
	else
	if (nColorId == ExecutableFileColor)
		m_cslSchemesList[nSchemeId].executableFileColor = color;
	else
	if (nColorId == SymlinkColor)
		m_cslSchemesList[nSchemeId].symlinkColor = color;
	else
	if (nColorId == BrokenSymlinkColor)
		m_cslSchemesList[nSchemeId].brokenSymlinkColor = color;
}

void FilesListColorsPage::updateCurrentScheme( bool bAlternatingRowColors )
{
	int nSchemeId = m_pSchemeComboBox->currentIndex();

	if (nSchemeId > 0) // first is read only
		m_cslSchemesList[nSchemeId].bAlternatingRowColors = bAlternatingRowColors;
}


QColor FilesListColorsPage::getColor( int nColorId, SourceOfColor srcOfColor, const QString &sSchemeName ) const
{
	if (nColorId > BrokenSymlinkColor) {
		qDebug() << "FilesListColorsPage::getColor. Id color over the range (0 - 15)!";
		return QColor(0, 0, 0);
	}
	QColor color(0, 0, 0);

	if (srcOfColor == ConfigSrc) {
		QString sScheme = sSchemeName;
		if (sScheme.isEmpty())
			sScheme = "flvStyle_"+m_pSchemeComboBox->currentText();

		Settings settings;
		if (nColorId == ItemColor)
			color.setNamedColor(settings.value(sScheme+"/ItemColor", "#000000").toString());
		else
		if (nColorId == ItemColorUnderCursor)
			color.setNamedColor(settings.value(sScheme+"/ItemColorUnderCursor", "#000000").toString());
		else
		if (nColorId == ColorOfSelectedItem)
			color.setNamedColor(settings.value(sScheme+"/ColorOfSelectedItem", "#000000").toString());
		else
		if (nColorId == ColorOfSelectedItemUnderCursor)
			color.setNamedColor(settings.value(sScheme+"/ColorOfSelectedItemUnderCursor", "#000000").toString());
		else
// 		if (nColorId == BackgroundColorOfSelectedItem)
// 			color.setNamedColor(settings.value(sScheme+"/BackgroundColorOfSelectedItem", "#000000").toString());
// 		else
// 		if (nColorId == BackgroundColorOfSelectedItemUnderCursor)
// 			color.setNamedColor(settings.value(sScheme+"/BackgroundColorOfSelectedItemUnderCursor", "#000000").toString());
// 		else
		if (nColorId == BackgroundColor)
			color.setNamedColor(settings.value(sScheme+"/BackgroundColor", "#000000").toString());
		else
		if (nColorId == SecondBackgroundColor)
			color.setNamedColor(settings.value(sScheme+"/SecondBackgroundColor", "#000000").toString());
		else
		if (nColorId == CursorColor)
			color.setNamedColor(settings.value(sScheme+"/CursorColor", "#000000").toString());
		else
		if (nColorId == HiddenFileColor)
			color.setNamedColor(settings.value(sScheme+"/HiddenFileColor", "#000000").toString());
		else
		if (nColorId == ExecutableFileColor)
			color.setNamedColor(settings.value(sScheme+"/ExecutableFileColor", "#000000").toString());
		else
		if (nColorId == SymlinkColor)
			color.setNamedColor(settings.value(sScheme+"/SymlinkColor", "#000000").toString());
		else
		if (nColorId == BrokenSymlinkColor)
			color.setNamedColor(settings.value(sScheme+"/BrokenSymlinkColor", "#000000").toString());
	}
	else
	if (srcOfColor == WidgetSrc) {
		QTreeWidgetItem *pItem;
		QTreeWidgetItemIterator it2(m_pTreeWidget);
		int i=0;
		while (i < m_nAllChildren) {
			pItem = (*it2);
			if (pItem->parent() == nullptr) {
				++i;
				++it2;
				continue;
			}
			if (i == nColorId)
				break;
			++i;
			++it2;
		}
		QPushButton *pBtn = (QPushButton *)m_pTreeWidget->itemWidget(pItem, 1);
		color = pBtn->palette().button().color();
		//qDebug() << "FilesListColorsPage::getColor, nColorId:" << nColorId << "color:" << bgColor.name();
	}
	else
	if (srcOfColor == StructSrc) {
		// --- get color from scheme
		QString sScheme = sSchemeName;
		int nSchemeId;
		if (sScheme.isEmpty())
			sScheme = m_pSchemeComboBox->currentText();
		if ((nSchemeId=m_pSchemeComboBox->findText(sScheme)) < 0) {
			qDebug() << "FilesListColorsPage::getColor. Cannot find given scheme name:" << sScheme;
			return QColor(0, 0, 0);
		}
		if (nColorId == ItemColor)
			color = m_cslSchemesList[nSchemeId].itemColor;
		else
		if (nColorId == ItemColorUnderCursor)
			color = m_cslSchemesList[nSchemeId].itemColorUnderCursor;
		else
		if (nColorId == ColorOfSelectedItem)
			color = m_cslSchemesList[nSchemeId].colorOfSelectedItem;
		else
		if (nColorId == ColorOfSelectedItemUnderCursor)
			color = m_cslSchemesList[nSchemeId].colorOfSelectedItemUnderCursor;
		else
// 		if (nColorId == BackgroundColorOfSelectedItem)
// 			color = m_cslSchemesList[nSchemeId].backgroundColorOfSelectedItem;
// 		else
// 		if (nColorId == BackgroundColorOfSelectedItemUnderCursor)
// 			color = m_cslSchemesList[nSchemeId].backgroundColorOfSelectedItemUnderCursor;
// 		else
		if (nColorId == BackgroundColor)
			color = m_cslSchemesList[nSchemeId].backgroundColor;
		else
		if (nColorId == SecondBackgroundColor)
			color = m_cslSchemesList[nSchemeId].secondBackgroundColor;
		else
		if (nColorId == CursorColor)
			color = m_cslSchemesList[nSchemeId].cursorColor;
		else
		if (nColorId == HiddenFileColor)
			color = m_cslSchemesList[nSchemeId].hiddenFileColor;
		else
		if (nColorId == ExecutableFileColor)
			color = m_cslSchemesList[nSchemeId].executableFileColor;
		else
		if (nColorId == SymlinkColor)
			color = m_cslSchemesList[nSchemeId].symlinkColor;
		else
		if (nColorId == BrokenSymlinkColor)
			color = m_cslSchemesList[nSchemeId].brokenSymlinkColor;
	}

	return color;
}

ColorsScheme & FilesListColorsPage::getColorsScheme( int nSchemeId )
{
	if (m_cslSchemesList.count() == 0)
		qDebug() << "FilesListColorsPage::getColorsScheme(. No schemes defined!"; // always shoud be at least one (defauld)

	if (nSchemeId < 0 || nSchemeId > m_cslSchemesList.count()-1) {
		qDebug() << "FilesListColorsPage::getColorsScheme(. Given scheme Id over the range!";
		return m_cslSchemesList[0];
	}

	return m_cslSchemesList[nSchemeId];
}

bool FilesListColorsPage::colorChangedInCurrentSchema() const
{
	QColor currentColor, configColor;
	bool bColorChanged = false;
	int nId = 0;

	while (nId < m_nAllChildren) {
		currentColor = getColor(nId, StructSrc);
		configColor  = getColor(nId, ConfigSrc);
		if (currentColor != configColor) {
			bColorChanged = true;
			break;
		}
		++nId;
	}

	return bColorChanged;
}


// -------------------------------------------------------
// ------------------------ SLOTS ------------------------

void FilesListColorsPage::slotChangeColor()
{
	int nSchemeId = m_pSchemeComboBox->currentIndex();
	if (nSchemeId == 0) // first is read only
		return;

	QString sSenderName = (sender() != NULL) ? sender()->objectName() : "?";
	if (sSenderName == "?")
		return;

	int nColorId = sSenderName.split("_").at(1).toInt();
	if (nColorId < 0)
		return;

	QColor initColor = getColor(nColorId); // get from current scheme
	QColor newColor  = QColorDialog::getColor(initColor);
	if (! newColor.isValid())
		return;

	// -- change color of button
	int nId = 0;
	QTreeWidgetItemIterator it(m_pTreeWidget);
	while (nId < m_nAllChildren) {
		if (nId == nColorId)
			break;
		++nId;
		++it;
	}
	QPushButton *pPushBtn = (QPushButton *)m_pTreeWidget->itemWidget((*it), 1);
	QPalette palette;
	palette.setColor(QPalette::Button, newColor); // before: pPushBtn->backgroundRole()
	pPushBtn->setPalette(palette);

	// -- update scheme (saved in container)
	updateCurrentScheme(nColorId, newColor);
	drawPreview();
}

void FilesListColorsPage::slotChangedScheme( int nSchemeNewId )
{
	qDebug() << "FilesListColorsPage::slotChangedScheme. selected scheme:" << m_pSchemeComboBox->itemText(nSchemeNewId);
	// -- enable/diable checkbix related with SecondBackgroundColor
	int nSchemeId = m_pSchemeComboBox->currentIndex();
	int nId = 0;
	QTreeWidgetItemIterator it(m_pTreeWidget);
	while (nId < m_nAllChildren) {
		if (nId == SecondBackgroundColor)
			break;
		++nId;
		++it;
	}
	QCheckBox *pChkBox = (QCheckBox *)m_pTreeWidget->itemWidget((*it), 0);
	pChkBox->setEnabled((nSchemeId > 0)); // first is read only
	pChkBox->setChecked(isSecondBackgroundColor());

	// -- update colors
	QPalette palette;
	QPushButton *pPushBtn;
	QTreeWidgetItemIterator it2(m_pTreeWidget);
	nId = 0;
	while (nId < m_nAllChildren) {
		if ((*it2)->parent() == nullptr && nId != CursorColor) {
			++nId;
			++it2;
			continue;
		}
		pPushBtn = (QPushButton *)m_pTreeWidget->itemWidget((*it2), 1);
		palette.setColor(pPushBtn->backgroundRole(), getColor(nId));
		pPushBtn->setPalette(palette);
		++nId;
		++it2;
	}

	// -- update preview and buttons
	drawPreview();

	m_pDeleteSchemeBtn->setEnabled((nSchemeId > 0));
// 	m_pSaveSchemeBtn->setEnabled((nSchemeId > 0));
}

void FilesListColorsPage::slotShowSecondBgColor( bool bShow )
{
	// -- enable/disable properly button and chenge color of button
	int nId = 0;
	QTreeWidgetItemIterator it(m_pTreeWidget);
	while (nId < m_nAllChildren) {
		if (nId == SecondBackgroundColor)
			break;
		++nId;
		++it;
	}
	QColor btnColor = (bShow) ? getColor(SecondBackgroundColor) : getColor(BackgroundColor);
	QPushButton *pPushBtn = (QPushButton *)m_pTreeWidget->itemWidget((*it), 1);
	QPalette palette;
	palette.setColor(pPushBtn->backgroundRole(), btnColor);
	pPushBtn->setPalette(palette);
	pPushBtn->setEnabled(bShow);

	// -- update scheme (saved in container) and preview
	updateCurrentScheme(bShow);
	drawPreview();
}

void FilesListColorsPage::slotNew()
{
	bool bOk = false;
	QString sNewSchemeName = QInputDialog::getText(this,
		tr("New scheme name")+" - QtCommander",
		tr("Please enter new scheme name:"),
		QLineEdit::Normal, tr("NewScheme"), &bOk
	);
	if (bOk == false)
		return;

	sNewSchemeName.replace(' ', '_'); // TODO ' ' change to reg.expr
	sNewSchemeName.replace('/', '_'); // TODO ' ' change to reg.expr
	sNewSchemeName.replace('\\', '_'); // TODO ' ' change to reg.expr

	ColorsScheme colorsScheme;

	colorsScheme.name = sNewSchemeName;
	colorsScheme.bAlternatingRowColors = m_cslSchemesList[0].bAlternatingRowColors;
// 	colorsScheme.bShowGrid = m_cslSchemesList[0].bShowGrid;
	colorsScheme.itemColor = m_cslSchemesList[0].itemColor;
	colorsScheme.itemColorUnderCursor = m_cslSchemesList[0].itemColorUnderCursor;
	colorsScheme.colorOfSelectedItem = m_cslSchemesList[0].colorOfSelectedItem;
	colorsScheme.colorOfSelectedItemUnderCursor = m_cslSchemesList[0].colorOfSelectedItemUnderCursor;
// 	colorsScheme.backgroundColorOfSelectedItem = m_cslSchemesList[0].backgroundColorOfSelectedItem;
// 	colorsScheme.backgroundColorOfSelectedItemUnderCursor = m_cslSchemesList[0].backgroundColorOfSelectedItemUnderCursor;
	colorsScheme.backgroundColor = m_cslSchemesList[0].backgroundColor;
	colorsScheme.secondBackgroundColor = m_cslSchemesList[0].secondBackgroundColor;
	colorsScheme.cursorColor = m_cslSchemesList[0].cursorColor;
	colorsScheme.hiddenFileColor = m_cslSchemesList[0].hiddenFileColor;
	colorsScheme.executableFileColor = m_cslSchemesList[0].executableFileColor;
	colorsScheme.symlinkColor = m_cslSchemesList[0].symlinkColor;
	colorsScheme.brokenSymlinkColor = m_cslSchemesList[0].brokenSymlinkColor;
// 	colorsScheme.gridColor = m_cslSchemesList[0].gridColor;

	m_cslSchemesList.append(colorsScheme);

	m_pSchemeComboBox->addItem(sNewSchemeName);
	m_pSchemeComboBox->setCurrentIndex(m_pSchemeComboBox->findText(sNewSchemeName));
	slotChangedScheme(m_pSchemeComboBox->currentIndex());
}


void FilesListColorsPage::slotDelete()
{
	if (m_bAlwaysAskBeforeDeleteScheme) {
		int nResult = MessageBox::yesNo(this, tr("Delete color scheme"), tr("Deleteing scheme: ")+m_pSchemeComboBox->currentText(),
			tr("Are you sure you want to delete this scheme?"),
				m_bAlwaysAskBeforeDeleteScheme, MessageBox::No, // set focus for this button
				tr("Always ask before delete scheme")
			);
		Settings settings;
		settings.setValue("filelistviewStyles/AlwaysAskBeforeDeleteScheme", m_bAlwaysAskBeforeDeleteScheme);
		if (nResult == 0 || nResult == MessageBox::No) // user closed dialog
			return;
	}

	int sIdSchemeToRemove = m_pSchemeComboBox->currentIndex();
	m_slRemovedSchemes << QString("flvStyle_"+m_pSchemeComboBox->currentText());
	qDebug() << "FilesListColorsPage::slotDelete. Removed scheme:" << m_pSchemeComboBox->currentText();

	m_pSchemeComboBox->removeItem(sIdSchemeToRemove);
	m_cslSchemesList.removeAt(sIdSchemeToRemove);
	slotChangedScheme(m_pSchemeComboBox->currentIndex());
}


void FilesListColorsPage::slotSave()
{
	qDebug() << "FilesListColorsPage::slotSave.";
	if (! colorChangedInCurrentSchema()) {
		qDebug() << "FilesListColorsPage::slotSave. No changed color in current scheme.";
		return;
	}

	Settings settings;
	int nSchemeId = m_pSchemeComboBox->currentIndex();
	QString sScheme = "flvStyle_"+m_pSchemeComboBox->currentText();
	if (m_pSchemeComboBox->currentText() == "DefaultColors" || m_pSchemeComboBox->count() == 1)
		return;

// 	bool bNewSchemToSave = (m_pSchemeComboBox->count() > m_nAmountOfSchemes);

	// -- save new scheme
	settings.update(sScheme+"/AlternatingRowColors", m_cslSchemesList[nSchemeId].bAlternatingRowColors);
	//settings.update(sScheme+"/ShowGrid", m_cslSchemesList[nSchemeId].bShowGrid);
	settings.update(sScheme+"/ItemColor", m_cslSchemesList[nSchemeId].itemColor.name());
	settings.update(sScheme+"/ItemColorUnderCursor", m_cslSchemesList[nSchemeId].itemColorUnderCursor.name());
	settings.update(sScheme+"/ColorOfSelectedItem", m_cslSchemesList[nSchemeId].colorOfSelectedItem.name());
	settings.update(sScheme+"/ColorOfSelectedItemUnderCursor", m_cslSchemesList[nSchemeId].colorOfSelectedItemUnderCursor.name());
// 	settings.update(sScheme+"/BackgroundColorOfSelectedItem", m_cslSchemesList[nSchemeId].backgroundColorOfSelectedItem.name());
// 	settings.update(sScheme+"/BackgroundColorOfSelectedItemUnderCursor", m_cslSchemesList[nSchemeId].backgroundColorOfSelectedItemUnderCursor.name());
	settings.update(sScheme+"/BackgroundColor", m_cslSchemesList[nSchemeId].backgroundColor.name());
	settings.update(sScheme+"/SecondBackgroundColor", m_cslSchemesList[nSchemeId].secondBackgroundColor.name());
	settings.update(sScheme+"/CursorColor", m_cslSchemesList[nSchemeId].cursorColor.name());
	settings.update(sScheme+"/HiddenFileColor", m_cslSchemesList[nSchemeId].hiddenFileColor.name());
	settings.update(sScheme+"/ExecutableFileColor", m_cslSchemesList[nSchemeId].executableFileColor.name());
	settings.update(sScheme+"/SymlinkColor", m_cslSchemesList[nSchemeId].symlinkColor.name());
	settings.update(sScheme+"/BrokenSymlinkColor", m_cslSchemesList[nSchemeId].brokenSymlinkColor.name());

	if (! m_slRemovedSchemes.isEmpty()) {
		foreach(QString sName, m_slRemovedSchemes) {
			qDebug() << "FilesListColorsPage::slotSave. Remove: " << sName;
			settings.remove(sName);
			m_slRemovedSchemes.removeOne(sName);
		}
	}
	QString sSchemesList;
	foreach (ColorsScheme colorsScheme, m_cslSchemesList) {
		sSchemesList += colorsScheme.name +"|";
	}
	sSchemesList.remove(sSchemesList.length()-1, 1);
	settings.update("filelistviewStyles/Schemas", sSchemesList);
	settings.update("filelistviewStyles/SchemeCurrentId", m_pSchemeComboBox->currentIndex());

// 	MessageBox::information(this, tr("Save scheme"), tr("Schema configuration updated."));
// 	m_nAmountOfSchemes = m_pSchemeComboBox->count();
}

