/***************************************************************************
 *   Copyright (C) 2007 by Piotr Mierzwi?ski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "fileslistothersettings.h"
#include "settings.h"


FilesListOtherSettings::FilesListOtherSettings()
{
	setupUi(this);

	init();
}


void FilesListOtherSettings::init()
{
	Settings settings;

	bool bShowIcons  = settings.value("filelistview/ShowIcons", true).toBool();
	bool bShowHidden = settings.value("filelistview/ShowHidden", true).toBool();
	bool bBoldedDir  = settings.value("filelistview/BoldedDir", true).toBool();
	bool bBytesRound = settings.value("filelistview/BytesRound", true).toBool();
	bool bSelectDirs = settings.value("filelistview/SelectDirs", true).toBool();

	m_pShowIconsChkBox->setChecked(bShowIcons);
	m_pShowHiddenChkBox->setChecked(bShowHidden);
	m_pBoldDirectoriesChkBox->setChecked(bBoldedDir);
	m_pBytesRoundChkBox->setChecked(bBytesRound);
	m_pSelectDirsChkBox->setChecked(bSelectDirs);
}

void FilesListOtherSettings::setDefaults()
{
	m_pShowIconsChkBox->setChecked(true);
	m_pShowHiddenChkBox->setChecked(true);
	m_pBoldDirectoriesChkBox->setChecked(true);
	m_pBytesRoundChkBox->setChecked(true);
	m_pSelectDirsChkBox->setChecked(true);
}


void FilesListOtherSettings::slotSave()
{
	Settings settings;

	settings.setValue("filelistview/ShowIcons",  m_pShowIconsChkBox->isChecked());
	settings.setValue("filelistview/ShowHidden", m_pShowHiddenChkBox->isChecked());
	settings.setValue("filelistview/BoldedDir",  m_pBoldDirectoriesChkBox->isChecked());
	settings.setValue("filelistview/BytesRound", m_pBytesRoundChkBox->isChecked());
	settings.setValue("filelistview/SelectDirs", m_pSelectDirsChkBox->isChecked());
}

