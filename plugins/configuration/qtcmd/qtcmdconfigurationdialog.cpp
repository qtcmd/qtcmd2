/***************************************************************************
 *   Copyright (C) 2018 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDebug>
#include <QToolButton>

#include "plugindata.h"
#include "listviewsettings.h"
#include "toolbarpagesettings.h"
#include "qtcmdconfigurationdialog.h"


QtCmdConfigurationDialog::QtCmdConfigurationDialog()
{
	qDebug() << "QtCmdConfigurationDialog::QtCmdConfigurationDialog.";
	m_pDialog = nullptr;
	m_pFontPage = nullptr;
	m_pColorsPage  = nullptr;
	m_pColumnsPage = nullptr;
	m_pFilesListOtherSettings = nullptr;
	m_pLfsPage = nullptr;
	m_pFtpPage = nullptr;
	m_pArchivesPage = nullptr;
	m_pFileSystemsOtherSettings = nullptr;
	m_pEmptyPage = nullptr;
	m_pKeyshortcutsPage = nullptr;
	m_pToolBarPage = nullptr;
	m_nListViewPagesCount = 0;
	m_nFileSystemsPagesCount = 0;
	m_nOtherPagesCount = 0;

	m_pPluginData = new PluginData;
	m_pPluginData->name         = "qtcmdconfigdialog_plugin";
	m_pPluginData->version      = "0.2";
	m_pPluginData->description  = QObject::tr("Plugin handles configuration dialog for application.");
	m_pPluginData->plugin_class = "config";
	m_pPluginData->mime_type    = "config/app";
}

QtCmdConfigurationDialog::~QtCmdConfigurationDialog()
{
	qDebug() << "QtCmdConfigurationDialog. DESTRUCTOR.";
	delete m_pDialog;
	delete m_pPluginData;
}


void QtCmdConfigurationDialog::init( QWidget *pParent, KeyShortcuts *pKeyShortcuts, QList <QActionGroup *> *pActionGroupLst )
{
	qDebug() << "QtCmdConfigurationDialog::init.";
	m_pDialog = new QDialog(pParent);
	setupUi(m_pDialog);

	connect(m_pToolBox,  static_cast<void(QToolBox::*)(int)>(&QToolBox::currentChanged), this, &QtCmdConfigurationDialog::slotSectionChanged);

	connect(m_pFilesListWidget,       &QListWidget::currentRowChanged, this, &QtCmdConfigurationDialog::slotShowPageInSection);
	connect(m_pFileSystemsListWidget, &QListWidget::currentRowChanged, this, &QtCmdConfigurationDialog::slotShowPageInSection);
	connect(m_pOtherPageListWidget,   &QListWidget::currentRowChanged, this, &QtCmdConfigurationDialog::slotShowPageInSection);

	connect(m_pOkBtn,       &QPushButton::clicked, this, &QtCmdConfigurationDialog::slotOkButtonPressed);
// 	connect(m_pApplyBtn,    &QPushButton::clicked, this, &QtCmdConfigurationDialog::slotApplyButtonPressed); // removed from dialog
	connect(m_pDefaultsBtn, &QPushButton::clicked, this, &QtCmdConfigurationDialog::slotDefaultsButtonPressed);


	m_pToolBox->setMinimumWidth((width()/4)-20);
	m_pToolBox->setMaximumWidth((width()/4)-20);

	setWindowTitle(tr("Configuration") + " - QtCommander");

	// --- Files List pages
	m_pFontPage = new FontChooserPage(FontChooserPage::FILE_LIST_VIEW);
	m_pColorsPage  = new FilesListColorsPage;
	m_pColumnsPage = new FilesListColumnsPage;
	m_pFilesListOtherSettings = new FilesListOtherSettings;

	m_pStackOfWidgets->addWidget(m_pColorsPage);
	m_pStackOfWidgets->addWidget(m_pColumnsPage);
	m_pStackOfWidgets->addWidget(m_pFontPage);
	m_pStackOfWidgets->addWidget(m_pFilesListOtherSettings);
	m_nListViewPagesCount = m_pStackOfWidgets->count();

	// --- File System pages
	m_pArchivesPage = new ArchivesPage;
	m_pFtpPage      = new FtpPage;
	m_pLfsPage      = new LfsPage;
	m_pFileSystemsOtherSettings = new FileSystemsOtherSettings;

	m_pStackOfWidgets->addWidget(m_pArchivesPage);
	m_pStackOfWidgets->addWidget(m_pFtpPage);
	m_pStackOfWidgets->addWidget(m_pLfsPage);
	m_pStackOfWidgets->addWidget(m_pFileSystemsOtherSettings);
	m_nFileSystemsPagesCount = m_pStackOfWidgets->count();

	// --- Other pages
	m_pKeyshortcutsPage = new KeyShortcutsPage(true); // true -> appSettings
	m_pToolBarPage = new ToolBarPage;

	m_pStackOfWidgets->addWidget(m_pKeyshortcutsPage);
	m_pStackOfWidgets->addWidget(m_pToolBarPage);
	m_nOtherPagesCount = m_pStackOfWidgets->count();

	// --- Empty page (need to be last one)
	m_pEmptyPage = new QWidget; // an empty page for no items list
	m_pStackOfWidgets->addWidget(m_pEmptyPage);

// 	m_pKeyshortcutsPage->init(true); // true -> appSettings, call here because in constructor is missing, because of connect
	m_pKeyshortcutsPage->setKeyShortcuts(pKeyShortcuts);
	m_pToolBarPage->init(pActionGroupLst, "mainwindow");

	// initialize first item for each section
	m_pFilesListWidget->setCurrentRow(0);
	m_pFileSystemsListWidget->setCurrentRow(0);
	m_pOtherPageListWidget->setCurrentRow(0);

	// --- init view onto dialog
	resize(QSize(635, 515).expandedTo(minimumSizeHint()));

	static QList <int>sSW; // width the children of spliter obj.
	sSW << (width()*34)/100 << 0;
	sSW[1] = width()-sSW[0];
	m_pSplitter->setSizes(sSW);
}

void QtCmdConfigurationDialog::raiseWindow( const QString &sSectionName )
{
// 	qDebug() << "QtCmdConfigurationDialog::raiseWindow. Show page:" << sSectionName;
	if (m_pDialog != nullptr) {
		m_pDialog->show();
		showCurrentPage(sSectionName);
	}
	else
		qDebug() << "QtCmdConfigurationDialog::raiseWindow. ERROR. QDialog object not created!";
}

void QtCmdConfigurationDialog::showCurrentPage( const QString &sSectionName )
{
// 	qDebug() << "QtCmdConfigurationDialog::showCurrentPage. sSectionName:" << sSectionName;
	int nItemId = -1;
	if      (sSectionName == "FilesListPage") {
		nItemId = m_pFilesListWidget->currentRow();
		m_pFilesListWidget->setFocus();
	}
	else if (sSectionName == "FilesSystemsPage") {
		nItemId = m_pFileSystemsListWidget->currentRow();
		m_pFileSystemsListWidget->setFocus();
	}
	else if (sSectionName == "OtherPage") {
		nItemId = m_pOtherPageListWidget->currentRow();
		m_pOtherPageListWidget->setFocus();
	}

	if (nItemId >= 0)
		slotShowPageInSection(nItemId);
}

/*
void QtCmdConfigurationDialog::slotSetKeyShortcuts( int *pKeyShortcuts )
{
	if ( m_pKeyshortcutsPage )
		m_pKeyshortcutsPage->setKeyShortcuts( pKeyShortcuts );
}
*/

void QtCmdConfigurationDialog::slotDefaultsButtonPressed()
{
	QString sCurrentPageName = m_pToolBox->itemText(m_pToolBox->currentIndex());
	QString sCurrentItem;

	if (sCurrentPageName == tr("Files List")) {
		sCurrentItem = m_pFilesListWidget->currentItem()->text();

		if (sCurrentItem == tr("Colors"))
			m_pColorsPage->setDefaults();
		else if (sCurrentItem == tr("Columns"))
			m_pColumnsPage->setDefaults();
		else if (sCurrentItem == tr("Fonts"))
			m_pFontPage->setDefaults();
		else if (sCurrentItem == tr("Other"))
			m_pFilesListOtherSettings->setDefaults();
	}
	else
	if (sCurrentPageName == tr("Files Systems")) {
		sCurrentItem = m_pFileSystemsListWidget->currentItem()->text();

		if (sCurrentItem == tr("Archives"))
			m_pArchivesPage->setDefaults();
		else if (sCurrentItem == tr("FTP"))
			m_pFtpPage->setDefaults();
		else if (sCurrentItem == tr("Local"))
			m_pLfsPage->setDefaults();
		else if (sCurrentItem == tr("Other"))
			m_pFileSystemsOtherSettings->setDefaults();
	}
	else
	if (sCurrentPageName == tr("Other")) {
		sCurrentItem = m_pOtherPageListWidget->currentItem()->text();

		if (sCurrentItem == tr("Keyshortcuts"))
			m_pKeyshortcutsPage->setDefaults();
		if (sCurrentItem == tr("Tool bars"))
			m_pToolBarPage->setDefaults();
	}
}

void QtCmdConfigurationDialog::slotOkButtonPressed()
{
	slotApplyButtonPressed();
	slotSaveButtonPressed();
	m_pDialog->close();
}


void QtCmdConfigurationDialog::slotApplyButtonPressed()
{
	//qDebug() << "QtCmdConfigurationDialog::slotApplyButtonPressed. objectName:" << m_pToolBox->currentWidget()->objectName() << "itemText" << m_pToolBox->itemText(m_pToolBox->currentIndex());
	QString sCurrentPageName = m_pToolBox->itemText(m_pToolBox->currentIndex());

	// --- inits the file list settings
	if (sCurrentPageName == tr("Files List")) {
		ListViewSettings listViewSettings;
		QString sCurrentItem = m_pFilesListWidget->currentItem()->text();
		//qDebug() << "QtCmdConfigurationDialog::slotApplyButtonPressed. Files List->" << sCurrentItem;

		if (sCurrentItem == tr("Colors")) {
			listViewSettings.showSecondBackgroundColor = m_pColorsPage->isSecondBackgroundColor(); //  m_pColorsPage->m_pSecBgColChkBox->isChecked();
		// 	listViewSettings.showGrid               = m_pColorsPage->m_pShowGridChkBox->isChecked();
			listViewSettings.itemColor              = m_pColorsPage->getColor(ItemColor);
			listViewSettings.itemColorUnderCursor   = m_pColorsPage->getColor(ItemColorUnderCursor);
			listViewSettings.colorOfSelectedItem    = m_pColorsPage->getColor(ColorOfSelectedItem);
			listViewSettings.colorOfSelectedItemUnderCursor = m_pColorsPage->getColor(ColorOfSelectedItemUnderCursor);
		// 	listViewSettings.backgroundColorOfSelectedItem = m_pColorsPage->getColor(BackgroundColorOfSelectedItem);
		// 	listViewSettings.backgroundColorOfSelectedItemUnderCursor = m_pColorsPage->getColor(BackgroundColorOfSelectedItemUnderCursor);
			listViewSettings.backgroundColor        = m_pColorsPage->getColor(BackgroundColor);
			listViewSettings.secondBackgroundColor  = m_pColorsPage->getColor(SecondBackgroundColor);
			listViewSettings.cursorColor            = m_pColorsPage->getColor(CursorColor);
		// 	listViewSettings.gridColor              = m_pColorsPage->getColor(GridColor);
			listViewSettings.hiddenFileColor        = m_pColorsPage->getColor(HiddenFileColor);
			listViewSettings.executableFileColor    = m_pColorsPage->getColor(ExecutableFileColor);
			listViewSettings.symlinkColor           = m_pColorsPage->getColor(SymlinkColor);
			listViewSettings.brokenSymlinkColor     = m_pColorsPage->getColor(BrokenSymlinkColor);
		}
		else if (sCurrentItem == tr("Columns")) {
			listViewSettings.columnsWidthTab[NAMEcol]  = m_pColumnsPage->columnWidth(NAMEcol);
			//listViewSettings.columnsWidthTab[EXTNAMEcol]  = m_pColumnsPage->columnWidth(EXTNAMEcol );
			listViewSettings.columnsWidthTab[SIZEcol]  = m_pColumnsPage->columnWidth(SIZEcol);
			listViewSettings.columnsWidthTab[TIMEcol]  = m_pColumnsPage->columnWidth(TIMEcol);
			listViewSettings.columnsWidthTab[PERMcol]  = m_pColumnsPage->columnWidth(PERMcol);
			listViewSettings.columnsWidthTab[OWNERcol] = m_pColumnsPage->columnWidth(OWNERcol);
			listViewSettings.columnsWidthTab[GROUPcol] = m_pColumnsPage->columnWidth(GROUPcol);
			listViewSettings.columnsWidthTab[FTYPEcol] = m_pColumnsPage->columnWidth(FTYPEcol);
			listViewSettings.synchronizeColWidthInBothPanels = m_pColumnsPage->m_pSynchronizeColsWidthChkBox->isChecked();
		}
		else if (sCurrentItem == tr("Fonts")) {
			listViewSettings.fontFamily = m_pFontPage->fontFamily();
			listViewSettings.fontSize   = m_pFontPage->fontSize();
			listViewSettings.fontBold   = m_pFontPage->fontBold();
			listViewSettings.fontItalic = m_pFontPage->fontItalic();
		}
		else if (sCurrentItem == tr("Other")) {
			listViewSettings.showIcons       = m_pFilesListOtherSettings->m_pShowIconsChkBox->isChecked();
			listViewSettings.showHiddenFiles = m_pFilesListOtherSettings->m_pShowHiddenChkBox->isChecked();
			listViewSettings.boldedDir       = m_pFilesListOtherSettings->m_pBoldDirectoriesChkBox->isChecked();
			listViewSettings.bytesRound      = m_pFilesListOtherSettings->m_pBytesRoundChkBox->isChecked();
			listViewSettings.selectDirs      = m_pFilesListOtherSettings->m_pSelectDirsChkBox->isChecked();
		}
		Q_EMIT ConfigurationDialog::signalApply(listViewSettings, sCurrentItem);
	}
	else // --- inits the file system settings
	if (sCurrentPageName == tr("Files Systems")) {
		FileSystemSettings fileSystemSettings;
		QString sCurrentItem = m_pFileSystemsListWidget->currentItem()->text();
		//qDebug() << "QtCmdConfigurationDialog::slotApplyButtonPressed. Files Systems->" << sCurrentItem;

		if (sCurrentItem == tr("Archives")) {
			fileSystemSettings.workDirectoryPath = m_pArchivesPage->workingDirPath();
			fileSystemSettings.alwaysOverwriteWhenArchiving   = m_pArchivesPage->m_pAlwaysOverwriteAddToArchChkBox->isChecked();
			fileSystemSettings.alwaysOverwriteWithNewArchive  = m_pArchivesPage->m_pAlwaysOverwriteNewArchChkBox->isChecked();
			fileSystemSettings.closeProgressDlgWhenArchivFinished  = m_pArchivesPage->m_pCloseProgressDlgAftterArchFinished->isChecked();
			fileSystemSettings.closeProgressDlgWhenExtractFinished = m_pArchivesPage->m_pCloseProgressDlgAftterExtrFinished->isChecked();
			// ... particular archivers settings
		}
		else if (sCurrentItem == tr("FTP")) {
			fileSystemSettings.standByConnection         = m_pFtpPage->m_pStandByConnectionChkBox->isChecked();
			fileSystemSettings.numOfTimeToRetryIfFtpBusy = m_pFtpPage->numOfTimeToRetryIfBusy();
			fileSystemSettings.closeProgressDlgWhenDwonloadFinished  = m_pFtpPage->m_pCloseProgressDlgAftterDownlFinished->isChecked();
			fileSystemSettings.closeProgressDlgWhenUploadFinished  = m_pFtpPage->m_pCloseProgressDlgAftterUploadFinished->isChecked();
		}
		else if (sCurrentItem == tr("Local")) {
			fileSystemSettings.moveToTrash           = m_pLfsPage->m_pMoveToTrash->isChecked();
			fileSystemSettings.alwaysWeighBeforeCopy = m_pLfsPage->m_pWeighBeforeCopyChkBox->isChecked();
			fileSystemSettings.alwaysWeighBeforeMove = m_pLfsPage->m_pWeighBeforeMoveChkBox->isChecked();
			fileSystemSettings.alwaysWeighBeforeRemove = m_pLfsPage->m_pWeighBeforeRemoveChkBox->isChecked();
			fileSystemSettings.pathToTrash           = m_pLfsPage->trashPath();
			fileSystemSettings.closeProgressDlgWhenCopyFinished   = m_pLfsPage->m_pCloseProgressDlgAfterCpyFinished->isChecked();
			fileSystemSettings.closeProgressDlgWhenMoveFinished   = m_pLfsPage->m_pCloseProgressDlgAfterMovFinished->isChecked();
			fileSystemSettings.closeProgressDlgWhenDeleteFinished = m_pLfsPage->m_pCloseProgressDlgAftterDelFinished->isChecked();
		}
		else if (sCurrentItem == tr("Other")) {
			fileSystemSettings.alwaysOverwriteInCaseCpMvDwExt = m_pFileSystemsOtherSettings->m_pAlwaysOverwriteInCaseCpMvRmChkBox->isChecked();
			fileSystemSettings.alwaysAskBeforeRemoving  = m_pFileSystemsOtherSettings->m_pAskBeforeDeleteChkBox->isChecked();
			fileSystemSettings.alwaysSavePermission     = m_pFileSystemsOtherSettings->m_pAlwaysSavePermissionChkBox->isChecked();
			fileSystemSettings.openDirAfterCreationIt   = m_pFileSystemsOtherSettings->m_pOpenDirAfterCreationItChkBox->isChecked();
		}
		Q_EMIT ConfigurationDialog::signalApply(fileSystemSettings, sCurrentItem);
	}
	else // --- inits the other apps. settings
	if (sCurrentPageName == tr("Other")) {
		QString sCurrentItem = m_pOtherPageListWidget->currentItem()->text();

		if (sCurrentItem == tr("Keyshortcuts"))
			Q_EMIT ConfigurationDialog::signalApplyKeyShortcuts(m_pKeyshortcutsPage->keyShortcuts());

		if (sCurrentItem == tr("Tool bars")) {
			ToolBarPageSettings base, tools, navi;
			base.tbName  = "Base";	      base.tbActions  = m_pToolBarPage->actions("Base");
			tools.tbName = "Tools";	      tools.tbActions = m_pToolBarPage->actions("Tools");
			navi.tbName  = "Navigation";  navi.tbActions  = m_pToolBarPage->actions("Navigation");

			Q_EMIT ConfigurationDialog::signalApply(base);
			Q_EMIT ConfigurationDialog::signalApply(tools);
			Q_EMIT ConfigurationDialog::signalApply(navi);
		}
	}
}


void QtCmdConfigurationDialog::slotSaveButtonPressed()
{
	// --- Files List pages
	m_pColorsPage->slotSave();
	m_pFontPage->slotSave();
	m_pColumnsPage->slotSave();
	m_pFilesListOtherSettings->slotSave();
	// --- FileSystem pages
	m_pArchivesPage->slotSave();
	m_pLfsPage->slotSave();
	m_pFtpPage->slotSave();
	m_pFileSystemsOtherSettings->slotSave();
	// --- Other pages
	m_pKeyshortcutsPage->slotSave();
	m_pToolBarPage->slotSave();
}


void QtCmdConfigurationDialog::slotSectionChanged( int /*nItemId*/ )
{
	QString sSectionName = m_pToolBox->currentWidget()->objectName();
	showCurrentPage(sSectionName);
}

void QtCmdConfigurationDialog::slotShowPageInSection( int nItemId )
{
	QString sTitle;
	QString sSectionName = m_pToolBox->currentWidget()->objectName();
	int nListWidgetsRow = (nItemId < 0) ? 0 : nItemId;

	if (sSectionName == "FilesListPage") {
		sTitle = m_pFilesListWidget->item(nListWidgetsRow)->text();
	}
	else
	if (sSectionName == "FilesSystemsPage") {
		sTitle = m_pFileSystemsListWidget->item(nListWidgetsRow)->text();
		nListWidgetsRow += m_nListViewPagesCount; // +pageOffset
	}
	else
	if (sSectionName == "OtherPage") {
		sTitle = m_pOtherPageListWidget->item(nListWidgetsRow)->text();
		nListWidgetsRow += m_nFileSystemsPagesCount; // +pageOffset
	}
// 	qDebug() << "QtCmdConfigurationDialog::slotShowPageInSection. nListWidgetsRow:" << nListWidgetsRow << "sSectionName:" << sSectionName << "sTitle" << sTitle;

	m_pStackOfWidgets->setCurrentIndex(nListWidgetsRow);
	m_pTitleLab->setText(sTitle);
}

