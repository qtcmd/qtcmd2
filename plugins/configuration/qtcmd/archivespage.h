/***************************************************************************
 *   Copyright (C) 2007 by Piotr Mierzwi?ski <piom@nes.pl>                 *
 *                         Mariusz Borowski <mariusz@nes.pl>               *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _ARCHIVESPAGE_H
#define _ARCHIVESPAGE_H

#include <QProcess>
#include <QStringList>

#include "ui_archivespagedlg.h"

/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class ArchivesPage : public QWidget, public Ui::ArchivesPageDlg
{
	Q_OBJECT
private:
	enum Columns  { AVAILABLEcol=0, NAMEcol, COMPRESSIONcol, FULLNAMEcol, PATHcol, EXTRASETcol };
	enum RARsett  { CreateSolidArchive=0, RarSfxArchive, RarSaveSymbolicLinks, SaveFileOwnerAndGroup, MultimediaCompression, OverwriteExistingFiles, DictionarySize };
	enum ZIPsett  { ZipSfxArchive=0, ZipSaveSymbolicLinks, ZipDoNotSaveExtraAttrib };
	enum ARCHpath { BZIP2path, GZIPpath, XZpath, LZMApath, TARpath, RARpath, ZIPpath, ARJpath };

	int m_ntRarAddsSettings[7];
	int m_ntZipAddsSettings[3];

	QStringList m_slArchiverFullNameList;

	QProcess *m_pProcess;

	void init();
	// enable/disable all column in given row except first
	void setRowEnable( int nRow, bool bEnable );
	void showExtraSettings( int nRow );

public:
	ArchivesPage();
	~ArchivesPage() {}

	QString workingDirPath();
	QString compressionLevel( const QString &sArchiverName );
	QString archiverFullName( const QString &sArchiverName );

	/**Set defauls settings, so archive settings.
	 */
	void setDefaults();

private Q_SLOTS:
	void slotEditBoxTextApplied( QTableWidgetItem *pItem, const QString &sNewText );
	void slotDetectSupportedArchives();
	void slotToggleAutodetect( bool bToggleAutodetect ) {
		m_pDetectArchivesBtn->setEnabled(! bToggleAutodetect);
	}
	void slotChangeWorkingDirectoryPath();
	void slotReadDataFromStdOut();

	void slotButtonClicked( QTableWidgetItem *pItem );

public Q_SLOTS:
	void slotSave();

};

#endif // _ARCHIVESPAGE_H
