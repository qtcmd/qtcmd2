/***************************************************************************
 *   Copyright (C) 2007 by Piotr Mierzwi?ski <piom@nes.pl>                 *
 *                         Mariusz Borowski <mariusz@nes.pl>               *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QtGui>
#include <QComboBox>
#include <QFileDialog>


#include "settings.h"
#include "messagebox.h"
#include "comboboxdata.h"
#include "listwidgetview.h"

#include "archivespage.h"


ArchivesPage::ArchivesPage()
	: m_pProcess(nullptr)
{
	setupUi(this);

	connect(m_pSelectPathBtn,     &QPushButton::clicked, this, &ArchivesPage::slotChangeWorkingDirectoryPath);
	connect(m_pDetectArchivesBtn, &QPushButton::clicked, this, &ArchivesPage::slotDetectSupportedArchives);
	connect(m_pAutodetect,        &QCheckBox::toggled,   this, &ArchivesPage::slotToggleAutodetect);

	m_pProcess = new QProcess;
	connect(m_pProcess, &QProcess::readyReadStandardOutput, this, &ArchivesPage::slotReadDataFromStdOut);

	init();
}


void ArchivesPage::init()
{
	Settings settings;
	// prepare view
	// -- add columns TODO make 'add column' support for plugin ListWidgetView
	m_pArchiversListView->addColumn(ListWidgetView::CheckBox,   tr("Available"), Qt::AlignHCenter);
	m_pArchiversListView->addColumn(ListWidgetView::TextLabel,  tr("Name")); // (default is left), Qt::AlignLeft
	m_pArchiversListView->addColumn(ListWidgetView::ComboBox,   tr("Compression level")); // (alignment hardcoded) , Qt::AlignHCenter | Qt::AlignVCenter
	m_pArchiversListView->addColumn(ListWidgetView::EditBox,    tr("Full name"));
	m_pArchiversListView->addColumn(ListWidgetView::PushButton, tr("Path"), Qt::AlignHCenter);
	m_pArchiversListView->addColumn(ListWidgetView::PushButton, tr("Extra settings"), Qt::AlignHCenter);

/*
	// set only if detected
	m_slArchiverFullNameList.append(settings.readEntry("Archives/Bzip2FullName", ""));
	m_slArchiverFullNameList.append(settings.readEntry("Archives/GzipFullName", ""));
	m_slArchiverFullNameList.append(settings.readEntry("Archives/RarFullName", ""));

	m_slArchiverFullNameList.append(settings.readEntry("Archives/TarFullName", ""));
	m_slArchiverFullNameList.append(settings.readEntry("Archives/ZipFullName", ""));
*/
	// -- set look&feel
/*	QString sScheme = settings.readEntry("FilesPanel/ColorsScheme", tr("DefaultColors"));
	QString sColor  = settings.readEntry(""+sScheme+"/SecondBackgroundColor", "FFEBDC");
	m_pArchiversListView->setSecondColorOfBg(color(sColor, "FFEBDC"));
	m_pArchiversListView->setEnableTwoColorsOfBg(true);
	m_pArchiversListView->setCursorForColumn(NAMEcol); // TODO add support for ListWidgetView to setCursorForColumn
	m_pArchiversListView->setCursorForColumn(AVAILABLEcol, false); // pItem->setFlags(Qt::ItemIsEnabled); na wszystkich wierszach tej kolumny
	m_pArchiversListView->setCursorForTxtColumnOnly(true); // ustaw kursor tylko na TextLabel, EditBox, KeyEditBox i ComboBox
*/
	//ListViewWidgetsItem *pItem;

	QStringList slCompressionLevels;
	QStringList slArchivers;
	QString sArchName, sArchFullName;
	bool bArchFullNameList = (m_slArchiverFullNameList.count() > 0);
	int nIdOffset;
	slArchivers << "bzip2" << "gzip" << "xz" << "lzma" << "tar" << "rar" << "zip" << "arj";

	QPalette sysPalette  = m_pArchiversListView->palette();
	QColor systemBgCol   = sysPalette.window().color();
	QColor textColor     = (systemBgCol.lightness() > 150) ? Qt::black : Qt::white;
// 	qDebug() << "systemBgCol.lightness:" << systemBgCol.lightness() << "col:" << textColor.name();

	for (int row=0; row<slArchivers.count(); row++) {
		sArchName = slArchivers[row];
		slCompressionLevels.clear();
		slCompressionLevels.append(tr("store"));

		nIdOffset = 0;
		if (sArchName == "tar" && bArchFullNameList)
			sArchFullName = m_slArchiverFullNameList[TARpath];
		else
		if (/*sArchName == "tar.bz2" || sArchName == "tar.gz" || */sArchName == "bzip2" || sArchName == "gzip" || sArchName == "xz" || sArchName == "lzma") {
			slCompressionLevels.clear(); // non store
			slCompressionLevels << "1 - "+tr("fast") << "2" << "3" << "4" << "5" << "6" << "7" << "8" << "9 - "+tr("best");
			if (bArchFullNameList) {
				if (sArchName == "bzip2")
					sArchFullName = m_slArchiverFullNameList[BZIP2path];
				else
				if (sArchName == "gzip")
					sArchFullName = m_slArchiverFullNameList[GZIPpath];
				else
				if (sArchName == "xz")
					sArchFullName = m_slArchiverFullNameList[XZpath];
				else
				if (sArchName == "lzma")
					sArchFullName = m_slArchiverFullNameList[LZMApath];
				else
					sArchFullName = m_slArchiverFullNameList[TARpath];
			}
		}
		else
		if (sArchName == "rar") {
			slCompressionLevels.append(tr("the fastest"));
			slCompressionLevels.append(tr("fast"));
			slCompressionLevels.append(tr("default"));
			slCompressionLevels.append(tr("good"));
			slCompressionLevels.append(tr("the best"));
			if (bArchFullNameList)
				sArchFullName = m_slArchiverFullNameList[RARpath];
		}
		else
		if (sArchName == "zip") {
			slCompressionLevels << "1 - "+tr("fast") << "2" << "3" << "4" << "5" << "6 - "+tr("default") << "7" << "8" << "9 - "+tr("best");
			if (bArchFullNameList)
				sArchFullName = m_slArchiverFullNameList[ZIPpath];
			nIdOffset = 1;
		}

		m_pArchiversListView->addRow();
		m_pArchiversListView->setCellValue(row, NAMEcol, slArchivers[row]);
		m_pArchiversListView->setCellValue(row, COMPRESSIONcol, slCompressionLevels, "", (slCompressionLevels.count()/2+nIdOffset));
		m_pArchiversListView->setCellAttributes(row, NAMEcol, textColor, ListWidgetView::Bold);
		m_pArchiversListView->setCellValue(row, PATHcol, "...");
		if (row == RARpath || row == ZIPpath)
			m_pArchiversListView->setCellValue(row, EXTRASETcol, "+");
		else
			m_pArchiversListView->setEnableCell(row, EXTRASETcol, false, false);

		setRowEnable(row, ! sArchFullName.isEmpty());
		if (! sArchFullName.isEmpty())
			m_pArchiversListView->setCellAttributes(row, FULLNAMEcol, sArchFullName);
		/*
		pItem = new ListViewWidgetsItem(m_pArchiversListView, CheckBox, TextLabel, ComboBox, EditBox);
		pItem->setTextValue(NAMEcol, slArchivers[i]);
		pItem->insertStringList(COMPRESSIONcol, slCompressionLevels);
		c = (sArchName == "zip") ? 1 : 0;
		pItem->setText(COMPRESSIONcol, slCompressionLevels[slCompressionLevels.nCount()/2+c]); // default is first
		pItem->setTextAttributs(NAMEcol, Qt::black, true);
		if (sArchFullName.isEmpty()) {
			pItem->setEnableCell(NAMEcol, false); // disable all archivers
			pItem->setEnableCell(COMPRESSIONcol, false); // disable all archivers
		}
		else
			pItem->setText(FULLNAMEcol, sArchFullName);
		*/
	} // for, slArchivers

	// --- init RAR addons settings
	m_ntRarAddsSettings[CreateSolidArchive] = settings.value("Archives/RarSolidArchive", true).toBool();
	m_ntRarAddsSettings[RarSfxArchive] = settings.value("Archives/RarSfxArchive", false).toBool();
	m_ntRarAddsSettings[RarSaveSymbolicLinks] = settings.value("Archives/RarSaveLinks", true).toBool();
	m_ntRarAddsSettings[SaveFileOwnerAndGroup] = settings.value("Archives/RarSaveOwnerAndGroup", true).toBool();
	m_ntRarAddsSettings[MultimediaCompression] = settings.value("Archives/RarMultimediaCompression", false).toBool();
	m_ntRarAddsSettings[OverwriteExistingFiles] = settings.value("Archives/RarOvrExistingFiles", false).toBool();
	m_ntRarAddsSettings[DictionarySize] = settings.value("Archives/RarDictionarySize", 512).toInt();
	// --- init ZIP addons settings
	m_ntZipAddsSettings[ZipSfxArchive] = settings.value("Archives/ZipSfxArchive", false).toBool();
	m_ntZipAddsSettings[ZipSaveSymbolicLinks] = settings.value("Archives/ZipSaveSymbolicLinks", true).toBool();
	m_ntZipAddsSettings[ZipDoNotSaveExtraAttrib] = settings.value("Archives/ZipDoNotSaveExtraAttrib", false).toBool();

	connect(m_pArchiversListView, static_cast<void(ListWidgetView::*)(QTableWidgetItem *, const QString &)>(&ListWidgetView::editBoxTextApplied), this, &ArchivesPage::slotEditBoxTextApplied);
	connect(m_pArchiversListView, static_cast<void(ListWidgetView::*)(QTableWidgetItem *)>(&ListWidgetView::buttonClicked), this, &ArchivesPage::slotButtonClicked);

	m_pArchiversListView->selectRow(0);
	m_pArchiversListView->resizeColumnsToContents();

	bool bAutodetect = settings.value("Archives/Autodetect", true).toBool();
	m_pAutodetect->setChecked(bAutodetect);
	if (bAutodetect)
		slotDetectSupportedArchives();
	slotToggleAutodetect(bAutodetect);


	QString sWorkDir = settings.value("Archives/WorkingDirectory", QDir::homePath()+"/tmp/qtcmd").toString();
	m_pWorkingDirPathLnEdit->setText(sWorkDir);

	m_pAlwaysOverwriteAddToArchChkBox->setChecked(settings.value("Archives/AlwaysOverwriteWhenArchiving", true).toBool()); // adding to arch
	m_pAlwaysOverwriteNewArchChkBox->setChecked(settings.value("Archives/AlwaysOverwriteWithNewArchive", true).toBool()); // creating new archive
	m_pCloseProgressDlgAftterArchFinished-> setChecked(settings.value("Archives/CloseProgressDlgWhenArchivFinished",   false).toBool());
	m_pCloseProgressDlgAftterExtrFinished-> setChecked(settings.value("Archives/CloseProgressDlgWhenExtractFinished",   false).toBool());
}


void ArchivesPage::setRowEnable( int nRow, bool bEnable )
{
	m_pArchiversListView->setEnableCell(nRow, NAMEcol,        bEnable);
	m_pArchiversListView->setEnableCell(nRow, COMPRESSIONcol, bEnable);
	m_pArchiversListView->setEnableCell(nRow, FULLNAMEcol,    bEnable);
	m_pArchiversListView->setEnableCell(nRow, PATHcol,        bEnable);
	m_pArchiversListView->setEnableCell(nRow, EXTRASETcol,    bEnable);

	m_pArchiversListView->setEditableCell(nRow, NAMEcol, false); // because by default is editable

	if (nRow != RARpath && nRow != ZIPpath)
		m_pArchiversListView->setEnableCell(nRow, EXTRASETcol, false, false); // don't need make editable as false, because only enabled cell is set as editable
}

void ArchivesPage::showExtraSettings( int nRow )
{
	QTableWidgetItem *pItem = m_pArchiversListView->item(nRow, NAMEcol);
	QString sCurrentArch = pItem->text();

	if (sCurrentArch != "rar" && sCurrentArch != "zip")
		return;

	QStringList slAddonsSettings;
	if (sCurrentArch == "rar") { // do not change order of this list
		slAddonsSettings.append(tr("Create solid archive"));
		slAddonsSettings.append(tr("Create SFX archive"));
		slAddonsSettings.append(tr("Save symbolic links"));
		slAddonsSettings.append(tr("Save file owner and group"));
		slAddonsSettings.append(tr("Multimedia compression"));
		slAddonsSettings.append(tr("Overwrite existing files"));
	}
	else
	if (sCurrentArch == "zip") { // do not change order of this list
		slAddonsSettings.append(tr("Create SFX archive"));
		slAddonsSettings.append(tr("Save symbolic links"));
		slAddonsSettings.append(tr("Do not save file owner and group and file times"));
	}

	// --- create a dialog and the objects to inserting
	QDialog *pDlg = new QDialog;
	QPushButton *pOkBtn      = new QPushButton(tr("&OK"), pDlg);
	QPushButton *pCancelBtn  = new QPushButton(tr("&Cancel"), pDlg);
	QGridLayout *pGridLayout = new QGridLayout(pDlg);
	//pGridLayout->addWidget(

	//void addWidget (QWidget * widget, int row, int column, Qt::Alignment alignment = 0)
	//QGridLayout *pGridLayout = new QGridLayout(pDlg, 1, 1, 11, 6);
	QSpacerItem *pSpacer1 = new QSpacerItem(16, 21, QSizePolicy::Minimum,   QSizePolicy::Expanding);
	QSpacerItem *pSpacer2 = new QSpacerItem(31, 16, QSizePolicy::Expanding, QSizePolicy::Minimum);

	QLabel *pLabel = new QLabel(pDlg);
	QComboBox *pDictionarySizeCombo = new QComboBox(pDlg);
	if (sCurrentArch == "rar") {
		pDictionarySizeCombo->addItems(QString("64,128,256,512,1024,2048,4096").split(','));
		pLabel->setText(tr("Dictionary size")+" :");
	}
	else
		pDictionarySizeCombo->hide();

	// signals and slots connections
	connect(pOkBtn,      &QPushButton::clicked, pDlg, &QDialog::accept);
	connect(pCancelBtn,  &QPushButton::clicked, pDlg, &QDialog::reject);

	// --- insert the objects into the dialog
	int i;
	QCheckBox *chkBox;
	QList <QCheckBox *> checkBoxList;

	for (i=0; i<slAddonsSettings.count(); i++) {
		chkBox = new QCheckBox(slAddonsSettings[i], pDlg);
		pGridLayout->addWidget(chkBox, i, 0);
		//pGridLayout->addMultiCellWidget(chkBox, i, i, 0, 3);
		checkBoxList << chkBox;
		if (sCurrentArch == "rar")
			chkBox->setChecked(m_ntRarAddsSettings[i]);
		else
		if (sCurrentArch == "zip")
			chkBox->setChecked(m_ntZipAddsSettings[i]);
	}

	if (sCurrentArch == "rar") {
		pGridLayout->addWidget(pLabel, i, 0);
		pGridLayout->addWidget(pDictionarySizeCombo, i, 1);
		pDictionarySizeCombo->setItemText(pDictionarySizeCombo->currentIndex(), QString::number(m_ntRarAddsSettings[DictionarySize]));
		//pGridLayout->addMultiCellWidget(pLabel, i, i, 0, 1);
		//pGridLayout->addMultiCellWidget(pDictionarySizeCombo, i, i, 2, 3);
		i++;
	}
	pGridLayout->addItem(pSpacer1, i, 1);
	i++;
	pGridLayout->addItem(pSpacer2, i, 0);
	pGridLayout->addWidget(pOkBtn, i, 1);
	pGridLayout->addWidget(pCancelBtn, i, 3);

	pDlg->adjustSize();
	pDlg->setWindowTitle(sCurrentArch+" "+tr("extra settings")+" - QtCommander");

	// --- show dialog
	int nResult = pDlg->exec();

	// --- get settings from dialog
	if (nResult > 0) { // 1 == OK
		uint nCount = 0;
		for (int i=0; i<checkBoxList.count(); i++) {
			if (sCurrentArch == "rar")
				m_ntRarAddsSettings[nCount] = checkBoxList.at(i)->isChecked();
			else
			if (sCurrentArch == "zip")
				m_ntZipAddsSettings[nCount] = checkBoxList.at(i)->isChecked();
			nCount++;
		}
		if (sCurrentArch == "rar")
			m_ntRarAddsSettings[DictionarySize] = pDictionarySizeCombo->currentText().toInt(); // KB (*1024);
	}

	delete pDlg;
}

void ArchivesPage::setDefaults()
{
	int nIdOffset;
	QStringList slCompressionLvl;

	QTableWidgetItem *pItem;
	for (int i=0; i<m_pArchiversListView->rowCount(); i++) {
		pItem = m_pArchiversListView->item(i, NAMEcol);
		nIdOffset = (pItem->text() == "zip") ? 1 : 0;
		pItem = m_pArchiversListView->item(i, COMPRESSIONcol);
		slCompressionLvl = pItem->data(Qt::DisplayRole).value<ComboBoxData>().items();
		m_pArchiversListView->setCellValue(i, COMPRESSIONcol, slCompressionLvl, "", (slCompressionLvl.count()/2+nIdOffset));
	}

	// --- set default RAR addons settings
	m_ntRarAddsSettings[CreateSolidArchive]     = true;
	m_ntRarAddsSettings[RarSfxArchive]          = false;
	m_ntRarAddsSettings[RarSaveSymbolicLinks]   = true;
	m_ntRarAddsSettings[SaveFileOwnerAndGroup]  = true;
	m_ntRarAddsSettings[MultimediaCompression]  = false;
	m_ntRarAddsSettings[OverwriteExistingFiles] = false;
	m_ntRarAddsSettings[DictionarySize] = 512;

	// --- set default ZIP addons settings
	m_ntZipAddsSettings[ZipSfxArchive]           = false;
	m_ntZipAddsSettings[ZipSaveSymbolicLinks]    = true;
	m_ntZipAddsSettings[ZipDoNotSaveExtraAttrib] = false;


	m_pWorkingDirPathLnEdit->setText(QDir::homePath()+"/tmp/qtcmd");
	m_pAlwaysOverwriteAddToArchChkBox->setChecked(true);
	m_pAlwaysOverwriteNewArchChkBox->setChecked(true);
    m_pCloseProgressDlgAftterArchFinished->setChecked(false);
	m_pCloseProgressDlgAftterExtrFinished->setChecked(false);
}


void ArchivesPage::slotDetectSupportedArchives()
{
	QString sCmd = "whereis"; // prints paths list, like this: "rar: /usr/bin/rar"
	QString sArguments = "-b";

	QString sArchiverName;
	for (int i=0; i<m_pArchiversListView->rowCount(); i++) {
		sArchiverName = m_pArchiversListView->item(i, NAMEcol)->text();
		if (sArchiverName.indexOf("tar.") != -1) {
			continue;
		}
		sArguments += " "+sArchiverName;
	}
#if QT_VERSION >= QT_VERSION_CHECK(5, 15, 0)
	QStringList slArgs = sArguments.split(QLatin1Char(' '), Qt::SkipEmptyParts);
	qDebug() << "DrivesMenu::runProcess. Start:" << sCmd << slArgs;
	m_pProcess->start(sCmd, slArgs);
#else
	sCmd += sArguments;
	qDebug() << "ArchivesPage::slotDetectSupportedArchives. sCmd:" << sCmd;
	m_pProcess->start(sCmd);
#endif
}

void ArchivesPage::slotSave()
{
	if (m_slArchiverFullNameList.isEmpty())
		return;

	Settings settings;

	settings.setValue("Archives/Autodetect", m_pAutodetect->isChecked());
	// --- write full name for all archivers
	settings.setValue("Archives/Bzip2FullName", m_slArchiverFullNameList[BZIP2path]);
	settings.setValue("Archives/GzipFullName", m_slArchiverFullNameList[GZIPpath]);
	settings.setValue("Archives/XZFullName", m_slArchiverFullNameList[XZpath]);
	settings.setValue("Archives/LZMAFullName", m_slArchiverFullNameList[LZMApath]);
	settings.setValue("Archives/TarFullName", m_slArchiverFullNameList[TARpath]);
	settings.setValue("Archives/RarFullName", m_slArchiverFullNameList[RARpath]);
	settings.setValue("Archives/ZipFullName", m_slArchiverFullNameList[ZIPpath]);
	settings.setValue("Archives/ArjFullName", m_slArchiverFullNameList[ARJpath]);
	// --- write RAR addons settings
	settings.setValue("Archives/RarSolidArchive", m_ntRarAddsSettings[CreateSolidArchive]);
	settings.setValue("Archives/RarSfxArchive", m_ntRarAddsSettings[RarSfxArchive]);
	settings.setValue("Archives/RarSaveLinks", m_ntRarAddsSettings[RarSaveSymbolicLinks]);
	settings.setValue("Archives/RarSaveOwnerAndGroup", m_ntRarAddsSettings[SaveFileOwnerAndGroup]);
	settings.setValue("Archives/RarMultimediaCompression", m_ntRarAddsSettings[MultimediaCompression]);
	settings.setValue("Archives/RarOvrExistingFiles", m_ntRarAddsSettings[OverwriteExistingFiles]);
	settings.setValue("Archives/RarDictionarySize", m_ntRarAddsSettings[DictionarySize]);
	// --- write ZIP addons settings
	settings.setValue("Archives/ZipSfxArchive", m_ntZipAddsSettings[ZipSfxArchive]);
	settings.setValue("Archives/ZipSaveSymbolicLinks", m_ntZipAddsSettings[ZipSaveSymbolicLinks]);
	settings.setValue("Archives/ZipDoNotSaveExtraAttrib", m_ntZipAddsSettings[ZipDoNotSaveExtraAttrib]);

	settings.setValue("Archives/WorkingDirectory", m_pWorkingDirPathLnEdit->text());
	settings.setValue("Archives/AlwaysOverwriteWhenArchiving", m_pAlwaysOverwriteAddToArchChkBox->isChecked());
	settings.setValue("Archives/AlwaysOverwriteWithNewArchive", m_pAlwaysOverwriteNewArchChkBox->isChecked());
	settings.setValue("Archives/CloseProgressDlgWhenArchivFinished",   m_pCloseProgressDlgAftterArchFinished->isChecked());
	settings.setValue("Archives/CloseProgressDlgWhenExtractFinished",   m_pCloseProgressDlgAftterExtrFinished->isChecked());
}

void ArchivesPage::slotChangeWorkingDirectoryPath()
{
	QString sDirName = QFileDialog::getExistingDirectory(this, tr("Select working directory for archivers")+ " - QtCommander", QDir::rootPath());
	if (! sDirName.isEmpty())
		m_pWorkingDirPathLnEdit->setText(sDirName);
}

void ArchivesPage::slotReadDataFromStdOut()
{
	QString sBuffer = m_pProcess->readAllStandardOutput();
	if (sBuffer.isEmpty())
		return;

	QString sArchiverName, sFullName, s;
	QStringList slOutputLines = sBuffer.split('\n');
	QStringList slLine;
	int nRow;
// 	int nMaxRows = m_pArchiversListView->rowCount();

// 	read output from: whereis -b bzip2 gzip xz lzma tar rar zip arj
	for (int i=0; i<slOutputLines.count()-1; i++) { // -1 for skip empty line
		slLine = slOutputLines[i].split(':');
		sArchiverName = slLine[0];
		sFullName = slLine[1].simplified();
		slLine = sFullName.split(' ');

		for (int j=0; j<slLine.count(); j++) {
			if (QFileInfo(slLine[j]).isDir())
				continue;
			if (QFileInfo(slLine[j]).isExecutable()) {
				sFullName = slLine[j];
				break;
			}
		}
		nRow = m_pArchiversListView->findItem(sArchiverName, NAMEcol);
		//qDebug() << "sArchiverName" << sArchiverName << ", sFullName= "<< sFullName << ", nRow=" << nRow;
		if (nRow >= 0) {
			if (! QFileInfo(sFullName).isExecutable()) {
				m_slArchiverFullNameList << "";
				continue;
			}
			m_slArchiverFullNameList << sFullName;
			m_pArchiversListView->setCellValue(nRow, FULLNAMEcol, sFullName);
			if (! sFullName.isEmpty()) {
				m_pArchiversListView->setCellValue(nRow, AVAILABLEcol, "true");
				setRowEnable(nRow, true);
			}
		}
	}

	m_pArchiversListView->resizeColumnsToContents();
	m_pArchiversListView->setFocus(); // need to refresh view
}


void ArchivesPage::slotEditBoxTextApplied( QTableWidgetItem *pItem, const QString &sNewText )
{
	int nRow    = pItem->row();
	int nColumn = pItem->column();
	qDebug() << "ArchivesPage::slotEditBoxTextApplied, (row=" << nRow <<", col=" << nColumn << "), newText=" << sNewText;

	QString sFileName = sNewText;
	if (sFileName.isEmpty()) {
		sFileName = QFileDialog::getOpenFileName(this,
			tr("Select archiver file")+" "+m_pArchiversListView->item(nRow, NAMEcol)->text()+" - QtCommander"
			, "/", tr("All files")+"(*)"
		);
	}
	bool bFail = false;
	if (sFileName.isEmpty()) {
		return;
	}
	QFileInfo file(sFileName);
	if (! file.exists() && ! bFail) {
		bFail = true;
		MessageBox::msg(0, MessageBox::CRITICAL, tr("Incorrect archiver name.")+"\n\n"+tr("File doesn't exist!"));
	}
	else {
		if (file.isDir()) {
			bFail = true;
			MessageBox::msg(0, MessageBox::CRITICAL, tr("Incorrect archiver name.")+"\n\n"+tr("This is directory!"));
		}
		else
		if (! file.isExecutable()) {
			bFail = true;
			MessageBox::msg(0, MessageBox::CRITICAL, tr("Incorrect archiver name.")+"\n\n"+tr("File isn't executable!"));
		}
	}
	if (bFail) {
		m_pArchiversListView->setCellValue(nRow, AVAILABLEcol, "false");
		setRowEnable(nRow, false);
	}
	else {
		m_pArchiversListView->setCellValue(nRow, FULLNAMEcol, sFileName);
		m_pArchiversListView->resizeColumnsToContents(); // fit column to new 'full name' size
		m_pArchiversListView->setFocus(); // need to refresh view
		m_pArchiversListView->selectRow(nRow);
	}
}


void ArchivesPage::slotButtonClicked( QTableWidgetItem *pItem )
{
	int nRow    = pItem->row();
	int nColumn = pItem->column();
	if (nColumn != AVAILABLEcol && nColumn != PATHcol && nColumn != EXTRASETcol)
		return;

	if (nColumn == AVAILABLEcol) {
	 	bool bEnableArch = (m_pArchiversListView->cellValue(nRow, AVAILABLEcol) == "true");
		if (bEnableArch) {
			QString sFileName = m_pArchiversListView->cellValue(nRow, FULLNAMEcol);
			if (sFileName.isEmpty()) {
				sFileName = QFileDialog::getOpenFileName(this,
					tr("Select archiver file")+" "+m_pArchiversListView->item(nRow, NAMEcol)->text()+" - QtCommander"
					, "/", tr("All files")+"(*)"
				);
			}
			if (sFileName.isEmpty()) {
				m_pArchiversListView->setCellValue(nRow, AVAILABLEcol, "false");
				return;
			}
			else {
				m_pArchiversListView->setCellValue(nRow, FULLNAMEcol, sFileName);
				m_pArchiversListView->resizeColumnsToContents(); // fit column to new 'full name' size
				m_pArchiversListView->setFocus(); // need to refresh view
				m_pArchiversListView->selectRow(nRow);
			}
			if (QFileInfo(sFileName).isDir()) {
				m_pArchiversListView->setCellValue(nRow, AVAILABLEcol, "false");
				MessageBox::msg(0, MessageBox::CRITICAL, tr("Incorrect archiver name.")+"\n\n"+tr("This is directory!"));
				return;
			}
			else
			if (! QFileInfo(sFileName).isExecutable()) {
				m_pArchiversListView->setCellValue(nRow, AVAILABLEcol, "false");
				MessageBox::msg(0, MessageBox::CRITICAL, tr("Incorrect archiver name.")+"\n\n"+tr("File isn't executable!"));
				return;
			}
		}
		setRowEnable(nRow, bEnableArch);
	}
	else
	if (nColumn == PATHcol) {
		QString sFullFileName = QFileDialog::getOpenFileName(this,
				tr("Select archiver file")+" "+m_pArchiversListView->item(nRow, NAMEcol)->text()+" - QtCommander"
				, "/", tr("All files")+"(*)");
		if (sFullFileName.isEmpty())
			return;
		if (QFileInfo(sFullFileName).isExecutable()) {
			m_pArchiversListView->setCellValue(nRow, FULLNAMEcol, sFullFileName);
			m_pArchiversListView->setCellValue(nRow, AVAILABLEcol, "true");
			setRowEnable(nRow, true);
			m_pArchiversListView->resizeColumnsToContents(); // fits column to new 'full name' length
			m_pArchiversListView->setFocus(); // need to refresh view
			m_pArchiversListView->selectRow(nRow);
		}
		else
			MessageBox::msg(0, MessageBox::CRITICAL, tr("Incorrect archiver name.")+"\n\n"+tr("File isn't executable!"));
	}
	else
	if (nColumn == EXTRASETcol) {
		showExtraSettings (nRow);
	}
}


QString ArchivesPage::workingDirPath()
{
	return m_pWorkingDirPathLnEdit->text();
}


QString ArchivesPage::compressionLevel( const QString &sArchiverName )
{
	int nRow = m_pArchiversListView->findItem(sArchiverName, NAMEcol);

	if (nRow >= 0 && m_pArchiversListView->cellEnable(nRow, COMPRESSIONcol))
		return m_pArchiversListView->item(nRow, COMPRESSIONcol)->text();

	return QString();
}


QString ArchivesPage::archiverFullName( const QString &sArchiverName )
{
	int nRow = m_pArchiversListView->findItem(sArchiverName, NAMEcol);

	if (nRow >= 0 && m_pArchiversListView->cellEnable(nRow, NAMEcol))
		return m_pArchiversListView->item(nRow, FULLNAMEcol)->text();

	return QString();
}

