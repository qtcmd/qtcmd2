/***************************************************************************
 *   Copyright (C) 2007 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDir>
#include <QFileDialog>

#include "lfspage.h"
#include "settings.h"


LfsPage::LfsPage()
{
	setupUi(this);
	// TODO: we can add on dialog size/weigh of trash, and button for cleaning trash and button to open it in current panel

	connect(m_pSelectPathBtn, &QPushButton::clicked, this, &LfsPage::slotChangeTrashPath);
	connect(m_pMoveToTrash,   &QPushButton::clicked, this, &LfsPage::slotMoveToTheTrash);

	init();
}


void LfsPage::init()
{
	Settings settings;

	m_pMoveToTrash->setChecked(settings.value("LFS/AlwaysMoveToTheTrash", false).toBool());
	m_pTrashPathEdit->setText(settings.value("LFS/TrashDirectory", QDir::homePath()+"/.trash_QtCmd/").toString());

	m_pWeighBeforeCopyChkBox->setChecked(settings.value("LFS/AlwaysWeighBeforeCopy", true).toBool());
	m_pWeighBeforeMoveChkBox->setChecked(settings.value("LFS/AlwaysWeighBeforeMove", true).toBool());
	m_pWeighBeforeRemoveChkBox->setChecked(settings.value("LFS/AlwayspWeighBeforeRemove", true).toBool());

	m_pCloseProgressDlgAfterCpyFinished-> setChecked(settings.value("vfs/CloseProgressDlgWhenCopyFinished",   false).toBool());
	m_pCloseProgressDlgAfterMovFinished-> setChecked(settings.value("vfs/CloseProgressDlgWhenMoveFinished",   false).toBool());
	m_pCloseProgressDlgAftterDelFinished->setChecked(settings.value("vfs/CloseProgressDlgWhenDeleteFinished", false).toBool());

	slotMoveToTheTrash();
}

QString LfsPage::trashPath()
{
	return m_pTrashPathEdit->text();
}

void LfsPage::setDefaults()
{
	m_pMoveToTrash->setChecked(false);
	m_pTrashPathEdit->setText(QDir::homePath()+"/.trash_QtCmd/");

	m_pWeighBeforeCopyChkBox->setChecked(true);
	m_pWeighBeforeMoveChkBox->setChecked(true);
	m_pWeighBeforeRemoveChkBox->setChecked(true);

	m_pCloseProgressDlgAfterCpyFinished->setChecked(false);
	m_pCloseProgressDlgAfterMovFinished->setChecked(false);
	m_pCloseProgressDlgAftterDelFinished->setChecked(false);
}


void LfsPage::slotSave()
{
	Settings settings;

	settings.setValue("LFS/AlwaysMoveToTheTrash", m_pMoveToTrash->isChecked());
	settings.setValue("LFS/TrashDirectory", m_pTrashPathEdit->text());

	settings.setValue("LFS/AlwaysWeighBeforeCopy", m_pWeighBeforeCopyChkBox->isChecked());
	settings.setValue("LFS/AlwaysWeighBeforeMove", m_pWeighBeforeMoveChkBox->isChecked());
	settings.setValue("LFS/AlwayspWeighBeforeRemove", m_pWeighBeforeRemoveChkBox->isChecked());

	settings.setValue("vfs/CloseProgressDlgWhenCopyFinished", m_pCloseProgressDlgAfterCpyFinished->isChecked());
	settings.setValue("vfs/CloseProgressDlgWhenMoveFinished", m_pCloseProgressDlgAfterMovFinished->isChecked());
	settings.setValue("vfs/CloseProgressDlgWhenDeleteFinished", m_pCloseProgressDlgAftterDelFinished->isChecked());
}

void LfsPage::slotChangeTrashPath()
{
	QString sDirName = QFileDialog::getExistingDirectory(this, tr("Select path for trash")+ " - QtCommander", QDir::rootPath());
	if (! sDirName.isEmpty())
		m_pTrashPathEdit->setText(sDirName);
}

void LfsPage::slotMoveToTheTrash()
{
	bool bChecked = m_pMoveToTrash->isChecked();
	m_pSelectPathBtn->setEnabled(bChecked);
	m_pTrashPathEdit->setEnabled(bChecked);
}
