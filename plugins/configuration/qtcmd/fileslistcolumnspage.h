/***************************************************************************
 *   Copyright (C) 2007 by Piotr Mierzwi�ski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _FILESLISTCOLUMNSPAGE_H
#define _FILESLISTCOLUMNSPAGE_H

#include "ui_fileslistcolumnspagedlg.h"

/**
	@author Piotr Mierzwi�ski <piom@nes.pl>
*/
class FilesListColumnsPage : public QWidget, public Ui::FilesListColumnsPageDlg
{
	Q_OBJECT
public:
	static const int MAX_COLUMNS_IN_CFL = 7; // common files list
	static const int MAX_COLUMNS_IN_FFL = 7; // found files list

private:
	int m_ntAlltColumnsWidth[MAX_COLUMNS_IN_CFL];
	int m_ntInitColumnsWidth[MAX_COLUMNS_IN_CFL];

	QButtonGroup *m_pColumnsButtonGroup;

public:
	FilesListColumnsPage();
	~FilesListColumnsPage();

	void init();

	int  columnWidth( int nLogicId );
// 	bool synchronizeColWidthInBothPanels() const { return m_pSynchronizeColumnsWidhtChkBox->isChecked(); }

	/**Set defauls settings, so defaul columns.
	 */
	void setDefaults();


private Q_SLOTS:
	void slotDefaultSettings();

public Q_SLOTS:
	void slotSave();

};

#endif // _FILESLISTCOLUMNSPAGE_H
