/***************************************************************************
 *   Copyright (C) 2007 by Piotr Mierzwi?ski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _COLORSPAGE_H
#define _COLORSPAGE_H

#include <QList>

#include "iconcache.h"
#include "listviewsettings.h"

#include "ui_fileslistcolorspagedlg.h"

/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class FilesListColorsPage : public QWidget, public Ui::FilesListColorsPageDlg
{
	Q_OBJECT
private:
	QList <ColorsScheme> m_cslSchemesList;

	int m_nAllChildren;
// 	int m_nAmountOfSchemes;
	bool m_bAlwaysAskBeforeDeleteScheme;
	QStringList m_slRemovedSchemes;


public:
	FilesListColorsPage();
	~FilesListColorsPage() {}

	enum SourceOfColor {
		WidgetSrc=0, // QPushButton
		StructSrc,   // structure of ColorsScheme
		ConfigSrc    // configuration file
	};

	/** Gets color for given Id using schemes list or directly from widget
	 * @param nColorId id of color, according to @em Colors
	 * @param srcOfColor source of color, defined in @em SourceOfColor
	 * @param sSchemeName name of scheme, empty will be treat as current
	 * Note. Getting color from widget is taking into account only current scheme.
	 * @return color
	 */
	QColor getColor( int nColorId, SourceOfColor srcOfColor=StructSrc, const QString &sSchemeName="" ) const;

	/** Gets scheme with given Id
	 * @param nSchemeId Id of scheme
	 * @return color scheme
	 */
	ColorsScheme & getColorsScheme( int nSchemeId );

	/** Checks if current scheme has set second background color.
	 * @return true if second background color is set, otherwise false
	 */
	bool isSecondBackgroundColor();

	/**Set defauls settings, so defaul color scheme.
	 */
	void setDefaults() {
		m_pSchemeComboBox->setCurrentIndex(0);
		slotChangedScheme(0);
	}


private:
	IconCache *m_pIconCache;


	/** Initialize current widget, so
	 * - loads setting
	 * - makes beutiful list widgets
	 * - createds widgets on list
	 * - draws preview
	 */
	void init();

	/** Returns icon for gicen path, where might contains 2 parts like: "document/open-folder", whereas source file name calls: document-open-folder.png.
	 *  Second case is 1 part, like "settings", whereas source file name calls settings.ong.
	 * @param sIconName path to icon described above
	 * @return icon object
	 */
	QIcon icon( const QString &sIconName ) const {
		return m_pIconCache->icon(sIconName);
	}

	/** Checks if any color in current scheme changed comparing set in configuration file.
	 * @return true if any color changed otherwise false
	 */
	bool colorChangedInCurrentSchema() const;

	/** Drows preview of colors which will be used by list view.
	 */
	void drawPreview();

	/** Updates color with given Id in internal buffer for current scheme.
	 * @param nColorId Id of color to set
	 * @param color color which need to be set
	 */
	void updateCurrentScheme( int nColorId, QColor color );

	/** Updates status of second background color with given value in internal buffer for current scheme.
	 * @param bAlternatingRowColors status of second background color to set
	 */
	void updateCurrentScheme( bool bAlternatingRowColors );


private slots:
	void slotNew();
	void slotDelete();
	void slotChangeColor();
	void slotShowSecondBgColor( bool bShow );
	void slotChangedScheme( int nSchemeNewId );


public slots:
	void slotSave();


};

#endif // _COLORSPAGE_H
