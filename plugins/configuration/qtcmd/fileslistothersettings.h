/***************************************************************************
 *   Copyright (C) 2007 by Piotr Mierzwi?ski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _FILESLISTOTHERSETTINGS_H
#define _FILESLISTOTHERSETTINGS_H

#include <QButtonGroup>

#include "ui_fileslistothersettingsdlg.h"

/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class FilesListOtherSettings : public QWidget, public Ui::FilesListOtherSettingsDlg
{
	Q_OBJECT
public:
	FilesListOtherSettings();
// 	~FilesListOtherSettings();

	/** Initializes settings using configuration file.
	 */
	void init();

	/**Set defauls settings, so defaul other settings.
	 */
	void setDefaults();

public slots:
	/** Saves all settings values into configuration file.
	 */
	void slotSave();

};

#endif // _FILESLISTOTHERSETTINGS_H
