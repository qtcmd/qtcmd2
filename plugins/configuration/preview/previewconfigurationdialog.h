/***************************************************************************
 *   Copyright (C) 2007 by Piotr Mierzwi?ski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _PREVIEWCONFIGURATIONDIALOG_H
#define _PREVIEWCONFIGURATIONDIALOG_H

#include <QDialog>

#include "configurationdialog.h"

#include "fontchooserpage.h"
#include "othertextviewpage.h"
#include "syntaxhighlightingpage.h"

#include "keyshortcutspage.h"
#include "toolbarpage.h"

#include "preview/viewer.h"
#include "keyshortcuts.h"
#include "textviewsettings.h"

#include "ui_previewconfigurationdlg.h"

/**
	@author Piotr Mierzwiński
*/
class PreviewConfigurationDialog : public ConfigurationDialog, public Ui::PreviewConfigurationDlg
{
	Q_OBJECT
private:
	PluginData  *m_pPluginData;
	QDialog     *m_pDialog;

	SyntaxHighlightingPage *m_pSyntaxhighlightingPage;
	OtherTextViewPage      *m_pOtherTextViewPage;
	FontChooserPage        *m_pFontViewPage;
	KeyShortcutsPage       *m_pKeyshortcutsPage;
	ToolBarPage            *m_pToolBarPage;

	QWidget *m_pTextViewPage;
	QWidget *m_pImageViewPage;
	QWidget *m_pVideoViewPage;
	QWidget *m_pSoundViewPage;
	QWidget *m_pBinaryViewPage;
	QWidget *m_pOtherViewPage;
	QWidget *m_pEmptyPage1, *m_pEmptyPage2, *m_pEmptyPage3, *m_pEmptyPage4;

	int m_nTextViewPagesCount, m_nImageViewPagesCount, m_nVideoViewPagesCount, m_nSoundViewPagesCount, m_nBinaryViewPagesCount, m_nOtherViewPagesCount;


	void showCurrentPage( const QString &sSectionName );

public:
	PreviewConfigurationDialog();
	~PreviewConfigurationDialog();

	PluginData *pluginData() { return m_pPluginData; }

	void init( QWidget *pParent, KeyShortcuts *pKeyShortcuts, QList <QActionGroup *> *pActionGroupLst );

	/** Shows this window.
	 * Method is called in FileViewer to show configuration dialog
	 * @param sSectionName name of section to activate
	 */
	void raiseWindow( const QString &sSectionName );

private Q_SLOTS:
	void slotOkButtonPressed();
	void slotApplyButtonPressed();
	void slotSaveButtonPressed();
	void slotCurrentItemChanged( int nItemId );
	void slotSetKeyShortcuts( KeyShortcuts *pKeyShortcuts );
	void slotDefaultsButtonPressed();

	void slotSectionChanged( int nItemId );
	void slotShowPageInSection( int nItemId );
// 	void slotItemSelectionChanged();

//public Q_SLOTS:
//	void shAttribut( int id, SHattribut & shAttiribut );

};

#endif // _PREVIEWCONFIGURATIONDIALOG_H
