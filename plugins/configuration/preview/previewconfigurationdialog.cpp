/***************************************************************************
 *   Copyright (C) 2007 by Piotr Mierzwi?ski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QList>
#include <QDebug>
#include <QToolButton>

#include "../../preview/textviewer/syntaxhighlighter.h"

#include "previewconfigurationdialog.h"


PreviewConfigurationDialog::PreviewConfigurationDialog()
{
	qDebug() << "PreviewConfigurationDialog::PreviewConfigurationDialog.";

	m_pPluginData = new PluginData;
	m_pPluginData->name         = "previewconfigdialog_plugin";
	m_pPluginData->version      = "0.2";
	m_pPluginData->description  = QObject::tr("Plugin handles configuration dialog for preview.");
	m_pPluginData->plugin_class = "config";
	m_pPluginData->mime_type    = "config/preview";

	m_pDialog = nullptr;
	m_pEmptyPage1 = nullptr;
	m_pEmptyPage2 = nullptr;
	m_pEmptyPage3 = nullptr;
	m_pEmptyPage4 = nullptr;
}

PreviewConfigurationDialog::~PreviewConfigurationDialog()
{
	delete m_pEmptyPage1; delete m_pEmptyPage2; delete m_pEmptyPage3; delete m_pEmptyPage4;
	delete m_pPluginData;
	delete m_pDialog;
	qDebug() << "~PreviewConfigurationDialog DESTRUCTOR.";
}


void PreviewConfigurationDialog::init( QWidget *pParent, KeyShortcuts *pKeyShortcuts, QList <QActionGroup *> *pActionGroupLst )
{
// 	qDebug() << "PreviewConfigurationDialog::init.";
	m_pDialog = new QDialog(pParent);
	setupUi(m_pDialog);

	connect(m_pToolBox,  static_cast<void(QToolBox::*)(int)>(&QToolBox::currentChanged), this, &PreviewConfigurationDialog::slotSectionChanged);

	connect(m_pTextViewListWidget,   &QListWidget::currentRowChanged, this, &PreviewConfigurationDialog::slotShowPageInSection);
	connect(m_pImageViewListWidget,  &QListWidget::currentRowChanged, this, &PreviewConfigurationDialog::slotShowPageInSection);
	connect(m_pVideoViewListWidget,  &QListWidget::currentRowChanged, this, &PreviewConfigurationDialog::slotShowPageInSection);
	connect(m_pSoundViewListWidget,  &QListWidget::currentRowChanged, this, &PreviewConfigurationDialog::slotShowPageInSection);
	connect(m_pBinaryViewListWidget, &QListWidget::currentRowChanged, this, &PreviewConfigurationDialog::slotShowPageInSection);
	connect(m_pOtherListWidget,      &QListWidget::currentRowChanged, this, &PreviewConfigurationDialog::slotShowPageInSection);

	connect(m_pOkBtn,       &QPushButton::clicked, this, &PreviewConfigurationDialog::slotOkButtonPressed);
// 	connect(m_pApplyBtn,    &QPushButton::clicked, this, &PreviewConfigurationDialog::slotApplyButtonPressed); // removed from dialog
	connect(m_pDefaultsBtn, &QPushButton::clicked, this, &PreviewConfigurationDialog::slotDefaultsButtonPressed);


// 	m_pToolBox->setMinimumWidth(width()/4);
	m_pToolBox->setMinimumWidth((width()/4)-20);
	m_pToolBox->setMaximumWidth((width()/4)-20);

	setWindowTitle(tr("Configuration for preview")+" - QtCommander");

	// --- TextViewListBox pages
	m_pFontViewPage           = new FontChooserPage(FontChooserPage::PREVIEW_FILE);
	m_pOtherTextViewPage      = new OtherTextViewPage;
	m_pSyntaxhighlightingPage = new SyntaxHighlightingPage;

	m_pStackOfWidgets->addWidget(m_pFontViewPage);
	m_pStackOfWidgets->addWidget(m_pSyntaxhighlightingPage);
	m_pStackOfWidgets->addWidget(m_pOtherTextViewPage);
	m_nTextViewPagesCount = m_pStackOfWidgets->count();

	m_pEmptyPage1 = new QWidget; // temp.till page will be added
	m_pEmptyPage2 = new QWidget; // temp.till page will be added
	m_pEmptyPage3 = new QWidget; // temp.till page will be added
	m_pEmptyPage4 = new QWidget; // temp.till page will be added

	// --- ImageViewListBox pages
	m_pStackOfWidgets->addWidget(m_pEmptyPage1);
	m_nImageViewPagesCount = m_pStackOfWidgets->count();
	// --- VideoViewListBox pages
	m_pStackOfWidgets->addWidget(m_pEmptyPage2);
	m_nVideoViewPagesCount = m_pStackOfWidgets->count();
	// --- SoundViewListBox pages
	m_pStackOfWidgets->addWidget(m_pEmptyPage3);
	m_nSoundViewPagesCount = m_pStackOfWidgets->count();
	// --- BinaryViewListBox pages
	m_pStackOfWidgets->addWidget(m_pEmptyPage4);
	m_nBinaryViewPagesCount = m_pStackOfWidgets->count();

	// --- OtherView pages
	m_pKeyshortcutsPage = new KeyShortcutsPage(false); // false -> previewSettings
	m_pToolBarPage      = new ToolBarPage;

	m_pStackOfWidgets->addWidget(m_pKeyshortcutsPage);
	m_pStackOfWidgets->addWidget(m_pToolBarPage);
	m_nOtherViewPagesCount = m_pStackOfWidgets->count();

	// initialize first item for each section
	m_pTextViewListWidget->setCurrentRow(0);
	m_pImageViewListWidget->setCurrentRow(0);
	m_pVideoViewListWidget->setCurrentRow(0);
	m_pSoundViewListWidget->setCurrentRow(0);
	m_pBinaryViewListWidget->setCurrentRow(0);
	m_pOtherListWidget->setCurrentRow(0);

// 	m_pKeyshortcutsPage->init(true); // true -> appSettings, call here because in constructor is missing, because of connect
	m_pKeyshortcutsPage->setKeyShortcuts(pKeyShortcuts);
 	m_pToolBarPage->init(pActionGroupLst, "view");

	resize(QSize(620, 454).expandedTo(minimumSizeHint()));

	static QList <int> sSW; // width the children of spliter obj.
	sSW << (width()*27)/100 << 0;
	sSW[1] = width()-sSW[0];
	m_pSplitter->setSizes(sSW);
}

void PreviewConfigurationDialog::raiseWindow( const QString &sSectionName )
{
// 	qDebug() << "PreviewConfigurationDialog::raiseWindow. Show page:" << sSectionName;
	if (m_pDialog != nullptr) {
		m_pDialog->show();
		showCurrentPage(sSectionName);
	}
	else
		qDebug() << "PreviewConfigurationDialog::raiseWindow. ERROR. QDialog object not created!";
}

void PreviewConfigurationDialog::showCurrentPage( const QString &sSectionName )
{
// 	qDebug() << "PreviewConfigurationDialog::showCurrentPage. sSectionName:" << sSectionName;
	int nItemId = -1;
	if      (sSectionName == "TextViewPage") {
		nItemId = m_pTextViewListWidget->currentRow();
		m_pTextViewListWidget->setFocus();
	}
	else if (sSectionName == "ImageViewPage") {
		nItemId = m_pImageViewListWidget->currentRow();
		m_pImageViewListWidget->setFocus();
	}
	else if (sSectionName == "VideoViewPage") {
		nItemId = m_pVideoViewListWidget->currentRow();
		m_pVideoViewListWidget->setFocus();
	}
	else if (sSectionName == "SoundViewPage") {
		nItemId = m_pSoundViewListWidget->currentRow();
		m_pSoundViewListWidget->setFocus();
	}
	else if (sSectionName == "BinaryViewPage") {
		nItemId = m_pBinaryViewListWidget->currentRow();
		m_pBinaryViewListWidget->setFocus();
	}
	else if (sSectionName == "OtherViewPage") {
		nItemId = m_pOtherListWidget->currentRow();
		m_pOtherListWidget->setFocus();
	}

	slotShowPageInSection(nItemId);
}

// ---------- S L O T S -----------

void PreviewConfigurationDialog::slotSectionChanged( int /*nItemId*/ )
{
	QString sSectionName = m_pToolBox->currentWidget()->objectName();
	showCurrentPage(sSectionName);
}

void PreviewConfigurationDialog::slotShowPageInSection( int nItemId )
{
	QString sTitle;
	QString sSectionName = m_pToolBox->currentWidget()->objectName();
	int nListWidgetsRow = (nItemId < 0) ? 0 : nItemId;
// 	qDebug() << "PreviewConfigurationDialog::slotShowPageInSection. nItemId:" << nItemId << "nListWidgetsRow:" << nListWidgetsRow << "sSectionName:" << sSectionName;
	if (nItemId < 0)
		sTitle = tr("No options defined");

	if (sSectionName == "TextViewPage") {
		if (nItemId >= 0)
			sTitle = m_pTextViewListWidget->item(nListWidgetsRow)->text();
	}
	else
	if (sSectionName == "ImageViewPage") {
		if (nItemId > 0)
			sTitle = m_pImageViewListWidget->item(nListWidgetsRow)->text();
		nListWidgetsRow += m_nTextViewPagesCount; // +pageOffset
	}
	else
	if (sSectionName == "VideoViewPage") {
		if (nItemId > 0)
			sTitle = m_pVideoViewListWidget->item(nListWidgetsRow)->text();
        nListWidgetsRow += m_nImageViewPagesCount; // +pageOffset
	}
	else
	if (sSectionName == "SoundViewPage") {
		if (nItemId > 0)
			sTitle = m_pSoundViewListWidget->item(nListWidgetsRow)->text();
        nListWidgetsRow += m_nVideoViewPagesCount; // +pageOffset
	}
	else
	if (sSectionName == "BinaryViewPage") {
		if (nItemId > 0)
			sTitle = m_pBinaryViewListWidget->item(nListWidgetsRow)->text();
        nListWidgetsRow += m_nSoundViewPagesCount; // +pageOffset
	}
	else
	if (sSectionName == "OtherViewPage") {
		if (nItemId > 0)
			sTitle = m_pOtherListWidget->item(nListWidgetsRow)->text();
        nListWidgetsRow += m_nBinaryViewPagesCount; // +pageOffset
	}
// 	qDebug() << "PreviewConfigurationDialog::slotShowPageInSection. nListWidgetsRow:" << nListWidgetsRow << "sSectionName:" << sSectionName << "sTitle:" << sTitle;

	m_pStackOfWidgets->setCurrentIndex(nListWidgetsRow);
	m_pTitleLab->setText(sTitle);
}

void PreviewConfigurationDialog::slotDefaultsButtonPressed()
{
	QString sCurrentPageName = m_pToolBox->itemText(m_pToolBox->currentIndex());
	QString sCurrentItem;

	if (sCurrentPageName == tr("Text preview")) {
		sCurrentItem = m_pTextViewListWidget->currentItem()->text();

		if (sCurrentItem == tr("Fonts"))
			m_pFontViewPage->setDefaults();
		else if (sCurrentItem == tr("Syntax highlighting"))
			m_pSyntaxhighlightingPage->setDefaults();
		else if (sCurrentItem == tr("Other"))
			m_pOtherTextViewPage->setDefaults();
	}
	else
	if (sCurrentPageName == tr("Image preview")) {
	}
	else
	if (sCurrentPageName == tr("Video preview")) {
	}
	else
	if (sCurrentPageName == tr("Sound preview")) {
	}
	else
	if (sCurrentPageName == tr("Bonary preview")) {
	}
	else
	if (sCurrentPageName == tr("Other")) {
		sCurrentItem = m_pOtherListWidget->currentItem()->text();

		if (sCurrentItem == tr("Keyshortcuts"))
			m_pKeyshortcutsPage->setDefaults();
		if (sCurrentItem == tr("Tool bars"))
			m_pToolBarPage->setDefaults();
	}
}

void PreviewConfigurationDialog::slotSetKeyShortcuts( KeyShortcuts *pKeyShortcuts )
{
	if (m_pKeyshortcutsPage != nullptr)
		m_pKeyshortcutsPage->setKeyShortcuts(pKeyShortcuts);
}


void PreviewConfigurationDialog::slotOkButtonPressed()
{
// 	qDebug() << "PreviewConfigurationDialog::slotOkButtonPressed";
	slotApplyButtonPressed();
	m_pFontViewPage->slotSave();
	m_pSyntaxhighlightingPage->slotSave();
// 	m_pOtherTextViewPage->slotSave();
	m_pKeyshortcutsPage->slotSave();
	m_pToolBarPage->slotSave();

// 	Q_EMIT ConfigurationDialog::signalSaveSettings();

	m_pDialog->close();
}

void PreviewConfigurationDialog::slotApplyButtonPressed()
{
	QString sCurrentPageName = m_pToolBox->itemText(m_pToolBox->currentIndex());
	QString sCurrentItem;
// 	qDebug() << "QtCmdConfigurationDialog::slotApplyButtonPressed. sCurrentPageName:" << sCurrentPageName;

	// --- inits the file list settings
	if (sCurrentPageName == tr("Text preview")) {
		TextViewSettings textViewSettings;
		sCurrentItem = m_pTextViewListWidget->currentItem()->text();
// 		qDebug() << "QtCmdConfigurationDialog::slotApplyButtonPressed. Files List->Item:" << sCurrentItem;

		if (sCurrentItem == tr("Fonts")) {
			textViewSettings.fontFamily = m_pFontViewPage->fontFamily();
			textViewSettings.fontSize   = m_pFontViewPage->fontSize();
			textViewSettings.fontBold   = m_pFontViewPage->fontBold();
			textViewSettings.fontItalic = m_pFontViewPage->fontItalic();
		}
		else
		if (sCurrentItem == tr("Syntax highlighting")) {
			if (m_pSyntaxhighlightingPage->highlightingChanged()) {
				SHattribut newSHattribut;
				for (int i=0; i<11; i++) { // TODO name this constant value
					m_pSyntaxhighlightingPage->shAttribut(i, newSHattribut);
					textViewSettings.shAttrib[i].color  = newSHattribut.color;
					textViewSettings.shAttrib[i].bold   = newSHattribut.bold;
					textViewSettings.shAttrib[i].italic = newSHattribut.italic;
				}
			}
			SyntaxHighlighter::SyntaxHighlighting sh;
			int currentSH = m_pSyntaxhighlightingPage->currentHighlighting();
			if (currentSH == 0)
				sh = SyntaxHighlighter::CCpp;
			else
			if (currentSH == 1)
				sh = SyntaxHighlighter::HTML;
			else
				sh = SyntaxHighlighter::NONE;
			textViewSettings.currentSyntaxHighlighting = sh;
		}
		else
		if (sCurrentItem == tr("Other")) {
			textViewSettings.tabWidth       = m_pOtherTextViewPage->tabWidth();
			textViewSettings.externalEditor = m_pOtherTextViewPage->externalEditorPath();
		}
		Q_EMIT ConfigurationDialog::signalApply(textViewSettings, sCurrentItem);
	}
/*	else
	if (sCurrentPageName == tr("Image preview")) {
		ImageViewSettings imageViewSettings;
		Q_EMIT ConfigurationDialog::signalApply(imageViewSettings, sCurrentItem);
	}
	else
	if (sCurrentPageName == tr("Video preview")) {
		VideoViewSettings videoViewSettings;
		Q_EMIT ConfigurationDialog::signalApply(videoViewSettings, sCurrentItem);
	}
	else
	if (sCurrentPageName == tr("Sound preview")) {
		SoundViewSettings soundViewSettings;
		Q_EMIT ConfigurationDialog::signalApply(soundViewSettings, sCurrentItem);
	}
	else
	if (sCurrentPageName == tr("Binary preview")) {
		BinaryViewSettings binaryViewSettings;
		Q_EMIT ConfigurationDialog::signalApply(binaryViewSettings, sCurrentItem);
	}*/
	else // --- inits the other apps. settings
	if (sCurrentPageName == tr("Other")) {
		sCurrentItem = m_pOtherListWidget->currentItem()->text();

		if (sCurrentItem == tr("Keyshortcuts"))
			Q_EMIT ConfigurationDialog::signalApplyKeyShortcuts(m_pKeyshortcutsPage->keyShortcuts());
		else
		if (sCurrentItem == tr("Tool bars")) {
			ToolBarPageSettings base, view, edit;
			base.tbName = "Base";	base.tbActions = m_pToolBarPage->actions("Base");
			view.tbName = "View";	view.tbActions = m_pToolBarPage->actions("View");
			edit.tbName = "Edit";	edit.tbActions = m_pToolBarPage->actions("Edit");

			Q_EMIT ConfigurationDialog::signalApply(base);
			Q_EMIT ConfigurationDialog::signalApply(view);
			Q_EMIT ConfigurationDialog::signalApply(edit);
		}
	}
}

