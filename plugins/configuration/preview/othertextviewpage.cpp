/***************************************************************************
 *   Copyright (C) 2007 by Piotr Mierzwi?ski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDebug>
#include <QFileDialog>

#include "settings.h"
#include "othertextviewpage.h"


OtherTextViewPage::OtherTextViewPage( QWidget */*pParent*/ )
{
	setupUi(this);

	connect(m_pExtEditorChkBox, &QCheckBox::toggled,  this, &OtherTextViewPage::slotEnableDisableExtEditor);
	connect(m_pSelectPathBtn, &QPushButton::released, this, &OtherTextViewPage::slotSelectPathForExtApp);

	m_pExtEditorChkBox->setEnabled(false);

	init();
}


void OtherTextViewPage::init()
{
	Settings settings;

	int  nTabWidth = settings.value("textViewer/TabWidth", 4).toInt();
	m_pTabSizeSpinBox->setValue(nTabWidth);

	bool bUseExtEditor = settings.value("textViewer/UseExternalEditor", false).toBool();
	m_pExtEditorChkBox->setChecked(bUseExtEditor);

	QString sExternalEditorPath = settings.value("textViewer/ExternalEditorPath", "").toString();
	m_pExtEditorPathLnEdit->setText(sExternalEditorPath);
	slotEnableDisableExtEditor(bUseExtEditor);
}

void OtherTextViewPage::setDefaults()
{
	m_pTabSizeSpinBox->setValue(4);
	m_pExtEditorChkBox->setChecked(false);
	m_pExtEditorPathLnEdit->setText("");
	slotEnableDisableExtEditor(false);
}

int OtherTextViewPage::tabWidth() const
{
	return m_pTabSizeSpinBox->value();
}

QString OtherTextViewPage::externalEditorPath() const
{
	return (m_pExtEditorChkBox->isChecked()) ? m_pExtEditorPathLnEdit->text() : QLatin1String("");
}



void OtherTextViewPage::slotEnableDisableExtEditor( bool bEnable )
{
	m_pExtEditorPathLnEdit->setEnabled(bEnable);
	m_pSelectPathBtn->setEnabled(bEnable);
}

void OtherTextViewPage::slotSelectPathForExtApp()
{
	QFileDialog fd(this, tr("Select application to edit..."), "/usr");
	fd.setFileMode(QFileDialog::ExistingFile);
	QString sSelectedAppName;
	if (fd.exec()) {
		sSelectedAppName = fd.selectedFiles().at(0);
// 		qDebug() << "OtherTextViewPage::slotSelectPathForExtApp. Selected application:" << sSelectedAppName;
	}
	if (! sSelectedAppName.isEmpty())
		m_pExtEditorPathLnEdit->setText(sSelectedAppName);
}

void OtherTextViewPage::slotSave()
{
	qDebug() << "OtherTextViewPage::slotSave";
	Settings settings;
	settings.setValue("textViewer/TabWidth", m_pTabSizeSpinBox->value());
	settings.setValue("textViewer/UseExternalEditor", m_pExtEditorChkBox->isChecked());
	settings.setValue("textViewer/ExternalEditorPath", m_pExtEditorPathLnEdit->text());
}

// TODO Needs to check if pressed OK/Apply on main window if enabled external application and path is empty
