/***************************************************************************
                          textviewsettings.h  -  description
                             -------------------
    begin                : sat apr 10 2004
    copyright            : (C) 2004 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef _TEXTVIEWSETTINGS_H_
#define _TEXTVIEWSETTINGS_H_

#include <qcolor.h>

struct SHattribut {
	QColor color;
	bool bold;
	bool italic;
};

struct TextViewSettings {
	QString fontFamily;
	uint fontSize;
	bool fontBold;
	bool fontItalic;
	uint tabWidth;
	QString externalEditor;
	SHattribut shAttrib[11];
	int currentSyntaxHighlighting;
};

#endif
