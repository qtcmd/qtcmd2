/***************************************************************************
 *   Copyright (C) 2007 by Piotr Mierzwi?ski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef OTHERTEXTVIEWPAGE_H
#define OTHERTEXTVIEWPAGE_H

#include "ui_othertextviewpagedlg.h"

/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class OtherTextViewPage : public QWidget, public Ui::OtherTextViewPageDlg
{
	Q_OBJECT
public:
	OtherTextViewPage( QWidget *pParent=nullptr );

//private:
	void init();
	int  tabWidth() const;
	QString externalEditorPath() const;

	/**Set defauls settings, so tool bar settings.
	 */
	void setDefaults();

private Q_SLOTS:
	void slotEnableDisableExtEditor( bool bEnable );
	void slotSelectPathForExtApp();

public Q_SLOTS:
	void slotSave();

};

#endif // OTHERTEXTVIEWPAGE_H
