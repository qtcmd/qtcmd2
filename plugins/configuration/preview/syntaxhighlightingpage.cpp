/***************************************************************************
 *   Copyright (C) 2007 by Piotr Mierzwi?ski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QtGui>
#include <QTableWidgetItem>

#include "settings.h"
#include "syntaxhighlightingpage.h"


SyntaxHighlightingPage::SyntaxHighlightingPage()
{
	setupUi(this);

	connect(m_pHighlightingComboBox,  static_cast<void(QComboBox::*)(int)>(&QComboBox::activated), this, &SyntaxHighlightingPage::slotSelectHighlighting);

	init();
}


void SyntaxHighlightingPage::init()
{
	// prepare view
	// -- adding columns  TODO make 'add column' support for plugin ListWidgetView
	m_pListWidgetView->addColumn(ListWidgetView::TextLabel, tr("Context")/*, Qt::AlignmentFlag eColumnAlign=Qt::AlignLeft*/);
	m_pListWidgetView->addColumn(ListWidgetView::CheckBox,  tr("Bold"));
	m_pListWidgetView->addColumn(ListWidgetView::CheckBox,  tr("Italic"));
	m_pListWidgetView->addColumn(ListWidgetView::ColorChooser, tr("Color"));
	m_pListWidgetView->addColumn(ListWidgetView::CheckBox,  tr("Use default"));

	Settings settings;

	QStringList slHighlightingList;// = settings.readListEntry("textViewer/SyntaxHighlighting/HighlightingList");
	slHighlightingList << tr("Source/C,C++") << tr("Markup/HTML");

	if (slHighlightingList.count() > 0)
		m_pHighlightingComboBox->addItems(slHighlightingList);

	m_CCppMap.clear();
	m_HtmlMap.clear();
	m_CCppInitMap.clear();
	m_HtmlInitMap.clear();

	// --- init default highlightings
	// C/C++
	m_CCppInitMap.append("Base N Integer;(default)FF00FF,,;");
	m_CCppInitMap.append("Character;(default)008080,,;");
	m_CCppInitMap.append("Comment;(default)808080,,Italic;");
	m_CCppInitMap.append("Data type;(default)800000,,;");
	m_CCppInitMap.append("Decimal or Value;(default)0000FF,,;");
	m_CCppInitMap.append("Extentions;(default)0095FF,Bold,;");
	m_CCppInitMap.append("Floating point;(default)800080,,;");
	m_CCppInitMap.append("Key word;(default)000000,Bold,;");
	m_CCppInitMap.append("Normal;(default)000000,,;");
	m_CCppInitMap.append("Preprocesor;(default)008000,,;");
	m_CCppInitMap.append("String;(default)FF0000,,;");

	// HTML (an empty items for void SettingsViewDialog::slotApplyButtonPressed())
	m_HtmlInitMap.append(";,,;");
	m_HtmlInitMap.append(";,,;");
	m_HtmlInitMap.append("Comment;(default)808080,,Italic;");
	m_HtmlInitMap.append("Data type;(default)800000,,;");
	m_HtmlInitMap.append("Decimal or Value;(default)0000FF,,;");
	m_HtmlInitMap.append(";,,;");
	m_HtmlInitMap.append(";,,;");
	m_HtmlInitMap.append("Key word;(default)000000,Bold,;");
	m_HtmlInitMap.append("Normal;(default)000000,,;");
	m_HtmlInitMap.append(";,,;");
	m_HtmlInitMap.append("String;(default)FF0000,,;");

	// --- read from config file
	QString sSchemeFullName, sScheme, sContext, sDefaultValue, sValue, sData; // = "Highlighting_"+m_pHighlightingComboBox->currentText();
	QStringList slScheme;
	for (int si=0;si < slHighlightingList.count(); si++) {
		sScheme = slHighlightingList.at(si);
		sSchemeFullName = "Highlighting_"+sScheme;
		sSchemeFullName.replace('/', '-');

		if (sScheme == tr("Source/C,C++"))
			slScheme = m_CCppInitMap;
		else
		if (sScheme == tr("Markup/HTML"))
			slScheme = m_HtmlInitMap;

		for (int mi=0;mi < slScheme.count(); mi++) {
			sContext = slScheme[mi].section(';', 0,0);
			if (! sContext.isEmpty()) {
				sDefaultValue = slScheme[mi].section(';', 1,1);
				sDefaultValue.replace("(default)", "");
				sValue = settings.value(sSchemeFullName+"/"+sContext, sDefaultValue).toString();
				//qDebug() << sScheme << sContext << "newValue:" << sValue << "defaultValue:" << sDefaultValue;

				if (sScheme == tr("Source/C,C++"))
					m_CCppMap.append(sContext+";"+sValue+";");
				else
				if (sScheme == tr("Markup/HTML"))
					m_HtmlMap.append(sContext+";"+sValue+";");
			}
		}
	}

	// --- set default highlighting
	m_bInsertDefault = false;
	slotSelectHighlighting(CCpp);

	m_pListWidgetView->selectRow(0);
	m_pListWidgetView->resizeColumnsToContents();

	connect(m_pListWidgetView,  static_cast<void(ListWidgetView::*)(QTableWidgetItem *)>(&ListWidgetView::buttonClicked), this, &SyntaxHighlightingPage::slotButtonClicked);
	connect(m_pListWidgetView,  static_cast<void(ListWidgetView::*)(QTableWidgetItem *)>(&ListWidgetView::colorChanged),  this, &SyntaxHighlightingPage::slotColorChanged);
}

void SyntaxHighlightingPage::setDefaults()
{
	m_bInsertDefault = true;
	slotSelectHighlighting(m_pHighlightingComboBox->currentIndex());

	// --- update main 'slShMap'
	QStringList slShMap;
	int nCurrentHighlighting = m_pHighlightingComboBox->currentIndex();

	if (nCurrentHighlighting == CCpp)
		slShMap = m_CCppMap;
	else
	if (nCurrentHighlighting == Html)
		slShMap = m_HtmlMap;

	QString sData;
	for (int i=0; i<slShMap.count(); i++) {
		sData = slShMap[i];
		if (! sData.section(';', 2,2).isEmpty()) // if sDefineSH exists then
			slShMap[i] = sData.left(sData.lastIndexOf(';')+1); // remove it
	}
	// --- update main '*Map'
	if (nCurrentHighlighting == CCpp)
		m_CCppMap = slShMap;
	else
	if (nCurrentHighlighting == Html)
		m_HtmlMap = slShMap;
}

void SyntaxHighlightingPage::shAttribut( int id, SHattribut &shAttiribut )
{
	if (id > 10)
		return;

	QStringList slShMap;
	int nCurrentHighlighting = m_pHighlightingComboBox->currentIndex();

	if (nCurrentHighlighting == CCpp)
		slShMap = m_CCppMap;
	else
	if (nCurrentHighlighting == Html)
		slShMap = m_HtmlMap;

	QString sh = slShMap[id];
	QString sData;
	if (sh.at(sh.length()-1) == ';') // is default only
		sData = sh.section(';', 1,1);
	else { // defined
		sData = sh;
		sData = sData.remove(0, sData.lastIndexOf(';')+1); // remove default
	}

	shAttiribut.bold   = (sData.section(',', 1,1).toLower() == "bold");
	shAttiribut.italic = (sData.section(',', 2,2).toLower() == "italic");

	sData = sData.section(',', 0,0);
	if (sData.indexOf("(default)") == 0)
		sData.remove(0, 9); // remove string '(default)'

//	shAttiribut.color = color(sData);
	shAttiribut.color = QColor(sData);
}

bool SyntaxHighlightingPage::highlightingChanged()
{
	for (int i=0; i<m_pListWidgetView->rowCount(); i++) {
		if (m_pListWidgetView->cellValue(i, UseDefaultCOL) == "true")
			return true;
	}

	return false;
}

int SyntaxHighlightingPage::currentHighlighting()
{
	return m_pHighlightingComboBox->currentIndex();
}


void SyntaxHighlightingPage::slotSelectHighlighting( int nCurrentHighlighting )
{
	QStringList slShMap;
	QString sData, sDefineSH, sDefaultSH;
	ListWidgetView::TextStyle eBold, eItalic;

	if (nCurrentHighlighting == CCpp)
		slShMap = m_CCppMap;
	else
	if (nCurrentHighlighting == Html)
		slShMap = m_HtmlMap;

	m_pListWidgetView->removeRows();

	int nRow = 0;
	for (int i=0; i<slShMap.count(); ++i) {
		if (slShMap[i].section(';', 0,0).isEmpty())
			continue;

		m_pListWidgetView->addRow();

		sDefaultSH = slShMap[i].section(';', 1,1);
		sDefineSH  = slShMap[i].section(';', 2,2);
		m_pListWidgetView->setCellValue(nRow, ContextCOL, slShMap[i].section(';', 0,0));

		if (m_bInsertDefault)
			sData = sDefaultSH;
		else
			sData = (sDefineSH.isEmpty()) ? sDefaultSH : sDefineSH;

		if (sData.section(',', 1,1).toLower() == "bold")
			m_pListWidgetView->setCellValue(nRow, BoldCOL, "true");
		if (sData.section(',', 2,2).toLower() == "italic")
			m_pListWidgetView->setCellValue(nRow, ItalicCOL, "true");

		sData = sData.section(',', 0,0);
		if (sData.indexOf("(default)") == 0) {
			m_pListWidgetView->setCellValue(nRow, UseDefaultCOL, "true");
			sData.remove(0, 9); // remove string '(default)'
		}

		eBold   = (m_pListWidgetView->cellValue(nRow, BoldCOL)   == "true") ? ListWidgetView::Bold   : ListWidgetView::Regular;
		eItalic = (m_pListWidgetView->cellValue(nRow, ItalicCOL) == "true") ? ListWidgetView::Italic : ListWidgetView::Regular;
		if (! sData.isEmpty())
			m_pListWidgetView->setCellValue(nRow, ColorCOL, "#"+sData);

		m_pListWidgetView->setCellAttributes(nRow, ContextCOL, QColor(m_pListWidgetView->cellValue(nRow, ColorCOL)), (ListWidgetView::TextStyle)(eBold | eItalic));
		nRow++;
	}

	m_bInsertDefault = false;
}

void SyntaxHighlightingPage::slotButtonClicked( QTableWidgetItem *pItem )
{
	QString sData, sValue;
	QStringList slShMap;
	int nCurrentHighlighting = m_pHighlightingComboBox->currentIndex();

	if (nCurrentHighlighting == CCpp)
		slShMap = m_CCppMap;
	else
	if (nCurrentHighlighting == Html)
		slShMap = m_HtmlMap;

	int nRow    = pItem->row();
	int nColumn = pItem->column();
	// --- search id of clicked pItem
	int id;
	bool bFound = false;
	for (id=0; id<slShMap.count(); id++) {
		if (slShMap[id].section(';', 0,0) == m_pListWidgetView->item(nRow, ContextCOL)->text()) {
			bFound = true;
			break;
		}
	}
	if (! bFound)
		return;

	// --- set default value
	if (nColumn == UseDefaultCOL) {
		if (m_pListWidgetView->cellValue(nRow, UseDefaultCOL) == "true") {
			//qDebug() << "default is true";
			sData = slShMap[id].section(';', 1,1);

			sValue = sData.section(',', 1,1).toLower();
			m_pListWidgetView->setCellValue(nRow, BoldCOL,   (sValue == "bold" ) ? "true" : "false");
			sValue = sData.section(',', 2,2).toLower();
			m_pListWidgetView->setCellValue(nRow, ItalicCOL, (sValue == "italic") ? "true" : "false");

			sData.remove(0, 9); // remove string '(default)'

			m_pListWidgetView->setCellValue(nRow, ColorCOL, "#"+sData.section(',', 0,0));
			sData = slShMap[id].left(slShMap[id].lastIndexOf(';')+1); // init by default value
		}
	}
	else {
		m_pListWidgetView->setCellValue(nRow, UseDefaultCOL, "false");

		int nDefineSHid = slShMap[id].indexOf(';')+1;
		sData = slShMap[id].left(nDefineSHid);
		//sData.remove(nDefineSHid, slShMap[id].length()-nDefineSHid);
		sData += m_pListWidgetView->cellValue(nRow, ColorCOL)+",";
		sData.remove(QChar('#'));
		if (m_pListWidgetView->cellValue(nRow, BoldCOL) == "true")
			sData += "Bold";
		sData += ",";
		if (m_pListWidgetView->cellValue(nRow, ItalicCOL) == "true")
			sData += "Italic";
	}
	sData += ";";

// --- update main '*Map'
	if (nCurrentHighlighting == CCpp)
		m_CCppMap[id] = sData;
	else
	if (nCurrentHighlighting == Html)
		m_HtmlMap[id] = sData;

// the *Map formats:
//   ContextName;(default)color,bold,itali;
//   ContextName;(default)color,bold,itali;color,bold,italic

	int nTxtWeight = ListWidgetView::Regular;
	if (m_pListWidgetView->cellValue(nRow, BoldCOL) == "true")
		nTxtWeight |= (ListWidgetView::TextStyle)ListWidgetView::Bold;
	if (m_pListWidgetView->cellValue(nRow, ItalicCOL) == "true")
		nTxtWeight |= (ListWidgetView::TextStyle)ListWidgetView::Italic;

	m_pListWidgetView->setCellAttributes(nRow, ContextCOL, QColor(m_pListWidgetView->cellValue(nRow, ColorCOL)), (ListWidgetView::TextStyle)nTxtWeight);

	m_bInsertDefault = false;
}

void SyntaxHighlightingPage::slotSave()
{
	// FIXME Temporary saving highlighting settings into configuration file

	QString sScheme = "Highlighting_"+m_pHighlightingComboBox->currentText();
	qDebug() << "SyntaxHighlightingPage::slotSave. scheme:" << sScheme;
	QStringList slShMap;
	QString sData, sContext;
	Settings settings;

	int nCurrentHighlighting = m_pHighlightingComboBox->currentIndex();

	if (nCurrentHighlighting == CCpp)
		slShMap = m_CCppMap;
	else
	if (nCurrentHighlighting == Html)
		slShMap = m_HtmlMap;

	for (int i=0; i<slShMap.count(); ++i) {
		if (slShMap[i].section(';', 0,0).isEmpty())
			continue;

		sContext = slShMap[i].section(';', 0,0);
		sData = slShMap[i].section(';', 1,1);
		sData.replace("(default)", "");
		sScheme.replace('/', '-');

		qDebug() << "setting.update" << sScheme+"-"+sContext << sData;
		settings.update(sScheme+"/"+sContext, sData);
	}
}

