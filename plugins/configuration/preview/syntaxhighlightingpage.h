/***************************************************************************
 *   Copyright (C) 2007 by Piotr Mierzwi?ski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef SYNTAXHIGHLIGHTINGPAGE_H
#define SYNTAXHIGHLIGHTINGPAGE_H

#include <QWidget>

#include "textviewsettings.h"
#include "ui_syntaxhighlightingpagedlg.h"

/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class SyntaxHighlightingPage : public QWidget, public Ui::SyntaxHighlightingPageDlg
{
	Q_OBJECT
private:
	enum Columns { ContextCOL=0, BoldCOL, ItalicCOL, ColorCOL, UseDefaultCOL };
	enum Highlighting { CCpp=0, Html };

	QStringList m_CCppInitMap, m_HtmlInitMap;
	QStringList m_CCppMap, m_HtmlMap;
	bool m_bInsertDefault;

public:
	SyntaxHighlightingPage();

	void init();

	bool highlightingChanged();
	int  currentHighlighting();

	/**Set defauls settings, so tool bar settings.
	 */
	void setDefaults();

private slots:
	void slotSelectHighlighting( int nCurrentHighlighting );
	void slotSetDefaultHighlighting();
	void slotButtonClicked( QTableWidgetItem *pItem );
	void slotColorChanged( QTableWidgetItem *pItem ) {
		slotButtonClicked(pItem);
	}

public slots:
	void shAttribut( int id, SHattribut & shAttiribut );
	void slotSave();

};

#endif // SYNTAXHIGHLIGHTINGPAGE_H
