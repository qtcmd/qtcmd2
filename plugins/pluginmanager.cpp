/***************************************************************************
 *   Copyright (C) 2007 by Mariusz Borowski <b0mar@nes.pl>                 *
 *   Copyright (C) 2011 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDebug> // needed for qDebug()

#include "pluginmanager.h"
#include "pluginfactory.h"


PluginManager::PluginManager()
{
	qDebug() << "PluginManager::PluginManager. PRIVATE (empty).";
}

PluginManager::~PluginManager()
{
	qDebug() << "PluginManager::~PluginManager. DESTRUCTOR (empty)";
	// Note. All subsystems will be destroyed in PluginFactory destructor.
}


PluginManager* PluginManager::instance()
{
	static PluginManager pm;
	return &pm;
}

void *PluginManager::getObject( const QString &sMime, const QString &sPluginClass ) const
{
	if (sMime.isEmpty()) {
		qDebug() << "PluginManager::getObject. Internal error. Came empty mime!";
		return NULL;
	}
	if (sPluginClass.isEmpty()) {
		qDebug() << "PluginManager::getObject. Internal error. Came empty sPluginClass!";
		return NULL;
	}
	if (sPluginClass == "config") {
		if (sMime != "config/app" && sMime != "config/preview") {
			qDebug() << "PluginManager::getObject. Internal error. Unknown mime type for config plugin class. Handled are only: 'config/app' and 'config/preview'";
			return NULL;
		}
	}

	void *pObject = NULL;
	PluginFactory *pluginFactory = PluginFactory::instance();

	if (sPluginClass == "viewer")
		pObject = pluginFactory->createObject(sMime, sPluginClass);
	else // subsystem and others (already created)
	if (sPluginClass == "subsystem")
		pObject = pluginFactory->getObject(sMime, sPluginClass);
	else
	if (sPluginClass == "config")
		pObject = pluginFactory->getObject(sMime, sPluginClass);

	return pObject;
}

Vfs::Subsystem *PluginManager::subsystem( const QString &sMime )
{
	void *pObject = getObject(sMime, "subsystem");
	if (pObject == NULL)
		return NULL;

	m_currentSubsystem = (Vfs::Subsystem *)(pObject);
	return m_currentSubsystem;
}

Preview::Viewer *PluginManager::viewer( const QString &sMime )
{
	void *pObject = getObject(sMime, "viewer"); // new obj.is created
	if (pObject == NULL)
		return NULL;

	m_currentViewer = (Preview::Viewer *)(pObject);
	return m_currentViewer;
}

ConfigurationDialog *PluginManager::config( const QString &sMime )
{
	void *pObject = getObject(sMime, "config"); // new obj.is created
	if (pObject == NULL)
		return NULL;

	m_currentConfigurationDlg = (ConfigurationDialog *)(pObject);
	return m_currentConfigurationDlg;
}


bool PluginManager::isPluginRegistrered( const QString &sMime, const QString &sPluginClass ) const
{
	void *pObject = PluginFactory::instance()->getObject(sMime, sPluginClass);

	return (pObject != NULL);
}

bool PluginManager::isNewSubsystem( const QString &sMime ) const
{
	PluginFactory *pPluginFactory = PluginFactory::instance();
	void *pPluginObject = pPluginFactory->getObject(sMime, "subsystem");
	bool  bNewSubsys = false;

	if (pPluginObject != NULL) {
// 		qDebug() << "PluginManager::isNewSubsystem. get pointer to new or old Subsystem, and return differences status";
		bNewSubsys = (m_currentSubsystem != (Vfs::Subsystem *)pPluginObject);
// 		qDebug() << "  -- m_currentSubsystem:" << m_currentSubsystem << "pluginObject:" << (Vfs::Subsystem *)pluginObject;
// 		m_currentSubsystem = (Vfs::Subsystem *)pPluginObject;
		//m_currentSubsystem = static_cast<Vfs::Subsystem *>(pluginManager->object(sMime));
	}

	return bNewSubsys;
}

QStringList PluginManager::getAllRegistreredPluginsInfo( int dataType, const QString &sPluginClass ) const
{
	PluginFactory *pluginFactory = PluginFactory::instance();
	QStringList slAllPluginsData = pluginFactory->getPluginsDataInfo(sPluginClass);
	QStringList slPluginDataRet, slData;
	QString sPluginData;

	enum { CLASS=0, NAME, VERSION, DESCRIPTION, MIME_TYPE, PROTOCOL, FILE_EXT };

	foreach(QString sData, slAllPluginsData) {
		if (dataType == PluginData::ALL) {
			slPluginDataRet = slAllPluginsData;
		}
		else {
			slData = sData.split('|');
			//bool st = dataType & PluginData::CLASS;
			if (dataType & PluginData::CLASS) {
				if (dataType == PluginData::CLASS)
					sPluginData  = slData.at(CLASS) + "|";
				else
					sPluginData += slData.at(CLASS) + "|";
			}
			if (dataType & PluginData::NAME) {
				if (dataType == PluginData::NAME)
					sPluginData  = slData.at(NAME) + "|";
				else
					sPluginData += slData.at(NAME) + "|";
			}
			if (dataType & PluginData::VERSION) {
				if (dataType == PluginData::VERSION)
					sPluginData  = slData.at(VERSION) + "|";
				else
					sPluginData += slData.at(VERSION) + "|";
			}
			if (dataType & PluginData::DESCRIPTION) {
				if (dataType == PluginData::DESCRIPTION)
					sPluginData  = slData.at(DESCRIPTION) + "|";
				else
					sPluginData += slData.at(DESCRIPTION) + "|";
			}
			if (dataType & PluginData::MIME_TYPE) {
				if (dataType == PluginData::MIME_TYPE)
					sPluginData  = slData.at(MIME_TYPE) + "|";
				else
					sPluginData += slData.at(MIME_TYPE) + "|";
			}
			if (dataType & PluginData::PROTOCOL) {
				if (dataType == PluginData::PROTOCOL)
					sPluginData  = slData.at(PROTOCOL) + "|";
				else
					sPluginData += slData.at(PROTOCOL) + "|";
			}
			if (dataType & PluginData::FILE_EXT) {
				if (dataType == PluginData::FILE_EXT)
					sPluginData  = slData.at(FILE_EXT) + "|";
				else
					sPluginData += slData.at(FILE_EXT) + "|";
				if (sPluginData == "|") // skip empty item
					continue;
				if (sPluginData.contains(';'))
					sPluginData.replace(';', '|');
			}
			sPluginData = sPluginData.left(sPluginData.length()-1);
			slPluginDataRet << sPluginData.split('|');
			sPluginData = "";
		}
	}

	return slPluginDataRet;
}

