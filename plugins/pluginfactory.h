/***************************************************************************
 *   Copyright (C) 2007 by Mariusz Borowski <b0mar@nes.pl>                 *
 *   Copyright (C) 2015 by Piotr Mierzwiñski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef PLUGINFACTORY_H
#define PLUGINFACTORY_H

#include <QMap>
#include <QPair>
#include <QString>
#include <QLibrary>

#include "plugindata.h"

/**
 @author Mariusz Borowski <b0mar@nes.pl>
 @author Piotr Mierzwiñski <piom@nes.pl>
 */
class PluginFactory
{
public:
	~PluginFactory();

	static PluginFactory *instance();

	/**
	 * Tries to load plugin which one path is passed.
	 * @param sAbsPathToPlugin absolute path to plugin
	 * @return information about loaded plugin in format: pluginClass|realated_mime
	 */
	QString load( const QString &sAbsPathToPlugin );

	/**
	 * Tries to unload plugin which one mime is passed.
	 * @param sInpMime mime for plugin
	 * @param sPluginClass class for plugin
	 * @return TRUE if unloading finished successfully otherwise FALSE.
	 */
	bool  unload( const QString &sInpMime, const QString &sPluginClass );

	void *getObject( const QString &sInpMime, const QString &sPluginClass, int nId=0 ) const;
	void *createObject( const QString &sInpMime, const QString &sPluginClass );

	bool isPluginAvailable( const QString &sInpMime, const QString &sInpClass = "" );

	/** Returns list containing all registrered plugins data. Every item on the list has following format:
	 * "CLASS|NAME|VERSION|DESCRIPTION|MIME_TYPE|PROTOCOL|FILE_EXT". Item: FILE_EXT might be split by semicolon.
	 * @param sPluginClass class for plugins
	 * @return list of strings
	 */
	QStringList getPluginsDataInfo( const QString &sPluginClass ) const;

	int  size( const QString &sPluginClass=QString() ) const;

	/** Updates configuration of mime used to mapping plugin by mime.
	 * There are updated two maps: m_mimeToPluginData and m_mimeToPluginLocation with sNewMimeLst (replacing sOldMimeLst).
	 * @param sPluginName plugin name to update
	 * @param sOldMimeLst old mime to be remove from configured in both mentioned maps
	 * @param sNewMimeLst new mime to be add to both mentioned maps
	 */
	void updateMimeConfig( const QString &sPluginName, const QString &sOldMimeLst, const QString &sNewMimeLst );

private:
	PluginFactory();

	QLibrary m_lib;

	/// plugin_obj_pointer, plugin_class_name
	typedef QPair <void *, QString> ObjClass;
	typedef QObject * (*t_func)(QObject *);
	typedef const char * (*PluginClass)();

	/// Mapping: mime (key) -> object and class
	QMultiMap <QString, ObjClass> m_subsysMimeToObjAndClassRelation;
	QMultiMap <QString, QString>  m_mimeToPluginLocation;
	//  For viewer class below map is used in destructor to remove objects. For both classes by method isPluginAvailable
	QMultiMap <QString, PluginData *> m_mimeToPluginData;

	QString m_sLastSearchingPluginPath; // path to plugin found in isPluginAvailable()
	QString m_sLastMatchingMime; // matching mime found in isPluginAvailable()

};

#endif // PLUGINFACTORY_H
