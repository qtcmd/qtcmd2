/***************************************************************************
 *   Copyright (C) 2007 by Mariusz Borowski <b0mar@nes.pl>                 *
 *   Copyright (C) 2011 by Piotr Mierzwi�ski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef PLUGINMANAGER_H
#define PLUGINMANAGER_H

#include <QString>

#include "qtcmdconfigurationdialog.h"
#include "preview/viewer.h"
#include "subsystem.h"

using namespace Preview;
/**
 @author Mariusz Borowski <b0mar@nes.pl>
 @author Piotr Mierzwiński <piom@nes.pl>
 */
class PluginManager
{
public:
	~PluginManager();

	static PluginManager *instance();

	/** Returns pointer to object realted to mime and plugin class.
	 * @param sMime mime to check
	 * @param sPluginClass plugin class to check
	 * @return pointer to object.
	 */
	void      *getObject( const QString &sMime, const QString &sPluginClass ) const;

	/** Gets pointer to object of subsystem related to passed mime.
	 * @param sMime mime to check
	 * @return pointer to object.
	 */
	Vfs::Subsystem *subsystem( const QString &sMime );

	/** Gets pointer to object of viewer related with passed mime.
	 * @param sMime mime to check
	 * @return pointer to object.
	 */
	Preview::Viewer *viewer( const QString &sMime );

	/** Gets pointer to object of configuration dialog.
	 * @param sMime mime type of config dialog (handled are: 'config/app' and 'config/preview')
	 * @return pointer to object.
	 */
	ConfigurationDialog *config( const QString &sMime );

	/** Checks if passed mime is related with loaded plugin.
	 * @param sMime mime to check
	 * @param sPluginClass plugin class to check
	 * @return TRUE if mime is related otherwise FALSE.
	 */
	bool isPluginRegistrered( const QString &sMime, const QString &sPluginClass ) const;

	/** Returns list containing all registrered plugins data for given dataType and plugin class.
	 * @param dataType type of data. Types might be contatenated by logical operator: '&'. For details @see PluginData::DataType
	 * @param sPluginClass plugin class for getting data
	 * @return list of plugins data. Each element might consist from one to couple of strings separated by '|' (it depends on dataType).
	 */
	QStringList getAllRegistreredPluginsInfo( int dataType, const QString &sPluginClass ) const;

	/** Sets pointer (returned by subsystem() method) to new subsystem
	 * or NULL if not found subsystem for given mime.
	 * @param sMime mime to check
	 * @return TRUE if set new pointer otherwise FALSE.
	 */
	bool isNewSubsystem( const QString &sMime ) const;


private:
	PluginManager();

	Vfs::Subsystem    *m_currentSubsystem;
	Preview::Viewer   *m_currentViewer;
	ConfigurationDialog *m_currentConfigurationDlg;

};

#endif
