/***************************************************************************
 *   Copyright (C) 2011 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDir>
#include <QTimer>
#include <QFileInfo>

//#include "rarsubsystemsettings.h"
#include "contextmenucmdids.h"
#include "systeminfohelper.h"
#include "pluginfactory.h"
#include "rarsubsystem.h"
#include "messagebox.h"
#include "settings.h"
#include "mimetype.h"

#include "preview/viewer.h"


namespace Vfs {

RarSubsystem::RarSubsystem()
{
	qDebug() << "RarSubsystem::RarSubsystem.";
	//setObjectName("RarSubsystem");
	m_error  = NO_ERROR;
	m_status = UNCONNECTED;
	//RarSubsystemSettings *rarSubsystemSettings = new RarSubsystemSettings;

	m_pPluginData = new PluginData;
	m_pPluginData->name         = "rarsubsystem_plugin";
	m_pPluginData->version      = "0.4";
	m_pPluginData->description  = QObject::tr("Plugin handles RAR archives.");
	m_pPluginData->plugin_class = "subsystem";
	m_pPluginData->mime_icon    = ""; // (used in configuration window, higher priority than own_icon)
	m_pPluginData->own_icon     = 0;  // QIcon pointer. (used in configuration window)
	m_pPluginData->mime_type    = "rarsubsystem"; // required only in starting moment, after that rar will be detected and property updated properly
	m_pPluginData->protocol     = "file";
	m_pPluginData->finding_files_fun        = 0; // ptr.to fun
	m_pPluginData->finding_inside_files_fun = 0; // ptr.to fun
//	m_pPluginData->popup_actions   = 0; // QActionGroup pointer  (rarSubsystemSettings->popupActions())
	m_pPluginData->toolBar      = 0;  // QToolBar pointer      (rarSubsystemSettings->toolBar())
	m_pPluginData->configPage   = 0;  // QWidget pointer       (rarSubsystemSettings->configPage())

// 	init(); // initialized globaly (in MainWindow)
	m_bForceReopen = false;
	m_bCreateNewArchive = false;
	m_bOperationFinished = false;
	m_processedFiles = new FileInfoList;
	m_preProcessedFiles = new FileInfoList;
	m_processedFileInfo = NULL;

	m_pProcess = new QProcess;
	connect(m_pProcess, &QProcess::readyReadStandardOutput, this, &RarSubsystem::slotReadStandardOutputAsyn);
	connect(m_pProcess, &QProcess::readyReadStandardError, this, &RarSubsystem::slotReadStandardError);
 	connect<void(QProcess::*)(int, QProcess::ExitStatus)>(m_pProcess, &QProcess::finished,  this, &RarSubsystem::slotProcessFinished); // signal is overloaded

	connect(&m_searchingTimer, &QTimer::timeout, this, &RarSubsystem::slotSearching);
}

RarSubsystem::~RarSubsystem()
{
	qDebug() << "RarSubsystem::~RarSubsystem. DESTRUCTOR";

// 	delete m_pPluginData; // removed in PluginFactory::~PluginFactory

	if (m_pProcess && m_pProcess->state() == QProcess::Running) {
		m_pProcess->terminate();
		m_pProcess->waitForFinished(3000);
	}
	delete m_pProcess;
	delete m_processedFiles;
	delete m_preProcessedFiles;
}


void RarSubsystem::init( QWidget *pParent, bool bInvokeDetection )
{
	QString sFN = "RarSubsystem::init.";
	m_currentCommand = NO_COMMAND;
	m_pParent = pParent;

	m_bOpenedArchive   = false;
	m_bWeighingArchive = false;
	m_nLevelInArch = 0; m_nLevelInArchCompl = 0;

	m_nNameColId = -1;
	m_fRarVer = -1;
	m_sProcessedFileName = "";

	qDebug() << sFN;

	if (bInvokeDetection) {
		qDebug() << sFN << "Start detection for command line applications..."; // if success then member m_pPluginData->file_ext will be properly updated
		m_pPluginData->mime_type = "";
		m_sRequestingPanel_open = "leftPanel_1:init";
		QString sCmdName = "rar";
		if (findExecutable(sCmdName).isEmpty())
			qDebug() << sFN << sCmdName << "not found in PATH.";
		else {
			qDebug() << sFN << sCmdName << "found in PATH.";
			m_pPluginData->file_ext  = sCmdName;
			m_pPluginData->mime_type = "application/x-rar:application/vnd.rar";
			PluginFactory::instance()->updateMimeConfig("rarsubsystem", "rarsubsystem", m_pPluginData->mime_type);
		}
	}
}

bool RarSubsystem::handledOperation( SubsystemCommand sc )
{
	if (sc == CREATE_LINK || sc == RENAME)
		return false;

	return true;
}

void RarSubsystem::notSupportedOp( const QString &sRequestingPanel )
{
	m_status = ERROR_OCCURED;  m_error = NOT_SUPPORTED;
	emit signalStatusChanged(m_status, sRequestingPanel);
}

FileInfo *RarSubsystem::root( bool bEmptyName )
{
	static QString fakeRoot = "/";
	QFileInfo fi(fakeRoot);

	return new FileInfo(bEmptyName ? "" : "..", fakeRoot, fi.size(), true, false, fi.lastModified(), "----------", fi.owner(), fi.group(), "folder/up");
}

FileInfo *RarSubsystem::fileInfo( const QString &sAbsFileName )
{
	qDebug() << "RarSubsystem::fileInfo. sAbsFileDirName" << sAbsFileName;
	if (sAbsFileName.endsWith("/..") || m_slParsingBufferLine.isEmpty())
		return root();

	QString sFileDirName = QDir::cleanPath("/"+sAbsFileName);
	QString sFileDirNameTmp = sAbsFileName; //sFileDirName;
	if (sFileDirNameTmp.at(0) == '/') // needed for correct searching
		sFileDirNameTmp.remove(0, 1);

	if (m_slParsingBufferLine.isEmpty()) {
		qDebug() << "RarSubsystem::fileInfo. Internal error. Empty m_sParsingBufferLine!!!";
		return root();
	}
	int lineId = m_slParsingBufferLine.indexOf(sFileDirNameTmp);
	if (lineId < 0) {
		qDebug() << "RarSubsystem::fileInfo. Internal error. Negative value lineId during looking for:" << sFileDirNameTmp;
		return root(true); // empty name
	}
	QString sName = m_slParsingBufferLine[lineId];
	QString sLine = m_slParsingBufferLine[lineId+1];

	QStringList lineStrList = sLine.split(' ');
	QString     sDateTime   = lineStrList[DATE] +" "+ lineStrList[TIME];
	bool        isDir       = (lineStrList[PERM].at(0) == 'd' || lineStrList[PERM].at(1) == 'D'); // Unix or Win dir
	sFileDirName = QString::fromLocal8Bit(sFileDirName.toLatin1().data());
	QString     sMime       = MimeType::getFileMime(sFileDirName, isDir);
	QString     sExtMime    = sMime +":"+ MimeType::getMimeIcon(sMime);
	sName = QString::fromLocal8Bit( sName.toLatin1().data() );
	FileInfo *item = new FileInfo(sName, m_sCurrentPath, atoll(lineStrList[SIZE].toLatin1()), isDir, false,
				QDateTime::fromString(sDateTime, "dd-MM-yy HH:mm").addYears(100), lineStrList[PERM], "?", "?", sExtMime);

	return item;
}


void RarSubsystem::open( const URI &uri, const QString &sRequestingPanel, bool bCompleter )
{
	m_bCompleterMode = bCompleter;
	QString sFN = "RarSubsystem::open.";
	if (m_bCompleterMode && m_currentUriCompleter.toString() == uri.toString()) {
		qDebug() << sFN << "Came path for already opened directory (QCompleter::splitPath issue).";
		return;
	}
	if (uri.host().isEmpty()) {
		qDebug() << sFN << "Cannot open archive due to empty host!";
		return;
	}
	if (m_pProcess->state() != QProcess::NotRunning) {
		qDebug() << sFN << "Process is already running (maybe in other tab was opened archive) or PROG_EXISTS not finished, currentCommand:" << toString(m_currentCommand);
		if (m_opQueue.first().command != VIEW) {
			if (m_currentCommand != PROG_EXISTS) {
				qDebug() << sFN << "  Adding OPEN command to queue.";
				m_opQueue << Operation(OPEN, uri, sRequestingPanel);
			}
		}
		return;
	}
	m_opQueue.clear();
	m_processedFiles->clear();

	qDebug() << sFN << "  host:" << uri.host() << "path:" << uri.path() << "last currentCommand:" << toString(m_currentCommand) << m_sRequestingPanel_open << "ForceReopen:" << m_bForceReopen << "RequestingPanel:" << sRequestingPanel;
	// Below condition is commented due to there is the risk that before opening second time the same archive rar might be updated to different and then parsing will work wrong
	// Thus better is check rar version and open archive again insead of use buffered archive which can't be properly parse with currently set rar archive.
//	if (! archiveOpened(uri)) { // use buffered archive, but will work only if m_fRarVer will be moved to from init() to constructor.
// 	if (! archiveOpened(uri) || m_fRarVer == -1 || m_bForceReopen) { // for parsing the buffer will be use procedure related with previously found rar version
// 		qDebug() << sFN << "Archive didn't buffer yet. Reopen current:" << uri.host();
		m_bOpenedArchive = false;
		m_currentCommand = (sRequestingPanel.contains("Viewer_")) ? VIEW : OPEN;
		m_bForceReopen = false;
// 	}
// 	else // when other action (FIND) was invoked previously then current command is different than LIST
// 		m_currentCommand = LIST;

	m_sHost = uri.host();
	m_sPath = uri.path();
	m_sRequestingPanel_open = sRequestingPanel;
	if (m_bCompleterMode)
		m_currentUriCompleter = uri;
	else
		m_currentUri = uri;

	m_bActionIsFinished = false;
	// below commented, because the most likely will be never called. The reason is m_currentCommand = OPEN; with commented condition placed above
// 	if (m_currentCommand == LIST) { // user navigates inside archive
// 		commandList();
// 		emit signalStatusChanged(m_status, m_sRequestingPanel_open);
// 		return;
// 	}

	if (! m_bOpenedArchive) {
		m_opQueue.clear();
		m_bCompleterMode ? m_nLevelInArchCompl = 0 : m_nLevelInArch = 0;
		// all operations are duplicated due to lack of knowledge which command exists unwanted are cleaned in slotProcessFinished
		initQueueForPreCheck(sRequestingPanel); // update queue with commands to check existing and version
		m_opQueue	<< Operation(m_currentCommand, "unrar v -c- \""+m_sHost+ "\"", sRequestingPanel)
					<< Operation(m_currentCommand, "rar v -c- \""+m_sHost+ "\"", sRequestingPanel);
		slotRunNextOperation();
	}
	else {
		m_currentCommand = LIST;
		commandList();
	}
}

FileInfoList RarSubsystem::listDirectory() const
{
	qDebug() << "RarSubsystem::listDirectory. Path:" << m_sCurrentPath;

	//FileInfoList fiList;
	//fiList.append(root());
	//return fiList;

	return m_fiList;
}

void RarSubsystem::commandList()
{
	m_bOpenedArchive = true;
	//qDebug() << "--- prepare to LIST (0). m_sPath:" << m_sPath << "m_sCurrentPath:" << m_sCurrentPath;
	m_sCurrentPath = "/"+m_sPath+"/";
	//qDebug() << "before clean - m_sCurrentPath" << m_sCurrentPath;
	m_sCurrentPath = QDir::cleanPath(m_sCurrentPath);

	bool goUp = (m_sPath.endsWith("/..") || m_sPath.endsWith("/../"));
	if (m_bCompleterMode) {
		if (goUp)
			m_nLevelInArchCompl -= 2;
		else
			m_nLevelInArchCompl = m_sCurrentPath.count(QDir::separator()) - ((m_sCurrentPath == QDir::separator()) ? 1 : 0);
	}
	else {
		if (goUp)
			m_nLevelInArch -= 2;
		else
			m_nLevelInArch = m_sCurrentPath.count(QDir::separator()) - ((m_sCurrentPath == QDir::separator()) ? 1 : 0);
	}
	QString sCurrentPathTmp = m_sCurrentPath + (m_sCurrentPath.endsWith(QDir::separator()) ? "" : "/"); // need to correct cut a name inside of parsingBufferedFilesList
	if (m_sPath.count('/') == 2 && goUp) // top level of archive - remove ending slash
		sCurrentPathTmp.remove(sCurrentPathTmp.length()-1, 1);
	if (! sCurrentPathTmp.isEmpty() && sCurrentPathTmp.at(0) == QDir::separator())
		sCurrentPathTmp.remove(0,1);

	qDebug() << "RarSubsystem::commandList. m_sPath:" << m_sPath << "m_sCurrentPath:" << m_sCurrentPath << "sCurrentPathTmp:" << sCurrentPathTmp << "m_nLevelInArch:" << (m_bCompleterMode ? m_nLevelInArchCompl : m_nLevelInArch);

	uint lvlInArch = (m_bCompleterMode ? m_nLevelInArchCompl : m_nLevelInArch);
	processingParsedFilesList(lvlInArch, sCurrentPathTmp);
	if (m_status == ERROR_OCCURED)
		return;
	m_currentUri.setPath(m_sCurrentPath);
	m_error  = NO_ERROR;
	m_status = m_bCompleterMode ? LISTED_COMPL : LISTED;
}

void RarSubsystem::parsingBufferedFilesList()
{
	QString sFN = "RarSubsystem::parsingBufferedFilesList.";
	if (m_sOutBuffer.isEmpty()) {
		qDebug() << sFN << "INTERNAL_ERROR. Empty buffer: m_sOutBuffer! Exit.";
		m_status = ERROR_OCCURED;  m_error = INTERNAL_ERROR;
		emit signalStatusChanged(m_status, m_sRequestingPanel_open);
		m_opQueue.clear();
		return;
	}
	qDebug() << sFN;

	// --- remove header and tail (9 and 4 lines)
	m_slParsingBufferLine = m_sOutBuffer.split('\n');
	if (m_fRarVer >= 5)  { // determine Name possition to speed up archive processing
		int nId = (m_sCommand == "unrar") ? 6 : 7;
		m_nNameColId = m_slParsingBufferLine[nId].indexOf("Name");
	}
	int nHeaderSize = (m_sCommand == "unrar") ? 8 : 9;
	for (int i=0; i<nHeaderSize; i++) {
		m_slParsingBufferLine.pop_front();
		if ((i%3)/2 == 1)
			m_slParsingBufferLine.pop_back();
	}
	m_slParsingBufferLine.pop_back(); // remove last empty line for ver.4 and summary for Size, Packed, Ratio in ver.5)
	if (m_fRarVer >= 5 && m_sCommand == "unrar")
		m_slParsingBufferLine.pop_back(); // remove last line
	// --- simple parsing
	int id = 0;
	QString line;
	foreach (line, m_slParsingBufferLine) {
		id++;
		if (m_fRarVer < 5) { // simplifying every second line
			if (! (id%2)) {
				line.replace(QRegExp("\\s+"), " ");
			}
		}
		line.remove(0, 1);
		line = line.trimmed();
		m_slParsingBufferLine[id-1] = line;
	}

	if (m_fRarVer >= 5)
		m_nFilesNumInArch = m_slParsingBufferLine.count();
	else
	if (m_fRarVer >= 4)
		m_nFilesNumInArch = m_slParsingBufferLine.count()/2;

	qDebug() << sFN << "Buffered lines:" << m_slParsingBufferLine.count();
	// buffer's test
	//qDebug() << m_sOutBuffer;
/*	foreach (line, m_sParsingBufferLine) {
		qDebug() << line;
	}*/
}

void RarSubsystem::processingParsedFilesList( unsigned int level, const QString &sInpPath )
{
	QString sFN = "RarSubsystem::processingParsedFilesList.";
	if (m_slParsingBufferLine.isEmpty()) {
		qDebug() << sFN << "Empty buffer: m_slParsingBufferLine! Exit.";
		m_status = ERROR_OCCURED;  m_error = INTERNAL_ERROR;
		emit signalStatusChanged(m_status, m_sRequestingPanel_open);
		m_opQueue.clear();
		return;
	}
	bool isDir, isSymLink, bFoundTopDir = false;
	unsigned int nSlashesNum, id = 0;
	QStringList lineStrList;
	//QString sSymLinkTarget;
	QString sLine, sName, sDateTime, sMime;
	FileInfo *pItem;

	qDebug() << sFN << "Level:" << level << "dirName:" << sInpPath;// << "m_sParsingBufferLine.count:" << m_sParsingBufferLine.count();
	// -- insert top item
	m_fiList.clear();
	m_fiList.append(root());

	if (m_fRarVer >= 5) {
		foreach (sLine, m_slParsingBufferLine) {
			sName = sLine.right((sLine.length()-m_nNameColId) + 1);
			sName = QString::fromLocal8Bit(sName.toLatin1().data());
			nSlashesNum = sName.count('/');
			if (nSlashesNum > 0) {
				if ((nSlashesNum > level || nSlashesNum < level))
					continue;
				if (nSlashesNum == level) {
					if (! sName.startsWith(sInpPath))
						continue;
					sName.remove(0, sInpPath.length());
				}
			}
			if (nSlashesNum != level)
				continue;
			lineStrList = sLine.split(QRegExp("\\s+"), Qt::SkipEmptyParts);
			if (sLine.startsWith(' ')) { // Microsoft Windows rar version
				isDir = (lineStrList[PERMv5].at(3) == 'D');
				if (sName.endsWith(' '))
					sName = sName.trimmed();
			}
			else {
				isDir = (lineStrList[PERMv5].at(0) == 'd');
			}
			if (isDir && nSlashesNum == 0)
				bFoundTopDir = true;
			isSymLink = (lineStrList[PERMv5].at(0) == 'l');
			sDateTime =  lineStrList[DATEv5] +" "+ lineStrList[TIMEv5];
			if (m_bCompleterMode && ! isDir)
				continue;
			sMime = MimeType::getFileMime(sName, isDir, isSymLink);
			//sSymLinkTarget = "";
			QDateTime dateTime;
			if (m_fRarVer >= 5.3)
				dateTime = QDateTime::fromString(sDateTime, "yyyy-MM-dd HH:mm");
			else
				dateTime = QDateTime::fromString(sDateTime, "dd-MM-yy HH:mm").addYears(100);
			pItem = new FileInfo(sName, sInpPath, ::atoll(lineStrList[SIZEv5].toLatin1()), isDir, false, dateTime, lineStrList[PERMv5], "?", "?", sMime);//, sSymLinkTarget);
			m_fiList.append(pItem);
		}
	}
	else
	if (m_fRarVer >= 4) {
		foreach (sLine, m_slParsingBufferLine) {
			id++;
			if ((id % 2) == 1) {
				sName = sLine; // file/dir name
			}
			else {
				nSlashesNum = sName.count('/');
				if (nSlashesNum > level || nSlashesNum < level)
					continue;
				if (nSlashesNum == level) {
					if (! sName.startsWith(sInpPath))
						continue;
					sName.remove(0, sInpPath.length());
				}
				lineStrList = sLine.split(' ');
				isSymLink = (lineStrList[PERM].at(0) == 'l');
				isDir     = (lineStrList[PERM].at(0) == 'd' || lineStrList[PERM].at(1) == 'D'); // Unix or Win dir
				if (isDir && nSlashesNum == 0)
					bFoundTopDir = true;
				if (m_bCompleterMode && ! isDir)
					continue;
				sDateTime =  lineStrList[DATE] +" "+ lineStrList[TIME];
				sName = QString::fromLocal8Bit(sName.toLatin1().data());
				sMime = MimeType::getFileMime(sName, isDir, isSymLink);
				//sSymLinkTarget = "";
				pItem = new FileInfo(sName, sInpPath, ::atoll(lineStrList[SIZE].toLatin1()), isDir, false, // isSymLink (workaround to correct shows size)
				QDateTime::fromString(sDateTime, "dd-MM-yy HH:mm").addYears(100), lineStrList[PERM], "?", "?", sMime);//, sSymLinkTarget);
				m_fiList.append(pItem);
			}
		} // foreach
	}
	else {
		qDebug() << sFN << "Unsupported rar version:" << m_fRarVer;
	}
	// -- adding fake top directory
	if (! bFoundTopDir && m_nLevelInArch == 0) {
		sName = sName.left(sName.indexOf(QDir::separator()));
		m_fiList.append(new FileInfo(sName, sInpPath, 0, true, false, QDateTime::fromTime_t(0), "----------", "?", "?", "folder"));
	}

	m_bCompleterMode ? m_nLevelInArchCompl++ : m_nLevelInArch++;
}


void RarSubsystem::openWith( FileInfoToRowMap *selectionMap, const URI &uri, const QString &sRequestingPanel )
{
	int nResult = -1;
	QString sFN = "RarSubsystem::openWith.";
	qDebug() << "RarSubsystem::openWith -- uri" << uri.toString() << "m_currentUri:" << m_currentUri.toString() << "absFileName:" << selectionMap->constBegin().key()->absoluteFileName();

	if (m_bConfirmExtractingForView) {
		int nSelFilesNum = selectionMap->count();
		QString sAbsFileName = m_currentUri.toString() + QDir::separator() + selectionMap->constBegin().key()->fileName();
		QString sQuestion = (nSelFilesNum > 1) ? tr("Extract and open files?") : tr("Do you want to extract and open given file?");
		QString sFImsg = (nSelFilesNum > 1) ? QString(tr("Selected %1 items")).arg(nSelFilesNum) : sAbsFileName;
		nResult = MessageBox::yesNo(m_pParent, tr("Open with..."), sFImsg, sQuestion,
									m_bConfirmExtractingForView,
									MessageBox::Yes, // default focused button
									tr("Always ask for confirmation")
		);
		if (nResult == 0) // user closed dialog
			return;
		Settings settings;
		settings.update("archive/ConfirmExtractingForOpenWith", m_bConfirmExtractingForOpenWith);
	}
	else
		nResult = MessageBox::Yes;

	if (nResult == MessageBox::Yes) {
		if (! archiveOpened(uri))
			m_sHost = uri.host();

		m_error = NO_ERROR;
		m_status = EXTRACTING;
		m_currentCommand = OPEN_WITH_CMD;
		m_bOperationFinished = false;
		m_sRequestingPanel_open = sRequestingPanel;
		m_processedFiles->clear();
		m_opQueue.clear();

		if (m_sTmpDir.isEmpty()) {
			qDebug() << sFN << "Internal ERROR. Working directory is not set!";
			m_error = CANNOT_EXTRACT;
			emit signalStatusChanged(Vfs::OPEN_WITH, sRequestingPanel);
			return;
		}

		long long nOpenedFilesWeigh = 0;
		QString sFullFileNamesToExtract, sFileNameNoHost, sFileName;
		FileInfo *fi;
		FileInfoToRowMap::const_iterator it = selectionMap->constBegin();
		while (it != selectionMap->constEnd()) {
			fi = it.key();
			sFileName = fi->absoluteFileName();
			sFileNameNoHost = sFileName.remove(m_sHost);
			if (sFileNameNoHost.startsWith('/'))
				sFileNameNoHost.remove(0, 1);
			sFullFileNamesToExtract += "\"" + sFileNameNoHost + "\" ";
			m_processedFiles->append(fi);
			nOpenedFilesWeigh += fi->size();
			++it;
		}
		long long freeBytes, totalBytes;
		Vfs::getStatFS(m_sTmpDir, freeBytes, totalBytes);
		if (freeBytes < nOpenedFilesWeigh) { // m_nSourceFileSize is init.into 'initPutFile()'
			m_error = NO_FREE_SPACE;
			emit signalStatusChanged(Vfs::ERROR_OCCURED, sRequestingPanel);
			return;
		}
//		unrar e /tmp/test/lxterminal-0.1.6.rar lxterminal-0.1.6/src/encoding.c /tmp/test/
//		rar x -cfg- -ow -ol -o+ -aplxterminal-0.1.6/ /home/piotr/test_archives/lxterminal-0.1.6.rar lxterminal-0.1.6/depcomp lxterminal-0.1.6/COPYING /tmp/qtcmd2/ # -o+ overwrite existing file
		QString sUnRarCmd = "unrar e -o+ \"" + m_sHost + "\" " + sFullFileNamesToExtract + " " + m_sTmpDir;
		QString sRarCmd   = "rar x -cfg- -ow -ol -o+ -ap\"" + fi->filePath() + "\" \"" + m_sHost + "\" " + sFullFileNamesToExtract + " " + m_sTmpDir;
		// all operations are duplicated due to lack of knowledge which command exists unwanted are cleaned in slotProcessFinished
		initQueueForPreCheck(sRequestingPanel); // update queue with commands to check existing and version
		m_opQueue	<< Operation(m_currentCommand, sUnRarCmd, sRequestingPanel)
					<< Operation(m_currentCommand, sRarCmd, sRequestingPanel);
		slotRunNextOperation();
	}
	else {
		m_bOperationFinished = true;
		emit signalStatusChanged(Vfs::OPEN_WITH, sRequestingPanel);
	}
}

void RarSubsystem::readFileToViewer( const QString &sHost, FileInfo *pFileInfo, QByteArray &baBuffer, int & nReadBlockSize, int nReadingMode, const QString &sRequestingPanel )
{
	QString sFN = "RarSubsystem::readFileToViewer.";
	qDebug() << sFN << "host:" << sHost << "absFileName:" << pFileInfo->absoluteFileName() << " method:" << nReadingMode << "sRequestingPanel:" << sRequestingPanel;
	m_error = NO_ERROR;
	m_currentCommand = READ_FILE_TO_VIEW;

	// -- check if temporary directory is set
	if (m_sTmpDir.isEmpty()) {
		qDebug() << sFN << "Internal ERROR. Working directory is not set!";
		m_error = CANNOT_EXTRACT;
		emit signalStatusChanged(Vfs::OPEN_WITH, sRequestingPanel);
		return;
	}
	// -- check readability
	if (nReadingMode == Preview::Viewer::GET_READABILITY) { // (3) always readable, because file will be extracted to tmp directory
		nReadBlockSize = 1; // need for FileViewer check
		m_processedFileInfo = pFileInfo;
		emit signalStatusChanged((m_status=Vfs::READY_TO_READ), sRequestingPanel); // unmark item
		return;
	}
	long long nInputFileWeigh = pFileInfo->size();
	long long freeBytes, totalBytes;
	Vfs::getStatFS(m_sTmpDir, freeBytes, totalBytes);
	if (freeBytes < nInputFileWeigh) {
		m_error = NO_FREE_SPACE;
		emit signalStatusChanged(Vfs::ERROR_OCCURED, sRequestingPanel);
		return;
	}
	// -- prepare to extract file
	m_sHost = sHost;
	m_sRequestingPanel_open = sRequestingPanel;

	QString sFileName = pFileInfo->absoluteFileName();
	QString sFileNameNoHost = sFileName.remove(m_sHost);
	if (sFileNameNoHost.startsWith('/'))
		sFileNameNoHost.remove(0, 1);
	//qDebug() << "RarSubsystem::readFileToViewer.  -- sFileNameNoHost:" << sFileNameNoHost;
	m_processedFiles->clear();
	m_processedFiles->append(pFileInfo);

	// need to remove file before extract it due to extracting process might hang
	QString sFileToRemove = sFileNameNoHost;
	sFileToRemove.replace(sFileToRemove.left(sFileToRemove.lastIndexOf('/')+1), m_sTmpDir);
	QFile(sFileToRemove).remove();

	int nResult = -1;
	if (m_bConfirmExtractingForView) {
		QString sAbsFileName = m_sHost + QDir::separator() + pFileInfo->absoluteFileName(); // m_currentUri.toString() points to current dir in archive including host
		QString sQuestion = tr("Do you want to extract and open given file?");
		nResult = MessageBox::yesNo(m_pParent, tr("View file"), sAbsFileName, sQuestion,
									m_bConfirmExtractingForView,
									MessageBox::Yes, // default focused button
									tr("Always ask for confirmation")
		);
		if (nResult == 0) // user closed dialog
			return;
		Settings settings;
		settings.update("archive/ConfirmExtractingForView", m_bConfirmExtractingForView);
	}
	else
		nResult = MessageBox::Yes;

	sFileNameNoHost = "\"" + sFileNameNoHost + "\" ";
	m_bOperationFinished = false;
	m_status = EXTRACTING;
	m_opQueue.clear();

	if (nResult == MessageBox::Yes) {
//		unrar e /tmp/test/lxterminal-0.1.6.rar lxterminal-0.1.6/src/encoding.c /tmp/test/
//		rar x -cfg- -ow -ol -o+ -aplxterminal-0.1.6/ /home/piotr/test_archives/lxterminal-0.1.6.rar lxterminal-0.1.6/depcomp lxterminal-0.1.6/COPYING /tmp/qtcmd2/ # -o+ overwrite existing file
		QString sUnRarCmd = "unrar e -o+ \"" + m_sHost + "\" " + sFileNameNoHost + " " + m_sTmpDir;
		QString sRarCmd   = "rar x -cfg- -ow -ol -o+ -ap\"" + pFileInfo->filePath() + "\" \"" + m_sHost + "\" " + sFileNameNoHost + " " + m_sTmpDir;
		// all operations are duplicated due to lack of knowledge which command exists unwanted are cleaned in slotProcessFinished
		initQueueForPreCheck(sRequestingPanel); // update queue with commands to check existing and version
		m_opQueue	<< Operation(m_currentCommand, sUnRarCmd, sRequestingPanel)
					<< Operation(m_currentCommand, sRarCmd, sRequestingPanel);
		slotRunNextOperation();
	}
	else {
		m_bOperationFinished = true;
		emit signalStatusChanged(Vfs::NOT_READY_TO_READ, sRequestingPanel);
	}
}

void RarSubsystem::processingOpenWithCmd()
{
	m_status = OPEN_WITH;
	m_error  = NO_ERROR;
// 	QStringList sParsingBufferLine;
// 	sParsingBufferLine = m_sOutBuffer.split('\n');
	QStringList sParsingBufferLine(m_sOutBuffer.split('\n'));
	int nLineNum = 0;
	int nMaxLine = sParsingBufferLine.count();
	while (nLineNum <= nMaxLine) {
		if (sParsingBufferLine[nLineNum].startsWith("Extracting  "))
			break;
		nLineNum++;
	}
	nMaxLine = nLineNum + 1;
	nLineNum = 0;
	while (++nLineNum < nMaxLine)
		sParsingBufferLine.pop_front();
	sParsingBufferLine.pop_back(); // remove status info line
	sParsingBufferLine.pop_back(); // remove status info line
// 				qDebug() << "sParsingBufferLine:\n" << sParsingBufferLine;
	int nFilesNumInArch = sParsingBufferLine.count();
	m_bOperationFinished = (nFilesNumInArch == m_processedFiles->count());
	qDebug() << "RarSubsystem::processingOpenWithCmd. OPEN_WITH_CMD command, filesNumInArch:" << nFilesNumInArch << "operationFinishedStatus:" << m_bOperationFinished;
}


void RarSubsystem::initWeighing( bool bResetCounters, const QString &sRequestingPanel )
{
	qDebug() << "RarSubsystem::initWeighing. currentCommand:" << toString(m_currentCommand);
	if (m_currentCommand != WEIGH_ARCH)
		m_processedFiles->clear();
	m_totalWeight = 0;
	m_nTotalItems = 0;
	if (bResetCounters) {
		m_nDirCounter = 0;
		m_nFileCounter = 0;
		m_nSymlinksCounter = 0;
	}
	m_error  = NO_ERROR;
	m_status = WEIGHING;
	m_bCreateNewArchive = false;
	m_bOperationFinished = false;
	emit signalStatusChanged(m_status, sRequestingPanel);
}

void RarSubsystem::weigh( FileInfoToRowMap *pSelectionMap, const URI &uri, const QString &sRequestingPanel, bool bWeighingAsSubCmd, bool bResetCounters )
{
	// -- check if opened archive is cached
	if (! archiveOpened(uri) && ! bWeighingAsSubCmd) {
		qDebug() << "RarSubsystem::weigh. Archive didn't buffer yet. Need to reopen.";
		open(uri, sRequestingPanel);
		m_opQueue.clear();
		m_opQueue << Operation(WEIGH, pSelectionMap, uri, sRequestingPanel, bResetCounters);
		slotRunNextOperation();
		return;
	}
	m_sRequestingPanel = sRequestingPanel;
	initWeighing(bResetCounters, sRequestingPanel);

	FileInfoToRowMap::const_iterator it = pSelectionMap->constBegin();
	while (it != pSelectionMap->constEnd()) {
		m_processedFiles->append(it.key());
		processingWeighCmd(it.key());
		++it;
	}

// 	if (! bWeighingAsSubCmd) { // commented due to: properties dialog needs this signal
		m_bOperationFinished = true;
		m_status = WEIGHED;
		emit signalStatusChanged(m_status, sRequestingPanel);
// 	}
}

void RarSubsystem::weighArchive( const QString &sAbsFileName, const QString &sRequestingPanel, bool bRunNextOperation )
{
	qDebug() << "RarSubsystem::weighArchive." << sRequestingPanel << m_sRequestingPanel;
	initWeighing(true, m_sRequestingPanel);

	m_nLevelInArch = 0;
	// all operations are duplicated due to lack of knowledge which command exists, later unwanted commands will be removed in slotProcessFinished
//	initQueueForPreCheck(); // update queue with commands to check existing and version
	m_opQueue.prepend(Operation(OPEN, "rar v -c- \""+sAbsFileName+"\"", sRequestingPanel));
	m_opQueue.prepend(Operation(OPEN, "unrar v -c- \""+sAbsFileName+"\"", sRequestingPanel));
	m_opQueue.prepend(Operation(CHK_VER, "rar", sRequestingPanel));
	m_opQueue.prepend(Operation(CHK_VER, "unrar", sRequestingPanel));
	m_opQueue.prepend(Operation(PROG_EXISTS, "which rar", sRequestingPanel));
	m_opQueue.prepend(Operation(PROG_EXISTS, "which unrar", sRequestingPanel));

	if (bRunNextOperation)
		slotRunNextOperation();
}

void RarSubsystem::weighItem( FileInfo *pFileInfo, const URI &uri, const QString &sRequestingPanel, bool bResetCounters )
{
	if (uri.host().isEmpty()) { // the most likely archive file
		m_opQueue.clear();
		m_opQueue << Operation(WEIGH_ARCH, pFileInfo, "", sRequestingPanel); // thanks that flag m_bWeighingArchive will be properly set
		slotRunNextOperation();
		return;
	}
	// -- check if opened archive is cached
	if (! archiveOpened(uri)) {
		qDebug() << "RarSubsystem::weighItem. Archive didn't buffer yet. Need to reopen.";
		open(uri, sRequestingPanel);
		m_opQueue << Operation(WEIGH_ITEM, pFileInfo, uri, sRequestingPanel, bResetCounters);
		slotRunNextOperation();
		return;
	}
	initWeighing(bResetCounters, sRequestingPanel);

	m_processedFiles->append(pFileInfo);
	processingWeighCmd(pFileInfo);

	m_bOperationFinished = true;
	m_status = WEIGHED;
	emit signalStatusChanged(m_status, sRequestingPanel);
}

void RarSubsystem::processingWeighCmd( FileInfo *pFileInfo )
{
	if (pFileInfo == NULL && ! m_bWeighingArchive) {
		qDebug() << "RarSubsystem::processingWeighCmd. empty fileInfo";
		return;
	}
	bool isDir, isSymLink;
	unsigned int slashesNum, id = 0;
	unsigned int level = m_nLevelInArch;
	QStringList lineStrList;
	QString sLine, sName;
	QString sPath;
// 	QString sPath = pFileInfo->absoluteFileName();
	if (! m_bWeighingArchive)
		sPath = pFileInfo->absoluteFileName();
	if (sPath.startsWith(QDir::separator()))
		sPath.remove(0, 1);

	if (! m_bWeighingArchive) {
		if (pFileInfo->isDir()) {
			sPath += QDir::separator();
			qDebug() << "RarSubsystem::processingWeighCmd. Path:" << sPath << "level:" << level;
		}
		else
			level--;
	}

	if (m_fRarVer >= 5) {
		foreach (sLine, m_slParsingBufferLine) {
			sName = sLine.right((sLine.length()-m_nNameColId) + 1);
			sName = QString::fromLocal8Bit(sName.toLatin1().data());
			slashesNum = sName.count('/');
			if (slashesNum > 0 && level > 0) {
				if ((slashesNum > level || slashesNum < level))
					continue;
				if (slashesNum == level) {
					if (! sName.startsWith(sPath))
						continue;
				}
			}
			if (slashesNum != level && level > 0)
				continue;
			lineStrList = sLine.split(QRegExp("\\s+"), Qt::SkipEmptyParts);
			if (lineStrList[PERMv5].length() == 10) // archive created in Unix
				isDir = (lineStrList[PERMv5].at(0) == 'd');
			else // archive created in Microsoft Windows
				isDir = (lineStrList[PERMv5].at(3) == 'D');
			isSymLink = (lineStrList[PERMv5].at(0) == 'l');
			if (isSymLink) {
				m_nSymlinksCounter++;
				m_totalWeight += atoll(lineStrList[SIZEv5].toLatin1());
			}
			else {
				if (isDir)
					m_nDirCounter++;
				else {
					m_nFileCounter++;
					m_totalWeight += atoll(lineStrList[SIZEv5].toLatin1());
				}
			}
		}
	} // (m_fRarVer >= 5)
	else
	if (m_fRarVer >= 4) {
		foreach (sLine, m_slParsingBufferLine) {
			id++;
			if ((id % 2) == 1) {
				sName = sLine; // file/dir name
			}
			else {
				slashesNum = sName.count('/');
				if (slashesNum < level)
					continue;
				if (! sName.startsWith(sPath))
					continue;

				sName.remove(0, sPath.length());
				//qDebug() << "name:" << sName;
				lineStrList = sLine.split(' ');
				isSymLink = (lineStrList[PERM].at(0) == 'l');
				isDir     = (lineStrList[PERM].at(0) == 'd' || lineStrList[PERM].at(1) == 'D'); // Unix or Win dir

				if (isSymLink)
					m_nSymlinksCounter++;
				if (! isDir) {
					m_totalWeight += atoll(lineStrList[SIZE].toLatin1());
					m_nFileCounter++;
				}
				else
					m_nDirCounter++;

				if (! m_bWeighingArchive) {
					if (! pFileInfo->isDir())
						break;
				}
			}
		} // foreach
	} // (m_fRarVer >= 4)

	if (! m_bWeighingArchive) {
		if (pFileInfo->isDir())
			m_nDirCounter++; // main dir
	}

	m_nTotalItems = (m_nFileCounter + m_nDirCounter); // skip m_nSymlinksCounter because were counted as files
//	m_nTotalItems = (m_nFileCounter + m_nDirCounter + m_nSymlinksCounter);
}

void RarSubsystem::getWeighingResult( qint64 &weight, qint64 &realWeight, uint &files, uint &dirs )
{
	weight = m_totalWeight;
	realWeight = -1;
	files  = m_nFileCounter;
	dirs   = m_nDirCounter;
}


void RarSubsystem::initRemoving( const QString &sRequestingPanel )
{
	if (m_status == WEIGHED) {
		qDebug() << "RarSubsystem::initRemoving. WEIGHED - totalWeigh:" << m_totalWeight << FileInfo::bytesRound(m_totalWeight) << "files:" << m_nFileCounter << "dirs:" << m_nDirCounter << "all items:" << m_nTotalItems;
	}
	m_opQueue.clear();
	m_processedFiles->clear();
	m_OpTotalWeight  = 0;
	m_nOpDirCounter  = 0;
	m_nOpFileCounter = 0;
	m_nOpSymlinksCounter = 0;
	m_currentCommand = REMOVE;
	m_status = REMOVING;
	m_bCreateNewArchive = false;
	m_sRequestingPanel = sRequestingPanel; // used in slotProcessFinished
	emit signalStatusChanged(m_status, sRequestingPanel);
}

void RarSubsystem::remove( FileInfoToRowMap *pSelectionMap, const URI &uri, bool bWeighBefore, const QString &sRequestingPanel, bool bForceRemoveAll )
{
	QString sFN = "RarSubsystem::remove.";
	if (pSelectionMap == NULL) {
		qDebug() << sFN << "Empty selectionMap.";
		m_status = ERROR_OCCURED;  m_error = INTERNAL_ERROR;
		emit signalStatusChanged(m_status, sRequestingPanel);
		return;
	}
	int selectedItemsNum = pSelectionMap->size();
	qDebug() << sFN << "Selected items:" << selectedItemsNum << "weighBefore:" << bWeighBefore;
	if (selectedItemsNum == 0) {
		qDebug() << sFN << "No selected items.";
		return;
	}

	// -- start remove operation
	initWeighing(true, m_sRequestingPanel);
	if (bWeighBefore) {
		weigh(pSelectionMap, uri, sRequestingPanel);
		emit signalCounterProgress(m_nDirCounter,  DIR_COUNTER, true);
		emit signalCounterProgress(m_nFileCounter, FILE_COUNTER, true);
		emit signalCounterProgress(m_totalWeight,  WEIGH_COUNTER, true);
	}

	initRemoving(sRequestingPanel);

	QString sFullFileNamesToRemove, sFileName;
	FileInfo *pFileInfo;
	FileInfoToRowMap::const_iterator it = pSelectionMap->constBegin();
	while (it != pSelectionMap->constEnd()) {
		pFileInfo = it.key();
		sFileName = pFileInfo->absoluteFileName();
		if (sFileName.startsWith(QDir::separator()))
			sFileName.remove(0, 1);
		sFullFileNamesToRemove += "\"" + sFileName + "\" ";
// 		m_totalWeight += pFileInfo->size();
		m_processedFiles->append(pFileInfo);
		++it;
	}

	// rar d lxterminal-0.1.6.rar lxterminal-0.1.6/man  (dir)
	// rar d lxterminal-0.1.6.rar lxterminal-0.1.6/NEWS (file)
	QString sCmd = "rar d \"" + m_sHost + "\" " + sFullFileNamesToRemove;
	m_opQueue << Operation(PROG_EXISTS, "which rar", sRequestingPanel);
	m_opQueue << Operation(m_currentCommand, sCmd, sRequestingPanel);
	slotRunNextOperation();
}

void RarSubsystem::processingRemoveCmd()
{
// 	qDebug() << "RarSubsystem::processingRemoveCmd.\n" << m_sOutBuffer;
	if (m_sOutBuffer.contains("No files to delete"))
		return;

	QStringList slLinesBuffer = m_sOutBuffer.split('\n');
	bool bRemovingEmptyArch = false, bIsDir = false;
	QString sInpLine, sFileName;
	FileInfo *pFI;

	for (int i=0; i<slLinesBuffer.count(); i++) {
		sInpLine = slLinesBuffer.at(i);
		if (! sInpLine.startsWith("Deleting"))
			continue;
		if (sInpLine.startsWith("Deleting from"))
			continue;
		sFileName = sInpLine.right(sInpLine.length() - sInpLine.indexOf(" ") - 1);
		bRemovingEmptyArch = (sFileName.indexOf(m_sHost) >= 0);
		foreach (pFI, *m_processedFiles) {
			//qDebug() << "pFI->absoluteFileName:" << pFI->absoluteFileName() << "sFileName:" << sFileName;
			if (sFileName.startsWith(pFI->absoluteFileName())) {
				if (! sFileName.startsWith(pFI->absoluteFileName()+QDir::separator()))
					bIsDir = pFI->isDir() && ! pFI->isSymLink();
				else
					bIsDir = false;
				break;
			}
		}
		emit signalNameOfProcessedFile(sFileName);
		if (bIsDir) {
			emit signalCounterProgress(++m_nOpDirCounter, DIR_COUNTER, false);
		}
		else {
			emit signalCounterProgress(++m_nOpFileCounter, FILE_COUNTER, false);
		}
		m_nOpTotalFiles = m_nOpFileCounter + m_nOpDirCounter + m_nOpSymlinksCounter;
		emit signalTotalProgress((100 * m_nOpTotalFiles) / m_nTotalItems, m_totalWeight);
	}

	if (bRemovingEmptyArch && slLinesBuffer.count() > 0)
		MessageBox::msg(m_pParent, MessageBox::WARNING, tr("Removing empty archive"), m_sHost);
}

void RarSubsystem::processingArchiveCmd() // adding to archive or making an archive
{
// 	qDebug() << "RarSubsystem::processingArchiveCmd.\n" << m_sOutBuffer;
	QStringList slLinesBuffer = m_sOutBuffer.split('\n');
	QString sInpLine, sFileName, sFileFromAddedDir;
	bool bCountFile, bIsDir = false;
	int  nProcessedFiles;
	FileInfo *pFI = NULL;
	QFileInfo fi;

	for (int i=0; i<slLinesBuffer.count(); i++) {
		sInpLine = slLinesBuffer.at(i);
		if (! sInpLine.startsWith("Adding") && ! sInpLine.startsWith("Updating"))
			continue;
		if (sInpLine.startsWith("Updating archive"))
			continue;
		sFileName = sInpLine.left(sInpLine.indexOf(0x08));
		sFileName.remove(0, sFileName.indexOf(QDir::rootPath())); // remove: "Adding"
		sFileName = sFileName.trimmed();
		bCountFile = true;
		if (m_bCreateNewArchive) { // do manualy checking for every file, because rar on output doesn't distinguish files and directories
			fi.setFile(sFileName);
			bIsDir = fi.isDir();
		}
		else { // update existing archive
			foreach (pFI, *m_preProcessedFiles) {
				//qDebug() << "pFI->absoluteFileName:" << pFI->absoluteFileName() << "sFileName:" << sFileName;
				if (sFileName.startsWith(pFI->absoluteFileName())) {
					if (! sFileName.startsWith(pFI->absoluteFileName()+QDir::separator()))
						bIsDir = pFI->isDir() && ! pFI->isSymLink();
					else {
						bIsDir = false;
						if (! m_bCreateNewArchive)
							bCountFile = false; // don't count file from added directory
					}
					break;
				}
			}
		}
		emit signalNameOfProcessedFile(sFileName);
		if (bIsDir) {
			if (m_bCreateNewArchive)
				emit signalCounterProgress(++m_nOpDirCounter, DIR_COUNTER, false);
			else
			if (m_sLastDirName != pFI->fileName()) {
				emit signalCounterProgress(++m_nOpDirCounter, DIR_COUNTER, false);
				m_sLastDirName = pFI->fileName();
			}
		}
		else {
			if (bCountFile)
				emit signalCounterProgress(++m_nOpFileCounter, FILE_COUNTER, false);
			else {
				if (! m_bCreateNewArchive) {
					sFileFromAddedDir = sFileName.right(sFileName.length() - sFileName.indexOf(pFI->fileName()));
					if (sFileFromAddedDir.length() > m_sLastDirName.length())
						m_slAddedFilesFromSubdirectories.append(sFileName+"|"+ sFileFromAddedDir);
				}
			}
		}
		nProcessedFiles = m_nOpFileCounter + m_nOpDirCounter;
		if (m_nTotalItems > 0) {
			emit signalDataTransferProgress(nProcessedFiles, m_nTotalItems, 0, false); // last two args are not relevant
			emit signalTotalProgress((100 * nProcessedFiles) / m_nTotalItems, m_totalWeight);
		}
	}
}

void RarSubsystem::processingExtractCmd()
{
// 	qDebug() << "RarSubsystem::processingExtractCmd.\n" << m_sOutBuffer;
	QStringList slLinesBuffer = m_sOutBuffer.split('\n');
	if (slLinesBuffer.size() == 0)
		return;

	int nProcessedFiles, nStatId;
	QString sInpLine, sFileName;

	if (m_whatExtract == Vfs::EXTRACT_ARCHIVE) {
		FileInfo *pFileInfo;
		bool bIsDir, bContainsSeparator;
		QString sAbsCurrentPath = QFileInfo(m_sLastShellCmd.split('"').at(2)).path();
		sAbsCurrentPath = sAbsCurrentPath.trimmed();
		sAbsCurrentPath += QDir::separator();

		for (int i=0; i<slLinesBuffer.count(); i++) {
			sInpLine = slLinesBuffer.at(i);
			bIsDir = sInpLine.startsWith("Creating");
			if (! sInpLine.startsWith("Extracting") && ! bIsDir)
				continue;
			if (sInpLine.startsWith("Extracting from"))
				continue;
			if (m_processedFiles->size() > 1) // extrating more than 1 archive
				emit signalDataTransferProgress(++m_nOpFileCounter, m_nTotalItems, 0, false); // last two args are not relevant
//			else // comment was required to correctly extract multiply archives into current directory ("Extract here" operation)
			{ // extrating only 1 archive - need to process all files
				if (sInpLine.indexOf(0x08) != -1)
					sFileName = sInpLine.left(sInpLine.indexOf(0x08));
				else {
					nStatId = sInpLine.indexOf("OK");
					if (nStatId > 0)
						sFileName = sInpLine.left(nStatId);
					else {
						nStatId = sInpLine.indexOf("ERROR");
						if (nStatId > 0)
							sFileName = sInpLine.left(nStatId);
						else
							sFileName = sInpLine;
						//qDebug() << "RarSubsystem::processingExtractCmd. Cannot extract file:" << sFileName;
					}
				}
				sFileName.remove(0, sFileName.indexOf(QDir::rootPath())); // remove leading: "Extracting"/"Creating"
				sFileName = sFileName.trimmed();
				if (m_processedFiles->size() == 1) {
					emit signalNameOfProcessedFile(sFileName);
					if (bIsDir)
						emit signalCounterProgress(++m_nOpDirCounter, DIR_COUNTER, false);
					else
					if (sInpLine.startsWith("Extracting"))
						emit signalCounterProgress(++m_nOpFileCounter, FILE_COUNTER, false);
					nProcessedFiles = m_nOpFileCounter + m_nOpDirCounter;
					emit signalDataTransferProgress(nProcessedFiles, m_nTotalItems, 0, false); // last two args are not relevant
					emit signalTotalProgress((100 * nProcessedFiles) / m_nTotalItems, m_totalWeight);
				}
			}
			// -- helpful in the update data model after "Extract here" operation
			sFileName = sFileName.remove(0, sAbsCurrentPath.length());
			bContainsSeparator = sFileName.contains(QDir::separator());
			if ((bIsDir && ! bContainsSeparator) || (sInpLine.startsWith("Extracting") && ! bContainsSeparator)) { // directories and files from top level
				QFileInfo fi(sAbsCurrentPath+sFileName);
				if (fi.exists()) {
					QString sPerm = permissionsAsString(fi.absoluteFilePath());
					QString sMime = MimeType::getFileMime(fi.absoluteFilePath(), fi.isDir(), fi.isSymLink());
					qint64  nSize = fi.isSymLink() ? fi.symLinkTarget().length() : fi.size();
					QString sSymLinkTarget = fi.symLinkTarget();
					pFileInfo = new FileInfo(sFileName, fi.path(), nSize, fi.isDir(), fi.isSymLink(), fi.lastModified(), sPerm, fi.owner(), fi.group(), sMime, sSymLinkTarget);
					m_preProcessedFiles->append(pFileInfo);
				}
			}
		}
	}
	else { // Vfs::EXTRACT_FILES
		bool bIsDir = false;
		FileInfo *pFI;
		for (int i=0; i<slLinesBuffer.count(); i++) {
			sInpLine = slLinesBuffer.at(i);
			if (! sInpLine.startsWith("Extracting") && ! sInpLine.startsWith("Creating"))
				continue;
			if (sInpLine.startsWith("Extracting from"))
				continue;
			if (sInpLine.indexOf(0x08) != -1)
				sFileName = sInpLine.left(sInpLine.indexOf(0x08));
			else {
				nStatId = sInpLine.indexOf("OK");
				if (nStatId > 0)
					sFileName = sInpLine.left(nStatId);
				else {
					nStatId = sInpLine.indexOf("ERR");
					if (nStatId > 0)
						sFileName = sInpLine.left(nStatId);
					qDebug() << "RarSubsystem::processingExtractCmd. Cannot extract file:" << sFileName;
				}
			}
			sFileName.remove(0, sFileName.indexOf(QDir::rootPath())); // remove leading: "Extracting"/"Creating"
			sFileName = sFileName.trimmed();
			foreach (pFI, *m_processedFiles) {
				if (sFileName.indexOf(pFI->fileName()) != -1) {
					bIsDir = pFI->isDir() && ! pFI->isSymLink();
					break;
				}
			}
			emit signalNameOfProcessedFile(sFileName);
			if (bIsDir) {
				if (m_sLastDirName != pFI->fileName()) {
					emit signalCounterProgress(++m_nOpDirCounter, DIR_COUNTER, false);
					m_sLastDirName = pFI->fileName();
				}
				else // the most likely file from extracting directory
					emit signalCounterProgress(++m_nOpFileCounter, FILE_COUNTER, false);
			}
			else {
				emit signalCounterProgress(++m_nOpFileCounter, FILE_COUNTER, false);
			}
			nProcessedFiles = m_nOpFileCounter + m_nOpDirCounter;
			emit signalDataTransferProgress(nProcessedFiles, m_nTotalItems, 0, false); // last two args are not relevant
			emit signalTotalProgress((100 * nProcessedFiles) / m_nTotalItems, m_totalWeight);
		}
	}
}


void RarSubsystem::initSearching()
{
	m_processedFiles->clear();
	m_nMatchedFiles = 0;
	m_nFileCounter = 0;
	m_nDirCounter = 0;
	m_status = FINDING;
	m_error  = NO_ERROR;
	m_currentCommand = FIND;
	m_sSearchingResult = "";
	m_bOperationFinished = false;
	m_bCreateNewArchive = false;
	m_findRegExpLst.clear();
	emit signalStatusChanged(m_status, m_sRequestingPanel); // update status bar
}

void RarSubsystem::searchFiles( FileInfoToRowMap *pSelectionMap, const URI &uri, const FindCriterion &fcFindCriterion, const QString &sRequestingPanel )
{
	if (pSelectionMap == NULL) {
		qDebug() << "RarSubsystem::searchFiles. Empty selectionMap.";
		return;
	}
	if (fcFindCriterion.isEmpty()) {
		qDebug() << "RarSubsystem::searchFiles. Empty findCriterion.";
		return;
	}
	// -- check if opened archive is cached
	if (! archiveOpened(uri)) {
		qDebug() << "RarSubsystem::searchFiles. Archive didn't cache yet. Need to reopen.";
		open(uri, sRequestingPanel);
		m_opQueue << Operation(FIND, NULL, "", sRequestingPanel);
		slotRunNextOperation();
		return;
	}

	qDebug() << "RarSubsystem::searchFiles";
	m_findCriterion = fcFindCriterion;
	m_sRequestingPanel = sRequestingPanel;

	initSearching();
	QStringList slNameLst = m_findCriterion.nameLst.split('|');
	foreach (QString sName, slNameLst)
		m_findRegExpLst.append(QRegExp(sName, m_findCriterion.caseSensitivity, m_findCriterion.patternSyntax));
	startSearching(pSelectionMap, uri);
}

void RarSubsystem::startSearching( FileInfoToRowMap *, const URI &uri )
{
	qDebug() << "RarSubsystem::startSearching. -- uri.path (start path):" << uri.path();
	// TODO: include selectionMap in searching
	m_sSearchHost = uri.host();
	m_sStartSearchPath = uri.path().right(uri.path().length()-1);

	m_nSearchedLines = -1;
	m_searchingTimer.start(0);
}

bool RarSubsystem::isFileMatches( FileInfo *pFileInfo )
{
	bool matchFileOwner = true;
	bool matchFileGroup = true;
	bool matchTime = true;
	bool matchSize = true;
	bool matchName = false;
// 	bool matchName = m_findRegExp.exactMatch(fileInfo->fileName()); // check name matching
	foreach (QRegExp p, m_findRegExpLst) {
		if ((matchName=p.exactMatch(pFileInfo->fileName()))) // check if given name matches to pattern
			break;
	}
	if (! matchName)
		return false;

	if (m_findCriterion.checkFileOwner)
		matchFileOwner = (pFileInfo->owner() == m_findCriterion.owner);
	if (m_findCriterion.checkFileGroup)
		matchFileGroup = (pFileInfo->group() == m_findCriterion.group);

	if (m_findCriterion.checkTime) {
		QDateTime fileTime = pFileInfo->lastModified();
		matchTime = (fileTime >= m_findCriterion.firstTime && fileTime <= m_findCriterion.secondTime);
	}

	qint64 fileSize = pFileInfo->size();
	if (    m_findCriterion.checkFileSize != FindCriterion::NONE) {
		if (m_findCriterion.checkFileSize == FindCriterion::EQUAL)
			matchSize = (fileSize == m_findCriterion.fileSize);
		else if (m_findCriterion.checkFileSize == FindCriterion::ATLEAST)
			matchSize = (fileSize >= m_findCriterion.fileSize);
		else if (m_findCriterion.checkFileSize == FindCriterion::MAXIMUM)
			matchSize = (fileSize <= m_findCriterion.fileSize);
		else if (m_findCriterion.checkFileSize == FindCriterion::GREAT_THAN)
			matchSize = (fileSize >  m_findCriterion.fileSize);
		else if (m_findCriterion.checkFileSize == FindCriterion::LESS_THAN)
			matchSize = (fileSize <  m_findCriterion.fileSize);
	}

	return (matchName && matchFileOwner && matchFileGroup && matchTime && matchSize);
}

QString RarSubsystem::searchingResult()
{
	m_sSearchingResult = QString("%1 %2 %3").arg(m_nMatchedFiles).arg(m_nFileCounter).arg(m_nDirCounter);
	return m_sSearchingResult;
}


void RarSubsystem::copyFiles( FileInfoToRowMap *pSelectionMap, const QString &sTargetPath, bool bMove, bool bWeighBefore, const QString &sRequestingPanel )
{
	URI uri(sTargetPath);
	if (! uri.isValid()) {
		if (QFileInfo(QFileInfo(sTargetPath).path()).exists()) // probably new archive, so only directory should exists
			uri.setPath(sTargetPath);
		else {
			qDebug() << "RarSubsystem::copyFiles. Target directory doesn't exist:" << QFileInfo(sTargetPath).path();
			return;
		}
	}
	addTo((bMove ? MOVE_TO_ARCHIVE : COPY_TO_ARCHIVE), pSelectionMap, uri, sRequestingPanel);
}

void RarSubsystem::addTo( SubsystemCommand subsystemCommand, FileInfoToRowMap *pSelectionMap, const URI &uri, const QString &sRequestingPanel )
{
	QString sFN = "RarSubsystem::addTo.";
	if (pSelectionMap == NULL || pSelectionMap->size() == 0) {
		qDebug() << sFN << "Internal ERROR. Empty or null SelectionMap!";
		return;
	}
	m_opQueue.clear();
	m_currentCommand = subsystemCommand;
	// 	emit signalGetWeighingResult(m_totalWeight, m_nTotalItems);
	// 	qDebug() << sFN << "File:" << uri.toString() << "Weigh:" << FileInfo::bytesRound(m_totalWeight) << "TotalItems:";
	qDebug() << sFN << "File:" << uri.toString();
	if (! QFileInfo(uri.toString()).exists()) {
		if (! uri.toString().endsWith('/')) {
			createNewArchive(uri.toString(), pSelectionMap, sRequestingPanel);
			return;
		}
	}
	bool bArchiveFiles = (subsystemCommand == COPY_TO_ARCHIVE || subsystemCommand == MOVE_TO_ARCHIVE);
	if (bArchiveFiles && uri.path().isEmpty()) {
		m_error  = CANNOT_ARCHIVE;
		m_status = ERROR_OCCURED;
		m_sProcessedFileName = uri.host();
		emit signalStatusChanged(m_status, sRequestingPanel);
		return;
	}
	if (uri.host().isEmpty()) {
		qDebug() << sFN << "Internal ERROR. Empty uri.host!";
		return;
	}
	Settings settings;
	bool bAlwaysOverwrite = settings.value("FilesSystem/AlwaysOverwriteWhenArchiving", true).toBool();
	m_sRequestingPanel = sRequestingPanel;
	m_sRequestingPanel_open = sRequestingPanel;
	m_processedFileInfo = pSelectionMap->constBegin().key();
	initArchiving();
	m_preProcessedFiles->clear();

	QString   sUriPath = uri.path().right(uri.path().length()-1); // remove first slash
	QString   sRarCmdForFiles, sRarCmdForDirs;
	QString   sFullFileNamesToAdd, sFullDirsNamesToAdd;
	QString   sOvrMode = bAlwaysOverwrite ? " -o+ " : "  -o- ";
	FileInfo *pFileInfo, *pNewFileInfo;
	int nDirsNum  = 0;
	int nFilesNum = 0;

	m_slAddedFilesFromSubdirectories.clear();

	FileInfoToRowMap::const_iterator it = pSelectionMap->constBegin();
	while (it != pSelectionMap->constEnd()) {
		pFileInfo = it.key();
		if (bArchiveFiles) {
			if (pFileInfo->isDir() && ! pFileInfo->isSymLink()) {
				sFullDirsNamesToAdd += " \""+pFileInfo->absoluteFileName()+"\"";
			}
			else { // file or symlink
				sFullFileNamesToAdd += " \""+pFileInfo->absoluteFileName()+"\"";
			}
		}
		m_preProcessedFiles->append(pFileInfo);
		pNewFileInfo = new FileInfo(pFileInfo);
		pNewFileInfo->setData(2, sUriPath, true); // workaround for setting path (for second panel we need to set correct path for new item)
		m_processedFiles->append(pNewFileInfo);
		if (pFileInfo->isDir() && ! pFileInfo->isSymLink())
			nDirsNum++;
		else
			nFilesNum++;
		m_totalWeight += pFileInfo->size();
		++it;
	}

	m_nTotalItems = pSelectionMap->count();
	if (bArchiveFiles) {
		m_opQueue << Operation(PROG_EXISTS, "which rar", sRequestingPanel);
		m_opQueue << Operation(CHK_VER, "rar", sRequestingPanel);
		if (! sFullFileNamesToAdd.isEmpty()) {
			//rar a -o+ -ep -ol -ap"lxterminal-0.1.6" "/home/piotr/tmp/lxterminal-0.1.6.rar" "/home/tst_qtcmd/test_archives/1/fran.txt"
			sRarCmdForFiles = "rar a"+sOvrMode+"-ep -ol -ap\""+sUriPath+"\" \""+uri.host()+"\"" + sFullFileNamesToAdd; // no need to put space after -ap switch; default file is overwrote
			m_opQueue << Operation(subsystemCommand, sRarCmdForFiles, sRequestingPanel);
		}
		if (! sFullDirsNamesToAdd.isEmpty()) {
			//rar a -o+ -ep1 -ap"lxterminal-0.1.6" "/home/piotr/tmp/lxterminal-0.1.6.rar" "/home/tst_qtcmd/test_archives/1/bbc"
			sRarCmdForDirs = "rar a"+sOvrMode+"-ep1 -ap\""+sUriPath+"\" \""+uri.host()+"\"" + sFullDirsNamesToAdd; // no need to put space after -ap switch; default file is overwrote
			m_opQueue << Operation(subsystemCommand, sRarCmdForDirs, sRequestingPanel);
		}
		emit signalCounterProgress(nDirsNum, DIR_COUNTER, true);
		emit signalCounterProgress(nFilesNum, FILE_COUNTER, true);
		emit signalCounterProgress(m_totalWeight, WEIGH_COUNTER, true);
	}
	emit signalNameOfProcessedFile(m_processedFileInfo->absoluteFileName(), uri.host()+uri.path());
	slotRunNextOperation();
}

void RarSubsystem::createNewArchive( const QString &sInArchiveName, FileInfoToRowMap *pSelectionMap, const QString &sRequestingPanel )
{
	QString sFN = "RarSubsystem::createNewArchive.";
	if (pSelectionMap == NULL || pSelectionMap->size() == 0) {
		qDebug() << sFN << "Internal ERROR. Empty or null SelectionMap!";
		return;
	}
// 	bool bSingleMultiFileArchive = (sInArchiveName.endsWith('|'));
	qDebug() << sFN << "ArchiveName:" << sInArchiveName;
	QString sTargetPath = QFileInfo(sInArchiveName).path();
	if (! QFileInfo(sTargetPath).isWritable()) {
		m_error  = CANNOT_WRITE;
		m_status = ERROR_OCCURED;
		m_sProcessedFileName = sTargetPath;
		emit signalStatusChanged(m_status, sRequestingPanel);
		return;
	}
	m_sRequestingPanel_open = sRequestingPanel;
	FileInfo *pFileInfo = pSelectionMap->constBegin().key();
	m_processedFileInfo = pFileInfo;
	m_preProcessedFiles->clear();
	m_preProcessedFiles->append(pFileInfo);

	initArchiving();
// 	m_nTotalItems = 0; // weighing is doing in different subsystem
	emit signalGetWeighingResult(m_totalWeight, m_nTotalItems);
	m_bCreateNewArchive = true;
	Settings settings;
	bool bAlwaysOverwriteTargetIfCompress = settings.value("FilesSystem/AlwaysOverwriteWithNewArchive", false).toBool();

	QString sCompressionLevel = settings.value("archive/RarCompressionLevel", "6").toString(); // range 0-5
	int nCompressionLevel = sCompressionLevel.toInt();
	if (nCompressionLevel < 0)
		sCompressionLevel = "0";
	else
	if (nCompressionLevel > 5)
		sCompressionLevel = "5";

	QString sFullFileNamesToAdd = "";
	FileInfoToRowMap::const_iterator it = pSelectionMap->constBegin();
	while (it != pSelectionMap->constEnd()) {
		pFileInfo = it.key();
		m_processedFiles->append(pFileInfo); // m_processedFiles is cleaned in initArchiving
		sFullFileNamesToAdd += "\""+pFileInfo->absoluteFileName();
		sFullFileNamesToAdd += "\" ";
		++it;
	}
	sFullFileNamesToAdd.remove(sFullFileNamesToAdd.length()-1, 1); // remove last coma

	QString sArchiveName = sInArchiveName;
	QString sFullArchiveName = sInArchiveName;
	if (sInArchiveName.endsWith('|')) { // SingleMultiFileArchive
		sFullArchiveName = sFullArchiveName.left(sFullArchiveName.indexOf('|'));
		sArchiveName = QFileInfo(sFullArchiveName).absoluteFilePath();
	}
	if (bAlwaysOverwriteTargetIfCompress) {
		if (QFile(sFullArchiveName).remove())
			qDebug() << sFN << "Removed file:" << sFullArchiveName;
	}

	QString sDictionarySize = "1024";// m_pSettings->value("archive/RarDictionarySize", "1024").toString(); // md<n>[k,m,g]  Dictionary size in KB, MB or GB
	QString sCmd = "rar a -m" + sCompressionLevel +" -md"+sDictionarySize +" -ep1 -ap\"\" -ol \""+ sArchiveName +"\" "+ sFullFileNamesToAdd;
	//rar a -m6 -md1024 -ep1 -ap"" /home/piotrek/Wideo/filmy_folder_przejsciowy/Adobe.rar /home/Adobe
	m_opQueue << Operation(m_currentCommand, sCmd, sRequestingPanel);
	emit signalNameOfProcessedFile(m_processedFileInfo->absoluteFileName(), sFullArchiveName);
	slotRunNextOperation();
}

void RarSubsystem::extractTo(WhatExtract whatExtract, bool bMove, FileInfoToRowMap *pSelectionMap, const QString &sTargetPath, const QString &sRequestingPanel)
{
	QString sFN = "RarSubsystem::extractTo.";
	if (pSelectionMap == NULL || pSelectionMap->size() == 0) {
		qDebug() << sFN << "Internal ERROR. Empty or null SelectionMap!";
		return;
	}
	qDebug() << sFN << "TargetPath:" << sTargetPath;
	m_opQueue.clear();
	if (! QFileInfo(sTargetPath).isWritable()) {
		m_error  = CANNOT_WRITE;
		m_status = ERROR_OCCURED;
		m_currentCommand = EXTRACT;
		m_bOperationFinished = true;
		m_sProcessedFileName = sTargetPath;
		emit signalStatusChanged(m_status, sRequestingPanel);
		return;
	}

	Settings settings;
	bool bPersistPathInExtractingArch = settings.value("archive/PersistPathInExtractingArch", true).toBool();
	bool bWeighBeforeArchExtracting   = settings.value("archive/WeighBeforeArchExtracting", true).toBool();
	bool bWeighBeforeFilesExtracting  = true; // always weigh files
	bool bAlwaysOverwrite = settings.value("FilesSystem/AlwaysOverwrite", true).toBool();
	m_sRequestingPanel = sRequestingPanel;
	m_sRequestingPanel_open = sRequestingPanel;
	m_whatExtract = whatExtract;
	m_processedFileInfo = pSelectionMap->constBegin().key();
	initWeighing(true, m_sRequestingPanel);
	if (whatExtract == Vfs::EXTRACT_FILES) {
		if (bWeighBeforeFilesExtracting) {
			weigh(pSelectionMap, currentURI(), sRequestingPanel);
			emit signalCounterProgress(m_nDirCounter,  DIR_COUNTER, true);
			emit signalCounterProgress(m_nFileCounter, FILE_COUNTER, true);
			emit signalCounterProgress(m_totalWeight,  WEIGH_COUNTER, true);
		}
	}
	initExtracting();

	FileInfo *pFileInfo;
	QString   sFileNameList;
	QString   sUnRarCmd, sRarCmd;
	QString   sFullFileNamesToExtract, sFileName;
	QString   sOvrMode = bAlwaysOverwrite ? " -o+ " : "  -o- ";
	QString   sExtrCmd = bPersistPathInExtractingArch ? " x " : " e ";
	sOvrMode = " -o+ "; // because "Overwrite mode" is not supported yet TODO: implement "Overwrite mode" for extracting entire archives

	if ((whatExtract == EXTRACT_ARCHIVE && ! bWeighBeforeArchExtracting) || whatExtract == EXTRACT_FILES)
		initQueueForPreCheck(sRequestingPanel); // update queue with commands to check existing and version

    FileInfoToRowMap::const_iterator it = pSelectionMap->constBegin();
	while (it != pSelectionMap->constEnd()) {
		pFileInfo = it.key();
		if (whatExtract == EXTRACT_ARCHIVE) {
			if (! fileIsMatchingToSubsystemMime(m_pPluginData->mime_type, pFileInfo)) {
				++it;
				continue;
			}
// 			if (! bAlwaysOverwrite) {
// 				emit signalShowFileOverwriteDlg();
// 			}
			sFileName = absFileNameInLocalFS(pFileInfo, m_sTmpDir);
			sFullFileNamesToExtract = "\"" + sFileName + "\" ";
			// rar x archive.rar /path/to/extract/to
			sUnRarCmd = "unrar" + sExtrCmd + sOvrMode + sFullFileNamesToExtract + sTargetPath;
			sRarCmd   = "rar"   + sExtrCmd + sOvrMode + sFullFileNamesToExtract + sTargetPath;
			if (bWeighBeforeArchExtracting)
				m_opQueue << Operation(WEIGH_ARCH, pFileInfo, sTargetPath, sRequestingPanel);
			m_opQueue << Operation(EXTRACT, sUnRarCmd, sRequestingPanel);
			m_opQueue << Operation(EXTRACT, sRarCmd, sRequestingPanel);
		}
		else { // EXTRACT_FILES
			sFileNameList += " \"" + pFileInfo->absoluteFileName() +"\"";
		}
		m_processedFiles->append(pFileInfo);
		++it;
	}

	qDebug() << sFN << "TotalItems:" << m_nTotalItems << "processedFiles:" << m_processedFiles->size();
	if (whatExtract == EXTRACT_FILES) {
		if (bMove)
			m_currentCommand = Vfs::EXTRACT_AND_REMOVE;
		// unrar x -cfg- -ow -ol -o+ -ap"lxterminal-0.1.6/" "/home/piotr/tmp/lxterminal-0.1.6.rar" "lxterminal-0.1.6/data" "lxterminal-0.1.6/config.h.in" "/home/tst_qtcmd/test_archives/1/bbc1/"
		sUnRarCmd = "unrar x -cfg- -ow -ol"+ sOvrMode +"-ap\""+pFileInfo->filePath()+"\" \""+m_sHost+"\"" + sFileNameList + " \""+sTargetPath+"\"";
		sRarCmd   = "rar x -cfg- -ow -ol"+ sOvrMode +"-ap\""+pFileInfo->filePath()+"\" \""+m_sHost+"\"" + sFileNameList + " \""+sTargetPath+"\"";
		// all operations are duplicated due to lack of knowledge which command exists unwanted are cleaned in slotProcessFinished
		m_opQueue   << Operation(m_currentCommand, sUnRarCmd, sRequestingPanel)
					<< Operation(m_currentCommand, sRarCmd, sRequestingPanel);
	}
	emit signalNameOfProcessedFile(sFileName, sTargetPath);
	slotRunNextOperation();
}

void RarSubsystem::initArchiving()
{
	m_sOutBuffer = "";
	m_sLastDirName = "";
	m_opQueue.clear();
	m_processedFiles->clear();
	m_totalWeight    = 0;
	m_OpTotalWeight  = 0;
	m_nOpDirCounter  = 0;
	m_nOpFileCounter = 0;
	m_nOpSymlinksCounter = 0;
	m_status = ARCHIVING;
	m_bCreateNewArchive = false;
	emit signalStatusChanged(m_status, m_sRequestingPanel);
}

void RarSubsystem::initExtracting()
{
	if (m_status == WEIGHED) {
		qDebug() << "RarSubsystem::initExtracting. WEIGHED - totalWeigh:" << m_totalWeight << FileInfo::bytesRound(m_totalWeight) << "files:" << m_nFileCounter << "dirs:" << m_nDirCounter << "all items:" << m_nTotalItems;
	}
// 	qDebug() << "RarSubsystem::initExtracting. Cleaning variables.";
	m_sOutBuffer = "";
	m_sLastDirName = "";
	m_opQueue.clear();
	m_processedFiles->clear();
	m_preProcessedFiles->clear();
	m_OpTotalWeight  = 0;
	m_nOpDirCounter  = 0;
	m_nOpFileCounter = 0;
	m_nOpSymlinksCounter = 0;
	m_currentCommand = EXTRACT;
	m_status = EXTRACTING;
	m_bCreateNewArchive = false;
	// 	m_sRequestingPanel = sRequestingPanel; // used in slotProcessFinished
	emit signalStatusChanged(m_status, m_sRequestingPanel);
}


void RarSubsystem::updateContextMenu( ContextMenu *pContextMenu, KeyShortcuts *pKS )
{
	qDebug() << "RarSubsystem::updateContextMenu.";
	m_pContextMenu = pContextMenu;

	m_pContextMenu->setVisibleAction(CMD_ExtractTo, false);
	m_pContextMenu->setVisibleAction(CMD_Makelink, false);
	m_pContextMenu->setVisibleAction(CMD_EditSymlink, false);
	m_pContextMenu->setVisibleAction(CMD_Rename, false);
	m_pContextMenu->setVisibleAction(CMD_CreateNew_MENU, false);
}


void RarSubsystem::searchingFinished()
{
	m_searchingTimer.stop();
	m_processedFiles->clear();
	qDebug() << "RarSubsystem::searchingFinished. Finished at line:" << m_nSearchedLines;
	m_status = FOUND;
	m_bOperationFinished = true;
	emit signalStatusChanged(m_status, m_sRequestingPanel);
}

void RarSubsystem::setBreakOperation()
{
	qDebug() << "RarSubsystem::setBreakOperation. Operation stopped.";
	if (m_currentCommand == FIND)
		searchingFinished();
}

void RarSubsystem::setPauseOperation( bool bPause )
{
	qDebug() << "RarSubsystem::setPauseOperation. Pause:" << bPause;
	m_bPausedOperation = bPause;
	m_error = NO_ERROR;
	if (bPause) {
		m_searchingTimer.stop();
		m_pausedStatus = m_status;
		m_status = OPR_PAUSED;
	} else {
		m_status = m_pausedStatus;
		m_searchingTimer.start(0);
	}
	emit signalStatusChanged(m_status, m_sRequestingPanel);
}

void RarSubsystem::initQueueForPreCheck( const QString &sRequestingPanel )
{
	m_opQueue	<< Operation(PROG_EXISTS, "which unrar", sRequestingPanel)
				<< Operation(PROG_EXISTS, "which rar", sRequestingPanel)
				<< Operation(CHK_VER, "unrar", sRequestingPanel)
				<< Operation(CHK_VER, "rar", sRequestingPanel);
}

void RarSubsystem::updateParsingBuffer( SubsystemCommand subsysCmd )
{
	bool bArchiveFiles = (subsysCmd == COPY_TO_ARCHIVE || subsysCmd == MOVE_TO_ARCHIVE);
	if (subsysCmd != REMOVE && ! bArchiveFiles)
		return;

	QString sNewItem, sSize, sNewSize, sFoundName;
	QString sPerm, sAbsFileName, sLocalName;
	QFileInfo fi;
	int nId = -1;

	foreach (FileInfo *pFI, *m_processedFiles) {
		sLocalName = pFI->absoluteFileName();
		if (subsysCmd == REMOVE) {
			if (m_fRarVer >= 5) {
				nId = m_slParsingBufferLine.indexOf(QRegExp(".* "+sLocalName));
				if (nId != -1)
					m_slParsingBufferLine.removeAt(nId);
			}
			else {  // 4.x
				nId = m_slParsingBufferLine.indexOf(sLocalName);
				if (nId != -1) {
					m_slParsingBufferLine.removeAt(nId); // name
					m_slParsingBufferLine.removeAt(nId); // attributes
				}
			}
			nId = m_fiList.indexOf(pFI);
			if (nId != -1)
				m_fiList.removeAt(nId);
		}
		else
		if (bArchiveFiles) {
			if (sLocalName.startsWith(QDir::separator()))
				sLocalName.remove(0,1);
			sSize = QString("%1").arg(pFI->size());
			if (m_fRarVer >= 5) {
				nId = m_slParsingBufferLine.indexOf(QRegExp(".* "+sLocalName));
				if (nId != -1) {
					sFoundName = m_slParsingBufferLine.at(nId);
					sFoundName = sFoundName.right((sFoundName.length()-m_nNameColId) + 1);
					sFoundName = QString::fromLocal8Bit(sFoundName.toLatin1().data());
					if (sFoundName == sLocalName)
						continue;
				}
				sNewItem = pFI->permissions()+"  0  ??%  "+pFI->lastModified().toString("dd-MM-yy HH:mm")+"  ????????  ";
				sNewSize = sSize + QString(m_nNameColId - sNewItem.length() - sSize.length() - 1, ' ');
				sNewItem.insert(11, sNewSize); // permissions length + 1
				sNewItem += sLocalName;
				if (m_slParsingBufferLine.indexOf(sNewItem) != -1)
					continue;
			}
			else { // 4.x
				if (m_slParsingBufferLine.indexOf(sLocalName) != -1) // NOTE might occure an ERROR if unrar is in ver.5.x and rar in ver.4.x
					continue;
				if (m_slParsingBufferLine.indexOf(QRegExp(".* "+sLocalName))) // unrar is in ver.5.x and rar in ver.4.x
					continue;
				m_slParsingBufferLine.append(sLocalName);
				sNewItem = sSize+" 0 ??% "+pFI->lastModified().toString("dd-MM-yy HH:mm")+" "+pFI->permissions()+"  ???????? m5d 2.9";
			}
			m_slParsingBufferLine.append(sNewItem);
			m_fiList.append(pFI);
		}
	}
	m_nFilesNumInArch = m_slParsingBufferLine.count();

	// -- update internal list with newly files from added directory
	if (m_slAddedFilesFromSubdirectories.isEmpty())
		return;

	foreach (QString sFullFileName, m_slAddedFilesFromSubdirectories) {
		sLocalName = m_currentUri.path() +"/"+ sFullFileName.split('|').at(1);
		if (subsysCmd == REMOVE) {
			if (sLocalName.startsWith(QDir::separator())) // remove first separator
				sLocalName = sLocalName.right(sLocalName.length()-1);
			if (m_fRarVer >= 5) {
				nId = m_slParsingBufferLine.indexOf(QRegExp(".* "+sLocalName));
				if (nId != -1)
					m_slParsingBufferLine.removeAt(nId);
			}
			else {  // 4.x
				nId = m_slParsingBufferLine.indexOf(sLocalName);
				if (nId != -1) {
					m_slParsingBufferLine.removeAt(nId); // name
					m_slParsingBufferLine.removeAt(nId); // attributes
				}
			}
		}
		else
		if (bArchiveFiles) {
			sAbsFileName = sFullFileName.split('|').at(0);
			if (sLocalName.startsWith('/'))
				sLocalName = sLocalName.right(sLocalName.length()-1);
			fi.setFile(sAbsFileName);
			sSize = QString("%1").arg(fi.size());
			sPerm = permissionsAsString(sAbsFileName);
			if (m_fRarVer >= 5) {
				nId = m_slParsingBufferLine.indexOf(QRegExp(".* "+sLocalName));
				if (nId != -1) {
					sFoundName = m_slParsingBufferLine.at(nId);
					sFoundName = sFoundName.right((sFoundName.length()-m_nNameColId) + 1);
					sFoundName = QString::fromLocal8Bit(sFoundName.toLatin1().data());
					if (sFoundName == sLocalName)
						continue;
				}
				sNewItem = sPerm+"  0  ??%  "+fi.lastModified().toString("dd-MM-yy HH:mm")+"  ????????  ";
				QString sNewSize = sSize + QString(m_nNameColId - sNewItem.length() - sSize.length() - 1, ' ');
				sNewItem.insert(11, sNewSize); // permissions length + 1
				sNewItem += sLocalName;
				m_slParsingBufferLine.append(sNewItem);
			}
			else { // 4.x
				if (m_slParsingBufferLine.indexOf(sLocalName) != -1)
					continue;
				if (m_slParsingBufferLine.indexOf(sLocalName) < 0) {
					m_slParsingBufferLine.append(sLocalName);
					sNewItem = sSize+" 0 ??% "+fi.lastModified().toString("dd-MM-yy HH:mm")+" "+sPerm+"  ???????? m5d 2.9";
					m_slParsingBufferLine.append(sNewItem);
				}
			}
		}
	}
	if (subsysCmd == REMOVE)
		m_slAddedFilesFromSubdirectories.clear();

	m_nFilesNumInArch = m_slParsingBufferLine.count();
}

// ------------- SLOTs -------------

void RarSubsystem::slotRunNextOperation()
{
	QString sFN = "RarSubsystem::slotRunNextOperation.";
	if (m_opQueue.count() == 0) {
		qDebug() << sFN << "Empty queue. Exit.";
		return;
	}
	if (m_pProcess->state() != QProcess::NotRunning) {
		qDebug() << sFN << "Process is already running.";
		return;
	}
	m_sOutBuffer = "";
	m_sErrBuffer = "";

	Operation operation = m_opQueue.first();
	m_currentCommand = operation.command;  // internal command
	if (m_currentCommand == OPEN || m_currentCommand == VIEW)
		m_sRequestingPanel_open = operation.subsysMngrName;
	else
		m_sRequestingPanel = operation.subsysMngrName;

	bool bShellCmd = (! operation.shellCmd.isEmpty());
	m_opQueue.pop_front();
	qDebug() << sFN << " RequestingPanel:" << m_sRequestingPanel << "RequestingPanel_open:" << m_sRequestingPanel_open << " opQueue.size:" << m_opQueue.size();
	if (m_currentCommand == WEIGH_ARCH)
		m_bWeighingArchive = true;

	if (m_currentCommand == OPEN || m_currentCommand == VIEW) {
		if (bShellCmd)
			m_status = CONNECTING;
		else // reopen
			open(operation.uri, operation.subsysMngrName);
	}

	if (bShellCmd) {
		if      (m_currentCommand == OPEN_WITH_CMD)
			m_status = OPENING_WITH; // TODO: show progress bar for extracting files
		else if (m_currentCommand == COPY_TO_ARCHIVE || m_currentCommand == MOVE_TO_ARCHIVE)
			m_status = ARCHIVING;
		else if (m_currentCommand == EXTRACT || m_currentCommand == EXTRACT_AND_REMOVE)
			m_status = EXTRACTING;

		if (m_currentCommand != PROG_EXISTS)
			emit signalStatusChanged(m_status, m_sRequestingPanel_open);

		m_sLastShellCmd = operation.shellCmd;
		qDebug() << sFN << "Command:" << toString(m_currentCommand) << "Execute shell cmd:" << m_sLastShellCmd << " RequestingPanel:" << m_sRequestingPanel_open;
		if (m_currentCommand == EXTRACT) {
			if (m_whatExtract == EXTRACT_ARCHIVE) {
				m_nOpFileCounter = 0; // counter of all items in extracting archive
				QString sExtractedArch = m_sLastShellCmd.split('\"').at(1);
				for (int i = 0; i < m_processedFiles->size(); ++i) {
					if (m_processedFiles->at(i)->absoluteFileName() == sExtractedArch) {
						m_processedFileInfo = m_processedFiles->at(i);
						break;
					}
				}
			}
		}
		if (m_currentCommand == REMOVE)
			QTimer::singleShot(0, this, &RarSubsystem::slotStartProcess);
		else
			m_pProcess->start(m_sLastShellCmd);
	} else {
		if      (m_currentCommand == WEIGH)
			weigh(operation.selectionMap, operation.uri, operation.subsysMngrName, operation.booleanArg); // bWeighingAsSubCmd=true
		else if (m_currentCommand == WEIGH_ITEM)
			weighItem(operation.fileInfo, operation.uri, operation.subsysMngrName, operation.booleanArg);
		else if (m_currentCommand == WEIGH_ARCH) {
			weighArchive(absFileNameInLocalFS(operation.fileInfo, m_sTmpDir), operation.subsysMngrName);
// 			weighArchive(operation.fileInfo->absoluteFileName());
		}
		else if (m_currentCommand == REMOVE)
			remove(operation.selectionMap, operation.uri, operation.booleanArg, operation.subsysMngrName);
	}
}

void RarSubsystem::slotProcessFinished( int nExitCode, QProcess::ExitStatus exitStatus )
{
	QString sFN = "RarSubsystem::slotProcessFinished.";
	QString sSuccessFailrue = (nExitCode == 0) ?  "(Success)" : "(Failrue)";
	qDebug() << sFN << "ExitCode:" << nExitCode << sSuccessFailrue << " exitStatus:" << exitStatus << "m_currentCommand:" << Vfs::toString(m_currentCommand) << m_sRequestingPanel_open;
	m_bActionIsFinished = true;

	QString sCmdName;
	if (m_currentCommand == PROG_EXISTS)
		sCmdName = m_sLastShellCmd.split(' ').at(1); // useful only for 'which' command

	if (exitStatus == QProcess::NormalExit && nExitCode == 0) {
		m_error = NO_ERROR;
		if (m_currentCommand == PROG_EXISTS) {
			if (! m_sOutBuffer.isEmpty()) {
				qDebug() << sFN << "--" << sCmdName << "found in PATH.";
				m_sCommand = sCmdName;
				if (sCmdName == "unrar") {
					// -- clean queue with unnecessary operations, becase all are duplicated due to on begin lack of knowledge which command exists
					int size = m_opQueue.size();
					for (int i=0; i<size; i++) {
						if (! m_opQueue.at(i).shellCmd.startsWith(sCmdName)) {
							m_opQueue.remove(i);
							size--;
						}
					}
				}
				if (m_sRequestingPanel_open != "leftPanel_1:init")
					m_status = CONNECTED;
				qDebug() << sFN << sCmdName << "found in PATH.";
				QTimer::singleShot(20, this, &RarSubsystem::slotRunNextOperation);
			}
		}
		else
		if (m_currentCommand == CHK_VER) {
			if (! m_sOutBuffer.isEmpty()) {
				// -- get version
				QStringList slBuf = m_sOutBuffer.split('\n');
				if (slBuf.size() > 1) {
					QString sVerRow = slBuf[1];
					QStringList slVerBuf = sVerRow.split(' ');
					m_fRarVer = slVerBuf[1].toFloat();
					qDebug() << sFN << "--" << m_sCommand << "version is:" << m_fRarVer;
				}
				m_status = CONNECTED;
				QTimer::singleShot(20, this, &RarSubsystem::slotRunNextOperation);
			}
		}
		else
		if (m_currentCommand == OPEN || m_currentCommand == VIEW) { // parsing buffer for level = 0
			if (! m_sErrBuffer.isEmpty()) {
				//qDebug() << "m_sErrBuffer" << m_sErrBuffer;
				if (m_sErrBuffer.indexOf("Cannot open") != -1) { // RAR returns exitcode 0 even if an error occured
					m_status = ERROR_OCCURED;
					m_error  = CANNOT_OPEN;
					qDebug() << sFN << "-- Cannot open archive:" << m_sPath;
					return;
				}
			}
			else // no error, check if open for weighing arch. or for listing
			if (m_bWeighingArchive) {
				m_status = WEIGHING;
				emit signalStatusChanged(m_status, m_sRequestingPanel_open);
				m_currentCommand = WEIGH_ARCH;
			}
			else {
				if (m_currentCommand != VIEW) {
					m_status = LISTING;
					emit signalStatusChanged(m_status, m_sRequestingPanel_open);
				}
				parsingBufferedFilesList(); // parsing output of sys.op.command (inits m_nFilesNumInArch)
				if (m_sPath.endsWith(QDir::separator())) // if level > 0 then set next called command
					m_currentCommand = LIST;
			}
			qDebug() << sFN << "Switched from OPEN or VIEW command to:" << Vfs::toString(m_currentCommand);
		}

		if (m_currentCommand == OPEN || m_currentCommand == VIEW) {  // processing buffer for level = 0
			processingParsedFilesList(0, "");
			if (m_status == ERROR_OCCURED)
				return;
			m_sCurrentPath = QDir::separator();
			m_bOpenedArchive = true;
			m_currentUri.setPath(m_sCurrentPath);
			//qDebug() << m_sOutBuffer;
			//qDebug() << m_sParsingBufferLine;
			if (m_currentCommand != VIEW) {
				m_currentCommand = LIST;
				m_status = m_bCompleterMode ? LISTED_COMPL : LISTED;
			}
			else
				m_status = VIEW_COMPL;

			qDebug() << sFN << "Switched from OPEN or VIEW command to " << Vfs::toString(m_currentCommand) << "status:" << Vfs::toString(m_status) << "m_sPath:" << m_sPath << "m_sCurrentPath:" << m_sCurrentPath << "m_bOpenedArchive:" << m_bOpenedArchive << "m_nFilesNumInArch:" << m_nFilesNumInArch;
			emit signalStatusChanged(m_status, m_sRequestingPanel_open);
		}
		else
		if (m_currentCommand == LIST) {
			commandList();
		}
		else
		if (m_currentCommand == OPEN_WITH_CMD) {
			if (! m_sOutBuffer.isEmpty())
				processingOpenWithCmd();
			else {
				m_status = ERROR_OCCURED;
				m_error  = OTHER_ERROR;
			}
			emit signalStatusChanged(m_status, m_sRequestingPanel);
		}
		else
		if (m_currentCommand == REMOVE) {
			;
			m_status = REMOVED;
			m_bForceReopen = true;
			emit signalTimerStartStop(false);
			emit signalOperationFinished(); // only updates button label to 'Done' on progress dialog
			emit signalStatusChanged(m_status, m_sRequestingPanel);
			updateParsingBuffer(REMOVE); // also update internal files list with files added from subdirectories
		}
		else
		if (m_currentCommand == READ_FILE_TO_VIEW) {
			m_status = EXTRACTED_TO_VIEW;
			emit signalStatusChanged(m_status, m_sRequestingPanel);
		}
		else
		if (m_currentCommand == WEIGH_ARCH) {
			parsingBufferedFilesList(); // parsing output of sys.op.command (inits m_nFilesNumInArch)
			processingWeighCmd(NULL);
			m_status = WEIGHED;
			//emit signalStatusChanged(m_status, m_sRequestingPanel_open); // helped for Properties dialog
			emit signalCounterProgress(m_nDirCounter,  DIR_COUNTER, true);
			emit signalCounterProgress(m_nFileCounter, FILE_COUNTER, true);
			emit signalCounterProgress(m_totalWeight,  WEIGH_COUNTER, true);
			m_bWeighingArchive = false;
			if (m_opQueue.count() > 0) {
				if (m_opQueue.at(0).command == EXTRACT) {
					if (m_whatExtract == Vfs::EXTRACT_ARCHIVE) {
						long long freeBytes, totalBytes;
						Vfs::getStatFS(m_sTmpDir, freeBytes, totalBytes);
						if (freeBytes < m_totalWeight) {
							m_opQueue.clear();
							m_error = NO_FREE_SPACE;
							emit signalStatusChanged(Vfs::ERROR_OCCURED, m_sRequestingPanel);
							return;
						}
					}
				}
				slotRunNextOperation();
				return;
			}
			else {
				emit signalStatusChanged(m_status, m_sRequestingPanel);
				return;
			}
		}
		else
		if (m_currentCommand == EXTRACT || m_currentCommand == EXTRACT_AND_REMOVE) {
			if (m_opQueue.count() > 0) {
				emit signalStatusChanged(EXTRACTED, m_sRequestingPanel); // for update extracting status on progress window
				QTimer::singleShot(0, this, &RarSubsystem::slotRunNextOperation);
				return;
			}
			else {
				m_status = EXTRACTED;
				if (m_whatExtract == EXTRACT_FILES)
					m_bOperationFinished = true;
				emit signalOperationFinished(); // only updates button label on progress dialog to 'Done' on progress dialog
				emit signalStatusChanged(m_status, m_sRequestingPanel);
			}
		}
		else
		if (m_currentCommand == COPY_TO_ARCHIVE || m_currentCommand == MOVE_TO_ARCHIVE) {
			// need to run on two times, because directories and files are added separately
			if (m_opQueue.count() > 0) {
				QTimer::singleShot(0, this, &RarSubsystem::slotRunNextOperation);
				return;
			}
			else { // (m_opQueue.size() == 0)
				m_status = ARCHIVED;
				m_bForceReopen = true;
				m_bOperationFinished = true;
				if (! m_bCreateNewArchive)
					updateParsingBuffer(m_currentCommand); // also update internal files list with files added from subdirectories
				emit signalOperationFinished(); // only updates button label on progress dialog to 'Done'
				emit signalStatusChanged(m_status, m_sRequestingPanel);
			}
		}

		if ((m_currentCommand == LIST || m_currentCommand == VIEW) && m_opQueue.count() > 0) {
			qDebug() << sFN << toString(m_currentCommand) << "command finished. Call next queued command";
			if (m_sRequestingPanel_open != m_opQueue.first().subsysMngrName) { // if opened two the same archives then list previous
				emit signalStatusChanged(m_status, m_sRequestingPanel_open);
				QTimer::singleShot(0, this, &RarSubsystem::slotRunNextOperation);
				return;
			}
			else
				slotRunNextOperation();
		}

		if (m_currentCommand == LIST && m_status != CONNECTED) {
			qDebug() << sFN << "Listing finished. status:" << toString(m_status);
			emit signalStatusChanged(m_status, m_sRequestingPanel_open);
		}
	}
	else { // error occured
		qDebug() << sFN << "-- Process finished with error:" << nExitCode << "command:" << toString(m_currentCommand);
		qDebug() << sFN << "LastShellCmd:" << m_sLastShellCmd;
		qDebug() << m_sErrBuffer;
		m_status = ERROR_OCCURED;
		m_error  = OTHER_ERROR;
		m_bForceReopen = true;
		if (m_processedFileInfo != NULL)
			m_sProcessedFileName = m_processedFileInfo->absoluteFileName();
		if (m_currentCommand == PROG_EXISTS) {
			if (! m_sErrBuffer.isEmpty()) {
				m_error = CMD_NOT_FOUND;
				qDebug() << sFN << sCmdName << "not found in PATH!";
				if (sCmdName == "unrar") {
					m_status = CONNECTING;
					m_error  = NO_ERROR;
					// -- clean queue with unnecessary operations, becase all are duplicated due to on begin lack of knowledge which command exists
					int size = m_opQueue.size();
					for (int i=0; i<size; i++) {
						if (m_opQueue.at(i).shellCmd.startsWith(sCmdName)) {
							m_opQueue.remove(i);
							size--;
						}
					}
					QTimer::singleShot (20, this, SLOT(slotRunNextOperation())); // get next command from queue
					return;
				}
				else { // rar
					// - find properly command
					while (m_opQueue.size() > 0) {
						if (m_opQueue.first().command != PROG_EXISTS && m_opQueue.first().command != CHK_VER)
							break;
						m_opQueue.pop_front();
					}
					m_sProcessedFileName = "";
					if (m_opQueue.size() > 0) {
						Operation operation = m_opQueue.first();
						if (operation.command == REMOVE)
							m_sProcessedFileName = tr("Removing operation is not possible.")+"\n\n"+tr("Cannot find rar command in system PATH!");
						else
						if (operation.command == COPY_TO_ARCHIVE)
							m_sProcessedFileName = tr("Archiving operation is not possible.")+"\n\n"+tr("Cannot find rar command in system PATH!");
						else {
							if (operation.command == OPEN || operation.command == VIEW) m_sProcessedFileName = tr("Opening archive");
							else
							if (operation.command == EXTRACT || operation.command == EXTRACT_AND_REMOVE) m_sProcessedFileName = tr("Extracting");
							else
							if (operation.command == WEIGH_ARCH) m_sProcessedFileName = tr("Weighing");
							else
							if (operation.command == OPEN_WITH_CMD) m_sProcessedFileName = tr("Opening file");
							else
							if (operation.command == READ_FILE_TO_VIEW) m_sProcessedFileName = tr("Previewing file");
							m_sProcessedFileName += " "+tr("operation is not possible.") +"\n\n"+tr("Cannot find nor unrar neither rar command in system PATH!");
						}
					}
				}
			}
		}
		else
		if (m_currentCommand == CHK_VER) {
			m_error = CMD_NOT_FOUND;
			sCmdName = m_sLastShellCmd.split(' ').at(0);
			qDebug() << sFN << "-- Cannot check version for:" << sCmdName;
		}
		else
		if (m_currentCommand == OPEN_WITH_CMD) {
			if (! m_sErrBuffer.isEmpty()) {
				m_error = CANNOT_EXTRACT;
				qDebug() << sFN << "-- Cannot extract all input files";
			}
		}
		else
		if (m_currentCommand == OPEN || m_currentCommand == READ_FILE_TO_VIEW) {
			m_sProcessedFileName = m_currentUri.host();
			m_error = CANNOT_READ;
		}
		else if (m_currentCommand == REMOVE) {
			m_error = CANNOT_REMOVE;
		}
		else
		if (m_currentCommand == EXTRACT || m_currentCommand == EXTRACT_AND_REMOVE) {
			m_error = CANNOT_EXTRACT;
			// following error often happens in remove operation: 9  CREATE ERROR  Create file error  "Cannot create __rar_0.495". Helps restart of app.
			// This is happend sometimes if user want extract and immediately remove extracted directory/file.
			// Command running directly from terminal works.
		}
		else
		if (m_currentCommand == COPY_TO_ARCHIVE || m_currentCommand == MOVE_TO_ARCHIVE) {
			m_error = CANNOT_ARCHIVE;
		}
		m_bOperationFinished = true;
		emit signalStatusChanged(m_status, ((m_currentCommand == LIST || m_currentCommand == OPEN) ? m_sRequestingPanel_open : m_sRequestingPanel));
	}
}

void RarSubsystem::slotBreakProcess()
{
	if (m_pProcess->exitStatus() == QProcess::CrashExit) { // condition maybe not necessary (not tested) :/
		m_pProcess->kill();
		m_bActionIsFinished = true;
		m_status = OPR_STOPPED;
		m_error  = NO_ERROR;
		m_opQueue.clear();
		emit signalStatusChanged(m_status, ((m_currentCommand == LIST || m_currentCommand == OPEN) ? m_sRequestingPanel_open : m_sRequestingPanel));
	}
}


void RarSubsystem::slotReadStandardOutput()
{
	if (m_currentCommand == REMOVE
		|| m_currentCommand == EXTRACT || m_currentCommand == EXTRACT_AND_REMOVE
		|| m_currentCommand == COPY_TO_ARCHIVE || m_currentCommand == MOVE_TO_ARCHIVE )
	{
		m_sOutBuffer = m_pProcess->readAllStandardOutput();

		if (m_currentCommand == REMOVE) // slotReadStandardOutput is never called, because tar doesn't prints deleted files
			processingRemoveCmd();
		else
		if (m_currentCommand == EXTRACT || m_currentCommand == EXTRACT_AND_REMOVE)
			processingExtractCmd();
		else
			if (m_currentCommand == COPY_TO_ARCHIVE || m_currentCommand == MOVE_TO_ARCHIVE)
			processingArchiveCmd();
	}
	else
		m_sOutBuffer += m_pProcess->readAllStandardOutput();
}

void RarSubsystem::slotReadStandardError()
{
	m_sErrBuffer += m_pProcess->readAllStandardError();
}

void RarSubsystem::slotStartProcess()
{
	m_pProcess->start(m_sLastShellCmd);
}

void RarSubsystem::slotReadStandardOutputAsyn()
{
	if (m_currentCommand == REMOVE)
		QTimer::singleShot(0, this, &RarSubsystem::slotReadStandardOutput);
	else
		slotReadStandardOutput();
}

void RarSubsystem::slotSearching()
{
	if (m_findCriterion.stopIfXMatched) {
		if (m_nMatchedFiles == m_findCriterion.stopAfterXMaches) {
			searchingFinished();
			return;
		}
	}
	m_nSearchedLines++;
	if (m_nSearchedLines >= m_nFilesNumInArch) {
		searchingFinished();
		return;
	}

	int nLineId;
	QString sInpLine, sLine, sFileName, sFullPath, sMime, sDateTime, sSize, sPerm;
	bool isDir, isSymLink;
	QStringList slLine;
	FileInfo *pFileInfo;

	sInpLine = m_slParsingBufferLine.at(m_nSearchedLines);
	if (m_fRarVer < 5) {
		sLine = sInpLine;
		nLineId = m_slParsingBufferLine.indexOf(sLine);
		if (nLineId < 0)
			return;
	}
	else { // ver.5.x
		sFileName = sInpLine.right((sInpLine.length() - m_nNameColId) + 1);
		sLine = sFileName;
	}
	emit signalNameOfProcessedFile(sLine);
	if (! m_sStartSearchPath.isEmpty()) { // doesn't top level
		if (! sLine.startsWith(m_sStartSearchPath))
			return;
	}
	//sName = QFileInfo(sLine).fileName();
	sFileName = sLine.right(sLine.length() - sLine.lastIndexOf(QDir::separator()) - 1);
	sFullPath = m_sSearchHost + QDir::separator() + QFileInfo(sLine).path();
	if (m_fRarVer < 5) {
		if (nLineId +1 > m_slParsingBufferLine.size()-1)
			return;
		sLine = m_slParsingBufferLine[nLineId +1];
		if (sLine.isEmpty())
			return;
		slLine = sLine.split(' ');
		if (slLine.count() < PERM)
			return;
	}
	else { // ver.5.x
		slLine = sInpLine.split(QRegExp("\\s+"), Qt::SkipEmptyParts);
		if (slLine.count() < TIMEv5)
			return;
	}
	//qDebug() << "line to parse" << sLine;
	if (m_fRarVer < 5) {
		isSymLink = (slLine[PERM].at(0) == 'l');
		isDir     = (slLine[PERM].at(0) == 'd' || slLine[PERM].at(1) == 'D'); // Unix or Win dir
		sDateTime =  slLine[DATE] +" "+ slLine[TIME];
		sSize = slLine[SIZE];
		sPerm = slLine[PERM];
	}
	else { // ver.5.x
		isSymLink = (slLine[PERMv5].at(0) == 'l');
		isDir     = (slLine[PERMv5].at(0) == 'd' || slLine[PERMv5].at(1) == 'D'); // Unix or Win dir
		sDateTime =  slLine[DATEv5] +" "+ slLine[TIMEv5];
		sSize = slLine[SIZEv5];
		sPerm = slLine[PERMv5];
	}
	sFileName = QString::fromLocal8Bit(sFileName.toLatin1().data());
	sMime = MimeType::getFileMime(sFileName, isDir, isSymLink);
	//sSymLinkTarget = "";
	pFileInfo = new FileInfo(sFileName, sFullPath, ::atoll(sSize.toLatin1()), isDir, false, // isSymLink (workaround to correct show size)
					QDateTime::fromString(sDateTime, "dd-MM-yy HH:mm").addYears(100), sPerm, "?", "?", sMime);//, sSymLinkTarget);

	m_nFileCounter += (! isDir) ? 1 : 0;
	m_nDirCounter  += (isDir && !isSymLink) ? 1 : 0;

	if (isFileMatches(pFileInfo)) {
		m_processedFiles->append(pFileInfo);
		m_nMatchedFiles++;
		emit signalStatusChanged(Vfs::FOUND, m_sRequestingPanel);
		m_status = FINDING;
	}
	else
		delete pFileInfo;
}


} // namespace Vfs
