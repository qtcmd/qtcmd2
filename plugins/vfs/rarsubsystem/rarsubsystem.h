/***************************************************************************
 *   Copyright (C) 2011 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef RARSUBSYSTEM_H
#define RARSUBSYSTEM_H

#include <QDebug>
#include <QTimer>
#include <QProcess>

#include "subsystem.h" // includes also: QFile QVector QDateTime QStringList QUrlInfo
#include "plugindata.h"
#include "contextmenu.h"


namespace Vfs {

/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class RarSubsystem : public Subsystem
{
	Q_OBJECT
public:
	RarSubsystem();
	virtual ~RarSubsystem();

	SubsystemError   error() const { return m_error; }
	SubsystemStatus  status() const { return m_status; };
	SubsystemCommand currentCommand() const { return m_currentCommand; }

	void init( QWidget *pParent, bool bInvokeDetection=false );

	FileInfo     *root( bool bEmptyName=false );
	FileInfoList  listDirectory() const;
	void          getListBuffer( QString &sListBuffer ) { sListBuffer = m_sOutBuffer; }

	/** Returns 'quit subsystem by performing cd up on top level' status.
	 * @return TRUE if can get out, otherwise FALSE.
	 */
	bool          canUpThanRootDir() const { return true; }

	/** Returns change directory status.
	 * @param sAbsPath absolute path to directory
	 * @return TRUE if directory can change, otherwise FALSE.
	 */
	bool          canChangeDirTo( const QString &sAbsPath ) { return true; }

	/** Calls command opening archive and listnig on level 0.
	 * @param uri uri to open
	 */
	void          open( const URI &uri, const QString &sRequestingPanel, bool bCompleter=false );
	void          close( bool bForce ) { qDebug() << "RarSubsystem::close."; m_bForceReopen = true; } // disconnecting not supported

	void          openWith( FileInfoToRowMap *selectionMap, const URI &uri, const QString &sRequestingPanel );
	void          readFileToViewer( const QString &sHost, Vfs::FileInfo *pFileInfo, QByteArray &baBuffer, int &nReadBlockSize, int nReadingMode, const QString &sRequestingPanel );

	void          weigh( FileInfoToRowMap *pSelectionMap, const URI &uri, const QString &sRequestingPanel, bool bWeighingAsSubCmd=true, bool bResetCounters=true );
	void          weighArchive( const QString &sAbsFileName, const QString &sRequestingPanel, bool bRunNextOperation=true );
	void          weighItem( FileInfo *pFileInfo, const URI &uri, const QString &sRequestingPanel, bool bResetCounters );
	void          getWeighingResult( qint64 &weight, qint64 &realWeight, uint &files, uint &dirs );

	void          makeDir( const QString &sParentDirPath, const QString &sInNewDirsList, const QString &sRequestingPanel ) { notSupportedOp(sRequestingPanel); }
	void          makeFile( const QString &sParentDirPath, const QString &sInFileNameList, const QString &sRequestingPanel ) { notSupportedOp(sRequestingPanel); }
	void          rename( FileInfo *fileInfoOldName, const QString &sNewName, const QString &sRequestingPanel ) { notSupportedOp(sRequestingPanel); }
	void          remove( FileInfoToRowMap *pSelectionMap, const URI &uri, bool bWeighBefore, const QString &sRequestingPanel, bool bForceRemoveAll=false );

	void          searchFiles( FileInfoToRowMap *pSelectionMap, const URI &uri, const FindCriterion &fcFindCriterion, const QString &sRequestingPanel );
	QString       searchingResult();

	void          addTo( SubsystemCommand subsystemCommand, FileInfoToRowMap *pSelectionMap, const URI &uri, const QString &sRequestingPanel );
	void          extractTo( WhatExtract whatExtract, bool bMove, FileInfoToRowMap *pSelectionMap, const QString &sTargetPath, const QString &sRequestingPanel );
	void          copyFiles( FileInfoToRowMap *pSelectionMap, const QString &sTargetPath, bool bMove, bool bWeighBefore, const QString &sRequestingPanel );

	URI           currentURI() const { return m_bCompleterMode ? m_currentUriCompleter : m_currentUri; }

	FileInfo     *fileInfo( const QString &sAbsFileName );

// 	void          executeProgramm( const QString &sAbsFileName ) const {} // not supported in archive

	PluginData   *pluginData() { return m_pPluginData; }

	FileInfoList *processedFiles() { return m_processedFiles; }
	FileInfoList *postMovedFiles() { return m_preProcessedFiles; }
	FileInfo     *processedFileInfo() const { return m_processedFileInfo; }
	QString       processedFileName() const { return m_sProcessedFileName; }
	bool          operationFinished() const { return m_bOperationFinished; }

	void          updateContextMenu( ContextMenu *pContextMenu, KeyShortcuts *pKS=nullptr );

	void          setWorkingDirectoryPath( const QString &sDirectoryPath ) { m_sTmpDir = sDirectoryPath; }

	bool          handledOperation( SubsystemCommand sc );

	void          setBreakOperation();
	void          setPauseOperation( bool bPause );

private:
	SubsystemError    m_error;
	SubsystemStatus   m_status, m_pausedStatus;
	SubsystemCommand  m_currentCommand;

	enum Columns_v4 { SIZE=0, PACK, RATIO, DATE, TIME, PERM, CRC, METH, VER };
	enum Columns_v5 { PERMv5=0, SIZEv5, PACKv5, RATIOv5, DATEv5, TIMEv5, CRCv5, NAMEv5 };

	ContextMenu      *m_pContextMenu;
	QWidget          *m_pParent;


	void          notSupportedOp( const QString &sRequestingPanel );

	/** Parses output of command listing files.
	 */
	void          parsingBufferedFilesList();

	/** Adds files to the FileInfoList list from given level and matches to given directory name.
	 */
	void          processingParsedFilesList( unsigned int level, const QString &sInpPath );

	/** Performs command list on parsed and buffered archive
	 */
	void          commandList();

	bool          archiveOpened( const URI &uri ) { return (uri.host() == m_sHost && m_status == LISTED); }

	void          processingOpenWithCmd();

	void          initWeighing( bool bResetCounters, const QString &sRequestingPanel );
	void          processingWeighCmd( FileInfo *pFileInfo );

	void          initRemoving( const QString &sRequestingPanel );
	void          processingRemoveCmd();

	void          initSearching();
	void          startSearching( FileInfoToRowMap *pSelectionMap, const URI &uri );
	bool          isFileMatches( FileInfo *pFileInfo );
	void          searchingFinished();

	void          initArchiving();
	void          initExtracting();
	void          processingArchiveCmd();
	void          processingExtractCmd();

	void          initQueueForPreCheck( const QString &sRequestingPanel );

	void          updateParsingBuffer( SubsystemCommand subsysCmd );

	void          createNewArchive( const QString &sInArchiveName, FileInfoToRowMap *pSelectionMap, const QString &sRequestingPanel );

	QStringList    m_slParsingBufferLine;
	FileInfoList   m_fiList;

	QProcess      *m_pProcess;
	QString        m_sOutBuffer, m_sErrBuffer;
	QString        m_sCurrentPath;
	QString        m_sHost, m_sPath;
	QString        m_sLastShellCmd;
	URI            m_currentUri, m_currentUriCompleter;

	QVector < Operation > m_opQueue;
	bool           m_bActionIsFinished;
	bool           m_bOpenedArchive;
	unsigned int   m_nLevelInArch, m_nLevelInArchCompl;
	int            m_nFilesNumInArch;

	PluginData    *m_pPluginData;

	QString        m_sRequestingPanel_open, m_sRequestingPanel;

	bool           m_bOperationFinished;
	FileInfoList  *m_preProcessedFiles; // original incomming files list (used in addTo, because in this fun.items located on m_processedFiles are modified)
	FileInfoList  *m_processedFiles;
	FileInfo      *m_processedFileInfo;
	QString        m_sProcessedFileName;
	QString        m_sTmpDir;

	uint           m_nDirCounter,   m_nFileCounter,   m_nSymlinksCounter;
	uint           m_nOpDirCounter, m_nOpFileCounter, m_nOpSymlinksCounter;
	long long      m_totalWeight, m_OpTotalWeight;
	uint           m_nTotalItems, m_nOpTotalFiles;

	QList <QRegExp> m_findRegExpLst;
	FindCriterion  m_findCriterion;
	QString        m_sSearchingResult;
	int            m_nMatchedFiles;

	QString        m_sCommand;
	float          m_fRarVer;
	int            m_nNameColId;

	bool           m_bCompleterMode;

	QTimer         m_searchingTimer;
	QString        m_sSearchHost;
	QString        m_sStartSearchPath;
	int            m_nSearchedLines;

	bool           m_bPausedOperation;
	bool           m_bWeighingArchive;
	WhatExtract    m_whatExtract;

	QString        m_sLastDirName; // used in ARCHIVE_FILES command
	QStringList    m_slAddedFilesFromSubdirectories; // used in ARCHIVE_FILES command, to update internal files list with files added from subdirectories

	bool           m_bForceReopen;
	bool           m_bCreateNewArchive;

private slots:
	void slotProcessFinished( int nExitCode, QProcess::ExitStatus exitStatus );
	void slotBreakProcess();

	void slotReadStandardOutput();
	void slotReadStandardError();
	void slotRunNextOperation();

	void slotStartProcess();
	void slotReadStandardOutputAsyn();

	void slotSearching();

};

} // namespace Vfs

#endif
