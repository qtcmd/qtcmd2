/***************************************************************************
 *   Copyright (C) 2011 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDir>
#include <QTimer>
#include <QFileInfo>

//#include "tarsubsystemsettings.h"
#include "contextmenucmdids.h"
#include "systeminfohelper.h"
#include "pluginfactory.h"
#include "tarsubsystem.h"
#include "messagebox.h"
#include "settings.h"
#include "mimetype.h"

#include "preview/viewer.h"


namespace Vfs {

TarSubsystem::TarSubsystem() : LISTING_COLS(5)
{
	qDebug() << "TarSubsystem::TarSubsystem.";
	//setObjectName("TarSubsystem");
	m_error  = NO_ERROR;
	m_status = UNCONNECTED;
	//TarSubsystemSettings *tarSubsystemSettings = new TarSubsystemSettings;

	m_pPluginData = new PluginData;
	m_pPluginData->name         = "tarsubsystem_plugin";
	m_pPluginData->version      = "0.4";
	m_pPluginData->description  = QObject::tr("Plugin handles TAR archives (tar.gz, tar.bz2, tar.lzma, tar.xz, tar, tar.lz, tar.Z).");
	m_pPluginData->plugin_class = "subsystem";
	m_pPluginData->mime_icon    = ""; // (used in configuration window, higher priority than own_icon)
	m_pPluginData->own_icon     = 0;  // QIcon pointer. (used in configuration window)
	m_pPluginData->mime_type    = "tarsubsystem"; // required only in starting moment. After that it will updated by detects tar related compressors
	m_pPluginData->protocol     = "file";
	m_pPluginData->finding_files_fun        = 0; // ptr.to fun
	m_pPluginData->finding_inside_files_fun = 0; // ptr.to fun
// 	m_pPluginData->popup_actions   = 0; // QActionGroup pointer  (tarSubsystemSettings->popupActions())
	m_pPluginData->toolBar      = 0;  // QToolBar pointer      (tarSubsystemSettings->toolBar())
	m_pPluginData->configPage   = 0;  // QWidget pointer       (tarSubsystemSettings->configPage())

// 	init(); // initialized globaly (in MainWindow)
	m_bForceReopen = false;
	m_bCreateNewArchive = false;
	m_bOperationFinished = false;
	m_processedFiles = new FileInfoList;
	m_preProcessedFiles = new FileInfoList;
	m_processedFileInfo = NULL;

	m_pProcess = new QProcess;
	connect(m_pProcess, &QProcess::readyReadStandardOutput, this, &TarSubsystem::slotReadStandardOutputAsyn);
	connect(m_pProcess, &QProcess::readyReadStandardError, this, &TarSubsystem::slotReadStandardError);
	connect<void(QProcess::*)(int, QProcess::ExitStatus)>(m_pProcess, &QProcess::finished,  this, &TarSubsystem::slotProcessFinished);

	connect(&m_searchingTimer, &QTimer::timeout, this, &TarSubsystem::slotSearching);
}

TarSubsystem::~TarSubsystem()
{
	qDebug() << "TarSubsystem::~TarSubsystem. DESTRUCTOR";

// 	delete m_pPluginData; // removed in PluginFactory::~PluginFactory

	if (m_pProcess && m_pProcess->state() == QProcess::Running) {
		m_pProcess->terminate();
		m_pProcess->waitForFinished(3000);
	}
	delete m_pProcess;
	delete m_processedFiles;
	delete m_preProcessedFiles;
}


void TarSubsystem::init( QWidget *pParent, bool bInvokeDetection )
{
	QString sFN = "TarSubsystem::init.";
	m_currentCommand = NO_COMMAND;
	m_pParent = pParent;

	m_bOpenedArchive   = false;
	m_bWeighingArchive = false;
	m_nLevelInArch = 0; m_nLevelInArchCompl = 0;

	qDebug() << sFN;

	if (bInvokeDetection) {
		qDebug() << sFN << "Start detection for command line applications..."; // if success then member m_pPluginData->file_ext will be properly updated
		m_pPluginData->mime_type = "";
		m_sRequestingPanel_open = "leftPanel_1:init";
		QString sCmdName;
		QStringList slBinaries;
		slBinaries << "tar" << "gzip" << "bzip2" << "lzma" << "xz" << "lzip"; // << compress
		foreach (sCmdName, slBinaries) {
			if (findExecutable(sCmdName).isEmpty())
				qDebug() << sFN << sCmdName << "not found in PATH.";
			else {
				qDebug() << sFN << sCmdName << "found in PATH.";
				if  (sCmdName == "compress")  m_pPluginData->mime_type += ":application/x-compress";
				else if (sCmdName == "tar")   m_pPluginData->mime_type += ":application/x-tar";
				else if (sCmdName == "gzip")  m_pPluginData->mime_type += ":application/x-compressed-tar:application/gzip";
				else if (sCmdName == "bzip2") m_pPluginData->mime_type += ":application/x-bzip-compressed-tar:application/x-bzip";
				else if (sCmdName == "lzma")  m_pPluginData->mime_type += ":application/x-lzma-compressed-tar:application/x-lzma";
				else if (sCmdName == "xz")    m_pPluginData->mime_type += ":application/x-xz-compressed-tar:application/x-xz";
				else if (sCmdName == "lzip")  m_pPluginData->mime_type += ":application/x-lzip";

				if (m_pPluginData->mime_type.at(0) == ':')
					m_pPluginData->mime_type.remove(0,1);
				if (sCmdName == "gzip")
					sCmdName = "gz";
				else if (sCmdName == "lzip")
					sCmdName = "lz";
				else if (sCmdName == "bzip2")
					sCmdName = "bz2";
				if (sCmdName != "tar") // tar needs to be first for next checking
					sCmdName.insert(0, ";tar.");

				m_pPluginData->file_ext += sCmdName;
			}
		}
		PluginFactory::instance()->updateMimeConfig("tarsubsystem", "tarsubsystem", m_pPluginData->mime_type);
	}
}

bool TarSubsystem::handledOperation( SubsystemCommand sc )
{
	if (sc == CREATE_LINK || sc == RENAME)
		return false;

	return true;
}

void TarSubsystem::notSupportedOp( const QString &sRequestingPanel )
{
	m_status = ERROR_OCCURED;  m_error = NOT_SUPPORTED;
	emit signalStatusChanged(m_status, sRequestingPanel);
}

FileInfo* TarSubsystem::root( bool bEmptyName )
{
	static QString fakeRoot = "/";
	QFileInfo fi(fakeRoot);

	return new FileInfo(bEmptyName ? "" : "..", fakeRoot, fi.size(), true, false, fi.lastModified(), "----------", fi.owner(), fi.group(), "folder/up");
}

QString TarSubsystem::getName( const QString &sLine ) const
{
	unsigned int colId = 0, id = 0;
	while ((colId++) < LISTING_COLS) {
		if (sLine[id] == ' ')
			while (sLine[id] == ' ') id++;
		if (sLine[id] != ' ')
			while (sLine[id] != ' ') id++;
	}
	QString sName = sLine.right(sLine.length()-id-1);

	return sName;
}

FileInfo *TarSubsystem::fileInfo( const QString &sAbsFileName )
{
	qDebug() << "TarSubsystem::fileInfo. sAbsFileDirName" << sAbsFileName;
	if (sAbsFileName.endsWith("/..") || m_slParsingBufferLine.isEmpty())
		return root();

	QString sFileDirName = QDir::cleanPath("/"+sAbsFileName);
	QString sFileDirNameTmp = sAbsFileName; //sFileDirName;
	if (sFileDirNameTmp.at(0) == '/') // need to corrent find
		sFileDirNameTmp.remove(0, 1);
	if (sFileDirNameTmp.isEmpty())
		sFileDirNameTmp = sAbsFileName.left(sAbsFileName.indexOf("/.."));
	if (sFileDirNameTmp == "/")
		return root(true); // empty name
	//qDebug() << "sFileDirNameTmp:" << sFileDirNameTmp+"/";
	QString sName, sLine;
	int slashesNum = 0;
	for (QStringList::const_iterator it = m_slParsingBufferLine.begin(); it != m_slParsingBufferLine.end(); ++it) {
		sLine = *it;
		sName = getName(sLine);
		slashesNum = sName.count('/');
		if (sName == sFileDirNameTmp+"/" || slashesNum == 1) {
			if (slashesNum == 1) { // top level dir
				if (sName.endsWith('/')) {
					break;
				}
				sName = sName.left(sName.indexOf('/'));
				return new FileInfo(
							sName, m_sCurrentPath, 0, true, false,
							QDateTime::fromString(QDateTime::currentDateTime().toString(), "MM-dd-yyyy HH:mm:SS"),
							"----------", "?", "?", "folder"
							);
			}
			break;
		}
	}
	//qDebug() << "sLine" << sLine << "sName" << sName;
	QStringList lineStrList = sLine.split(' ');
	QString     sDateTime   = lineStrList[DATE] +" "+ lineStrList[TIME];
	bool        isDir       = (sName.at(sName.length()-1) == '/');
	bool        isSymLink   = (lineStrList[PERM].at(0) == 'l');
	if (isSymLink)
		sName = sName.left(sName.indexOf("->")-1);
	sFileDirName = QString::fromLocal8Bit(sFileDirName.toLatin1().data());
	QString     sMime       = MimeType::getFileMime(sFileDirName, isDir, isSymLink);
	QString     sExtMime    = sMime +":"+ MimeType::getMimeIcon(sMime);
	sName        = QString::fromLocal8Bit(sName.toLatin1().data());
	FileInfo *item = new FileInfo(
			                 sName, m_sCurrentPath, atoll(lineStrList[SIZE].toLatin1()), isDir, isSymLink,
			                 QDateTime::fromString(sDateTime, "MM-dd-yyyy HH:mm:SS"), "----------", "?", "?", sExtMime);

	return item;
}


void TarSubsystem::open( const URI &uri, const QString &sRequestingPanel, bool bCompleter )
{
	m_bCompleterMode = bCompleter;
	QString sFN = "TarSubsystem::open.";
	if (m_bCompleterMode && m_currentUriCompleter.toString() == uri.toString()) {
		qDebug() << sFN << "Came path for already opened directory (QCompleter::splitPath issue).";
		return;
	}
	if (uri.host().isEmpty()) {
		qDebug() << sFN << "Cannot open archive due to empty host!";
		return;
	}
	if (m_pProcess->state() != QProcess::NotRunning) {
		qDebug() << sFN << "Process is already running (maybe in other tab was opened archive) or PROG_EXISTS not finished, currentCommand:" << toString(m_currentCommand);
		if (m_opQueue.first().command != VIEW) {
			if (m_currentCommand != PROG_EXISTS) {
				qDebug() << sFN << "  Adding OPEN command to queue.";
				m_opQueue << Operation(OPEN, uri, sRequestingPanel);
			}
		}
		return;
	}
	m_opQueue.clear();
	m_processedFiles->clear();

	qDebug() << sFN << "  host:" << uri.host() << "path:" << uri.path() << "last currentCommand:" << toString(m_currentCommand) << m_sRequestingPanel_open << "ForceReopen:" << m_bForceReopen << "RequestingPanel:" << sRequestingPanel;
// 	if (! archiveOpened(uri) || m_bForceReopen) {
// 		qDebug() << sFN << "Archive didn't buffer yet. Reopen current:" << uri.host();
		m_bOpenedArchive = false;
		m_currentCommand = (sRequestingPanel.contains("Viewer_")) ? VIEW : OPEN;
		m_bForceReopen = false;
		m_sRequestingPanel_open = sRequestingPanel;
// 	}
// 	else // when other action (FIND) was invoked previously then current command is different than LIST
// 		m_currentCommand = LIST;

	m_sHost = uri.host();
	m_sPath = uri.path();

	if (m_bCompleterMode)
		m_currentUriCompleter = uri;
	else
		m_currentUri = uri;

	m_bActionIsFinished = false;
	// below commented, because the most likely will be never called. The reason is m_currentCommand = OPEN; with commented condition placed above
// 	if (m_currentCommand == LIST) { // user navigates inside archive
// 		commandList();
// 		emit signalStatusChanged(m_status, m_sRequestingPanel_open);
// 		return;
// 	}

	if (! m_bOpenedArchive) {
		m_opQueue.clear();
		QString sMime = uri.mime();
// 		qDebug() << sFN << "Not OpenedArchive. sMime:" << sMime << "uri:" << uri.toString();
		if (! sMime.contains("-tar") && ! sMime.contains("x-lzip")) { // compressed file (not tar archive)
			m_nLevelInArch = 0;
			m_opQueue << Operation(PROG_EXISTS, "which "+compressApp(sMime), sRequestingPanel);
			m_opQueue << Operation(COPY, "cp -f \""+m_sHost+"\" "+m_sTmpDir, sRequestingPanel);
			m_opQueue << Operation(DECOMPRESS_TO_VIEW, compressApp(sMime) + " -df \""+m_sTmpDir+QFileInfo(m_sHost).fileName()+"\"", sRequestingPanel);
			slotRunNextOperation();
			return;
		}
		m_bCompleterMode ? m_nLevelInArch = 0 : m_nLevelInArchCompl = 0;
		m_opQueue << Operation(PROG_EXISTS, "which tar", sRequestingPanel);
		QString sFileMime = MimeType::getFileMime(uri.host());
		if (sFileMime == "application/x-compress")
			m_opQueue << Operation(m_currentCommand, "tar ztvf \""+m_sHost+"\"", sRequestingPanel); // compress is supported by gzip (add check gzip and other compresors exist)
		else
		if (sFileMime == "application/x-lzma-compressed-tar")
			m_opQueue << Operation(m_currentCommand, "tar --lzma -tvf \""+m_sHost+"\"", sRequestingPanel); // because lzma is not automagicaly handled by tar
		else
		if (sFileMime == "application/x-lzip")
			m_opQueue << Operation(m_currentCommand, "tar --lzip -tvf \""+m_sHost+"\"", sRequestingPanel); // because lzma is not automagicaly handled by tar
		else
			m_opQueue << Operation(m_currentCommand, "tar tvf \""+m_sHost+"\"", sRequestingPanel);

		slotRunNextOperation();
	}
	else {
		m_currentCommand = LIST;
		commandList();
	}
}

FileInfoList TarSubsystem::listDirectory() const
{
	qDebug() << "TarSubsystem::listDirectory. Path:" << m_sCurrentPath;

	//FileInfoList fiList;
	//fiList.append(root());
	//return fiList;

	return m_fiList;
}

void TarSubsystem::commandList()
{
	m_bOpenedArchive = true;
	//qDebug() << "--- prepare to LIST (0). m_sPath:" << m_sPath << "m_sCurrentPath:" << m_sCurrentPath;
	m_sCurrentPath = "/"+m_sPath+"/";
	//qDebug() << "before clean - m_sCurrentPath" << m_sCurrentPath;
	m_sCurrentPath = QDir::cleanPath(m_sCurrentPath);

	bool goUp = (m_sPath.endsWith("/..") || m_sPath.endsWith("/../"));
	if (m_bCompleterMode) {
		if (goUp)
			m_nLevelInArchCompl -= 2;
		else
			m_nLevelInArchCompl = m_sCurrentPath.count(QDir::separator()) - ((m_sCurrentPath == QDir::separator()) ? 1 : 0);
	}
	else {
		if (goUp)
			m_nLevelInArch -= 2;
		else
			m_nLevelInArch = m_sCurrentPath.count(QDir::separator()) - ((m_sCurrentPath == QDir::separator()) ? 1 : 0);
	}
	QString sCurrentPathTmp = m_sCurrentPath + (m_sCurrentPath.endsWith(QDir::separator()) ? "" : "/"); // need to correct cut a name inside of parsingBufferedFilesList
	if (m_sPath.count('/') == 2 && goUp) // top level of archive - remove ending slash
		sCurrentPathTmp.remove(sCurrentPathTmp.length()-1, 1);
	if (! sCurrentPathTmp.isEmpty() && sCurrentPathTmp.at(0) == QDir::separator())
		sCurrentPathTmp.remove(0,1);

	qDebug() << "TarSubsystem::commandList. m_sPath:" << m_sPath << "m_sCurrentPath:" << m_sCurrentPath << "sCurrentPathTmp:" << sCurrentPathTmp << "m_nLevelInArch:" << (m_bCompleterMode ? m_nLevelInArchCompl : m_nLevelInArch);

	uint lvlInArch = (m_bCompleterMode ? m_nLevelInArchCompl : m_nLevelInArch);
	processingParsedFilesList(lvlInArch, sCurrentPathTmp);
	if (m_status == ERROR_OCCURED)
		return;
	m_currentUri.setPath(m_sCurrentPath);
	m_error  = NO_ERROR;
	m_status = m_bCompleterMode ? LISTED_COMPL : LISTED;
}

void TarSubsystem::parsingBufferedFilesList()
{
	QString sFN = "TarSubsystem::parsingBufferedFilesList.";
	if (m_sOutBuffer.isEmpty()) {
		qDebug() << sFN << "INTERNAL_ERROR. Empty buffer: m_sOutBuffer! Exit.";
		m_status = ERROR_OCCURED;  m_error = INTERNAL_ERROR;
		emit signalStatusChanged(m_status, m_sRequestingPanel_open);
		m_opQueue.clear();
		return;
	}
	qDebug() << sFN;

	m_slParsingBufferLine = m_sOutBuffer.split('\n');
// 	qDebug() << sFN << "Buffer lines:" << m_sParsingBufferLine.count();
	m_slParsingBufferLine.pop_back(); // remove last empty line

	m_nFilesNumInArch = m_slParsingBufferLine.count();

	// buffer's test
/*	qDebug() << m_sOutBuffer;
	QString line;
	foreach (line, m_sParsingBufferLine) {
		qDebug() << line;
	}*/
}

void TarSubsystem::processingParsedFilesList( unsigned int level, const QString &sInpPath )
{
	QString sFN = "TarSubsystem::processingParsedFilesList.";
	if (m_slParsingBufferLine.isEmpty()) {
		qDebug() << sFN << "Empty buffer: m_slParsingBufferLine! Exit.";
		m_status = ERROR_OCCURED;  m_error = INTERNAL_ERROR;
		emit signalStatusChanged(m_status, m_sRequestingPanel_open);
		m_opQueue.clear();
		return;
	}
	bool isDir, isSymLink;
	unsigned int slashesNum;
	int slashId, nameLen, targetLinkId;
	QStringList lineStrLst, ownerAndGroupLst, sNameList;
	QString sL0Name, sLastName = "x";
	QString sMime, sLine, sName, sDateTime, sOwner, sGroup, sPath, sTargetSymLink;
	FileInfo *item;

	qDebug() << sFN << "Level:" << level << "dirName:" << sInpPath;// << "m_sParsingBufferLine.count:" << m_sParsingBufferLine.count();
	// -- insert top item
	m_fiList.clear();
	m_fiList.append(root());
	// prepare path to matching
	sPath = m_sCurrentPath.right(m_sCurrentPath.length()-1) + "/";
	//qDebug() << "sPath:" << sPath;
	int pathLen = sPath.length();

	foreach (sLine, m_slParsingBufferLine) {
		sName = getName(sLine);
		if (level > 0) {
			if (sName.startsWith(sPath)) {
				slashId = sName.indexOf('/', pathLen);
				if (slashId > 0) { // dir or symlink
					targetLinkId = sName.indexOf("->");
					if (targetLinkId > 0)
						sTargetSymLink = sName.right(sName.length()-targetLinkId-3);
					else
						sTargetSymLink = "";
					sName = sName.mid(pathLen, (slashId - pathLen)+1);
				}
				else { // file
					slashesNum = sName.count('/');
					if (level <= slashesNum) {
						nameLen = sName.length() - pathLen;
						sName = sName.mid(pathLen, nameLen);
						//qDebug() << "slashId:" << slashId << "oldName:" << sOldName << "name:" << sName << "id:" << pathLen << "len:" << nameLen;
					}
				}
				if (sName == sLastName)
					continue;
				sLastName = sName;
				if (sName.isEmpty())
					continue;
			}
			else
				continue;
		}
		else { // level == 0
			slashId = sName.indexOf('/');
			if (slashId > 0) {
				targetLinkId = sName.indexOf("->");
				if (targetLinkId > 0)
					sTargetSymLink = sName.right(sName.length()-targetLinkId-3);
				else
					sTargetSymLink = "";
				sName = sName.left(slashId+1);
			}
		}
		if (sName.endsWith('/')) {
			isDir = true;
			sName.remove(sName.length()-1, 1); // remove ending slash
		}
		else
			isDir = false;

		if (sNameList.indexOf(sName) != -1)
			continue;
		sNameList.append(sName);

		lineStrLst = sLine.split(QRegExp("\\s+"));

		isSymLink = (lineStrLst[PERM].at(0) == 'l');
		if (isSymLink) {
			sName = sName.left(sName.indexOf("->")-1);
			isDir = false;
		}
		if (m_bCompleterMode && ! isDir)
			continue;
		sName = QString::fromLocal8Bit( sName.toLatin1().data() );

		sDateTime = lineStrLst[DATE] +" "+ lineStrLst[TIME];
		ownerAndGroupLst = lineStrLst[OWNER_GROUP].split('/');
		sOwner = ownerAndGroupLst[0];
		sGroup = ownerAndGroupLst[1];
		sMime  = MimeType::getFileMime(sName, isDir, isSymLink);
		item = new FileInfo(
			sName, sInpPath, atoll(lineStrLst[SIZE].toLatin1()), isDir, isSymLink,
							QDateTime::fromString(sDateTime, Qt::ISODate), lineStrLst[PERM], sOwner, sGroup, sMime, sTargetSymLink
		);
		m_fiList.append(item);
	} // foreach

	m_bCompleterMode ? m_nLevelInArchCompl++ : m_nLevelInArch++;
}


void TarSubsystem::openWith( FileInfoToRowMap *selectionMap, const URI &uri, const QString &sRequestingPanel )
{
	int nResult = -1;
	QString sFN = "TarSubsystem::openWith.";
	qDebug() << sFN << "uri" << uri.toString() << "m_currentUri:" << m_currentUri.toString() << "absFileName:" << selectionMap->constBegin().key()->absoluteFileName();

	if (m_bConfirmExtractingForOpenWith) {
		int nSelFilesNum = selectionMap->count();
		QString sAbsFileName = m_currentUri.toString() + QDir::separator() + selectionMap->constBegin().key()->fileName();
		QString sQuestion = (nSelFilesNum > 1) ? tr("Extract and open files?") : tr("Do you want to extract and open given file?");
		QString sFImsg = (nSelFilesNum > 1) ? QString(tr("Selected %1 items")).arg(nSelFilesNum) : sAbsFileName;
		nResult = MessageBox::yesNo(m_pParent, tr("Open with..."), sFImsg, sQuestion,
									m_bConfirmExtractingForOpenWith,
									MessageBox::Yes, // default focused button
									tr("Always ask for confirmation")
		);
		if (nResult == 0) // user closed dialog
			return;
		Settings settings;
		settings.update("archive/ConfirmExtractingForOpenWith", m_bConfirmExtractingForOpenWith);
	}
	else
		nResult = MessageBox::Yes;

	if (nResult == MessageBox::Yes) {
		if (! archiveOpened(uri))
			m_sHost = uri.host();

		m_error = NO_ERROR;
		m_status = EXTRACTING;
		m_currentCommand = OPEN_WITH_CMD;
		m_bOperationFinished = false;
		m_sRequestingPanel_open = sRequestingPanel;
		m_processedFiles->clear();
		m_opQueue.clear();

		if (m_sTmpDir.isEmpty()) {
			qDebug() << sFN << "Internal ERROR. Working directory is not set!";
			m_error = CANNOT_EXTRACT;
			emit signalStatusChanged(Vfs::OPEN_WITH, sRequestingPanel);
			return;
		}

		long long nOpenedFilesWeigh = 0;
		QString sFullFileNamesToExtract, sFileNameNoHost, sFileName;
		FileInfo *fi;
		FileInfoToRowMap::const_iterator it = selectionMap->constBegin();
		while (it != selectionMap->constEnd()) {
			fi = it.key();
			sFileName = fi->absoluteFileName();
			sFileNameNoHost = sFileName.remove(m_sHost);
			if (sFileNameNoHost.startsWith('/'))
				sFileNameNoHost.remove(0, 1);
			sFullFileNamesToExtract += "\"" + sFileNameNoHost + "\"";
			m_processedFiles->append(fi);
			nOpenedFilesWeigh += fi->size();
			++it;
		}
		long long freeBytes, totalBytes;
		Vfs::getStatFS(m_sTmpDir, freeBytes, totalBytes);
		if (freeBytes < nOpenedFilesWeigh) { // m_nSourceFileSize is init.into 'initPutFile()'
			m_error = NO_FREE_SPACE;
			emit signalStatusChanged(Vfs::ERROR_OCCURED, sRequestingPanel);
			return;
		}
// 		tar -C /tmp/qtcmd2/ -x  "lxterminal-0.1.6/depcomp" "lxterminal-0.1.6/COPYING" -vf /home/piotr/test_archives/lxterminal-0.1.6.tar.lzma --strip-components=1
		QString sExtrSwitch = (MimeType::getFileMime(m_sHost) == "application/x-compress") ? " -zx " : " -x ";
		QString sCmd = "tar -C " + m_sTmpDir + sExtrSwitch + sFullFileNamesToExtract + " -vf " + "\"" + m_sHost + "\"" + " --strip-components=" + QString("%1").arg(sFileNameNoHost.count(QDir::separator()));
		m_opQueue << Operation(PROG_EXISTS, "which tar", sRequestingPanel);
		m_opQueue << Operation(m_currentCommand, sCmd, sRequestingPanel);
		slotRunNextOperation();
	}
	else {
		m_bOperationFinished = true;
		emit signalStatusChanged(Vfs::OPEN_WITH, sRequestingPanel);
	}
}

void TarSubsystem::readFileToViewer( const QString &sHost, FileInfo *pFileInfo, QByteArray &baBuffer, int &nReadBlockSize, int nReadingMode, const QString &sRequestingPanel )
{
	QString sFN = "TarSubsystem::readFileToViewer.";
	qDebug() << sFN << "host:" << sHost << "absFileName:" << pFileInfo->absoluteFileName() << " method:" << nReadingMode << "sRequestingPanel:" << sRequestingPanel;
	m_error = NO_ERROR;
	m_currentCommand = READ_FILE_TO_VIEW;

	// -- check if temporary directory is set
	if (m_sTmpDir.isEmpty()) {
		qDebug() << sFN << "Internal ERROR. Working directory is not set!";
		m_error = CANNOT_EXTRACT;
		emit signalStatusChanged(Vfs::OPEN_WITH, sRequestingPanel);
		return;
	}
	// -- check readability
	if (nReadingMode == Preview::Viewer::GET_READABILITY) { // (3) always readable, because file will be extracted to tmp directory
		nReadBlockSize = 1; // need for FileViewer check
		m_processedFileInfo = pFileInfo;
		emit signalStatusChanged((m_status=Vfs::READY_TO_READ), sRequestingPanel); // unmark item
		return;
	}
	long long nInputFileWeigh = pFileInfo->size();
	long long freeBytes, totalBytes;
	Vfs::getStatFS(m_sTmpDir, freeBytes, totalBytes);
	if (freeBytes < nInputFileWeigh) {
		m_error = NO_FREE_SPACE;
		emit signalStatusChanged(Vfs::ERROR_OCCURED, sRequestingPanel);
		return;
	}
	// -- prepare to extract file
	m_sHost = sHost;
	m_sRequestingPanel_open = sRequestingPanel;

	QString sFileName = pFileInfo->absoluteFileName();
	QString sFileNameNoHost = sFileName.remove(m_sHost);
	if (sFileNameNoHost.startsWith('/'))
		sFileNameNoHost.remove(0, 1);
	//qDebug() << "TarSubsystem::readFileToViewer.  --- sFileNameNoHost:" << sFileNameNoHost;
	m_processedFiles->clear();
	m_processedFiles->append(pFileInfo);

	// need to remove file before extract it due to extracting process might hang
	QString sFileToRemove = sFileNameNoHost;
	sFileToRemove.replace(sFileToRemove.left(sFileToRemove.lastIndexOf('/')+1), m_sTmpDir);
	QFile(sFileToRemove).remove();

	int nResult = -1;
	if (m_bConfirmExtractingForView) {
		QString sAbsFileName = m_sHost + QDir::separator() + sFileNameNoHost; // m_currentUri.toString() points to current dir in archive including host
		QString sQuestion = tr("Do you want to extract and open given file?");
		nResult = MessageBox::yesNo(m_pParent, tr("View file"), sAbsFileName, sQuestion,
									m_bConfirmExtractingForView,
									MessageBox::Yes, // default focused button
									tr("Always ask for confirmation")
		);
		if (nResult == 0) // user closed dialog
			return;
		Settings settings;
		settings.update("archive/ConfirmExtractingForView", m_bConfirmExtractingForView);
	}
	else
		nResult = MessageBox::Yes;

	sFileNameNoHost = "\"" + sFileNameNoHost + "\" ";
	m_bOperationFinished = false;
	m_status = EXTRACTING;
	m_opQueue.clear();
	if (nResult == MessageBox::Yes) {
		// tar -C /tmp/qtcmd2/ -x  "lxterminal-0.1.6/depcomp" "lxterminal-0.1.6/COPYING" -vf /home/piotr/test_archives/lxterminal-0.1.6.tar.lzma --strip-components=1
		// --strip-components - extract without path
		QString sExtrSwitch = (MimeType::getFileMime(m_sHost) == "application/x-compress") ? " -zx " : " -x ";
		QString sCmd= "tar -C " + m_sTmpDir + sExtrSwitch + sFileNameNoHost + " -vf " + "\"" + m_sHost + "\"" + " --strip-components=" + QString("%1").arg(sFileNameNoHost.count(QDir::separator()));
		m_opQueue << Operation(PROG_EXISTS, "which tar", sRequestingPanel);
		m_opQueue << Operation(m_currentCommand, sCmd, sRequestingPanel);
		slotRunNextOperation();
	}
	else {
		m_bOperationFinished = true;
		emit signalStatusChanged(Vfs::NOT_READY_TO_READ, sRequestingPanel);
	}
}

void TarSubsystem::processingOpenWithCmd()
{
	m_status = OPEN_WITH;
	m_error  = NO_ERROR;
	QStringList sParsingBufferLine(m_sOutBuffer.split('\n'));
	sParsingBufferLine.pop_back(); // remove last empty line
// 	qDebug() << "sParsingBufferLine:\n" << sParsingBufferLine;
	int nFilesNumInArch = sParsingBufferLine.count();
	m_bOperationFinished = (nFilesNumInArch == m_processedFiles->count());
	qDebug() << "TarSubsystem::processingOpenWithCmd. filesNumInArch:" << nFilesNumInArch << "operationFinishedStatus:" << m_bOperationFinished;
}


void TarSubsystem::initWeighing( bool bResetCounters, const QString &sRequestingPanel )
{
	qDebug() << "TarSubsystem::initWeighing. currentCommand:" << toString(m_currentCommand);
	if (m_currentCommand != WEIGH_ARCH)
		m_processedFiles->clear();
	m_totalWeight = 0;
	m_nTotalItems = 0;
	if (bResetCounters) {
		m_nDirCounter = 0;
		m_nFileCounter = 0;
		m_nSymlinksCounter = 0;
	}
	m_error  = NO_ERROR;
	m_status = WEIGHING;
	m_bCreateNewArchive = false;
	m_bOperationFinished = false;
	emit signalStatusChanged(m_status, sRequestingPanel);
}

void TarSubsystem::weigh( FileInfoToRowMap *selectionMap, const URI &uri, const QString &sRequestingPanel, bool bWeighingAsSubCmd, bool bResetCounters )
{
	// -- check if opened archive is cached
	if (! archiveOpened(uri) && ! bWeighingAsSubCmd) {
		qDebug() << "TarSubsystem::weigh. Archive didn't buffer yet. Need to reopen.";
		open(uri, sRequestingPanel);
		m_opQueue.clear();
		m_opQueue << Operation(WEIGH, selectionMap, uri, sRequestingPanel, bResetCounters);
		slotRunNextOperation();
		return;
	}
	m_sRequestingPanel = sRequestingPanel;
	initWeighing(bResetCounters, sRequestingPanel);

	FileInfoToRowMap::const_iterator it = selectionMap->constBegin();
	while (it != selectionMap->constEnd()) {
		m_processedFiles->append(it.key());
		processingWeighCmd(it.key());
		++it;
	}

// 	if (! bWeighingAsSubCmd) { // commented due to: properties dialog needs this signal
		m_bOperationFinished = true;
		m_status = WEIGHED;
		emit signalStatusChanged(m_status, sRequestingPanel);
// 	}
}

void TarSubsystem::weighArchive( const QString &sAbsFileName, const QString &sRequestingPanel, bool bRunNextOperation )
{
	qDebug() << "TarSubsystem::weighArchive." << sRequestingPanel << m_sRequestingPanel;
	initWeighing(true, m_sRequestingPanel);

	m_nLevelInArch = 0;
	if (MimeType::getFileMime(sAbsFileName) == "application/x-compress")
		m_opQueue.prepend(Operation(OPEN, "tar ztvf \""+sAbsFileName+"\"", sRequestingPanel)); // compress is supported by gzip (add check gzip and other compresors exist)
	else
		m_opQueue.prepend(Operation(OPEN, "tar tvf \""+sAbsFileName+"\"", sRequestingPanel));
	m_opQueue.prepend(Operation(PROG_EXISTS, "which tar", sRequestingPanel));

	if (bRunNextOperation)
		slotRunNextOperation();
}

void TarSubsystem::weighItem( FileInfo *pFileInfo, const URI &uri, const QString &sRequestingPanel, bool bResetCounters )
{
	if (uri.host().isEmpty()) { // the most likely archive file
		m_opQueue.clear();
		m_opQueue << Operation(WEIGH_ARCH, pFileInfo, "", sRequestingPanel); // thanks that flag m_bWeighingArchive will be properly set
		slotRunNextOperation();
		return;
	}
	// -- check if opened archive is cached
	if (! archiveOpened(uri)) {
		qDebug() << "TarSubsystem::weighItem. Archive didn't buffer yet. Need to reopen.";
		open(uri, sRequestingPanel);
		m_opQueue << Operation(WEIGH_ITEM, pFileInfo, uri, sRequestingPanel, bResetCounters);
		slotRunNextOperation();
		return;
	}
	initWeighing(bResetCounters, sRequestingPanel);

	m_processedFiles->append(pFileInfo);
	processingWeighCmd(pFileInfo);

	m_bOperationFinished = true;
	m_status = WEIGHED;
	emit signalStatusChanged(m_status, sRequestingPanel);
}

void TarSubsystem::processingWeighCmd( FileInfo *pFileInfo )
{
	if (pFileInfo == NULL && ! m_bWeighingArchive) {
		qDebug() << "TarSubsystem::processingWeighCmd. empty fileInfo";
		return;
	}
	bool isDir = false;
	bool isSymLink;
	unsigned int level = m_nLevelInArch;
	int slashId;
	QStringList lineStrLst, sNameList;
	QString sL0Name, sLastName = "x";
	QString sLine, sName;
	QString sPath;
// 	QString sPath = pFileInfo->absoluteFileName();
	if (! m_bWeighingArchive)
		sPath = pFileInfo->absoluteFileName();
	if (sPath.startsWith(QDir::separator()))
		sPath.remove(0, 1);
	const bool bInputIsDir = (! m_bWeighingArchive) ? pFileInfo->isDir() : false;

	if (bInputIsDir) {
		sPath += QDir::separator();
		qDebug() << "TarSubsystem::processingWeighCmd. Path:" << sPath << "level:" << level;
	}

	foreach (sLine, m_slParsingBufferLine) {
		sName = getName(sLine);
		if (level > 0) {
			if (bInputIsDir) {
				if (sName.startsWith(sPath)) {
					sName.remove(0, sPath.length());
					if (sName == sLastName)
						continue;
					sLastName = sName;
					if (sName.isEmpty())
						continue;
// 					qDebug() << "name" << sName;
				}
				else
					continue;
			}
			else
			if (sName.endsWith('/') || sName != sPath)
				continue;
		}
		else { // level == 0 (optimalisation: get first item only because all lines have the same dir on this level)
			slashId = sName.indexOf('/');
			if (slashId > 0) {
				if (! m_bWeighingArchive)
					sName = sName.left(slashId+1);
				if (bInputIsDir || m_bWeighingArchive) {
					if (sName.endsWith('/')) {
						isDir = true;
						sName.remove(sName.length()-1, 1); // remove ending slash
					}
					else
						isDir = false;
				}

				lineStrLst = sLine.split(QRegExp("\\s+"));
				isSymLink = (lineStrLst[PERM].at(0) == 'l');

				if (isSymLink)
					m_nSymlinksCounter++;
				if (! isDir) {
					m_totalWeight += atoll(lineStrLst[SIZE].toLatin1());
					m_nFileCounter++;
				}
				else
					m_nDirCounter++;

				if (m_bWeighingArchive)
					continue;
				else
					break;
			}
		}
// 		qDebug() << "name" << sName;
		if (! m_bWeighingArchive) {
			if (bInputIsDir) {
				if (sName.endsWith('/')) {
					isDir = true;
					sName.remove(sName.length()-1, 1); // remove ending slash
				}
				else
					isDir = false;
			}
		}

		if (sNameList.indexOf(sName) != -1)
			continue;
		sNameList.append(sName);

		lineStrLst = sLine.split(QRegExp("\\s+"));
		isSymLink = (lineStrLst[PERM].at(0) == 'l');

		if (isSymLink)
			m_nSymlinksCounter++;
		if (! isDir) {
			m_totalWeight += atoll(lineStrLst[SIZE].toLatin1());
			m_nFileCounter++;
		}
		else
			m_nDirCounter++;

		if (! m_bWeighingArchive) {
			if (! bInputIsDir)
				break;
		}
	} // foreach

	if (! m_bWeighingArchive) {
		if (bInputIsDir)
			m_nDirCounter++; // main dir
	}

	m_nTotalItems = (m_nFileCounter + m_nDirCounter); // skip m_nSymlinksCounter because were counted as files
}

void TarSubsystem::getWeighingResult( qint64 &weight, qint64 &realWeight, uint &files, uint &dirs )
{
	weight = m_totalWeight;
	realWeight = -1;
	files  = m_nFileCounter;
	dirs   = m_nDirCounter;
}


void TarSubsystem::initRemoving( const QString &sRequestingPanel )
{
	if (m_status == WEIGHED) {
		qDebug() << "TarSubsystem::initRemoving(). WEIGHED - totalWeigh:" << m_totalWeight << FileInfo::bytesRound(m_totalWeight) << "files:" << m_nFileCounter << "dirs:" << m_nDirCounter << "all items:" << m_nTotalItems;
	}
	m_opQueue.clear();
	m_processedFiles->clear();
	m_OpTotalWeight  = 0;
	m_nOpDirCounter  = 0;
	m_nOpFileCounter = 0;
	m_nOpSymlinksCounter = 0;
	m_currentCommand = REMOVE;
	m_status = REMOVING;
	m_bCreateNewArchive = false;
	m_sRequestingPanel = sRequestingPanel; // used in slotProcessFinished
	emit signalStatusChanged(m_status, sRequestingPanel);
}

void TarSubsystem::remove( FileInfoToRowMap *selectionMap, const URI &uri, bool weighBefore, const QString &sRequestingPanel, bool bForceRemoveAll )
{
	QString sFN = "TarSubsystem::remove.";
	if (selectionMap == NULL) {
		qDebug() << sFN << "Empty selectionMap.";
		m_status = ERROR_OCCURED;  m_error = INTERNAL_ERROR;
		emit signalStatusChanged(m_status, sRequestingPanel);
		return;
	}
	int selectedItemsNum = selectionMap->size();
	qDebug() << sFN << "Selected items:" << selectedItemsNum << "weighBefore:" << weighBefore;
	if (selectedItemsNum == 0) {
		qDebug() << sFN << "No selected items.";
		return;
	}
	QString sMime = MimeType::getMimeFromPath(uri.host());
	bool bCompressedTarArch = (sMime != "application/x-tar");
	Settings settings;
	if (bCompressedTarArch) {
		bool bTarAlwaysDecompressIfUpdateArchive = settings.value("archive/TarAlwaysDecompressIfUpdateArchive", false).toBool();;
		if (! bTarAlwaysDecompressIfUpdateArchive) {
			if (QFileInfo(uri.host()).exists()) {
				int nResult = MessageBox::yesNo(m_pParent, tr("Removing from archive ..."), uri.host(),
												"\n"+tr("Compressed tar archive. Its update requires decompress")+"\n"+tr("and compress archive again.")+"\n"+tr("Originally used compression level will be lost.")+
												"\n"+tr("Do you want to continue?"),
												bTarAlwaysDecompressIfUpdateArchive,
									MessageBox::Yes, // default focused button
									tr("Do not ask again")
				);
				settings.update("archive/TarAlwaysDecompressIfUpdateArchive", bTarAlwaysDecompressIfUpdateArchive);
				if (nResult == MessageBox::No)
					return;
			}
		}
	}

	// -- start remove operation
	initWeighing(true, m_sRequestingPanel);
	if (weighBefore) {
		weigh(selectionMap, uri, sRequestingPanel);
		emit signalCounterProgress(m_nDirCounter,  DIR_COUNTER, true);
		emit signalCounterProgress(m_nFileCounter, FILE_COUNTER, true);
		emit signalCounterProgress(m_totalWeight,  WEIGH_COUNTER, true);
	}

	initRemoving(sRequestingPanel);

	QString sFullFileNamesToRemove, sFileName;
	FileInfo *pFileInfo;
	FileInfoToRowMap::const_iterator it = selectionMap->constBegin();
	while (it != selectionMap->constEnd()) {
		pFileInfo = it.key();
		sFileName = pFileInfo->absoluteFileName();
		if (sFileName.startsWith(QDir::separator()))
			sFileName.remove(0, 1);
		sFullFileNamesToRemove += "\"" + sFileName + "\" ";
		m_processedFiles->append(pFileInfo);
		++it;
	}
	if (bCompressedTarArch) {
		if (! decompress(m_currentUri, sRequestingPanel))
			return;
	}
	QString sArchiveName;
	if (bCompressedTarArch)
		sArchiveName = QFileInfo(uri.host()).path()+"/"+QFileInfo(uri.host()).completeBaseName();
	else
		sArchiveName = uri.host();
	// 	tar vf lxterminal-0.1.6.tar --delete lxterminal-0.1.6/man/*   (dir)
	// 	tar vf lxterminal-0.1.6.tar --delete lxterminal-0.1.6/INSTALL (file)
	QString sRmCmd = "tar vf \"" + sArchiveName + "\" --delete " + sFullFileNamesToRemove;
	m_opQueue << Operation(m_currentCommand, sRmCmd, sRequestingPanel);
	if (bCompressedTarArch) {
		if (! compress(m_currentUri, sRequestingPanel, true)) // cannot restore level of compression, so will be used default; true for skip showing confirmation message if file exists
			return;
	}
	slotRunNextOperation();
}

void TarSubsystem::processingRemoveCmd()
{
	qDebug() << "TarSubsystem::processingRemoveCmd. Unfortunately tar doesn't print deleted files!"; // despite of use the "v" option in command
	emit signalNameOfProcessedFile(tr("tar doesn't print deleted files!"));
	m_OpTotalWeight  = m_totalWeight;
	m_nOpFileCounter = m_nFileCounter;
	m_nOpDirCounter  = m_nDirCounter;
	m_nOpSymlinksCounter = m_nSymlinksCounter;
	emit signalCounterProgress(m_nOpFileCounter, FILE_COUNTER, false);
	emit signalCounterProgress(m_OpTotalWeight,  WEIGH_COUNTER, false);
	emit signalCounterProgress(m_nOpDirCounter,  DIR_COUNTER, false);

	m_nOpTotalFiles = m_nOpFileCounter + m_nOpDirCounter + m_nOpSymlinksCounter;
	if (m_nTotalItems > 0)
		emit signalTotalProgress((100 * m_nOpTotalFiles) / m_nTotalItems, m_totalWeight);
}

void TarSubsystem::processingArchiveCmd()
{
// 	qDebug() << "TarSubsystem::processingArchiveCmd.\n" << m_sOutBuffer;
	uint nProcessedFiles;
	QStringList slLinesBuffer = m_sOutBuffer.split('\n', Qt::SkipEmptyParts);
	if (slLinesBuffer.size() == 0)
		return;

	bool bIsDir;
	QString sFileName, sInpLine;

	for (int i=0; i<slLinesBuffer.count(); i++) {
		sInpLine = slLinesBuffer.at(i);
		//sFileName = sInpLine.right(sInpLine.length() - sInpLine.indexOf(" ") - 1); // commented, because it broke names containing spaces
		sFileName = sInpLine;
		bIsDir = (sFileName.endsWith('/'));
		if (bIsDir)
			sFileName = sFileName.left(sFileName.length()-1);
		sFileName = sFileName.right(sFileName.length() - sFileName.lastIndexOf('/') - 1);
		if (! m_sLastDirName.isEmpty() && ! m_bCreateNewArchive)
			sFileName.insert(0, m_sLastDirName +"/");
		if (bIsDir) {
			if (m_sLastDirName != sFileName) {
				emit signalCounterProgress(++m_nOpDirCounter, DIR_COUNTER, false);
				m_sLastDirName = sFileName;
			}
		}
		else {
			if (! sFileName.startsWith(m_sLastDirName +"/"))
				emit signalCounterProgress(++m_nOpFileCounter, FILE_COUNTER, false);
			else {
				if (! m_bCreateNewArchive)
					m_slAddedFilesFromSubdirectories.append(sInpLine+"|"+sFileName);
			}
		}
		emit signalNameOfProcessedFile(sInpLine);
		nProcessedFiles = m_nOpFileCounter + m_nOpDirCounter;
		if (! sFileName.startsWith(m_sLastDirName +"/")) {
			m_nOpTotalFiles = m_nOpFileCounter + m_nOpDirCounter; // + m_nOpSymlinksCounter;
			if (m_nTotalItems > 0) {
				emit signalTotalProgress((100 * m_nOpTotalFiles) / m_nTotalItems, m_totalWeight);
				emit signalDataTransferProgress(nProcessedFiles, m_nTotalItems, 0, false); // last two args are not relevant
			}
		}
	}
}

void TarSubsystem::processingExtractCmd()
{
// 	qDebug() << "TarSubsystem::processingExtractCmd.\n" << m_sOutBuffer;
	QStringList slLinesBuffer = m_sOutBuffer.split('\n', Qt::SkipEmptyParts);
	if (slLinesBuffer.size() == 0)
		return;

	int nProcessedFiles;
	QString sFileName;

	if (m_whatExtract == Vfs::EXTRACT_ARCHIVE) {
		bool bIsDir;
		FileInfo *pFileInfo;
		QString sAbsCurrentPath = QFileInfo(m_sLastShellCmd.split('"').at(3)).path();

		for (int i=0; i<slLinesBuffer.count(); i++) {
			sFileName = slLinesBuffer.at(i);
			bIsDir = sFileName.endsWith('/');
			if (m_processedFiles->size() > 1) // extrating more than 1 archive
				emit signalDataTransferProgress(++m_nOpFileCounter, m_nTotalItems, 0, false); // last two args are not relevant
//			else // comment was required to correctly extract multiply archives into current directory ("Extract here" operation)
			{ // extrating only 1 archive - need to process all files
				if (m_processedFiles->size() == 1) {
					emit signalNameOfProcessedFile(sFileName);
					if (bIsDir)
						emit signalCounterProgress(++m_nOpDirCounter, DIR_COUNTER, false);
					else
						emit signalCounterProgress(++m_nOpFileCounter, FILE_COUNTER, false);
					nProcessedFiles = m_nOpFileCounter + m_nOpDirCounter;
					emit signalDataTransferProgress(nProcessedFiles, m_nTotalItems, 0, false); // last two args are not relevant
					emit signalTotalProgress((100 * nProcessedFiles) / m_nTotalItems, m_totalWeight);
				}
			}
			// -- helpful in the update data model after "Extract here" operation
			if ((bIsDir && sFileName.count('/') == 1) || ! sFileName.contains('/')) { // directories and files from top level
				if (bIsDir)
					sFileName = sFileName.left(sFileName.length()-1);
				QFileInfo fi(sAbsCurrentPath+"/"+sFileName);
				if (fi.exists()) {
					QString sPerm = permissionsAsString(fi.absoluteFilePath());
					QString sMime = MimeType::getFileMime(fi.absoluteFilePath(), fi.isDir(), fi.isSymLink());
					qint64  nSize = fi.isSymLink() ? fi.symLinkTarget().length() : fi.size();
					QString sSymLinkTarget = fi.symLinkTarget();
					pFileInfo = new FileInfo(sFileName, fi.path(), nSize, fi.isDir(), fi.isSymLink(), fi.lastModified(), sPerm, fi.owner(), fi.group(), sMime, sSymLinkTarget);
					m_preProcessedFiles->append(pFileInfo);
				}
			}
		}
	}
	else { // Vfs::EXTRACT_FILES
		bool isDir;
		FileInfo *pFI;
		for (int i=0; i<slLinesBuffer.count(); i++) {
			sFileName = slLinesBuffer.at(i);
			foreach (pFI, *m_processedFiles) {
				if (sFileName.indexOf(pFI->fileName()) != -1) {
					isDir = pFI->isDir() && ! pFI->isSymLink();
					break;
				}
			}
			emit signalNameOfProcessedFile(sFileName);
			if (isDir || sFileName.endsWith('/')) {
				if (m_sLastDirName != pFI->fileName()) {
					emit signalCounterProgress(++m_nOpDirCounter, DIR_COUNTER, false);
					m_sLastDirName = pFI->fileName();
					if (! sFileName.endsWith('/'))
						emit signalCounterProgress(++m_nOpFileCounter, FILE_COUNTER, false);
				}
				else // the most likely file from extracted directory
					emit signalCounterProgress(++m_nOpFileCounter, FILE_COUNTER, false);
			}
			else
				emit signalCounterProgress(++m_nOpFileCounter, FILE_COUNTER, false);
		}
		nProcessedFiles = m_nOpFileCounter + m_nOpDirCounter;
		emit signalDataTransferProgress(nProcessedFiles, m_nTotalItems, 0, false); // last two args are not relevant
		emit signalTotalProgress((100 * nProcessedFiles) / m_nTotalItems, m_totalWeight);
	}
}


void TarSubsystem::initSearching()
{
	m_processedFiles->clear();
	m_nMatchedFiles = 0;
	m_nFileCounter = 0;
	m_nDirCounter = 0;
	m_status = FINDING;
	m_error  = NO_ERROR;
	m_currentCommand = FIND;
	m_sSearchingResult = "";
	m_bOperationFinished = false;
	m_bCreateNewArchive = false;
	m_findRegExpLst.clear();
	emit signalStatusChanged(m_status, m_sRequestingPanel); // update status bar
}

void TarSubsystem::searchFiles( FileInfoToRowMap *pSelectionMap, const URI &uri, const FindCriterion &fcFindCriterion, const QString &sRequestingPanel )
{
	if (pSelectionMap == NULL) {
		qDebug() << "TarSubsystem::searchFiles. Empty selectionMap.";
		return;
	}
	if (fcFindCriterion.isEmpty()) {
		qDebug() << "TarSubsystem::searchFiles. Empty findCriterion.";
		return;
	}
	// -- check if opened archive is cached
	if (! archiveOpened(uri)) {
		qDebug() << "TarSubsystem::searchFiles. Archive didn't cache yet. Need to reopen.";
		open(uri, sRequestingPanel);
		m_opQueue << Operation(FIND, NULL, "", sRequestingPanel);
		slotRunNextOperation();
		return;
	}

	qDebug() << "TarSubsystem::searchFiles";
	m_findCriterion = fcFindCriterion;
	m_sRequestingPanel = sRequestingPanel;

	initSearching();
	QStringList slNameLst = m_findCriterion.nameLst.split('|');
	foreach (QString sName, slNameLst)
		m_findRegExpLst.append(QRegExp(sName, m_findCriterion.caseSensitivity, m_findCriterion.patternSyntax));
	startSearching(pSelectionMap, uri);
}

void TarSubsystem::startSearching( FileInfoToRowMap *, const URI &uri )
{
	qDebug() << "TarSubsystem::startSearching. -- uri.path (start path):" << uri.path();
	// TODO: include selectionMap in searching
	m_sSearchHost = uri.host();
	m_sStartSearchPath = uri.path().right(uri.path().length()-1);
	if (m_sStartSearchPath.isEmpty())
		m_sStartSearchPath = QDir::separator();

	m_nStartSearchLevel = m_sStartSearchPath.count('/') - 1;
	m_nStartSearchPathLen = m_sStartSearchPath.length();

	m_nSearchedLines = -1;
	m_searchingTimer.start(0);
}

bool TarSubsystem::isFileMatches( FileInfo *pFileInfo )
{
	bool matchFileOwner = true;
	bool matchFileGroup = true;
	bool matchTime = true;
	bool matchSize = true;
	bool matchName = false;
// 	bool matchName = m_findRegExp.exactMatch(fileInfo->fileName()); // check name matching
	foreach (QRegExp p, m_findRegExpLst) {
		if ((matchName=p.exactMatch(pFileInfo->fileName()))) // check if given name matches to pattern
			break;
	}
	if (! matchName)
		return false;

	if (m_findCriterion.checkFileOwner)
		matchFileOwner = (pFileInfo->owner() == m_findCriterion.owner);
	if (m_findCriterion.checkFileGroup)
		matchFileGroup = (pFileInfo->group() == m_findCriterion.group);

	if (m_findCriterion.checkTime) {
		QDateTime fileTime = pFileInfo->lastModified();
		matchTime = (fileTime >= m_findCriterion.firstTime && fileTime <= m_findCriterion.secondTime);
	}

	qint64 fileSize = pFileInfo->size();
	if (    m_findCriterion.checkFileSize != FindCriterion::NONE) {
		if (m_findCriterion.checkFileSize == FindCriterion::EQUAL)
			matchSize = (fileSize == m_findCriterion.fileSize);
		else if (m_findCriterion.checkFileSize == FindCriterion::ATLEAST)
			matchSize = (fileSize >= m_findCriterion.fileSize);
		else if (m_findCriterion.checkFileSize == FindCriterion::MAXIMUM)
			matchSize = (fileSize <= m_findCriterion.fileSize);
		else if (m_findCriterion.checkFileSize == FindCriterion::GREAT_THAN)
			matchSize = (fileSize >  m_findCriterion.fileSize);
		else if (m_findCriterion.checkFileSize == FindCriterion::LESS_THAN)
			matchSize = (fileSize <  m_findCriterion.fileSize);
	}

	return (matchName && matchFileOwner && matchFileGroup && matchTime && matchSize);
}

QString TarSubsystem::searchingResult()
{
	m_sSearchingResult = QString("%1 %2 %3").arg(m_nMatchedFiles).arg(m_nFileCounter).arg(m_nDirCounter);
	return m_sSearchingResult;
}


void TarSubsystem::copyFiles( FileInfoToRowMap *pSelectionMap, const QString &sTargetPath, bool bMove, bool bWeighBefore, const QString &sRequestingPanel )
{
	URI uri(sTargetPath);
	if (! uri.isValid()) {
		if (QFileInfo(QFileInfo(sTargetPath).path()).exists()) // probably new archive, so only directory should exists
			uri.setPath(sTargetPath);
		else {
			qDebug() << "TarSubsystem::copyFiles. Target directory doesn't exist:" << QFileInfo(sTargetPath).path();
			return;
		}
	}
	addTo((bMove ? MOVE_TO_ARCHIVE : COPY_TO_ARCHIVE), pSelectionMap, uri, sRequestingPanel);
}

void TarSubsystem::addTo( SubsystemCommand subsystemCommand, FileInfoToRowMap *pSelectionMap, const URI &uri, const QString &sRequestingPanel )
{
	QString sFN = "TarSubsystem::addTo.";
	if (pSelectionMap == NULL || pSelectionMap->size() == 0) {
		qDebug() << sFN << "Internal ERROR. Empty or null SelectionMap!";
		return;
	}
	m_opQueue.clear();
	m_sCompressionType = "";
	m_currentCommand = subsystemCommand;
// 	emit signalGetWeighingResult(m_totalWeight, m_nTotalItems);
// 	qDebug() << sFN << "File:" << uri.toString() << "Weigh:" << FileInfo::bytesRound(m_totalWeight) << "TotalItems:";
	qDebug() << sFN << "File:" << uri.toString();
	if (! QFileInfo(uri.toString()).exists()) {
		if (! uri.toString().endsWith('/')) {
			createNewArchive(uri.toString(), pSelectionMap, sRequestingPanel);
			return;
		}
	}
	bool bArchiveFiles = (subsystemCommand == COPY_TO_ARCHIVE || subsystemCommand == MOVE_TO_ARCHIVE);
	if (bArchiveFiles && uri.path().isEmpty()) {
		m_error  = CANNOT_ARCHIVE;
		m_status = ERROR_OCCURED;
		m_sProcessedFileName = uri.host();
		emit signalStatusChanged(m_status, sRequestingPanel);
		return;
	}
	if (uri.host().isEmpty()) {
		qDebug() << sFN << "Internal ERROR. Empty uri.host!";
		return;
	}
	QString sMime = MimeType::getMimeFromPath(uri.host());
	bool bCompressedTarArch = (sMime != "application/x-tar");
	if (bCompressedTarArch) {
		Settings settings;
		bool bTarAlwaysDecompressIfUpdateArchive = settings.value("archive/TarAlwaysDecompressIfUpdateArchive", false).toBool();;
		if (! bTarAlwaysDecompressIfUpdateArchive) {
			if (QFileInfo(uri.host()).exists()) {
				int nResult = MessageBox::yesNo(m_pParent, tr("Adding to archive ..."), uri.host(),
									"\n"+tr("Compressed tar archive. Its update requires decompress")+"\n"+tr("and compress archive again.")+"\n"+tr("Originally used compression level will be lost.")+
									"\n"+tr("Do you want to continue?"),
									bTarAlwaysDecompressIfUpdateArchive,
									MessageBox::Yes, // default focused button
									tr("Do not ask again")
				);

				settings.update("archive/TarAlwaysDecompressIfUpdateArchive", bTarAlwaysDecompressIfUpdateArchive);
				if (nResult == MessageBox::No)
					return;
			}
		}
	}
	m_sRequestingPanel = sRequestingPanel;
	m_sRequestingPanel_open = sRequestingPanel;
	m_processedFileInfo = pSelectionMap->constBegin().key();
	initArchiving();
	m_preProcessedFiles->clear();

	QString   sUriPath = uri.path().right(uri.path().length()-1);
	QString   sTarCmdForFiles, sTarCmdForDir;
	QString   sFileNameList, sFullFileNamesToAdd;
	FileInfo *pFileInfo, *pNewFileInfo;
	QStringList slDirsCommands;
	int nDirsNum  = 0;
	int nFilesNum = 0;

	QString sArchiveName;
	if (bCompressedTarArch)
		sArchiveName = QFileInfo(uri.host()).path()+"/"+QFileInfo(uri.host()).completeBaseName();
	else
		sArchiveName = uri.host();

    FileInfoToRowMap::const_iterator it = pSelectionMap->constBegin();
	while (it != pSelectionMap->constEnd()) {
		pFileInfo = it.key();
		if (bArchiveFiles) {
			if (pFileInfo->isDir() && ! pFileInfo->isSymLink()) {
				sTarCmdForDir = "tar -rvf \""+sArchiveName+"\" --transform \"s,.*/,"+sUriPath+pFileInfo->fileName()+"/,\" \""+pFileInfo->absoluteFileName()+"\"";
				//tar -rvf "/home/tst_qtcmd/test_archives/1/lxterminal-0.1.6.tar" --transform "s,.*/,lxterminal-0.1.6/bbc/," "/home/tst_qtcmd/test_archives/1/bbc"
				slDirsCommands.append(sTarCmdForDir);
				sTarCmdForDir = "tar -vf \""+sArchiveName+"\" --delete \""+sUriPath+pFileInfo->fileName()+"/"+pFileInfo->fileName()+"\"";
				//tar vf "/home/tst_qtcmd/test_archives/1/lxterminal-0.1.6.tar" --delete lxterminal-0.1.6/bbc/bbc
				slDirsCommands.append(sTarCmdForDir);
			}
			else {
				sFullFileNamesToAdd += " \""+pFileInfo->absoluteFileName()+"\"";
			}
		}
		m_preProcessedFiles->append(pFileInfo);
		pNewFileInfo = new FileInfo(pFileInfo);
		pNewFileInfo->setData(2, sUriPath, true); // workaround for setting path (for second panel we need to set correct path for new item)
		m_processedFiles->append(pNewFileInfo);
		if (pFileInfo->isDir() && ! pFileInfo->isSymLink())
			nDirsNum++;
		else
			nFilesNum++;
		m_totalWeight += pFileInfo->size();
		++it;
	}

	m_nTotalItems = pSelectionMap->size();
	if (bArchiveFiles) {
		if (! sFullFileNamesToAdd.isEmpty()) {
			// tar -uvf /home/piotr/tmp/cd999.tar  --transform   's,.*/,/cd999/Eva (1=2cd)/,' /home/piotr/tmp/lista3.txt
			// tar -uvf "/home/tst_qtcmd/test_archives/1/lxterminal-0.1.6.tar" --transform "s,.*/,/lxterminal-0.1.6/,"  "/home/tst_qtcmd/test_archives/1/fran.txt"
			sTarCmdForFiles = "tar -rvf \""+sArchiveName+"\" --transform \"s,.*/,"+sUriPath+",\"" + sFullFileNamesToAdd;
		}
		if (bCompressedTarArch) {
			if (! decompress(m_currentUri, sRequestingPanel))
				return;
		}
		if (! sTarCmdForFiles.isEmpty())
			m_opQueue << Operation(subsystemCommand, sTarCmdForFiles, sRequestingPanel);
		//m_opQueue << Operation(PROG_EXISTS, "which tar", sRequestingPanel); // skip because tar archive is alredy opened, tar had to exist
		if (! slDirsCommands.isEmpty()) {
			m_slAddedFilesFromSubdirectories.clear();
			foreach(QString sTarCmd, slDirsCommands) {
				m_opQueue << Operation(subsystemCommand, sTarCmd, sRequestingPanel);
			}
		}
		if (bCompressedTarArch) {
			if (! compress(m_currentUri, sRequestingPanel, true)) // cannot restore level of compression, so will be used default; true for skip showing confirmation message if file exists
				return;
		}
		emit signalCounterProgress(nDirsNum, DIR_COUNTER, true);
		emit signalCounterProgress(nFilesNum, FILE_COUNTER, true);
		emit signalCounterProgress(m_totalWeight, WEIGH_COUNTER, true);
	}
	emit signalNameOfProcessedFile(m_processedFileInfo->absoluteFileName(), uri.host()+uri.path());
	slotRunNextOperation();
}
// viewCompressedFile (as output we're getting decompressed file)
// gzip -dc /home/piotr/mnt_d1/mlDonkey/incoming/files_o/filmy/lista3.txt.gz 2>/dev/null

// Verify archive
// tar tvfW tecmint-14-09-12.tar

void TarSubsystem::createNewArchive( const QString &sInArchiveName, FileInfoToRowMap *pSelectionMap, const QString &sRequestingPanel )
{
	QString sFN = "TarSubsystem::createNewArchive.";
	if (pSelectionMap == NULL || pSelectionMap->size() == 0) {
		qDebug() << sFN << "Internal ERROR. Empty or null SelectionMap!";
		return;
	}
// 	bool bSingleMultiFileArchive = (sInArchiveName.endsWith('|'));
	qDebug() << sFN << "ArchiveName:" << sInArchiveName;
	QString sTargetPath = QFileInfo(sInArchiveName).path();
	m_sTargetArchivePath = sTargetPath;
	if (! QFileInfo(sTargetPath).isWritable()) {
		m_error  = CANNOT_WRITE;
		m_status = ERROR_OCCURED;
		m_sProcessedFileName = sTargetPath;
		emit signalStatusChanged(m_status, sRequestingPanel);
		return;
	}
	m_sRequestingPanel_open = sRequestingPanel;
	FileInfo *pFileInfo = pSelectionMap->constBegin().key();
	m_processedFileInfo = pFileInfo;
	m_preProcessedFiles->clear();
	m_preProcessedFiles->append(pFileInfo);
	m_sSingleMultiFileArchiveName = "";

	initArchiving();
// 	m_nTotalItems = 0; // weighing is doing in different subsystem
	emit signalGetWeighingResult(m_totalWeight, m_nTotalItems);
	m_bCreateNewArchive = true;
	Settings settings;
	bool bAlwaysOverwriteTargetIfCompress = settings.value("FilesSystem/AlwaysOverwriteWithNewArchive", false).toBool();

	// -- detect compression type
	int nExtId = sInArchiveName.length() - sInArchiveName.lastIndexOf(".tar");
	m_sCompressionType = (nExtId == 4) ? "" : sInArchiveName.right(nExtId - 5);
	qDebug() << sFN << "Selected CompressionType:" << m_sCompressionType;
	int nCompressionTypeLen = m_sCompressionType.length();
	if (nCompressionTypeLen > 0)
		nCompressionTypeLen++;

	QString sFileNamesToAdd = "";
	QString sFullFileNamesToAdd = "";
	FileInfoToRowMap::const_iterator it = pSelectionMap->constBegin();
	while (it != pSelectionMap->constEnd()) {
		pFileInfo = it.key();
		m_processedFiles->append(pFileInfo); // m_processedFiles is cleaned in initArchiving
		sFullFileNamesToAdd += "\""+pFileInfo->absoluteFileName();
		sFullFileNamesToAdd += "\"|";
		sFileNamesToAdd += pFileInfo->fileName();
		if (pFileInfo->isDir())
			sFileNamesToAdd += "/";
		sFileNamesToAdd += "|";
		++it;
	}
	sFullFileNamesToAdd.remove(sFullFileNamesToAdd.length()-1, 1); // remove last coma

	QString sFullArchiveName = sInArchiveName;
	QString sArchiveName = "\""+ sInArchiveName.left(sInArchiveName.length() - nCompressionTypeLen) +"\""; // rid off (alternatively) compressor extension
	if (sInArchiveName.endsWith('|')) { // SingleMultiFileArchive
		sFullArchiveName = sFullArchiveName.left(sFullArchiveName.indexOf('|'));
		if (sArchiveName.indexOf('|') > 0)
			sArchiveName.remove(sArchiveName.indexOf('|'), 1);
		m_sSingleMultiFileArchiveName = sArchiveName;
		m_sCompressionType.remove('|');
	}
	uint i=0;
	QString sCmd;
	QStringList sFileNamesToAddLst = sFileNamesToAdd.split('|', Qt::SkipEmptyParts);
	QStringList sFullFileNamesToAddLst = sFullFileNamesToAdd.split('|', Qt::SkipEmptyParts);
	foreach (QString sName, sFileNamesToAddLst) {
		if (! sName.endsWith('/')) { // file
			sCmd = "tar -rvf " + sArchiveName +" --transform \"s,.*/,,\" "+sFullFileNamesToAddLst.at(i); // for file remove leading path
			//tar -uvf "/home/piotrek/Wideo/filmy_folder_przejsciowy/short/New_Archive.tar" --transform "s,.*/,," "/home/piotrek/tmp/firefox_20150418.session"
		}
		else { // directory
			sName = sName.left(sName.indexOf('/'));
			sCmd = "tar -rvf " + sArchiveName +" --transform \"s,.*/"+sName+","+sName+",\" "+sFullFileNamesToAddLst.at(i);
			//tar -rvf "/home/piotrek/Wideo/filmy_folder_przejsciowy/short/New_Archive.tar" --transform "s,.*/openSuse_info,openSuse_info," "/home/piotrek/tmp/openSuse_info"
			//tar -rvf /home/piotrek/Wideo/filmy_folder_przejsciowy/short/New_Archive.tar --transform "s,home/piotrek/tmp/openSuse_info,openSuse_info," /home/piotrek/tmp/openSuse_info
		}
		m_opQueue << Operation(m_currentCommand, sCmd, sRequestingPanel);
		if (bAlwaysOverwriteTargetIfCompress) {
			if (QFile(sFullArchiveName).remove())
				qDebug() << sFN << "Removed file:" << sFullArchiveName;
		}
		i++;
	}
	emit signalNameOfProcessedFile(m_processedFileInfo->absoluteFileName(), sFullArchiveName);
	slotRunNextOperation();
}

void TarSubsystem::extractTo( WhatExtract whatExtract, bool bMove, FileInfoToRowMap *pSelectionMap, const QString &sTargetPath, const QString &sRequestingPanel )
{
	QString sFN = "TarSubsystem::extractTo.";
	if (pSelectionMap == NULL || pSelectionMap->size() == 0) {
		qDebug() << sFN << "Internal ERROR. Empty or null SelectionMap!";
		return;
	}
	qDebug() << sFN << "TargetPath:" << sTargetPath;
	m_opQueue.clear();
	if (! QFileInfo(sTargetPath).isWritable()) {
		m_error  = CANNOT_WRITE;
		m_status = ERROR_OCCURED;
		m_currentCommand = EXTRACT;
		m_bOperationFinished = true;
		m_sProcessedFileName = sTargetPath;
		emit signalStatusChanged(m_status, sRequestingPanel);
		return;
	}

	Settings settings;
	bool bPersistPathInExtractingArch = settings.value("archive/PersistPathInExtractingArch", true).toBool();
	bool bWeighBeforeArchExtracting   = settings.value("archive/WeighBeforeArchExtracting", true).toBool();
	bool bWeighBeforeFilesExtracting  = true; // always weigh files
	bool bAlwaysOverwrite = settings.value("FilesSystem/AlwaysOverwrite", true).toBool();
	m_sRequestingPanel = sRequestingPanel;
	m_sRequestingPanel_open = sRequestingPanel;
	m_whatExtract = whatExtract;
	m_processedFileInfo = pSelectionMap->constBegin().key();

	initWeighing(true, m_sRequestingPanel);
	if (whatExtract == Vfs::EXTRACT_FILES) {
		if (bWeighBeforeFilesExtracting) {
			weigh(pSelectionMap, currentURI(), sRequestingPanel);
			emit signalCounterProgress(m_nDirCounter,  DIR_COUNTER, true);
			emit signalCounterProgress(m_nFileCounter, FILE_COUNTER, true);
			emit signalCounterProgress(m_totalWeight,  WEIGH_COUNTER, true);
		}
	}
	initExtracting();

	FileInfo *pFileInfo;
	QString   sFileNameList;
	QString   sCmd, sExtrSwitch;
	QString   sFullFileNamesToExtract, sFileName;
	QString   sExclPath = (! bPersistPathInExtractingArch) ? " --exclude \".*/$\" " : " ";
	QString   sOvrMode  = (bAlwaysOverwrite) ? " --overwrite " : " ";
	sOvrMode = " --overwrite "; // because "Overwrite mode" is not supported yet TODO: implement "Overwrite mode" for extracting entire archives
	QStringList slFileNames;

	if ((whatExtract == EXTRACT_ARCHIVE && ! bWeighBeforeArchExtracting) || whatExtract == EXTRACT_FILES)
		m_opQueue << Operation(PROG_EXISTS, "which tar", sRequestingPanel);

    FileInfoToRowMap::const_iterator it = pSelectionMap->constBegin();
	while (it != pSelectionMap->constEnd()) {
		pFileInfo = it.key();
		if (whatExtract == EXTRACT_ARCHIVE) {
			if (! fileIsMatchingToSubsystemMime(m_pPluginData->mime_type, pFileInfo)) {
				++it;
				continue;
			}
			sFileName = absFileNameInLocalFS(pFileInfo, m_sTmpDir);
			sFullFileNamesToExtract = "\"" + sFileName + "\" ";
			// tar -C /path/to/extract/to -xvf archive.tar
			sExtrSwitch = (MimeType::getFileMime(sFileName) == "application/x-compress") ? " -zxvf " : " -xvf ";
			sCmd = "tar -C \"" + sTargetPath+"\"" + sExclPath + sOvrMode + sExtrSwitch + sFullFileNamesToExtract;
			if (bWeighBeforeArchExtracting)
				m_opQueue << Operation(WEIGH_ARCH, pFileInfo, sTargetPath, sRequestingPanel);
			m_opQueue << Operation(EXTRACT, sCmd, sRequestingPanel);
		}
		else { // EXTRACT_FILES
			slFileNames.append(pFileInfo->absoluteFileName());
			sFileNameList += " \"" + pFileInfo->absoluteFileName() +"\"";
		}
		m_processedFiles->append(pFileInfo);
		++it;
	}

	qDebug() << sFN << "TotalItems:" << m_nTotalItems << "processedFiles:" << m_processedFiles->size();
	if (whatExtract == EXTRACT_FILES) {
		if (bMove)
			m_currentCommand = Vfs::EXTRACT_AND_REMOVE;
		sExtrSwitch = (MimeType::getFileMime(m_sHost) == "application/x-compress") ? " -zx " : " -x ";
		sCmd= "tar -C " + sTargetPath + sExtrSwitch + sFileNameList + " -vf " + "\"" + m_sHost + "\"" + " --strip-components=" + QString("%1").arg(slFileNames.at(0).count(QDir::separator()));
		m_opQueue << Operation(m_currentCommand, sCmd, sRequestingPanel);
	}
	emit signalNameOfProcessedFile(sFileName, sTargetPath);
	slotRunNextOperation();
}

void TarSubsystem::initArchiving()
{
	m_sOutBuffer = "";
	m_sLastDirName = "";
	m_opQueue.clear();
	m_processedFiles->clear();
	m_totalWeight    = 0;
	m_OpTotalWeight  = 0;
	m_nOpDirCounter  = 0;
	m_nOpFileCounter = 0;
	m_nOpSymlinksCounter = 0;
	m_status = ARCHIVING;
	m_bCreateNewArchive = false;
	emit signalStatusChanged(m_status, m_sRequestingPanel);
}

void TarSubsystem::initExtracting()
{
	if (m_status == WEIGHED) {
		qDebug() << "TarSubsystem::initExtracting. WEIGHED - totalWeigh:" << m_totalWeight << FileInfo::bytesRound(m_totalWeight) << "files:" << m_nFileCounter << "dirs:" << m_nDirCounter << "all items:" << m_nTotalItems;
	}
// 	qDebug() << "TarSubsystem::initExtracting. Cleaning variables.";
	m_sOutBuffer = "";
	m_sLastDirName = "";
	m_opQueue.clear();
	m_processedFiles->clear();
	m_preProcessedFiles->clear();
	m_OpTotalWeight  = 0;
	m_nOpDirCounter  = 0;
	m_nOpFileCounter = 0;
	m_nOpSymlinksCounter = 0;
	m_currentCommand = EXTRACT;
	m_status = EXTRACTING;
	m_bCreateNewArchive = false;
	// 	m_sRequestingPanel = sRequestingPanel; // used in slotProcessFinished
	emit signalStatusChanged(m_status, m_sRequestingPanel);
}


void TarSubsystem::updateContextMenu( ContextMenu *pContextMenu, KeyShortcuts *pKS )
{
	qDebug() << "TarSubsystem::updateContextMenu.";
	m_pContextMenu = pContextMenu;

	m_pContextMenu->setVisibleAction(CMD_ExtractTo, false);
	m_pContextMenu->setVisibleAction(CMD_Makelink, false);
	m_pContextMenu->setVisibleAction(CMD_EditSymlink, false);
	m_pContextMenu->setVisibleAction(CMD_Rename, false);
	m_pContextMenu->setVisibleAction(CMD_CreateNew_MENU, false);
}


void TarSubsystem::searchingFinished()
{
	m_searchingTimer.stop();
	m_processedFiles->clear();
	qDebug() << "TarSubsystem::searchingFinished. Finished at line:" << m_nSearchedLines;
	m_status = FOUND;
	m_bOperationFinished = true;
	emit signalStatusChanged(m_status, m_sRequestingPanel);
}

void TarSubsystem::setBreakOperation()
{
	qDebug() << "TarSubsystem::setBreakOperation. Operation stopped.";
	if (m_currentCommand == FIND)
		searchingFinished();
}

void TarSubsystem::setPauseOperation( bool bPause )
{
	qDebug() << "TarSubsystem::setPauseOperation. Pause:" << bPause;
	m_bPausedOperation = bPause;
	m_error = NO_ERROR;
	if (bPause) {
		m_searchingTimer.stop();
		m_pausedStatus = m_status;
		m_status = OPR_PAUSED;
	} else {
		m_status = m_pausedStatus;
		m_searchingTimer.start(0);
	}
	emit signalStatusChanged(m_status, m_sRequestingPanel);
}

void TarSubsystem::updateParsingBuffer( SubsystemCommand subsysCmd )
{
	bool bArchiveFiles = (subsysCmd == COPY_TO_ARCHIVE || subsysCmd == MOVE_TO_ARCHIVE);
	if (subsysCmd != REMOVE && ! bArchiveFiles)
		return;

	QString sPerm, sAbsFileName, sLocalName;
	QString sNewItem, sFoundName;
	QFileInfo fi;
	int nId = -1;

	foreach (FileInfo *pFI, *m_processedFiles) {
		sLocalName = pFI->absoluteFileName();
		if (subsysCmd == REMOVE) {
			if (pFI->isDir())
				sLocalName += QDir::separator();
			nId = m_slParsingBufferLine.indexOf(QRegExp(".* "+sLocalName));
			if (nId != -1)
				m_slParsingBufferLine.removeAt(nId);
			nId = m_fiList.indexOf(pFI);
			if (nId != -1)
				m_fiList.removeAt(nId);
		}
		else
		if (bArchiveFiles) {
			if (sLocalName.startsWith(QDir::separator()))
				sLocalName = sLocalName.right(sLocalName.length()-1);
			if (pFI->isDir())
				sLocalName += QDir::separator();
			// Commented due to tar always add new files DOENS'T REPLACE if exist, so there is possible existing more than one file with the same name
		/*	nId = m_slParsingBufferLine.indexOf(QRegExp(".* "+sLocalName));
			if (nId != -1) {
				sFoundName = m_slParsingBufferLine.at(nId);
				sFoundName = getName(sFoundName);
				sFoundName = QString::fromLocal8Bit(sFoundName.toLatin1().data());
				if (sFoundName == sLocalName)
					continue;
			} */
			sNewItem = pFI->permissions()+" "+pFI->owner()+"/"+pFI->group()+" "+QString("%1").arg(pFI->size())+" "+pFI->lastModified().toString("yyyy-MM-dd HH:mm")+" "+sLocalName;
			m_slParsingBufferLine.append(sNewItem);
			m_fiList.append(pFI);
		}
	}
	m_nFilesNumInArch = m_slParsingBufferLine.count();

	// -- update internal list with newly files from added directory
	if (m_slAddedFilesFromSubdirectories.isEmpty())
		return;

	foreach (QString sFullFileName, m_slAddedFilesFromSubdirectories) {
		sLocalName = m_currentUri.path() +"/"+ sFullFileName.split('|').at(1);
		if (subsysCmd == REMOVE) {
			if (sLocalName.startsWith('/'))
				sLocalName = sLocalName.right(sLocalName.length()-1);
			nId = m_slParsingBufferLine.indexOf(QRegExp(".* "+sLocalName));
			if (nId != -1)
				m_slParsingBufferLine.removeAt(nId);
		}
		else
		if (bArchiveFiles) {
			sAbsFileName = sFullFileName.split('|').at(0);
			if (sLocalName.startsWith('/'))
				sLocalName = sLocalName.right(sLocalName.length()-1);
			// Commented due to tar always add new files DOENS'T REPLACE if exist, so there is possible existing more than one file with the same name
		/*	nId = m_slParsingBufferLine.indexOf(QRegExp(".* "+sLocalName));
			if (nId != -1) {
				sFoundName = m_slParsingBufferLine.at(nId);
				sFoundName = getName(sFoundName);
				sFoundName = QString::fromLocal8Bit(sFoundName.toLatin1().data());
				if (sFoundName == sLocalName)
					continue;
			} */
			sPerm = permissionsAsString(sAbsFileName);
			fi.setFile(sAbsFileName);
			sNewItem = sPerm+" "+fi.owner()+"/"+fi.group()+" "+QString("%1").arg(fi.size())+" "+fi.lastModified().toString("yyyy-MM-dd HH:mm")+" "+sLocalName;
			if (m_slParsingBufferLine.indexOf(sLocalName) < 0)
				m_slParsingBufferLine.append(sNewItem);
		}
	}
	if (subsysCmd == REMOVE)
		m_slAddedFilesFromSubdirectories.clear();

	m_nFilesNumInArch = m_slParsingBufferLine.count();
}


bool TarSubsystem::compress( const URI &uri, const QString &sRequestingPanel, bool bSkipTargetExistingCheck )
{
	QString sCmd = compressApp(uri.mime());
	if (sCmd.isEmpty()) {
		qDebug() << "TarSubsystem::compress. Cannot find matching compress application to given mime:" << uri.mime();
		return false;
	}
	Settings settings;
	bool bAlwaysOverwriteTargetIfCompress = settings.value("FilesSystem/AlwaysOverwriteWhenArchiving", false).toBool();
	qDebug() << "TarSubsystem::compress. FileName:" << uri.toString() << "SkipTargetExistingCheck:" << bSkipTargetExistingCheck << "AlwaysOverwriteWhenArchiving:" << bAlwaysOverwriteTargetIfCompress;
	if (! bSkipTargetExistingCheck) {
		if (! bAlwaysOverwriteTargetIfCompress) {
			if (QFileInfo(uri.host()).exists()) {
				int nResult = MessageBox::yesNo(m_pParent, tr("Compression ..."), uri.host(), tr("Target archive already exists!")+"\n"+tr("Do you want to overwrite it?"),
										bAlwaysOverwriteTargetIfCompress,
										MessageBox::Yes, // default focused button
										tr("Always overwrite")
				);

				settings.update("FilesSystem/AlwaysOverwriteWhenArchiving", bAlwaysOverwriteTargetIfCompress);
				if (nResult == MessageBox::No)
					return false;
			}
		}
	}

	QString sCCmd = sCmd + " -f " + QFileInfo(uri.host()).path()+"/"+QFileInfo(uri.host()).completeBaseName();
	m_opQueue << Operation(PROG_EXISTS, "which "+sCmd, sRequestingPanel);
	m_opQueue << Operation(COMPRESS, sCCmd, sRequestingPanel);
	return true;
}

void TarSubsystem::compressTar( bool bCompress, const QString &sArchiveName, const QString &sRequestingPanel )
{
	QString sFN = "TarSubsystem::compressTar.";
	Settings settings;
	QString sCompressionLevel = settings.value("archive/"+m_sCompressionType+"LevelCompression", "6").toString(); // range 0-9
	int nCompressionLevel = sCompressionLevel.toInt();
	if (nCompressionLevel < 0)
		sCompressionLevel = "0";
	else
	if (nCompressionLevel > 9)
		sCompressionLevel = "9";

	qDebug() << sFN << "ArchiveName:" << sArchiveName << "Compress:" << bCompress << "CompressionLevel:" << sCompressionLevel << "CompressionType:" << m_sCompressionType;
	QString sCmd = compressor(m_sCompressionType);
	if (bCompress) {
		m_currentCommand = COMPRESS;
		sCmd += " -f -" + QString("%1").arg(nCompressionLevel);
	}
	else {
		m_currentCommand = DECOMPRESS;
		sCmd += " -d";
	}
	sCmd += " "+ sArchiveName;
	qDebug() << sFN << "sCmd:" << sCmd; // gzip -f -6 /home/piotrek/Wideo/filmy_folder_przejsciowy/Adobe.tar
	m_opQueue << Operation(m_currentCommand, sCmd, sRequestingPanel);
	emit signalNameOfProcessedFile(sArchiveName);
	slotRunNextOperation();
	// lz     tar cf - %f | lzip -f -6 > "%f.tar.lz"  (not handled yet)
	// 7za    tar cf - %f | 7za a -si "%f.tar.7z"  (7-zip not handled yet)
	// ---
	// Tip. How to view compressed files: extract to standard output
}

bool TarSubsystem::decompress( const URI &uri, const QString &sRequestingPanel )
{
	QString sCmd = compressApp(uri.mime());
	if (sCmd.isEmpty()) {
		qDebug() << "TarSubsystem::decompress. Cannot find matching decompress application to given mime:" << uri.mime();
		m_opQueue.clear();
		m_error  = CANNOT_ARCHIVE;
		m_status = ERROR_OCCURED;
		m_sProcessedFileName = uri.host();
		emit signalStatusChanged(m_status, sRequestingPanel);
		return false;
	}
	Settings settings;
	bool bAlwaysOverwriteTargetIfDecompress = settings.value("FilesSystem/AlwaysOverwrite", false).toBool();;
	qDebug() << "TarSubsystem::decompress. FileName:" << uri.toString() << "AlwaysOverwriteIfDecompress:" << bAlwaysOverwriteTargetIfDecompress;
	if (! bAlwaysOverwriteTargetIfDecompress) {
		QString sTarArch = QFileInfo(uri.host()).path()+"/"+QFileInfo(uri.host()).completeBaseName();
		if (QFileInfo(sTarArch).exists()) {
			int nResult = MessageBox::yesNo(m_pParent, tr("Decompression ..."), sTarArch, tr("Target archive already exists!")+"\n"+tr("Do you want to overwrite it?"),
								bAlwaysOverwriteTargetIfDecompress,
								MessageBox::Yes, // default focused button
								tr("Always overwrite")
			);

			settings.update("FilesSystem/AlwaysOverwrite", bAlwaysOverwriteTargetIfDecompress);
			if (nResult == MessageBox::No)
				return false;
		}
	}

	QString sDCmd = sCmd + " -df " + uri.host();
	m_opQueue << Operation(PROG_EXISTS, "which "+sCmd, sRequestingPanel);
	m_opQueue << Operation(DECOMPRESS, sDCmd, sRequestingPanel);
	return true;
}

QString TarSubsystem::compressApp( const QString &sMime ) const
{
	if (sMime == "application/x-compressed-tar" || sMime == "application/gzip")
		return "gzip";
	else
		if (sMime == "application/x-bzip-compressed-tar" || sMime == "application/x-bzip")
		return "bzip2";
	else
	if (sMime == "application/x-lzma-compressed-tar" || sMime == "application/x-lzma")
		return "lzma";
	else
	if (sMime == "application/x-xz-compressed-tar" || sMime == "application/x-xz")
		return "xz";
	else
	if (sMime == "application/x-lzip")
		return "lz";
	else
	if (sMime == "application/x-compress")
		return "compress";
	else
	if (sMime == "application/x-tar")
		return "tar";

	return "";
}

QString TarSubsystem::compressor( const QString &sExtension ) const
{
	if (sExtension == "gz") return "gzip";
	else
	if (sExtension == "bz2")  return "bzip2";
	else
	if (sExtension == "lzma")  return "lzma";
	else
	if (sExtension == "lz")  return "lzip";
	else
	if (sExtension == "xz")  return "xz";
	else
	if (sExtension == "Z")  return "compress";

	return "";
}

// ------------- SLOTs -------------

void TarSubsystem::slotRunNextOperation()
{
	QString sFN = "TarSubsystem::slotRunNextOperation.";
	if (m_opQueue.count() == 0) {
		qDebug() << sFN << "Empty queue. Exit.";
		return;
	}
	if (m_pProcess->state() != QProcess::NotRunning) {
		qDebug() << sFN << "Process is already running.";
		return;
	}
	m_sOutBuffer = "";
	m_sErrBuffer = "";

	Operation operation = m_opQueue.first();
	m_currentCommand = operation.command;  // internal command
	if (m_currentCommand == OPEN || m_currentCommand == VIEW)
		m_sRequestingPanel_open = operation.subsysMngrName;
	else
		m_sRequestingPanel = operation.subsysMngrName;

	bool bShellCmd = (! operation.shellCmd.isEmpty());
	m_opQueue.pop_front();
	qDebug() << sFN << " RequestingPanel:" << m_sRequestingPanel << "RequestingPanel_open:" << m_sRequestingPanel_open << " opQueue.size:" << m_opQueue.size();
	if (m_currentCommand == WEIGH_ARCH)
		m_bWeighingArchive = true;

	if (m_currentCommand == OPEN || m_currentCommand == VIEW) {
		if (bShellCmd)
			m_status = CONNECTING;
		else // reopen
			open(operation.uri, operation.subsysMngrName);
	}

	if (bShellCmd) {
		if      (m_currentCommand == OPEN_WITH_CMD)
			m_status = OPENING_WITH; // TODO: show progress bar for extracting files
		else if (m_currentCommand == REMOVE)
			m_status = REMOVING;
		else if (m_currentCommand == COPY_TO_ARCHIVE || m_currentCommand == MOVE_TO_ARCHIVE)
			m_status = ARCHIVING;
		else if (m_currentCommand == EXTRACT || m_currentCommand == EXTRACT_AND_REMOVE)
			m_status = EXTRACTING;
		else if (m_currentCommand == COMPRESS)
			m_status = COMPRESSING;
		else if (m_currentCommand == DECOMPRESS || m_currentCommand == DECOMPRESS_TO_VIEW)
			m_status = DECOMPRESSING;
		else if (m_currentCommand == COPY)
			m_status = COPYING;

		if (m_currentCommand != PROG_EXISTS)
			emit signalStatusChanged(m_status, m_sRequestingPanel_open);

		m_sLastShellCmd = operation.shellCmd;
		qDebug() << sFN << "Command:" << toString(m_currentCommand) << "Execute shell cmd:" << m_sLastShellCmd << " RequestingPanel:" << m_sRequestingPanel_open;
		if (m_currentCommand == EXTRACT) {
			if (m_whatExtract == EXTRACT_ARCHIVE) {
				QString sExtractedArch = m_sLastShellCmd.split('\"').at(3);
				for (int i = 0; i < m_processedFiles->size(); ++i) {
					if (m_processedFiles->at(i)->absoluteFileName() == sExtractedArch) {
						m_processedFileInfo = m_processedFiles->at(i);
						break;
					}
				}
			}
		}
		if (m_currentCommand == REMOVE || m_currentCommand == DECOMPRESS)
			QTimer::singleShot(0, this, &TarSubsystem::slotStartProcess);
		else
			m_pProcess->start(m_sLastShellCmd);
	} else {
		if      (m_currentCommand == WEIGH)
			weigh(operation.selectionMap, operation.uri, operation.subsysMngrName, operation.booleanArg); // bWeighingAsSubCmd=true
		else if (m_currentCommand == WEIGH_ITEM)
			weighItem(operation.fileInfo, operation.uri, operation.subsysMngrName, operation.booleanArg);
		else if (m_currentCommand == WEIGH_ARCH) {
			weighArchive(absFileNameInLocalFS(operation.fileInfo, m_sTmpDir), operation.subsysMngrName);
// 			weighArchive(operation.fileInfo->absoluteFileName());
		}
		else if (m_currentCommand == REMOVE)
			remove(operation.selectionMap, operation.uri, operation.booleanArg, operation.subsysMngrName);
	}
}

void TarSubsystem::slotProcessFinished( int nExitCode, QProcess::ExitStatus exitStatus )
{
	QString sFN = "TarSubsystem::slotProcessFinished.";
	QString sSuccessFailrue = (nExitCode == 0) ?  "(Success)" : "(Failrue)";
	qDebug() << sFN << "ExitCode:" << nExitCode << "exitStatus:" << exitStatus << "m_currentCommand:" << Vfs::toString(m_currentCommand) << m_sRequestingPanel_open;
	m_bActionIsFinished = true;

	QString sCmdName;
	if (m_currentCommand == PROG_EXISTS)
		sCmdName = m_sLastShellCmd.split(' ').at(1); // useful only for 'which' command

	if (exitStatus == QProcess::NormalExit && nExitCode == 0) {
		m_error = NO_ERROR;
		if (m_currentCommand == PROG_EXISTS) {
			if (! m_sOutBuffer.isEmpty()) {
				if (m_sRequestingPanel_open != "leftPanel_1:init")
					m_status = CONNECTED;
				qDebug() << sFN << sCmdName << "found in PATH.";
				QTimer::singleShot (20, this, &TarSubsystem::slotRunNextOperation);
				return;
			}
		}
		else
		if (m_currentCommand == OPEN || m_currentCommand == VIEW) { // if open for weighing arch. or for listing or for view
			if (m_bWeighingArchive) {
				m_status = WEIGHING;
				emit signalStatusChanged(m_status, m_sRequestingPanel_open);
				m_currentCommand = WEIGH_ARCH;
			}
			else {
				if (m_currentCommand != VIEW) {
					m_status = LISTING;
					emit signalStatusChanged(m_status, m_sRequestingPanel_open);
				}
				parsingBufferedFilesList(); // parsing output of sys.op.command (inits m_nFilesNumInArch)
				if (m_sPath.endsWith(QDir::separator()) && m_currentCommand != VIEW) // if level > 0 and not view then set next called command
					m_currentCommand = LIST;
			}
			qDebug() << sFN << "Switched from OPEN or VIEW command to:" << Vfs::toString(m_currentCommand) << "and status to:" << Vfs::toString(m_status);
		}
		else
		if (m_currentCommand == COPY) {
			slotRunNextOperation();
			return;
		}

		if (m_currentCommand == OPEN || m_currentCommand == VIEW) { // parsing buffer for level = 0
			processingParsedFilesList(0, "");
			if (m_status == ERROR_OCCURED)
				return;
			m_sCurrentPath = QDir::separator();
			m_bOpenedArchive = true;
			qDebug() << sFN << "Command:" << Vfs::toString(m_currentCommand) << "m_sPath:" << m_sPath << "m_sCurrentPath:" << m_sCurrentPath << "m_bOpenedArchive:" << m_bOpenedArchive << "m_nFilesNumInArch:" << m_nFilesNumInArch;
			m_currentUri.setPath(m_sCurrentPath);
			if (m_currentCommand != VIEW) {
				m_currentCommand = LIST;
				m_status = m_bCompleterMode ? LISTED_COMPL : LISTED;
			}
			else
				m_status = VIEW_COMPL;

			qDebug() << sFN << "Switched from OPEN or VIEW command to:" << Vfs::toString(m_currentCommand) << "and status to:" << Vfs::toString(m_status);
			emit signalStatusChanged(m_status, m_sRequestingPanel_open);
			//qDebug() << m_sOutBuffer;
			//qDebug() << m_sParsingBufferLine;
		}
		else
		if (m_currentCommand == LIST) {
			commandList();
		}
		else
		if (m_currentCommand == OPEN_WITH_CMD) {
			if (! m_sOutBuffer.isEmpty())
				processingOpenWithCmd();
			else {
				m_status = ERROR_OCCURED;
				m_error  = OTHER_ERROR;
			}
			emit signalStatusChanged(m_status, m_sRequestingPanel);
		}
		else
		if (m_currentCommand == REMOVE) {
			if (m_opQueue.size() > 0) {
				if (m_opQueue.at(0).command == REMOVE) {
					QTimer::singleShot (0, this, &TarSubsystem::slotRunNextOperation);
					return;
				}
				QTimer::singleShot (0, this, &TarSubsystem::slotRunNextOperation);
			}
			m_status = REMOVED;
			m_bForceReopen = true;
			m_bOperationFinished = true;
			processingRemoveCmd(); // Workaround. Unfortunately tar doesn't prints deleted files
			updateParsingBuffer(REMOVE);
			emit signalTimerStartStop(false);
			emit signalOperationFinished(); // only updates button label to 'Done' on progress dialog
			emit signalStatusChanged(m_status, m_sRequestingPanel);
		}
		else
		if (m_currentCommand == READ_FILE_TO_VIEW) {
			m_status = EXTRACTED_TO_VIEW;
			emit signalStatusChanged(m_status, m_sRequestingPanel);
		}
		else
		if (m_currentCommand == WEIGH_ARCH) {
			parsingBufferedFilesList(); // parsing output of sys.op.command (inits m_nFilesNumInArch)
			processingWeighCmd(NULL);
			m_status = WEIGHED;
			//emit signalStatusChanged(m_status, m_sRequestingPanel_open); // helped for Properties dialog
			emit signalCounterProgress(m_nDirCounter,  DIR_COUNTER, true);
			emit signalCounterProgress(m_nFileCounter, FILE_COUNTER, true);
			emit signalCounterProgress(m_totalWeight,  WEIGH_COUNTER, true);
			m_bWeighingArchive = false;
			if (m_opQueue.count() > 0) {
				if (m_opQueue.at(0).command == EXTRACT) {
					if (m_whatExtract == Vfs::EXTRACT_ARCHIVE) {
						long long freeBytes, totalBytes;
						Vfs::getStatFS(m_sTmpDir, freeBytes, totalBytes);
						if (freeBytes < m_totalWeight) {
							m_opQueue.clear();
							m_error = NO_FREE_SPACE;
							emit signalStatusChanged(Vfs::ERROR_OCCURED, m_sRequestingPanel);
							return;
						}
					}
				}
				slotRunNextOperation();
				return;
			}
			else {
				emit signalStatusChanged(m_status, m_sRequestingPanel);
				return;
			}
		}
		else
		if (m_currentCommand == EXTRACT || m_currentCommand == EXTRACT_AND_REMOVE) {
			if (m_opQueue.count() > 0) {
				emit signalStatusChanged(EXTRACTED, m_sRequestingPanel); // for update extracting status on progress window
				QTimer::singleShot (0, this, &TarSubsystem::slotRunNextOperation);
				return;
			}
			else {
				m_status = EXTRACTED;
				if (m_whatExtract == EXTRACT_FILES)
					m_bOperationFinished = true;
				emit signalOperationFinished(); // only updates button label to 'Done' on progress dialog
				emit signalStatusChanged(m_status, m_sRequestingPanel);
			}
		}
		else
		if (m_currentCommand == COPY_TO_ARCHIVE || m_currentCommand == MOVE_TO_ARCHIVE) {
			if (m_opQueue.size() > 0) {
				if (m_opQueue.at(0).command == m_currentCommand) {
					QTimer::singleShot (0, this, &TarSubsystem::slotRunNextOperation);
					return;
				}
				QTimer::singleShot (0, this, &TarSubsystem::slotRunNextOperation);
			}
			if (m_sCompressionType.isEmpty()) {
				m_status = ARCHIVED;
				m_bForceReopen = true;
				m_bOperationFinished = true;
				if (! m_bCreateNewArchive)
					updateParsingBuffer(m_currentCommand);
				emit signalOperationFinished(); // only updates button label to 'Done' on progress dialog
				emit signalStatusChanged(m_status, m_sRequestingPanel);
			}
			else {
				if (m_sSingleMultiFileArchiveName.isEmpty())
					compressTar(true, m_sTargetArchivePath +"/"+ m_processedFileInfo->fileName()+".tar", m_sRequestingPanel);
				else
					compressTar(true, m_sSingleMultiFileArchiveName, m_sRequestingPanel);
			}
			return;
		}
		else
		if (m_currentCommand == COMPRESS) {
			m_status = COMPRESSED; // this is switched into ARCHIVED in SubsytemManager
			if (m_opQueue.size() == 0) {
				m_bOperationFinished = true;
				emit signalStatusChanged(m_status, m_sRequestingPanel_open);
// 				if (m_bCreateNewArchive) // in this moment handled in SubsystemManager::slotSubsystemStatusChanged
// 					emit signalOperationFinished(); // only updates button label to 'Done' on progress dialog
			}
			else
				QTimer::singleShot (0, this, &TarSubsystem::slotRunNextOperation);
			return;
		}
		else
		if (m_currentCommand == DECOMPRESS || m_currentCommand == DECOMPRESS_TO_VIEW) {
			m_status = (m_currentCommand == DECOMPRESS) ? DECOMPRESSED : DECOMPRESSED_TO_VIEW;
			if (m_opQueue.size() == 0)
				emit signalStatusChanged(m_status, m_sRequestingPanel_open);
			else
				slotRunNextOperation();
			return;
		}

		if ((m_currentCommand == LIST || m_currentCommand == VIEW) && m_opQueue.count() > 0) {
			qDebug() << sFN << toString(m_currentCommand) << "command finished. Call next queued command";
			if (m_sRequestingPanel_open != m_opQueue.first().subsysMngrName) { // if opened two the same archives then list previous
				emit signalStatusChanged(m_status, m_sRequestingPanel_open);
				QTimer::singleShot (0, this, &TarSubsystem::slotRunNextOperation);
				return;
			}
			else
				slotRunNextOperation();
		}

		if (m_currentCommand == LIST && m_status != CONNECTED) {
			qDebug() << sFN << "Listing finished. status:" << toString(m_status);
			emit signalStatusChanged(m_status, m_sRequestingPanel_open);
		}
	}
	else { // error occured
		qDebug() << sFN << "-- Process finished with error:" << nExitCode << "command:" << toString(m_currentCommand);
		qDebug() << sFN << "LastShellCmd:" << m_sLastShellCmd;
		qDebug() << m_sErrBuffer;
		m_status = ERROR_OCCURED;
		m_error  = OTHER_ERROR;
		m_bForceReopen = true;
		if (m_currentCommand == PROG_EXISTS) {
			if (! m_sErrBuffer.isEmpty()) {
				m_error = CMD_NOT_FOUND;
				qDebug() << sFN << sCmdName << "not found in PATH!";
				// - find properly command
				while (m_opQueue.size() > 0) {
					if (m_opQueue.first().command != PROG_EXISTS && m_opQueue.first().command != CHK_VER)
						break;
					m_opQueue.pop_front();
				}
				m_sProcessedFileName = "";
				if (m_opQueue.size() > 0) {
					Operation operation = m_opQueue.first();
					if (operation.command == REMOVE) m_sProcessedFileName = tr("Removing operation");
					else
					if (operation.command == OPEN || operation.command == VIEW) m_sProcessedFileName = tr("Opening archive");
					else
					if (operation.command == EXTRACT || operation.command == EXTRACT_AND_REMOVE) m_sProcessedFileName = tr("Extracting");
					else
					if (operation.command == WEIGH_ARCH) m_sProcessedFileName = tr("Weighing");
					else
					if (operation.command == OPEN_WITH_CMD) m_sProcessedFileName = tr("Opening file");
					else
					if (operation.command == READ_FILE_TO_VIEW) m_sProcessedFileName = tr("Previewing file");
					else
					if (operation.command == COMPRESS) m_sProcessedFileName = tr("Compressing file");
					else
					if (operation.command == DECOMPRESS) m_sProcessedFileName = tr("Decompressing file");
					m_sProcessedFileName += " "+tr("operation is not possible.") +"\n\n"+sCmdName+"\n\n" + tr("Cannot find command in system PATH!");
					if (operation.command == COMPRESS || operation.command == DECOMPRESS) {
						m_opQueue.clear();
					}
				}
			}
		}
		else
		if (m_currentCommand == OPEN_WITH_CMD) {
			if (! m_sErrBuffer.isEmpty()) {
				m_error = CANNOT_EXTRACT;
				qDebug() << sFN << "-- Cannot extract all input files";
			}
		}
		else
		if (m_currentCommand == OPEN || m_currentCommand == READ_FILE_TO_VIEW) {
			m_sProcessedFileName = m_currentUri.host();
			m_error = CANNOT_READ;
		}
		else if (m_currentCommand == REMOVE) {
			m_error = CANNOT_REMOVE;
		}
		else
		if (m_currentCommand == EXTRACT || m_currentCommand == EXTRACT_AND_REMOVE) {
			m_error = CANNOT_EXTRACT;
		}
		else
		if (m_currentCommand == COPY_TO_ARCHIVE || m_currentCommand == MOVE_TO_ARCHIVE) {
			m_sProcessedFileName = m_currentUri.host();
			m_error = CANNOT_ARCHIVE;
		}
		if (m_currentCommand == COMPRESS) {
			m_sProcessedFileName = m_currentUri.host();
			m_error = CANNOT_COMPRESS;
		}
		if (m_currentCommand == DECOMPRESS || m_currentCommand == DECOMPRESS_TO_VIEW) {
			m_sProcessedFileName = m_currentUri.host();
			m_error = CANNOT_DECOMPRESS;
		}
		m_bOperationFinished = true;
		emit signalStatusChanged(m_status, ((m_currentCommand == LIST || m_currentCommand == OPEN) ? m_sRequestingPanel_open : m_sRequestingPanel));
	}
}

void TarSubsystem::slotBreakProcess()
{
	if (m_pProcess->exitStatus() == QProcess::CrashExit) { // condition maybe not necessary (not tested) :/
		m_pProcess->kill();
		m_bActionIsFinished = true;
		m_status = OPR_STOPPED;
		m_error  = NO_ERROR;
		m_opQueue.clear();
		emit signalStatusChanged(m_status, ((m_currentCommand == LIST || m_currentCommand == OPEN) ? m_sRequestingPanel_open : m_sRequestingPanel));
	}
}


void TarSubsystem::slotReadStandardOutput()
{
	if (m_currentCommand == REMOVE
		|| m_currentCommand == EXTRACT || m_currentCommand == EXTRACT_AND_REMOVE
		|| m_currentCommand == COPY_TO_ARCHIVE || m_currentCommand == MOVE_TO_ARCHIVE )
	{
		m_sOutBuffer = m_pProcess->readAllStandardOutput();

		if (m_currentCommand == REMOVE) // slotReadStandardOutput is never called, because tar doesn't prints deleted files
			processingRemoveCmd();
		else
		if (m_currentCommand == EXTRACT || m_currentCommand == EXTRACT_AND_REMOVE)
			processingExtractCmd();
		else
			if (m_currentCommand == COPY_TO_ARCHIVE || m_currentCommand == MOVE_TO_ARCHIVE)
			processingArchiveCmd();
	}
	else
		m_sOutBuffer += m_pProcess->readAllStandardOutput();
}

void TarSubsystem::slotReadStandardError()
{
	m_sErrBuffer += m_pProcess->readAllStandardError();
}

void TarSubsystem::slotStartProcess()
{
	m_pProcess->start(m_sLastShellCmd);
}

void TarSubsystem::slotReadStandardOutputAsyn()
{
	if (m_currentCommand == REMOVE)
		QTimer::singleShot(0, this, &TarSubsystem::slotReadStandardOutput);
	else
		slotReadStandardOutput();
}

void TarSubsystem::slotSearching()
{
	if (m_findCriterion.stopIfXMatched) {
		if (m_nMatchedFiles == m_findCriterion.stopAfterXMaches) {
			searchingFinished();
			return;
		}
	}
	m_nSearchedLines++;
	if (m_nSearchedLines >= m_nFilesNumInArch) {
		searchingFinished();
		return;
	}

	bool isSymLink, isDir = false;
	int slashId, targetLinkId;
	QString sLine, sName, sDateTime, sFullPath, sOwner, sGroup, sMime, sTargetSymLink, sLastName;
	QStringList slLine, slOwnerAndGroup, slName;
	FileInfo *pFileInfo;

	sLine = m_slParsingBufferLine.at(m_nSearchedLines);
	sName = getName(sLine);
// 	qDebug() << "name" << sName << "sFullPath" << sFullPath;
	if (m_nStartSearchLevel > 0) {
		if (sName.startsWith(m_sStartSearchPath)) {
			slashId = sName.indexOf('/', m_nStartSearchPathLen);
			if (slashId > 0) { // dir or symlink
				targetLinkId = sName.indexOf("->");
				if (targetLinkId > 0)
					sTargetSymLink = sName.right(sName.length()-targetLinkId-3);
				else
					sTargetSymLink = "";
				sName = sName.mid(m_nStartSearchPathLen, (slashId - m_nStartSearchPathLen)+1);
			}
			else { // file
				unsigned int slashesNum = sName.count('/');
				if (m_nStartSearchLevel <= slashesNum) {
					int nameLen = sName.length() - m_nStartSearchPathLen;
					sName = sName.mid(m_nStartSearchPathLen, nameLen);
					//qDebug() << "slashId:" << slashId << "oldName:" << sOldName << "name:" << sName << "id:" << pathLen << "len:" << nameLen;
				}
			}
			if (sName == sLastName)
				return;
			sLastName = sName;
			if (sName.isEmpty())
				return;
		}
		else
			return;
	}
	else { // level == 0
		slashId = sName.indexOf('/');
		if (slashId > 0) {
			targetLinkId = sName.indexOf("->");
			if (targetLinkId > 0)
				sTargetSymLink = sName.right(sName.length()-targetLinkId-3);
			else
				sTargetSymLink = "";
		}
	}
	if (sName.endsWith('/')) {
		isDir = true;
		sName.remove(sName.length()-1, 1); // remove ending slash
	}
	else
		isDir = false;

	if (slName.indexOf(sName) != -1)
		return;
	slName.append(sName);

	slLine = sLine.split(QRegExp("\\s+"));
	isSymLink = (slLine[PERM].at(0) == 'l');
	if (isSymLink) {
		sName = sName.left(sName.indexOf("->")-1);
		isDir = false;
	}
	sFullPath = m_sSearchHost + QDir::separator() + QFileInfo(sName).path();
// 		sName = QFileInfo(sLine).fileName();
	sName = sName.right(sName.length() - sName.lastIndexOf(QDir::separator()) - 1);
	sName = QString::fromLocal8Bit(sName.toLatin1().data());

	sDateTime = slLine[DATE] +" "+ slLine[TIME];
	slOwnerAndGroup = slLine[OWNER_GROUP].split('/');
	sOwner = slOwnerAndGroup[0];
	sGroup = slOwnerAndGroup[1];
	sMime  = MimeType::getFileMime(sName, isDir, isSymLink);

	pFileInfo = new FileInfo(
				sName, sFullPath, atoll(slLine[SIZE].toLatin1()), isDir, isSymLink,
				QDateTime::fromString(sDateTime, Qt::ISODate), slLine[PERM], sOwner, sGroup, sMime, sTargetSymLink
			);
	m_nFileCounter += (!isDir) ? 1 : 0;
	m_nDirCounter  += (isDir && !isSymLink) ? 1 : 0;

	if (isFileMatches(pFileInfo)) {
		m_processedFiles->append(pFileInfo);
		m_nMatchedFiles++;
		emit signalStatusChanged(Vfs::FOUND, m_sRequestingPanel);
		m_status = FINDING;
	}
	else
		delete pFileInfo;
}


} // namespace Vfs
