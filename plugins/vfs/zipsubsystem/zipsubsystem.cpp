/***************************************************************************
 *   Copyright (C) 2011 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDir>
#include <QTimer>
#include <QFileInfo>

//#include "zipsubsystemsettings.h"
#include "contextmenucmdids.h"
#include "systeminfohelper.h"
#include "pluginfactory.h"
#include "zipsubsystem.h"
#include "messagebox.h"
#include "settings.h"
#include "mimetype.h"

#include "preview/viewer.h"


namespace Vfs {

ZipSubsystem::ZipSubsystem() : LISTING_COLS(7), m_nNameColId(ID_of_COL_NAME)
{
	qDebug() << "ZipSubsystem::ZipSubsystem.";
	//setObjectName("ZipSubsystem");
	m_error  = NO_ERROR;
	m_status = UNCONNECTED;
	//ZipSubsystemSettings *zipSubsystemSettings = new ZipSubsystemSettings;

	m_pPluginData = new PluginData;
	m_pPluginData->name         = "zipsubsystem_plugin";
	m_pPluginData->version      = "0.4";
	m_pPluginData->description  = QObject::tr("Plugin handles ZIP archives.");
	m_pPluginData->plugin_class = "subsystem";
	m_pPluginData->mime_icon    = ""; // (used in configuration window, higher priority than own_icon)
	m_pPluginData->own_icon     = 0;  // QIcon pointer. (used in configuration window)
	m_pPluginData->mime_type    = "zipsubsystem"; // required only in starting moment, after that zip will be detected and property updated properly
	m_pPluginData->protocol     = "file";
	m_pPluginData->finding_files_fun        = 0; // ptr.to fun
	m_pPluginData->finding_inside_files_fun = 0; // ptr.to fun
// 	m_pPluginData->popup_actions   = 0; // QActionGroup pointer  (zipSubsystemSettings->popupActions())
	m_pPluginData->toolBar      = 0;  // QToolBar pointer      (zipSubsystemSettings->toolBar())
	m_pPluginData->configPage   = 0;  // QWidget pointer       (zipSubsystemSettings->configPage())

// 	init(); // initialized globaly (in MainWindow)
	m_bForceReopen = false;
	m_bCreateNewArchive = false;
	m_bOperationFinished = false;
	m_processedFiles = new FileInfoList;
	m_preProcessedFiles = new FileInfoList;
	m_processedFileInfo = NULL;

	m_pProcess = new QProcess;
	connect(m_pProcess, &QProcess::readyReadStandardOutput, this, &ZipSubsystem::slotReadStandardOutputAsyn);
	connect(m_pProcess, &QProcess::readyReadStandardError, this, &ZipSubsystem::slotReadStandardError);
	connect<void(QProcess::*)(int, QProcess::ExitStatus)>(m_pProcess, &QProcess::finished,  this, &ZipSubsystem::slotProcessFinished);

	connect(&m_searchingTimer, &QTimer::timeout, this, &ZipSubsystem::slotSearching);
}

ZipSubsystem::~ZipSubsystem()
{
	qDebug() << "ZipSubsystem::~ZipSubsystem. DESTRUCTOR";

// 	delete m_pPluginData; // removed in PluginFactory::~PluginFactory

	if (m_pProcess && m_pProcess->state() == QProcess::Running) {
		m_pProcess->terminate();
		m_pProcess->waitForFinished(3000);
	}
	delete m_pProcess;
	delete m_processedFiles;
	delete m_preProcessedFiles;
}


void ZipSubsystem::init( QWidget *pParent, bool bInvokeDetection )
{
	QString sFN = "ZipSubsystem::init.";
	m_currentCommand = NO_COMMAND;
	m_pParent = pParent;

	m_bOpenedArchive   = false;
	m_bWeighingArchive = false;
	m_nLevelInArch = 0; m_nLevelInArchCompl = 0;

	qDebug() << sFN;

	if (bInvokeDetection) {
		qDebug() << sFN << "Start detection for command line applications..."; // if success then member m_pPluginData->file_ext will be properly updated
		m_pPluginData->mime_type = "";
		m_sRequestingPanel_open = "leftPanel_1:init";
		QString sCmdName = "zip";
		if (findExecutable(sCmdName).isEmpty())
			qDebug() << sFN << sCmdName << "not found in PATH.";
		else {
			qDebug() << sFN << sCmdName << "found in PATH.";
			m_pPluginData->file_ext  = sCmdName;
			m_pPluginData->mime_type = "application/zip";
			PluginFactory::instance()->updateMimeConfig("zipsubsystem", "zipsubsystem", m_pPluginData->mime_type);
		}
	}
}

bool ZipSubsystem::handledOperation( SubsystemCommand sc )
{
	if (sc == CREATE_LINK || sc == RENAME)
		return false;

	return true;
}

void ZipSubsystem::notSupportedOp( const QString &sRequestingPanel )
{
	m_status = ERROR_OCCURED;  m_error = NOT_SUPPORTED;
	emit signalStatusChanged(m_status, sRequestingPanel);
}

FileInfo *ZipSubsystem::root( bool bEmptyName )
{
	static QString fakeRoot = "/";
	QFileInfo fi(fakeRoot);

	return new FileInfo(bEmptyName ? "" : "..", fakeRoot, fi.size(), true, false, fi.lastModified(), "----------", fi.owner(), fi.group(), "folder/up");
}

QString ZipSubsystem::getName( const QString &sLine ) const
{
	QString sName = sLine.right((sLine.length() - m_nNameColId) + 1);
	sName = QString::fromLocal8Bit(sName.toLatin1().data());
	return sName;
}

FileInfo *ZipSubsystem::fileInfo( const QString &sAbsFileName )
{
	qDebug() << "ZipSubsystem::fileInfo. sAbsFileDirName" << sAbsFileName;
	if (sAbsFileName.endsWith("/..") || m_slParsingBufferLine.isEmpty())
		return root();

	QString sFileDirName = QDir::cleanPath("/"+sAbsFileName);
	QString sFileDirNameTmp = sAbsFileName; //sFileDirName;
	if (sFileDirNameTmp.at(0) == '/') // need to corrent find
		sFileDirNameTmp.remove(0, 1);
	if (sFileDirNameTmp.isEmpty())
		sFileDirNameTmp = sAbsFileName.left(sAbsFileName.indexOf("/.."));
	if (sFileDirNameTmp == "/")
		return root(true); // empty name
	//qDebug() << "sFileDirNameTmp:" << sFileDirNameTmp+"/";
	QString sName, sLine;
	for (QStringList::const_iterator it = m_slParsingBufferLine.begin(); it != m_slParsingBufferLine.end(); ++it) {
		sLine = *it;
		sName = getName(sLine);
		if (sName == sFileDirNameTmp+QDir::separator())
			break;
	}
	//qDebug() << "sLine" << sLine << "sName" << sName;
	QStringList lineStrList = sLine.split(' ');
	//QString     sDateTime   = lineStrList[DATE] +" "+ lineStrList[TIME];
	QString     sDateTime = lineStrList[DATETIME];
	bool        isDir       = (sName.at(sName.length()-1) == '/');
	sFileDirName = QString::fromLocal8Bit(sFileDirName.toLatin1().data());
	QString     sMime       = MimeType::getFileMime(sFileDirName, isDir);
	QString     sExtMime    = sMime +":"+ MimeType::getMimeIcon(sMime);
	sName        = QString::fromLocal8Bit(sName.toLatin1().data());
	//QDateTime  dt = QDateTime::fromString(sDateTime, "yyyy-MM-dd HH:mm");
	QDateTime dt = QDateTime::fromString(sDateTime, "yyyyMMdd.HHmmss"); // format used by zipinfo/unzip -ZT (MMM needs add.effort to decode due to Qt translate name to locale)
	if (! dt.isValid())
		QDateTime::fromString(sDateTime, "MM-dd-yyyy HH:mm"); // US format
	FileInfo *item = new FileInfo(sName, m_sCurrentPath, atoll(lineStrList[SIZE].toLatin1()), isDir, false, dt, "----------", "?", "?", sExtMime);

	return item;
}


void ZipSubsystem::open( const URI &uri, const QString &sRequestingPanel, bool bCompleter )
{
	m_bCompleterMode = bCompleter;
	QString sFN = "ZipSubsystem::open.";
	if (m_bCompleterMode && m_currentUriCompleter.toString() == uri.toString()) {
		qDebug() << sFN << "Came path for already opened directory (QCompleter::splitPath issue).";
		return;
	}
	if (uri.host().isEmpty()) {
		qDebug() << sFN << "Cannot open archive due to empty host!";
		return;
	}
	if (m_pProcess->state() != QProcess::NotRunning) {
		qDebug() << sFN << "Process is already running (maybe in other tab was opened archive) or PROG_EXISTS not finished, currentCommand:" << toString(m_currentCommand);
		if (m_opQueue.first().command != VIEW) {
			if (m_currentCommand != PROG_EXISTS) {
				qDebug() << sFN << "  Adding OPEN command to queue.";
				m_opQueue << Operation(OPEN, uri, sRequestingPanel);
			}
		}
		return;
	}
	m_opQueue.clear();
	m_processedFiles->clear();

	qDebug() << sFN << "  host:" << uri.host() << "path:" << uri.path() << "last currentCommand:" << toString(m_currentCommand) << m_sRequestingPanel_open << "ForceReopen:" << m_bForceReopen << "RequestingPanel:" << sRequestingPanel;
// 	if (! archiveOpened(uri) || m_bForceReopen) {
// 		qDebug() << sFN << "Archive didn't buffer yet. Reopen current:" << uri.host();
		m_bOpenedArchive = false;
		m_currentCommand = (sRequestingPanel.contains("Viewer_")) ? VIEW : OPEN;
		m_bForceReopen = false;
		m_sRequestingPanel_open = sRequestingPanel;
// 	}
// 	else // when other action (FIND) was invoked previously then current command is different than LIST
// 		m_currentCommand = LIST;

	m_sHost = uri.host();
	m_sPath = uri.path();

	if (m_bCompleterMode)
		m_currentUriCompleter = uri;
	else
		m_currentUri = uri;

	m_bActionIsFinished = false;
	// below commented, because the most likely will be never called. The reason is m_currentCommand = OPEN; with commented condition placed above
// 	if (m_currentCommand == LIST) { // user navigates inside archive
// 		commandList();
// 		emit signalStatusChanged(m_status, m_sRequestingPanel_open);
// 		return;
// 	}

	if (! m_bOpenedArchive) {
		m_opQueue.clear();
		m_bCompleterMode ? m_nLevelInArch = 0 : m_nLevelInArchCompl = 0;
		m_opQueue	<< Operation(PROG_EXISTS, "which unzip", sRequestingPanel)
					<< Operation(m_currentCommand, "unzip -ZT \""+m_sHost+"\"", sRequestingPanel);
		slotRunNextOperation();
	}
	else {
		m_currentCommand = LIST;
		commandList();
	}
}

FileInfoList ZipSubsystem::listDirectory() const
{
	qDebug() << "ZipSubsystem::listDirectory. Path:" << m_sCurrentPath;

	//FileInfoList fiList;
	//fiList.append(root());
	//return fiList;

	return m_fiList;
}

void ZipSubsystem::commandList()
{
	m_bOpenedArchive = true;
	//qDebug() << "--- prepare to LIST (0). m_sPath:" << m_sPath << "m_sCurrentPath:" << m_sCurrentPath;
	m_sCurrentPath = "/"+m_sPath+"/";
	//qDebug() << "before clean - m_sCurrentPath" << m_sCurrentPath;
	m_sCurrentPath = QDir::cleanPath(m_sCurrentPath);

	bool goUp = (m_sPath.endsWith("/..") || m_sPath.endsWith("/../"));
	if (m_bCompleterMode) {
		if (goUp)
			m_nLevelInArchCompl -= 2;
		else
			m_nLevelInArchCompl = m_sCurrentPath.count(QDir::separator()) - ((m_sCurrentPath == QDir::separator()) ? 1 : 0);
	}
	else {
		if (goUp)
			m_nLevelInArch -= 2;
		else
			m_nLevelInArch = m_sCurrentPath.count(QDir::separator()) - ((m_sCurrentPath == QDir::separator()) ? 1 : 0);
	}
	QString sCurrentPathTmp = m_sCurrentPath + (m_sCurrentPath.endsWith(QDir::separator()) ? "" : "/"); // need to correct cut a name inside of parsingBufferedFilesList
	if (m_sPath.count('/') == 2 && goUp) // top level of archive - remove ending slash
		sCurrentPathTmp.remove(sCurrentPathTmp.length()-1, 1);
	if (! sCurrentPathTmp.isEmpty() && sCurrentPathTmp.at(0) == QDir::separator())
		sCurrentPathTmp.remove(0,1);

	qDebug() << "ZipSubsystem::commandList. m_sPath:" << m_sPath << "m_sCurrentPath:" << m_sCurrentPath << "sCurrentPathTmp:" << sCurrentPathTmp << "m_nLevelInArch:" << (m_bCompleterMode ? m_nLevelInArchCompl : m_nLevelInArch);

	uint lvlInArch = (m_bCompleterMode ? m_nLevelInArchCompl : m_nLevelInArch);
	processingParsedFilesList(lvlInArch, sCurrentPathTmp);
	if (m_status == ERROR_OCCURED)
		return;
	m_currentUri.setPath(m_sCurrentPath);
	m_error  = NO_ERROR;
	m_status = m_bCompleterMode ? LISTED_COMPL : LISTED;
}

void ZipSubsystem::parsingBufferedFilesList()
{
	QString sFN = "ZipSubsystem::parsingBufferedFilesList.";
	if (m_sOutBuffer.isEmpty()) {
		qDebug() << sFN << "INTERNAL_ERROR. Empty buffer: m_sOutBuffer! Exit.";
		m_status = ERROR_OCCURED;  m_error = INTERNAL_ERROR;
		emit signalStatusChanged(m_status, m_sRequestingPanel_open);
		m_opQueue.clear();
		return;
	}
	qDebug() << sFN;

	m_slParsingBufferLine = m_sOutBuffer.split('\n');
	qDebug() << sFN << "Buffer lines:" << m_slParsingBufferLine.count();
	// -- remove header and tail (2 and 2 lines)
	m_slParsingBufferLine.pop_front();
	m_slParsingBufferLine.pop_front();
	m_slParsingBufferLine.pop_back();
	m_slParsingBufferLine.pop_back();
	m_nFilesNumInArch = m_slParsingBufferLine.count();

	// buffer's test
/*	qDebug() << m_sOutBuffer;
	QString line;
	foreach (line, m_sParsingBufferLine) {
		qDebug() << line;
	}*/
}

void ZipSubsystem::processingParsedFilesList( unsigned int level, const QString &sInpPath )
{
	QString sFN = "ZipSubsystem::processingParsedFilesList.";
	if (m_slParsingBufferLine.isEmpty()) {
		qDebug() << sFN << "INTERNAL_ERROR. Empty buffer: m_slParsingBufferLine! Exit.";
		m_status = ERROR_OCCURED;  m_error = INTERNAL_ERROR;
		emit signalStatusChanged(m_status, m_sRequestingPanel_open);
		m_opQueue.clear();
		return;
	}
	bool isDir, bFoundTopDir = false;
	unsigned int nSlashesNum;
	QStringList lineStrList;
	QString sMime, sLine, sName, sDateTime;
	FileInfo *pFileInfo;

	qDebug() << sFN << "Level:" << level << "dirName:" << sInpPath; // << "m_sParsingBufferLine.count:" << m_sParsingBufferLine.count();
	// -- insert top item
	m_fiList.clear();
	m_fiList.append(root());

	foreach (sLine, m_slParsingBufferLine) {
		sName = getName(sLine);
		isDir = sName.endsWith('/');
		nSlashesNum = sName.count('/');
		if (isDir && nSlashesNum == 1)
			bFoundTopDir = true; // required for archives where top dir doesn't exist as separate entry

		if (isDir && nSlashesNum > 0) {
			nSlashesNum--;
		}
		if (level != nSlashesNum)
			continue;
		if (level > 0) {
			if (! sName.startsWith(sInpPath))
				continue;
			sName.remove(0, sInpPath.length());
		}
		if (m_bCompleterMode && ! isDir)
			continue;

		lineStrList = sLine.split(QRegExp("\\s+"), Qt::SkipEmptyParts);

		sName = QString::fromLocal8Bit(sName.toLatin1().data());
		if (sName.endsWith('/'))
			sName.remove(sName.length()-1, 1); // remove ending slash
		//sDateTime = lineStrList[DATE] +" "+ lineStrList[TIME]; // format used by zipinfo/unzip -Z/unzip -v
		sDateTime = lineStrList[DATETIME];
		sMime = MimeType::getFileMime(sName, isDir);
		//QDateTime dt = QDateTime::fromString(sDateTime, "yy-MMM-dd HH:mm"); // format used by zipinfo/unzip -Z
		QDateTime dt = QDateTime::fromString(sDateTime, "yyyyMMdd.HHmmss"); // format used by zipinfo/unzip -ZT (MMM needs add.effort to decode due to Qt translate name to locale)
		if (! dt.isValid()) {
			dt = QDateTime::fromString(sDateTime, "yyyy-MM-dd HH:mm");
			if (! dt.isValid())
				dt = QDateTime::fromString(sDateTime, "MM-dd-yyyy HH:mm"); // US format
		}
		pFileInfo = new FileInfo(sName, sInpPath, atoll(lineStrList[SIZE].toLatin1()), isDir, false, dt, lineStrList[PERM], "?", "?", sMime);
		m_fiList.append(pFileInfo);
	} // foreach
	// -- adding fake top directory
	if (! bFoundTopDir && m_nLevelInArch == 0 && nSlashesNum > 0) {
		QString sTopDirName = m_slParsingBufferLine.at(0).left(m_slParsingBufferLine.at(0).indexOf('/'));
		sName = getName(sTopDirName);
		m_fiList.append(new FileInfo(sName, sInpPath, 0, true, false, QDateTime::fromTime_t(0), "dr-xr-xr-x", "?", "?", "folder"));
	}

	m_bCompleterMode ? m_nLevelInArchCompl++ : m_nLevelInArch++;
}


void ZipSubsystem::openWith( FileInfoToRowMap *selectionMap, const URI &uri, const QString &sRequestingPanel )
{
	int nResult = -1;
	QString sFN = "ZipSubsystem::openWith.";
	qDebug() << sFN << "uri" << uri.toString() << "m_currentUri:" << m_currentUri.toString() << "absFileName:" << selectionMap->constBegin().key()->absoluteFileName();

	if (m_bConfirmExtractingForOpenWith) {
		int nSelFilesNum = selectionMap->count();
		QString sAbsFileName = m_currentUri.toString() + QDir::separator() + selectionMap->constBegin().key()->fileName();
		QString sQuestion = (nSelFilesNum > 1) ? tr("Extract and open files?") : tr("Do you want to extract and open given file?");
		QString sFImsg = (nSelFilesNum > 1) ? QString(tr("Selected %1 items")).arg(nSelFilesNum) : sAbsFileName;
		nResult = MessageBox::yesNo(m_pParent, tr("Open with..."), sFImsg, sQuestion,
									m_bConfirmExtractingForOpenWith,
									MessageBox::Yes, // default focused button
									tr("Always ask for confirmation")
		);
		if (nResult == 0) // user closed dialog
			return;
		Settings settings;
		settings.update("archive/ConfirmExtractingForOpenWith", m_bConfirmExtractingForOpenWith);
	}
	else
		nResult = MessageBox::Yes;

	if (nResult == MessageBox::Yes) {
		if (! archiveOpened(uri))
			m_sHost = uri.host();

		m_error = NO_ERROR;
		m_status = EXTRACTING;
		m_currentCommand = OPEN_WITH_CMD;
		m_bOperationFinished = false;
		m_sRequestingPanel_open = sRequestingPanel;
		m_processedFiles->clear();
		m_opQueue.clear();

		if (m_sTmpDir.isEmpty()) {
			qDebug() << sFN << "Internal ERROR. Working directory is not set!";
			m_error = CANNOT_EXTRACT;
			emit signalStatusChanged(Vfs::OPEN_WITH, sRequestingPanel);
			return;
		}

		long long nOpenedFilesWeigh = 0;
		QString sFullFileNamesToExtract, sFileNameNoHost, sFileName;
		FileInfo *fi;
		FileInfoToRowMap::const_iterator it = selectionMap->constBegin();
		while (it != selectionMap->constEnd()) {
			fi = it.key();
			sFileName = fi->absoluteFileName();
			sFileNameNoHost = sFileName.remove(m_sHost);
			if (sFileNameNoHost.startsWith('/'))
				sFileNameNoHost.remove(0, 1);
			sFullFileNamesToExtract += "\"" + sFileNameNoHost + "\"";
			m_processedFiles->append(fi);
			nOpenedFilesWeigh += fi->size();
			++it;
		}
		long long freeBytes, totalBytes;
		Vfs::getStatFS(m_sTmpDir, freeBytes, totalBytes);
		if (freeBytes < nOpenedFilesWeigh) { // m_nSourceFileSize is init.into 'initPutFile()'
			m_error = NO_FREE_SPACE;
			emit signalStatusChanged(Vfs::ERROR_OCCURED, sRequestingPanel);
			return;
		}
// 		unzip -oj /home/piotr/test_archives/lxterminal-0.1.6.zip "lxterminal-0.1.6/depcomp" "lxterminal-0.1.6/COPYING" -d /tmp/qtcmd2/  # -o overwrite existing file
		QString sCmd = "unzip -oj \"" + m_sHost + "\" " + sFullFileNamesToExtract + " -d " + m_sTmpDir;
		m_opQueue << Operation(PROG_EXISTS, "which unzip", sRequestingPanel);
		m_opQueue << Operation(m_currentCommand, sCmd, sRequestingPanel);
		slotRunNextOperation();
	}
	else {
		m_bOperationFinished = true;
		emit signalStatusChanged(Vfs::OPEN_WITH, sRequestingPanel);
	}
}

void ZipSubsystem::readFileToViewer( const QString &sHost, FileInfo *pFileInfo, QByteArray &baBuffer, int &nReadBlockSize, int nReadingMode, const QString &sRequestingPanel )
{
	QString sFN = "ZipSubsystem::readFileToViewer.";
	qDebug() << sFN << "host:" << sHost << "absFileName:" << pFileInfo->absoluteFileName() << " method:" << nReadingMode << "sRequestingPanel:" << sRequestingPanel;
	m_error = NO_ERROR;
	m_currentCommand = READ_FILE_TO_VIEW;

	// -- check if temporary directory is set
	if (m_sTmpDir.isEmpty()) {
		qDebug() << sFN << "Internal ERROR. Working directory is not set!";
		m_error = CANNOT_EXTRACT;
		emit signalStatusChanged(Vfs::OPEN_WITH, sRequestingPanel);
		return;
	}
	// -- check readability
	if (nReadingMode == Preview::Viewer::GET_READABILITY) { // (3) always readable, because file will be extracted to tmp directory
		nReadBlockSize = 1; // need for FileViewer check
		m_processedFileInfo = pFileInfo;
		emit signalStatusChanged((m_status=Vfs::READY_TO_READ), sRequestingPanel); // unmark item
		return;
	}
	long long nInputFileWeigh = pFileInfo->size();
	long long freeBytes, totalBytes;
	Vfs::getStatFS(m_sTmpDir, freeBytes, totalBytes);
	if (freeBytes < nInputFileWeigh) {
		m_error = NO_FREE_SPACE;
		emit signalStatusChanged(Vfs::ERROR_OCCURED, sRequestingPanel);
		return;
	}
	// -- prepare to extract file
	m_sHost = sHost;
	m_sRequestingPanel_open = sRequestingPanel;

	QString sFileName = pFileInfo->absoluteFileName();
	QString sFileNameNoHost = sFileName.remove(m_sHost);
	if (sFileNameNoHost.startsWith('/'))
		sFileNameNoHost.remove(0, 1);
	//qDebug() << "ZipSubsystem::readFileToViewer.  --- sFileNameNoHost:" << sFileNameNoHost;
	m_processedFiles->clear();
	m_processedFiles->append(pFileInfo);

	// need to remove file before extract it due to extracting process might hang
	QString sFileToRemove = sFileNameNoHost;
	sFileToRemove.replace(sFileToRemove.left(sFileToRemove.lastIndexOf('/')+1), m_sTmpDir);
	QFile(sFileToRemove).remove();

	int nResult = -1;
	if (m_bConfirmExtractingForView) {
		QString sAbsFileName = m_sHost + QDir::separator() + pFileInfo->absoluteFileName(); // m_currentUri.toString() points to current dir in archive including host
		QString sQuestion = tr("Do you want to extract and open given file?");
		nResult = MessageBox::yesNo(m_pParent, tr("View file"), sAbsFileName, sQuestion,
									m_bConfirmExtractingForView,
									MessageBox::Yes, // default focused button
									tr("Always ask for confirmation")
		);
		if (nResult == 0) // user closed dialog
			return;
		Settings settings;
		settings.update("archive/ConfirmExtractingForView", m_bConfirmExtractingForView);
	}
	else
		nResult = MessageBox::Yes;

	sFileNameNoHost = "\"" + sFileNameNoHost + "\" ";
	m_bOperationFinished = false;
	m_status = EXTRACTING;
	m_opQueue.clear();
	if (nResult == MessageBox::Yes) {
 		//unzip -oj /home/piotr/test_archives/lxterminal-0.1.6.zip "lxterminal-0.1.6/depcomp" "lxterminal-0.1.6/COPYING" -d /tmp/qtcmd2/  # -o overwrite existing file
		QString sCmd = "unzip -oj \"" + m_sHost + "\" " + sFileNameNoHost + " -d " + m_sTmpDir;
		m_opQueue << Operation(PROG_EXISTS, "which unzip", sRequestingPanel);
		m_opQueue << Operation(m_currentCommand, sCmd, sRequestingPanel);
		slotRunNextOperation();
	}
	else {
		m_bOperationFinished = true;
		emit signalStatusChanged(Vfs::NOT_READY_TO_READ, sRequestingPanel);
	}
}

void ZipSubsystem::processingOpenWithCmd()
{
	m_status = OPEN_WITH;
	m_error  = NO_ERROR;
	QStringList sParsingBufferLine(m_sOutBuffer.split('\n'));
	sParsingBufferLine.pop_front(); // remove front line with archive name
// 	qDebug() << "sParsingBufferLine:\n" << sParsingBufferLine;
	int nFilesNumInArch = sParsingBufferLine.count() - 1; // because in buffer first line this is archive name
	m_bOperationFinished = (nFilesNumInArch == m_processedFiles->count());
	qDebug() << "ZipSubsystem::processingOpenWithCmd. OPEN_WITH_CMD command, filesNumInArch:" << nFilesNumInArch << "operationFinishedStatus:" << m_bOperationFinished;
}


void ZipSubsystem::initWeighing( bool bResetCounters, const QString &sRequestingPanel )
{
	qDebug() << "ZipSubsystem::initWeighing. currentCommand:" << toString(m_currentCommand);
	if (m_currentCommand != WEIGH_ARCH)
		m_processedFiles->clear();
	m_totalWeight = 0;
	m_nTotalItems = 0;
	if (bResetCounters) {
		m_nDirCounter = 0;
		m_nFileCounter = 0;
		m_nSymlinksCounter = 0;
	}
	m_error  = NO_ERROR;
	m_status = WEIGHING;
	m_bCreateNewArchive = false;
	m_bOperationFinished = false;
	emit signalStatusChanged(m_status, sRequestingPanel);
}

void ZipSubsystem::weigh( FileInfoToRowMap *selectionMap, const URI &uri, const QString &sRequestingPanel, bool bWeighingAsSubCmd, bool bResetCounters )
{
	// -- check if opened archive is cached
	if (! archiveOpened(uri) && ! bWeighingAsSubCmd) {
		qDebug() << "ZipSubsystem::weigh. Archive didn't buffer yet. Need to reopen.";
		open(uri, sRequestingPanel);
		m_opQueue.clear();
		m_opQueue << Operation(WEIGH, selectionMap, uri, sRequestingPanel, bResetCounters);
		slotRunNextOperation();
		return;
	}
	m_sRequestingPanel = sRequestingPanel;
	initWeighing(bResetCounters, sRequestingPanel);

	FileInfoToRowMap::const_iterator it = selectionMap->constBegin();
	while (it != selectionMap->constEnd()) {
		m_processedFiles->append(it.key());
		processingWeighCmd(it.key());
		++it;
	}

// 	if (! bWeighingAsSubCmd) { // commented due to: properties dialog needs this signal
		m_bOperationFinished = true;
		m_status = WEIGHED;
		emit signalStatusChanged(m_status, sRequestingPanel);
// 	}
}

void ZipSubsystem::weighArchive( const QString &sAbsFileName, const QString &sRequestingPanel, bool bRunNextOperation )
{
	qDebug() << "ZipSubsystem::weighArchive." << sRequestingPanel << m_sRequestingPanel;
	initWeighing(true, m_sRequestingPanel);

	m_nLevelInArch = 0;
	m_opQueue.prepend(Operation(OPEN, "unzip -ZT \""+sAbsFileName+"\"", sRequestingPanel));
	m_opQueue.prepend(Operation(PROG_EXISTS, "which unzip", sRequestingPanel));

	if (bRunNextOperation)
		slotRunNextOperation();
}

void ZipSubsystem::weighItem( FileInfo *pFileInfo, const URI &uri, const QString &sRequestingPanel, bool bResetCounters )
{
	if (uri.host().isEmpty()) { // the most likely archive file
		m_opQueue.clear();
		m_opQueue << Operation(WEIGH_ARCH, pFileInfo, "", sRequestingPanel); // thanks that flag m_bWeighingArchive will be properly set
		slotRunNextOperation();
		return;
	}
	// -- check if opened archive is cached
	if (! archiveOpened(uri)) {
		qDebug() << "ZipSubsystem::weighItem. Archive didn't buffer yet. Need to reopen.";
		open(uri, sRequestingPanel);
		m_opQueue << Operation(WEIGH_ITEM, pFileInfo, uri, sRequestingPanel, bResetCounters);
		slotRunNextOperation();
		return;
	}
	initWeighing(bResetCounters, sRequestingPanel);

	m_processedFiles->append(pFileInfo);
	processingWeighCmd(pFileInfo);

	m_bOperationFinished = true;
	m_status = WEIGHED;
	emit signalStatusChanged(m_status, sRequestingPanel);
}

void ZipSubsystem::processingWeighCmd( FileInfo *pFileInfo )
{
	if (pFileInfo == NULL && ! m_bWeighingArchive) {
		qDebug() << "ZipSubsystem::processingWeighCmd. empty fileInfo";
		return;
	}
	bool isDir;
	unsigned int slashesNum;
	unsigned int level = m_nLevelInArch;
	QStringList lineStrList;
	QString sLine, sName;
	QString sPath;

	if (! m_bWeighingArchive)
		sPath = pFileInfo->absoluteFileName();
	if (sPath.startsWith(QDir::separator()))
		sPath.remove(0, 1);

	if (! m_bWeighingArchive) {
		if (pFileInfo->isDir()) {
			sPath += QDir::separator();
			qDebug() << "ZipSubsystem::processingWeighCmd. Path:" << sPath << "level:" << level;
		}
		else
			level--;
	}

	foreach (sLine, m_slParsingBufferLine) {
		sName = getName(sLine);
		isDir = (sName.at(sName.length()-1) == '/');
		slashesNum = sName.count('/');

		if (isDir && slashesNum > 0)
			slashesNum--;
		if (slashesNum < level)
			continue;

		if (! sName.startsWith(sPath))
			continue;

		lineStrList = sLine.split(QRegExp("\\s+"), Qt::SkipEmptyParts);

		if (! isDir) {
			m_totalWeight += atoll(lineStrList[SIZE].toLatin1());
			m_nFileCounter++;
		}
		else
			m_nDirCounter++;

		if (! m_bWeighingArchive) {
			if (! pFileInfo->isDir())
				break;
		}
	} // foreach

	if (! m_bWeighingArchive) {
		if (pFileInfo->isDir())
			m_nDirCounter++; // main dir
	}

	m_nTotalItems = (m_nFileCounter + m_nDirCounter); // skip m_nSymlinksCounter because were counted as files
}

void ZipSubsystem::getWeighingResult( qint64 &weight, qint64 &realWeight, uint &files, uint &dirs )
{
	weight = m_totalWeight;
	realWeight = -1;
	files  = m_nFileCounter;
	dirs   = m_nDirCounter;
}


void ZipSubsystem::initRemoving( const QString &sRequestingPanel )
{
	if (m_status == WEIGHED) {
		qDebug() << "ZipSubsystem::initRemoving(). WEIGHED - totalWeigh:" << m_totalWeight << FileInfo::bytesRound(m_totalWeight) << "files:" << m_nFileCounter << "dirs:" << m_nDirCounter << "all items:" << m_nTotalItems;
	}
	m_opQueue.clear();
	m_processedFiles->clear();
	m_OpTotalWeight  = 0;
	m_nOpDirCounter  = 0;
	m_nOpFileCounter = 0;
// 	m_nOpSymlinksCounter = 0; // ZIP can't recoginze symlinks
	m_currentCommand = REMOVE;
	m_status = REMOVING;
	m_bCreateNewArchive = false;
	m_sRequestingPanel = sRequestingPanel; // used in slotProcessFinished
	emit signalStatusChanged(m_status, sRequestingPanel);
}

void ZipSubsystem::remove( FileInfoToRowMap *selectionMap, const URI &uri, bool weighBefore, const QString &sRequestingPanel, bool bForceRemoveAll )
{
	QString sFN = "ZipSubsystem::remove.";
	if (selectionMap == NULL) {
		qDebug() << sFN << "Empty selectionMap.";
		m_status = ERROR_OCCURED;  m_error = INTERNAL_ERROR;
		emit signalStatusChanged(m_status, sRequestingPanel);
		return;
	}
	int selectedItemsNum = selectionMap->size();
	qDebug() << sFN << "Selected items:" << selectedItemsNum << "weighBefore:" << weighBefore;
	if (selectedItemsNum == 0) {
		qDebug() << sFN << "No selected items.";
		return;
	}

	// -- start remove operation
	initWeighing(true, m_sRequestingPanel);
	if (weighBefore) {
		weigh(selectionMap, uri, sRequestingPanel);
		emit signalCounterProgress(m_nDirCounter,  DIR_COUNTER, true);
		emit signalCounterProgress(m_nFileCounter, FILE_COUNTER, true);
		emit signalCounterProgress(m_totalWeight,  WEIGH_COUNTER, true);
	}

	initRemoving(sRequestingPanel);

	QString sFullFileNamesToRemove;
	FileInfo *pFileInfo;
	FileInfoToRowMap::const_iterator it = selectionMap->constBegin();
	while (it != selectionMap->constEnd()) {
		pFileInfo = it.key();
		sFullFileNamesToRemove += "\"" + pFileInfo->absoluteFileName();
		if (pFileInfo->isDir())
			sFullFileNamesToRemove += QDir::separator() + QString("*");
		sFullFileNamesToRemove += "\" ";
		m_processedFiles->append(pFileInfo);
		++it;
	}
	// zip -d lxterminal-0.1.6.zip lxterminal-0.1.6/man/*  (dir)
	// zip -d lxterminal-0.1.6.zip lxterminal-0.1.6/NEWS   (file)
	QString sCmd = "zip -d \"" + m_sHost + "\" " + sFullFileNamesToRemove;
	m_opQueue << Operation(PROG_EXISTS, "which zip", sRequestingPanel);
	m_opQueue << Operation(m_currentCommand, sCmd, sRequestingPanel);
	slotRunNextOperation();
}

void ZipSubsystem::processingRemoveCmd()
{
// 	qDebug() << "ZipSubsystem::processingRemoveCmd.\n" << m_sOutBuffer;
	if (m_sOutBuffer.contains("zip error")) {
		qDebug() << m_sOutBuffer;
		m_status = ERROR_OCCURED;
		m_error  = CANNOT_REMOVE;
		return;
	}
	QStringList slLinesBuffer = m_sOutBuffer.split('\n', Qt::SkipEmptyParts);
	if (slLinesBuffer.size() == 0)
		return;

	int nLineId;
	QString sName, sInpLine, sLine;
	QStringList slSplitLine;
	bool bIsDir, bZipFileEmpty;

	for (int i=0; i<slLinesBuffer.count(); i++) {
		sInpLine = slLinesBuffer.at(i);
		bZipFileEmpty = sInpLine.startsWith(("\t"));
		if (bZipFileEmpty)
			continue;
		sLine = sInpLine.right(sInpLine.length() - sInpLine.indexOf(": ") - 2); // remove "deleting: "
		//qDebug() << "sLine" << sLine;
		emit signalNameOfProcessedFile(sLine);
		sLine.insert(0, ".* ");
		nLineId = m_slParsingBufferLine.indexOf(QRegExp(sLine));
		if (nLineId < 0)
			continue;

		sLine = m_slParsingBufferLine[nLineId];
		sName = getName(sLine);
		bIsDir = sName.endsWith('/');
		slSplitLine = sLine.split(QRegExp("\\s+"), Qt::SkipEmptyParts);

		if (bIsDir)
			emit signalCounterProgress(++m_nOpDirCounter, DIR_COUNTER, false);
		else {
			m_OpTotalWeight += atoll(slSplitLine[SIZE].toLatin1());
			emit signalCounterProgress(++m_nOpFileCounter, FILE_COUNTER, false);
			emit signalCounterProgress(m_OpTotalWeight, WEIGH_COUNTER, false);
		}
		m_nOpTotalFiles = m_nOpFileCounter + m_nOpDirCounter;
		emit signalTotalProgress((100 * m_nOpTotalFiles) / m_nTotalItems, m_totalWeight);
	}
	if (bZipFileEmpty)
		MessageBox::msg(m_pParent, MessageBox::WARNING, tr("zip file empty"), m_sHost);
}

void ZipSubsystem::processingArchiveCmd()
{
// 	qDebug() << "ZipSubsystem::processingArchiveCmd.\n" << m_sOutBuffer;
	QStringList slLinesBuffer = m_sOutBuffer.split('\n', Qt::SkipEmptyParts);
	if (slLinesBuffer.size() == 0)
		return;

	FileInfo *pFI;
	int nProcessedFiles;
	QString sFileName, sSubDirFileName;
	bool isDir = false;

	for (int i=0; i<slLinesBuffer.count(); i++) {
		sFileName = slLinesBuffer.at(i);
		if (m_bCreateNewArchive) {
			if (! sFileName.startsWith("  adding:"))
				continue;
			sFileName.replace(": ", ": /");
		}
		sFileName.remove(0, sFileName.indexOf(QDir::rootPath())); // remove: "added: "
		sFileName = sFileName.trimmed();
		if (m_bCreateNewArchive)
			sFileName = sFileName.left(sFileName.indexOf('\t'));
		isDir = sFileName.endsWith('/');
		foreach (pFI, *m_preProcessedFiles) {
			if (sFileName.indexOf(pFI->absoluteFileName()) != -1) {
				if (m_bCreateNewArchive)
					isDir = sFileName.endsWith('/');
				else
					isDir = pFI->isDir() && ! pFI->isSymLink();
				break;
			}
		}
		emit signalNameOfProcessedFile(sFileName);
		if (isDir) {
			if (m_sLastDirName != pFI->fileName()) { // maybe:  || m_bCreateNewArchive
				emit signalCounterProgress(++m_nOpDirCounter, DIR_COUNTER, false);
				if (! m_bCreateNewArchive)
					m_sLastDirName = pFI->fileName();
			}
			if (! m_bCreateNewArchive) {
				sSubDirFileName = sFileName.right(sFileName.length() - sFileName.indexOf(pFI->fileName()+"/"));
				if (sSubDirFileName.length() > m_sLastDirName.length())
					m_slAddedFilesFromSubdirectories.append(sFileName+"|"+sSubDirFileName);
			}
		}
		else {
			emit signalCounterProgress(++m_nOpFileCounter, FILE_COUNTER, false);
		}
		nProcessedFiles = m_nOpFileCounter + m_nOpDirCounter;
		if (m_nTotalItems > 0) {
			emit signalDataTransferProgress(nProcessedFiles, m_nTotalItems, 0, false); // last two args are not relevant
			emit signalTotalProgress((100 * nProcessedFiles) / m_nTotalItems, m_totalWeight);
		}
	}
}

void ZipSubsystem::processingExtractCmd()
{
// 	qDebug() << "ZipSubsystem::processingExtractCmd.\n" << m_sOutBuffer;
	QStringList slLinesBuffer = m_sOutBuffer.split('\n', Qt::SkipEmptyParts);
	if (slLinesBuffer.size() == 0)
		return;

	int nProcessedFiles;
	QString sFileName;

	if (m_whatExtract == Vfs::EXTRACT_ARCHIVE) {
		// NOTE. For win zip archies ver.2.5 (for 2.0 is OK) containing couple of directories on top
		// on output there is no directories, so progress might be not correct, despite archive was extracted correctly
		FileInfo *pFileInfo;
		bool bIsDir, bContainsSeparator;
		QString sAbsCurrentPath = QFileInfo(m_sLastShellCmd.split('"').at(3)).path();
		sAbsCurrentPath += QDir::separator();

		for (int i=0; i<slLinesBuffer.count(); i++) {
			sFileName = slLinesBuffer.at(i);
			if (sFileName.startsWith("Archive:"))
				continue;
			bIsDir = (sFileName.endsWith('/'));
			if (m_processedFiles->size() > 1) // extrating more than 1 archive
				emit signalDataTransferProgress(++m_nOpFileCounter, m_nTotalItems, 0, false); // last two args are not relevant
//			else // comment was required to correctly extract multiply archives into current directory ("Extract here" operation)
			{ // extrating only 1 archive - need to process all files
				sFileName.remove(0, sFileName.indexOf(QDir::rootPath())); // remove leading: "inflating:"
				if (m_processedFiles->size() == 1) {
					emit signalNameOfProcessedFile(sFileName);
					if (bIsDir)
						emit signalCounterProgress(++m_nOpDirCounter, DIR_COUNTER, false);
					else
						emit signalCounterProgress(++m_nOpFileCounter, FILE_COUNTER, false);
					nProcessedFiles = m_nOpFileCounter + m_nOpDirCounter;
					emit signalDataTransferProgress(nProcessedFiles, m_nTotalItems, 0, false); // last two args are not relevant
					emit signalTotalProgress((100 * nProcessedFiles) / m_nTotalItems, m_totalWeight);
				}
			}
			// -- helpful in the update data model after "Extract here" operation
			sFileName = sFileName.remove(0, sAbsCurrentPath.length());
			if (bIsDir)
				sFileName = sFileName.left(sFileName.length()-1);
			bContainsSeparator = sFileName.contains(QDir::separator());
			if ((bIsDir && ! bContainsSeparator) || ! bContainsSeparator) { // directories and files from top level
				QFileInfo fi(sAbsCurrentPath+sFileName);
				if (fi.exists()) {
					QString sPerm = permissionsAsString(fi.absoluteFilePath());
					QString sMime = MimeType::getFileMime(fi.absoluteFilePath(), fi.isDir(), fi.isSymLink());
					qint64  nSize = fi.isSymLink() ? fi.symLinkTarget().length() : fi.size();
					QString sSymLinkTarget = fi.symLinkTarget();
					pFileInfo = new FileInfo(sFileName, fi.path(), nSize, fi.isDir(), fi.isSymLink(), fi.lastModified(), sPerm, fi.owner(), fi.group(), sMime, sSymLinkTarget);
					m_preProcessedFiles->append(pFileInfo);
				}
			}
		}
	}
	else { // Vfs::EXTRACT_FILES
		bool isDir;
		FileInfo *pFI;
		for (int i=0; i<slLinesBuffer.count(); i++) {
			sFileName = slLinesBuffer.at(i);
			if (sFileName.startsWith("Archive:"))
				continue;
			sFileName.remove(0, sFileName.indexOf(QDir::rootPath())); // remove: "  inflating: " or "   creating: "
			sFileName = sFileName.trimmed();
			foreach (pFI, *m_processedFiles) {
				if (sFileName.indexOf(QDir::separator()+pFI->fileName()+QDir::separator()) != -1) { // maybe it's dir
					isDir = pFI->isDir() && ! pFI->isSymLink();
					break;
				}
				else
				if (sFileName.indexOf(QDir::separator()+pFI->fileName()) != -1) { // is file
					isDir = false;
					break;
				}
			}
			emit signalNameOfProcessedFile(sFileName);
			if (isDir) {
				if (m_sLastDirName != pFI->fileName()) {
					emit signalCounterProgress(++m_nOpDirCounter, DIR_COUNTER, false);
					m_sLastDirName = pFI->fileName();
				}
				else  // the most likely file from inside of extracting directory
					emit signalCounterProgress(++m_nOpFileCounter, FILE_COUNTER, false);
			}
			else {
				emit signalCounterProgress(++m_nOpFileCounter, FILE_COUNTER, false);
			}
			nProcessedFiles = m_nOpFileCounter + m_nOpDirCounter;
			emit signalDataTransferProgress(nProcessedFiles, m_nTotalItems, 0, false); // last two args are not relevant
			emit signalTotalProgress((100 * nProcessedFiles) / m_nTotalItems, m_totalWeight);
		}
	}
}


void ZipSubsystem::initSearching()
{
	m_processedFiles->clear();
	m_nMatchedFiles = 0;
	m_nFileCounter = 0;
	m_nDirCounter = 0;
	m_status = FINDING;
	m_error  = NO_ERROR;
	m_currentCommand = FIND;
	m_sSearchingResult = "";
	m_bOperationFinished = false;
	m_bCreateNewArchive = false;
	m_findRegExpLst.clear();
	emit signalStatusChanged(m_status, m_sRequestingPanel); // update status bar
}

void ZipSubsystem::searchFiles( FileInfoToRowMap *pSelectionMap, const URI &uri, const FindCriterion &fcFindCriterion, const QString &sRequestingPanel )
{
	if (pSelectionMap == NULL) {
		qDebug() << "ZipSubsystem::searchFiles. Empty selectionMap.";
		return;
	}
	if (fcFindCriterion.isEmpty()) {
		qDebug() << "ZipSubsystem::searchFiles. Empty findCriterion.";
		return;
	}
	// -- check if opened archive is cached
	if (! archiveOpened(uri)) {
		qDebug() << "ZipSubsystem::searchFiles. Archive didn't cache yet. Need to reopen.";
		open(uri, sRequestingPanel);
		m_opQueue << Operation(FIND, NULL, "", sRequestingPanel);
		slotRunNextOperation();
		return;
	}

	qDebug() << "ZipSubsystem::searchFiles";
	m_findCriterion = fcFindCriterion;
	m_sRequestingPanel = sRequestingPanel;

	initSearching();
	QStringList slNameLst = m_findCriterion.nameLst.split('|');
	foreach (QString sName, slNameLst)
		m_findRegExpLst.append(QRegExp(sName, m_findCriterion.caseSensitivity, m_findCriterion.patternSyntax));
	startSearching(pSelectionMap, uri);
}

void ZipSubsystem::startSearching( FileInfoToRowMap *, const URI &uri )
{
	qDebug() << "ZipSubsystem::startSearching. -- uri.path (start path):" << uri.path();
	// TODO: include selectionMap in searching
	m_sSearchHost = uri.host();
	m_sStartSearchPath = uri.path().right(uri.path().length()-1);
	if (m_sStartSearchPath.isEmpty())
		m_sStartSearchPath = QDir::separator();

	m_nSearchedLines = -1;
	m_searchingTimer.start(0);
}

bool ZipSubsystem::isFileMatches( FileInfo *pFileInfo )
{
	bool matchFileOwner = true;
	bool matchFileGroup = true;
	bool matchTime = true;
	bool matchSize = true;
	bool matchName = false;
// 	bool matchName = m_findRegExp.exactMatch(fileInfo->fileName()); // check name matching
	foreach (QRegExp p, m_findRegExpLst) {
		if ((matchName=p.exactMatch(pFileInfo->fileName()))) // check if given name matches to pattern
			break;
	}
	if (! matchName)
		return false;

	if (m_findCriterion.checkFileOwner)
		matchFileOwner = (pFileInfo->owner() == m_findCriterion.owner);
	if (m_findCriterion.checkFileGroup)
		matchFileGroup = (pFileInfo->group() == m_findCriterion.group);

	if (m_findCriterion.checkTime) {
		QDateTime fileTime = pFileInfo->lastModified();
		matchTime = (fileTime >= m_findCriterion.firstTime && fileTime <= m_findCriterion.secondTime);
	}

	qint64 fileSize = pFileInfo->size();
	if (    m_findCriterion.checkFileSize != FindCriterion::NONE) {
		if (m_findCriterion.checkFileSize == FindCriterion::EQUAL)
			matchSize = (fileSize == m_findCriterion.fileSize);
		else if (m_findCriterion.checkFileSize == FindCriterion::ATLEAST)
			matchSize = (fileSize >= m_findCriterion.fileSize);
		else if (m_findCriterion.checkFileSize == FindCriterion::MAXIMUM)
			matchSize = (fileSize <= m_findCriterion.fileSize);
		else if (m_findCriterion.checkFileSize == FindCriterion::GREAT_THAN)
			matchSize = (fileSize >  m_findCriterion.fileSize);
		else if (m_findCriterion.checkFileSize == FindCriterion::LESS_THAN)
			matchSize = (fileSize <  m_findCriterion.fileSize);
	}

	return (matchName && matchFileOwner && matchFileGroup && matchTime && matchSize);
}

QString ZipSubsystem::searchingResult()
{
	m_sSearchingResult = QString("%1 %2 %3").arg(m_nMatchedFiles).arg(m_nFileCounter).arg(m_nDirCounter);
	return m_sSearchingResult;
}


void ZipSubsystem::copyFiles( FileInfoToRowMap *pSelectionMap, const QString &sTargetPath, bool bMove, bool bWeighBefore, const QString &sRequestingPanel )
{
	URI uri(sTargetPath);
	if (! uri.isValid()) {
		if (QFileInfo(QFileInfo(sTargetPath).path()).exists()) // probably new archive, so only directory should exists
			uri.setPath(sTargetPath);
		else {
			qDebug() << "ZipSubsystem::copyFiles. Target directory doesn't exist:" << QFileInfo(sTargetPath).path();
			return;
		}
	}
	addTo((bMove ? MOVE_TO_ARCHIVE : COPY_TO_ARCHIVE), pSelectionMap, uri, sRequestingPanel);
}

void ZipSubsystem::addTo( SubsystemCommand subsystemCommand, FileInfoToRowMap *pSelectionMap, const URI &uri, const QString &sRequestingPanel )
{
	QString sFN = "ZipSubsystem::addTo.";
	if (pSelectionMap == NULL || pSelectionMap->size() == 0) {
		qDebug() << sFN << "Internal ERROR. Empty or null SelectionMap!";
		return;
	}
	m_opQueue.clear();
	m_currentCommand = subsystemCommand;
// 	emit signalGetWeighingResult(m_totalWeight, m_nTotalItems);
// 	qDebug() << sFN << "File:" << uri.toString() << "Weigh:" << FileInfo::bytesRound(m_totalWeight) << "TotalItems:";
	qDebug() << sFN << "File:" << uri.toString();
	if (! QFileInfo(uri.toString()).exists()) {
		if (! uri.toString().endsWith('/')) {
			createNewArchive(uri.toString(), pSelectionMap, sRequestingPanel);
			return;
		}
	}
	bool bArchiveFiles = (subsystemCommand == COPY_TO_ARCHIVE || subsystemCommand == MOVE_TO_ARCHIVE);
	if (bArchiveFiles && uri.path().isEmpty()) {
		m_error  = CANNOT_ARCHIVE;
		m_status = ERROR_OCCURED;
		m_sProcessedFileName = uri.host();
		emit signalStatusChanged(m_status, sRequestingPanel);
		return;
	}
	if (uri.host().isEmpty()) {
		qDebug() << sFN << "Internal ERROR. Empty uri.host!";
		return;
	}
	m_sRequestingPanel = sRequestingPanel;
	m_sRequestingPanel_open = sRequestingPanel;
	m_processedFileInfo = pSelectionMap->constBegin().key();
	initArchiving();
	m_preProcessedFiles->clear();
	m_sFullFileNamesToAdd  = ""; // used also in addFilesToArchive()
	m_sFileNamesInsideArch = ""; // used also in addFilesToArchive()

	QString   sUriPath = uri.path().right(uri.path().length()-1);
	QString   sZipCmd;
	//QString   sFullFileNamesToAdd;
	FileInfo *pFileInfo, *pNewFileInfo;
	int nDirsNum  = 0;
	int nFilesNum = 0;

	FileInfoToRowMap::const_iterator it = pSelectionMap->constBegin();
	while (it != pSelectionMap->constEnd()) {
		pFileInfo = it.key();
		if (bArchiveFiles) {
			if (pFileInfo->isDir() && ! pFileInfo->isSymLink()) {
				m_sFullFileNamesToAdd  += " '"+pFileInfo->absoluteFileName()+"/',";
				m_sFileNamesInsideArch += " '"+sUriPath+pFileInfo->fileName()+"/',";
			}
			else {
				m_sFullFileNamesToAdd  += " '"+pFileInfo->absoluteFileName()+"',";
				m_sFileNamesInsideArch += " '"+sUriPath+pFileInfo->fileName()+"',";
			}
		}
		m_preProcessedFiles->append(pFileInfo);
		pNewFileInfo = new FileInfo(pFileInfo);
		pNewFileInfo->setData(2, sUriPath, true); // workaround for setting path (for second panel we need to set correct path for new item)
		m_processedFiles->append(pNewFileInfo);
		if (pFileInfo->isDir() && ! pFileInfo->isSymLink())
			nDirsNum++;
		else
			nFilesNum++;
		m_totalWeight += pFileInfo->size();
		++it;
	}

// 	m_nTotalItems = 0; // weighing is doing in different subsystem
	emit signalGetWeighingResult(m_totalWeight, m_nTotalItems);
	if (bArchiveFiles) {
		m_opQueue << Operation(PROG_EXISTS, "which python", sRequestingPanel);
		m_opQueue << Operation(subsystemCommand, sZipCmd, sRequestingPanel);
		emit signalCounterProgress(nDirsNum, DIR_COUNTER, true);
		emit signalCounterProgress(nFilesNum, FILE_COUNTER, true);
		emit signalCounterProgress(m_totalWeight, WEIGH_COUNTER, true);
	}
	emit signalNameOfProcessedFile(m_processedFileInfo->absoluteFileName(), uri.host()+uri.path());
	slotRunNextOperation();
}

QString ZipSubsystem::addTo_PythonScript( const QString &sPyInterpreter, const QString &sArchiveName, const QString &sInList, const QString &sInsideArchPathList )
{
	QString sAddToZip_Python = "#!"+sPyInterpreter+"\n\n\
import zipfile as zf, sys, os \n\
\n\
archiveName = '"+sArchiveName+"' \n\
listA = ["+ sInList +"] \n\
listB = ["+ sInsideArchPathList +"] \n\
\n\
def addFolderToZip(zip_file, folder, fileInArch): \n\
	zip_file.write(folder, fileInArch) # add empty folder \n\
	for file in os.listdir(folder): \n\
		full_path = os.path.join(folder, file) \n\
		if os.path.isfile(full_path): \n\
			zip_file.write(full_path, fileInArch+file) \n\
			print ('added: ' + str(full_path)) \n\
		elif os.path.isdir(full_path): \n\
			print ('Entering folder: ' + str(full_path)) \n\
			addFolderToZip(zip_file, full_path, fileInArch) \n\
\n\
arch=zf.ZipFile(archiveName, 'a', zf.ZIP_DEFLATED) \n\
\n\
for x in range(0, len(listA)): \n\
	if listA[x].endswith('/'): \n\
		addFolderToZip(arch, listA[x], listB[x]) \n\
	else: \n\
		arch.write(listA[x], listB[x]) \n\
		print ('added: '+listA[x]) \n\
\n\
arch.close() \n\
raise SystemExit \n";

	return sAddToZip_Python;
}

QString ZipSubsystem::createArch_PythonScript( const QString &sPyInterpreter, const QString &sInList, const QString &sArchivePath, const QString &sCompressionLevel, const QString &sArchiveName )
{
	QString sAddToZip_Python = "#!"+sPyInterpreter+"\n\n\
import zipfile as zf, os, zlib \n\
\n\
inArchiveName = '"+sArchiveName+"' \n\
archivePath = '"+sArchivePath+"' # should be always set \n\
srcList = ["+ sInList +"] \n\
\n\
zlib.Z_DEFAULT_COMPRESSION = "+sCompressionLevel+" \n\
\n\
def zipdir(path, ziph): \n\
    # ziph is zipfile handle \n\
    for root, dirs, files in os.walk(path): \n\
        print('  adding: ' + root + '/') \n\
        ziph.write(root, root + '/') # add empty folder \n\
        for file in files: \n\
            print('  adding: ' + str(os.path.join(root, file))) \n\
            ziph.write(os.path.join(root, file)) \n\
\n\
if __name__ == '__main__': \n\
    for fullpath in srcList: \n\
        basePath = os.path.dirname(os.path.realpath(fullpath)) \n\
        source = os.path.basename(os.path.normpath(fullpath)) \n\
\n\
        if inArchiveName == '': \n\
            archiveName = archivePath + source + '.zip' \n\
        else: \n\
            archiveName = archivePath + inArchiveName \n\
\n\
        os.chdir(basePath) \n\
        zipf = zf.ZipFile(archiveName, 'a', zf.ZIP_DEFLATED) \n\
        if os.path.isdir(fullpath): \n\
            zipdir(source, zipf) \n\
        else: \n\
            zipf.write(source) \n\
            print('  adding: ' + os.path.join(archivePath, source)) \n\
\n\
        zipf.close() \n\
\n\
raise SystemExit \n";
 	//qDebug() << sAddToZip_Python;
	return sAddToZip_Python;
}

void ZipSubsystem::startPythonScript()
{
	QString sTmpFile = m_sTmpDir+QString("addToZip.%1.python").arg(QDateTime::currentDateTime().toTime_t());
	QFile file(sTmpFile);
	if (! file.open(QIODevice::WriteOnly | QIODevice::Text)) {
		qDebug() << "ZipSubsystem::startPythonScript. ERROR. Cannot create:" << file.fileName();
		return;
	}
	m_sAddToPythonScriptFileName = file.fileName();

	QTextStream out(&file);
	if (m_currentCommand == MAKE_ARCHIVE)
		out << createArch_PythonScript(m_sPythonInterpreter, m_sFullFileNamesToAdd, m_sPathToArchive, m_sCompressionLevel, m_sSingleMultiFileArchiveName);
	else
		out << addTo_PythonScript(m_sPythonInterpreter, m_currentUri.host(), m_sFullFileNamesToAdd, m_sFileNamesInsideArch);

	file.close();
	file.setPermissions(QFile::ReadOwner|QFile::ExeOwner);

	m_sLastShellCmd = file.fileName();
	QTimer::singleShot(0, this, &ZipSubsystem::slotStartProcess);
}


void ZipSubsystem::createNewArchive(const QString &sInArchiveName, FileInfoToRowMap *pSelectionMap, const QString &sRequestingPanel)
{
	QString sFN = "ZipSubsystem::createNewArchive.";
	if (pSelectionMap == NULL || pSelectionMap->size() == 0) {
		qDebug() << sFN << "Internal ERROR. Empty or null SelectionMap!";
		return;
	}
// 	bool bSingleMultiFileArchive = (sInArchiveName.endsWith('|'));
	qDebug() << sFN << "ArchiveName:" << sInArchiveName;
	QString sTargetPath = QFileInfo(sInArchiveName).path();
	if (! QFileInfo(sTargetPath).isWritable()) {
		m_error  = CANNOT_WRITE;
		m_status = ERROR_OCCURED;
		m_sProcessedFileName = sTargetPath;
		emit signalStatusChanged(m_status, sRequestingPanel);
		return;
	}
	m_sRequestingPanel_open = sRequestingPanel;
	FileInfo *pFileInfo = pSelectionMap->constBegin().key();
	m_processedFileInfo = pFileInfo;
	m_preProcessedFiles->clear();
	m_preProcessedFiles->append(pFileInfo);
	QString sZipCmd;

	initArchiving();
// 	m_nTotalItems = 0; // weighing is doing in different subsystem
	emit signalGetWeighingResult(m_totalWeight, m_nTotalItems);
	m_bCreateNewArchive = true;
	Settings settings;
	bool bAlwaysOverwriteTargetIfCompress = settings.value("FilesSystem/AlwaysOverwriteWhenArchiving", false).toBool();

	m_sSingleMultiFileArchiveName = "";
	QString sFullArchiveName = sInArchiveName;
	if (sInArchiveName.endsWith('|')) { // SingleMultiFileArchive
		m_sSingleMultiFileArchiveName = QFileInfo(sInArchiveName.left(sInArchiveName.indexOf('|'))).fileName();
		sFullArchiveName = sFullArchiveName.left(sFullArchiveName.indexOf('|'));
	}

	m_sCompressionLevel = settings.value("archive/ZipLevelCompression", "6").toString(); // range 0-9
	int nCompressionLevel = m_sCompressionLevel.toInt();
	if (nCompressionLevel < 0)
		m_sCompressionLevel = "0";
	else
	if (nCompressionLevel > 9)
		m_sCompressionLevel = "9";

	if (bAlwaysOverwriteTargetIfCompress) {
		if (QFile(sFullArchiveName).remove())
			qDebug() << sFN << "Removed file:" << sFullArchiveName;
	}

	m_currentCommand = MAKE_ARCHIVE;
	m_opQueue << Operation(PROG_EXISTS, "which python", sRequestingPanel);
	m_opQueue << Operation(m_currentCommand, sZipCmd, sRequestingPanel); // ? empty sZipCmd;

	m_sFullFileNamesToAdd = "";
	FileInfoToRowMap::const_iterator it = pSelectionMap->constBegin();
	while (it != pSelectionMap->constEnd()) {
		pFileInfo = it.key();
		m_processedFiles->append(pFileInfo); // m_processedFiles is cleaned in initArchiving
		m_sFullFileNamesToAdd += "'"+pFileInfo->absoluteFileName();
		if (pFileInfo->isDir())
			m_sFullFileNamesToAdd += "/";
		m_sFullFileNamesToAdd += "',";
		++it;
	}

	m_sPathToArchive = sTargetPath += "/";
	emit signalNameOfProcessedFile(m_processedFileInfo->absoluteFileName(), sFullArchiveName);
	slotRunNextOperation();
}

void ZipSubsystem::extractTo( WhatExtract whatExtract, bool bMove, FileInfoToRowMap *pSelectionMap, const QString &sTargetPath, const QString &sRequestingPanel )
{
	QString sFN = "ZipSubsystem::extractTo.";
	if (pSelectionMap == NULL || pSelectionMap->size() == 0) {
		qDebug() << sFN << "Internal ERROR. Empty or null SelectionMap!";
		return;
	}
	qDebug() << sFN << "TargetPath:" << sTargetPath;
	m_opQueue.clear();
	if (! QFileInfo(sTargetPath).isWritable()) {
		m_error  = CANNOT_WRITE;
		m_status = ERROR_OCCURED;
		m_currentCommand = EXTRACT;
		m_bOperationFinished = true;
		m_sProcessedFileName = sTargetPath;
		emit signalStatusChanged(m_status, sRequestingPanel);
		return;
	}

	Settings settings;
	bool bPersistPathInExtractingArch = settings.value("archive/PersistPathInExtractingArch", true).toBool();
	bool bWeighBeforeArchExtracting   = settings.value("archive/WeighBeforeArchExtracting", true).toBool();
	bool bWeighBeforeFilesExtracting  = true; // always weigh files
	bool bAlwaysOverwrite = settings.value("FilesSystem/AlwaysOverwrite", true).toBool();
	m_sRequestingPanel = sRequestingPanel;
	m_sRequestingPanel_open = sRequestingPanel;
	m_whatExtract = whatExtract;
	m_processedFileInfo = pSelectionMap->constBegin().key();

	initWeighing(true, m_sRequestingPanel);
	if (whatExtract == Vfs::EXTRACT_FILES) {
		if (bWeighBeforeFilesExtracting) {
			weigh(pSelectionMap, currentURI(), sRequestingPanel);
			emit signalCounterProgress(m_nDirCounter,  DIR_COUNTER, true);
			emit signalCounterProgress(m_nFileCounter, FILE_COUNTER, true);
			emit signalCounterProgress(m_totalWeight,  WEIGH_COUNTER, true);
		}
	}
	initExtracting();

	FileInfo *pFileInfo;
	QString   sFileNameList, sDirNameList;
	QString   sFullFileNamesToExtract, sFileName, sCmd;
	QString   sExclPath = (! bPersistPathInExtractingArch) ? " -j " : " ";
	QString   sOvrMode  = (bAlwaysOverwrite) ? " -o " : " ";
	sOvrMode = " -o "; // because "Overwrite mode" is not supported yet TODO: implement "Overwrite mode" for extracting entire archives
	long long nTotalWeight = 0;
	QStringList slMvCmds, slRmCmds;

	if ((whatExtract == EXTRACT_ARCHIVE && ! bWeighBeforeArchExtracting) || whatExtract == EXTRACT_FILES)
		m_opQueue << Operation(PROG_EXISTS, "which unzip", sRequestingPanel);

    FileInfoToRowMap::const_iterator it = pSelectionMap->constBegin();
	while (it != pSelectionMap->constEnd()) {
		pFileInfo = it.key();
		if (whatExtract == EXTRACT_ARCHIVE) {
			if (! fileIsMatchingToSubsystemMime(m_pPluginData->mime_type, pFileInfo)) {
				++it;
				continue;
			}
			sFileName = absFileNameInLocalFS(pFileInfo, m_sTmpDir);
			sFullFileNamesToExtract = "\"" + sFileName + "\" ";
			// unzip <target-zip-file> '<folder-to-extract/*>' -d <destination-path>
			sCmd = "unzip " + sOvrMode + sExclPath + sFullFileNamesToExtract + " -d \""+sTargetPath+"\"";
			if (bWeighBeforeArchExtracting)
				m_opQueue << Operation(WEIGH_ARCH, pFileInfo, sTargetPath, sRequestingPanel);
			m_opQueue << Operation(EXTRACT, sCmd, sRequestingPanel);
		}
		else { // EXTRACT_FILES
			if (pFileInfo->isDir() && ! pFileInfo->isSymLink()) {
				sDirNameList += " \""+pFileInfo->absoluteFileName()+"/*\"";
				if (m_nLevelInArch > 1) {
					if (bAlwaysOverwrite)
						slMvCmds.append("rm -rf "+sTargetPath+pFileInfo->fileName());
					slMvCmds.append("mv " + sTargetPath+pFileInfo->absoluteFileName()+" "+sTargetPath);
					QString sRmDir = sTargetPath+QFileInfo(pFileInfo->absoluteFileName()).path();
					slRmCmds.append("rm -rf "+sRmDir);
				}
			}
			else {
				sFileNameList += " \""+pFileInfo->absoluteFileName()+"\"";
			}
			nTotalWeight += pFileInfo->size();
		}
		m_processedFiles->append(pFileInfo);
		++it;
	}
	qDebug() << sFN << "TotalItems:" << m_nTotalItems << "processedFiles:" << m_processedFiles->size();

	if (whatExtract == EXTRACT_FILES) {
		if (bMove)
			m_currentCommand = Vfs::EXTRACT_AND_REMOVE;
		if (! sFileNameList.isEmpty()) {
			// unzip -oj "/home/piotr/tmp/lxterminal-0.1.6.zip"  "lxterminal-0.1.6/README" -d "/home/tst_qtcmd/test_archives/1/bbc1/"
			sCmd = "unzip" +sOvrMode+"-j \"" + m_sHost + "\" " + sFileNameList+" -d \""+sTargetPath+"\"";;
			m_opQueue << Operation(POST_EXTRACT, sCmd, sRequestingPanel);
		}
		if (! sDirNameList.isEmpty()) {
			// unzip -o "/home/piotr/tmp/lxterminal-0.1.6.zip"  "lxterminal-0.1.6/*" -d "/home/tst_qtcmd/test_archives/1/bbc1/"
			sCmd = "unzip" +sOvrMode+"\"" + m_sHost + "\" " + sDirNameList+" -d \""+sTargetPath+"\"";;
			m_opQueue << Operation(POST_EXTRACT, sCmd, sRequestingPanel);
		}
		if (! slMvCmds.isEmpty() && ! slRmCmds.isEmpty()) {
			foreach(sCmd, slMvCmds) {
				m_opQueue << Operation(POST_EXTRACT, sCmd, sRequestingPanel);
			}
			foreach(sCmd, slRmCmds) {
				m_opQueue << Operation(m_currentCommand, sCmd, sRequestingPanel);
			}
		}
	}

	emit signalNameOfProcessedFile(sFileName, sTargetPath);
	slotRunNextOperation();
}

void ZipSubsystem::initArchiving()
{
	m_sOutBuffer = "";
	m_sLastDirName = "";
	m_opQueue.clear();
	m_processedFiles->clear();
	m_totalWeight    = 0;
	m_OpTotalWeight  = 0;
	m_nOpDirCounter  = 0;
	m_nOpFileCounter = 0;
// 	m_nOpSymlinksCounter = 0;
	m_status = ARCHIVING;
	m_bCreateNewArchive = false;
	emit signalStatusChanged(m_status, m_sRequestingPanel);
}

void ZipSubsystem::initExtracting()
{
	if (m_status == WEIGHED) {
		qDebug() << "ZipSubsystem::initExtracting. WEIGHED - totalWeigh:" << m_totalWeight << FileInfo::bytesRound(m_totalWeight) << "files:" << m_nFileCounter << "dirs:" << m_nDirCounter << "all items:" << m_nTotalItems;
	}
// 	qDebug() << "ZipSubsystem::initExtracting. Cleaning variables.";
	m_sOutBuffer = "";
	m_sLastDirName = "";
	m_opQueue.clear();
	m_processedFiles->clear();
	m_preProcessedFiles->clear();
	m_OpTotalWeight  = 0;
	m_nOpDirCounter  = 0;
	m_nOpFileCounter = 0;
// 	m_nOpSymlinksCounter = 0;
	m_currentCommand = EXTRACT;
	m_status = EXTRACTING;
	m_bCreateNewArchive = false;
	// 	m_sRequestingPanel = sRequestingPanel; // used in slotProcessFinished
	emit signalStatusChanged(m_status, m_sRequestingPanel);
}


void ZipSubsystem::updateContextMenu( ContextMenu *pContextMenu, KeyShortcuts *pKS )
{
	qDebug() << "ZipSubsystem::updateContextMenu.";
	m_pContextMenu = pContextMenu;

	m_pContextMenu->setVisibleAction(CMD_ExtractTo, false);
	m_pContextMenu->setVisibleAction(CMD_Makelink, false);
	m_pContextMenu->setVisibleAction(CMD_EditSymlink, false);
	m_pContextMenu->setVisibleAction(CMD_Rename, false);
	m_pContextMenu->setVisibleAction(CMD_CreateNew_MENU, false);
}


void ZipSubsystem::searchingFinished()
{
	m_searchingTimer.stop();
	m_processedFiles->clear();
	qDebug() << "ZipSubsystem::searchingFinished. Finished at line:" << m_nSearchedLines;
	m_status = FOUND;
	m_bOperationFinished = true;
	emit signalStatusChanged(m_status, m_sRequestingPanel);
}

void ZipSubsystem::setBreakOperation()
{
	qDebug() << "ZipSubsystem::setBreakOperation. Operation stopped.";
	if (m_currentCommand == FIND)
		searchingFinished();
}

void ZipSubsystem::setPauseOperation( bool bPause )
{
	qDebug() << "ZipSubsystem::setPauseOperation. Pause:" << bPause;
	m_bPausedOperation = bPause;
	m_error = NO_ERROR;
	if (bPause) {
		m_searchingTimer.stop();
		m_pausedStatus = m_status;
		m_status = OPR_PAUSED;
	} else {
		m_status = m_pausedStatus;
		m_searchingTimer.start(0);
	}
	emit signalStatusChanged(m_status, m_sRequestingPanel);
}

void ZipSubsystem::updateParsingBuffer( SubsystemCommand subsysCmd )
{
	bool bArchiveFiles = (subsysCmd == COPY_TO_ARCHIVE || subsysCmd == MOVE_TO_ARCHIVE);
	if (subsysCmd != REMOVE && ! bArchiveFiles)
		return;

	QString sAbsFileName, sLocalName, sSize, sNewSize, sType;
	QString sNewItem;
	QFileInfo fi;
	int nId = -1;

	foreach (FileInfo *pFI, *m_processedFiles) {
		if (subsysCmd == REMOVE) {
			sLocalName = ".* "+pFI->absoluteFileName();
			if (pFI->isDir())
				sLocalName += QDir::separator();
			nId = m_slParsingBufferLine.indexOf(QRegExp(sLocalName));
			if (nId != -1)
				m_slParsingBufferLine.removeAt(nId);
			nId = m_fiList.indexOf(pFI);
			if (nId != -1)
				m_fiList.removeAt(nId);
		}
		else
		if (bArchiveFiles) {
			sLocalName = pFI->absoluteFileName();
			if (sLocalName.startsWith(QDir::separator()))
				sLocalName.remove(0,1);
			if (pFI->isDir()) {
				sLocalName += QDir::separator();
				sType = "bx";
			}
			else
				sType = "tx";
			nId = m_slParsingBufferLine.indexOf(QRegExp(".* "+sLocalName));
			if (nId != -1)
				continue;
			sSize = QString("%1").arg(pFI->size());
			sNewItem = pFI->permissions()+"  3.0 unx  "+sType+" defX " + pFI->lastModified().toString("yyyyMMdd.HHmmss") +" ";
			sNewSize = QString(m_nNameColId - sNewItem.length() - sSize.length() - 1, ' ') + sSize; // add leading spaces
			sNewItem.insert(20, sNewSize); // permissions length + 1
			sNewItem += sLocalName;
			//qDebug() << sNewItem;
			m_slParsingBufferLine.append(sNewItem);
			m_fiList.append(pFI);
		}
	}
	m_nFilesNumInArch = m_slParsingBufferLine.count();

	// -- update internal list with newly files from added directory
	if (m_slAddedFilesFromSubdirectories.isEmpty())
		return;

	foreach (QString sFullFileName, m_slAddedFilesFromSubdirectories) {
		if (subsysCmd == REMOVE) {
			sLocalName = m_currentUri.path() +"/"+ sFullFileName.split('|').at(1);
			if (sLocalName.startsWith(QDir::separator())) // remove first separator
				sLocalName = sLocalName.right(sLocalName.length()-1);
			nId = m_slParsingBufferLine.indexOf(QRegExp(".* "+sLocalName) );
			if (nId != -1)
				m_slParsingBufferLine.removeAt(nId);
		}
		else
		if (bArchiveFiles) {
			sAbsFileName = sFullFileName.split('|').at(0);
			sLocalName   = m_currentUri.path() +"/"+ sFullFileName.split('|').at(1);
			if (sLocalName.startsWith('/'))
				sLocalName = sLocalName.right(sLocalName.length()-1);
			nId = m_slParsingBufferLine.indexOf(QRegExp(".* "+sLocalName));
			if (nId != -1)
				continue;
			fi.setFile(sAbsFileName);
			sSize = QString("%1").arg(fi.size());
			sNewItem = permissionsAsString(sAbsFileName)+"  3.0 unx  tx defX " + fi.lastModified().toString("yyyyMMdd.HHmmss") +" ";
			sNewSize = QString(m_nNameColId - sNewItem.length() - sSize.length() - 1, ' ') + sSize; // add leading spaces
			sNewItem.insert(20, sNewSize); // permissions length + 1
			sNewItem += sLocalName;
			//qDebug() << sNewItem;
			m_slParsingBufferLine.append(sNewItem);
		}
	}
	if (subsysCmd == REMOVE)
		m_slAddedFilesFromSubdirectories.clear();

	m_nFilesNumInArch = m_slParsingBufferLine.count();
}

// ------------- SLOTs -------------

void ZipSubsystem::slotRunNextOperation()
{
	QString sFN = "ZipSubsystem::slotRunNextOperation.";
	if (m_opQueue.count() == 0) {
		qDebug() << sFN << "Empty queue. Exit.";
		return;
	}
	if (m_pProcess->state() != QProcess::NotRunning) {
		qDebug() << sFN << "Process is already running.";
		return;
	}
	m_sOutBuffer = "";
	m_sErrBuffer = "";

	Operation operation = m_opQueue.first();
	m_currentCommand = operation.command;  // internal command
	if (m_currentCommand == OPEN || m_currentCommand == VIEW)
		m_sRequestingPanel_open = operation.subsysMngrName;
	else
		m_sRequestingPanel = operation.subsysMngrName;

	bool bShellCmd = (! operation.shellCmd.isEmpty());
	m_opQueue.pop_front();
	qDebug() << sFN << " RequestingPanel:" << m_sRequestingPanel << "RequestingPanel_open:" << m_sRequestingPanel_open << " opQueue.size:" << m_opQueue.size();
	if (m_currentCommand == WEIGH_ARCH)
		m_bWeighingArchive = true;

	if (m_currentCommand == OPEN || m_currentCommand == VIEW) {
		if (bShellCmd)
			m_status = CONNECTING;
		else // reopen
			open(operation.uri, operation.subsysMngrName);
	}

	if (bShellCmd) {
		if      (m_currentCommand == OPEN_WITH_CMD)
			m_status = OPENING_WITH; // TODO: show progress bar for extracting files
		else if (m_currentCommand == COPY_TO_ARCHIVE || m_currentCommand == MOVE_TO_ARCHIVE)
			m_status = ARCHIVING;
		else if (m_currentCommand == EXTRACT || m_currentCommand == EXTRACT_AND_REMOVE || m_currentCommand == POST_EXTRACT)
			m_status = EXTRACTING;

		if (m_currentCommand != PROG_EXISTS)
			emit signalStatusChanged(m_status, m_sRequestingPanel_open);

		m_sLastShellCmd = operation.shellCmd;
		qDebug() << sFN << "Command:" << toString(m_currentCommand) << "Execute shell cmd:" << m_sLastShellCmd << " RequestingPanel:" << m_sRequestingPanel_open;
		if (m_currentCommand == EXTRACT) {
			if (m_whatExtract == EXTRACT_ARCHIVE) {
				m_nOpFileCounter = 0; // counter of all items in extracting archive
				QString sExtractedArch = m_sLastShellCmd.split('\"').at(1);
				for (int i = 0; i < m_processedFiles->size(); ++i) {
					if (m_processedFiles->at(i)->absoluteFileName() == sExtractedArch) {
						m_processedFileInfo = m_processedFiles->at(i);
						break;
					}
				}
			}
		}
		//QStringList slArgs;
		if (m_currentCommand == REMOVE)
			QTimer::singleShot(0, this, &ZipSubsystem::slotStartProcess);
		else
			m_pProcess->start(m_sLastShellCmd);
// warning: ‘void QProcess::start(const QString&, QIODevice::OpenMode)’ is deprecated: Use QProcess::start(const QString &program, const QStringList &arguments,OpenMode mode = ReadWrite) instead [-Wdeprecated-declarations]
	} else {
		if      (m_currentCommand == WEIGH)
			weigh(operation.selectionMap, operation.uri, operation.subsysMngrName, operation.booleanArg); // bWeighingAsSubCmd=true
		else if (m_currentCommand == WEIGH_ITEM)
			weighItem(operation.fileInfo, operation.uri, operation.subsysMngrName, operation.booleanArg);
		else if (m_currentCommand == WEIGH_ARCH) {
			weighArchive(absFileNameInLocalFS(operation.fileInfo, m_sTmpDir), operation.subsysMngrName);
// 			weighArchive(operation.fileInfo->absoluteFileName());
		}
		else if (m_currentCommand == REMOVE)
			remove(operation.selectionMap, operation.uri, operation.booleanArg, operation.subsysMngrName);
		else if (m_currentCommand == COPY_TO_ARCHIVE || m_currentCommand == MOVE_TO_ARCHIVE || m_currentCommand == MAKE_ARCHIVE)
			startPythonScript();
	}
}

void ZipSubsystem::slotProcessFinished( int nExitCode, QProcess::ExitStatus exitStatus )
{
	QString sFN = "ZipSubsystem::slotProcessFinished.";
	QString sSuccessFailrue = (nExitCode == 0) ?  "(Success)" : "(Failrue)";
	qDebug() << sFN << "ExitCode:" << nExitCode << "exitStatus:" << exitStatus << "m_currentCommand:" << toString(m_currentCommand) << m_sRequestingPanel_open;
	m_bActionIsFinished = true;

	QString sCmdName;
	if (m_currentCommand == PROG_EXISTS)
		sCmdName = m_sLastShellCmd.split(' ').at(1); // useful only for 'which' command

	if (exitStatus == QProcess::NormalExit && nExitCode == 0) {
		qDebug() << sFN << "-- Process finished with success.";
		m_error = NO_ERROR;
		if (m_currentCommand == PROG_EXISTS) {
			if (! m_sOutBuffer.isEmpty()) {
				if (sCmdName == "python") {
					m_sPythonInterpreter = m_sOutBuffer;
					m_sPythonInterpreter.remove('\n');
				}
				if (m_sRequestingPanel_open != "leftPanel_1:init")
					m_status = CONNECTED;
				qDebug() << sFN << sCmdName << "found in PATH.";
				QTimer::singleShot (20, this, &ZipSubsystem::slotRunNextOperation);
				return;
			}
		}
		else
		if (m_currentCommand == EXTRACT || m_currentCommand == EXTRACT_AND_REMOVE || m_currentCommand == POST_EXTRACT || m_currentCommand == VIEW) {
			if (m_bWeighingArchive) {
				m_status = WEIGHING;
				emit signalStatusChanged(m_status, m_sRequestingPanel_open);
				m_currentCommand = WEIGH_ARCH;
			}
			else {
				if (m_currentCommand != VIEW) {
					m_status = LISTING;
					emit signalStatusChanged(m_status, m_sRequestingPanel_open);
				}
				parsingBufferedFilesList(); // parsing output of sys.op.command (inits m_nFilesNumInArch)
				if (m_sPath.endsWith(QDir::separator()) && m_currentCommand != VIEW) // if level > 0 and not view then set next called command
					m_currentCommand = LIST;
			}
			qDebug() << sFN << "Switched from EXTRACT or EXTRACT_AND_REMOVE or POST_EXTRACT command to:" << Vfs::toString(m_currentCommand);
		}

		if (m_currentCommand == OPEN || m_currentCommand == VIEW) { // processing buffer for level = 0
			processingParsedFilesList(0, "");
			if (m_status == ERROR_OCCURED)
				return;
			m_sCurrentPath = QDir::separator();
			m_bOpenedArchive = true;
			qDebug() << sFN << "Command:" << Vfs::toString(m_currentCommand) << "m_sPath:" << m_sPath << "m_sCurrentPath:" << m_sCurrentPath << "m_bOpenedArchive:" << m_bOpenedArchive << "m_nFilesNumInArch:" << m_nFilesNumInArch;
			m_currentUri.setPath(m_sCurrentPath);
			if (m_currentCommand != VIEW) {
				m_currentCommand = LIST;
				m_status = m_bCompleterMode ? LISTED_COMPL : LISTED;
			}
			else
				m_status = VIEW_COMPL;

			qDebug() << sFN << "Switched from OPEN or VIEW command to:" << Vfs::toString(m_currentCommand) << "and status to:" << Vfs::toString(m_status);
			emit signalStatusChanged(m_status, m_sRequestingPanel_open);
			//qDebug() << m_sOutBuffer;
			//qDebug() << m_sParsingBufferLine;
		}
		else
		if (m_currentCommand == LIST) {
			commandList();
		}
		else
		if (m_currentCommand == OPEN_WITH_CMD) {
			if (! m_sOutBuffer.isEmpty())
				processingOpenWithCmd();
			else {
				m_status = ERROR_OCCURED;
				m_error  = OTHER_ERROR;
			}
			emit signalStatusChanged(m_status, m_sRequestingPanel);
		}
		else
		if (m_currentCommand == REMOVE) {
			;
			emit signalTimerStartStop(false);
			m_status = REMOVED;
			emit signalOperationFinished(); // only updates button label to 'Done' on progress dialog
			emit signalStatusChanged(m_status, m_sRequestingPanel);
			updateParsingBuffer(REMOVE); // update also internal files list with files added from subdirectories
		}
		else
		if (m_currentCommand == READ_FILE_TO_VIEW) {
			m_status = EXTRACTED_TO_VIEW;
			emit signalStatusChanged(m_status, m_sRequestingPanel);
		}
		else
		if (m_currentCommand == WEIGH_ARCH) {
			parsingBufferedFilesList(); // parsing output of sys.op.command (inits m_nFilesNumInArch)
			processingWeighCmd(NULL);
			m_status = WEIGHED;
			//emit signalStatusChanged(m_status, m_sRequestingPanel_open); // helped for Properties dialog
			emit signalCounterProgress(m_nDirCounter,  DIR_COUNTER, true);
			emit signalCounterProgress(m_nFileCounter, FILE_COUNTER, true);
			emit signalCounterProgress(m_totalWeight,  WEIGH_COUNTER, true);
			m_bWeighingArchive = false;
			if (m_opQueue.count() > 0) {
				if (m_opQueue.at(0).command == EXTRACT) {
					if (m_whatExtract == Vfs::EXTRACT_ARCHIVE) {
						long long freeBytes, totalBytes;
						Vfs::getStatFS(m_sTmpDir, freeBytes, totalBytes);
						if (freeBytes < m_totalWeight) {
							m_opQueue.clear();
							m_error = NO_FREE_SPACE;
							emit signalStatusChanged(Vfs::ERROR_OCCURED, m_sRequestingPanel);
							return;
						}
					}
				}
				slotRunNextOperation();
				return;
			}
			else {
				emit signalStatusChanged(m_status, m_sRequestingPanel);
				return;
			}
		}
		else
		if (m_currentCommand == EXTRACT || m_currentCommand == EXTRACT_AND_REMOVE || m_currentCommand == POST_EXTRACT) {
			if (m_opQueue.count() > 0) {
				if (m_currentCommand != POST_EXTRACT)
					emit signalStatusChanged(EXTRACTED, m_sRequestingPanel); // for update extracting status on progress window
				QTimer::singleShot (0, this, &ZipSubsystem::slotRunNextOperation);
				return;
			}
			else {
				m_status = EXTRACTED;
				if (m_whatExtract == EXTRACT_FILES)
					m_bOperationFinished = true;
				emit signalOperationFinished(); // only updates button label on progress dialog to 'Done'
				emit signalStatusChanged(m_status, m_sRequestingPanel);
			}
		}
		else
		if (m_currentCommand == COPY_TO_ARCHIVE || m_currentCommand == MOVE_TO_ARCHIVE || m_currentCommand == MAKE_ARCHIVE) {
			// need to run on two times, because directories and files are added separately
			if (m_opQueue.count() > 0) {
				QTimer::singleShot (0, this, &ZipSubsystem::slotRunNextOperation);
				return;
			}
			else { // (m_opQueue.size() == 0)
				QFile (m_sAddToPythonScriptFileName).remove();
				m_status = ARCHIVED;
				m_bForceReopen = true;
				m_bOperationFinished = true;
				if (! m_bCreateNewArchive)
					updateParsingBuffer(m_currentCommand); // update also internal files list with files added from subdirectories
				emit signalOperationFinished(); // only updates button label on progress dialog to 'Done'
				emit signalStatusChanged(m_status, m_sRequestingPanel);
			}
		}

		if ((m_currentCommand == LIST || m_currentCommand == VIEW) && m_opQueue.count() > 0) {
			qDebug() << sFN << toString(m_currentCommand) << "command finished. Call next queued command";
			if (m_sRequestingPanel_open != m_opQueue.first().subsysMngrName) { // if opened two the same archives then list previous
				emit signalStatusChanged(m_status, m_sRequestingPanel_open);
				QTimer::singleShot (0, this, &ZipSubsystem::slotRunNextOperation);
				return;
			}
			else
				slotRunNextOperation();
		}

		if (m_currentCommand == LIST && m_status != CONNECTED) {
			qDebug() << sFN << "Listing finished. status:" << toString(m_status);
			emit signalStatusChanged(m_status, m_sRequestingPanel_open);
		}
	}
	else { // error occured
		qDebug() << sFN << "-- Process finished with error:" << nExitCode << "command:" << toString(m_currentCommand);
		qDebug() << sFN << "LastShellCmd:" << m_sLastShellCmd;
		qDebug() << m_sErrBuffer;
		m_status = ERROR_OCCURED;
		m_error  = OTHER_ERROR;
		m_bForceReopen = true;
		if (m_currentCommand == PROG_EXISTS) {
			if (! m_sErrBuffer.isEmpty()) {
				m_error = CMD_NOT_FOUND;
				qDebug() << sFN << sCmdName << "not found in PATH!";
				// - find properly command
				while (m_opQueue.size() > 0) {
					if (m_opQueue.first().command != PROG_EXISTS && m_opQueue.first().command != CHK_VER)
						break;
					m_opQueue.pop_front();
				}
				m_sProcessedFileName = "";
				if (m_opQueue.size() > 0) {
					Operation operation = m_opQueue.first();
					if (operation.command == REMOVE) m_sProcessedFileName = tr("Removing operation");
					else
					if (operation.command == OPEN || operation.command == VIEW) m_sProcessedFileName = tr("Opening archive");
					else
					if (operation.command == EXTRACT || operation.command == EXTRACT_AND_REMOVE || operation.command == POST_EXTRACT) m_sProcessedFileName = tr("Extracting");
					else
					if (operation.command == WEIGH_ARCH) m_sProcessedFileName = tr("Weighing");
					else
					if (operation.command == OPEN_WITH_CMD) m_sProcessedFileName = tr("Opening file");
					else
					if (operation.command == READ_FILE_TO_VIEW) m_sProcessedFileName = tr("Previewing file");
					m_sProcessedFileName += " "+tr("operation is not possible.") +"\n\n"+tr("Cannot find tar command in system PATH!");
				}
			}
		}
		else
		if (m_currentCommand == OPEN_WITH_CMD) {
			if (! m_sErrBuffer.isEmpty()) {
				m_error = CANNOT_EXTRACT;
				qDebug() << sFN << "-- Cannot extract all input files";
			}
		}
		else
		if (m_currentCommand == OPEN || m_currentCommand == READ_FILE_TO_VIEW) {
			m_sProcessedFileName = m_currentUri.host();
			m_error = CANNOT_READ;
		}
		else if (m_currentCommand == REMOVE) {
			m_error = CANNOT_REMOVE;
		}
		else
		if (m_currentCommand == EXTRACT || m_currentCommand == EXTRACT_AND_REMOVE || m_currentCommand == POST_EXTRACT) {
			m_error = CANNOT_EXTRACT;
		}
		else
		if (m_currentCommand == COPY_TO_ARCHIVE || m_currentCommand == MOVE_TO_ARCHIVE) {
			//QFile (m_sAddToPythonScriptFileName).remove();
			m_error = CANNOT_ARCHIVE;
		}
		m_bOperationFinished = true;
		emit signalStatusChanged(m_status, ((m_currentCommand == LIST || m_currentCommand == OPEN) ? m_sRequestingPanel_open : m_sRequestingPanel));
	}
}

void ZipSubsystem::slotBreakProcess()
{
	if (m_pProcess->exitStatus() == QProcess::CrashExit) { // condition maybe not necessary (not tested) :/
		m_pProcess->kill();
		m_bActionIsFinished = true;
		m_status = OPR_STOPPED;
		m_error  = NO_ERROR;
		m_opQueue.clear();
		emit signalStatusChanged(m_status, ((m_currentCommand == LIST || m_currentCommand == OPEN) ? m_sRequestingPanel_open : m_sRequestingPanel));
	}
}


void ZipSubsystem::slotReadStandardOutput()
{
	if (m_currentCommand == REMOVE
		|| m_currentCommand == EXTRACT || m_currentCommand == EXTRACT_AND_REMOVE || m_currentCommand == POST_EXTRACT
		|| m_currentCommand == COPY_TO_ARCHIVE || m_currentCommand == MOVE_TO_ARCHIVE || m_currentCommand == MAKE_ARCHIVE)
	{
		m_sOutBuffer = m_pProcess->readAllStandardOutput();

		if (m_currentCommand == REMOVE) // slotReadStandardOutput is never called, because tar doesn't prints deleted files
			processingRemoveCmd();
		else
		if (m_currentCommand == EXTRACT || m_currentCommand == EXTRACT_AND_REMOVE || m_currentCommand == POST_EXTRACT)
			processingExtractCmd();
		else
		if (m_currentCommand == COPY_TO_ARCHIVE || m_currentCommand == MOVE_TO_ARCHIVE || m_currentCommand == MAKE_ARCHIVE)
			processingArchiveCmd();
	}
	else
		m_sOutBuffer += m_pProcess->readAllStandardOutput();
}

void ZipSubsystem::slotReadStandardError()
{
	m_sErrBuffer += m_pProcess->readAllStandardError();
}

void ZipSubsystem::slotStartProcess()
{
	m_pProcess->start(m_sLastShellCmd);
}

void ZipSubsystem::slotReadStandardOutputAsyn()
{
	if (m_currentCommand == REMOVE)
		QTimer::singleShot(0, this, &ZipSubsystem::slotReadStandardOutput);
	else
		slotReadStandardOutput();
}

void ZipSubsystem::slotSearching()
{
	if (m_findCriterion.stopIfXMatched) {
		if (m_nMatchedFiles == m_findCriterion.stopAfterXMaches) {
			searchingFinished();
			return;
		}
	}
	m_nSearchedLines++;
	if (m_nSearchedLines >= m_nFilesNumInArch) {
		searchingFinished();
		return;
	}

	bool isDir, isSymLink = false;
	QStringList slLine;
	QString sMime, sLine, sName, sDateTime, sFullPath;
	FileInfo *pFileInfo;

	sLine = m_slParsingBufferLine.at(m_nSearchedLines);
	sName = QDir::separator() + getName(sLine);
	isDir = (sName.at(sName.length()-1) == '/');

	if (! sName.startsWith(m_sStartSearchPath))
		return;
	sName.remove(0, m_sStartSearchPath.length());

	slLine = sLine.split(QRegExp("\\s+"), Qt::SkipEmptyParts);
	sFullPath = m_sSearchHost + QDir::separator() + QFileInfo(sName).path();
	sName = sName.right(sName.length() - sName.lastIndexOf(QDir::separator()) - 1);
	sName = QString::fromLocal8Bit(sName.toLatin1().data());

	if (sName.endsWith('/'))
		sName.remove(sName.length()-1, 1); // remove ending slash
	//sDateTime = slLine[DATE] +" "+ slLine[TIME];
	sDateTime = slLine[DATETIME];
	sMime = MimeType::getFileMime(sName, isDir);
	//QDateTime dt = QDateTime::fromString(sDateTime, "yyyy-MM-dd HH:mm");
	QDateTime dt = QDateTime::fromString(sDateTime, "yyyyMMdd.HHmmss"); // format used by zipinfo/unzip -ZT (MMM needs add.effort to decode due to Qt translate name to locale)
	if (! dt.isValid())
		QDateTime::fromString(sDateTime, "MM-dd-yyyy HH:mm"); // US format
	pFileInfo = new FileInfo(sName, sFullPath, atoll(slLine[SIZE].toLatin1()), isDir, false, dt, "----------", "?", "?", sMime);
	//qDebug() << "--- name:" << sName << "size:" << lineStrList[SIZE] << "time:" << sDateTime;

	m_nFileCounter += (!isDir) ? 1 : 0;
	m_nDirCounter  += (isDir && !isSymLink) ? 1 : 0;

	if (isFileMatches(pFileInfo)) {
		m_processedFiles->append(pFileInfo);
		m_nMatchedFiles++;
		emit signalStatusChanged(Vfs::FOUND, m_sRequestingPanel);
		m_status = FINDING;
	}
	else
		delete pFileInfo;
}


} // namespace Vfs
