/***************************************************************************
 *   Copyright (C) 2015 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef LOCALSUBSYSTEMWATCHER_H
#define LOCALSUBSYSTEMWATCHER_H

#include <sys/inotify.h>

#include <QMap>
#include <QTimer>
#include <QObject>
#include <QStringList>

#include "subsystemconst.h"

namespace Vfs {

/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class LocalSubsystemWatcher : public QObject
{
	Q_OBJECT
public:
	LocalSubsystemWatcher();
	LocalSubsystemWatcher( const QStringList &slPaths );
	~LocalSubsystemWatcher();

	void setLock( bool bLock );

	void addPaths( const QStringList &slPaths, bool bIfUniqe=true );
	bool addPath( const QString &sPath, bool bIfUniqe=true );

	void removePaths( const QStringList &slPaths );
	bool removePath( const QString &sPath );

	/** Removes all watching paths.
	 */
	void clear() {  removePaths(m_slPaths);  }

	/** Returs list of watching directories.
	 * @return QString list
	 */
	QStringList directories() const { return m_slPaths; }

	/** Subsystem status for last event. In this momemt supported are following:
	 * NEED_REFRESH_ATR, NEED_REFRESH_ADD, NEED_REFRESH_DEL, NEED_REFRESH_REN.
	 * @return status of type Vfs::SubsystemStatus
	 */
	SubsystemStatus subsystemStatus() const { return m_eSubsystemStatus; }

	/** Returns file name related to last event.
	 * @return file name
	 */
	QString relatedFileName() const { return m_sRelatedFileName; }

	/** Returns old file name if appeared rename event.
	 * @return file name
	 */
	QString oldFileName() const { return m_sOldFileName; } // useful for rename event

	/** Returns new file name if appeared rename event.
	 * @return file name
	 */
	QString newFileName() const { return m_sNewFileName; } // useful for rename event

private:
	int m_nInotifyFD;
 	QStringList m_slPaths;
	QString m_sRelatedFileName, m_sOldFileName, m_sNewFileName;
	QMap <QString, int> m_mapPathToWD; // WD - Watch Descriptor
	QTimer *m_pWatcherTimer;

	SubsystemStatus m_eSubsystemStatus;
	bool m_bLock;

	enum UpdatePathOperation {
		ADD=0,
		REMOVE
	};
	QString toString( UpdatePathOperation eOperation );


	/**
	 * Initialize Inotify file descriptor and watch timer object,
	 * which will cyclically call of reading Inotify events.
	 */
	bool init();

	/** Adds or remove list of paths to watched paths.
	 * @param slPaths list of paths
	 * @param eOperation available operation (add or remove). @see UpdatePathOperation
	 */
	void updateWatchedPaths( const QStringList &slPaths, UpdatePathOperation eOperation );

	/** Adds/removes path to/from watched paths (inotify list).
	 * @param sPath path
	 * @param eOperation available operation (add or remove). @see UpdatePathOperation
	 */
	bool updateWatchedPaths( const QString &sPath, UpdatePathOperation eOperation );

	/** Updates properly status (SubsystemStatus type) in member of class (@see subsystemStatus()).
	 * Updates file name(s) in properly member(s) of class (for rename there are two names:
	 * @see oldFileName() and @see newFileName()). And finaly sends signal: @see directoryChanged()
	 * @param ie pointer to inotify_event structure
	 */
	void manageInotifyEvent( struct inotify_event *ie );

private slots:
	/** Reads inotify events in non blocking mode.
	 * When event appears then calls function of managing events: @see manageInotifyEvent()
	 */
	void slotWatchInotifyEvents();

signals:
	void directoryChanged( const QString &sPath );

};

} // namespace Vfs
#endif // LOCALSUBSYSTEMWATCHER_H
