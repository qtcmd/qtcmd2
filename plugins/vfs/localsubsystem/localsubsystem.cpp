/***************************************************************************
 *   Copyright (C) 2005 by Mariusz Borowski <b0mar@nes.pl>                 *
 *   Copyright (C) 2009 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
//#include <QDebug> // need to qDebug()
#include <QDir>
#include <QTimer>
#include <QProcess>

#include <errno.h>
#include <unistd.h>
// #include <sys/stat.h>

//#include "localsubsystemsettings.h"
#include "../widgets/keyshortcutsappids.h"
#include "contextmenucmdids.h"
#include "systeminfohelper.h"
#include "preview/viewer.h"
#include "localsubsystem.h"
#include "messagebox.h"
#include "settings.h"
#include "mimetype.h"

using namespace Preview;

namespace Vfs {

LocalSubsystem::LocalSubsystem()
{
	qDebug() << "LocalSubsystem::LocalSubsystem()";
	//setObjectName("LocalSubsystem");
	m_error  = NO_ERROR;
	m_status = UNCONNECTED;
	//LocalSubsystemSettings *localSubsystemSettings = new LocalSubsystemSettings;

	m_pluginData = new PluginData;
	m_pluginData->name         = "localsubsystem_plugin";
	m_pluginData->version      = "0.5";
	m_pluginData->description  = QObject::tr("Plugin supports local file system.");
	m_pluginData->plugin_class = "subsystem";
	m_pluginData->mime_icon    = ""; // (used in configuration window, higher priority than own_icon)
	m_pluginData->own_icon     = 0;  // QIcon pointer. (used in configuration window)
	m_pluginData->mime_type    = "folder:folder/symlink:folder/locked:symlink/broken";
	m_pluginData->protocol     = "file";
	m_pluginData->finding_files_fun        = 0; // ptr.to fun
	m_pluginData->finding_inside_files_fun = 0; // ptr.to fun
//	m_pluginData->popup_actions   = 0; // QActionGroup pointer  (localSubsystemSettings->popupActions())
	m_pluginData->toolBar         = 0; // QToolBar pointer      (localSubsystemSettings->toolBar())
	m_pluginData->configPage      = 0; // QWidget pointer       (localSubsystemSettings->configPage())

	Settings settings;
	m_lsw = new LocalSubsystemWatcher();
	connect<void(LocalSubsystemWatcher::*)(const QString &)>(m_lsw, &LocalSubsystemWatcher::directoryChanged,  this, &LocalSubsystem::slotWatcher_DirectoryModified); // signal is overloaded
	m_lsw->setLock(false); // start watching
//
	m_processedFiles = new FileInfoList();
	m_postMovedFiles = new FileInfoList();

	m_bInitedLeftAndRight = false;
	m_operationFinished = false;
	m_bCompleterMode = false;

	m_bInitOperation   = true;
	m_bSaveAttributes  = true;
	m_bAlwaysOverwrite = false; // in involved operation (here only Copy/Move) is getting from config.
	m_bSkipCurrentFile = false;
	m_bNoneOverwriting = false;
	m_bFollowByLink    = false;
	m_bWeightBefore    = false; // don't weigh before operation
	m_bBreakOperation  = false;
	m_bFileHasBeenOpened = false;
	m_bHardLink = false; // make symlinks

	m_pNewAttributes = nullptr;
	m_pFileBuffer = nullptr;
	clearFilesCounter();
	m_nFileBufferSize = 1024*1024; //(1024*1024)*1; // 1 MB buffer

	m_recursiveOperation = NO_COMMAND;
	m_currentOperation = NO_COMMAND;
	m_currentCommand = NO_COMMAND;

	m_sCurrentUserName = Vfs::getCurrentUserName();
	m_nCurrentUserID = Vfs::getCurrentUserId();
	m_sRequestingPanel_open = "";

	m_bShowMsgCannotSetAttributes = true;
	m_bShowMsgCannotCreateLink = true;
	m_bShowMsgCannotCheckFile = true;
	m_bShowMsgCannotDelete = true;
	m_bShowMsgCannotWeigh = true;

// 	m_bRemoveToTrash = false;  // actual remove
// 	m_sTrashPath = "";
	connect(&m_operationTimer, &QTimer::timeout, this, &LocalSubsystem::slotFileProcessing);
}


LocalSubsystem::~LocalSubsystem()
{
// 	delete m_pluginData; // removed in PluginFactory::~PluginFactory

	m_processedFiles->clear();
	m_postMovedFiles->clear();
	delete m_processedFiles;
	delete m_postMovedFiles;
	if (m_pFileBuffer != nullptr) {
		delete m_pFileBuffer;  m_pFileBuffer = nullptr;
	}
	delete m_lsw;
	while (! m_stackOfDIRptrs.isEmpty())
		::closedir(m_stackOfDIRptrs.pop());
	delete m_pNewAttributes;
	qDebug() << "LocalSubsystem::~LocalSubsystem. DESTRUCTOR";
}


void LocalSubsystem::init( QWidget *pParent, bool )
{
	m_currentCommand = NO_COMMAND;
	m_pParent = pParent;

	qDebug() << "LocalSubsystem::init.";
}

void LocalSubsystem::clearFilesCounter()
{
	m_nDirCounter     = 0;
	m_nFileCounter    = 0;
	m_totalWeight     = 0;
	m_realTotalWeight = 0;
	m_nOpFileCounter  = 0;
	m_nMatchedFiles   = 0;
	m_nSymlinksCounter = 0;
	m_nSkippedFiles   = 0;
}

void LocalSubsystem::clearStacks()
{
	m_stackOfDIRptrs.clear();
	m_stackForFirstInodeOfDir.clear();
	m_stackOfDirInode.clear();
	m_stackForThrowedOffInode.clear();
	m_stackForCurrLocInDir.clear();
}

void LocalSubsystem::setBreakOperation()
{
	if (m_recursiveOperation != NO_COMMAND) {
		qDebug() << "LocalSubsystem::setBreakOperation() m_recursiveOperation" << m_recursiveOperation;
		m_status = OPR_STOPPED;
		m_error  = NO_ERROR;
		m_bBreakOperation = true;
	}
}

void LocalSubsystem::setPauseOperation( bool bPause )
{
	qDebug() << "LocalSubsystem::setPauseOperation. Pause:" << bPause << "m_recursiveOperation" << m_recursiveOperation;
	m_bPauseOperation = bPause;
	m_error = NO_ERROR;
	if (bPause) {
		m_pausedStatus = m_status;
		m_status = OPR_PAUSED;
	} else {
		m_status = m_pausedStatus;
		slotStartProcessDir(); //QTimer::singleShot(0, this, &LocalSubsystem::slotStartProcessDir);
	}
}

void LocalSubsystem::updateContextMenu( ContextMenu *pContextMenu, KeyShortcuts *pKS )
{
	qDebug() << "LocalSubsystem::updateContextMenu.";
	m_pContextMenu = pContextMenu;

	m_pContextMenu->addSeparator();
	m_pContextMenu->addAction(tr("Make &link(s)"), CMD_Makelink,    pKS->keyCode(MakeALink_APP));
	m_pContextMenu->addAction(tr("&Edit symlink"), CMD_EditSymlink, pKS->keyCode(EditCurrentLink_APP));

	m_pContextMenu->setVisibleAction(CMD_Archive_SEP, false);
	m_pContextMenu->setVisibleAction(CMD_ExtractTo,   false);
	m_pContextMenu->setVisibleAction(CMD_ExtractHere, false);
}

FileInfoList LocalSubsystem::listDirectory() const
{
	qDebug() << "LocalSubsystem::listDirectory:" << (m_bCompleterMode ? m_currentUriCompleter.path() : m_currentUri.path());
	FileInfoList vfiLst;
	//m_currentCommand = LIST;

	if (m_bCompleterMode) {
		if (m_currentUriCompleter.path().isEmpty()) {
			qDebug() << "--- Empty path!";
			return vfiLst;
		}
	} else {
		if (m_currentUri.path().isEmpty()) {
			qDebug() << "--- Empty path!";
			return vfiLst;
		}
	}
	//m_status = LISTING; // commented due to const method
	//emit signalStatusChanged(Vfs::LISTING);
// 	QDir dir(m_currentUri.path());
	QDir dir;
	if (m_bCompleterMode)
		dir.setPath(m_currentUriCompleter.path());
	else
		dir.setPath(m_currentUri.path());

	dir.setFilter(QDir::AllEntries | QDir::Hidden | QDir::System);
	QFileInfoList lst = dir.entryInfoList();

	QString sMime, sPerm = "----------";
	QString sOwner, sGroup;

	for (QFileInfoList::const_iterator it = lst.begin(); it != lst.end(); ++it) {
		if (m_bCompleterMode && ! it->isDir())
			continue;

		if (it->fileName() == ".")
			continue;

		if (it->fileName() != "..")
			sMime = MimeType::getFileMime(it->absoluteFilePath(), it->isDir(), it->isSymLink(), true);
		else
		if (m_currentUri.path() != QDir::rootPath())
			sMime = "folder/up"; // for ".." it->absoluteFilePath() returns absolute path
		else
			continue;

		sPerm = permissionsAsString(it->absoluteFilePath());
		//qDebug() << "fileName:" << it->fileName() << "perm:" << sPerm << "mime: " << sMime;
		sOwner = it->owner().isEmpty() ? QString::number(it->ownerId()) : it->owner();
		sGroup = it->group().isEmpty() ? QString::number(it->groupId()) : it->group();
		if (it->isSymLink()) {
			sOwner = QFileInfo(m_currentUri.path()).owner();
			sGroup = QFileInfo(m_currentUri.path()).group();
		}
		FileInfo *item = new FileInfo(
					it->fileName(), it->absolutePath(), (it->isDir() ? 0 : it->size()),
					it->isDir(), it->isSymLink(),
					it->lastModified(), sPerm, sOwner, sGroup,
					sMime, it->symLinkTarget());
		vfiLst.append(item);
// 		qDebug() << "fileName:" << item->fileName() << "isDir:" << item->isDir() << "isSymLink:" << item->isSymLink();
	}

	//if (lst.count() > 0)
	//	m_status = LISTED;

	//emit signalStatusChanged(Vfs::LISTED);

	return vfiLst;
}

FileInfo *LocalSubsystem::root( bool bEmptyName )
{
	static QString fakeRoot = QDir::separator();
	QFileInfo fi(fakeRoot);

	return new FileInfo(bEmptyName ? "" : "..", fakeRoot, fi.size(), fi.isDir(), false, fi.lastModified(), "----------", fi.owner(), fi.group(), "folder/up");
}

FileInfo *LocalSubsystem::fileInfo( const QString &sAbsFileName )
{
	QFileInfo fi(sAbsFileName);
	if (! fi.exists()) {
		qDebug() << "LocalSubsystem::fileInfo. File or directory:" << sAbsFileName << "doesn't exist!";
		return new FileInfo("", "", 0, false, false, QDateTime(), "", "", "", "");
	}
	// -- get mimetype for current file
	QString sMime    = MimeType::getFileMime(sAbsFileName);
	QString sExtMime = sMime + ":" + MimeType::getMimeIcon(sMime); // example: application/x-bzip-compressed-tar:package-x-generic

	QString sPerm = permissionsAsString(sAbsFileName);
	FileInfo *item = new FileInfo(
					fi.fileName(),
					fi.absolutePath(),
					fi.size(),
					fi.isDir(),
					fi.isSymLink(),
					fi.lastModified(),
					sPerm,
					fi.owner(),
					fi.group(),
					sExtMime
				);

	return item;
}

QString LocalSubsystem::tildeParsing( const QString &sInpPath )
{
	QString sPath = sInpPath;
	if (sPath.endsWith("/~") || sPath.indexOf('~') == 0)
		sPath = QDir::homePath();

	return sPath;
}

bool LocalSubsystem::canChangeDirTo( const QString &sInpAbsPath )
{
	//qDebug() << "LocalSubsystem::canChangeToDir.";
	if (sInpAbsPath.isEmpty()) {
		qDebug() << "LocalSubsystem::canChangeToDir. Empty input path!";
		return false;
	}
	QString sAbsPath = tildeParsing(sInpAbsPath);

	QFileInfo fi(sAbsPath);
	if (! fi.exists()) {
		return false;
	}
	if (! fi.isReadable()) {
		return false;
	}
	if (! fi.isExecutable()) {
		return false;
	}

	return true;
}

bool LocalSubsystem::canOpenFileForReading( FileInfo *pFileInfo )
{
	//qDebug() << "LocalSubsystem::canOpenFileForReading.";
	m_error = NO_ERROR;
	bool bStatus = true;
	if (pFileInfo == nullptr) {
		qDebug() << "LocalSubsystem::canOpenFileForReading. Empty input file name!";
		bStatus = false;
		m_error = CANNOT_READ;
	}
	QString sAbsFileName = tildeParsing(pFileInfo->absoluteFileName());

	QFileInfo fi(sAbsFileName);
	if (! fi.exists()) {
		bStatus = false;
		m_error = DOES_NOT_EXIST;
	}
	if (! fi.isReadable()) {
		bStatus = false;
		m_error = CANNOT_READ;
	}

	m_status = bStatus ?  READY_TO_READ : NOT_READY_TO_READ;
	emit signalStatusChanged(m_status, m_sRequestingPanel); //QTimer::singleShot(0, this, &LocalSubsystem::slotStatusChanged);

	return bStatus;
}

void LocalSubsystem::open( const URI &uri, const QString &sRequestingPanel, bool bCompleter )
{
	m_bCompleterMode = bCompleter;
	QString sAbsPath = QDir::cleanPath(uri.path());
	if (sAbsPath != QDir::rootPath())
		sAbsPath += QDir::separator();
	sAbsPath = tildeParsing(sAbsPath);
	qDebug() << "LocalSubsystem::open. new path:" << sAbsPath << "bCompleter:" << bCompleter << "ReqPanel:" << sRequestingPanel;
	if (m_bCompleterMode && m_currentUriCompleter.toString() == sAbsPath) {
		qDebug() << "LocalSubsystem::open. Came path for already opened directory (QCompleter::splitPath issue).";
		return;
	}
	bool bDirCanChange = canChangeDirTo(sAbsPath);
	m_currentCommand = LIST;

	if (! bDirCanChange) {
		qDebug() << "  -- Cannot change directory to:" << sAbsPath;
		m_error  = CANNOT_OPEN;
		m_status = ERROR_OCCURED;
	}
	else {
		if (m_recursiveOperation != NO_COMMAND) { // handle break operation
			setBreakOperation();
			if (! m_bCompleterMode)
				m_breakOpURI = uri;
// 			m_sRequestingPanel_breakOp = sRequestingPanel;
			QTimer::singleShot(0, this, &LocalSubsystem::slotOpenLastAfterBreak);
			return;
		}
		if (! m_bCompleterMode) {
			m_watchedDirsMap.insert(sRequestingPanel, uri.toString()); // it replaces path related with sRequestingPanel
			QStringList slWatchedDirs = m_watchedDirsMap.values();
			slWatchedDirs.removeDuplicates();
			m_lsw->clear();
			m_lsw->addPaths(slWatchedDirs);
			qDebug() << "  -- LSW watchedDirs:" << m_lsw->directories();
			m_currentUri = uri;
			m_currentUri.setPath(sAbsPath);
		}
		else {
			m_currentUriCompleter = uri;
			m_currentUriCompleter.setPath(sAbsPath);
		}
		m_error  = NO_ERROR;
		m_status = m_bCompleterMode ? LISTED_COMPL : LISTED;

		if (! m_bCompleterMode) {
			if (sRequestingPanel.indexOf("left") == 0)
				m_sLeftPanelPath = sAbsPath;
			else if (sRequestingPanel.indexOf("right") == 0)
				m_sRightPanelPath = sAbsPath;
			m_bInitedLeftAndRight = (! m_sLeftPanelPath.isEmpty() && ! m_sRightPanelPath.isEmpty());
			m_theSameDir = ((m_sLeftPanelPath == m_sRightPanelPath) && m_bInitedLeftAndRight);
		}
	}
	//qDebug() << "LocalSubsystem::open. QFileSystemWatcher * m_currentUri: " << m_currentUri.toString() << "before emit signalStatusChanged m_status:" << m_status;
// 	m_sRequestingPanel_open = sRequestingPanel; // doesn't reliable, because would be set the name of last listed panel
	emit signalStatusChanged(m_status, sRequestingPanel);
}

void LocalSubsystem::openWith( FileInfoToRowMap *selectionMap, const URI &uri, const QString &sRequestingPanel )
{
	Q_UNUSED(uri);
	qDebug() << "LocalSubsystem::openWith, selected:" << selectionMap->count() << "file(s)";
	m_processedFiles->clear();

	QMap<FileInfo *, int>::const_iterator it = selectionMap->constBegin();
	while (it != selectionMap->constEnd()) {
		m_processedFiles->append(it.key());
		++it;
	}
	m_error  = NO_ERROR;
	m_status = OPEN_WITH;
	m_currentCommand = OPEN_WITH_CMD;
	m_recursiveOperation = NO_COMMAND;
	m_operationFinished = true;

	emit signalStatusChanged(m_status, sRequestingPanel); // for showing operation status on status bar
}

void LocalSubsystem::rename( FileInfo *fileInfoOldName, const QString &sAbsNewName, const QString &sRequestingPanel )
{
	QString sAbsOldName = fileInfoOldName->absoluteFileName();
	if (sAbsNewName.isEmpty() || sAbsOldName == sAbsNewName)
		return;

	qDebug() << "LocalSubsystem::rename. oldName:" << sAbsOldName << "newName:" << sAbsNewName;
	m_lsw->setLock(true);
	m_currentCommand = RENAME;
	m_recursiveOperation = NO_COMMAND;
	// m_currentUri.path() - NOT USABLE, because contains path for last listed directory (left or right panel)
	QDir dir;
	m_error  = (dir.rename(sAbsOldName, sAbsNewName)) ? NO_ERROR : CANNOT_RENAME;
	m_status = (m_error == NO_ERROR) ? RENAMED : ERROR_OCCURED;

	QString sNewName = sAbsNewName.right(sAbsNewName.length()-sAbsNewName.lastIndexOf(QDir::separator())-1);
	QString sMime    = MimeType::getFileMime(sAbsNewName);

	FileInfo *pNewFileInfo = new FileInfo(fileInfoOldName);
	pNewFileInfo->setData(COL_NAME, sNewName);
	pNewFileInfo->setData(COL_MIME, sMime);

	m_processedFiles->clear();
	m_processedFiles->append(pNewFileInfo); // used for updating FileListView
	m_slListOfItemsToModify.clear();
	m_slListOfItemsToModify.append(QFileInfo(sAbsOldName).fileName());

	emit signalStatusChanged(m_status, sRequestingPanel);
	m_sRequestingPanel_open = sRequestingPanel; // used in slotWatcher_DirectoryChanged
	m_lsw->setLock(false); // unlock subsystem watcher
}

void LocalSubsystem::makeLink( FileInfoToRowMap *pSelectionMap, const QString &sTargetLinkName, bool bHardLink, const QString &sRequestingPanel )
{
	qDebug() << "LocalSubsystem::makeLink. Amount of links:" << pSelectionMap->count() << "targetLinkName:" << sTargetLinkName << "hardLink:" << bHardLink;
// 	m_sRequestingPanel = sRequestingPanel;  // initialized in slotStartNextJob
// 	m_OpTotalWeight = 0; // initialized in initCopying()
	m_bHardLink = bHardLink;
	m_recursiveOperation = NO_COMMAND;
	m_nSelectedFiles = pSelectionMap->count();

// 	m_currentCommand = CREATE_LINK;  // initialized in slotStartNextJob
// 	m_currentOperation = CREATE_LINK; // initialized in slotStartNextJob
// 	m_error  = NO_ERROR;  // initialized in initCopying()
// 	m_status = CREATING_LINK; // initialized in initCopying()
// 	m_lsw->setLock(true);
	m_bAlwaysOverwrite = false;
// 	m_bNoneOverwriting = false; // initialized in initCopying()
	m_operationFinished = false;
// 	m_eUpdatingFilesWith = NONEupd; // initialized in initCopying()
	// - some of above are initialized in initCopying
	m_bShowMsgCannotCreateLink = true;
	m_opQueue.clear();
// 	m_processedFiles->clear(); // initialized in initCopying()

	// ----
	addOperations2queue(pSelectionMap, CREATE_LINK, sTargetLinkName, sRequestingPanel);
	QTimer::singleShot(0, this, &LocalSubsystem::slotStartNextJob);
}

void LocalSubsystem::editSymLink( FileInfo *pFileInfo, const QString &sSymLinkNewTarget, const QString &sRequestingPanel )
{
	QString sSymLink = pFileInfo->absoluteFileName();
	qDebug() << "LocalSubsystem::editSymLink. SymLink:" << sSymLink << "SymLinkNewTarget:" << sSymLinkNewTarget;

	m_status = EDITING_LINK;
	m_currentCommand = EDIT_LINK;
// 	m_sRequestingPanel = sRequestingPanel;
	m_lsw->setLock(true); // lock subsystem watcher
	m_recursiveOperation = NO_COMMAND;

	m_error = (::unlink(QFile::encodeName(sSymLink)) == 0) ? NO_ERROR : CANNOT_EDIT_LINK;
	if (m_error != NO_ERROR) {
		m_sProcessedFileName = sSymLink;
		m_status = ERROR_OCCURED;
	} else { // old symlink removed successfuly, create new one
		if (makeNextLink(sSymLinkNewTarget, sSymLink)) {
			QFileInfo fi(sSymLink);
			qint64  nSize = sSymLinkNewTarget.length();
			QString sPerm = permissionsAsString(sSymLink);
			QString sOwner = QFileInfo(pFileInfo->filePath()).owner();
			QString sGroup = QFileInfo(pFileInfo->filePath()).group();
			QString sMime = MimeType::getFileMime(sSymLink, fi.isDir(), true);
			QString sSymLinkTarget = fi.symLinkTarget();
			FileInfo *pNewFileInfo = new FileInfo(fi.fileName(), fi.path(), nSize, fi.isDir(), true, fi.lastModified(), sPerm, sOwner, sGroup, sMime, sSymLinkTarget);
			m_processedFiles->clear();
			m_processedFiles->append(pNewFileInfo); // used for updating FileListView
// 			m_slListOfItemsToModify.clear();
// 			m_slListOfItemsToModify.append(QFileInfo(sSymLink).fileName());
			m_status = EDITED_LINK;
		}
	}
	emit signalStatusChanged(m_status, sRequestingPanel);

	m_lsw->setLock(false); // lock subsystem watcher
	// -- cleaning
	m_processedFiles->clear();
	m_slListOfItemsToModify.clear();
	m_operationFinished = true;
	m_status = UNKNOWN_STAT;
	m_error = NO_ERROR;
	m_sRequestingPanel_open = sRequestingPanel; // used in slotWatcher_DirectoryChanged
}

void LocalSubsystem::makeDir( const QString &sParentDirPath, const QString &sInNewDirsList, const QString &sRequestingPanel )
{
	if (sInNewDirsList.isEmpty())
		return;

	m_lsw->setLock(true); // lock subsystem watcher
	m_processedFiles->clear();
	m_currentCommand = CREATE_DIR;
	m_recursiveOperation = NO_COMMAND;

	QDir dir;
	FileInfo *pNewFI;
	bool bCameAbsPath, bDirExists = false, bErrorAppeared = false;
	QString sNewDirsList = sInNewDirsList;
	sNewDirsList.replace('|', "/|");
	QStringList newDirsList = sNewDirsList.split('|');
	QString sInpDirName, sDirName, sDirPath, sChkDirName, sCreatedDirName;
	QString sMime, sPerm;

	for (int i=0; i<newDirsList.count(); i++) {
		sInpDirName = newDirsList[i];
		sDirName = sParentDirPath + sInpDirName;
		int chkDirNameStartId = sInpDirName.startsWith('/') ? 1 : 0;
		sChkDirName = sParentDirPath + sInpDirName.left(sInpDirName.indexOf('/', chkDirNameStartId));
		bCameAbsPath = (sInpDirName.startsWith('/'));
		if (bCameAbsPath) {
			sChkDirName = sInpDirName;
			sChkDirName.remove(sChkDirName.length()-1, 1);
		}
		pNewFI = nullptr;
		bDirExists = QFileInfo(sDirName).exists();
		qDebug() << "LocalSubsystem::makeDir. Absolute dirName:" << sDirName << "exists:" << bDirExists;

		if (bDirExists) {
			m_error = ALREADY_EXISTS;
			m_status = ERROR_OCCURED;
			// prepare name and path for fake item
			if (bCameAbsPath) {
				sDirName = sInpDirName.right(sInpDirName.length()-sInpDirName.lastIndexOf('/', -2) - 1);
				sDirName.remove(sDirName.length()-1, 1);
				sDirPath = sChkDirName;
				sDirPath.replace("/"+sDirName, "");
			}
			else {
				sDirName = sInpDirName.left(sInpDirName.indexOf('/', chkDirNameStartId));
				sDirPath = sParentDirPath.mid(0, sParentDirPath.length()-1);
			}
			// fake item only for setting focus on an existing item
			pNewFI = new FileInfo(sDirName, sDirPath, 0, true, false, QDateTime(), "", "", "", "");
		}
		else { // doesn't exist try to create
			m_error = (dir.mkpath(sDirName)) ? NO_ERROR : CANNOT_CREATE_DIR;
			m_status = (m_error == NO_ERROR) ? CREATED_DIR : ERROR_OCCURED;
			m_sProcessedFileName = sDirName;

			if (m_status == CREATED_DIR && ! bDirExists) {
				// -- prepare path for QFileInfo
				sCreatedDirName = bCameAbsPath ? sDirName : sParentDirPath + sInpDirName.left(sInpDirName.indexOf('/'));
				if (sCreatedDirName.endsWith(QDir::separator()))
					sCreatedDirName = sCreatedDirName.left(sCreatedDirName.length()-1);
				// -- necessary to update data model
				QFileInfo fi(sCreatedDirName);
				sMime = "folder";
				sPerm = permissionsAsString(sCreatedDirName);
				if (bCameAbsPath) {
					sDirName = sCreatedDirName.right(sCreatedDirName.length()-sCreatedDirName.lastIndexOf('/', -2) - 1);
					sDirPath = sCreatedDirName.replace("/"+fi.fileName(), "");
				}
				else {
					sDirName = fi.fileName();
					sDirPath = sParentDirPath.mid(0, sParentDirPath.length()-1);
				}
				pNewFI = new FileInfo(sDirName, sDirPath, fi.size(), true, false, fi.lastModified(), sPerm, fi.owner(), fi.group(), sMime);
			}
		}
		if (pNewFI != nullptr)
			m_processedFiles->append(pNewFI);

		if (m_error != NO_ERROR) { // if an error appeared then most likely will repeats for all items
			bErrorAppeared = true;
			emit signalStatusChanged(m_status, sRequestingPanel);
		}
	}

	if (! bErrorAppeared) { // update data model with newly created items
		m_operationFinished = true;
		emit signalStatusChanged(m_status, sRequestingPanel);
	}
	jobFinished();
	m_lsw->setLock(false); // unlock subsystem watcher
	m_sRequestingPanel_open = sRequestingPanel; // used in slotWatcher_DirectoryChanged
}

void LocalSubsystem::makeFile( const QString &sParentDirPath, const QString &sInFileNameList, const QString &sRequestingPanel )
{
	if (sInFileNameList.isEmpty())
		return;

	m_lsw->setLock(true);
	m_processedFiles->clear();
	m_currentCommand = CREATE_FILE;
	m_recursiveOperation = NO_COMMAND;

	FileInfo *pNewFI;
	bool bFileExists = false, bErrorAppeared = false;
	QString sNewFileName, sFullFileName, sMime, sPerm;
	QStringList newFilesList = sInFileNameList.split('|');

	for (int i=0; i<newFilesList.count(); i++) {
		sNewFileName = newFilesList[i];
		sFullFileName = sParentDirPath + sNewFileName;
		bFileExists = QFileInfo(sFullFileName).exists();
		pNewFI = nullptr;
		qDebug() << "LocalSubsystem::makeFile. NewFileName:" << sNewFileName << "exists:" << bFileExists;

		if (bFileExists) {
			m_error = ALREADY_EXISTS;
			m_status = ERROR_OCCURED;
			// fake item only for setting focus on an existing item
			pNewFI = new FileInfo(sNewFileName, sParentDirPath.mid(0, sParentDirPath.length()-1), 0, false, false, QDateTime(), "", "", "", "");
		}
		else {
			QFile emptyFile(sFullFileName);
			m_error = emptyFile.open(QIODevice::ReadWrite | QIODevice::Append) ? NO_ERROR : CANNOT_CREATE_FILE;
			m_status = (m_error == NO_ERROR) ? CREATED_FILE : ERROR_OCCURED;
			m_sProcessedFileName = sFullFileName;
			emptyFile.close();

			if (m_status == CREATED_FILE) {
				// -- necessary for update model
				QFileInfo fi(sFullFileName);
				sMime = MimeType::getFileMime(sFullFileName);
				sPerm = permissionsAsString(sFullFileName);
				pNewFI = new FileInfo(fi.fileName(), sParentDirPath.mid(0, sParentDirPath.length()-1), fi.size(), false, false, fi.lastModified(), sPerm, fi.owner(), fi.group(), sMime);
			}
		}
		if (pNewFI != nullptr)
			m_processedFiles->append(pNewFI);

		if (m_error != NO_ERROR) { // if an error appeared then most likely will repeats for all items
			bErrorAppeared = true;
			emit signalStatusChanged(m_status, sRequestingPanel);
		}
	}

	if (! bErrorAppeared) { // update data model with newly created items
		m_operationFinished = true;
		emit signalStatusChanged(m_status, sRequestingPanel);
	}
	jobFinished();
	m_lsw->setLock(false); // unlock subsystem watcher
	m_sRequestingPanel_open = sRequestingPanel; // used in slotWatcher_DirectoryChanged
}

void LocalSubsystem::remove( FileInfoToRowMap *pSelectionMap, const URI &uri, bool bWeighBefore, const QString &sRequestingPanel, bool bForceRemoveAll )
{
	if (pSelectionMap == nullptr) {
		qDebug() << "LocalSubsystem::remove. Empty selectionMap.";
		return;
	}
	int nSelectedItems = pSelectionMap->size();
	qDebug() << "LocalSubsystem::remove. SelectedItems:" << nSelectedItems << "WeighBefore:" << bWeighBefore;
	if (nSelectedItems == 0) {
		qDebug() << "LocalSubsystem::remove. Not selected items.";
		return;
	}
	m_sRequestingPanel = sRequestingPanel;
	m_nSelectedFiles = pSelectionMap->count();
	m_opQueue.clear();
	m_processedFiles->clear();
	m_totalWeight = 0;
	m_realTotalWeight = 0;
	m_bForceRemoveAll = bForceRemoveAll;
	m_bShowMsgCannotDelete = true;
	m_bChangeRecursivelyAttributes = true; // prevents setting not recursive operation during weighing
	if (bWeighBefore) {
		weighing2queue(pSelectionMap, uri, sRequestingPanel);
	}
	m_operationFinished = false;
	m_bInitOperation = true;
// 	m_nFileCounterR = 0;
	m_nDirCounterR  = 0;
	m_OpTotalWeight = 0;
	addOperations2queue(pSelectionMap, REMOVE, "", sRequestingPanel);

	QTimer::singleShot(0, this, &LocalSubsystem::slotStartNextJob);
}

void LocalSubsystem::setAttributes( FileInfoToRowMap *pSelectionMap, const URI &uri, FileAttributes *pChangedAttrib, bool bWeighBefore, const QString &sRequestingPanel )
{
	if (pSelectionMap == nullptr) {
		qDebug() << "LocalSubsystem::setAttributes. Empty SelectionMap.";
		return;
	}
	if (pChangedAttrib == nullptr) {
		qDebug() << "LocalSubsystem::setAttributes. Empty ChangedAttrib.";
		return;
	}
	int selectedItemsNum = pSelectionMap->size();
	qDebug() << "LocalSubsystem::setAttributes. Selected items:" << selectedItemsNum << "WeighBefore:" << bWeighBefore;
	if (selectedItemsNum == 0) {
		qDebug() << "LocalSubsystem::setAttributes. Not selected items.";
		return;
	}
	m_pNewAttributes = pChangedAttrib; // used for delete external obj.
	m_sRequestingPanel = sRequestingPanel;

	m_opQueue.clear();
	m_processedFiles->clear();
	m_slListOfItemsToModify.clear();
	m_totalWeight = 0;
	m_realTotalWeight = 0;
	if (bWeighBefore) {
		weighing2queue(pSelectionMap, uri, sRequestingPanel);
	}
	m_bInitOperation = true;
	m_operationFinished = false;
	m_bShowMsgCannotSetAttributes = true;
// 	m_nFileCounterR = 0;
	m_nDirCounterR  = 0;
	m_OpTotalWeight = 0;
	m_eAttribChgStat = AttrChgOK;
	addOperations2queue(pSelectionMap, SET_ATTRIBUTES_CMD, "", sRequestingPanel);
	m_bChangeRecursivelyAttributes = pChangedAttrib->bRecursiveChanges;
	m_changeAttributesFor = pChangedAttrib->changePermFor;

	// -- init access and modified time
	m_uTimeBuf.actime  = -1;
	m_uTimeBuf.modtime = -1;
	if (pChangedAttrib->changeAccModTimeFor != FileAttributes::NONE) {
		m_uTimeBuf.actime  = pChangedAttrib->dtLastAccessTime.toTime_t();
		m_uTimeBuf.modtime = pChangedAttrib->dtLastModifiedTime.toTime_t();
	}
	// -- init owner Id and group Id
	m_nNewOwnerId = -1;
	m_nNewGroupId = -1;
	if (pChangedAttrib->changeOwnGrpFor != FileAttributes::NONE) {
		m_nNewOwnerId = Vfs::getUserId(pChangedAttrib->sOwner);
		m_nNewGroupId = Vfs::getGroupId(pChangedAttrib->sGroup);
	}

	m_lsw->setLock(true);
	QTimer::singleShot(0, this, &LocalSubsystem::slotStartNextJob);
}

void LocalSubsystem::searchFiles( FileInfoToRowMap *pSelectionMap, const URI &uri, const FindCriterion &fcFindCriterion, const QString &sRequestingPanel )
{
	if (fcFindCriterion.isEmpty()) {
		qDebug() << "LocalSubsystem::searchFiles. Empty findCriterion.";
		return;
	}
	m_findCriterion = fcFindCriterion;
	m_findRegExpLst.clear();
	QStringList slNameLst = m_findCriterion.nameLst.split('|');
	foreach (QString sName, slNameLst)
		m_findRegExpLst.append(QRegExp(sName, m_findCriterion.caseSensitivity, m_findCriterion.patternSyntax));

	m_sRequestingPanel = sRequestingPanel;
	m_opQueue.clear();
// 	weighing2queue(selectionMap, uri, sRequestingPanel);
	m_bChangeRecursivelyAttributes = true; // prevents setting not recursive operation during weighing
	m_operationFinished = false;
	m_bInitOperation = true;
	m_bShowMsgCannotCheckFile = true;
// 	addOperations2queue(selectionMap, FIND, sRequestingPanel);
	m_opQueue << Operation(FIND, nullptr, "", sRequestingPanel);
	m_bFindRecursively = fcFindCriterion.findRecursive;

	qDebug() << "LocalSubsystem::searchFiles. invokes slotStartNextJob";
	QTimer::singleShot(0, this, &LocalSubsystem::slotStartNextJob);
}

QString LocalSubsystem::searchingResult()
{
	m_sSearchingResult = QString("%1 %2 %3").arg(m_nMatchedFiles).arg(m_nFileCounter).arg(m_nDirCounter);
	return m_sSearchingResult;
}

void LocalSubsystem::copyFiles( FileInfoToRowMap *pSelectionMap, const QString &sTargetPath, bool bMove, bool bWeighBefore, const QString &sRequestingPanel )
{
	if (pSelectionMap == nullptr) {
		qDebug() << "LocalSubsystem::copyFiles. Empty selectionMap.";
		return;
	}
	int selectedItemsNum = pSelectionMap->size();
	bool bDifferentTargetPath = (m_currentUri.toString() != sTargetPath);
	qDebug() << "LocalSubsystem::copyFiles. Selected items:" << selectedItemsNum << "weighBefore:" << bWeighBefore << "targetPath different than current:" << bDifferentTargetPath;
	if (selectedItemsNum == 0) {
		qDebug() << "LocalSubsystem::copyFiles. Not selected items.";
		return;
	}
	m_sRequestingPanel = sRequestingPanel;
	m_sCopyTargetPath  = sTargetPath; // need to set for newly created FileInfo (what is put to second panel)
	m_nSelectedFiles = pSelectionMap->count();
	// Better idea is read below on higher lever, to not need force programmer of other file systems implement this here.
	// Current fun.should use as parameter pointer to container/struct with properly params
	Settings settings;
	m_bSaveAttributes  = settings.value("vfs/SaveAttributes", true).toBool();
	m_bAlwaysOverwrite = settings.value("FilesSystem/AlwaysOverwrite", false).toBool();
// 	bWeighBeforeCopying = m_pSettings->value("vfs/WeighBeforeCopying", true).toBool();
	if (m_bSaveAttributes)
		m_pNewAttributes = new FileAttributes; // for all changed attribues. Default value is FileAttributes::FILES_AND_DIRS

	m_opQueue.clear();
	m_processedFiles->clear();
	m_postMovedFiles->clear();
	m_totalWeight = 0;
	m_realTotalWeight = 0;
	m_bChangeRecursivelyAttributes = true; // prevents setting not recursive operation during weighing
	if (bWeighBefore) {
		URI uri; // unused in weighing2queue
		weighing2queue(pSelectionMap, uri, sRequestingPanel);
	}
	m_operationFinished = false;
	m_bInitOperation = true;
	m_bShowMsgCannotDelete = true; // used by MOVE operation
// 	m_nFileCounterR = 0;
	m_nOpFileCounter = 0;
	m_nDirCounterR  = 0;
	m_OpTotalWeight = 0;
	addOperations2queue(pSelectionMap, (bMove ? MOVE : COPY), sTargetPath, sRequestingPanel);

	QTimer::singleShot(0, this, &LocalSubsystem::slotStartNextJob);
}

void LocalSubsystem::weigh( FileInfoToRowMap *selectionMap, const URI &uri, const QString &sRequestingPanel, bool bWeighingAsSubCmd, bool bResetCounters )
{
	m_opQueue.clear();
	m_processedFiles->clear();
	m_sRequestingPanel = sRequestingPanel;
	m_currentCommand = WEIGH;
	m_bChangeRecursivelyAttributes = true; // prevents setting not recursive operation during weighing
	weighing2queue(selectionMap, uri, sRequestingPanel, bWeighingAsSubCmd, bResetCounters);
	QTimer::singleShot(0, this, &LocalSubsystem::slotStartNextJob);
}

void LocalSubsystem::weighItem( FileInfo *fileInfo, const URI &uri, const QString &sRequestingPanel, bool bResetCounters )
{
	Q_UNUSED(uri);
	m_opQueue.clear();
	m_processedFiles->clear();
	initWeighing(bResetCounters, sRequestingPanel);
	m_bWeighingAsSubCmd = false;
	m_currentCommand = WEIGH;
	m_sRequestingPanel = sRequestingPanel;
	m_opQueue << Operation(WEIGH, fileInfo, "", sRequestingPanel);
	QTimer::singleShot(0, this, &LocalSubsystem::slotStartNextJob);
}

void LocalSubsystem::weighing2queue( FileInfoToRowMap *selectionMap, const URI &uri, const QString &sRequestingPanel, bool bWeighingAsSubCmd, bool bResetCounters )
{
	Q_UNUSED(uri);
	initWeighing(bResetCounters, sRequestingPanel);
	m_bWeighingAsSubCmd = bWeighingAsSubCmd;
	addOperations2queue(selectionMap, WEIGH, "", sRequestingPanel);
}

void LocalSubsystem::initWeighing( bool bResetCounters, const QString &sRequestingPanel )
{
	m_operationFinished = false;
	m_currentCommand = WEIGH;
	m_currentOperation = WEIGH;
	m_bNoneOverwriting = false;
	m_bSkipCurrentFile = false;
	m_bBreakOperation  = false;
	m_error  = NO_ERROR;
	m_status = WEIGHING;
	m_bShowMsgCannotWeigh = true;
	clearStacks();
	if (bResetCounters) {
		m_nTotalFiles = 0;
		clearFilesCounter();
	}
	emit signalStatusChanged(m_status, sRequestingPanel); // update status bar
}

void LocalSubsystem::getWeighingResult( qint64 &weight, qint64 &realWeight, uint &files, uint &dirs )
{
	weight = m_totalWeight;
	realWeight = m_realTotalWeight;
	files  = m_nFileCounter;
	dirs   = m_nDirCounter;
}

void LocalSubsystem::addOperations2queue( FileInfoToRowMap *pSelectionMap, SubsystemCommand subsystemCmd, const QString &sTargetPath, const QString &sRequestingPanel )
{
	FileInfoToRowMap::const_iterator it = pSelectionMap->constBegin();
	while (it != pSelectionMap->constEnd()) {
		m_opQueue << Operation(subsystemCmd, it.key(), sTargetPath, sRequestingPanel); // adds FileInfo pointer
		++it;
	}
}

SubsystemError LocalSubsystem::isExecutableFile( const QString &sAbsFileName ) const
{
	QFileInfo fi(sAbsFileName);

	if (fi.isReadable()) {
		if (! fi.isExecutable()) {
			qDebug() << "LocalSubsystem::isExecutableFile. File:" << sAbsFileName << "is'nt executable.";
			return CANNOT_EXECUTE;
		}
	}
	else {
		qDebug() << "LocalSubsystem::isExecutableFile. Cannot read file:" << sAbsFileName;
		return CANNOT_READ;
	}

	return NO_ERROR;
}

WritableStatus LocalSubsystem::dirWritableStatus()
{
	QFileInfo fi(m_currentUri.path());

	if (fi.isWritable()) {
		// required double check, because for some directories mounted by nfs, object QFileInfo returns incorrect status
		if (getCurrentUserId() == fi.ownerId()) {
			if (! fi.permission(QFile::WriteOwner))
				return DIR_NOT_WRITABLE;
		}
		else   // different
		if (getCurrentUserGroupId() == fi.groupId()) {
			if (! fi.permission(QFile::WriteGroup))
				return DIR_NOT_WRITABLE;
		}
		else { // different
			if (! fi.permission(QFile::WriteOther))
				return DIR_NOT_WRITABLE;
		}

		return DIR_WRITABLE;
	}

	return DIR_NOT_WRITABLE;
}

void LocalSubsystem::executeApplication( const QString &sAbsFileName )
{
	SubsystemError error;
	if ((error=isExecutableFile(sAbsFileName)) != NO_ERROR) {
		qDebug() << "LocalSubsystem::executeApplication. File:" << sAbsFileName << "hasn't permission to execute!";
		m_status = ERROR_OCCURED;
		m_error  = error;
		emit signalStatusChanged(m_status, m_sRequestingPanel);
		return;
	}
	// TODO LocalSubsystem::executeApplication. if console program then show console emulator
	QProcess *run = new QProcess(parent());
	run->start(sAbsFileName); // shows error: file doesn't exist
}

bool LocalSubsystem::isDirectoryEmpty( const QString &sDirName )
{
	bool bEmptyDir = false;
	m_error = NO_ERROR;

	if ((m_pDIR=::opendir(QFile::encodeName(sDirName+"/"))) != nullptr) {
		bEmptyDir = true;
/*		struct dirent entry;
		int error;
		m_pDirNextEntry = nullptr;
		do {
			for (;;) {
				if ((error=readdir_r(m_pDIR, &entry, &m_pDirNextEntry)) != 0) { // (thread safe function)
					qDebug() << "LocalSubsystem::isDirectoryEmpty. readdir error:" << errno;
					break;
				}
			}
			if (error != 0) {
				m_sCurrentFileName = m_pDirNextEntry->d_name;
				if (m_sCurrentFileName != "." && m_sCurrentFileName != "..") {
					bEmptyDir = false;
					break;
				}
			}
		}
		while (m_pDirNextEntry != nullptr);*/
		while ((m_pDirNextEntry=::readdir(m_pDIR)) != nullptr) {
			m_sCurrentFileName = m_pDirNextEntry->d_name;
			if (m_sCurrentFileName != "." && m_sCurrentFileName != "..") {
				bEmptyDir = false;
				break;
			}
		}
		::closedir(m_pDIR);
	}
	else {
		m_status = ERROR_OCCURED;
		m_error  = CANNOT_READ;
	}

	return bEmptyDir;
}

void LocalSubsystem::slotStartNextJob()
{
	if (m_opQueue.isEmpty() || m_status == OPR_STOPPED) {
		jobFinished();
		return;
	}
	SubsystemCommand prevCmd = m_currentCommand;

	Operation operation = m_opQueue.first();
	m_currentCommand = operation.command;  // internal command
	FileInfo *pFileInfo = operation.fileInfo;
	m_sRequestingPanel = operation.subsysMngrName;
	m_bPauseOperation = false;
	if (m_currentCommand != SET_ATTRIBUTES_CMD)
		m_bSkipCurrentFile = false;

	if (m_currentCommand == COPY || m_currentCommand == MOVE) {
		m_sTargetFileName = operation.targetPath + pFileInfo->fileName();
	}
	m_processedFileInfo = pFileInfo;

	if (m_currentCommand != WEIGH)
		m_currentOperation = m_currentCommand;
	else
	if (pFileInfo != nullptr) // WEIGH
		m_processedFiles->append(pFileInfo);

	if (m_currentOperation == CREATE_LINK) {
		m_sSourceFileName = pFileInfo->absoluteFileName();
		m_sTargetFileName = operation.targetPath;
		if (QFileInfo(m_sTargetFileName).isDir())
			m_sTargetFileName += pFileInfo->fileName();
	}

	if (pFileInfo != nullptr) // FIND
		m_sSourceFileName = pFileInfo->absoluteFileName();

	emit signalNameOfProcessedFile(m_sSourceFileName);

// 	if (prevCmd != m_currentCommand)
//		emit stateChanged((int)m_currentCommand); // sets info on the progress dlg.
	if (m_currentCommand != WEIGH && prevCmd != m_currentCommand) { // reset counters
		emit signalCounterProgress(0, FILE_COUNTER, false);
		emit signalCounterProgress(0, DIR_COUNTER,  false);
	}
	qDebug() << "LocalSubsystem::slotStartNextJob. currentCommand:" << toString(m_currentCommand) << "FileInfo:" << ((pFileInfo != nullptr) ? pFileInfo->fileName() : " nullptr")
			<< "SourceFileName:" << m_sSourceFileName << "TargetFileName:" << m_sTargetFileName << "currentOperation:" << toString(m_currentOperation);

	switch ( m_currentCommand ) {
		case WEIGH: {
			initWeighing(false, m_sRequestingPanel); // false means don't clear the counters
			weighNextItem(pFileInfo);
			if (m_status == OPR_STOPPED) {
				jobFinished();
				return;
			}
			break;
		}
		case COPY:
		case MOVE: {
			initCopying();
			initCopiedFiles(); // and start copying file
			if (m_status == OPR_STOPPED) {
				jobFinished();
				return;
			}
			break;
		}
		case REMOVE: {
			initRemoving();
			if (m_status == OPR_STOPPED) { // user selected 'Cancel'
				jobFinished();
				return;
			}
			if (! removeNextFile()) { // remove file failed or in recursive op.when first file has been removed or dir.weigh failed
				if (m_recursiveOperation == NO_COMMAND && m_opQueue.size() > 0 && m_error != NO_ERROR) // appeared an error, but there is/are some item(s) in queue
					; // only call currentCommandFinished() in this scope
				else
				if ((m_status != OPR_STOPPED && m_error != NO_ERROR) || m_error == NO_ERROR) // when removing directory
					return;
				else {
					if (m_error == CANNOT_READ || m_error == SKIP_FILE) {
						if (m_error == SKIP_FILE)
							m_error = NO_ERROR;
					}
				}
			}
			if (m_error != SKIP_FILE)
				currentCommandFinished();
			break;
		}
		case CREATE_LINK: {
			initCopying();
			initCopiedFiles(); // and make a link
			break;
		}
		case SET_ATTRIBUTES_CMD: {
			initSettingAttributes();
			if (m_status == OPR_STOPPED) { // user select 'Cancel'
				jobFinished();
				return;
			}
			if (! setAttributesNextFile()) { // set attributes directory
				if (m_status != OPR_STOPPED && m_error != NO_ERROR && m_error != SKIP_FILE)
					return;
				else
				if (m_error == CANNOT_READ || m_error == SKIP_FILE) {
					if (m_error == SKIP_FILE)
						m_error = NO_ERROR;
				}
			}
			break;
		}
		case FIND: {
			initSearchingFiles();
			if (! recursiveOperation(FIND, m_findCriterion.location)) {
				m_sProcessedFileName = m_findCriterion.location;
				currentCommandFinished();
				if (m_error == CANNOT_READ || m_error == SKIP_FILE) {
					if (m_error == SKIP_FILE)
						m_error = NO_ERROR;
				}
			}
			break;
		}
		default: break;
	}

	if (m_currentOperation != NO_COMMAND && m_currentCommand != FIND && m_recursiveOperation == NO_COMMAND) {
		if (m_currentCommand == COPY || m_currentCommand == MOVE || m_currentCommand == REMOVE) {
			if (m_error != NO_ERROR || m_nSourceFileSize == 0 || m_bSkipCurrentFile || m_bNoneOverwriting) {
				if ((m_currentCommand == MOVE || m_currentCommand == REMOVE) && m_error != NO_ERROR) {
					m_error = NO_ERROR; // to be able continue an operation
					m_status = (m_currentCommand == MOVE) ? MOVING : REMOVING;
				}
				else
					currentCommandFinished();
			}
		}
		else
			currentCommandFinished();
	}
}

bool LocalSubsystem::weighNextItem( FileInfo *pFileInfo )
{
	m_bOkStat = (::lstat64(QFile::encodeName(pFileInfo->absoluteFileName()), & m_statBuffer) == 0);
	if (pFileInfo->isSymLink()) {
		m_nSymlinksCounter++;
		QFileInfo fi(pFileInfo->absoluteFileName()); // m_sSourceFileName (set in slotStartNextJob)
		m_totalWeight     += fi.symLinkTarget().length(); // resolve link
		m_realTotalWeight += (m_bOkStat) ? ((m_statBuffer.st_size/m_statBuffer.st_blksize)+1)*m_statBuffer.st_blksize : -1;
		m_recursiveOperation = NO_COMMAND;
	}
	else {
		if (pFileInfo->isDir()) {
			if (m_opQueue.size() > 1) {
				if (! m_bChangeRecursivelyAttributes)
					m_recursiveOperation = NO_COMMAND;
				else {
					if (! isDirectoryEmpty(pFileInfo->absoluteFileName())) { // check if empty // m_sSourceFileName
						if (m_error == NO_ERROR)
							return recursiveOperation(WEIGH, pFileInfo->absoluteFileName());  // m_sSourceFileName
					}
				}
			}
			else
			if (! isDirectoryEmpty(pFileInfo->absoluteFileName())) { // check if empty // m_sSourceFileName
				if (m_error == NO_ERROR)
					recursiveOperation(WEIGH, pFileInfo->absoluteFileName());  // m_sSourceFileName
			}
		}
		else { // regular file
			m_recursiveOperation = NO_COMMAND;
// 			m_nTotalFiles++; // counts in currentCommandFinished()
			m_totalWeight     +=  pFileInfo->size();
			m_realTotalWeight += (m_bOkStat) ? ((m_statBuffer.st_size/m_statBuffer.st_blksize)+1)*m_statBuffer.st_blksize : -1;
			//m_OpTotalWeight += m_statBuffer.st_size;
			emit signalCounterProgress(m_totalWeight, WEIGH_COUNTER, true);
		}
	}
	if (m_error == NO_ERROR)
		emit signalCounterProgress(m_totalWeight, WEIGH_COUNTER, true);

	//currentCommandFinished() for WEIGH is called in fun. slotStartNextJob

	return true;
}

bool LocalSubsystem::recursiveOperation( SubsystemCommand recursiveOperation, const QString &sSourceDirName, const QString &sTargetDirName )
{
	qDebug() << "LocalSubsystem::recursiveOperation. recursiveOperation:" << toString(recursiveOperation) << " SourceDirName:" << sSourceDirName << " TargetDirName:" << sTargetDirName;
	m_bWasDir = true;
	m_error = NO_ERROR;
	m_stackOfDirInode.clear();
	m_recursiveOperation = recursiveOperation;

	m_sAbsSrcFileName = sSourceDirName;
	if (sSourceDirName.endsWith(QDir::separator()) && sSourceDirName != QDir::separator()) // remove last dirSeparator
		m_sAbsSrcFileName.remove(sSourceDirName.length()-1, 1);
	m_sAbsSrcDirName = m_sAbsSrcFileName;

	if (recursiveOperation == COPY || recursiveOperation == MOVE) {
		m_sAbsTargetFileName = sTargetDirName;
		m_sAbsTargetDirName  = m_sAbsTargetFileName;
		m_sAbsSrcDirName     = m_sAbsSrcFileName;
		m_fInfo.setFile(m_sAbsTargetDirName);

/*		QString sDirName       = QDir(m_sAbsTargetDirName).dirName();
		QString sParentDirPath = QDir::cleanPath(m_sAbsTargetDirName+"/../");
		FileInfo *fi = new FileInfo(sDirName, sParentDirPath, 0, true, false, QDateTime(), "", "", "", "");
		if (m_currentCommand == MOVE)
			m_postMovedFiles->append(fi);
		fi->setLocation(fi->filePath()); // necessary for find item to unmark
		fi->setData(2, m_sCopyTargetPath, true); // workaround for setting path (in second panel we need to set correct path for new item)
		if (! m_renameInCopying.isEmpty()) {
			QString sFileName = m_renameInCopying.right(m_renameInCopying.length() - m_renameInCopying.lastIndexOf(QDir::separator()) - 1);
			QString sPathName = m_renameInCopying.left(m_renameInCopying.length() - sFileName.length());
			if (fi->filePath() == sPathName)
				fi->setData(0, sFileName);
			m_renameInCopying = "";
		}
		m_processedFiles->append(fi);*/

		if (! m_fInfo.exists()) {
			if (! m_dir.mkpath(m_sAbsTargetDirName)) {
			//if (::mkdir(QFile::encodeName(m_sAbsTargetDirName), 0755) == -1) {// make main copied directory
				m_sTargetFileName = m_fInfo.absolutePath(); // FileInfoExt::filePath(targetDirName) // because into currentCommandFinished will get is this value
				m_sProcessedFileName = m_sTargetFileName; // need to correct show error message
				m_error = CANNOT_CREATE_DIR;
				m_recursiveOperation = NO_COMMAND; // why?
				return false;
			}
		}
		m_nOpFileCounter++;
		m_sProcessedFileName = m_sTargetFileName; // need to correct inserting an item
		emit signalCounterProgress(++m_nDirCounter, DIR_COUNTER);
		emit signalDataTransferProgress(0, 0, 1, false);
		emit signalTotalProgress((100 * m_nOpFileCounter) / m_nTotalFiles, m_totalWeight);
	}
	else
	if (recursiveOperation == FIND) {
		m_bFindAll = (m_findCriterion.filesType == FindCriterion::ALL);
	}

	DIR *mainDirPtr = nullptr;
	if ((mainDirPtr=::opendir(QFile::encodeName(m_sAbsSrcDirName))) == 0) { // only for readable checking
		qDebug() << "LocalSubsystem::recursiveOperation. Cannot open directory:" << sSourceDirName << "opendir error:" << errno;
		m_fInfo.setFile(m_sAbsSrcDirName);
		m_status = ERROR_OCCURED;
		m_error = m_fInfo.exists() ? CANNOT_READ : DOES_NOT_EXIST;
		m_recursiveOperation = NO_COMMAND; // why?
		return false;
	}
	else
		::closedir(mainDirPtr); // in slotStartProcessDir() dir is opened again

	slotStartProcessDir();

	return true;
}

void LocalSubsystem::currentCommandFinished()
{
// 	qDebug() << "LocalSubsystem::currentCommandFinished. currentCommand:" << toString(m_currentCommand) << " error:" << toString(m_error) << " recursiveOperation:" << toString(m_recursiveOperation);
//	mOperationTimer.stop(); // asynchronously listing may need it (in this momemnt not used)
	bool bWasErrorBefore = (m_error != NO_ERROR);
	if (m_currentCommand == COPY || m_currentCommand == MOVE) {
		m_operationTimer.stop();
		if (! bWasErrorBefore || m_status == OPR_STOPPED || m_error == CANNOT_WRITE || m_error == CANNOT_REMOVE) { // CannotRemove - target file (if exists)
			getFileFinished();
			if (m_error != CANNOT_WRITE && m_error != CANNOT_REMOVE && ! m_bSkipCurrentFile && ! m_bNoneOverwriting) {
				putFileFinished();
				if (m_recursiveOperation != m_currentCommand /*|| m_recursiveOperation == NO_COMMAND*/) { // if equal then we are inside recursively copying/moving
					if (! m_bSkipCurrentFile && ! m_bNoneOverwriting) {
						if (m_error == NO_ERROR) { // collect items to remove from list view
// 							m_processedFileInfo->setMark(false); // m_selectionMap in VfsFlatListModel isn't updated
							if (m_currentCommand == MOVE)
								m_postMovedFiles->append(m_processedFileInfo); // collect items to remove from list
							FileInfo *pFI = new FileInfo(m_processedFileInfo);
							if (m_currentCommand == COPY) { // for MOVE item has beed already collected
								//fi->setMark(false); // shouldn't do it here
								pFI->setLocation(pFI->filePath()); // necessary for find item to unmark
								pFI->setData(2, m_sCopyTargetPath, true); // workaround for setting path (in second panel we need to set correct path for new item)
								pFI->setData(2, m_fInfo.lastModified());
								pFI->setData(3, permissionsToString(permissions(m_processedFileInfo->absoluteFileName()), false));
								pFI->setData(4, m_fInfo.owner());
								pFI->setData(5, m_fInfo.group());
								if (! m_renameInCopying.isEmpty()) {
									QString sFileName = m_renameInCopying.right(m_renameInCopying.length() - m_renameInCopying.lastIndexOf(QDir::separator()) - 1);
									QString sPathName = m_renameInCopying.left(m_renameInCopying.length() - sFileName.length());
									if (pFI->filePath() == sPathName)
										pFI->setData(0, sFileName);
									//fi->fileName() FIXME despite earlier inovoked fi->setData(0, sFileName); the name has not been changed, check it
									m_renameInCopying = "";
								}
								m_processedFiles->clear(); // due to every single item will be deselected
								m_processedFiles->append(pFI); // it's doing in putFileFinished
								qDebug() << "LocalSubsystem::currentCommandFinished. Added item to the m_processedFiles list. FileName:" << pFI->fileName() << "location:" << pFI->location()  << "list_size" << m_processedFiles->size();
							} // m_currentCommand == COPY
						} // (m_error == NO_ERROR
					}
				}
			}
		}
		if (m_recursiveOperation == NO_COMMAND && m_error == NO_ERROR) {
			m_opQueue.pop_front();
			m_operationFinished = (m_opQueue.size() == 0); // need to set now, because jobFinished is called after back from SubsystemManager::slotSubsystemStatusChanged
			emit signalStatusChanged((m_currentCommand == COPY ? Vfs::COPIED : Vfs::MOVED), m_sRequestingPanel); // insert every copied file/dir in the list view
		}
	}
	else
	if (bWasErrorBefore && (m_currentCommand == REMOVE || m_currentCommand == SET_ATTRIBUTES_CMD || m_currentCommand == FIND)) {
		if ((m_bShowMsgCannotDelete        && m_currentCommand == REMOVE) ||
			(m_bShowMsgCannotSetAttributes && m_currentCommand == SET_ATTRIBUTES_CMD) ||
			(m_bShowMsgCannotCheckFile     && m_currentCommand == FIND)) {
			int result = showMsgCannotDo(m_processedFileInfo);
			if (result == MessageBox::Again) {
				m_error = NO_ERROR;
				m_sAbsSrcFileName = QFile::decodeName(::get_current_dir_name()); // restore current dir
				slotDirProcessingItem(); //QTimer::singleShot(0, this, &LocalSubsystem::slotDirProcessingItem);
				return;
			}
			else if (result == MessageBox::Break) {
				return;
			}
		}
		else m_error = SKIP_FILE;
	}

	if (! m_bSkipCurrentFile && ! m_bNoneOverwriting) {
		if ((m_error == NO_ERROR && ! bWasErrorBefore) || (m_error == CANNOT_SET_PERM || m_error == CANNOT_SET_MA_TIME || m_error == CANNOT_SET_OWNER_GROUP)) {
			if (m_currentCommand == WEIGH) {
				m_nTotalFiles++; // counter for total progress bar
				SubsystemCommand nextCmd = m_opQueue.size() > 1 ? m_opQueue.at(1).command : NO_COMMAND;
				if (nextCmd == WEIGH || nextCmd == REMOVE || nextCmd == SET_ATTRIBUTES_CMD || nextCmd == COPY || nextCmd == MOVE) { // weigh top dir/file
					bool isDir;
					if (m_recursiveOperation == WEIGH)
						isDir = S_ISDIR(m_statBuffer.st_mode);
					else
						isDir = (m_processedFileInfo->isDir() && !m_processedFileInfo->isSymLink());
					emit signalCounterProgress((isDir ? ++m_nDirCounter : ++m_nFileCounter), (isDir ? DIR_COUNTER : FILE_COUNTER), true); // update total value
				}
				else
					emit signalCounterProgress(++m_nFileCounter, FILE_COUNTER, true); // update total value
			}
			else
			if (m_currentCommand == m_currentOperation) {
				if (m_error == NO_ERROR) {
					if ((!(m_recursiveOperation == REMOVE && m_currentCommand == REMOVE) && m_currentOperation != SET_ATTRIBUTES_CMD) || m_currentOperation == REMOVE)
						m_nOpFileCounter++;
					if (m_currentOperation == SET_ATTRIBUTES_CMD && m_bAttribChgSuccess)
						m_nOpFileCounter++;
					if (m_nTotalFiles) {
						emit signalTotalProgress((100 * m_nOpFileCounter) / m_nTotalFiles, m_totalWeight);
						emit signalCounterProgress(m_OpTotalWeight, WEIGH_COUNTER);  // here is updated weight of all files
					}
				}
			}
			// update local counter
			if (m_currentCommand == REMOVE || m_currentCommand == SET_ATTRIBUTES_CMD) {
				if (m_recursiveOperation == NO_COMMAND) {
					if (m_processedFileInfo->isDir() && ! m_processedFileInfo->isSymLink()) { // top dir FileInfo
						if (m_currentOperation != SET_ATTRIBUTES_CMD)
							emit signalCounterProgress(++m_nDirCounter, DIR_COUNTER);
						else
						if (m_currentOperation == SET_ATTRIBUTES_CMD && m_bAttribChgSuccess)
							emit signalCounterProgress(++m_nDirCounter, DIR_COUNTER);
					}
					else {// non recursive file operation
						if (m_error == NO_ERROR)
							emit signalCounterProgress(++m_nFileCounter, FILE_COUNTER);
					}
				}
				else // always file (dir is counting in fun: slotStartProcessDir, slotDirProcessingItem)
					emit signalCounterProgress(++m_nFileCounter, FILE_COUNTER);
			}
		}
	}

	if (m_recursiveOperation == NO_COMMAND) {
		if (m_currentOperation == CREATE_LINK) {
			QFileInfo fi(m_sTargetFileName);
			if (fi.exists()) {
				QString sPerm = permissionsAsString(m_sTargetFileName);
				QString sMime = MimeType::getFileMime(fi.absoluteFilePath(), fi.isDir(), fi.isSymLink());
				FileInfo *pFI = new FileInfo(fi.fileName(), fi.path(), m_nSourceFileSize, fi.isDir(), fi.isSymLink(), fi.lastModified(), sPerm, fi.owner(), fi.group(), sMime, m_sSourceFileName);
				m_processedFiles->clear();
				m_processedFiles->append(pFI);
				emit signalStatusChanged(m_status, m_sRequestingPanel); // insert every created link in the list view
			}
		}
		if (m_opQueue.isEmpty() && m_currentCommand != COPY && m_currentCommand != MOVE)
			return;
		else {
			if (m_currentCommand != COPY && m_currentCommand != MOVE) {
				if (m_currentCommand == CREATE_LINK && m_error != NO_ERROR) {
					int result = showMsgCannotDo(m_processedFileInfo);
					if (result != MessageBox::Again)
						emit signalStatusChanged(m_status, m_sRequestingPanel); // for update list view and status bar in current tab
					if (result == -1 || result == MessageBox::SkipAll || result == MessageBox::Skip) { // to be able continue an operation in silent mode
						emit signalStatusChanged(m_status, m_sRequestingPanel); // for update list view and status bar in current tab
						m_status = CREATING_LINK;
						m_error = NO_ERROR;
					}
					m_processedFiles->clear();
					if (result == MessageBox::Again) {
						m_status = CREATING_LINK;
						m_error = NO_ERROR;
						slotStartNextJob(); //QTimer::singleShot(0, this, &LocalSubsystem::slotStartNextJob);
						return;
					}
					else if (result == MessageBox::Break) {
						return;
					}
				}
				m_opQueue.pop_front(); // drop an action
			}

			if (! m_opQueue.isEmpty()) {
				if ((m_currentCommand == MOVE || m_currentCommand == WEIGH) && (m_error != NO_ERROR || m_bSkipCurrentFile || m_bNoneOverwriting)) {
					if (m_currentCommand == MOVE) {
						int result = showMsgCannotDo(m_processedFileInfo);
						if (result != MessageBox::Again)
							emit signalStatusChanged(m_status, m_sRequestingPanel); // for update list view and status bar in current tab
						if (result == -1 || result == MessageBox::SkipAll || result == MessageBox::Skip) { // to be able continue an operation in silent mode
							emit signalStatusChanged(m_status, m_sRequestingPanel); // for update list view and status bar in current tab
							m_status = MOVING;
							m_error = NO_ERROR;
						}
						m_processedFiles->clear();
						if (result == MessageBox::Again) {
							m_status = MOVING;
							m_error = NO_ERROR;
							slotStartNextJob(); //QTimer::singleShot(0, this, &LocalSubsystem::slotStartNextJob);
							return;
						}
						else if (result == MessageBox::Break) {
							return;
						}
					}
					m_opQueue.pop_front(); // drop 'Remove' action or failed Move (removing failed)
				}
			}
		}
		if (! m_bSkipCurrentFile) {
			if (m_currentCommand == REMOVE || m_currentCommand == SET_ATTRIBUTES_CMD) {
				if (! m_opQueue.isEmpty()) {
					if (m_opQueue.first().command != CREATE_LINK)
						if (bWasErrorBefore)
							emit signalStatusChanged((m_status = ERROR_OCCURED), m_sRequestingPanel);
				}
				else {
					if (bWasErrorBefore)
						emit signalStatusChanged((m_status = ERROR_OCCURED), m_sRequestingPanel);
				}
			}
			else
			if (m_currentCommand != WEIGH) { // and different than REMOVE and SET_ATTRIBUTES_CMD
				if (m_currentCommand == COPY || m_currentCommand == MOVE)
					if (m_error == NO_ERROR)
						m_sProcessedFileName = m_sTargetFileName; // need to correct inserting on a list and to show error info
				if (m_error != NO_ERROR && m_status != OPR_STOPPED) {
					if (/*m_currentCommand == COPY ||*/ m_currentCommand == CREATE_LINK) {
						//m_processedFileInfo->setMark(false); // ugly unmarking that doesn't update list of selected in VfsFlatListModel
						m_postMovedFiles->clear();
						m_postMovedFiles->append(processedFileInfo()); // for showing properly error message
					}
					if (m_currentCommand != MOVE && m_currentCommand != CREATE_LINK) {
						if (m_currentCommand == COPY)
							m_opQueue.pop_front(); // drop failed action
						m_operationFinished = (m_opQueue.size() == 0);
						emit signalStatusChanged((m_status = ERROR_OCCURED), m_sRequestingPanel);
					}
				}
				if ((m_currentCommand == COPY || m_currentCommand == MOVE) && m_error != NO_ERROR) {
					if (m_operationFinished) // most likely m_opQueue.count() == 0
						m_error = NO_ERROR; // to be able to quit with success
					if (m_opQueue.count() > 0) {  // to be able continue an operation there is need:
						m_status = (m_currentCommand == COPY) ? COPYING : MOVING;
						m_error = NO_ERROR;
					}
				}
			} // m_currentCommand != WEIGH
		} // ! m_bSkipCurrentFile
	}
	else {
		m_sAbsSrcFileName = QFile::decodeName(::get_current_dir_name()); // restore current dir
		if (m_error != NO_ERROR && m_currentCommand != WEIGH && m_currentCommand != FIND) {
			if (! m_sAbsTargetFileName.isEmpty())
				m_sProcessedFileName = m_sAbsTargetFileName;
		}
		else
		if (m_error != NO_ERROR && m_currentCommand == WEIGH) {
			if (m_error != CANNOT_READ) // for CannotRead is setting
				m_sProcessedFileName = m_sAbsSrcFileName;
			emit signalStatusChanged((m_status = ERROR_OCCURED), m_sRequestingPanel); // shows error announcement
		}
		if (m_currentCommand == COPY || m_currentCommand == MOVE)
			m_sAbsTargetFileName = m_sAbsTargetDirName; // prepares for updating it with next file name in slotDirProcessingItem()
	}

	if (m_bBreakOperation || m_opQueue.isEmpty() || (m_currentCommand == CD && bWasErrorBefore) || m_status == OPR_STOPPED)
		jobFinished();
	else
	if (m_recursiveOperation != NO_COMMAND) {
		if (m_error == NO_ERROR || m_error == SKIP_FILE) {
			if (m_error == SKIP_FILE) {
				m_error = NO_ERROR;
				if (m_status != ERROR_OCCURED)
					slotDirProcessing(); //QTimer::singleShot(0, this, &LocalSubsystem::slotDirProcessing);
				else { // probably before was error in removing operation, therefore m_error has been overwrote to NO_ERROR from SKIP_FILE (which user can choose)
					m_opQueue.pop_front();
					slotStartNextJob(); //QTimer::singleShot(0, this, &LocalSubsystem::slotStartNextJob);
				}
			}
			else { // m_error == NO_ERROR
				if (m_error == NO_ERROR && m_status != ERROR_OCCURED && m_currentCommand == m_recursiveOperation)
					slotDirProcessing(); //QTimer::singleShot(0, this, &LocalSubsystem::slotDirProcessing);
				else // probably before was error in removing operation, therefore m_error has been overwrote to NO_ERROR from SKIP_FILE (which user can choose)
					slotStartNextJob(); //QTimer::singleShot(0, this, &LocalSubsystem::slotStartNextJob);
			}
		}
	}
	else
		slotStartNextJob(); //QTimer::singleShot(0, this, &LocalSubsystem::slotStartNextJob);
}

void LocalSubsystem::jobFinished()
{
	qDebug() << "LocalSubsystem::jobFinished. currentCommand" << Vfs::toString(m_currentCommand) << "recursiveOperation" << Vfs::toString(m_recursiveOperation) << " Start cleaning";
	if (m_bBreakOperation) {
		m_status = OPR_STOPPED;
		m_error  = NO_ERROR;
		m_currentCommand = m_recursiveOperation;
	}
	m_operationFinished = true;
	m_operationTimer.stop();
	m_opQueue.clear(); // if an error ocurred then list is not empty

	m_stackForCurrLocInDir.clear();
	m_stackForFirstInodeOfDir.clear();
	while (! m_stackOfDIRptrs.isEmpty())
		::closedir(m_stackOfDIRptrs.pop());
	m_stackOfDirInode.clear();
	m_stackForThrowedOffInode.clear();

	bool bNoError    = (m_error == NO_ERROR);
	bool bDoesntStop = (m_status != OPR_STOPPED);
	SubsystemCommand cOp = m_currentOperation;

	if (cOp == COPY || cOp == MOVE) {
		if (m_pFileBuffer) {
			delete m_pFileBuffer;  m_pFileBuffer = nullptr;
		}
		if (m_pNewAttributes != nullptr) {
			delete m_pNewAttributes; m_pNewAttributes = nullptr;
		}
		m_sProcessedFileName = m_sTargetFileName; // need to correct inserting on the list
		if (bNoError && bDoesntStop)
			m_status = (cOp == COPY) ? COPIED : MOVED;
	}
	else
	if (cOp == FIND) {
		m_processedFiles->clear();
		m_status = FOUND;
	}
	else
	if (cOp == WEIGH) {
		m_sProcessedFileName = m_sAbsSrcFileName;
		if (m_status != WEIGHED) {
			if (bNoError && bDoesntStop)
				m_status = WEIGHED;
			if (m_error == SKIP_FILE) {
				m_status = WEIGHED;  m_error = NO_ERROR;
			}
			emit signalStatusChanged(m_status, m_sRequestingPanel);
		}
	}
	else
	if (cOp == REMOVE) {
		if (bNoError && bDoesntStop)
			m_status = REMOVED;
	}
	else
	if (cOp == SET_ATTRIBUTES_CMD) {
		if (m_pNewAttributes != nullptr) {
			delete m_pNewAttributes; m_pNewAttributes = nullptr;
		}
		if (bNoError && bDoesntStop)
			m_status = SET_ATTRIBUTES;
	}

	if (cOp == RENAME || cOp == SET_ATTRIBUTES_CMD ||
		cOp == CREATE_DIR  || cOp == CREATE_FILE ||
		cOp == COPY || cOp == MOVE) {
		emit signalTimerStartStop(false);
		if (cOp != COPY && cOp != MOVE)
			m_lsw->setLock(false);
	}

	if (/*cOp != WEIGH &&*/ m_recursiveOperation != NO_COMMAND
		|| m_currentOperation == REMOVE
		|| m_currentOperation == SET_ATTRIBUTES_CMD) // for update selection
		emit signalStatusChanged(m_status, m_sRequestingPanel); // useful for recursive operations excluding WEIGHING

	if (m_nSkippedFiles > 0) {
		qDebug() << "LocalSubsystem::jobFinished. SkippedFiles:" << m_nSkippedFiles;
		if (m_currentOperation == REMOVE) {
			m_error = CANNOT_REMOVE;
			emit signalStatusChanged(ERROR_OCCURED, m_sRequestingPanel);
		}
	}

	m_processedFiles->clear();
	m_postMovedFiles->clear();

	m_status = UNKNOWN_STAT;
	m_error  = NO_ERROR;
	m_recursiveOperation = NO_COMMAND;
	m_currentOperation = m_currentCommand = NO_COMMAND;
	m_bSkipCurrentFile = false;
	m_bNoneOverwriting = false;
	m_bInitOperation   = true;
	m_nTotalFiles = 0;
	clearFilesCounter();
	emit signalOperationFinished(); // only updates button label on progress dialog to 'Done'
}

void LocalSubsystem::initRemoving()
{
	if (m_status == WEIGHED) {
		qDebug() << "LocalSubsystem::initRemoving(). WEIGHED - totalWeigh:" << m_totalWeight << FileInfo::bytesRound(m_totalWeight) << "files:" << m_nFileCounter << "dirs:" << m_nDirCounter << "all items:" << m_nTotalFiles;
	}
	m_status = REMOVING;
	m_error  = NO_ERROR;
	m_recursiveOperation = NO_COMMAND;
	m_bBreakOperation = false;
	clearStacks();
	if (m_bInitOperation) {
		m_bRecursivelyRemoveAllDirs = m_bForceRemoveAll;
		m_bNoneRecursivelyRemoveDir = false;
		m_processedFiles->clear();
		clearFilesCounter();
		m_bInitOperation = false;
	}
	emit signalStatusChanged(m_status, m_sRequestingPanel); // update status bar
}

void LocalSubsystem::initSettingAttributes()
{
	m_status = SETTING_ATTRIBUTES;
	m_error  = NO_ERROR;
	m_recursiveOperation = NO_COMMAND;
	m_bBreakOperation = false;
	clearStacks();
	if (m_bInitOperation) {
		m_processedFiles->clear();
		clearFilesCounter();
		m_bInitOperation = false;
	}
	emit signalStatusChanged(m_status, m_sRequestingPanel); // update status bar
}

void LocalSubsystem::initSearchingFiles()
{
	m_status = FINDING;
	m_error  = NO_ERROR;
	m_recursiveOperation = NO_COMMAND;
	m_bBreakOperation = false;
	clearStacks();
	if (m_bInitOperation) {
		m_processedFiles->clear();
		clearFilesCounter();
		m_nMatchedFiles = 0;
		m_sSearchingResult = "";
		m_bInitOperation = false;
	}
	emit signalStatusChanged(m_status, m_sRequestingPanel); // update status bar
}

bool LocalSubsystem::initCopiedFiles( long long fileSize )
{
	if (m_sSourceFileName == m_sTargetFileName) {
		m_error = CANNOT_COPY_TO_THESAME;
		m_sProcessedFileName = m_sSourceFileName;
		emit signalStatusChanged(Vfs::ERROR_OCCURED, m_sRequestingPanel); // update status bar
		m_error = NO_ERROR; // to be able continue an operation
		m_opQueue.pop_front();
		QTimer::singleShot(0, this, &LocalSubsystem::slotStartNextJob);
		return true;
	}

	if (m_recursiveOperation == NO_COMMAND)
		emit signalNameOfProcessedFile(m_sSourceFileName, m_sTargetFileName);

	m_FI.setFile(m_sSourceFileName);
	if (m_currentCommand != CREATE_LINK) {
		if (! m_FI.exists()) // if (isError(m_sSourceFileName, VFS::Exists))
			return false;
		else
			m_error = NO_ERROR;
	}

	bool stat = true;
	if (m_FI.isFile() || m_FI.isSymLink() || m_currentCommand == CREATE_LINK) {
		if ((stat=initGetFile(fileSize)))
			if ((stat=initPutFile())) {
				//slotFileProcessing();
				if (m_currentCommand == COPY || m_currentCommand == MOVE)
					copyingFile();
			}
	}
	else
		stat = recursiveOperation(m_currentCommand, m_sSourceFileName, m_sTargetFileName);

	return stat;
}

void LocalSubsystem::initCopying()
{
	if (m_currentOperation == MOVE)
		m_status = MOVING;
	else
		m_status = (m_currentOperation == CREATE_LINK) ? CREATING_LINK : COPYING;

	m_error = NO_ERROR;
	m_recursiveOperation = NO_COMMAND;
	m_bBreakOperation = false;
	m_bSkipCurrentFile = false;
	m_bNoneOverwriting = false;
	m_eUpdatingFilesWith = NONEupd;
	clearStacks();
	if (m_bInitOperation) {
		emit signalTotalProgress(0, m_totalWeight);
		clearFilesCounter();
		m_processedFiles->clear(); // due to copying made before could add items
		m_bInitOperation = false;
	}
	emit signalStatusChanged(m_status, m_sRequestingPanel); // update status bar
	m_OpTotalWeight = 0;

	if (m_currentOperation == CREATE_LINK)
		return;

	if (m_pFileBuffer != nullptr)
		delete m_pFileBuffer;
	m_pFileBuffer = new char[ m_nFileBufferSize ];
}

bool LocalSubsystem::initGetFile( long long fileSize )
{
	if (m_currentOperation == CREATE_LINK) {
		m_nSourceFileSize = m_sSourceFileName.length();
		return true;
	}
	bool isSymLink = m_FI.isSymLink();
	if (! m_FI.isReadable() && ! isSymLink) {
		m_error = CANNOT_READ;
		m_status = ERROR_OCCURED;
		return false;
	}
	m_error = NO_ERROR;

	if (fileSize < 0) {
		if (! isSymLink) {
			m_nSourceFileSize = QFile(m_sSourceFileName).size(); }
		else
			m_nSourceFileSize = m_FI.symLinkTarget().length();
	}
	else
		m_nSourceFileSize = fileSize;

	if (! isSymLink) {
		m_fileIn.setFileName(m_sSourceFileName);
		if (! m_fileIn.open(QIODevice::ReadOnly)) {
			m_sProcessedFileName = m_sSourceFileName;
			m_status = ERROR_OCCURED;
			m_error  = CANNOT_READ;
		}
	}

	return (m_error == NO_ERROR);
}

bool LocalSubsystem::initPutFile()
{
	// --- checking amount of free bytes in target directory
	m_fInfo.setFile(m_sTargetFileName);
	long long freeBytes, totalBytes;
	Vfs::getStatFS(m_fInfo.absolutePath(), freeBytes, totalBytes);
	if (freeBytes < m_nSourceFileSize) { // m_nSourceFileSize is init.in 'initGetFile()'
		m_sProcessedFileName = m_fInfo.absolutePath();
		m_status = ERROR_OCCURED;
		m_error  = NO_FREE_SPACE;
		return false;
	}
	m_nWritedBytes = 0;
	// --- checking whether overwrite a file is possible
	enum FileOverwriteBtns { Yes=1, No, DiffSize, Rename, All, Update, None, Cancel }; // the same like in OverwriteFileDialog class
	int  result = -1;
	bool bSourceIsSymLink = m_FI.isSymLink();
	bool bTargetIsSymLink = m_fInfo.isSymLink();
	bool bDontCreateTargetFile = false, bRenameTargetFile = false;
	FileInfo targetFileInfo(m_fInfo.fileName(), m_fInfo.path(), (bTargetIsSymLink ? m_fInfo.symLinkTarget().length() : m_fInfo.size()), m_fInfo.isDir(), bTargetIsSymLink, m_fInfo.lastModified(), "", "", "", "");
	FileInfo sourceFileInfo(m_FI.fileName(), m_FI.path(), (bSourceIsSymLink ? m_FI.symLinkTarget().length() : m_FI.size()), m_FI.isDir(), bSourceIsSymLink, m_FI.lastModified(), "", "", "", "");
	if (! m_bAlwaysOverwrite) {
		if (m_eUpdatingFilesWith == NONEupd) {
 			//if (! isError(m_sTargetFileName, VFS::Exists)) {
			if (m_fInfo.exists()) {
				if (! m_bNoneOverwriting) { // shows query's dialog
					emit signalDataTransferProgress(0, m_nSourceFileSize, 0, true);
					emit signalShowFileOverwriteDlg(sourceFileInfo, targetFileInfo, result); // returns reference to targetFileInfo and reference to result
					m_sTargetFileName = targetFileInfo.absoluteFileName(); // because name may be changed and then targetFileInfo will be updated
					if (result == 0) // dialog has been closed
						result = Cancel;
				}
				else { // none overwriting
					if (m_error == ALREADY_EXISTS)
						bDontCreateTargetFile = true;
				}
			}
		}
		m_error = NO_ERROR;
		if (result != -1 && ! bRenameTargetFile) {
			if (result == No)
				bDontCreateTargetFile = true;
			else
			if (result == DiffSize)
				m_eUpdatingFilesWith = SIZEupd;
			else
			if (result == Rename)
				bRenameTargetFile = true;
			else
			if (result == All)
				m_bAlwaysOverwrite = true;
			else
			if (result == Update)
				m_eUpdatingFilesWith = DATEupd;
			else
			if (result == None) {
				m_bNoneOverwriting = true; // disable to show OverwritingDialog
				//return false; // need to disable 'slotFileProcessing()'
			}
			else
			if (result == Cancel) {
				m_opQueue.clear();
				m_status = OPR_STOPPED;
				m_error  = NO_ERROR;
				jobFinished();
				return false;
			}
		} // result != -1
	} // ! m_bAlwaysOverwrite

	if (m_eUpdatingFilesWith == DATEupd) {
		QDateTime UTCTime(QDate(1970,1,1), QTime(0,0));
		uint sourceFileTime = UTCTime.secsTo(QFileInfo(m_sSourceFileName).lastModified());
		uint targetFileTime = UTCTime.secsTo(m_fInfo.lastModified());
		bDontCreateTargetFile = (sourceFileTime <= targetFileTime);
	}
	if (m_eUpdatingFilesWith == SIZEupd)
		bDontCreateTargetFile = (m_nSourceFileSize == m_fInfo.size()); //  QFileInfo(m_sTargetFileName).size()

	if ((m_eUpdatingFilesWith != NONEupd && bDontCreateTargetFile) || m_bNoneOverwriting) {
		m_error = NO_ERROR;
		m_sProcessedFileName = m_sSourceFileName;
		if (m_currentCommand == MOVE) {
			removeFile(m_sSourceFileName, true);
			emit signalCounterProgress(++m_nFileCounter, FILE_COUNTER); // update local value, due to putFileFinished will be not called
			if (m_nTotalFiles) {
				m_nOpFileCounter++;
				emit signalTotalProgress((100 * (m_nOpFileCounter)) / m_nTotalFiles, m_totalWeight);
				m_OpTotalWeight += m_FI.size();
				emit signalCounterProgress(m_OpTotalWeight, WEIGH_COUNTER);  // here is updated weight of all files
			}
		}
		if (m_bNoneOverwriting /*|| m_error != NO_ERROR*/)
			return false; // need to disable 'slotFileProcessing()'
	}

	if (bDontCreateTargetFile) {
		m_bSkipCurrentFile = true;
		if (m_recursiveOperation != NO_COMMAND || m_currentCommand != NO_COMMAND) { // bo w slocie slotDirProcessing nie jest wyw. ponizsza fun. (jest specjalnie omijana)
			currentCommandFinished();
			m_bSkipCurrentFile = false;
		}
		else { // need to remove current file from the SelectedItemsList
// 			m_status = OPR_STOPPED;
			m_error  = NO_ERROR;
// 			emit signalStatusChanged((m_status = Vfs::ERROR_OCCURED), m_sRequestingPanel); // commented due to overwriting m_error
// 			emit commandFinished(m_currentCommand, false);
		}
		return false; // need to disable processing current file
	}

	if ((! bRenameTargetFile && result != -1) || m_bAlwaysOverwrite || ! bDontCreateTargetFile) { // overwriting - need to remove existing target file
		if (m_fInfo.exists()) { // remove target file before copy new
			if (m_fInfo.isFile() || bTargetIsSymLink) {
				removeFile(m_sTargetFileName);
//				emit signalCounterProgress(++m_nFileCounter, FILE_COUNTER); // update local value (commented due to fix case: Przenoszenie katalogow:3.b. m_nFileCounter is inc.in putFileFinished)
				if (m_error != NO_ERROR) {
					if (m_recursiveOperation != NO_COMMAND || m_currentCommand != NO_COMMAND) // in slot slotDirProcessing below fun. is didn't call  (is intentionally skipped)
						currentCommandFinished();
					return false;
				}
			}
			else {
				if (m_fInfo.isDir()) {
					;//removeDir();
				}
			}
		}
	}

	if (bSourceIsSymLink || m_currentCommand == CREATE_LINK) {
		m_fInfo.setFile(m_sSourceFileName); // m_FI has input file
		initMakeLink();
	}
	else {
		if (bRenameTargetFile) {
			result = Rename;
			m_fInfo.setFile(targetFileInfo.absoluteFileName());
			m_sProcessedFileName = m_sTargetFileName;
			while (m_fInfo.exists()) {
				m_error = ALREADY_EXISTS;
				emit signalStatusChanged((m_status=Vfs::ERROR_OCCURED), m_sRequestingPanel);
// 				emit commandFinished(m_currentCommand, true); // shows error announcement
				emit signalShowFileOverwriteDlg(sourceFileInfo, targetFileInfo, result); // returns reference to targetFileInfo and reference to result
				if (result == No) { // user replied "Cancel" in entering new name dialog
					bDontCreateTargetFile = true;
					break;
				}
				m_sTargetFileName = targetFileInfo.absoluteFileName(); // because name may be changed and then targetFileInfo will be updated
				m_fInfo.setFile(m_sTargetFileName);
			}
			emit signalNameOfProcessedFile(m_sSourceFileName, m_sTargetFileName);
			m_error = NO_ERROR;
		}
		if (bDontCreateTargetFile) {
			m_bSkipCurrentFile = true;
			return false;
		}
		// --- make a new target file
		m_fileOut.setFileName(m_sTargetFileName);
		if (! m_fileOut.open(QIODevice::WriteOnly | QIODevice::Append)) {
			m_sProcessedFileName = m_fInfo.absolutePath();
			m_status = ERROR_OCCURED;
			m_error  = CANNOT_WRITE;
			return false;
		}
		if (m_nSourceFileSize == 0) { // empty file already has been created (by 'open()')
			emit signalDataTransferProgress(0, 0, 1, true);
			m_fileOut.close();
		}
		if (bRenameTargetFile) { // need to insert item before it has been copying
			// ERROR, cmd.Put inserts, but in a current panel !
			m_renameInCopying = targetFileInfo.absoluteFileName(); // set new name for properly inserting item on the list
		}
	}

	return (m_error == NO_ERROR);
}

void LocalSubsystem::getFileFinished()
{
	if (m_status == OPR_STOPPED) {
		bool chk = false;
		QString sInfo = FileInfo::bytesRound(m_nWritedBytes) + " "+ tr("bytes have been copied.");
		int nResult = MessageBox::yesNo(m_pParent, tr("Stopped copying"), m_sTargetFileName,
										sInfo + "\n\n\t"+tr("Remove this file")+" ?",
										chk,
										MessageBox::Yes // default focused button
									);
/*		int result = MessageBox::yesNo(m_pParent,
		 tr("Breaked copying")+" - QtCommander",
		 m_sTargetFileName+"\n\n\t"+formatNumber(m_nWritedBytes,BKBMBGBformat)+" "+
		 tr("bytes have been copied.")+"\n\n\t"+tr("Remove this file")+" ?",
		 MessageBox::Yes
		);*/
		if (nResult == MessageBox::Yes)
			removeFile(m_sTargetFileName); // not asynchronously remove
	}
	m_fileIn.close();
}

void LocalSubsystem::putFileFinished()
{
	if (m_bSkipCurrentFile)
		return;
	if (! m_fInfo.isSymLink())
		m_fileOut.close();

	if (m_error == NO_ERROR) {
		if (! m_fInfo.isSymLink()) {
			if (m_bSaveAttributes) {
				::lstat64(QFile::encodeName(m_sSourceFileName), &m_statBuffer);
				// if effective user ID equals the user ID of the file then setTime and setPerm successful
				setFileTime(m_sTargetFileName, m_statBuffer.st_atime, m_statBuffer.st_mtime);
				setFilePermissions(m_sTargetFileName, Vfs::permissions(m_sSourceFileName));
			}
			if (m_currentCommand == MOVE) {
				// One need to collect items, because in removeFile if an error appeared then signal is sent to SubsystemManager::slotStatusChanged,
				// where item (copied file) will be inserted on target list view.
				m_postMovedFiles->append(m_processedFileInfo); // collect items to remove from list or showing remove error
				FileInfo *pFI = new FileInfo(m_processedFileInfo);
				pFI->setLocation(m_fInfo.path()); // necessary for find item to unmark
				pFI->setData(2, m_sCopyTargetPath, true); // workaround for setting path (in second panel we need to set correct path for new item)
				pFI->setData(2, m_fInfo.lastModified());
				pFI->setData(3, permissionsToString(permissions(m_processedFileInfo->absoluteFileName()), false));
				pFI->setData(4, m_fInfo.owner());
				pFI->setData(5, m_fInfo.group());
				if (! m_renameInCopying.isEmpty()) {
					QString sFileName = m_renameInCopying.right(m_renameInCopying.length() - m_renameInCopying.lastIndexOf(QDir::separator()) - 1);
					QString sPathName = m_renameInCopying.left(m_renameInCopying.length() - sFileName.length());
					if (pFI->filePath() == sPathName)
						pFI->setData(0, sFileName);
					//pFI->fileName() FIXME despite earlier inovoked pFI->setData(0, sFileName); the name has not been changed, check it
					m_renameInCopying = "";
				}
				m_processedFiles->append(pFI);
				qDebug() << "LocalSubsystem::putFileFinished. Added item to the processedFiles list. FileName:" << pFI->fileName() << " location:" << pFI->location()  << " list_size" << m_processedFiles->size();
				if (m_fInfo.isDir()) {
					removeDir(m_sSourceFileName);
					return;
				}
				else { // file
					removeFile(m_sSourceFileName);
					if (m_error != NO_ERROR) {
						if (m_recursiveOperation != NO_COMMAND) // in slot slotDirProcessing below fun.is didn't call (is intentionally skipped)
							currentCommandFinished();
						return;
					}
				}
			} // m_currentCommand == MOVE
		} // ! m_fInfo.isSymLink()
// 		else {
// 			makeNextLink(m_sSourceFileName, m_sTargetFileName);
// 		}
		emit signalCounterProgress(++m_nFileCounter, FILE_COUNTER, false);
	}
}

bool LocalSubsystem::initMakeLink()
{
	if (m_fInfo.isSymLink())
		m_sSourceFileName = m_fInfo.symLinkTarget();

	int nSrcFileNameLen = m_sSourceFileName.length();

	emit signalDataTransferProgress(nSrcFileNameLen, nSrcFileNameLen, nSrcFileNameLen, true);
	emit signalDataTransferProgress(nSrcFileNameLen, nSrcFileNameLen, nSrcFileNameLen, false);

	bool bLinkCreated = makeNextLink(m_sSourceFileName, m_sTargetFileName);
	if (bLinkCreated) {
		m_OpTotalWeight += nSrcFileNameLen;
		if (m_recursiveOperation == NO_COMMAND)
			m_status = CREATED_LINK;
	}

	if (m_currentCommand == CREATE_LINK && m_opQueue.size() > 0 && m_currentOperation != m_currentCommand)
		m_opQueue.pop_front();

	return bLinkCreated;
}

void LocalSubsystem::copyingFile()
{
	if (m_status == OPR_STOPPED || m_nSourceFileSize == 0 || m_fInfo.isSymLink()) {
		if (m_nSourceFileSize == 0)
			emit signalDataTransferProgress(0, 0, 1, false);
		currentCommandFinished();
		return;
	}
	int realReadBytes = m_fileIn.read(m_pFileBuffer, m_nFileBufferSize);

	if (realReadBytes < 0) { // an error occured
		m_sProcessedFileName = m_sSourceFileName;
		m_error = READING_ERROR;
	}
	else {
		uint realWriteBytes = m_fileOut.write(m_pFileBuffer, realReadBytes);
		m_nWritedBytes  += realWriteBytes;
		m_OpTotalWeight += realWriteBytes;
		emit signalDataTransferProgress(m_nWritedBytes, m_nSourceFileSize, realWriteBytes, false);
	}

	if (realReadBytes < m_nFileBufferSize)
		currentCommandFinished();
	else
		m_operationTimer.start(0);
}

bool LocalSubsystem::makeNextLink( const QString &sSymLinkTarget, const QString &sNewSymLink, bool checkErrors )
{
	m_fInfo2.setFile(sNewSymLink);
	if (m_currentOperation == CREATE_LINK)
		m_sProcessedFileName = sNewSymLink;

	if (checkErrors) {
		m_fInfo.setFile(sSymLinkTarget);
		if (! m_fInfo.isReadable()) // if (isError(sourceFileName, VFS::Readable))
			return false;
		m_fInfo.setFile(m_fInfo2.absolutePath());
		if (m_fInfo.isWritable()) // if (isError(FileInfoExt::filePath(targetFileName), VFS::Writable))
			return false;
	}

	if (m_bAlwaysOverwrite) { // overwrite the target file
		m_sProcessedFileName = sNewSymLink;
		removeFile(sNewSymLink);
		if (m_error != NO_ERROR)
			return false;
	}
	else {
		if (m_fInfo2.exists()) // if (! isError(targetFileName, VFS::Exists))
			return false;
		else
			m_error = NO_ERROR;
	}

	if (! m_bHardLink) {
		if (::symlink(QFile::encodeName(sSymLinkTarget), QFile::encodeName(sNewSymLink)) != 0) {
			qDebug() << "LocalSubsystem::makeNextLink. create symlink error:" << errno;
			m_status = ERROR_OCCURED;
			if (m_currentCommand == EDIT_LINK)
				m_error = CANNOT_EDIT_LINK;
			else {
				if (errno == EACCES) // Permission denied
					m_error = CANNOT_WRITE;
				else
					m_error = CANNOT_CREATE_LINK;
			}
			return false;
		}
	}
	else // hard link
	if (::link(QFile::encodeName(sSymLinkTarget), QFile::encodeName(sNewSymLink)) != 0) {
		qDebug() << "LocalSubsystem::makeNextLink. create hardlink error:" << errno;
		m_status = ERROR_OCCURED;
		if (errno == EACCES) // Permission denied
			m_error = CANNOT_WRITE;
		else
			m_error  = CANNOT_CREATE_LINK;
		return false;
	}

	return true;
}


bool LocalSubsystem::removeNextFile()
{
	qDebug() << "LocalSubsystem::removeNextFile. SourceFileName:" << m_sSourceFileName;
	m_fInfo.setFile(m_sSourceFileName);
	if (m_fInfo.isSymLink()) {
		emit signalNameOfProcessedFile(m_sSourceFileName);
		removeFile(m_sSourceFileName);
		if (m_error != NO_ERROR)
			return false;
	}
	else
	if (m_fInfo.exists()) { // QFileInfo treats broken link like not existing file (Qt4 bug?)
		emit signalNameOfProcessedFile(m_sSourceFileName);
		if (m_fInfo.isFile()) {
			removeFile(m_sSourceFileName);
			if (m_error != NO_ERROR)
				return false;
		}
		else {
			removeDir(m_sSourceFileName);
			return false;
		}
	}
	return true;
}

void LocalSubsystem::removeFile( const QString &sFileName, bool bSilentMode )
{
	m_bOkStat = (::lstat64(QFile::encodeName(sFileName), & m_statBuffer) == 0);
	m_error = QFile::remove(sFileName) ? NO_ERROR : CANNOT_REMOVE;
	m_recursiveOperation = NO_COMMAND;
	qDebug() << "LocalSubsystem::removeFile. FileName:" << sFileName << " error" << toString(m_error);
	if (m_error != NO_ERROR && ! bSilentMode) {
		m_sProcessedFileName = sFileName;
		m_status = ERROR_OCCURED;
	} else { // removed with no error
		// Case when m_bFollowByLink == true:  what if sFileName will be symlink to directory? then needs recursive weigh
		// Note, this is not safe setting and should not be used
		if (m_currentCommand == REMOVE && m_error == NO_ERROR && ! bSilentMode) {
			m_processedFiles->append(m_processedFileInfo);
			m_OpTotalWeight += m_statBuffer.st_size;
			emit signalCounterProgress(m_OpTotalWeight, WEIGH_COUNTER);
		}
	}

	if (m_error != NO_ERROR) {
		qDebug() << "LocalSubsystem::removeFile. Cannot remove file:" << sFileName << "silent mode:" << bSilentMode;
		if (! bSilentMode) {
			m_operationFinished = (m_processedFiles->size() == m_nSelectedFiles);
		}
	}
}

void LocalSubsystem::removeDir( const QString &sDirName )
{
	m_error = NO_ERROR;
	m_fInfo2.setFile(sDirName);
	m_fInfo.setFile(m_fInfo2.absolutePath());
	m_sProcessedFileName = sDirName;
	qDebug() << "LocalSubsystem::removeDir. DirName:" << sDirName;
	if (! m_fInfo.isWritable()) {
        if (m_nCurrentUserID != 0/*root*/ && m_fInfo.owner() != m_sCurrentUserName) {
			m_status = ERROR_OCCURED;
			m_error  = CANNOT_REMOVE;
		}
	}
	else
	if (isDirectoryEmpty(sDirName)) {
		if (m_dir.rmdir(sDirName)) {
			emit signalCounterProgress(++m_nDirCounterR, DIR_COUNTER);
			if (m_currentCommand != MOVE) {
				m_processedFiles->append(m_processedFileInfo);
				emit signalStatusChanged(Vfs::REMOVED, m_sRequestingPanel); // for update data model
			}
			currentCommandFinished();
		}
		else {
			m_status = ERROR_OCCURED;
			m_error  = CANNOT_REMOVE;
		}
		return;
	}
	else
		m_currentOperation = REMOVE;

	if (m_error != NO_ERROR) // an error occured
		currentCommandFinished();
	else
	if (m_currentOperation == REMOVE) {
		int result = -1;
		if (! m_bRecursivelyRemoveAllDirs && ! m_bNoneRecursivelyRemoveDir) {
			result = showMsgDirNotEmptyForRemoving(sDirName);
			m_bSkipCurrentFile = (result == MessageBox::No);
			if (result == MessageBox::All)
				m_bRecursivelyRemoveAllDirs = true;
			else
			if (result == MessageBox::None)
				m_bNoneRecursivelyRemoveDir = true;
			else
			if (result == MessageBox::Cancel || result == 0) {
				m_currentCommand = REMOVE;
				m_status = OPR_STOPPED;
				m_error  = NO_ERROR;
// 				m_processedFiles->clear();
				m_opQueue.clear();
				jobFinished();
				return;
			}
		}
		if (m_bSkipCurrentFile || m_bNoneRecursivelyRemoveDir) {
			m_error = NO_ERROR;
			m_opQueue.pop_front();
			QTimer::singleShot(0, this, &LocalSubsystem::slotStartNextJob);
			return;
		}
		if (! m_bNoneRecursivelyRemoveDir && result != MessageBox::No)
			recursiveOperation(REMOVE, sDirName);
		else
			currentCommandFinished();
	}
	else
	if (m_currentOperation == MOVE)
		recursiveOperation(REMOVE, sDirName);
}

int LocalSubsystem::showMsgDirNotEmptyForRemoving( const QString &sDirName )
{
	emit signalTimerStartStop(false);

	bool chk = false;
	QStringList slBtnNames = (QStringList() << tr("&Yes") << tr("&No") << tr("&All") << tr("N&one") << tr("&Cancel"));
	int nResult = MessageBox::common(m_pParent,
			tr("Removing directory") + " - QtCommander",
			sDirName, tr("Directory is not empty")+".\n"+tr("Delete it recursively?"), chk,
			slBtnNames, MessageBox::Yes, QMessageBox::Warning
	);

	if (nResult != MessageBox::Cancel) // continue operation
		emit signalTimerStartStop(true);

	return nResult;
}

int LocalSubsystem::showMsgCannotDo( FileInfo * )
{
	// -- skip error message
	if       (m_currentCommand == CREATE_LINK && ! m_bShowMsgCannotCreateLink)
		return -1;
	else if ((m_currentCommand == MOVE || m_currentCommand == REMOVE) && ! m_bShowMsgCannotDelete)
		return -1;
	else if  (m_currentCommand == SET_ATTRIBUTES_CMD && ! m_bShowMsgCannotSetAttributes)
		return -1;
	else if  (m_currentCommand == FIND && ! m_bShowMsgCannotCheckFile)
		return -1;
	else if  (m_currentCommand == WEIGH && ! m_bShowMsgCannotWeigh)
		return -1;

	m_bSkipCurrentFile = false;
	emit signalTimerStartStop(false);
	m_fInfo.setFile(m_sProcessedFileName);
	QString sFileTypeMsg = (m_fInfo.isDir()) ? tr("directory") : tr("file");
	if (m_fInfo.isSymLink())
		sFileTypeMsg = tr("symbolic link");
	QString sErrMsg;
	QString sAsk = tr("What would you like to do?");
	qDebug() << "LocalSubsystem::showMsgCannotDo." << toString(m_currentCommand) << "errno" << errno << "for:" << m_sProcessedFileName;
	if (errno == EACCES) // 13
		sErrMsg = tr("Permission denied.");
	else if (errno == ENOTEMPTY) // 39
		sErrMsg = tr("Directory not empty.");
	else if (errno == EPERM) // 1
		sErrMsg = tr("Operation not permitted.");

	QString sMsg1, sMsg2;
	if (m_currentCommand == REMOVE) {
		sMsg1 = tr("Removing error");
		sMsg2 = tr("Cannot remove");
	}
	else if (m_currentCommand == SET_ATTRIBUTES_CMD) {
			sMsg1 = tr("Setting attributes error");
		if      (m_error == CANNOT_SET_PERM)
			sMsg2 = tr("Cannot set permission of");
		else if (m_error == CANNOT_SET_MA_TIME)
			sMsg2 = tr("Cannot set last access time or last modification time");
		else if (m_error == CANNOT_SET_OWNER_GROUP)
			sMsg2 = tr("Cannot set owner and/or group");
	}
	else if (m_currentCommand == WEIGH) {
		sMsg1 = tr("Weighing error");
		if (m_error == CANNOT_OPEN)
			sMsg2 = tr("Cannot change path to above");
	}
	else if (m_currentCommand == FIND) {
		sMsg1 = tr("Searching error");
		if (m_error == CANNOT_STAT) { // errno = 2
			sMsg2 = tr("Cannot check")+" "+sFileTypeMsg+". "+tr("Probably incorrect encoding file name");
			sFileTypeMsg = "";
		}
		else if (m_error == CANNOT_OPEN) { // (errno == EAGAIN) { // 11
			sMsg2 = tr("Cannot change path to above");
			sFileTypeMsg = "";
		}
	}
	else if (m_currentCommand == MOVE) {
		sMsg1 = tr("Moving error");
		if (m_error == CANNOT_REMOVE) { // (errno == EACCES) { // 13
			sMsg2 = tr("Cannot remove")+" "+sFileTypeMsg;
			sFileTypeMsg = "";
		}
	}
	else if (m_currentCommand == CREATE_LINK) {
		sMsg1 = tr("Creating link error");
		if (m_error == CANNOT_WRITE || m_error == CANNOT_CREATE_LINK) { // (errno == EACCES) { // 13
			sMsg2 = tr("Cannot create link for this")+" "+sFileTypeMsg;
			sFileTypeMsg = "";
		}
	}

	bool chk = false;
	QStringList slBtnNames = (QStringList() << tr("&Skip") << tr("Skip &all") << tr("&Again") << tr("&Break"));
	qDebug() << "LocalSubsystem::showMsgCannotDo." << sFileTypeMsg;
	int nResult = MessageBox::common(m_pParent,
			sMsg1 + " - QtCommander", m_sProcessedFileName,
			"\n"+sMsg2 +" "+ sFileTypeMsg +". "+ sErrMsg +"\n\n"+ sAsk, chk,
// 			sMsg2 +" "+ sDirTypeMsg +"\n" + sErrMsg + " (" + tr("Appeared error no:") + QString(": %1)").arg(errno), chk,
			slBtnNames, MessageBox::Skip, QMessageBox::Warning
	);
	if (nResult == MessageBox::Skip) {
		m_nSkippedFiles++;
		m_error = SKIP_FILE; // otherwise is CANNOT_REMOVE
		m_processedFiles->append(m_processedFileInfo); // used for updating FileListView
		emit signalStatusChanged(ERROR_OCCURED, m_sRequestingPanel);
	}
	if (nResult == MessageBox::SkipAll) {
		if      (m_currentCommand == REMOVE)
			m_bShowMsgCannotDelete = false;
		else if (m_currentCommand == SET_ATTRIBUTES_CMD)
			m_bShowMsgCannotSetAttributes = false;
		else if (m_currentCommand == FIND)
//			if (m_error == Vfs::CANNOT_OPEN || m_error == Vfs::CANNOT_READ)
				m_bShowMsgCannotCheckFile = false;
		else if (m_currentCommand == MOVE) {
			if (m_error == CANNOT_REMOVE)
				m_bShowMsgCannotDelete = false;
		}
		else if (m_currentCommand == CREATE_LINK) {
			if (m_error == CANNOT_WRITE || m_error == CANNOT_CREATE_LINK)
				m_bShowMsgCannotCreateLink = false;
		}
		else if (m_currentCommand == WEIGH) {
//			if (m_error == Vfs::CANNOT_OPEN || m_error == Vfs::CANNOT_READ)
				m_bShowMsgCannotWeigh = false;
		}
	}
	else if (nResult == MessageBox::Break) {
		m_status = OPR_STOPPED;
		m_error  = NO_ERROR;
		m_opQueue.clear();
		jobFinished();
		return nResult;
	}
	if (nResult != MessageBox::Break) // continue an operation
		emit signalTimerStartStop(true);
	if (nResult != MessageBox::Again) { // continue an operation
		if (m_currentCommand != MOVE && m_currentCommand != CREATE_LINK)
			m_bSkipCurrentFile = true;
	}
	if (m_currentCommand == FIND)
		m_status = FINDING;

	return nResult;
}


bool LocalSubsystem::setAttributesNextFile()
{
	if (m_bChangeRecursivelyAttributes) {
		if (! recursiveOperation(SET_ATTRIBUTES_CMD, m_sSourceFileName))
			m_eAttribChgStat = AttrChgERR;
	}
	else {
		if (! m_sSourceFileName.isEmpty()) {
			m_sProcessedFileName = m_sSourceFileName;
			FileInfo *newFileInfo = new FileInfo(m_processedFileInfo);
			m_eAttribChgStat = setFileOwnerAndGroup(m_sSourceFileName);
			if (m_eAttribChgStat != AttrChgERR) { // probably effective user ID equals the user ID of the file
				bool bItemAdded = false;
				if (m_eAttribChgStat == AttrChgOK) {
					newFileInfo->setData(COL_OWNER, m_pNewAttributes->sOwner);
					newFileInfo->setData(COL_GROUP, m_pNewAttributes->sGroup);
					m_processedFiles->append(newFileInfo); // used for updating FileListView
					m_slListOfItemsToModify.append(m_processedFileInfo->fileName());
					bItemAdded = true;
				}
				m_eAttribChgStat = setFilePermissions(m_sSourceFileName);
				if (m_eAttribChgStat != AttrChgERR) {
					if (m_eAttribChgStat == AttrChgOK) {
						newFileInfo->setData(COL_PERM, permissionsToString(m_pNewAttributes->nPermissions, m_processedFileInfo->isDir()));
						if (! bItemAdded) {
							m_processedFiles->append(newFileInfo); // used for updating FileListView
							m_slListOfItemsToModify.append(m_processedFileInfo->fileName());
							bItemAdded = true;
						}
					}
					m_eAttribChgStat = setFileTime(m_sSourceFileName);
					if (m_eAttribChgStat != AttrChgERR) {
						if (m_eAttribChgStat == AttrChgOK) {
							newFileInfo->setData(COL_TIME, m_pNewAttributes->dtLastModifiedTime.toString("yyyy-MM-dd HH:mm:ss"));
							if (! bItemAdded) {
								m_processedFiles->append(newFileInfo); // used for updating FileListView
								m_slListOfItemsToModify.append(m_processedFileInfo->fileName());
								bItemAdded = true;
							}
						}
					}
				}
				if (! bItemAdded)
					delete newFileInfo;
			}
		}
		// NOTE: multi rename work only for non recursively mode
		if (m_pNewAttributes->changeAttribNameFor != FileAttributes::NONE) {
			for (int i = 0; i < m_pNewAttributes->slRenamedFiles.count(); ++i) {
				if (m_processedFileInfo->absoluteFileName() == m_pNewAttributes->slOriginalFilesName[i])
					rename(m_processedFileInfo, m_pNewAttributes->slRenamedFiles[i], m_sRequestingPanel);
			}
		}
		if (m_bAttribChgSuccess) {
			m_bSkipCurrentFile = false;
			m_status = Vfs::SET_ATTRIBUTES;
			emit signalStatusChanged(m_status, m_sRequestingPanel);
		}
		else {
			showMsgCannotDo(nullptr);
			if (m_bSkipCurrentFile) {
				m_error = SKIP_FILE;
				if (m_currentCommand == REMOVE) m_status = REMOVING;
				else
				if (m_currentCommand == WEIGH) m_status = WEIGHING;
				else
				if (m_currentCommand == SET_ATTRIBUTES_CMD) m_status = SETTING_ATTRIBUTES;
				else
				if (m_currentCommand == FIND) m_status = FINDING;
			}
		}
	}

	return (m_eAttribChgStat != AttrChgERR);
}

LocalSubsystem::AttribChgStat LocalSubsystem::setFilePermissions( const QString& sInFileName, int inPermissions )
{
	if (m_pNewAttributes->changePermFor == FileAttributes::NONE && inPermissions < 0)
		return AttrChgSKIP;

	int permissions = (inPermissions < 0) ? m_pNewAttributes->nPermissions : inPermissions;
	QString sFileName = sInFileName;
	m_fInfo.setFile(sInFileName);
	m_bAttribChgSuccess = false;

	if (m_bFollowByLink) {
		sFileName = (m_fInfo.isSymLink()) ? m_fInfo.symLinkTarget() : sInFileName;
	}
	bool changePerm;
	if (m_pNewAttributes->changePermFor == FileAttributes::FILES_AND_DIRS)
		changePerm = true;
	else
	if (m_fInfo.isDir() && ! m_fInfo.isSymLink())
		changePerm = (m_pNewAttributes->changePermFor == FileAttributes::DIRS_ONLY);
	else
		changePerm = (m_pNewAttributes->changePermFor == FileAttributes::FILES_ONLY);

	if (changePerm) {
		if (::chmod(QFile::encodeName(sFileName), permissions) != 0) {
			m_status = ERROR_OCCURED;
			m_error  = CANNOT_SET_PERM;
			qDebug() << "LocalSubsystem::setFilePermissions. chmod error:" << errno << "file:" << sFileName;
			return AttrChgERR;
		}
		else
			m_bAttribChgSuccess = true;
	}
	else
		return AttrChgSKIP;

	return AttrChgOK;
}

LocalSubsystem::AttribChgStat LocalSubsystem::setFileTime( const QString& sInFileName, int inSecondsToAccess, int inSecondsToModification )
{
	if (m_pNewAttributes->changeAccModTimeFor == FileAttributes::NONE && (inSecondsToAccess < 0 && inSecondsToModification < 0))
		return AttrChgSKIP;

	if (inSecondsToAccess > 0 && inSecondsToModification > 0) {
		m_uTimeBuf.actime  = inSecondsToAccess;
		m_uTimeBuf.modtime = inSecondsToModification;
	}
	QString sFileName = sInFileName;
	m_fInfo.setFile(sInFileName);
	m_bAttribChgSuccess = false;

	if (m_bFollowByLink) {
		sFileName = (m_fInfo.isSymLink()) ? m_fInfo.symLinkTarget() : sInFileName;
	}
	bool changePerm;
	if (m_pNewAttributes->changeAccModTimeFor == FileAttributes::FILES_AND_DIRS)
		changePerm = true;
	else
	if (m_fInfo.isDir() && ! m_fInfo.isSymLink())
		changePerm = (m_pNewAttributes->changeAccModTimeFor == FileAttributes::DIRS_ONLY);
	else
		changePerm = (m_pNewAttributes->changeAccModTimeFor == FileAttributes::FILES_ONLY);

	if (changePerm) {
		if (::utime(QFile::encodeName(sFileName), &m_uTimeBuf) != 0) {
			m_status = ERROR_OCCURED;
			m_error  = CANNOT_SET_MA_TIME;
			qDebug() << "LocalSubsystem::setFileTime. utime error:" << errno << "file:" << sFileName;
			return AttrChgERR;
		}
		else
			m_bAttribChgSuccess = true;
	}
	else
		return AttrChgSKIP;

	return AttrChgOK;
}

LocalSubsystem::AttribChgStat LocalSubsystem::setFileOwnerAndGroup( const QString& sFileName, const QString& sOwner, const QString& sGroup )
{
	if (m_pNewAttributes->changeOwnGrpFor == FileAttributes::NONE)
		return AttrChgSKIP;

	int ownerId = ( sOwner.isEmpty()) ? -1 : m_nNewOwnerId;
	int groupId = ( sGroup.isEmpty()) ? -1 : m_nNewGroupId;

	return setFileOwnerAndGroup(sFileName, ownerId, groupId);
}

LocalSubsystem::AttribChgStat LocalSubsystem::setFileOwnerAndGroup( const QString &sInFileName, int inOwnerId, int inGroupId )
{
	if (m_pNewAttributes->changeOwnGrpFor == FileAttributes::NONE && (inOwnerId < 0 && inGroupId < 0))
		return AttrChgSKIP;

	int ownerId = inOwnerId;
	int groupId = inGroupId;
	if (inOwnerId < 0 && inGroupId < 0) {
		ownerId = m_nNewOwnerId;
		groupId = m_nNewGroupId;
	}
	QString sFileName = sInFileName;
	m_fInfo.setFile(sFileName);
	m_bAttribChgSuccess = false;

	if (m_nCurrentUserID != 0) { // if not superuser then check whether ownerId of sFileName is changed
		if (ownerId > 0 && m_fInfo.ownerId() != (uint)ownerId) {
			m_status = ERROR_OCCURED;
			m_error  = CANNOT_SET_OWNER_GROUP; // because only superuser can change owner of sFileName
			qDebug() << "LocalSubsystem::setFileOwnerAndGroup. only superuser can change owner for file:" << sFileName;
			return AttrChgERR;
		}
	}

	if (m_bFollowByLink) {
		sFileName = (m_fInfo.isSymLink()) ? m_fInfo.symLinkTarget() : sInFileName;
	}
	bool changePerm;
	if (m_pNewAttributes->changeOwnGrpFor == FileAttributes::FILES_AND_DIRS)
		changePerm = true;
	else
	if (m_fInfo.isDir() && ! m_fInfo.isSymLink())
		changePerm = (m_pNewAttributes->changeOwnGrpFor == FileAttributes::DIRS_ONLY);
	else
		changePerm = (m_pNewAttributes->changeOwnGrpFor == FileAttributes::FILES_ONLY);

	if (changePerm) {
		if (::chown(QFile::encodeName(sFileName), ownerId, groupId) != 0) {
			m_status = ERROR_OCCURED;
			m_error  = CANNOT_SET_OWNER_GROUP;
			qDebug() << "LocalSubsystem::setFileOwnerAndGroup. chown error:" << errno << "file:" << sFileName << "current user Id:" << m_nCurrentUserID;
			return AttrChgERR;
		}
		else
			m_bAttribChgSuccess = true;
	}
	else
		return AttrChgSKIP;

	return AttrChgOK;
}

void LocalSubsystem::addMatchedItem()
{
// 	QDateTime timeAccess, timeModified;
// 	timeAccess.setTime_t(m_statBuffer.st_atime);
// 	timeModified.setTime_t(m_statBuffer.st_mtime);

	m_fInfo.setFile(m_sAbsSrcFileName);
	QString sPerm = permissionsAsString(m_fInfo.absoluteFilePath());
	QString sMime = MimeType::getFileMime(m_fInfo.absoluteFilePath(), m_fInfo.isDir(), m_fInfo.isSymLink());
	FileInfo *item = new FileInfo(m_fInfo.fileName(), m_fInfo.absolutePath(), m_fInfo.size(), m_fInfo.isDir(), m_fInfo.isSymLink(),
	                              m_fInfo.lastModified(), sPerm, m_fInfo.owner(), m_fInfo.group(), sMime, m_fInfo.symLinkTarget());
	m_nMatchedFiles++;
// 	emit signalUpdateFindStatus(++m_nMatchedFiles, m_nFileCounter, m_nDirCounter); // TODO add supporrt for this in SubsystemManager::slotStatusChanged
	m_processedFileInfo = item;
	m_processedFiles->append(m_processedFileInfo);
	m_status = FOUND;
	emit signalStatusChanged(m_status, m_sRequestingPanel);
	m_status = FINDING;
}


bool LocalSubsystem::currentFileMatches()
{
	bool matchFileOwner = true;
	bool matchFileGroup = true;
	bool matchTime = true;
	bool matchSize = true;
	bool matchStop = true;
	bool matchName = false;
// 	bool matchName = m_findRegExp.exactMatch(m_sCurrentFileName); // check name matches
	foreach (QRegExp p, m_findRegExpLst) {
		if ((matchName=p.exactMatch(m_sCurrentFileName))) // check if given name matches to pattern
			break;
	}
	if (! matchName)
		return false;

	if (m_findCriterion.checkFileOwner)
		matchFileOwner = (m_statBuffer.st_uid == (uint)m_findCriterion.ownerId);
	if (m_findCriterion.checkFileGroup)
		matchFileGroup = (m_statBuffer.st_gid == (uint)m_findCriterion.groupId);

	if (m_findCriterion.checkTime) {
		QDateTime fileTime = QFileInfo(m_sAbsSrcFileName).lastModified();
		matchTime = (fileTime >= m_findCriterion.firstTime && fileTime <= m_findCriterion.secondTime);
	}

	if (    m_findCriterion.checkFileSize != FindCriterion::NONE) {
		if (m_findCriterion.checkFileSize == FindCriterion::EQUAL)
			matchSize = (m_statBuffer.st_size == m_findCriterion.fileSize);
		else if (m_findCriterion.checkFileSize == FindCriterion::ATLEAST)
			matchSize = (m_statBuffer.st_size >= m_findCriterion.fileSize);
		else if (m_findCriterion.checkFileSize == FindCriterion::MAXIMUM)
			matchSize = (m_statBuffer.st_size <= m_findCriterion.fileSize);
		else if (m_findCriterion.checkFileSize == FindCriterion::GREAT_THAN)
			matchSize = (m_statBuffer.st_size >  m_findCriterion.fileSize);
		else if (m_findCriterion.checkFileSize == FindCriterion::LESS_THAN)
			matchSize = (m_statBuffer.st_size <  m_findCriterion.fileSize);
	}

	if (m_findCriterion.stopIfXMatched) {
		if (m_nMatchedFiles == m_findCriterion.stopAfterXMaches) {
			matchStop = false;
			jobFinished();
		}
	}

	return (matchName && matchFileOwner && matchFileGroup && matchTime && matchSize && matchStop);
}


void LocalSubsystem::readFileToViewer( const QString &sHost, FileInfo *pFileInfo, QByteArray &baBuffer, int &nBytesRead, int nReadingMode, const QString &sRequestingPanel )
{
	QString sFN = "LocalSubsystem::readFileToViewer.";
	qDebug() << sFN << "sFileName:" << pFileInfo->absoluteFileName() << "bytes to read:" << nBytesRead  << "m_baBinBuffer.size" << baBuffer.size() << "nReadingMode:" << Viewer::toString((Viewer::ReadingMode)nReadingMode);
	m_currentCommand = READ_FILE_TO_VIEW;
	m_sProcessedFileName = pFileInfo->fileName();
	m_sRequestingPanel = sRequestingPanel.left(sRequestingPanel.lastIndexOf('_'));

	if (nReadingMode == Viewer::GET_READABILITY) {
		nBytesRead = canOpenFileForReading(pFileInfo) ? 1 : -1; // need for FileViewer check, also sets m_status, m_error and emits signalStatusChanged
		qDebug() << sFN << "--- file readable:" << (m_error == NO_ERROR);
		return;
	}
	if (pFileInfo->size() == 0) { // empty file
		m_status = READ_FILE;
		m_error  = NO_ERROR;
		emit signalStatusChanged(m_status, sRequestingPanel);
		nBytesRead = -1; // special flag telling FileViewer::slotReadFile that this is empty file
		return;
	}
	if (nBytesRead == 0)
		return;

	if (! m_bFileHasBeenOpened) {
		if (m_FileViewer.fileName() != pFileInfo->absoluteFileName())
			m_FileViewer.setFileName(pFileInfo->absoluteFileName());

		if (! m_FileViewer.open(QIODevice::ReadOnly)) {
			nBytesRead = 0;
			return;
		}
		else
			m_bFileHasBeenOpened = true;
	}

	m_status = READING_FILE;
	m_error  = NO_ERROR;
	qDebug() << sFN << "reading...";
	if (nReadingMode == Viewer::ALL || nBytesRead < 0) { // read all bytes of file
		baBuffer += m_FileViewer.readAll();
		m_bFileHasBeenOpened = false;
		m_FileViewer.close();
		nBytesRead = baBuffer.size();
	}
	else
	if (nReadingMode == Viewer::APPEND) { // read file by parts
		char *cBuffer = new char[nBytesRead +1]; // resize - +1 for null character (need to text nFiles)
		int nRealReadBytes = m_FileViewer.read(cBuffer, nBytesRead);
		if (nRealReadBytes < 0) { // an error occured
			m_sProcessedFileName = m_sSourceFileName;
			m_bFileHasBeenOpened = false;
			m_status = NOT_READY_TO_READ;
			m_error  = READING_ERROR;
			m_FileViewer.close();
		}
		else
		if (nRealReadBytes == 0) {
			m_FileViewer.close();
			m_bFileHasBeenOpened = false;
			m_status = READ_FILE;
		}
		if (nRealReadBytes > 0) {
			baBuffer.resize(0); // release buffer, because viewer has been updated
			baBuffer.reserve(nRealReadBytes);
			baBuffer.append(cBuffer, nRealReadBytes);
			//qDebug() << "baBuffer[0]:" << baBuffer[0];
			//qDebug() << "baBuffer[nRealReadBytes-1]:" << baBuffer[nRealReadBytes-1];
		}
		delete [] cBuffer;
		nBytesRead = nRealReadBytes;
	}

	emit signalStatusChanged(m_status, sRequestingPanel);
}


void LocalSubsystem::slotStartProcessDir()
{
// 	qDebug() << "LocalSubsystem::slotStartProcessDir. status:" << toString(m_status) << " AbsSrcFileName:" << m_sAbsSrcFileName;
	if (m_bPauseOperation || m_status == OPR_PAUSED)
		return;
	if (m_bBreakOperation || m_status == OPR_STOPPED) { // for find operation
		jobFinished();
		return;
	}
	m_bOkStat = (::lstat64(QFile::encodeName(m_sAbsSrcFileName), & m_statBuffer) == 0);
	if (! m_bOkStat) {
		qDebug() << "LocalSubsystem::slotStartProcessDir. Cannot perform stat for:" << m_sAbsSrcFileName << "lstat error:" << errno;
		m_error = CANNOT_STAT;
		m_sProcessedFileName = m_sAbsSrcFileName;
	}
	m_stackOfDirInode.push(m_statBuffer.st_ino);
	m_bNeedToSeek = false;

	if (! m_bWasDir) {
		if (m_stackForFirstInodeOfDir.isEmpty()) { //if (mDirInodeStack.top() == 0) { // or m_currLocInDIRstack.isEmpty()
			// --- recursive operation is finished
			// post operation actions for top directory
			if (m_recursiveOperation == WEIGH) {
				m_nTotalFiles++; // count top dir as totalFiles
				m_realTotalWeight += m_bOkStat ? (m_statBuffer.st_blksize/8)*m_statBuffer.st_blocks : 0; // ((4096/8)*allocatedBlocks) add weigh of current
				emit signalCounterProgress(++m_nDirCounter, DIR_COUNTER, true); // count top dir
			}
			else if (m_recursiveOperation == REMOVE) {
				m_bOkStat = (::rmdir(QFile::encodeName(m_sAbsSrcFileName)) == 0); // attempt to removing empty (if there were no errors before) top dir
				m_processedFiles->append(m_processedFileInfo);
				if (! m_bOkStat)
					m_error = CANNOT_REMOVE;
			} // if (m_error != NoError)
			else if (m_recursiveOperation == SET_ATTRIBUTES_CMD) {
				m_eAttribChgStat = setFileOwnerAndGroup(m_sAbsSrcFileName); // attempt to setting attributes for top dir
				if (m_eAttribChgStat != AttrChgERR) { // probably effective user ID equals the user ID of the file
					if ((m_eAttribChgStat=setFilePermissions(m_sAbsSrcFileName)) != AttrChgERR)
						m_eAttribChgStat = setFileTime(m_sAbsSrcFileName);
				}
				m_bOkStat = (m_eAttribChgStat != AttrChgERR);
			}
			else if (m_recursiveOperation == COPY || m_recursiveOperation == MOVE) {
				if (m_bSaveAttributes) {
					// if effective user ID equals the user ID of the file then setTime and setPerm successful
					setFileTime(m_sAbsTargetDirName, m_statBuffer.st_atime, m_statBuffer.st_mtime);
					setFilePermissions(m_sAbsTargetDirName, Vfs::permissions(m_sAbsSrcFileName));
				}
				FileInfo *fi = new FileInfo(m_processedFileInfo);
				if (m_recursiveOperation == MOVE)
					m_postMovedFiles->append(fi);
// 				fi->setMark(false); // m_selectionMap in VfsFlatListModel isn't updated
				fi->setLocation(fi->filePath());
				fi->setData(2, m_sCopyTargetPath, true); // workaround for setting path
				fi->setData(2, m_fInfo.lastModified());
				fi->setData(3, permissionsToString(permissions(m_processedFileInfo->absoluteFileName()), false));
				fi->setData(4, m_fInfo.owner());
				fi->setData(5, m_fInfo.group());
				if (m_recursiveOperation == NO_COMMAND) {
					if (! m_renameInCopying.isEmpty()) {
						QString sFileName = m_renameInCopying.right(m_renameInCopying.length() - m_renameInCopying.lastIndexOf(QDir::separator()) - 1);
						QString sPathName = m_renameInCopying.left(m_renameInCopying.length() - sFileName.length());
						if (fi->filePath() == sPathName)
							fi->setData(0, sFileName);
						m_renameInCopying = "";
					}
				}
				if (m_recursiveOperation == MOVE) {
					m_bOkStat = (::rmdir(QFile::encodeName(m_sAbsSrcFileName)) == 0); // attempt to removing empty (if there were no errors before) top dir
					if (! m_bOkStat) {
						m_error = CANNOT_REMOVE;
						m_processedFiles->append(m_processedFileInfo);
					}
					else
						m_processedFiles->append(fi);
				}
				else
					m_processedFiles->append(fi);
				if (m_error == NO_ERROR)
					qDebug() << "LocalSubsystem::slotStartProcessDir. _add_file_ (spd)" << m_processedFileInfo->fileName() << "size" << m_processedFiles->size();
			}
			// update an operation progress
			if (m_recursiveOperation != COPY && m_recursiveOperation != MOVE && m_recursiveOperation != FIND) {
				if (m_bOkStat && m_error == NO_ERROR) {
					if (m_recursiveOperation != WEIGH) {
						if ((!(m_recursiveOperation == REMOVE && m_currentCommand == REMOVE) && m_currentOperation != SET_ATTRIBUTES_CMD) || m_currentOperation == REMOVE) {
							m_nOpFileCounter++; // count top dir for remove
							emit signalCounterProgress((m_recursiveOperation == REMOVE) ? ++m_nDirCounterR : ++m_nDirCounter, DIR_COUNTER); // count top dir
						}
						if (m_currentOperation == SET_ATTRIBUTES_CMD && m_bAttribChgSuccess) {
							m_nOpFileCounter++; // count top dir for setAttributes
							emit signalCounterProgress((m_recursiveOperation == SET_ATTRIBUTES_CMD) ? ++m_nDirCounterR : ++m_nDirCounter, DIR_COUNTER); // count top dir
						}
						if (m_nTotalFiles)
							emit signalTotalProgress((100 * m_nOpFileCounter) / m_nTotalFiles, m_totalWeight);
					}
				}
			}
			// if one of above operation has finished by an error then show announcement
			if (! m_bOkStat) {
				if (m_recursiveOperation == REMOVE || m_recursiveOperation == SET_ATTRIBUTES_CMD)
					m_sProcessedFileName = m_sAbsSrcFileName+"/";
				else
					m_sProcessedFileName = m_sAbsTargetDirName+"/";
				if (m_recursiveOperation == REMOVE)
					m_error = CANNOT_REMOVE;
				m_status = ERROR_OCCURED;
				// emit commandFinished(m_currentCommand, (m_error!=NO_ERROR));
			}
			else
			if (m_error == NO_ERROR) {
				bool bNotStopped = (m_status != OPR_STOPPED);
				if (m_currentOperation == WEIGH) {
					m_slListOfItemsToModify.clear();
					m_slListOfItemsToModify.append(QFileInfo(m_sProcessedFileName).fileName());
					if (bNotStopped)
						m_status = WEIGHED;
					if (! m_bWeighingAsSubCmd) {
						emit signalStatusChanged(m_status, m_sRequestingPanel);
					}
				}
				else if (m_currentOperation == COPY && bNotStopped)
					m_status = COPIED;
				else if (m_currentOperation == MOVE && bNotStopped)
					m_status = MOVED;
				else if (m_currentOperation == REMOVE && bNotStopped)
					m_status = REMOVED;
				else if (m_currentOperation == SET_ATTRIBUTES_CMD && bNotStopped)
					m_status = SET_ATTRIBUTES;
			}
			// cleanig list and stacks
			while (! m_stackForCurrLocInDir.isEmpty())
				m_stackForCurrLocInDir.pop();
			while (! m_stackForFirstInodeOfDir.isEmpty())
				m_stackForFirstInodeOfDir.pop();
			while (! m_stackOfDIRptrs.isEmpty())
				::closedir(m_stackOfDIRptrs.pop());

			if (m_error != NO_ERROR && (m_recursiveOperation == REMOVE || m_recursiveOperation == SET_ATTRIBUTES_CMD || m_recursiveOperation == FIND)) {
				if ((m_bShowMsgCannotDelete        && m_currentCommand == REMOVE) ||
					(m_bShowMsgCannotSetAttributes && m_currentCommand == SET_ATTRIBUTES_CMD) ||
					(m_bShowMsgCannotCheckFile     && m_currentCommand == FIND)) {
					int result = showMsgCannotDo(m_processedFileInfo);
					if (result == MessageBox::Again) {
						m_error = NO_ERROR;
						recursiveOperation(m_currentCommand, m_sAbsSrcFileName);
						return;
					}
					else if (result == MessageBox::Break) {
						return;
					}
				}
			}
// 			m_recursiveOperation = NO_COMMAND; // commented due to such set should be doing only in recursiveOperation(), jobFinished() or post-init functions
			if (! m_opQueue.isEmpty())
				m_opQueue.pop_front(); // remove Put or single action
			if (m_opQueue.isEmpty())
				jobFinished();
			else
				slotStartNextJob(); //QTimer::singleShot(0, this, &LocalSubsystem::slotStartNextJob);
			return;
		}

		m_stackForThrowedOffInode.push(m_stackForFirstInodeOfDir.pop());

		if (m_stackOfDirInode.first() == m_stackForThrowedOffInode.first()) {
			if (m_recursiveOperation == REMOVE) { // attempt remove top empty dir (success is only if we were able to remove files inside)
				m_bOkStat = (::rmdir(QFile::encodeName(m_sAbsSrcFileName)) == 0);
				if (m_bOkStat) {
					emit signalCounterProgress(++m_nDirCounterR, DIR_COUNTER);
// 					emit signalCounterProgress((m_currentCommand == REMOVE) ? ++m_nDirCounterR : ++m_nDirCounter, DIR_COUNTER); // always m_recursiveOperation
					m_nOpFileCounter++;
					emit signalTotalProgress((100 * m_nOpFileCounter) / m_nTotalFiles, m_totalWeight);
				}
				else {
					qDebug() << "LocalSubsystem::slotStartProcessDir. Cannot remove directory:" << m_sAbsSrcFileName << "rmdir error:" << errno;
					m_sProcessedFileName = m_sAbsSrcFileName;
					m_error = CANNOT_REMOVE;
				}
			}
			else
			if (m_recursiveOperation == SET_ATTRIBUTES_CMD) { // attempt to set attributes top dir
				m_eAttribChgStat = setFileOwnerAndGroup(m_sAbsSrcFileName);
				if (m_eAttribChgStat != AttrChgERR) { // probably effective user ID equals the user ID of the file
					if ((m_eAttribChgStat=setFilePermissions(m_sAbsSrcFileName)) != AttrChgERR)
						m_eAttribChgStat = setFileTime(m_sAbsSrcFileName);
				}
				m_bOkStat = (m_eAttribChgStat != AttrChgERR);
				if (m_bOkStat) {
					if (m_bAttribChgSuccess) {
						emit signalCounterProgress(++m_nDirCounterR, DIR_COUNTER);
// 						emit signalCounterProgress((m_currentCommand == SET_ATTRIBUTES_CMD) ? ++m_nDirCounterR : ++m_nDirCounter, DIR_COUNTER); // always m_recursiveOperation
						m_nOpFileCounter++;
						emit signalTotalProgress((100 * m_nOpFileCounter) / m_nTotalFiles, m_totalWeight);
					}
				}
				else {
// 					qDebug() << "LocalSubsystem::slotStartProcessDir. Cannot set attributes for directory:" << m_sAbsSrcFileName << "error:" << errno;
					m_sProcessedFileName = m_sAbsSrcFileName;
				}
			}
			else
			if (m_recursiveOperation == COPY || m_recursiveOperation == MOVE) { // sets attributs for subdirectory
				if (m_bSaveAttributes) {
					// if effective user ID equals the user ID of the file then setTime and setPerm successful
					setFileTime(m_sAbsTargetDirName, m_statBuffer.st_atime, m_statBuffer.st_mtime);
					setFilePermissions(m_sAbsTargetDirName, Vfs::permissions(m_sAbsSrcFileName));
					if (m_error != NO_ERROR) {
						m_sProcessedFileName = m_sAbsTargetDirName+"/";
						emit signalStatusChanged(m_status, m_sRequestingPanel);
						//emit commandFinished(m_currentCommand, (m_error!=NO_ERROR));
					}
				}
			}
			if (m_error == NO_ERROR) { // one might to go dir up only if there was no error before
				m_sAbsSrcFileName = m_sAbsSrcFileName.left(m_sAbsSrcFileName.lastIndexOf('/')); // set path to upper dir
				m_sAbsTargetFileName = m_sAbsTargetFileName.left(m_sAbsTargetFileName.lastIndexOf('/'));
				if (m_sAbsSrcFileName.isEmpty())
					m_sAbsSrcFileName = "/";
				if ((m_recursiveOperation == COPY || m_recursiveOperation == MOVE) && m_sAbsTargetFileName.isEmpty())
					m_sAbsTargetFileName = "/";
				m_sAbsTargetDirName = m_sAbsTargetFileName;
				m_nCurrLocInDIR = (off_t)m_stackForCurrLocInDir.pop();
				m_bNeedToSeek = true;
			}
		}
	}
	m_bWasDir = false; // NOTE: even though dir happend before, values is set to false. Why?
	DIR *pTmpDirPtr = nullptr;
	if (m_error == NO_ERROR) // before could occure an error and we should not open dir in this case
		pTmpDirPtr = ::opendir(QFile::encodeName(m_sAbsSrcFileName));
	if (pTmpDirPtr != nullptr) {
		m_pDIR = pTmpDirPtr;
		m_stackOfDIRptrs.push(m_pDIR);
		m_nWasDots = -1;
	}
	else {
		if (m_error == NO_ERROR) { // no error before, but opendir finished fail
			qDebug() << "LocalSubsystem::slotStartProcessDir. Cannot open directory:" << m_sAbsSrcFileName << "opendir error:" << errno;
			m_sProcessedFileName = m_sAbsSrcFileName;
			m_status = ERROR_OCCURED;
			m_error  = CANNOT_OPEN;
		}
		if ((m_bShowMsgCannotDelete        && m_currentCommand == REMOVE) ||
			(m_bShowMsgCannotWeigh         && m_currentCommand == WEIGH) ||
			(m_bShowMsgCannotSetAttributes && m_currentCommand == SET_ATTRIBUTES_CMD) ||
			(m_bShowMsgCannotCheckFile     && m_currentCommand == FIND) || m_error == CANNOT_OPEN) { // no currentCommandFinished call, due to recursively calling of slotDirProcessing
			int result = showMsgCannotDo(m_processedFileInfo);
			if (result == -1 || result == MessageBox::SkipAll || result == MessageBox::Skip) { // reset error status to be able continue an operation in silent mode
				m_error = NO_ERROR;
				if (m_currentCommand == REMOVE) m_status = REMOVING;
				else
				if (m_currentCommand == WEIGH) m_status = WEIGHING;
				else
				if (m_currentCommand == SET_ATTRIBUTES_CMD) m_status = SETTING_ATTRIBUTES;
				else
				if (m_currentCommand == FIND) m_status = FINDING;
			}
			else
			if (result == MessageBox::Again) {
				m_error = NO_ERROR;
				m_sCurrentFileName = m_sAbsSrcFileName.right(m_sAbsSrcFileName.length() - m_sAbsSrcFileName.lastIndexOf('/') - 1);
				m_sAbsSrcFileName  = m_sAbsSrcFileName.left(m_sAbsSrcFileName.lastIndexOf('/'));
				if (! m_stackOfDIRptrs.isEmpty())
					m_stackOfDIRptrs.pop();
				m_nCurrLocInDIR = (off_t)m_stackForCurrLocInDir.pop(); // for correct seekdir after MessageBox::Again
				pTmpDirPtr = ::opendir(QFile::encodeName(m_sAbsSrcFileName));
				if (pTmpDirPtr != nullptr) {
					m_pDIR = pTmpDirPtr;
					m_stackOfDIRptrs.push(m_pDIR);
					m_bOkStat=(::chdir(QFile::encodeName(m_sAbsSrcFileName)) == 0);
					m_nWasDots = -1;
					if (! m_bOkStat) {
						qDebug() << "LocalSubsystem::slotStartProcessDir. Cannot change directory:" << m_sAbsSrcFileName << "chdir error:" << errno;
						m_status = ERROR_OCCURED;
						m_error  = CANNOT_READ;
						return;
					}
					m_bNeedToSeek = true;
				}
				if (m_bNeedToSeek) {  // for correct seekdir after MessageBox::Again
					if (m_nCurrLocInDIR != 0) // offset
						::seekdir(m_pDIR, m_nCurrLocInDIR);
				}
				slotDirProcessingItem();
				return;
			}
			else if (result == MessageBox::Break) {
				return;
			}
		}
		if (pTmpDirPtr == nullptr) { // the most likely CANNOT_REMOVE (also CANNOT_OPEN during weighing) // if we cannot remove potentialy empty dir, from which wasn't removed files then need to change path to parent
			m_sAbsSrcFileName = m_sAbsSrcFileName.left(m_sAbsSrcFileName.lastIndexOf('/')); // set path to upper dir
			if (m_sAbsSrcFileName.isEmpty())
				m_sAbsSrcFileName = "/";
		}
		else {
			m_error = NO_ERROR;
		}
		if (! m_stackForCurrLocInDir.isEmpty())
			m_nCurrLocInDIR = (off_t)m_stackForCurrLocInDir.pop();
		if (! m_stackForFirstInodeOfDir.isEmpty()) // fix for cannot read dir in first level
			m_stackForFirstInodeOfDir.pop();
		if (! m_stackOfDIRptrs.isEmpty())
			m_stackOfDIRptrs.pop();
// 		m_nCurrLocInDIR = (off_t)m_currLocInDIRstack.pop();
// 		while (! m_currLocInDIRstack.isEmpty() || m_nCurrLocInDIR == 0) // test, bad: made skipping files and dirs
// 			m_nCurrLocInDIR = (off_t)m_currLocInDIRstack.pop();
		if ((pTmpDirPtr=::opendir(QFile::encodeName(m_sAbsSrcFileName))) != nullptr) { // open parent dir
			m_pDIR = pTmpDirPtr;
			m_stackOfDIRptrs.push(m_pDIR);
			m_nWasDots = -1; // because new dir is opened, and slotDirProcessing will check it
		}
		m_bNeedToSeek = true;
	}
	if (m_error == NO_ERROR || m_error == SKIP_FILE) {
		if (::chdir(QFile::encodeName(m_sAbsSrcFileName)) != 0) { // if cannot open parent dir
			qDebug() << "LocalSubsystem::slotStartProcessDir. Cannot change directory:" << m_sAbsSrcFileName << "chdir error:" << errno;
			m_sProcessedFileName = m_sAbsSrcFileName;
			m_status = ERROR_OCCURED;
			m_error  = CANNOT_OPEN;
			if (m_stackForFirstInodeOfDir.isEmpty()) { // cannot chdir to main op.dir.
				if (m_currentCommand == WEIGH) { // allow to continue an operation after weighing error
					emit signalStatusChanged(m_status, m_sRequestingPanel); // show error message
					m_status = WEIGHED; // was an error, but it is necessary for continue operation
					m_error  = NO_ERROR;
					m_nTotalFiles++; // count top dir as totalFiles
					m_realTotalWeight += m_bOkStat ? (m_statBuffer.st_blksize/8)*m_statBuffer.st_blocks : 0; // ((4096/8)*allocatedBlocks) add weigh of current
					emit signalCounterProgress(++m_nDirCounter, DIR_COUNTER, true); // count top dir
					if (m_opQueue.size() > 1) {
						m_recursiveOperation = NO_COMMAND; // why?
						m_stackForCurrLocInDir.clear();
						m_stackForFirstInodeOfDir.clear();
						while (! m_stackOfDIRptrs.isEmpty())
							::closedir(m_stackOfDIRptrs.pop());
						m_stackOfDirInode.clear();
						m_stackForThrowedOffInode.clear();
						m_opQueue.pop_front();
						slotStartNextJob();
					}
				}
				else
					jobFinished();
				return;
			}
			else {
				m_sAbsSrcFileName = QFileInfo(m_sAbsSrcDirName).absolutePath();
				int result = showMsgCannotDo(m_processedFileInfo);
				if (result == -1 || result == MessageBox::SkipAll || result == MessageBox::Skip) {
					m_error = NO_ERROR; // to be able continue an operation in silent mode
				}
				else
				if (result == MessageBox::Again) {
					m_error = NO_ERROR;
					m_sAbsSrcFileName = QFile::decodeName(::get_current_dir_name()); // restore current dir
					m_nCurrLocInDIR = ::telldir(m_pDIR);
// 					::closedir(m_pDIR);
					if ((pTmpDirPtr=::opendir(QFile::encodeName(m_sAbsSrcFileName))) != nullptr) {
						m_pDIR = pTmpDirPtr;
						m_stackOfDIRptrs.push(m_pDIR);
						m_nWasDots = -1; // because new dir is opened, and slotDirProcessing will check it
						::seekdir(m_pDIR, m_nCurrLocInDIR_prev);
					}
					m_pDirNextEntry = ::readdir(m_pDIR);
					slotDirProcessingItem(); //QTimer::singleShot(0, this, &LocalSubsystem::slotDirProcessingItem);
					return;
				}
				else if (result == MessageBox::Break) {
					return;
				}
				if (! m_stackForCurrLocInDir.isEmpty())  m_stackForCurrLocInDir.pop();
				if (! m_stackForFirstInodeOfDir.isEmpty()) m_stackForFirstInodeOfDir.pop();
				if (! m_stackOfDIRptrs.isEmpty())        m_stackOfDIRptrs.pop();
				m_bNeedToSeek = true;
				if (m_currentCommand == WEIGH) {
					m_status = WEIGHING;
					m_error = NO_ERROR;
				}
			}
		}
	}
	if (m_bNeedToSeek) {
		if (m_nCurrLocInDIR != 0) // offset
			::seekdir(m_pDIR, m_nCurrLocInDIR);
	}
	else
		m_nWasDots = -1;

	if (m_recursiveOperation == WEIGH && m_error == SKIP_FILE) // doesn't go out from dir.after CANNOT_OPEN error
		m_error = NO_ERROR;

	slotDirProcessing();
}


void LocalSubsystem::slotDirProcessing()
{
	if (m_bBreakOperation || m_status == OPR_STOPPED) { // for find operation
		jobFinished();
		return;
	}
	if (m_pDIR != nullptr) {
/*		int error;
		struct dirent entry;
		for (;;) {
			if ((error=readdir_r(m_pDIR, &entry, &m_pDirNextEntry)) != 0) { // (thread safe function)
				qDebug() << "LocalSubsystem::slotDirProcessing. readdir error:" << errno;
				break;
			}
		}
		if (error != 0) {
			if (m_stackOfDIRptrs.size() > 0)
				::closedir(m_stackOfDIRptrs.pop()); // entire directory has been listed, so directory should be close
			slotStartProcessDir();
			return;
		}*/
		m_nCurrLocInDIR_prev = ::telldir(m_pDIR); // useful in FIND (when error appears and Again pressed)
		if ((m_pDirNextEntry=::readdir(m_pDIR)) == nullptr) { // all files have been processed
			if (m_stackOfDIRptrs.size() > 0)
				::closedir(m_stackOfDIRptrs.pop()); // entire directory has been listed, so directory should be close
			slotStartProcessDir();
			return;
		}
		m_sCurrentFileName = QFile::decodeName(m_pDirNextEntry->d_name);
// 		m_nCurrLocInDIR_next = ::telldir(m_pDIR);
	}
	if (m_nWasDots < 1) {
		if (m_sCurrentFileName == "." || m_sCurrentFileName == "..") {
			m_nWasDots++;
			slotDirProcessing();
			return;
		}
	}
// 	if (m_bPauseOperation)
// 		return;
	if (! m_bPauseOperation) // split slotDirProcessing due to be able to perform "Again" option for "showMsgCannotDelete"
		slotDirProcessingItem();
}

void LocalSubsystem::slotDirProcessingItem()
{
	if (m_bBreakOperation || m_status == OPR_STOPPED) { // for find operation
		jobFinished();
		return;
	}
// 	m_bOkStat = (::lstat(QFile::encodeName(m_sCurrentFileName), & m_statBuffer) == 0);
	m_bOkStat = (::lstat64(m_pDirNextEntry->d_name, & m_statBuffer) == 0);
// 	m_bOkStat = (::lstat(QFile::encodeName(m_pDirNextEntry->d_name), & m_statBuffer) == 0);
	if (! m_bOkStat) {
		qDebug() << "LocalSubsystem::slotDirProcessingItem. Cannot perform stat for:" << m_pDirNextEntry->d_name << "current dir name:" << QFile::decodeName(::get_current_dir_name()) << "lstat error:" << errno;
		SubsystemStatus prevStatus = m_status;
		m_status = ERROR_OCCURED;  m_error = CANNOT_STAT;
		m_sProcessedFileName = QFile::decodeName(::get_current_dir_name()) + "/" + m_sCurrentFileName;
		emit signalStatusChanged(m_status, m_sRequestingPanel); // show error message
		m_status = prevStatus;  m_error = NO_ERROR;
		if (m_status != FINDING) {
			slotDirProcessing(); //QTimer::singleShot(0, this, &LocalSubsystem::slotDirProcessing);
			return;
		}
	}
	if (m_pDIR == nullptr)
		m_bWasDir = S_ISDIR(m_statBuffer.st_mode);
	else {
		m_bWasDir = false;
		m_sAbsSrcFileName += "/"+m_sCurrentFileName;
		m_error = NO_ERROR;
	}

	if (m_recursiveOperation == COPY || m_recursiveOperation == MOVE) {
		m_sAbsTargetFileName += "/"+m_sCurrentFileName;
		m_sSourceFileName = m_sAbsSrcFileName;
		m_sTargetFileName = m_sAbsTargetFileName;
	}
	if (m_recursiveOperation != FIND)
		emit signalNameOfProcessedFile(m_sAbsSrcFileName, m_sAbsTargetFileName);

	if (S_ISLNK(m_statBuffer.st_mode) && m_bOkStat) {
		if (m_bFollowByLink && (m_recursiveOperation == WEIGH || m_recursiveOperation == REMOVE))
			m_bOkStat = ::stat64(QFile::encodeName(m_sAbsSrcFileName), & m_statBuffer);
		if (m_recursiveOperation == COPY || m_recursiveOperation == MOVE) {
			m_bOkStat = initCopiedFiles(); // make a link
		}
		else
		if (m_recursiveOperation == WEIGH) {
			m_realTotalWeight += m_bOkStat ? ((m_statBuffer.st_size/m_statBuffer.st_blksize)+1)*m_statBuffer.st_blksize : 0;
			m_totalWeight     += m_bOkStat ? m_statBuffer.st_size : 0; // sys.cmd.'du' weigh it as 0
			if (m_bWeighingAsSubCmd)
				emit signalCounterProgress(m_totalWeight, WEIGH_COUNTER, true);
		}
		else
		if (m_recursiveOperation == REMOVE) {
			if (m_bFollowByLink) {
				// what if m_sCurrentFileName will be symlink to directory? then needs recursive weigh
				m_FI.setFile(m_sAbsSrcFileName);
				m_sAbsSrcFileName = m_FI.symLinkTarget();
				m_bOkStat = (::unlink(QFile::encodeName(m_sAbsSrcFileName)) == 0);
			}
			else
				m_bOkStat = (::unlink(QFile::encodeName(m_sAbsSrcFileName)) == 0);
			if (! m_bOkStat) {
				qDebug() << "LocalSubsystem::slotDirProcessingItem. Cannot remove link:" << m_sAbsSrcFileName << "unlink error:" << errno;
				m_sProcessedFileName = m_sAbsSrcFileName;
				m_error = CANNOT_REMOVE;
			}
			else {
				m_OpTotalWeight += m_statBuffer.st_size;
			}
		}
		else
		if (m_recursiveOperation == SET_ATTRIBUTES_CMD) {
			if (m_changeAttributesFor != FileAttributes::DIRS_ONLY) {
				m_eAttribChgStat = setFileOwnerAndGroup(m_sAbsSrcFileName);
				if (m_eAttribChgStat != AttrChgERR) { // effective user ID equals the user ID of the file
					if ((m_eAttribChgStat=setFilePermissions(m_sAbsSrcFileName)) != AttrChgERR)
						m_eAttribChgStat = setFileTime(m_sAbsSrcFileName);
				}
				m_bOkStat = (m_eAttribChgStat != AttrChgERR);
				if (m_error != NO_ERROR)
					m_sProcessedFileName = m_sAbsSrcFileName;
				else
					m_OpTotalWeight += m_statBuffer.st_size;
			}
		}
		else
		if (m_recursiveOperation == FIND) {
			m_nFileCounter++;
			if (m_bFindAll || m_findCriterion.filesType == FindCriterion::LINKS || m_findCriterion.filesType == FindCriterion::FILESandLINKS)
				if (currentFileMatches())
					addMatchedItem(); // and update status of found
		}
	}
	else
	if (S_ISDIR(m_statBuffer.st_mode) && m_bOkStat) {
		m_bWasDir = true;
		if (m_recursiveOperation == COPY || m_recursiveOperation == MOVE) {
			m_bOkStat = (::mkdir(QFile::encodeName(m_sAbsTargetFileName), 0755) == 0);
			if (! m_bOkStat) {
				if (errno != EEXIST) { // file doesn't exist
					qDebug("LocalSubsystem::slotDirProcessingItem. Cannot create directory: '%s', errno=%d", m_sAbsTargetFileName.toLatin1().data(), errno);
					m_error = CANNOT_WRITE;
				}
				else
					m_bOkStat = true;
			}
		}
		else
		if (m_recursiveOperation == WEIGH) {
			m_bOkStat = true;
			m_realTotalWeight += m_bOkStat ? (m_statBuffer.st_blksize/8)*m_statBuffer.st_blocks : 0;  //4096/8==512, sys.cmd.'du' weigh it as 512
// 			m_totalWeight     +=  m_statBuffer.st_size; // for dir weight == 0
		}
		else
		if (m_recursiveOperation == REMOVE) { // if (mStatBuffer.st_size == 48) // if empty dir (true for non Win/DOS.partition)
			m_bOkStat = (::rmdir(QFile::encodeName(m_sAbsSrcFileName)) == 0); // attempt to remove of unknown sizes dir
			if (! m_bOkStat && errno != ENOTEMPTY)
				m_error = CANNOT_REMOVE;
		}
		else
		if (m_recursiveOperation == SET_ATTRIBUTES_CMD) {
			m_bAttribChgSuccess = false;
			if (m_changeAttributesFor != FileAttributes::FILES_ONLY) {
				m_eAttribChgStat = setFileOwnerAndGroup(m_sAbsSrcFileName);
				if (m_eAttribChgStat != AttrChgERR) { // effective user ID equals the user ID of the file
					if (m_eAttribChgStat == AttrChgOK)
						m_bAttribChgSuccess = true;
					if ((m_eAttribChgStat=setFilePermissions(m_sAbsSrcFileName)) != AttrChgERR) {
						if (m_eAttribChgStat == AttrChgOK)
							m_bAttribChgSuccess = true;
						m_eAttribChgStat = setFileTime(m_sAbsSrcFileName);
						if (m_eAttribChgStat == AttrChgOK)
							m_bAttribChgSuccess = true;
					}
				}
				m_bOkStat = (m_eAttribChgStat != AttrChgERR);
			}
			else m_bOkStat = true;
		}
		else
		if (m_recursiveOperation == FIND) {
			m_nDirCounter++;
			if (m_bFindAll || m_findCriterion.filesType == FindCriterion::DIRECTORIES) {
				if (currentFileMatches())
					addMatchedItem(); // and update found's status
			}
		}

		if (m_bOkStat) {
			if (m_recursiveOperation == WEIGH) { // move to case: IS_DIR
				m_nTotalFiles++;
				emit signalCounterProgress(++m_nDirCounter, DIR_COUNTER, true);
			}
			else
			if (m_recursiveOperation != FIND) {
				if ((!(m_recursiveOperation == REMOVE && m_currentCommand == REMOVE) && m_currentOperation != SET_ATTRIBUTES_CMD) || m_currentOperation == REMOVE) {
					m_nOpFileCounter++; // count top dir for remove
					if (m_currentOperation == COPY || m_currentOperation == MOVE) {
						emit signalCounterProgress(++m_nDirCounter, DIR_COUNTER, false);
						emit signalTotalProgress((100 * m_nOpFileCounter) / m_nTotalFiles, m_totalWeight);
						emit signalDataTransferProgress(0, 0, 1, false);
					}
					else
						emit signalCounterProgress((m_recursiveOperation == REMOVE) ? ++m_nDirCounterR : ++m_nDirCounter, DIR_COUNTER);
				}
				if (m_currentOperation == SET_ATTRIBUTES_CMD && m_bAttribChgSuccess) {
					m_nOpFileCounter++; // count top dir for setAttributes
					emit signalCounterProgress((m_recursiveOperation == SET_ATTRIBUTES_CMD) ? ++m_nDirCounterR : ++m_nDirCounter, DIR_COUNTER); // count top dir
				}
				emit signalCounterProgress(m_OpTotalWeight, WEIGH_COUNTER, false);
				if (m_recursiveOperation == REMOVE) {
					if (m_currentCommand != REMOVE) // probably MOVE command
						emit signalTotalProgress((100 * m_nOpFileCounter) / m_nTotalFiles, m_totalWeight);
					m_sAbsSrcFileName = QFile::decodeName(::get_current_dir_name()); // = m_sAbsSrcDirName, zawsze
					slotDirProcessing(); //QTimer::singleShot(0, this, &LocalSubsystem::slotDirProcessing); // after remove, listing must go on (in slotStartProcessDir)
					return;
				}
			}
		}
	}
	else
	if (m_bOkStat) { // it's a file
		if (m_recursiveOperation == COPY || m_recursiveOperation == MOVE) {
			if (! (m_bOkStat=initCopiedFiles(m_statBuffer.st_size))) {
				if (m_nSourceFileSize && ! errno) // becouse when empty file, initCopiedFiles return false
					if (m_status != OPR_STOPPED && m_error != NO_ERROR)
						qDebug("LocalSubsystem::slotDirProcessingItem. error copying or moving. FileName: %s, errno=%d", m_sSourceFileName.toLatin1().data(), errno);
			}
		}
		else
		if (m_recursiveOperation == WEIGH) {
			if (m_statBuffer.st_size > 0) // weigh only not empty file
				m_realTotalWeight += m_bOkStat ? ((m_statBuffer.st_size/m_statBuffer.st_blksize)+1)*m_statBuffer.st_blksize : 0;
			m_totalWeight += m_statBuffer.st_size;
			if (m_bWeighingAsSubCmd)
				emit signalCounterProgress(m_totalWeight, WEIGH_COUNTER, true);
		}
		else
		if (m_recursiveOperation == REMOVE) {
			if (m_bFollowByLink) {
				// what if m_sCurrentFileName will be symlink to directory? then needs recursive weigh
				m_FI.setFile(m_sAbsSrcFileName);
				m_sAbsSrcFileName = m_FI.symLinkTarget();
				m_bOkStat = (::unlink(QFile::encodeName(m_sAbsSrcFileName)) == 0);
			}
			else
				m_bOkStat = (::unlink(QFile::encodeName(m_sAbsSrcFileName)) == 0); // removeFile(m_sAbsSrcFileName);
			if (! m_bOkStat) {
				qDebug() << "LocalSubsystem::slotDirProcessingItem. Cannot remove file:" << m_sAbsSrcFileName << "unlink error:" << errno;
				m_sProcessedFileName = m_sAbsSrcFileName;
				m_error = CANNOT_REMOVE;
			}
			else
				m_OpTotalWeight += m_statBuffer.st_size;
		}
		else
		if (m_recursiveOperation == SET_ATTRIBUTES_CMD) {
			if (m_changeAttributesFor != FileAttributes::DIRS_ONLY) {
				m_eAttribChgStat = setFileOwnerAndGroup(m_sAbsSrcFileName);
				if (m_eAttribChgStat != AttrChgERR) { // effective user ID equals the user ID of the file
					if ((m_eAttribChgStat=setFilePermissions(m_sAbsSrcFileName)) != AttrChgERR)
						m_eAttribChgStat = setFileTime(m_sAbsSrcFileName);
				}
				m_bOkStat = (m_eAttribChgStat != AttrChgERR);
				if (m_error != NO_ERROR)
					m_sProcessedFileName = m_sAbsSrcFileName;
// 				else
// 					m_OpTotalWeight += m_statBuffer.st_size;
			}
		}
		else
		if (m_recursiveOperation == FIND) {
			m_nFileCounter++;
			if (m_bFindAll || m_findCriterion.filesType == FindCriterion::FILES_ONLY || m_findCriterion.filesType == FindCriterion::FILESandLINKS) {
				if (currentFileMatches())
					addMatchedItem(); // and update found's status
			}
		}
	}

// 	if (m_recursiveOperation == FIND && m_bFindAll) {
// 		if (currentFileMatches())
// 			addMatchedItem(); // and update found's status
// 	}
	if (m_recursiveOperation == FIND) {
		if (m_bWasDir)
			m_bWasDir = m_findCriterion.findRecursive;
		else {
			m_sAbsSrcFileName = QFile::decodeName(::get_current_dir_name());
			slotDirProcessing(); //QTimer::singleShot(0, this, &LocalSubsystem::slotDirProcessing);
			return;
		}
	}
	if (m_bWasDir) { // occured dir
		if (m_pDIR != nullptr) {
			m_sAbsSrcDirName    = m_sAbsSrcFileName;
			m_sAbsTargetDirName = m_sAbsTargetFileName;
			m_stackOfDirInode.push(m_statBuffer.st_ino);
			m_stackForFirstInodeOfDir.push((int &)m_stackOfDirInode.first());
			m_nCurrLocInDIR = ::telldir(m_pDIR);
			m_stackForCurrLocInDir.push((off_t *)m_nCurrLocInDIR);
		}
		while (! m_stackOfDIRptrs.isEmpty()) // close all opened dirs
			::closedir(m_stackOfDIRptrs.pop());
		slotStartProcessDir(); //QTimer::singleShot(0, this, &LocalSubsystem::slotStartProcessDir);
		if (m_pDIR == nullptr)
			m_bWasDir = false;
		return;
	}

	if (m_error != NO_ERROR) { // occured an error
		if (m_error == CANNOT_WRITE)
			m_sProcessedFileName = QFileInfo(m_sAbsTargetFileName).absolutePath(); //FileInfoExt::filePath(m_sAbsTargetFName);
		else
			m_sProcessedFileName = m_sAbsSrcFileName;
	}
	if (m_recursiveOperation != COPY && m_recursiveOperation != MOVE)
		currentCommandFinished(); // for each found file in directory (all operation except COPY and MOVE)
}

void LocalSubsystem::slotFileProcessing()
{
	switch(m_currentCommand) {
		case COPY:
		case MOVE:
			copyingFile();
			break;
// 		case VFS::List:
// 			getListedItem();
// 			break;

		default: break;
	}
}


void LocalSubsystem::slotWatcher_DirectoryModified( const QString &sPath )
{
	m_watchStatus = m_lsw->subsystemStatus();
	qDebug() << "LocalSubsystem::slotWatcher_DirectoryModified. inputPath:" << sPath << " status:" << toString(m_watchStatus);
	if (m_watchStatus == WATCHER_REQ_LOCK)
		return;

	setObjectName(sPath);
	Vfs::SubsystemCommand prevCommand = m_currentCommand; // prevent before mess, because this event might be callled in the middle of other operation
	m_currentCommand = WATCHER_INOTIFY;
	qDebug() << "LocalSubsystem::slotWatcher_DirectoryModified. watchedDirs:" << m_lsw->directories();

	m_processedFiles->clear();
	m_slListOfItemsToModify.clear();
	if (m_watchStatus == WATCHER_REQ_ADD || m_watchStatus == WATCHER_REQ_DEL || m_watchStatus == WATCHER_REQ_REN || m_watchStatus == WATCHER_REQ_CHATR) {
		if (m_watchStatus == WATCHER_REQ_REN || m_watchStatus == WATCHER_REQ_CHATR)
			m_slListOfItemsToModify.append(m_lsw->oldFileName());

		QString sFile = sPath;
// 		if (m_watchStatus == WATCHER_REQ_REN)
// 			sFile += m_lsdw->newFileName();
// 		else
			sFile += m_lsw->relatedFileName(); // for WATCHER_REQ_REN this is new file name (so value is the same like in m_lsdw->newFileName())

		FileInfo *pFI;
		QFileInfo fi(sFile);
		if (fi.exists()) {
			QString sPerm = permissionsAsString(sFile);
			QString sMime = MimeType::getFileMime(sFile, fi.isDir(), fi.isSymLink());
			qint64  nSize = fi.isSymLink() ? fi.symLinkTarget().length() : fi.size();
			QString sSymLinkTarget = fi.symLinkTarget();
			pFI = new FileInfo(fi.fileName(), fi.path(), nSize, fi.isDir(), fi.isSymLink(), fi.lastModified(), sPerm, fi.owner(), fi.group(), sMime, sSymLinkTarget);
		}
		else { // fake item for WATCHER_REQ_DEL status
			pFI = new FileInfo(fi.fileName(), sPath, 0, false, false, fi.lastModified(), "", "", "", "");
		}
		m_processedFiles->append(pFI);

		emit signalStatusChanged(m_watchStatus, m_sRequestingPanel_open);
	}
	m_currentCommand = prevCommand;
	m_watchStatus = UNKNOWN_STAT;
	setObjectName("");
	m_sRequestingPanel_open = "";
}

void LocalSubsystem::slotOpenLastAfterBreak()
{
	qDebug() << "LocalSubsystem::slotOpenLastAfterBreak() m_recursiveOperation" << m_recursiveOperation;
// 	open(m_breakOpURI, m_sRequestingPanel_breakOp);
}



} // namespace Vfs
