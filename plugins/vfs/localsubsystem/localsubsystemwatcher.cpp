/***************************************************************************
 *   Copyright (C) 2015 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "localsubsystemwatcher.h"

#include <poll.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>

#include <QDebug>
#include <QTimer>
#include <QFileInfo>


namespace Vfs {

LocalSubsystemWatcher::LocalSubsystemWatcher()
{
	init();
}

LocalSubsystemWatcher::LocalSubsystemWatcher( const QStringList &slPaths )
{
	init();

	if (! m_slPaths.isEmpty())
		addPaths(slPaths);
}


LocalSubsystemWatcher::~LocalSubsystemWatcher()
{
	qDebug() << "LocalSubsystemWatcher::~LocalSubsystemDirWatch.";
	m_pWatcherTimer->stop();
	delete m_pWatcherTimer; m_pWatcherTimer = NULL;
	if (! m_slPaths.isEmpty()) {
		updateWatchedPaths(m_slPaths, REMOVE);
		m_slPaths.clear();
	}
	// -- release resources
	if (m_nInotifyFD != -1) {
		if (::close(m_nInotifyFD) != 0)
			qDebug() << "LocalSubsystemWatcher::~LocalSubsystemDirWatch. ERROR. Cannot release file descriptor:" << m_nInotifyFD;
	}
}


bool LocalSubsystemWatcher::init()
{
	m_eSubsystemStatus = UNKNOWN_STAT;
	m_bLock = false;

	m_pWatcherTimer = new QTimer(this);
	connect(m_pWatcherTimer, &QTimer::timeout, this, &LocalSubsystemWatcher::slotWatchInotifyEvents);

	return true;
}

void LocalSubsystemWatcher::addPaths( const QStringList &slPaths, bool bIfUniqe )
{
	foreach (QString sPath, slPaths) {
		addPath(sPath, bIfUniqe);
	}
}

bool LocalSubsystemWatcher::addPath( const QString &sPath, bool bIfUniqe )
{
	if (! QFileInfo(sPath).exists()) {
		qDebug() << "LocalSubsystemWatcher::addPath. Cannot add path:" << sPath << "because doesn't exist in file system.";
		return false;
	}
	QString sNewPath = sPath;
	if (! sNewPath.endsWith('/'))
		sNewPath += "/";
	qDebug() << "LocalSubsystemWatcher::addPath." << sNewPath;

	if (! bIfUniqe) {
		m_slPaths.append(sNewPath);
		updateWatchedPaths(sNewPath, ADD);
		return true;
	}
	if (! m_slPaths.contains(sNewPath)) {
		m_slPaths.append(sNewPath);
		updateWatchedPaths(sNewPath, ADD);
		return true;
	}

	return false;
}

void LocalSubsystemWatcher::removePaths( const QStringList &slPaths )
{
	foreach (QString sPath, slPaths) {
		removePath(sPath);
	}
}

bool LocalSubsystemWatcher::removePath( const QString &sPath )
{
	if (! m_slPaths.contains(sPath)) {
		qDebug() << "LocalSubsystemWatcher::removePath." << sPath;
		m_slPaths.removeOne(sPath);
		updateWatchedPaths(sPath, REMOVE);
		return true;
	}

	return false;
}


void LocalSubsystemWatcher::updateWatchedPaths( const QStringList &slPaths, UpdatePathOperation eOperation )
{
	foreach (QString sPath, slPaths) {
		updateWatchedPaths(sPath, eOperation);
	}
}

bool LocalSubsystemWatcher::updateWatchedPaths( const QString &sPath, UpdatePathOperation eOperation )
{
	QString sFN = "LocalSubsystemWatcher::updateWatchedPaths";
	int wd;

	if (eOperation == ADD) {
		wd = ::inotify_add_watch(m_nInotifyFD, sPath.toLatin1(), IN_CREATE | IN_DELETE | IN_ATTRIB | IN_DELETE_SELF | IN_MOVE_SELF | IN_MOVED_FROM | IN_MOVED_TO); // IN_ALL_EVENTS
		m_mapPathToWD.insert(sPath, wd);
		qDebug() << sFN << "-- created new watch instance with watch descriptor:" << wd << "and associated with path:" << sPath;
		if (m_slPaths.size() == 1 && ! m_pWatcherTimer->isActive()) {
			m_pWatcherTimer->start(1000); // emits signal every 1 second
			qDebug() << sFN << "-- watching started.";
		}
	}
	else
	if (eOperation == REMOVE) {
		wd = m_mapPathToWD[sPath];
		::inotify_rm_watch(m_nInotifyFD, wd);
		if (m_mapPathToWD.size() > 0)
			m_mapPathToWD.remove(sPath);
		qDebug() << sFN << "-- removed watch descriptor:" << wd << "associated with path:" << sPath;
		if (m_slPaths.size() == 0)
			m_pWatcherTimer->stop();
	}

	return true;
}


#define BUF_LEN (10 * (sizeof(struct inotify_event) + NAME_MAX + 1))

void LocalSubsystemWatcher::slotWatchInotifyEvents()
{ // main code come from example of Linux Programming Interface book (demo_inotify.c), non blocking read from stackoverflow.com/questions/4664975
	QString sFN = "LocalSubsystemWatcher::slotWatchInotifyEvents.";
// 	qDebug() << sFN << "Processing events is locked:" << m_bLock;

	int poolRet, inotifyFD = m_nInotifyFD;
	char buf[BUF_LEN] __attribute__ ((aligned(8)));
	ssize_t numRead;
	char *pBuf;
	struct inotify_event *event;
	struct pollfd pfd = { inotifyFD, POLLIN, 0 };

	poolRet = poll(&pfd, 1, 50);  // timeout of 50ms
	if (poolRet < 0) {
		qDebug() << sFN << "poll failed with error:" << errno;
	} else if (poolRet == 0) {
		;// Timeout with no events, move on.
	} else {
		// Process the new event.
// 		qDebug() << sFN << "start read";
		numRead = ::read(inotifyFD, buf, BUF_LEN);
		if (numRead == 0) {
			qDebug() << sFN << "nothing read from file descriptor:" << inotifyFD;
			return;
		}
		if (numRead == -1) {
			qDebug() << sFN << "read error for file descriptor:" << inotifyFD;
			return;
		}
// 		qDebug() << sFN << "Read:" << numRead << "bytes from inotify with file descriptor:" << inotifyFD;
		if (numRead > 0) {
			// Process all of the events in buffer returned by read()
			for (pBuf = buf; pBuf < buf + numRead; ) {
				event = (struct inotify_event *) pBuf;
				manageInotifyEvent(event);
				pBuf += sizeof(struct inotify_event) + event->len;
			}
		}
	}
}

void LocalSubsystemWatcher::manageInotifyEvent( struct inotify_event *ie )
{
	QString sFN = "LocalSubsystemWatcher::manageInotifyEvent.";
	QString sPath = m_mapPathToWD.key(ie->wd);

	m_eSubsystemStatus = UNKNOWN_STAT;
	bool bRename = (ie->mask & IN_MOVED_FROM || ie->mask & IN_MOVED_TO);
	if (! bRename) {
		m_sOldFileName = "";
		m_sNewFileName = "";
	}
	m_sRelatedFileName = (ie->len > 0) ? ie->name : "";

// 	if (ie->mask & IN_ACCESS)        ;
// 	if (ie->mask & IN_CLOSE_WRITE)   ;
	if (ie->mask & IN_ATTRIB) {
		m_eSubsystemStatus = WATCHER_REQ_CHATR;
		m_sOldFileName = m_sRelatedFileName;
	}
	else
	if (ie->mask & IN_CREATE) {
		m_eSubsystemStatus = WATCHER_REQ_ADD;
	}
	else
	if (ie->mask & IN_DELETE) {
		m_eSubsystemStatus = WATCHER_REQ_DEL;
	}
	if (ie->mask & IN_DELETE_SELF || ie->mask & IN_MOVE_SELF) {
		::inotify_rm_watch(m_nInotifyFD, ie->wd);
		if (m_mapPathToWD.size() > 0)
			m_mapPathToWD.remove(sPath);
		qDebug() << sFN << "Watched path: "<< sPath << "has been removed!";
		qDebug() << sFN << "-- removed watch descriptor:" << ie->wd << "associated with path:" << sPath;
	}
	if (ie->mask & IN_MOVED_FROM) {
		m_sOldFileName = m_sRelatedFileName;
		m_eSubsystemStatus = WATCHER_REQ_DEL;
	}
	if (ie->mask & IN_MOVED_TO) {
		m_sNewFileName = m_sRelatedFileName;
		m_eSubsystemStatus = WATCHER_REQ_ADD;
		// Cannot handle rename because some external applications (i,e.: mc) invoke events: IN_MOVED_FROM and IN_MOVED_TO when user move file(s)
// 		if (! m_sOldFileName.isEmpty())
// 			m_eSubsystemStatus = NEED_REFRESH_REN;
	}
// 	if (ie->mask & IN_MODIFY)        ;
// 	if (ie->mask & IN_OPEN)          ;
// 	if (ie->mask & (IN_OPEN | IN_ISDIR)) ;
// 	if (ie->mask & IN_Q_OVERFLOW)    ;
// 	if (ie->mask & IN_UNMOUNT)       ;

	if (m_eSubsystemStatus != UNKNOWN_STAT && m_eSubsystemStatus != WATCHER_REQ_LOCK) {
		emit directoryChanged(sPath);
//  		qDebug() << sFN << "emit directoryChanged. REQ:" << Vfs::toString(m_eSubsystemStatus) << "RelatedFileName" << m_sRelatedFileName << "for Path" << sPath;
	}
}

void LocalSubsystemWatcher::setLock( bool bLock )
{
	qDebug() << "LocalSubsystemWatcher::setLock. Locked:" << bLock;
	m_bLock = bLock;
	int wd;

	if (bLock) {
		m_eSubsystemStatus = WATCHER_REQ_LOCK;
		m_pWatcherTimer->stop();
		// -- clear watching paths
		foreach (QString sPath, m_slPaths) {
			wd = m_mapPathToWD[sPath];
			::inotify_rm_watch(m_nInotifyFD, wd);
		}
		m_mapPathToWD.clear();
		// -- release resources
		if (m_nInotifyFD != -1) {
			if (::close(m_nInotifyFD) != 0)
				qDebug() << "LocalSubsystemWatcher::setLock. ERROR. Cannot release file descriptor:" << m_nInotifyFD;
		}
	}
	else { // unlock and start watching
		m_nInotifyFD = ::inotify_init();
		if (m_nInotifyFD < 0) {
			qDebug() << "LocalSubsystemWatcher::setLock. FATAL ERROR. Cannot create file descriptor.";
			return;
		}
		// -- update watcher with saved paths
		foreach (QString sPath, m_slPaths) {
			wd = ::inotify_add_watch(m_nInotifyFD, sPath.toLatin1(), IN_CREATE | IN_DELETE | IN_ATTRIB | IN_DELETE_SELF | IN_MOVE_SELF | IN_MOVED_FROM | IN_MOVED_TO); // IN_ALL_EVENTS
			m_mapPathToWD.insert(sPath, wd);
		}
		m_eSubsystemStatus = UNKNOWN_STAT;
 		m_pWatcherTimer->start(1000); // emits signal every 1 second
	}
}


// --- DEBUG functions ---

QString LocalSubsystemWatcher::toString( UpdatePathOperation eOperation )
{
	QString sOperation = "?";
	if      (eOperation == ADD)  sOperation = "ADD";
	else if (eOperation == REMOVE)  sOperation = "REMOVE";

	return sOperation;
}


} // namespace Vfs
