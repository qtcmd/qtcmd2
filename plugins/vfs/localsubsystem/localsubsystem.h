/***************************************************************************
 *   Copyright (C) 2005 by Mariusz Borowski <b0mar@nes.pl>                 *
 *   Copyright (C) 2009 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef LOCALSUBSYSTEM_H
#define LOCALSUBSYSTEM_H

#include <QDir>
#include <QStack>
#include <QTimer>
#include <QDebug>

#include "subsystem.h" // includes also: QFile QVector QDateTime QStringList QUrlInfo
#include "plugindata.h"
#include "contextmenu.h" // includes also: KeyShortcuts
#include "findcriterion.h"
#include "localsubsystemwatcher.h"

// #include <linux/types.h>
// #include <linux/stat.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <utime.h>

namespace Vfs {

/**
	@author Mariusz Borowski <b0mar@nes.pl>
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class LocalSubsystem : public Subsystem
{
	Q_OBJECT
public:
	LocalSubsystem();
	virtual ~LocalSubsystem();

	SubsystemError   error() const { return m_error; }
	SubsystemStatus  status() const { return m_status; };
	SubsystemCommand currentCommand() const { return m_currentCommand; }

	void init ( QWidget *pParent, bool );

	FileInfo     *root( bool bEmptyName=false );
	FileInfoList  listDirectory() const;

	/** Returns 'quit subsystem by performing cd up on top level' status.
	 * @return TRUE if can get out, otherwise FALSE.
	 */
	bool          canUpThanRootDir() const { return false; }

	/** Returns change directory status.
	 * @param sInpAbsPath absolute path to directory
	 * @return TRUE if directory can change, otherwise FALSE.
	 */
	bool          canChangeDirTo( const QString &sInpAbsPath );

	/** Checks if give file is readable and returns status.
	 * @param pFileInfo FileInfo pointer to file
	 * @return TRUE if file is readable, otherwise FALSE.
	 */
	bool          canOpenFileForReading( FileInfo *pFileInfo );

	/** Check path from given URI and set current path.
	 * @param uri uri to open
	 */
	void          open( const URI &uri, const QString &sRequestingPanel, bool bCompleter=false );
	void          close( bool bForce ) { qDebug() << "LocalSubsystem::close()"; } // disconnecting not supported

	void          openWith( FileInfoToRowMap *selectionMap, const URI &uri, const QString &sRequestingPanel );
	void          readFileToViewer( const QString &sHost, FileInfo *pFileInfo, QByteArray &baBuffer, int &nBytesRead, int nReadingMode, const QString &sRequestingPanel );

	void          rename( FileInfo *fileInfoOldName, const QString &sAbsNewName, const QString &sRequestingPanel );

	/** Make dir(s).
	 * @param sParentDirPath path to parent directory
	 * @param sInNewDirsList directory name or list of directries separated by |
	 */
	void          makeDir( const QString &sParentDirPath, const QString &sInNewDirsList, const QString &sRequestingPanel );
	void          makeFile( const QString &sParentDirPath, const QString &sInFileNameList, const QString &sRequestingPanel );
	void          makeLink( FileInfoToRowMap *pSelectionMap, const QString &sTargetLinkName, bool bHardLink, const QString &sRequestingPanel );
	void          editSymLink( FileInfo *pFileInfo, const QString &sSymLinkNewTarget, const QString &sRequestingPanel );

	void          weigh( FileInfoToRowMap *selectionMap, const URI &uri, const QString &sRequestingPanel, bool bWeighingAsSubCmd=true, bool bResetCounters=true );
	void          weighItem( FileInfo *fileInfo, const URI &uri, const QString &sRequestingPanel, bool bResetCounters=true );
	void          getWeighingResult( qint64 &weight, qint64 &realWeight, uint &files, uint &dirs );

	/** Performs removing operation.
	 * @param pSelectionMap selected items to remove
	 * @param uri reference to current opened location (in panel) - usable only in archive's subsystems
	 * @param bWeighBefore true made (recursive) weighing selected items, otherwise weighing is not performed
	 * @param sRequestingPanel name of SubsystemManager object for current panel
	 * @param bForceRemoveAll if TRUE then directories will be remove without confirmation
	 */
	void          remove( FileInfoToRowMap *pSelectionMap, const URI &uri, bool bWeighBefore, const QString &sRequestingPanel, bool bForceRemoveAll=false );

	/** Performs setting attributes operation.
	 * @param pSelectionMap selected items to attributes change
	 * @param uri reference to current opened location (in panel) - usable only in archive's subsystems
	 * @param pChangedAttrib pointer to the object containing values of changed attributes
	 * @param bWeighBefore true made (recursive) weighing selected items, otherwise weighing is not performed
	 * @param sRequestingPanel name of SubsystemManager object for current panel
	 */
	void          setAttributes( FileInfoToRowMap *pSelectionMap, const URI &uri, FileAttributes *pChangedAttrib, bool bWeighBefore, const QString &sRequestingPanel );

	void          searchFiles( FileInfoToRowMap *pSelectionMap, const URI &uri, const FindCriterion &fcFindCriterion, const QString &sRequestingPanel );
	QString       searchingResult();

	/** Invokes copying or moving of files from given list.
	 * @param pSelectionMap selected items to process
	 * @param sTargetPath target path
	 * @param bMove if TRUE then invoke moving operation otherwise invoke copying
	 * @param bWeighBefore if TRUE then invoke weighing operation before start this operation
	 * @param sRequestingPanel name of SubsystemManager object for current panel
	 */
	void          copyFiles( FileInfoToRowMap *pSelectionMap, const QString &sTargetPath, bool bMove, bool bWeighBefore, const QString &sRequestingPanel );

	URI           currentURI() const { return m_bCompleterMode ? m_currentUriCompleter : m_currentUri; }

	FileInfo     *fileInfo( const QString &sAbsFileName );

	SubsystemError isExecutableFile( const QString &sAbsFileName ) const;
	void          executeApplication( const QString &sAbsFileName );

	PluginData   *pluginData() { return m_pluginData; }

	FileInfoList *processedFiles() { return m_processedFiles; }
	FileInfoList *postMovedFiles() { return m_postMovedFiles; }
	QString       processedFileName() const { return m_sProcessedFileName; }
	FileInfo     *processedFileInfo() const { return m_processedFileInfo; }
	bool          operationFinished() const { return m_operationFinished; }

	void          updateContextMenu( ContextMenu *pContextMenu, KeyShortcuts *pKS=nullptr );

	WritableStatus dirWritableStatus();

//	bool          theSameDirInBothPanels() const { return m_theSameDir; }
	bool          theSameDirInBothPanels() const { return ((m_sLeftPanelPath == m_sRightPanelPath) && m_bInitedLeftAndRight); }

	/** Returns reference to the list of values before are changed (i.e.: before rename)
	 * @return string list
	 */
	QStringList & listOfItemsToModify() { return m_slListOfItemsToModify; }

	void          setWorkingDirectoryPath( const QString &sDirectoryPath ) {}

	void          setBreakOperation();
	void          setPauseOperation( bool bPause );

	bool          handledOperation( SubsystemCommand sc ) { return true; }

private:
	SubsystemError      m_error;
	SubsystemStatus     m_status, m_watchStatus;
	SubsystemStatus     m_pausedStatus;
	SubsystemCommand    m_currentCommand;
	SubsystemCommand    m_currentOperation, m_recursiveOperation;

	ContextMenu        *m_pContextMenu;
	QWidget            *m_pParent;

	URI                 m_currentUri, m_breakOpURI, m_currentUriCompleter;

	PluginData         *m_pluginData;

	LocalSubsystemWatcher *m_lsw;
	QMap<QString,QString> m_watchedDirsMap;

	QString             m_sRequestingPanel_open;
	bool                m_operationFinished;

	FileInfo           *m_processedFileInfo;
	QString             m_sProcessedFileName;
	FileInfoList       *m_processedFiles;
	FileInfoList       *m_postMovedFiles;

	QString             m_sLeftPanelPath, m_sRightPanelPath;
	QStringList         m_slListOfItemsToModify;
	bool                m_bInitedLeftAndRight;
	bool                m_theSameDir;


	QVector < Operation > m_opQueue;
	QString m_sRequestingPanel;

	QString m_sCurrentPath;

	QFileInfo m_fInfo, m_fInfo2, m_FI;
	QDir m_dir;

	QTimer m_operationTimer;

	uint m_nListingFilesCounter;
	uint m_nOpFileCounter, m_nErrorNumber;
	bool m_bPauseOperation, m_bBreakOperation, m_bInitOperation, m_bSkipCurrentFile;

	char *m_pFileBuffer;
	int   m_nFileBufferSize;
	bool  m_bFollowByLink, m_bSaveAttributes, m_bAlwaysOverwrite, m_bHardLink, m_bNoneOverwriting;
	long long m_nSourceFileSize, m_nWritedBytes;
	QString m_sSourceFileName, m_sTargetFileName;
	QFile m_fileIn, m_fileOut;

	bool m_bWeightBefore;
	long long m_totalWeight, m_OpTotalWeight, m_realTotalWeight;
	uint m_nDirCounter,  m_nFileCounter, m_nSymlinksCounter, m_nTotalFiles;
	uint m_nDirCounterR;//, m_nFileCounterR;

	bool m_bRemoveToTrash, m_bRecursivelyRemoveAllDirs, m_bNoneRecursivelyRemoveDir, m_bForceRemoveAll;
	QString m_sTrashPath;

	bool m_bTreeView, m_bGetFileLinkSize, m_bAsynchronouslyListing;

	DIR *m_pDIR;
	off_t m_nCurrLocInDIR, m_nCurrLocInDIR_prev; //, m_nCurrLocInDIR_next;
	struct stat64 m_statBuffer;
	struct dirent *m_pDirNextEntry;
	QStack <DIR *> m_stackOfDIRptrs;
	QStack <int  > m_stackForFirstInodeOfDir; // QStack <ino_t >  m_stackForFirstInodeOfDir; // Semantic analysis (KDevelop4) can't understand it and underline as error (using in code)
	QStack <int  > m_stackOfDirInode;         // QStack <ino_t >  m_stackOfDirInode;
	QStack <int  > m_stackForThrowedOffInode; // QStack <ino_t >  m_stackForThrowedOffInode;
	QStack <off_t *> m_stackForCurrLocInDir;
	bool m_bWasDir, m_bNeedToSeek, m_bOkStat;
	int  m_nWasDots;
	QString m_sAbsSrcFileName, m_sAbsTargetFileName;
	QString m_sAbsSrcDirName,  m_sAbsTargetDirName;
	QString m_sCurrentFileName;

	bool m_bChangeRecursivelyAttributes;
	int  m_nCurrentUserID;
	int  m_nNewOwnerId, m_nNewGroupId;
	QString m_sCurrentUserName;
	FileAttributes::ChangesFor m_changeAttributesFor;
	utimbuf m_uTimeBuf;
	FileAttributes *m_pNewAttributes;

	enum UpdatingType { NONEupd=0, DATEupd, SIZEupd };
	UpdatingType m_eUpdatingFilesWith;

	enum AttribChgStat { AttrChgSKIP=0, AttrChgOK, AttrChgERR };
	AttribChgStat m_eAttribChgStat;
	bool m_bAttribChgSuccess; // at least one attrib.changed

	FindCriterion m_findCriterion;
	int m_nMatchedFiles;
	bool m_bFindAll;
	QList <QRegExp> m_findRegExpLst;
	bool m_bFindRecursively;
	QString m_sSearchingResult; // matchedFiles,fileCounter,dirCounter
	QString m_sCopyTargetPath, m_renameInCopying;

	bool m_bWeighingAsSubCmd;
	bool m_bShowMsgCannotDelete, m_bShowMsgCannotSetAttributes, m_bShowMsgCannotCheckFile, m_bShowMsgCannotCreateLink, m_bShowMsgCannotWeigh;

	bool m_bFileHasBeenOpened;
	QFile m_FileViewer;

	bool m_bCompleterMode;
	int  m_nSelectedFiles, m_nSkippedFiles;

	void clearStacks();
	void clearFilesCounter();
	void addOperations2queue( FileInfoToRowMap *pSelectionMap, SubsystemCommand subsystemCmd, const QString &sTargetPath, const QString &sRequestingPanel );

	void jobFinished();
	void currentCommandFinished();
	bool recursiveOperation( SubsystemCommand recursiveOperation, const QString &sSourceDirName, const QString &sTargetDirName="" );

	bool isDirectoryEmpty( const QString &sDirName );

	void initWeighing( bool bResetCounters, const QString &sRequestingPanel );
	void weighing2queue( FileInfoToRowMap *selectionMap, const URI &, const QString &sRequestingPanel, bool bWeighingAsSubCmd=true, bool bResetCounters=true );
	bool weighNextItem( FileInfo *pFileInfo );

	void initCopying();
	bool initCopiedFiles( long long fileSize=-1 );
	bool initGetFile( long long fileSize=-1 );
	bool initPutFile();
	bool initMakeLink();
	void copyingFile();
	void putFileFinished();
	void getFileFinished();

	void initRemoving();
	bool removeNextFile();
	void removeDir( const QString &sDirName );
	void removeFile( const QString &sFileName, bool bSilentMode=false );

	void initSettingAttributes();
	bool setAttributesNextFile();
	AttribChgStat setFilePermissions( const QString &sInFileName, int inPermissions=-1 );
	AttribChgStat setFileTime( const QString &sInFileName, int inSecondsToAccess=-1, int inSecondsToModification=-1 );
	AttribChgStat setFileOwnerAndGroup( const QString &sFileName, const QString &sOwner, const QString &sGroup );
	AttribChgStat setFileOwnerAndGroup( const QString &sInFileName, int inOwnerId=-1, int inGroupId=-1 );

	void initSearchingFiles();
	bool currentFileMatches();
	void addMatchedItem();

	bool makeNextLink( const QString &sSymLinkTarget, const QString &sNewSymLink, bool checkErrors=false );

	int  showMsgDirNotEmptyForRemoving( const QString &sDirName );
	int  showMsgCannotDo( FileInfo *pFI=NULL );

	QString tildeParsing( const QString &sInpPath );

private slots:
	void slotWatcher_DirectoryModified( const QString &sPath );
	void slotStartNextJob();
	void slotStartProcessDir();
	void slotDirProcessing();
	void slotDirProcessingItem();
	void slotFileProcessing();
	void slotOpenLastAfterBreak();

};


} // namespace Vfs

#endif
