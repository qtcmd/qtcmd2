/***************************************************************************
 *   Copyright (C) 2011 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QUrl>
#include <QDir>
#include <QTimer>
#include <QDebug>
#include <QFileInfo>

#include <sys/stat.h>
#include <utime.h>
#include <errno.h>

//#include "ftpsubsystemsettings.h"
#include "systeminfohelper.h"
#include "preview/viewer.h"
#include "ftpsubsystem.h"
#include "messagebox.h"
#include "settings.h"
#include "mimetype.h"


namespace Vfs {

FtpSubsystem::FtpSubsystem()
	: m_bLOCAL_PROGRESS(false), m_bTOTAL_PROGRESS(true)
{
	qDebug() << "FtpSubsystem::FtpSubsystem.";
	//setObjectName("FtpSubsystem");
	m_error  = NO_ERROR;
	m_status = UNCONNECTED;
	m_currentCommand = NO_COMMAND;
	m_bOperationFinished = false;

	//FtpSubsystemSettings *ftpSubsystemSettings = new FtpSubsystemSettings;
	m_processedFiles = new FileInfoList;
	m_preProcessedFiles = new FileInfoList;

	m_pPluginData = new PluginData;
	m_pPluginData->name         = "ftpsubsystem_plugin";
	m_pPluginData->version      = "0.1.1";
	m_pPluginData->description  = QObject::tr("Plugin handles FTP connection.");
	m_pPluginData->plugin_class = "subsystem";
	m_pPluginData->mime_icon    = ""; // (used in configuration window, higher priority than own_icon)
	m_pPluginData->own_icon     = 0;  // QIcon pointer. (used in configuration window)
	m_pPluginData->mime_type    = "remote/ftp";
	m_pPluginData->protocol     = "ftp";
	m_pPluginData->finding_files_fun        = 0; // ptr.to fun
	m_pPluginData->finding_inside_files_fun = 0; // ptr.to fun
// 	m_pluginData->popup_actions   = 0; // QActionGroup pointer  (ftpSubsystemSettings->popupActions())
	m_pPluginData->toolBar      = 0; // QToolBar pointer     (ftpSubsystemSettings->toolBar())
	m_pPluginData->configPage   = 0; // QWidget pointer      (ftpSubsystemSettings->configPage())

	createQFtpObj();

	m_bReadFile  = false;
	m_bLinkChecking = false;
	m_bFileIsReadable = false;
	m_preRemovedCounter = 0;
	m_nOpenWithCounter = 0;
	m_bConfirmDownloadForOpenWith = true;
	m_bConfirmDownloadForView = true;
	m_bOperationCanceled = false;
	m_totalWeight = 0;
	m_totalFiles = 0;
	m_totalDirs = 0;
	m_pUploadFile = NULL;
}

FtpSubsystem::~FtpSubsystem()
{
	qDebug() << "FtpSubsystem::~FtpSubsystem. DESTRUCTOR";
	close();

// 	delete m_pPluginData; // removed in PluginFactory::~PluginFactory
	delete m_pUploadFile;
	delete m_pFtp;
	delete m_processedFiles;
	delete m_preProcessedFiles;
	m_fiFtpListedItems.clear();
	m_listedlinksList.clear();
}


void FtpSubsystem::init( QWidget *pParent, bool bInvokeDetection )
{
	m_currentCommand = NO_COMMAND;
	m_pParent = pParent;

	m_currentUri.reset();
	qDebug() << "FtpSubsystem::init.";

	Settings settings;
	if (settings.value("ftp/ShowConnDlgIfEmptyPassword").isNull())
		settings.setValue("ftp/ShowConnDlgIfEmptyPassword", true); // default value
}

/*
void FtpSubsystem::notSupportedOp( SubsystemCommand currentCommand, const QString &sRequestingPanel )
{
	m_currentCommand = currentCommand;
	m_status = ERROR_OCCURED;  m_error = NOT_SUPPORTED;
	emit signalStatusChanged(m_status, sRequestingPanel);
}
*/
FileInfoList FtpSubsystem::listDirectory() const
{
	qDebug() << "FtpSubsystem::listDirectory. items amount:" << m_fiFtpListedItems.count() << "path:" << m_currentUri.path();

	//FileInfoList fiList;
	//fiList.append(root());
	//return fiList;

	return m_fiFtpListedItems;
}

FileInfo *FtpSubsystem::root( bool bEmptyName )
{
	static QString sFakeRoot = "/";
	QFileInfo fi(sFakeRoot);

	return new FileInfo(bEmptyName ? "" : "..", sFakeRoot, fi.size(), true, false, fi.lastModified(), "----------", fi.owner(), fi.group(), "folder/up");
}

FileInfo *FtpSubsystem::fileInfo( const QString &sInAbsFileName )
{
// 	qDebug() << "FtpSubsystem::fileInfo. sAbsFileDirName" << sAbsFileName;
	if (sInAbsFileName.endsWith("/.."))
		return root();
	if (sInAbsFileName == "/")
		return root(true); // empty name

	FileInfo *pRetFI = NULL;
	QString sFindName;
	if (sInAbsFileName.startsWith('/') || sInAbsFileName.contains("://"))
		sFindName = sInAbsFileName.right(sInAbsFileName.length() - sInAbsFileName.lastIndexOf('/') - 1);
	else
		sFindName = sInAbsFileName;

	for (int i=0; i<m_fiFtpListedItems.size(); i++) {
		if (m_fiFtpListedItems.at(i)->fileName() == sFindName) {
			pRetFI = m_fiFtpListedItems.at(i);
			break;
		}
	}

	return pRetFI;
}

bool FtpSubsystem::canChangeDirTo( const QString &sInAbsPath )
{
	QString sFN = "FtpSubsystem::canChangeToDir.";
	qDebug() << sFN << "AbsPath:" << sInAbsPath;
	if (sInAbsPath.isEmpty()) {
		qDebug() << sFN << "Empty input path!";
		return false;
	}

	FileInfo *pFI = fileInfo(sInAbsPath);
	if (pFI != NULL) {
		bool    bIsDir = pFI->isDir();
		QString sName  = pFI->fileName();
		QString sPermission = pFI->permissions();
		bool    bIsReadable = (sPermission[1] == 'r' && sPermission[4] == 'r' && sPermission[7] == 'r');
		//qDebug() << "--- name:" << sName << ", isDir:" << bIsDir << ", permission:" << sPermission << ", isReadable:" << bIsReadable;

		if (bIsDir) {
			if (bIsReadable || sName == "..")
				return true;
		}
	}
	else {
		qDebug() << sFN << "WARNING. Cannot find FileInfo object for passed path.";
		return true;
	}

	return false;
}

bool FtpSubsystem::canOpenFileForReading( FileInfo *pFileInfo )
{
	QString sFN = "FtpSubsystem::close.";
	qDebug() << sFN << "InpFileName:" << pFileInfo->fileName();
	if (pFileInfo == NULL) {
		qDebug() << sFN << "Empty input file!";
		return false;
	}
	m_error = NO_ERROR;
	m_sNameOfProcessedFile = pFileInfo->absoluteFileName();
	QString sPermission = pFileInfo->permissions();
	if (pFileInfo->owner() == m_currentUri.user()) {
		if (sPermission[1] != 'r') { // checking for owner
			if (sPermission[7] != 'r') { // checking for other
				m_error  = CANNOT_READ;
				m_status = ERROR_OCCURED;
				return false;
			}
		}
	}
	else { // logged user is not owner of checked file
		if (sPermission[7] != 'r') { // checking for other
			m_error  = CANNOT_READ;
			m_status = ERROR_OCCURED;
			return false;
		}
	}
// 	qDebug() << sFN << "File is readable:";

	return true;
}

void FtpSubsystem::close( bool bForce )
{
	QString sFN = "FtpSubsystem::close.";
	qDebug() << sFN << "Force:" << bForce;
	if (m_pFtp->state() == QFtp::Connected || m_pFtp->state() == QFtp::LoggedIn || bForce) {
		qDebug() << sFN << "Disconnecting and cleaning commands.";
		m_currentUri.reset();
		m_cmdQueue.clear();
		m_currentCommand = NO_COMMAND;
		m_status = CLOSING;
		m_pFtp->clearPendingCommands();
		m_pFtp->abort();
		m_pFtp->close();
// 		m_pFtp->deleteLater();

		if (bForce) {
			// WORKAROUND: after break connecting operation, again calling connect made hungs QFtp (maybe timeout is the reason) on ConnectToHost command (close command didn't break connect)
// 			m_ftp->rawCommand("QUIT"); // (id: 3, 6) // doesn't work
			qDebug() << sFN << "Recreate QFtp object.";
			delete m_pFtp;
			createQFtpObj();
		}
		//emit signalStatusChanged(m_status, m_sRequestingPanel);
	}
	//cleanTemporaryFiles(); // remove temporary downloaded files (used by view support)
}

void FtpSubsystem::createQFtpObj()
{
	m_pFtp = new QFtp;
	connect<void(QFtp::*)(int)>(m_pFtp, &QFtp::commandStarted, this, &FtpSubsystem::slotCommandStarted); // signal is overloaded
	connect<void(QFtp::*)(int, bool)>(m_pFtp, &QFtp::commandFinished, this, &FtpSubsystem::slotCommandFinished); // signal is overloaded
	connect<void(QFtp::*)(const QUrlInfo &)>(m_pFtp, &QFtp::listInfo, this, &FtpSubsystem::slotListInfo); // signal is overloaded
	connect<void(QFtp::*)(qint64 done, qint64)>(m_pFtp, &QFtp::dataTransferProgress, this, &FtpSubsystem::slotDataTransferProgress); // signal is overloaded
	connect<void(QFtp::*)(int, const QString &)>(m_pFtp, &QFtp::rawCommandReply, this, &FtpSubsystem::slotRawCommandReply); // signal is overloaded
	connect<void(QFtp::*)(int)>(m_pFtp, &QFtp::stateChanged, this, &FtpSubsystem::slotStateChanged); // signal is overloaded
}

void FtpSubsystem::open( const URI &inUri, const QString &sRequestingPanel, bool bCompleter )
{
	QString sFN = "FtpSubsystem::open";
	m_sRequestingPanel = sRequestingPanel;
	if (! inUri.isValid()) {
		qDebug() << sFN << "Invalid uri!";
		return;
	}
// 	QFtp::State ftpState = m_pFtp->state();
// 	qDebug() << sFN << "State:" << qFtpToString(ftpState);
	m_bOperationCanceled = false;
	qDebug() << sFN << "LoggedIn:" << (m_pFtp->state() == QFtp::LoggedIn) << "to host:" << inUri.host() << "path:" << inUri.path() << "user:" << inUri.user() << "pass: ***"/**<< uri.password() **/<< "port:" << inUri.port();
	bool bCommNewUri = false;
	bool bNoEmptyUriMemb = (! inUri.protocol().isEmpty() && ! inUri.host().isEmpty() && ! inUri.user().isEmpty() && ! inUri.password().isEmpty());
	if (((! m_currentUri.host().isEmpty()    && m_currentUri.host() != inUri.host()) ||
		(! m_currentUri.user().isEmpty()     && m_currentUri.user() != inUri.user()) ||
		(! m_currentUri.password().isEmpty() && m_currentUri.password() != inUri.password()) ||
		(! m_currentUri.protocol().isEmpty() && m_currentUri.protocol() != inUri.protocol()) ||
		(m_currentUri.port() != inUri.port()))
		&& ! bNoEmptyUriMemb)
	{
		m_currentCommand = NO_COMMAND;
		bCommNewUri = true; // TRUE also when URI is full, so user and password are passed (then no need to show ConnectionDialog)
	}
	bool bTheSameUri = false;
	if (inUri == m_currentUri)
		bTheSameUri = true;

	m_currentUri = inUri;

	if (m_pFtp->state() != QFtp::LoggedIn || m_currentCommand == NO_COMMAND) {
		m_currentCommand = CONNECT;
		if (bCommNewUri) {
			bool bRetStat = false; // set in slot connected with below signal
			emit signalShowConnectionDlg(m_currentUri, m_currentUri.protocol(), "anonymous", bRetStat);
			if (! bRetStat) { // user canceled initialization of connection
				qDebug() << sFN << "--- connection canceled.";
				m_currentUri.reset();
				m_error  = CANCELED;
				m_status = ERROR_OCCURED;
				m_currentCommand = NO_COMMAND;
				emit signalStatusChanged(m_status, sRequestingPanel);
				return;
			}
		}
		emit signalStatusChanged(CONNECTING, sRequestingPanel);
		qDebug() << sFN << "host:" << m_currentUri.host() << "path:" << m_currentUri.path() << "user:" << m_currentUri.user() << "pass: ?"/* << m_currentUri.password() */<< "port:" << m_currentUri.port();
		m_currentCommand = OPEN;
		if (inUri == m_currentUri && bTheSameUri && (m_pFtp->state() == QFtp::LoggedIn || m_pFtp->state() == QFtp::Connected)) {
			qDebug() << sFN << "Incomming Uri is exactly the same like current.";
			return;
		}
		if (bCommNewUri && (m_pFtp->state() == QFtp::LoggedIn || m_pFtp->state() == QFtp::Connected)) { // if user set new host then disconnect previous connection
			qDebug() << sFN << "URI (excluding path) changed. Need to reconnect.";
			m_cmdQueue << DISCONNECT << CONNECT << LOGIN_CMD << CD << LIST;
			slotRunNextCommand();
			return;
		}
	}
	if (m_pFtp->state() != QFtp::Connected && m_pFtp->state() != QFtp::LoggedIn)
		m_cmdQueue << CONNECT << LOGIN_CMD << CD << LIST;
	else
	if (m_pFtp->state() == QFtp::LoggedIn)
		m_cmdQueue << CD << LIST;
	else
		m_cmdQueue << LOGIN_CMD << CD << LIST;

	slotRunNextCommand();
}

void FtpSubsystem::rename( FileInfo *pFileInfoOldName, const QString &sInNewName, const QString &sRequestingPanel )
{
	QString sInOldName = pFileInfoOldName->fileName();
	qDebug() << "FtpSubsystem::rename. m_currentUri:" << m_currentUri.toString() << "InOldName:" << sInOldName << "InNewName:" << sInNewName;
	if (sInNewName.isEmpty() || sInOldName == sInNewName)
		return;

	if (isLostConnection(tr("Renamng..."), sRequestingPanel))
		return;

	m_error = NO_ERROR;
	m_currentCommand = RENAME;
	m_remoteOperation = NO_COMMAND;
	m_sRequestingPanel = sRequestingPanel;
	m_bOperationCanceled = false;

	QString sNewName = sInNewName.right(sInNewName.length()-sInNewName.lastIndexOf(QDir::separator())-1);
	QString sMime = MimeType::getFileMime(sNewName, pFileInfoOldName->isDir(), pFileInfoOldName->isSymLink());

	FileInfo *pNewFileInfo = new FileInfo(pFileInfoOldName);
	pNewFileInfo->setData(COL_NAME, sNewName);
	pNewFileInfo->setData(COL_MIME, sMime);

	m_processedFiles->clear();
	m_processedFiles->append(pNewFileInfo); // used for updating FileListView
	m_slItemsToModify.clear();
	m_slItemsToModify.append(sInOldName);

	m_pFtp->rename(sInOldName, sInNewName);
}

void FtpSubsystem::makeDir( const QString &sParentDirPath, const QString &sInNewDirsList, const QString &sRequestingPanel )
{
	qDebug() << "FtpSubsystem::makeDir. InNewDirsList:" << sInNewDirsList;
	if (sInNewDirsList.isEmpty())
		return;

	if (isLostConnection(tr("Making directory..."), sRequestingPanel))
		return;

	m_rawCmdCount = 0;
	m_error = NO_ERROR;
	m_currentCommand = CREATE_DIR;
	m_remoteOperation = NO_COMMAND;
	m_sRequestingPanel = sRequestingPanel;
	m_bOperationCanceled = false;

	m_sFilesNameForStatLst.clear();
	m_preProcessedFiles->clear();
	m_processedFiles->clear();

	QStringList slNewDirsList = sInNewDirsList.split('|');
	m_processedFilesCount = slNewDirsList.count();
	QString sNewDirName;
	FileInfo *pDirFI; // obj.from this pointer is usable in slotCommandStarted and updateListedItems
	QString sPath = sParentDirPath.mid(1, sParentDirPath.length()-1);

	for (int i=0; i<m_processedFilesCount; i++) {
		sNewDirName = slNewDirsList[i];
		if (sNewDirName.endsWith('/'))
			sNewDirName.remove(sNewDirName.length()-1, 1);

		pDirFI = new FileInfo(QFileInfo(sNewDirName).fileName(), sPath, 0, true, false, QDateTime().currentDateTime(), "", "ftp", "ftp", "folder");
		m_preProcessedFiles->append(pDirFI);

		m_pFtp->mkdir(sNewDirName);
// 		m_pFtp->mkdir(sParentDirPath+sNewDirName);
		m_pFtp->rawCommand("STAT " + sNewDirName); // get stat for given dir
		m_sFilesNameForStatLst.append(sNewDirName);
		qDebug() << "FtpSubsystem::makeDir. Called MKDIR and STAT commands on QFtp instance. Added following dirName:" << sParentDirPath + sNewDirName;
	}
}

void FtpSubsystem::makeFile( const QString &sParentDirPath, const QString &sInFileNameList, const QString &sRequestingPanel )
{
	qDebug() << "FtpSubsystem::makeFile. InFileNameList:" << sInFileNameList;
	if (sInFileNameList.isEmpty())
		return;

	if (isLostConnection(tr("Making empty file..."), sRequestingPanel))
		return;

	Settings settings;

	m_preRemovedCounter = 0;
	m_removedCounter = 0;
	m_rawCmdCount = 0;
	m_error = NO_ERROR;
	m_currentCommand = CREATE_FILE;
	m_remoteOperation = NO_COMMAND;
	m_sRequestingPanel = sRequestingPanel;
	m_bOperationCanceled = false;

	m_sFilesNameForStatLst.clear();
	m_preProcessedFiles->clear();
	m_processedFiles->clear();

	m_bNoneOverwriting = false;
	m_bAlwaysOverwrite = settings.value("FilesSystem/AlwaysOverwrite", false).toBool();
	m_eUpdatingFilesWith = NONEupd;
	m_bSkipCopyingCurrentFile = false;

	QStringList newFilesList = sInFileNameList.split('|');
	m_processedFilesCount = newFilesList.count();
	QString sNewFullFileName, sNewFileName, sNewNameOfNewFile;
	QFile *pEmptyFile = NULL;
	FileInfo *pNewFileFI = NULL; // obj.from this pointer is usable in slotCommandStarted and updateListedItems
	FileInfo *pExistFI = NULL;

	for (int i=0; i<m_processedFilesCount; i++) {
		sNewFileName = newFilesList[i];
		sNewFullFileName = m_sTmpDir + sNewFileName;
		// -- create empty file on localsubsystem to be able upload it later
		pEmptyFile = new QFile(sNewFullFileName); // obj.deleted in slotCommandFinished for (command == QFtp::Put)
		m_error  = pEmptyFile->open(QIODevice::ReadWrite | QIODevice::Append) ? NO_ERROR : CANNOT_CREATE_FILE;
		m_status = (m_error == NO_ERROR) ? CREATING_FILE : ERROR_OCCURED;
		pEmptyFile->close();
		if (! pEmptyFile->exists())
			continue;

		pNewFileFI = new FileInfo(sNewFileName, sParentDirPath, 0, false, false, QDateTime().currentDateTime(), "", "", "", "");
		// -- check if file name the same as newly created alredy exist in current directory
		if ((pExistFI=fileInfo(sNewFileName)) != NULL) { // found file with the same name
			pNewFileFI->setData(COL_TIME, pExistFI->lastModified()); // update to show correct information in overwrite dialog about source file
			pNewFileFI->setData(COL_SIZE, pExistFI->size());
			if (isNeedToOverwriteFile(sNewFullFileName, pNewFileFI)) {
				if (m_bRenameTargetFile) {
					sNewFileName = QFileInfo(m_sTargetFileName).fileName();
					sNewNameOfNewFile = m_sTmpDir + sNewFileName;
					pNewFileFI->setData(COL_NAME, sNewFileName);
					QFile(sNewFullFileName).remove();
					pEmptyFile->setFileName(sNewNameOfNewFile);
					pEmptyFile->open(QIODevice::ReadWrite | QIODevice::Append);
					pEmptyFile->close();
// 					pEmptyFile->rename(sNewFullFileName, sNewNameOfNewFile); // rename, because will be uploaded file with new name (! QFtp don't see such file)
				}
			}
		}
		if (m_bSkipCopyingCurrentFile)
			continue;

		m_preProcessedFiles->append(pNewFileFI);
		// -- put created file on the server
		m_pFtp->put(pEmptyFile, sNewFileName);
// 		m_pFtp->put(pEmptyFile, m_currentUri.path() + sNewFileName);
		m_pFtp->rawCommand("STAT " + sNewFileName); // get stat for given dir

		m_sFilesNameForStatLst.append(sNewFileName);
		qDebug() << "FtpSubsystem::makeFile. Called PUT and STAT commands on QFtp instance. Added following fileName:" << sNewFileName;
	}
}

void FtpSubsystem::weigh( FileInfoToRowMap *pSelectionMap, const URI &uri, const QString &sRequestingPanel, bool bWeighingAsSubCmd, bool bResetCounters )
{
	m_status = WEIGHING;
	m_currentCommand = WEIGH;
	m_bOperationFinished = false;
	m_bOperationCanceled = false;

	FileInfo *pFI;
	//QFileInfo qfi;
	int files = 0, dirs = 0, symlinks = 0;
	m_totalWeight = 0;
	QMap<FileInfo *, int>::const_iterator it = pSelectionMap->constBegin();
	while (it != pSelectionMap->constEnd()) {
		pFI = it.key();
		if (pFI->isSymLink()) {
			//symlinks++;
			//qfi.setFile(fi->absoluteFilePath());
			//m_totalWeight += qfi.readLink().length(); // resolve link
			files++;
		}
		else {
			if (pFI->isDir())
				dirs++;
			else {
				files++;
				m_totalWeight += pFI->size();
			}
		}
		++it;
	}
	emit signalCounterProgress(files + symlinks, FILE_COUNTER, true);  // set total files
	emit signalCounterProgress(dirs,  DIR_COUNTER, true); // set total directories
	emit signalCounterProgress(0, FILE_COUNTER, false);  // set 0/total (initialize files and dirs counters)
	emit signalDataTransferProgress(0, m_totalWeight, 0, true);

	m_totalFiles = files + symlinks;
	m_totalDirs  = dirs;
	m_status = WEIGHED;
	m_bOperationFinished = true;

// 	if (! bWeighingAsSubCmd) // commented due to: properties dialog needs this signal
		emit signalStatusChanged(m_status, sRequestingPanel);
}

void FtpSubsystem::weighItem( FileInfo *pFileInfo, const URI &uri, const QString &sRequestingPanel, bool bClearCounters )
{
	if (pFileInfo->isDir()) {
		m_totalWeight = 0;
		m_totalFiles = 0;
		m_totalDirs  = 1;
	}
	else {
		m_totalWeight = pFileInfo->size();
		m_totalFiles = 1;
		m_totalDirs  = 0;
	}
	m_currentCommand = WEIGH;
	m_status = WEIGHED;
	m_bOperationFinished = true;
	emit signalStatusChanged(m_status, sRequestingPanel);
}

void FtpSubsystem::getWeighingResult( qint64 &weight, qint64 &realWeight, uint &files, uint &dirs )
{
	weight = m_totalWeight;
	realWeight = -1;
	files = m_totalFiles;
	dirs  = m_totalDirs;
}


void FtpSubsystem::remove( FileInfoToRowMap *pSelectionMap, const URI &uri, bool bWeighBefore, const QString &sRequestingPanel, bool bForceRemoveAll )
{
	QString sFN = "FtpSubsystem::remove.";
	if (pSelectionMap == NULL) {
		qDebug() << sFN << "Internal ERROR. Empty selectionMap.";
		return;
	}
	if (isLostConnection(tr("Removing..."), sRequestingPanel))
		return;

	m_selectionMap.clear();
	m_selectionMap.unite(*pSelectionMap); // * workaround W1, because QFtp clean commands queues and does abort after first error occurence

	m_sRequestingPanel = sRequestingPanel;
	m_nRmTotalFiles = pSelectionMap->size(); // used in progress dialog
	qDebug() << sFN << "Selected files and/or dirs:" << m_nRmTotalFiles << "weighBefore:" << bWeighBefore;
	if (m_nRmTotalFiles == 0)
		return;
	if (bWeighBefore) {
		qDebug() << sFN << "WARNING. Only flat weighing is supported!";
		weigh(pSelectionMap, uri, sRequestingPanel);
	}
	// -- removing files/dirs
	m_error = NO_ERROR;
	m_currentCommand = REMOVE;
	m_remoteOperation = NO_COMMAND;
	m_bOperationFinished = false;
	m_bOperationCanceled = false;
	m_processedFiles->clear();
	m_preProcessedFiles->clear();
	m_cmdQueue.clear();

	m_preRemovedCounter = 0;
	m_removedCounter = 0;
	m_removedWeight  = 0;
	m_rmFilesCounter = 0;
	m_rmDirsCounter  = 0;

	FileInfo *pFI;
	QString sProcessedFile;
	QMap<FileInfo *, int>::const_iterator it = pSelectionMap->constBegin();

	while (it != pSelectionMap->constEnd()) {
		//qDebug() << it.key() << ": " << it.value() << endl;
		pFI = it.key();
		sProcessedFile = pFI->fileName();

		if (pFI->isSymLink()) {
//			m_rawCmdCount = 0; m_status = LISTED; m_pFtp->rawCommand("STAT " + sProcessedFile); // only for test of STAT command
			m_pFtp->remove(sProcessedFile);
		}
		else if (pFI->isDir())
			m_pFtp->rmdir(sProcessedFile);
		else
			m_pFtp->remove(sProcessedFile);

		m_preProcessedFiles->append(pFI); // we can to add newly created object FileInfo too (then we need copying contructor in FileInfo class and removeing each object from list when it is clened)
		++it;
	}
}

void FtpSubsystem::removeAfterErr( FileInfoToRowMap *pSelectionMap, bool bWeighBefore )
{
	// * workaround W1, because QFtp clean commands queue and do abort after first error occured
	m_nRmTotalFiles = pSelectionMap->size(); // used in progress dialog
	qDebug() << "FtpSubsystem::removeAfterErr. Selected files and/or dirs:" << m_nRmTotalFiles << "weighBefore:" << bWeighBefore;
	if (m_nRmTotalFiles == 0)
		return;

	// -- removing files/dirs
	m_status = REMOVING;
	m_bOperationFinished = false;

	FileInfo *fi;
	QString sProcessedFile;
	QMap<FileInfo *, int>::const_iterator it = pSelectionMap->constBegin();
	while (it != pSelectionMap->constEnd()) {
		//qDebug() << it.key() << ": " << it.value() << endl;
		fi = it.key();
		sProcessedFile = fi->fileName();

		if      (fi->isSymLink())
			m_pFtp->remove(sProcessedFile);
		else if (fi->isDir())
			m_pFtp->rmdir(sProcessedFile);
		else
			m_pFtp->remove(sProcessedFile);

		//m_preProcessedFiles->append(fi); // we can to add newly created object FileInfo too (then we need copying contructor in FileInfo class and removeing each object from list when it is clened)
		++it;
	}
}

void FtpSubsystem::copyFiles( FileInfoToRowMap *pSelectionMap, const QString &sTargetPath, bool bMove, bool bWeighBefore, const QString &sRequestingPanel )
{
	bool bUpload = (URI(sTargetPath).mime().startsWith("remote/"));

	// 	qDebug() << "FtpSubsystem::download. host:" << sHost << " AbsFileName:" << pFileInfo->absoluteFileName() << " method:" << nReadingMode << "sRequestingPanel:" << sRequestingPanel;
	QString sFN = "FtpSubsystem::copyFiles.";
	sFN += (bUpload) ? " UPLOAD." : " DOWNLOAD.";
	if (pSelectionMap == NULL) {
		qDebug() << sFN << "Internal ERROR. Empty selectionMap.";
		return;
	}

	m_nSelectedFiles = pSelectionMap->size();
	bool bDifferentTargetPath = (m_currentUri.toString() != sTargetPath);
	qDebug() << sFN << "Selected items:" << m_nSelectedFiles << "weighBefore:" << bWeighBefore << "DifferentTargetPath:" << bDifferentTargetPath << " TargetPath:" << sTargetPath;
	if (m_nSelectedFiles == 0) {
		qDebug() << sFN << "Internal ERROR. Not selected items.";
		return;
	}

	QString sTitle = bMove ? tr("Moving...") :  tr("Copying...");
	if (isLostConnection(sTitle, sRequestingPanel))
		return;

	// 	m_recursiveOperation = NO_COMMAND;
	m_error = NO_ERROR;
	m_currentCommand = bMove ? MOVE : COPY;
	m_remoteOperation = bUpload ? UPLOAD : DOWNLOAD;
	m_sRequestingPanel = sRequestingPanel;
	m_bOperationFinished = true; // true valu is required for below checks
	m_bOperationCanceled = false;
	m_bAlwaysOverwriteSelected = false; // related with Overwrite dialog, maybe initial value should be taked fron confg.
	m_sNameOfProcessedFile = sTargetPath;
	// -- prepare target path
	m_sCopyTargetPath = sTargetPath; // need to set for newly created FileInfo (what is put to second panel)
	if (! m_sCopyTargetPath.endsWith(QDir::separator()))
		m_sCopyTargetPath += QDir::separator();
	if (m_remoteOperation == DOWNLOAD) { // check if target (local) directory is writeable
		if (! QFileInfo(sTargetPath).isWritable()) {
			qDebug() << sFN << "Target directory is not writable!";
			m_error = CANNOT_WRITE;
			emit signalStatusChanged(Vfs::ERROR_OCCURED, sRequestingPanel);
			return;
		}
	}
	else
		m_pUploadFile = NULL;

	// -- weigh all of files and dirs (only flat way is possible)
	m_nTotalDirs = 0;
	m_nTotalFiles = 0;
	m_nDirsCouter = 0;
	m_nFilesCouter = 0;
	m_removedCounter = 0;
	m_preRemovedCounter = 0;
	m_nBytesCopiedTotaly = 0;
	m_nCopyTotalFilesWeigh = 0;
	FileInfoToRowMap::const_iterator it = pSelectionMap->constBegin();
	FileInfo *pFI;
	while (it != pSelectionMap->constEnd()) {
		pFI = it.key();
		if (! pFI->isDir() || pFI->isSymLink()) {
			m_nCopyTotalFilesWeigh += pFI->size();
			m_nTotalFiles++;
		}
		else
			m_nTotalDirs++;
		++it;
	}

	if (! bUpload) { // DOWNLOAD
		// -- check if is available free space in target directory for all selected items (reliable only if are selected files only)
		long long freeBytes, totalBytes;
		Vfs::getStatFS(sTargetPath, freeBytes, totalBytes);
		if (freeBytes < m_nCopyTotalFilesWeigh) {
			m_status = ERROR_OCCURED;
			m_error  = NO_FREE_SPACE;
			emit signalStatusChanged(m_status, sRequestingPanel);
			return;
		}
	}
	m_eUpdatingFilesWith = NONEupd;
	m_bOperationFinished = false;
	m_bNoneOverwriting = false;
	m_nCopyCounter = 0;

	// -- init progress dialog
	emit signalCounterProgress(0, FILE_COUNTER, m_bLOCAL_PROGRESS);
	emit signalCounterProgress(m_nTotalFiles, FILE_COUNTER, m_bTOTAL_PROGRESS);
	emit signalCounterProgress(0, DIR_COUNTER, m_bLOCAL_PROGRESS);
	emit signalCounterProgress(m_nTotalDirs, DIR_COUNTER, m_bTOTAL_PROGRESS);
	emit signalTotalProgress((100 * m_nCopyCounter) / m_nSelectedFiles, m_nCopyTotalFilesWeigh);
	emit signalDataTransferProgress(m_nBytesCopiedTotaly, m_nCopyTotalFilesWeigh, 0, m_bTOTAL_PROGRESS);
	emit signalDataTransferProgress(0, 0, 0, m_bLOCAL_PROGRESS);
	// 	qDebug() << sFN << "CopyCounter:" << m_nCopyCounter << "SelectedFiles:" << m_nSelectedFiles << "CopyTotalFilesWeigh:" << m_nCopyTotalFilesWeigh << "CopyFilesWeigh:" << m_nBytesCopiedTotaly;

	m_cmdQueue.clear();
	m_processedFiles->clear();
	m_preProcessedFiles->clear();

	QString sProcessedFile;
	it = pSelectionMap->constBegin();
	QString sMime;

	while (it != pSelectionMap->constEnd()) {
		m_error = NO_ERROR;
		pFI = new FileInfo(it.key());
		sProcessedFile = pFI->fileName();
		if (bUpload) { // correction is required, because is not allow copy symlink to FTP, only file pointed by symlink is copied
			if (pFI->isSymLink()) {
				sMime = MimeType::getFileMime(pFI->symLinkTarget(), false);
				pFI->setData(COL_SIZE, QFileInfo(pFI->symLinkTarget()).size());
				pFI->setData(COL_MIME, sMime);
				pFI->setData(COL_PERM, permissionsAsString(pFI->symLinkTarget()));
			}
		}
		m_preProcessedFiles->append(pFI);

		if (m_currentCommand == MOVE && pFI->isDir()) {
			qDebug() << sFN << "Moving directories in not supported. Selected directory will be copied.";
			m_cmdQueue.append(COPY);
		}
		else
			m_cmdQueue.append(m_currentCommand);

		qDebug() << sFN << "Queue updated with file:" << sProcessedFile;
		++it;
	}

	if (m_preProcessedFiles->size() > 0)
		QTimer::singleShot(0, this, &FtpSubsystem::slotRunNextCommand);
}

void FtpSubsystem::openWith( FileInfoToRowMap *pSelectionMap, const URI &uri, const QString &sRequestingPanel )
{
	QString sFN = "FtpSubsystem::openWith.";
	if (pSelectionMap == NULL) {
		qDebug() << sFN << "Internal ERROR. Empty selectionMap.";
		return;
	}
	int nSelFilesNum = pSelectionMap->count();
	qDebug() << sFN << "Selected files and/or dirs:" << nSelFilesNum;
	if (nSelFilesNum == 0)
		return;
	if (isLostConnection(tr("Opening with..."), sRequestingPanel))
		return;

	int nResult = -1;
	if (m_bConfirmDownloadForOpenWith) {
		QString sAbsFileName = m_currentUri.toString() + QDir::separator() + pSelectionMap->constBegin().key()->fileName();
		QString sQuestion = (nSelFilesNum > 1) ? tr("Download and open files?") : tr("Do you want to download and open given file?");
		QString sFImsg = (nSelFilesNum > 1) ? QString(tr("Selected %1 items")).arg(nSelFilesNum) : sAbsFileName;
		nResult = MessageBox::yesNo(m_pParent, tr("Open with..."), sFImsg, sQuestion,
									m_bConfirmDownloadForOpenWith,
									MessageBox::Yes, // default focused button
									tr("Always ask for confirmation")
								);
		Settings settings;
		settings.update("ftp/ConfirmDownloadForOpenWith", m_bConfirmDownloadForOpenWith);
	}
	else
		nResult = MessageBox::Yes;

	if (nResult == MessageBox::No || nResult == 0) { // 0 if user closed dialog
		m_bOperationFinished = true;
		emit signalStatusChanged(Vfs::OPEN_WITH, sRequestingPanel);
		return;
	}
	m_error  = NO_ERROR;
	m_status = UNKNOWN_STAT;
	m_currentCommand = OPEN_WITH_CMD;
	m_remoteOperation = NO_COMMAND;
	m_bOperationFinished = false;
	m_bOperationCanceled = false;
	m_sRequestingPanel = sRequestingPanel;
	m_processedFiles->clear();
	m_preProcessedFiles->clear();
	m_nOpenWithCounter = 0;

	FileInfo *pFI;
	QStringList slProcessedFiles;
	long long nOpenedFilesWeigh = 0;
	FileInfoToRowMap::const_iterator it = pSelectionMap->constBegin();
	while (it != pSelectionMap->constEnd()) {
// 			qDebug() << it.key() << ": " << it.value() << endl;
		pFI = it.key();
		slProcessedFiles << pFI->fileName();
		m_preProcessedFiles->append(pFI);
		nOpenedFilesWeigh += pFI->size();
		++it;
	}
	long long freeBytes, totalBytes;
	Vfs::getStatFS(m_sTmpDir, freeBytes, totalBytes);
	if (freeBytes < nOpenedFilesWeigh) { // m_nSourceFileSize is init.into 'initPutFile()'
		m_error  = NO_FREE_SPACE;
		m_status = ERROR_OCCURED;
		emit signalStatusChanged(m_status, sRequestingPanel);
		return;
	}
	foreach (QString sProcessedFile, slProcessedFiles) {
		qDebug() << sFN << "Added file to downloading queue:" << sProcessedFile;
		if (updateTargetFileInLS(true)) {
			m_pFtp->get(sProcessedFile);
			qDebug() << sFN << "Called GET command on QFtp instance for following file:" << sProcessedFile;
		}
	}
}

void FtpSubsystem::readFileToViewer( const QString &sHost, FileInfo *pFileInfo, QByteArray &baBuffer, int &nBytesRead, int nReadingMode, const QString &sRequestingPanel )
{
	QString sFN = "FtpSubsystem::readFileToViewer.";
	qDebug() << sFN << "Host:" << sHost << " AbsFileName:" << pFileInfo->absoluteFileName() << " ReadingMode:" << nReadingMode << " RequestingPanel:" << sRequestingPanel;
	// -- check if temporary directory is set
	if (m_sTmpDir.isEmpty()) {
		qDebug() << sFN << "Internal ERROR. Working directory is not set!";
		m_status = ERROR_OCCURED;
		m_error = CANNOT_DOWNLOAD;
		emit signalStatusChanged(Vfs::DOWNLOADED_TO_VIEW, sRequestingPanel);
		return;
	}
	long long nOpenedFilesWeigh = pFileInfo->size();
	m_nBytesCopiedTotaly = 0;
	m_nCopyTotalFilesWeigh = nOpenedFilesWeigh;
	long long freeBytes, totalBytes;
	Vfs::getStatFS(m_sTmpDir, freeBytes, totalBytes);
	if (freeBytes < nOpenedFilesWeigh) {
		m_status = ERROR_OCCURED;
		m_error  = NO_FREE_SPACE;
		emit signalStatusChanged(m_status, sRequestingPanel);
		return;
	}
	if (isLostConnection(tr("Reading file to view..."), sRequestingPanel))
		return;
	// -- check readability
	if (nReadingMode == Preview::Viewer::GET_READABILITY) {
		nBytesRead = canOpenFileForReading(pFileInfo) ? 1 : 0; // need for FileViewer::prepareToView to check, also sets m_status, m_error and emits signalStatusChanged
		emit signalStatusChanged(((nBytesRead > 0) ? Vfs::READY_TO_READ : Vfs::NOT_READY_TO_READ), sRequestingPanel); // unmark item
		return;
	}
	int nResult = -1;
	if (m_bConfirmDownloadForView) {
		QString sAbsFileName = m_currentUri.toString() +"\n"+ QDir::separator() + pFileInfo->fileName();
		QString sQuestion = tr("Do you want to download and view given file?");
		nResult = MessageBox::yesNo(m_pParent, tr("File View..."), sAbsFileName, sQuestion,
									m_bConfirmDownloadForView,
									MessageBox::Yes, // default focused button
									tr("Always ask for confirmation")
								);
		Settings settings;
		settings.update("ftp/ConfirmDownloadForView", m_bConfirmDownloadForView);
	}
	else
		nResult = MessageBox::Yes;

	nBytesRead = 1;
	if (nResult == MessageBox::No || nResult == 0) { // 0 if user closed dialog
		nBytesRead = -1; // flag telling that need to close window of view
		m_bOperationFinished = true;
		return;
	}
	m_error  = NO_ERROR;
	m_status = DOWNLOADING;
	m_currentCommand = READ_FILE_TO_VIEW;
	m_remoteOperation = NO_COMMAND;
	m_bOperationFinished = false;
	m_bOperationCanceled = false;
	m_sRequestingPanel = sRequestingPanel;

	m_nOpenWithCounter = 0;
	m_processedFiles->clear();
	m_preProcessedFiles->clear();
	m_preProcessedFiles->append(pFileInfo);
	m_sNameOfProcessedFile = pFileInfo->absoluteFileName();

	QString sProcessedFile = pFileInfo->fileName();
	if (updateTargetFileInLS(true)) {
		m_pFtp->get(sProcessedFile);
		qDebug() << sFN << "Called GET command on QFtp instance for following file:" << sProcessedFile;
	}
	else {
		qDebug() << sFN << "Cannot start DOWNLOAD of file:" << sProcessedFile;
		m_status = DOWNLOADING;
		slotCommandFinished(QFtp::Get, false); // calling required only for properly update of counters and flags
	}
}


bool FtpSubsystem::updateTargetFileInLS( bool bInitOnly )
{
	bool bCopyOrMoveCmd = (m_currentCommand == COPY || m_currentCommand == MOVE);
	int  nDwnCounter = bCopyOrMoveCmd ? m_nCopyCounter : m_nOpenWithCounter;
	if (nDwnCounter < 0)
		return false;

	QString sFN = "FtpSubsystem::updateTargetFileInLS.";
	QString sTargetPath = (bCopyOrMoveCmd) ? m_sCopyTargetPath : m_sTmpDir;
	FileInfo *pFI = m_preProcessedFiles->at(nDwnCounter);
	QString sFullFileName = sTargetPath + pFI->fileName();
// 	qDebug() << sFN << "DwnCounter:" << nDwnCounter << "InitOnly:" << bInitOnly;

	if (bInitOnly) {
		m_processedFiles->append(pFI);
		if (pFI->isDir()) {
			m_nCopyCounter++;
			if (QFile(sFullFileName).exists())
				return true;
			if (! QDir().mkdir(sFullFileName)) {
				m_status = ERROR_OCCURED;
				m_error  = CANNOT_CREATE_DIR;
				return false;
			}
			QString sSrcDir = fullHostName() + pFI->absoluteFileName();
			emit signalNameOfProcessedFile(sSrcDir, sFullFileName);
			emit signalCounterProgress(++m_nDirsCouter, DIR_COUNTER);
			int nTotalItems = m_nFilesCouter + m_nDirsCouter;
			emit signalTotalProgress((100 * nTotalItems) / m_nSelectedFiles, m_nSelectedFiles);
			emit signalDataTransferProgress(0, 0, 1, m_bLOCAL_PROGRESS);
			qDebug() << sFN << "Directory created:" << sFullFileName;
			return true;
		}
		m_newDwnFile.setFileName(sFullFileName);
		m_sTargetFileName = sFullFileName;
		Settings settings;
		if (m_newDwnFile.exists()) {
			if (! m_bAlwaysOverwriteSelected) // if user selected All in "Overwrite dialg" then it has true value
				m_bAlwaysOverwrite = settings.value("FilesSystem/AlwaysOverwrite", false).toBool();
			if (m_bAlwaysOverwrite) {
				if (! removeFileInLS())
					return false;
			}
			else {
				if (! isNeedToOverwriteFile(sFullFileName, pFI))
					return false;
			}
		}
		m_error  = m_newDwnFile.open(QIODevice::ReadWrite | QIODevice::Append) ? NO_ERROR : CANNOT_CREATE_FILE;
		m_status = (m_error == NO_ERROR) ? OPENED_FOR_WRITE : ERROR_OCCURED;
		sFullFileName = m_newDwnFile.fileName(); // get name again, because file could be renamed
		if (m_status != ERROR_OCCURED)
			qDebug() << sFN << "Opened file:" << sFullFileName;
		else
			qDebug() << sFN << "Cannot open file:" << sFullFileName;

		return (m_status != ERROR_OCCURED);
	} // if (bInitOnly)

	if (pFI->isDir()) // recursive copying is not supported
		return true;

	// -- get data to buffer and write it to the file
	const qint64 nMaxLen = 1024*1024; // 1MB
	char  *cBuffer = new char[nMaxLen + 1];
	qint64 nRealReadBytes = m_pFtp->read(cBuffer, nMaxLen);
// 	qint64 nRealWriteBytes = 0;
// 	if (m_newDwnFile.isOpen())
// 		nRealWriteBytes = m_newDwnFile.write(cBuffer, nRealReadBytes);
		m_newDwnFile.write(cBuffer, nRealReadBytes);
// 	qDebug() << sFN << "Wrote bytes:" << nRealWriteBytes << "onto file:" << sFullFileName;
	delete [] cBuffer;

	return true;
}

void FtpSubsystem::updateFileAttributesInLS( const QString &sAbsFileName, bool bUpdatePermission, bool bUpdateTime )
{
	QString sFN = "FtpSubsystem::updateFileAttributesInLS.";
	bool bSetCurrentTime = (! bUpdatePermission && ! bUpdateTime); // this is required to trigger WATCHER_REQ_CHATR, thanks that list will be properly updated
	qDebug() << sFN << "AbsFileName:" << sAbsFileName << " UpdatePermission:" << bUpdatePermission << " UpdateTime:" << bUpdateTime << " SetCurrentTime:" << bSetCurrentTime;
	if (m_nCopyCounter < 0 || m_nCopyCounter > m_preProcessedFiles->size()) {
		qDebug() << sFN << "Internal error: m_nCopyCounter is over the range!";
		return;
	}

	FileInfo *pFI = m_preProcessedFiles->at(m_nCopyCounter-1);
	if (bUpdatePermission) {
		// -- set permission
		int nPermissions = permissionsFromString(pFI->permissions());
		if (::chmod(QFile::encodeName(sAbsFileName), nPermissions) != 0) {
			m_error  = CANNOT_SET_PERM;
			m_status = ERROR_OCCURED;
			qDebug() << sFN << "chmod error:" << errno << "file:" << sAbsFileName;
		}
	}
	// -- set access and modified time
	if (bUpdateTime || bSetCurrentTime) { // second condition
		utimbuf uTimeBuf;
		if (bSetCurrentTime) {
			uTimeBuf.actime  = QDateTime::currentDateTime().toTime_t();
			uTimeBuf.modtime = QDateTime::currentDateTime().toTime_t();
		}
		else {
			uTimeBuf.actime  = pFI->lastModified().toTime_t();
			uTimeBuf.modtime = pFI->lastModified().toTime_t();
		}
		if (::utime(QFile::encodeName(sAbsFileName), &uTimeBuf) != 0) {
			m_error  = CANNOT_SET_MA_TIME;
			m_status = ERROR_OCCURED;
			qDebug() << sFN << "utime error:" << errno << "file:" << sAbsFileName;
		}
	}
}

bool FtpSubsystem::removeFileInLS()
{
	if (! m_newDwnFile.remove()) {
		m_error  = CANNOT_REMOVE;
		m_status = ERROR_OCCURED;
		return false;
	}
	qDebug() << "FtpSubsystem::removeFileInLS. Removed file:" << m_newDwnFile.fileName();
	return true;
}

bool FtpSubsystem::isNeedToOverwriteFile( const QString &sTargetFullFileName, FileInfo *pSrcFileInfo )
{
	QFileInfo targetQFI(sTargetFullFileName);
	enum FileOverwriteBtns { Yes=1, No, DiffSize, Rename, All, Update, None, Cancel }; // all Id's are the same like in OverwriteFileDialog class
	if (m_status != DOWNLOADING && m_status != UPLOADING && m_status != CREATING_FILE)
		return false;

	QString sFN = "FtpSubsystem::isNeedToOverwriteFile.";
	int  nResult = -1;
	bool bTargetIsSymLink = targetQFI.isSymLink();
	bool bDontCreateTargetFile = false;
	m_bRenameTargetFile = false;
	FileInfo targetFileInfo(targetQFI.fileName(), targetQFI.path(), (bTargetIsSymLink ? targetQFI.symLinkTarget().length() : targetQFI.size()), targetQFI.isDir(), bTargetIsSymLink, targetQFI.lastModified(), "", "", "", "");
	FileInfo sourceFileInfo(pSrcFileInfo);

	if (m_eUpdatingFilesWith == NONEupd) {
		if (! m_bNoneOverwriting && ! m_bAlwaysOverwrite) {
			if (m_status == DOWNLOADING) {
				m_sTargetFileName = targetFileInfo.absoluteFileName(); // because name may be changed and then targetFileInfo will be updated
				emit signalNameOfProcessedFile(sourceFileInfo.absoluteFileName(), m_sTargetFileName);
				emit signalShowFileOverwriteDlg(targetFileInfo, sourceFileInfo, nResult); // returns reference to targetFileInfo and reference to result
			}
			else {
				m_sTargetFileName = m_currentUri.protocolWithSuffix()+m_currentUri.host()+"/"+sourceFileInfo.absoluteFileName(); // because name may be changed and then targetFileInfo will be updated
				emit signalNameOfProcessedFile(targetFileInfo.absoluteFileName(), m_sTargetFileName);
				emit signalShowFileOverwriteDlg(sourceFileInfo, targetFileInfo, nResult); // returns reference to targetFileInfo and reference to result
			}
			if (nResult == 0) // dialog has been closed
				nResult = Cancel;
		}
	}
	m_error = NO_ERROR;

	if (nResult == Yes) {
		if (m_status == DOWNLOADING)
			return removeFileInLS();
		else { // UPLOADING or CREATING_FILE (new empty)
			QString sProcessedFileName = QFileInfo(m_sTargetFileName).fileName();
			m_pFtp->remove(sProcessedFileName);
			qDebug() << sFN << "Called REMOVE command on QFtp instance for following file:" << sProcessedFileName;
			return true;
		}
	}

	if (nResult != -1 && ! m_bRenameTargetFile) {
		if (nResult == No)
			bDontCreateTargetFile = true;
		else
		if (nResult == DiffSize)
			m_eUpdatingFilesWith = SIZEupd;
		else
		if (nResult == Rename)
			m_bRenameTargetFile = true;
		else
		if (nResult == All) {
			m_bAlwaysOverwrite = true; // disable of showing OverwritingDialog
			m_bAlwaysOverwriteSelected = true;
		}
		else
		if (nResult == Update)
			m_eUpdatingFilesWith = DATEupd;
		else
		if (nResult == None) {
			m_bNoneOverwriting = true; // disable of showing OverwritingDialog
		}
		else
		if (nResult == Cancel) {
			cancelCurrentOperation();
			m_bSkipCopyingCurrentFile = true;
			return false;
		}
	} // nResult != -1
	if (m_bNoneOverwriting)
		bDontCreateTargetFile = true;

	if (m_eUpdatingFilesWith == DATEupd) {
		QDateTime UTCTime(QDate(1970,1,1), QTime(0,0));
		uint sourceFileTime = UTCTime.secsTo(sourceFileInfo.lastModified());
		uint targetFileTime = UTCTime.secsTo(targetFileInfo.lastModified());
		bDontCreateTargetFile = (sourceFileTime >= targetFileTime);
	}
	if (m_eUpdatingFilesWith == SIZEupd)
		bDontCreateTargetFile = (sourceFileInfo.size() == targetFileInfo.size()); //  QFileInfo(m_sTargetFileName).size()

	if (m_bRenameTargetFile) {
		if (m_status == DOWNLOADING) {
			m_sNameOfProcessedFile = QFileInfo(m_sTargetFileName).path()+"/"+sourceFileInfo.fileName(); //sourceFileInfo.absoluteFileName();
			m_newDwnFile.setFileName(m_sNameOfProcessedFile);
			while (m_newDwnFile.exists()) {
				m_error = ALREADY_EXISTS;
				emit signalStatusChanged(Vfs::ERROR_OCCURED, m_sRequestingPanel);
				emit signalShowFileOverwriteDlg(targetFileInfo, sourceFileInfo, nResult); // returns reference to targetFileInfo and reference to result
				if (nResult == No) { // user replied "Cancel" in entering new name dialog
					bDontCreateTargetFile = true;
					break;
				}
				m_sTargetFileName = targetFileInfo.filePath()+"/"+sourceFileInfo.fileName(); // targetFileInfo.absoluteFileName(); // because name may be changed and then targetFileInfo will be updated
				m_newDwnFile.setFileName(m_sTargetFileName);
			}
			m_sNameOfProcessedFile = sourceFileInfo.absoluteFileName();
		}
		else { // UPLOADING or CREATING_FILE (new empty)
			QString sTargetPath = fullHostName() + m_currentUri.path();
			m_sTargetFileName = sTargetPath + targetFileInfo.fileName();  // because name may be changed and then targetFileInfo will be updated
			m_sNameOfProcessedFile = m_sTargetFileName;
			while (fileInfo(m_sTargetFileName) != NULL) { // loop till found item in internal list of listed items (remote location)
				m_error = ALREADY_EXISTS;
				emit signalStatusChanged(Vfs::ERROR_OCCURED, m_sRequestingPanel);
				emit signalShowFileOverwriteDlg(sourceFileInfo, targetFileInfo, nResult); // returns reference to sourceFileInfo and reference to result
				if (nResult == No) { // user replied "Cancel" in entering new name dialog
					bDontCreateTargetFile = true;
					break;
				}
				m_sTargetFileName = sTargetPath + targetFileInfo.fileName();  // because name may be changed and then targetFileInfo will be updated
			}
			m_sNameOfProcessedFile = targetFileInfo.filePath()+"/"+sourceFileInfo.fileName();
		}
		if (m_status == DOWNLOADING)
			emit signalNameOfProcessedFile(sourceFileInfo.absoluteFileName(), m_sTargetFileName);
		else // UPLOADING or CREATING_FILE (new empty)
			emit signalNameOfProcessedFile(targetFileInfo.absoluteFileName(), m_sTargetFileName);
	}
	else {
		if (! bDontCreateTargetFile || m_bAlwaysOverwrite) {
			if (m_status == DOWNLOADING)
				removeFileInLS();
			else { // UPLOADING or CREATING_FILE (new empty)
				QString sProcessedFileName = QFileInfo(m_sTargetFileName).fileName();
				m_pFtp->remove(sProcessedFileName);
				qDebug() << sFN << "Called REMOVE command on QFtp instance for following file:" << sProcessedFileName;
			}
		}
	}

	if (bDontCreateTargetFile) {
		m_bSkipCopyingCurrentFile = true;
		return false; // don't create new file
	}

	return true;
}

FileInfo *FtpSubsystem::statCmdParsing( const QString &sStatDetails )
{
	if (sStatDetails.isEmpty())
		return NULL;

	QStringList slFilesList;
	QStringList slMainFilesList = sStatDetails.split("\r\n");
	bool bRunListingFilesAgain = false;

	if (m_status == LISTED) {
//		QString sFileName =  slMainFilesList[0].split(' ').at(2);
//		sFileName = sFileName.left(sFileName.length()-1);
		QString sDotItem = slMainFilesList[1];
		FileInfo *pLinkFI = m_listedlinksList.at(m_rawCmdCount);
//		foreach(pLinkFI, m_listedlinksList) {
//			if (pLinkFI->fileName() == sFileName) {
				if (sDotItem.at(0) != 'd') { // not directory
					QString sTargetFileName = sDotItem.right(sDotItem.length() - sDotItem.lastIndexOf(' ') - 1);
					if (sTargetFileName == "." || sTargetFileName == "..") { // link to directory
						pLinkFI->setData(6, "folder/symlink");
						pLinkFI->setData(7, tr("Unknown target"));
					}
					else
					if (sDotItem.indexOf("-> ") > 0) { // link to file
// 					if (sDotItem.split("-> ").count() > 1) { // link to file
						pLinkFI->setData(6, "file/symlink");
						pLinkFI->setData(7, sTargetFileName);
					}
					pLinkFI->setData(3, "lrwxrwxrwx");
				}
//				break;
//			}
//		}
		return pLinkFI;
	}
// 	if (! m_bGettingURL && ! sStatDetail.isEmpty()) {
		slFilesList = slMainFilesList;
		slMainFilesList[slMainFilesList.count()-1] = "."; // overwrite line "End Of Status"
// 	}
	const int count = (m_bGettingURL) ? slFilesList.size() : slMainFilesList.size();
	if (count < 3) { // file doesn't exist
		m_error = DOES_NOT_EXIST;
		if (m_bLinkChecking) {
			// not existing target file/dir is always a file
			m_slTargetFileForLinkLst[m_nLinkCheckingCounter] = m_slTargetFileForLinkLst[m_nLinkCheckingCounter] + "\nfile";
			if (m_nLinkCheckingCounter == m_slTargetFileForLinkLst.count()) {
				m_bGettingURL   = false;
				m_bLinkChecking = false;
				bRunListingFilesAgain = true;
			}
			else
				m_nLinkCheckingCounter++;
		}
		slotCommandFinished(QFtp::List, true); // ???
		return NULL;
	}
	QString sName, sTargetLink;
	QStringList slItemList;
	// --- collect all links
	if (m_slTargetFileForLinkLst.isEmpty()) {
		for (int i=1; i < slMainFilesList.count()-1; i++) { // -1 for skip last line: "End Of Status"
			if (slMainFilesList[i].at(0) == 'l') {
				sName = slMainFilesList[i].mid( 56, slMainFilesList[i].length()-56 ); // 56 it's begin position of name (server: proftp)
				slItemList = sName.split(" -> ");
				m_slTargetFileForLinkLst.append( slItemList[0]+"\n"+ slItemList[1] ); // name + target file
			}
		}
		// and init checking all links
		if (m_slTargetFileForLinkLst.count() > 0) {
			m_bLinkChecking = true;
			checkAllLinks(); // that contains in m_targetFileForLinkLst
			return NULL;
		}
	}

	FileInfo *pFI = NULL;
	bool isDir, isSymLink, isReadable;
	long long nSize;
	QTime time;
	QDate date;
	QDateTime lastModified;
	QString sPermissionStr, sOwner, sGroup, sMime;
	uint nameId;
	const int nFutureTolerance = 600; // QFtp part
	const int nCounter = (m_bGettingURL) ? 3 : slMainFilesList.count() - 1; // -1 for skip last line: "End Of Status"

	for (int i=1; i < nCounter; i++) {
		// get name and target file for link
		sName = (m_bGettingURL) ? slFilesList[i] : slMainFilesList[i];
		nameId = indexOfNameInStat(sName);
		sName = sName.mid(nameId, sName.length() - nameId);
		slItemList = sName.split(" -> "); // if not found given separator then here will be name
		if (slItemList.count() > 1) {
			sTargetLink = slItemList[1];
			sName = slItemList[0];
		}
		if (sName == ".")
			continue;
		if (sName == "..") {
			if (m_bGettingURL) { // get name from first line
				if (slFilesList[0].contains('/')) {
					int id1 = slFilesList[0].lastIndexOf('/');
					int id2 = slFilesList[0].lastIndexOf('/', id1 - 1);
					sName    = slFilesList[0].mid(id2 + 1, id1 - id2 - 1);
				}
				/*else {
					name = filesList[0];
					name.replace("Status of ", " ");
					name = name.left(name.indexOf(':'));
				}*/
			}
			else {
				if (slMainFilesList[0].contains('/')) {
					int id1 = slMainFilesList[0].lastIndexOf('/');
					int id2 = slMainFilesList[0].lastIndexOf('/', id1 - 1);
					sName    = slMainFilesList[0].mid(id2 + 1, id1 - id2 - 1);
				}
				/*else {
					name = mainFilesList[0];
					name.replace("Status of ", " ");
					name = name.left(name.indexOf(':'));
				}*/
			}
		}

		if (m_bGettingURL)
			slItemList = slFilesList[i].split(QRegExp("( )+"));
		else
			slItemList = slMainFilesList[i].split(QRegExp("( )+"));

		isDir          = (slItemList[0].at(0) == 'd');
		isSymLink      = (slItemList[0].at(0) == 'l');
		nSize           =  slItemList[4].toLongLong();
		sPermissionStr =  slItemList[0].right(9);
		sOwner         =  slItemList[2];
		sGroup         =  slItemList[3];
		// --- check file is readable and executable
		if (slItemList[2] == m_currentUri.user()) {
			isReadable   = (slItemList[0].at(1) == 'r'); // user can to read
		}
		else {
			isReadable   = (slItemList[0].at(7) == 'r'); // other can to read
		}
		// --- below part come from the QFtp class (Qt-3.2.3) (little modified by qtcmd's author)
		// --- set date and time
		QString dateStr = "Sun "+ slItemList[ 5 ]+" "+ slItemList[ 6 ]+" ";

		if (slItemList[ 7 ].contains( ":" )) {
			time = QTime( slItemList[ 7 ].left( 2 ).toInt(), slItemList[ 7 ].right( 2 ).toInt() );
			dateStr += QString::number( QDate::currentDate().year() );
		} else {
			dateStr += slItemList[ 7 ];
		}
		date = QDate::fromString(dateStr);
		lastModified = QDateTime(date, time);

		if (slItemList[ 7 ].contains( ":" )) {
			if (lastModified.secsTo( QDateTime::currentDateTime() ) < -nFutureTolerance) {
				QDateTime dt = lastModified;
				QDate d = dt.date();
				d.setDate(d.year()-1, d.month(), d.day());
				dt.setDate(d);
				lastModified = dt;
			}
		}
		// --- ---

		if (m_bGettingURL) // get file name from first line
			sName = slFilesList[0].mid( 10, slFilesList[0].length()-(1+10) ); // 14 - begin pos.of name in first line
		else // is listing, check current link
		if (m_slTargetFileForLinkLst.count() > 0 && isSymLink) {
			QStringList itemLst;
			for (int i=0; i < m_slTargetFileForLinkLst.count(); i++) {
				itemLst = m_slTargetFileForLinkLst[i].split('\n');
				if (sName == itemLst[0]) {
					isDir = (itemLst[2] == "dir");
					break;
				}
			}
		}
		sMime = MimeType::getFileMime(sName, (m_status == CREATED_DIR));
		pFI = new FileInfo(sName, m_currentUri.path(), nSize, isDir, isSymLink, lastModified, sPermissionStr, sOwner, sGroup, sMime);
		if (m_status == CREATED_DIR) {
			// required correction because stat returns the inside of checked directory, so here item is: ".."
			pFI->setData(COL_NAME, m_preProcessedFiles->at(m_rawCmdCount)->fileName());
			pFI->setData(COL_MIME, "folder");
		}
// 		qDebug() << "FtpSubsystem::statCmdParsing. FileInfo obj.created:" << pFI <<  << pFI->fileName();
		if (m_bGettingURL || m_bReadFile) {
			if (m_bReadFile)
				m_bFileIsReadable = isReadable;
			else
			if (m_bLinkChecking) {
				QString sLinkItem = m_slTargetFileForLinkLst[m_nLinkCheckingCounter];
				sLinkItem += (isDir) ? "\ndir" : "\nfile";
				m_slTargetFileForLinkLst[m_nLinkCheckingCounter] = sLinkItem;
				m_nLinkCheckingCounter++;
				if (m_nLinkCheckingCounter == m_slTargetFileForLinkLst.count()) {
					m_bGettingURL   = false;
					m_bLinkChecking = false;
					bRunListingFilesAgain = true;
				}
			}
			break;
		}
	} // for
	// code == 213 - SIZE command
	if (bRunListingFilesAgain) {
		slotRawCommandReply(211, QString());
		return NULL;
	}
	if (m_bGettingURL || m_bLinkChecking)
		return NULL;

	return pFI;
}

void FtpSubsystem::makeStatOnListedLinks()
{
	m_rawCmdCount = 0;
	foreach(FileInfo *pFI, m_listedlinksList) {
		m_pFtp->rawCommand("STAT " + pFI->fileName());
		//qDebug() << "FtpSubsystem::makeStatOnListedLinks. Called STAT command on QFtp instance for following file:" << pFI->fileName();
	}
}

void FtpSubsystem::checkAllLinks()
{
	m_bGettingURL = true;
	m_nLinkCheckingCounter = 0;
	QStringList slItemList;

	for (int i=0; i < m_slTargetFileForLinkLst.count(); i++) {
		slItemList = m_slTargetFileForLinkLst[i].split('\n');
		m_pFtp->rawCommand("STAT " + slItemList[i]); // need to check all target files
		qDebug() << "FtpSubsystem::checkAllLinks. Called STAT command on QFtp instance for following file:" << slItemList[i];
		//m_ftp->rawCommand( "STAT "+(mNameOfProcessedFile=itemList[1]) ); // need to check all target files
		//m_processedFiles->append(itemList[1]); // must be FileInfo in param
	}
}

int FtpSubsystem::indexOfNameInStat( const QString &sRow )
{
	if (sRow.isEmpty())
		return 0;

	int i = 1, sepCount = 0;
	while (i < sRow.length() && sepCount < 8 && sRow[i] != ' ') {
		++i;
		if (sRow[i] == ' ') {
			sepCount++;
			while (sRow[i] == ' ') i++;
		}
	}

	return i;
}

void FtpSubsystem::updateContextMenu( ContextMenu *pContextMenu, KeyShortcuts *pKS )
{
	qDebug() << "FtpSubsystem::updateContextMenu.";
	m_pContextMenu = pContextMenu;

	m_pContextMenu->setVisibleAction(CMD_Archive_SEP, false);
	m_pContextMenu->setVisibleAction(CMD_ExtractTo, false);
	m_pContextMenu->setVisibleAction(CMD_ExtractHere, false);

	m_pContextMenu->setVisibleAction(CMD_Makelink, false);
	m_pContextMenu->setVisibleAction(CMD_EditSymlink, false);
}

void FtpSubsystem::setBreakOperation()
{
	if (m_status == LISTED || m_pFtp->state() == QFtp::LoggedIn || m_pFtp->state() == QFtp::Unconnected)
		return;
	qDebug() << "FtpSubsystem::setBreakOperation.";
	m_status = OPR_STOPPED;
	emit signalStatusChanged(m_status, m_sRequestingPanel);
}

void FtpSubsystem::cancelCurrentOperation()
{
	m_bOperationCanceled = true;
	m_bOperationFinished = true;
	m_status = OPR_STOPPED;
	m_cmdQueue.clear();
	m_pFtp->clearPendingCommands();
	m_pFtp->abort();
}

void FtpSubsystem::updateFtpListedItems( Vfs::SubsystemCommand command )
{
	QString sFN = "FtpSubsystem::updateFtpListedItems.";
	qDebug() << sFN << "Command:" << toString(command);

	FileInfo *pFI = NULL;
	if (command == REMOVE) {
		pFI = m_preProcessedFiles->at((m_status == CREATING_FILE) ? m_nOpenWithCounter : m_removedCounter);
		if (! m_fiFtpListedItems.removeOne(pFI)) {
			qDebug() << sFN << "Came foreign pointer. Try remove by name.";
			foreach (FileInfo *pFItmp, m_fiFtpListedItems) {
				if (pFItmp->fileName() == pFI->fileName()) {
					m_fiFtpListedItems.removeOne(pFItmp);
					break;
				}
			}
		}
	}
	else
	if (command == CREATE_DIR || command == CREATE_FILE || command == COPY || command == MOVE) {
		int idx = (command == COPY || command == MOVE) ? m_nCopyCounter : m_rawCmdCount;
		if (command == CREATE_DIR || command == CREATE_FILE)
			pFI = m_processedFiles->at(idx);
		else { // command == COPY || command == MOVE
			if (m_status == UPLOADED)
				pFI = m_processedFiles->at(idx);
			else
				pFI = m_preProcessedFiles->at(idx);
		}
		m_fiFtpListedItems.append(pFI);
	}
	else
	if (command == RENAME) {
		bool bFound = false;
		QString sOldRenamedName = m_slItemsToModify.at(0);
		foreach (pFI, m_fiFtpListedItems) {
			if (pFI->fileName() == sOldRenamedName)
				break;
		}
		if (bFound)
			pFI->setData(COL_NAME, m_processedFiles->at(0)->fileName());
	}

	if (pFI != NULL)
		qDebug() << sFN << "Updated item:" << pFI->fileName();// << "pointer:" << pFI;
	else
		qDebug() << sFN << "Not updated any item due to cannot find it.";
}

bool FtpSubsystem::handledOperation( SubsystemCommand sc )
{
	if (sc == FIND || sc == CREATE_LINK || sc == EDIT_LINK)
		return false;

	return true;
}

bool FtpSubsystem::isLostConnection( const QString &sTitle, const QString &sRequestingPanel )
{
	if (m_pFtp->state() != QFtp::LoggedIn) {
		m_error = CANNOT_READ;
		emit signalStatusChanged(Vfs::ERROR_OCCURED, sRequestingPanel); // to be able close progress dialog
		bool bFake = false;
		int nResult = MessageBox::yesNo(m_pParent, sTitle, tr("Host lost connection with")+" "+m_currentUri.host(),
									tr("There is no possible to continue this operation.")+"\n"+
									tr("Do you want to reconnect?"),
									bFake,
							  MessageBox::Yes // default focused button
		);
		if (nResult == MessageBox::Yes) {
			m_error = NO_ERROR;
			open(m_currentUri, m_sRequestingPanel);
		}
		return true;
	}

	return false;
}

QString FtpSubsystem::fullHostName() const
{
	QString sFullHostName = m_currentUri.protocolWithSuffix() + m_currentUri.host() + "/";
	return sFullHostName;
}


// ------------ SLOTs ----------------

void FtpSubsystem::slotRunNextCommand()
{
	QString sFN = "FtpSubsystem::slotRunNextCommand.";
	if (m_cmdQueue.count() == 0) {
		qDebug() << sFN << "Empty command's queue. Exit.";
		return;
	}
	m_currentCommand = m_cmdQueue.first();
	qDebug() << sFN << "currentCommand:" << toString(m_currentCommand) << "cmdQueue.count:" << m_cmdQueue.count();
	m_cmdQueue.pop_front();

	if      (m_currentCommand == DISCONNECT) {
		m_pFtp->close();
	}
	else if (m_currentCommand == CONNECT) {
		m_pFtp->connectToHost(m_currentUri.host(), m_currentUri.port(m_currentUri.port()));
	}
	else if (m_currentCommand == LOGIN_CMD) {
		if (! m_currentUri.user().isEmpty())
			m_pFtp->login(QUrl::fromPercentEncoding(m_currentUri.user().toLatin1()), m_currentUri.password());
		else
			m_pFtp->login();
	}
	else if (m_currentCommand == CD) {
		if (! m_currentUri.path().isEmpty())
			m_pFtp->cd(m_currentUri.path());
	}
	else if (m_currentCommand == LIST) {
		m_pFtp->list();
	}
	else if (m_currentCommand == REMOVE) {
		if (m_status == ERROR_OCCURED)
			removeAfterErr(&m_selectionMap, true);
	}
	else if (m_currentCommand == COPY || m_currentCommand == MOVE) {
		m_bRenameTargetFile = false;
		FileInfo *pProcessedFI = m_preProcessedFiles->at(m_nCopyCounter);
		QString sProcessedFileName = pProcessedFI->fileName();
		bool bProcessedIsDir = pProcessedFI->isDir();
		if (m_remoteOperation == DOWNLOAD) {
			m_status = DOWNLOADING;
			if (updateTargetFileInLS(true)) {
				if (bProcessedIsDir) {
					if (m_cmdQueue.count() > 0)
						QTimer::singleShot(5, this, &FtpSubsystem::slotRunNextCommand);
					return;
				}
				m_pFtp->get(sProcessedFileName);
				qDebug() << sFN << "Called GET command on QFtp instance for following file:" << sProcessedFileName;
			}
			else {
				qDebug() << sFN << "Skip the DOWNLOAD of file:" << sProcessedFileName;
				slotCommandFinished(QFtp::Get, false); // calling required only for correct update of counters and flags, below
			}
		}
		else { // m_remoteOperation == UPLOAD
			m_status = UPLOADING;
			// check if current file already exists in target (remote) directory in this order is searching listed items (m_fiListedItems)
			FileInfo *pRemoteFI = fileInfo(sProcessedFileName);
			if (pRemoteFI != NULL) { // item exists
				if (bProcessedIsDir) {
					return;
				}
				if (sProcessedFileName == pRemoteFI->fileName()) {
					if (! m_bAlwaysOverwriteSelected) { // if user select All in overwrite dialgo then it has true value
						Settings settings;
						m_bAlwaysOverwrite = settings.value("vfs/AlwaysOverwrite", false).toBool();
					}
					if (m_bAlwaysOverwrite || m_bAlwaysOverwriteSelected) {
						m_pFtp->remove(sProcessedFileName);
						qDebug() << sFN << "Called REMOVE command on QFtp instance for following file:" << sProcessedFileName;
					}
					else {
						if (! isNeedToOverwriteFile(pProcessedFI->absoluteFileName(), pRemoteFI))  // don't upload
							return;
					}
				}
			}
			if (bProcessedIsDir) {
				m_pFtp->mkdir(sProcessedFileName);
				qDebug() << sFN << "Called MKDIR command on QFtp instance for following file:" << sProcessedFileName;
				return;
			}
			if (! m_sTargetFileName.isEmpty())
				sProcessedFileName = QFileInfo(m_sTargetFileName).fileName();

			m_pUploadFile = new QFile(m_preProcessedFiles->at(m_nCopyCounter)->absoluteFileName()); // file in local subsystem
			m_pUploadFile->open(QFile::ReadOnly);
			m_pFtp->put(m_pUploadFile, sProcessedFileName); // default: TransferType type = Binary
			qDebug() << sFN << "Called PUT command on QFtp instance for following file:" << sProcessedFileName << "opened in LS:" << m_preProcessedFiles->at(m_nCopyCounter)->absoluteFileName();
		}
	}
}

void FtpSubsystem::slotCommandStarted( int )
{
	if (m_bOperationCanceled)
		return;

	QString sFN = "FtpSubsystem::slotCommandStarted.";
	QFtp::Command command = m_pFtp->currentCommand();
	qDebug() << sFN << "QFtp::Command:" << qFtpToString(command) << " QFtp::state:" << qFtpToString(m_pFtp->state()) << " currentCommand:" << Vfs::toString(m_currentCommand);
	FileInfo *pFI;
	bool bCopyOrMoveCmd = (m_currentCommand == COPY || m_currentCommand == MOVE);
	bool bOpenOrViewCmd = (m_currentCommand == OPEN_WITH_CMD || m_currentCommand == READ_FILE_TO_VIEW);

	if      (command == QFtp::Close) {
		m_status = CLOSING;
	}
	else if (command == QFtp::ConnectToHost) {
		m_status = CONNECTING;
	}
	else if (command == QFtp::Login) {
		m_status = LOGIN;
	}
	else if (command == QFtp::Cd) {
		m_status = CHANGING_DIR;
	}
	else if (command == QFtp::List) {
		m_status = LISTING;
		m_listedlinksList.clear();
		// -- clear list and insert top item
		m_fiFtpListedItems.clear();
		m_fiFtpListedItems.append(root());
	}
	else if (command == QFtp::Rename) {
		m_status = RENAMING;
		m_sNameOfProcessedFile = m_slItemsToModify[0];
		qDebug() << sFN << "Start the RENAME of file from:" << m_sNameOfProcessedFile << "to:" << m_processedFiles->at(0)->fileName();
	}
	else if (command == QFtp::Remove || command == QFtp::Rmdir) {
		if (m_currentCommand == REMOVE) {
			// TODO before removing directory we can check if it's empty calling STAT command (only empty directory can be removed)
			m_status = REMOVING; // before could be WEIGHED
			m_sNameOfProcessedFile = m_preProcessedFiles->at(m_preRemovedCounter)->absoluteFileName();
			emit signalNameOfProcessedFile(m_sNameOfProcessedFile); // need for progress dialog
		}
		else {
			if (m_currentCommand == CREATE_FILE)
				pFI = m_preProcessedFiles->at(m_nOpenWithCounter);
			else // m_currentCommand == MOVE
				pFI = m_preProcessedFiles->at(m_nCopyCounter-1);
			m_sNameOfProcessedFile = fullHostName() + m_currentUri.path()+pFI->fileName(); // there is double slash after host
			if (m_status == UPLOADING || m_status == DOWNLOADING)
				emit signalNameOfProcessedFile(pFI->absoluteFileName(), m_sNameOfProcessedFile); // need for progress dialog
		}
		qDebug() << sFN << "Start the REMOVE of file:" << m_sNameOfProcessedFile;
	}
	else if (command == QFtp::Mkdir) {
		int nId = (m_currentCommand == CREATE_DIR) ? m_rawCmdCount : m_nCopyCounter;
		if (m_currentCommand == CREATE_DIR) {
			m_status = CREATING_DIR;
		}
		qDebug() << sFN << "Start the MAKE of dir:" << m_preProcessedFiles->at(nId)->fileName();
	}
	else if (command == QFtp::Get || bCopyOrMoveCmd || command == QFtp::Put) {
		if (m_currentCommand == CREATE_FILE) {
			m_status = CREATING_FILE;
			qDebug() << sFN << "Start the" << toString(m_status) << "of file:" << m_preProcessedFiles->at(m_rawCmdCount)->fileName();
		}
		else {
			m_status = (command == QFtp::Put) ? UPLOADING : DOWNLOADING;
			m_bSkipCopyingCurrentFile = false;
			emit signalStatusChanged(m_status, m_sRequestingPanel); // for update status bar
			if (bOpenOrViewCmd || bCopyOrMoveCmd || command == QFtp::Put) {
				int nDwnCounter = bCopyOrMoveCmd ? m_nCopyCounter : m_nOpenWithCounter;
				pFI = m_preProcessedFiles->at(nDwnCounter); // for RENAME here is changed name
				m_sNameOfProcessedFile = pFI->absoluteFileName();
				emit signalNameOfProcessedFile(m_sNameOfProcessedFile);
				qDebug() << sFN << "Start the" << toString(m_status) << "of file:" << pFI->fileName();
				m_dtCouter = 0;
			}
		}
	}
	else if (command == QFtp::RawCommand) {
		if (m_preProcessedFiles->size() > 0 && (m_currentCommand == CREATE_FILE || m_currentCommand == CREATE_DIR)) {
			qDebug() << sFN << "Start the RawCommand with internal command:" << toString(m_currentCommand) << "for file:" << m_preProcessedFiles->at(m_rawCmdCount)->fileName();
		}
	}
	else
		emit signalStatusChanged(m_status, m_sRequestingPanel); // for showing operation status on status bar
} // slotCommandStarted

void FtpSubsystem::slotCommandFinished( int , bool bError )
{
	if (m_bOperationCanceled)
		return;

	QString sFN = "FtpSubsystem::slotCommandFinished.";
	QFtp::Command command = m_pFtp->currentCommand();
	qDebug() << sFN << "QFtp::Command:" << qFtpToString(command) << " QFtp::state:" << qFtpToString(m_pFtp->state()) << " Error:"<< bError
			 << " QFtpErrorCode:"<< m_pFtp->error() << " currentCommand:" << Vfs::toString(m_currentCommand);

	bool bRemoveCommand = (command == QFtp::Remove || command == QFtp::Rmdir);
	bool bCopyOrMoveCmd = (m_currentCommand == COPY || m_currentCommand == MOVE);
	bool bOpenOrViewCmd = (m_currentCommand == OPEN_WITH_CMD || m_currentCommand == READ_FILE_TO_VIEW);
	FileInfo *pFI = NULL;

	if (! bError && m_status != ERROR_OCCURED) {
		if      (command == QFtp::Close) {
			m_status = CLOSED;
		}
		else if (command == QFtp::ConnectToHost) {
			m_status = CONNECTED;
		}
		else if (command == QFtp::Login) {
			m_status = LOGGED_IN;
		}
		else if (command == QFtp::Cd) {
			m_status = CHANGED_DIR;
			m_slTargetFileForLinkLst.clear();
			m_bLinkChecking = false;
			m_bGettingURL   = false;
			//qDebug() << "  -- directory changed to:" << m_currentUri.path();
		}
		else if (command == QFtp::List) {
			m_status = LISTED;
			makeStatOnListedLinks();
		}
		else if (command == QFtp::Rename) {
			m_status = RENAMED;
			updateFtpListedItems(m_currentCommand);
		}
		else if (bRemoveCommand) {
			pFI = m_preProcessedFiles->at(m_preRemovedCounter);
			if (m_status == REMOVING) {
				bool isSymLink, isFile = false;
				m_error  = NO_ERROR;
				m_status = REMOVED;

				if ((isSymLink= pFI->isSymLink())) {
					m_rmFilesCounter++;
					//qfi.setFile(fi->absoluteFilePath());
					//rmWeight += qfi.readLink().length(); // resolve link
				}
				else
				if (pFI->isDir()) {
					m_rmDirsCounter++;
				}
				else {
					isFile = true;
					m_rmFilesCounter++;
					m_removedWeight += pFI->size();
				}
				m_processedFiles->append(pFI);
				updateFtpListedItems(m_currentCommand);
				m_preRemovedCounter++;
				m_removedCounter++;
				emit signalCounterProgress((isFile || isSymLink) ? m_rmFilesCounter : m_rmDirsCounter, (isFile || isSymLink) ? FILE_COUNTER : DIR_COUNTER); // set counter for file or directory
				emit signalTotalProgress((100 * m_removedCounter)/m_nRmTotalFiles, m_nRmTotalFiles);
				emit signalDataTransferProgress(m_removedWeight, m_totalWeight, 0, true);
				emit signalOperationFinished(); // updates label of Cancel button (from 'Cancel' to "Done") and makes it focused
			}
			if (m_currentCommand == MOVE || m_currentCommand == CREATE_FILE) {
				if (m_status == DOWNLOADING || m_status == CREATING_FILE)
					updateFtpListedItems(REMOVE); // update inernal list containing listed items
				if (m_currentCommand != CREATE_FILE) {
					m_removedCounter++; // for properly update Internal listed item (uploading)
					m_preRemovedCounter++; // for properly set flag: m_bOperationFinished (downloading)
				}
				m_bOperationFinished = (m_preRemovedCounter == m_preProcessedFiles->size());
				emit signalStatusChanged(Vfs::REMOVED, m_sRequestingPanel); // update file list view
			}
			qDebug() << sFN << "File removed:" << pFI->fileName();// << " m_preRemovedCounter" << m_preRemovedCounter << "m_removedCounter:" << m_removedCounter;
		} // if (bRemoveCommand)
		else if (command == QFtp::Mkdir) {
			if (m_status == CREATING_DIR) {
				m_status = CREATED_DIR;
				return;
			}
			else
			if (m_status == UPLOADING) {
				m_status = UPLOADED;
				pFI = m_preProcessedFiles->at(m_nCopyCounter);
				QString sSrcDir = pFI->absoluteFileName();
				QString sTargetPath = m_sCopyTargetPath.mid(fullHostName().length(), (m_sCopyTargetPath.length()-fullHostName().length()));
				pFI->setData(2, sTargetPath, true); // workaround for setting path (for second panel we need to set correct path for new item)
				QString sFullFileName = fullHostName() + m_currentUri.path() + pFI->fileName();
				emit signalNameOfProcessedFile(sSrcDir, sFullFileName);
				emit signalCounterProgress(++m_nDirsCouter, DIR_COUNTER, m_bLOCAL_PROGRESS);
				int nTotalItems = m_nFilesCouter + m_nDirsCouter;
				emit signalTotalProgress((100 * nTotalItems) / m_nSelectedFiles, m_nSelectedFiles);
				emit signalDataTransferProgress(0, 0, 1, m_bLOCAL_PROGRESS);
				qDebug() << sFN << "Directory created:" << sFullFileName;
				m_processedFiles->append(pFI);
				m_nCopyCounter++;
			}
		}
		else if (command == QFtp::Put) {
			if (m_status == CREATING_FILE) {
				m_status = CREATED_FILE;
				return;
			}
			else
			if (m_status == UPLOADING) {
				bool bSkipFile = false;
				if (! m_bSkipCopyingCurrentFile) {
					pFI = m_preProcessedFiles->at(m_nCopyCounter);
					FileInfo *pNewFI = new FileInfo(pFI);
					pNewFI->setData(COL_TIME, QDateTime().currentDateTime());
					QString sTargetPath = m_sCopyTargetPath.mid(fullHostName().length(), (m_sCopyTargetPath.length()-fullHostName().length()));
					pNewFI->setData(2, sTargetPath, true); // workaround for setting path (for second panel we need to set correct path for new item)
					if (m_bRenameTargetFile)  // update name, there was rename action in overwrite dialog
						pNewFI->setData(COL_NAME, QFileInfo(m_sTargetFileName).fileName());
					m_processedFiles->append(pNewFI); // useful for update file list view
					m_nBytesCopiedTotaly += pFI->size();
					m_sNameOfProcessedFile = pFI->absoluteFileName(); // need to be abs.path due to later might be to use with removing
					updateFtpListedItems(m_currentCommand); // need to do for next upload operation would be correct hanled
					emit signalCounterProgress(++m_nFilesCouter, FILE_COUNTER, m_bLOCAL_PROGRESS);

					// moved into slotDataTransferProgress(), thanks that showing progress is smooth
					//emit signalTotalProgress((100 * m_nCopyCounter) / m_nSelectedFiles, m_nCopyTotalFilesWeigh);
					//emit signalDataTransferProgress(pFI->size(), pFI->size(), pFI->size(), m_bLOCAL_PROGRESS);
					//emit signalDataTransferProgress(m_nBytesCopiedTotaly, m_nCopyTotalFilesWeigh, 0, m_bTOTAL_PROGRESS);
					m_nCopyCounter++;
					qDebug() << sFN << "QFtp::Put command. File uploaded:" << pFI->fileName() << " CopyCounter:" << m_nCopyCounter << "BytesCopiedTotaly:" << m_nBytesCopiedTotaly;
				}
				else { // skip file
					bSkipFile = true;
					m_nCopyCounter++;
					qDebug() << sFN << "QFtp::Put command. Skip file:" << pFI->fileName() << " CopyCounter:" << m_nCopyCounter;
				}
				// -- finanlizing move action during uploading
				if (m_currentCommand == MOVE && ! bSkipFile) {
					if (! QFile(m_sNameOfProcessedFile).remove()) {
						m_error  = CANNOT_REMOVE;
						m_status = ERROR_OCCURED;
					}
				}
				// -- cleaning
				if (! m_bSkipCopyingCurrentFile) {
					m_pUploadFile->close();
					delete m_pUploadFile;  m_pUploadFile = NULL;
					if (m_currentCommand != MOVE)
						delete m_preProcessedFiles->at(m_nCopyCounter-1); // obj.has been duplicated above
				}
			} // m_status == UPLOADING
		} // command == QFtp::Put
		else if (command == QFtp::Get || bCopyOrMoveCmd) {
			if (bOpenOrViewCmd) {
				m_nOpenWithCounter++;
				qDebug() << sFN << "QFtp::Get command. File downloaded:" << m_preProcessedFiles->at(m_nOpenWithCounter-1)->fileName() << " OpenWithCounter:" << m_nOpenWithCounter;
			}
			else
			if (bCopyOrMoveCmd) {
				if (! m_bSkipCopyingCurrentFile) {
					pFI = m_preProcessedFiles->at(m_nCopyCounter);
					pFI->setData(2, m_sCopyTargetPath, true); // workaround for setting path (for second panel we need to set correct path for new item)
					m_nBytesCopiedTotaly += pFI->size();
					m_sNameOfProcessedFile = pFI->absoluteFileName();
					if (pFI->isDir() && ! pFI->isSymLink())
						emit signalCounterProgress(++m_nDirsCouter, DIR_COUNTER,  m_bLOCAL_PROGRESS);
					else
						emit signalCounterProgress(++m_nFilesCouter, FILE_COUNTER, m_bLOCAL_PROGRESS);

					// commented due to this is done in slotDataTransferProgress() and thanks that showing progress is smooth
					//emit signalTotalProgress((100 * m_nCopyCounter) / m_nSelectedFiles, m_nCopyTotalFilesWeigh);
					//emit signalDataTransferProgress(pFI->size(), pFI->size(), pFI->size(), m_bLOCAL_PROGRESS);
					//emit signalDataTransferProgress(m_nBytesCopiedTotaly, m_nCopyTotalFilesWeigh, 0, m_bTOTAL_PROGRESS);
					m_nCopyCounter++;
					qDebug() << sFN << "QFtp::Get command. File downloaded. CopyCounter:" << m_nCopyCounter << "BytesCopiedTotaly:" << m_nBytesCopiedTotaly;
				}
				else { // skip file
					m_nCopyCounter++;
					qDebug() << sFN << "QFtp::Get command. File skipped. CopyCounter:" << m_nCopyCounter;
				}
			} // bCopyOrMoveCmd
		} // command == QFtp::Get
		else if (command == QFtp::RawCommand) {
			qDebug() << sFN << "RawCommand. Status:" << toString(m_status) << " rawCmdCount" << m_rawCmdCount << " processedFilesCount" << m_processedFilesCount;
			if (m_status == CREATED_FILE || m_status == CREATED_DIR) {
				updateFtpListedItems(m_currentCommand);
				delete m_preProcessedFiles->at(m_rawCmdCount);
				if (m_status == CREATED_FILE)
					QFile(m_sTmpDir + m_processedFiles->at(m_rawCmdCount)->fileName()).remove(); // removing file from temporary directory
			}
			m_rawCmdCount++;
			if (m_status == LISTED && ! m_listedlinksList.isEmpty()) { // if any symlinks have been listed then data model on list view have to be updated when last STAT command is finished - due to sorting items issue
				if (m_rawCmdCount == m_listedlinksList.size())
					emit signalStatusChanged(m_status, m_sRequestingPanel);
			}
			if (m_rawCmdCount < m_processedFilesCount) // signal: signalStatusChanged must be called one time
				return;
		}

		if (m_status == UPLOADING && bRemoveCommand) // appeared overwriting in UPLOAD operation, there is queued PUT command so need to quit now
			return;

		if (m_cmdQueue.count() > 0)
			QTimer::singleShot(5, this, &FtpSubsystem::slotRunNextCommand);
	}
	else { // an error occured
		QFtp::Error ftpError = m_pFtp->error();

		if      (m_status == CHANGING_DIR || command == QFtp::Cd) {
			m_sNameOfProcessedFile = m_currentUri.path();
			if (ftpError == QFtp::UnknownError)
				m_error = CANNOT_OPEN;
		}
		else if (m_status == CONNECTING || command == QFtp::ConnectToHost) {
			m_error = CANNOT_CONNECT;
		}
		else if (m_status == CLOSING) {
			if (ftpError == QFtp::NotConnected) {
				qDebug() << sFN << "Connection already not active!";
				m_status = CLOSED;
				if (m_cmdQueue.count() > 0)
					QTimer::singleShot(5, this, &FtpSubsystem::slotRunNextCommand);
				return;
			}
		}
		else if (command == QFtp::Login) {
			m_error = CANNOT_LOGIN;
		}
		else if (command == QFtp::Put) {
			if (m_status == CREATING_FILE) {
				m_error = CANNOT_CREATE_FILE;
				for (int i = 0; i < processedFiles()->size(); ++i)
					QFile(m_sTmpDir + processedFiles()->at(i)->fileName()).remove();
			}
			else
			if (m_status == UPLOADING) { // user is not able to write into selected directory, break operation
				m_sNameOfProcessedFile = m_currentUri.path();
				m_error = CANNOT_WRITE;
				if (m_pUploadFile != NULL) {
					if (m_pUploadFile->isOpen())
						m_pUploadFile->close();
					delete m_pUploadFile;
				}
				m_bOperationFinished = true;
				emit signalStatusChanged(m_status, m_sRequestingPanel);
			}
			else
				m_error = CANNOT_PUT;
		}
		else if (command == QFtp::Get) { // m_status == DOWNLOADING
			if (m_status == ERROR_OCCURED)
				m_error = CANNOT_GET;
		}
		else if (bRemoveCommand) { // (command == QFtp::Remove || command == QFtp::Rmdir)
			pFI = m_preProcessedFiles->at(m_preRemovedCounter - 1);
			m_sNameOfProcessedFile = m_currentUri.protocolWithSuffix() + m_currentUri.host();
			if (m_currentUri.path().isEmpty())
				m_sNameOfProcessedFile += "/";
			m_sNameOfProcessedFile += pFI->absoluteFileName();
			if (pFI->isSymLink())
				qDebug() << sFN << "Cannot remove symlink:" << m_sNameOfProcessedFile;
			else
			if (pFI->isDir())
				qDebug() << sFN << "Cannot remove dir:" << m_sNameOfProcessedFile;
			else
				qDebug() << sFN << "Cannot remove file:" << m_sNameOfProcessedFile;

			if (! bCopyOrMoveCmd) // only REMOVE command
				m_processedFiles->append(pFI); // useful for message error (for REMOVE command)
// 			if (m_currentCommand == MOVE)
// 				delete m_preProcessedFiles->at(m_preRemovedCounter); // made CRASH, because obj.is used later
			m_error = CANNOT_REMOVE;
			m_preRemovedCounter++;
		}
		else if (command == QFtp::Mkdir) {
			m_error = CANNOT_CREATE_DIR; // QFtp::UnknownError when trying create already exists directory
			delete m_preProcessedFiles->at(m_rawCmdCount);
			m_rawCmdCount++;
		}
		else if (command == QFtp::Rename) {
			m_error = CANNOT_RENAME;
		}
		else if (command == QFtp::RawCommand) {
			m_error = OTHER_ERROR;
			if (m_status == CREATED_FILE) {
				QFile(m_sTmpDir + m_preProcessedFiles->at(m_rawCmdCount)->fileName()).remove();
				delete m_preProcessedFiles->at(m_rawCmdCount);
				m_error = CANNOT_CREATE_FILE;
				m_rawCmdCount++;
			}
		}
		else if (command == QFtp::Cd || command == QFtp::List) {
			m_error = CANNOT_OPEN;
		}
		m_cmdQueue.clear();
		m_status = ERROR_OCCURED;
		qDebug() << sFN << "An error occured. QFtpErrorCode:"<< ftpError << " m_error:" << m_error << " errorStr.:"<< m_pFtp->errorString()
				 << "\n\tstate:"<< qFtpToString(m_pFtp->state());

		if (bRemoveCommand) { // * workaround W1, because QFtp clean commands queue and calls abort when first error occures
			if (m_status == UPLOADING) { // failed upload due to no write permission in target directory
				// there is queued PUT operation and no write permission means no possibility to continue an operation, so need to drop all QFtp operation
				m_pFtp->clearPendingCommands();
				m_pFtp->abort();
				m_bOperationFinished = true;
			}
// 			if (m_remoteOperation != UPLOAD)
// 				m_cmdQueue.append(REMOVE);
// 			QTimer::singleShot(10, this, &FtpSubsystem::slotRunNextCommand); // calls removeAfterErr with copied in remove(), FileInfoToRowMap obj.
		}
	} // -- an error occured

	if (bRemoveCommand && m_currentCommand != CREATE_FILE) {
		m_selectionMap.erase(m_selectionMap.begin());  // * workaround W1, because QFtp clean commands queue and do abort after first error occured
		m_bOperationFinished = (m_preRemovedCounter == m_preProcessedFiles->size());
	}
	else
	if (command == QFtp::Get || bCopyOrMoveCmd) {
		if (m_status == ERROR_OCCURED) {
			if (bOpenOrViewCmd)
				m_nOpenWithCounter++;
			else
			if (bCopyOrMoveCmd)
				m_nCopyCounter++;
			emit signalStatusChanged(m_status, m_sRequestingPanel); // for showing error message
			if (m_bOperationFinished) {
				m_processedFiles->clear();
				m_preProcessedFiles->clear();
				return;
			}
		}

		if (bOpenOrViewCmd || bCopyOrMoveCmd) {
			if (bOpenOrViewCmd)
				m_bOperationFinished = (m_nOpenWithCounter == m_preProcessedFiles->size());
			else {
				if (m_currentCommand != MOVE || bRemoveCommand)
					m_bOperationFinished = (m_nCopyCounter == m_preProcessedFiles->size());
			}

			qDebug() << sFN << "Command:" << Vfs::toString(m_currentCommand) << " OperationFinished:" << m_bOperationFinished;
			if (m_status == ERROR_OCCURED)
				return;

			if (m_status == DOWNLOADING) {
				if (m_newDwnFile.isOpen())
					m_newDwnFile.close();

				if (bCopyOrMoveCmd && (! m_bSkipCopyingCurrentFile)) {
					Settings settings;
					bool bSaveAttributes = settings.value("vfs/SaveAttributes", true).toBool(); // default value is true
					if (bSaveAttributes)
						updateFileAttributesInLS(m_newDwnFile.fileName(), bSaveAttributes, bSaveAttributes);
					else
						updateFileAttributesInLS(m_newDwnFile.fileName(), false, false);
				}
			}
		}
	}

	if (bRemoveCommand && m_preRemovedCounter < m_preProcessedFiles->size() && ! bError)
		return;

	if (bOpenOrViewCmd || bCopyOrMoveCmd) {
		int nCopiedId = m_nCopyCounter - 1;
		if (m_currentCommand == MOVE && m_nCopyCounter > 0 && ! m_bOperationFinished) {
			if (m_status == DOWNLOADING) // copying finished successful, so we can remove file
				m_pFtp->remove(m_processedFiles->at(nCopiedId)->fileName());
			else
			if (m_status == UPLOADING) {
				if (QFile(m_preProcessedFiles->at(nCopiedId)->absoluteFileName()).remove()) {
					m_error  = CANNOT_REMOVE;
					m_status = ERROR_OCCURED;
					m_sNameOfProcessedFile = m_preProcessedFiles->at(nCopiedId)->absoluteFileName();
				}
				if (m_currentCommand == MOVE)
					m_bOperationFinished = true;
				emit signalStatusChanged(m_status, m_sRequestingPanel); // for updating list view and alt. showing error message
				delete m_preProcessedFiles->at(m_nCopyCounter - 1); // due to this obj.has been duplicated above
			}
		}
// 		if (m_status != ERROR_OCCURED && m_currentCommand != MOVE)
// 			delete m_preProcessedFiles->at(nCopiedId);  // made CRASH, because obj.is used later

		if (m_bOperationFinished) {
			if (m_currentCommand == OPEN_WITH_CMD)
				m_status = OPEN_WITH;
			else
			if (m_currentCommand == READ_FILE_TO_VIEW)
				m_status = DOWNLOADED_TO_VIEW;
			else
			if (bCopyOrMoveCmd) {
				if (m_status == DOWNLOADING)
					m_status = DOWNLOADED;
				else
				if (m_status == UPLOADING)
					m_status = UPLOADED;
			}
			qDebug() << sFN << "Command:" << Vfs::toString(m_currentCommand) << "Operation fninished, status:" << toString(m_status);
			emit signalOperationFinished(); // updates label of Cancel button (from 'Cancel' to "Done") and makes it focused
		}

		bool bGettingFinished = (bOpenOrViewCmd) ? (m_nOpenWithCounter < processedFiles()->size()) : (m_nCopyCounter < processedFiles()->size());
		if ((command == QFtp::Get || bCopyOrMoveCmd) && bGettingFinished && ! bError) // don't finish till all files will not be downloaded
			return;
	}

	if (! bCopyOrMoveCmd) {
		if (m_status == LISTED) { // update all items on list view only when there is no one symlink listed
			if (m_listedlinksList.isEmpty())
				emit signalStatusChanged(m_status, m_sRequestingPanel); // perform operation on list view, because command finished
		}
		else
			emit signalStatusChanged(m_status, m_sRequestingPanel); // perform operation on list view, because command finished
	}
	else { // COPY or MOVE
		if (m_bOperationFinished)
			emit signalStatusChanged(m_status, m_sRequestingPanel); // perform operation on list view, because command finished
	}
	if (m_bOperationFinished && m_error == NO_ERROR) {
		m_processedFiles->clear();
		m_preProcessedFiles->clear();
	}
} // slotCommandFinished


void FtpSubsystem::slotListInfo( const QUrlInfo &inUrlInfo )
{
//	qDebug() << "FtpSubsystem::slotListInfo";
	QString sName = inUrlInfo.name();
	bool    isDir = inUrlInfo.isDir();
	bool    isSymLink = inUrlInfo.isSymLink();
	QString sDirName  = m_currentUri.path();
	if (sDirName.startsWith('/'))
		sDirName.remove(0,1);

	QString sPermissions = permissionsToString(inUrlInfo.permissions(), isDir);
	sName = QString::fromLocal8Bit(sName.toLatin1().data());
	QString sMime = MimeType::getFileMime(sName, isDir, isSymLink);

	FileInfo *pItem = new FileInfo(sName, sDirName, inUrlInfo.size(), isDir, isSymLink, inUrlInfo.lastModified(), sPermissions, inUrlInfo.owner(), inUrlInfo.group(), sMime);
	m_fiFtpListedItems.append(pItem);
	if (pItem->isSymLink())
		m_listedlinksList.append(pItem);
}

void FtpSubsystem::slotDataTransferProgress( qint64 done, qint64 total )
{
// 	if (done == 0) // only for one time call
// 		qDebug() << "FtpSubsystem::slotDataTransferProgress. done:" << done << "total:" << total << "currentCommand:" << toString(m_currentCommand) << "qFtp_currentCommand:" << qFtpToString(m_pFtp->currentCommand());
	if ((m_pFtp->currentCommand() == QFtp::Get || m_pFtp->currentCommand() == QFtp::Put) && ! m_bSkipCopyingCurrentFile) {
		int nTransferedBytes = 0;
		if (done > 0) {
			m_dtCouter++;
			nTransferedBytes = done/m_dtCouter;
		}

		long long nDoneTotal = (m_nBytesCopiedTotaly + done);
// 		qDebug() << "FtpSubsystem::slotDataTransferProgress. DoneTotal:" << nDoneTotal << "CopyTotalFilesWeigh:" << m_nCopyTotalFilesWeigh;
		if (m_nCopyTotalFilesWeigh == 0) {  // prevent before Arithmetic exception in slotDataTransferProgress when is copied 0 size file
			emit signalDataTransferProgress(0, 0, 1, m_bLOCAL_PROGRESS);
			emit signalTotalProgress(100, m_nCopyTotalFilesWeigh);
			emit signalDataTransferProgress(nDoneTotal, m_nCopyTotalFilesWeigh, 0, m_bTOTAL_PROGRESS);
		}
		else {
			emit signalDataTransferProgress(done, total, nTransferedBytes, m_bLOCAL_PROGRESS);
			emit signalTotalProgress((100 * nDoneTotal) / m_nCopyTotalFilesWeigh, m_nCopyTotalFilesWeigh);
			emit signalDataTransferProgress(nDoneTotal, m_nCopyTotalFilesWeigh, 0, m_bTOTAL_PROGRESS);
		}
		if (m_pFtp->currentCommand() == QFtp::Get)
			updateTargetFileInLS();
	}
}

void FtpSubsystem::slotStateChanged( int state )
{
	//qDebug() << "FtpSubsystem::slotStateChanged" << stateStr(state);
	if (state == QFtp::Unconnected) {
		m_status = UNCONNECTED;
		emit signalStatusChanged(m_status, m_sRequestingPanel); // for showing on status bar
	}
}

void FtpSubsystem::slotRawCommandReply( int nReplyCode, const QString &sDetail )
{
	qDebug() << "FtpSubsystem::slotRawCommandReply. ReplyCode:" << nReplyCode << " currentCommand" << toString(m_currentCommand);
	// https://en.wikipedia.org/wiki/List_of_FTP_commands
	//qDebug() << "detail:" << detail;
	if (nReplyCode == 211 || nReplyCode == 213) { // STAT command
		if (m_status == CREATED_DIR || m_status == CREATED_FILE) {
			m_bGettingURL = false;
			// -- necessary for update data model
			FileInfo *pFI = statCmdParsing(sDetail); // returns newly created FileInfo obj.
			if (pFI != NULL) {
				m_processedFiles->append(pFI);
// 				qDebug() << "FtpSubsystem::slotRawCommandReply. Appended" << pFI->fileName() << "to m_processedFiles. Size:" << m_processedFiles->size();
			}
			else
				qDebug() << "-- not found item";
		}
		else
		if (m_status == LISTED) {
			if (! m_listedlinksList.isEmpty())
				statCmdParsing(sDetail); // returns FileInfo pointer
		}
	}
}

// -------------- HELPER (in debugging) function ------------

QString FtpSubsystem::qFtpToString( QFtp::State state )
{
	QString sState;

	if      (state == QFtp::Unconnected)  sState = tr("Unconnected");
	else if (state == QFtp::HostLookup)   sState = tr("Host lookup");
	else if (state == QFtp::Connecting)   sState = tr("Connecting");
	else if (state == QFtp::Connected)    sState = tr("Connected");
	else if (state == QFtp::LoggedIn)     sState = tr("Logged in");
	else if (state == QFtp::Closing)      sState = tr("Closing");
	else sState = tr("Unknown");

	return sState;
}

QString FtpSubsystem::qFtpToString( QFtp::Command command )
{
	QString sCommand;

	if      (command == QFtp::SetTransferMode)  sCommand = tr("SetTransferMode");
	else if (command == QFtp::SetProxy)  sCommand = tr("SetProxy");
	else if (command == QFtp::ConnectToHost)  sCommand = tr("ConnectToHost");
	else if (command == QFtp::Login)  sCommand = tr("Login");
	else if (command == QFtp::Close)  sCommand = tr("Close");
	else if (command == QFtp::List)  sCommand = tr("List");
	else if (command == QFtp::Cd)  sCommand = tr("Cd");
	else if (command == QFtp::Get)  sCommand = tr("Get");
	else if (command == QFtp::Put)  sCommand = tr("Put");
	else if (command == QFtp::Remove)  sCommand = tr("Remove");
	else if (command == QFtp::Mkdir)  sCommand = tr("Mkdir");
	else if (command == QFtp::Rmdir)  sCommand = tr("Rmdir");
	else if (command == QFtp::Rename)  sCommand = tr("Rename");
	else if (command == QFtp::RawCommand)  sCommand = tr("RawCommand");
	else if (command == QFtp::None)  sCommand = tr("None");

	return sCommand;
}


} // namespace Vfs
