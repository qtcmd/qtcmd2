/***************************************************************************
 *   Copyright (C) 2011 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef FTPSUBSYSTEM_H
#define FTPSUBSYSTEM_H

#include <qftp.h>

#include "subsystem.h" // includes also: QFile QVector QDateTime QStringList QUrlInfo
#include "plugindata.h"
#include "contextmenu.h"
#include "contextmenucmdids.h"


namespace Vfs {

/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class FtpSubsystem : public Subsystem
{
	Q_OBJECT
public:
	FtpSubsystem();
	virtual ~FtpSubsystem();

	SubsystemError   error() const { return m_error; }
	SubsystemStatus  status() const { return m_status; };
	SubsystemCommand currentCommand() const { return m_currentCommand; }

	void          init( QWidget *pParent, bool bInvokeDetection=false );

	FileInfo     *root( bool bEmptyName=false );
	FileInfoList  listDirectory() const;

	/** Returns status for possibility of change directory higher than root directory,
	 * which means quit current subsystem..
	 * @return TRUE if might to quit current subsystem, otherwise FALSE.
	 */
	bool          canUpThanRootDir() const { return true; }

	/** Returns status for possibility of change directory to given name.
	 * Note. This is offline test that rely on checking read and executable permission.
	 * Warning. Method works correctly only for directories in current directory!
	 * @param sInAbsPath absolute path to directory
	 * @return TRUE if directory can change, otherwise FALSE.
	 */
	bool          canChangeDirTo( const QString &sInAbsPath );

	/** Returns status for possibility of view/open for given file name.
	 * Note. This is offline test that rely on checking read permission and ownership.
	 * Warning. Method works correctly only for files in current directory!
	 * @param pFileInfo FileInfo pointer to file
	 * @return TRUE if file is readable, otherwise FALSE.
	 */
	bool          canOpenFileForReading( FileInfo *pFileInfo );

	/** Shows connection dialog or calls command listing gived directory.
	 * @param inUri uri to open
	 */
	void          open( const URI &inUri, const QString &sRequestingPanel, bool bCompleterr=false );

	void          openWith( FileInfoToRowMap *pSelectionMap, const URI &uri, const QString &sRequestingPanel );
	void          readFileToViewer( const QString &sHost, FileInfo *pFileInfo, QByteArray &baBuffer, int &nBytesRead, int nFileLoadMethod, const QString &sRequestingPanel );

	/** Calls disconnect command.
	 */
	void          close( bool bForce=false );

	void          makeDir( const QString &sParentDirPath, const QString &sInNewDirsList, const QString &sRequestingPanel );
	void          makeFile( const QString &sParentDirPath, const QString &sInFileNameList, const QString &sRequestingPanel );
	void          rename( FileInfo *pFileInfoOldName, const QString &sInNewName, const QString &sRequestingPanel );
	void          remove( FileInfoToRowMap *pSelectionMap, const URI &uri, bool bWeighBefore, const QString &sRequestingPanel, bool bForceRemoveAll=false );

	void          copyFiles( FileInfoToRowMap *pSelectionMap, const QString &sTargetPath, bool bMove, bool bWeighBefore, const QString &sRequestingPanel );

	void          removeAfterErr( FileInfoToRowMap *pSelectionMap, bool bWeighBefore ); // * workaround W1

	void          weigh( FileInfoToRowMap *pSelectionMap, const URI &uri, const QString &sRequestingPanel, bool bWeighingAsSubCmd = true, bool bResetCounters = true );
	void          weighItem( FileInfo *pFileInfo, const URI &uri, const QString &sRequestingPanel, bool bClearCounters );
	void          getWeighingResult( qint64 &weight, qint64 &realWeight, uint &files, uint &dirs );

	void          setBreakOperation();

	URI           currentURI() const { return m_currentUri; }

	/** Returns pointer to FileInfo object referencing to given file name in list view.
	 * Warning. Method works correctly only for files/directories in current directory!
	 * @param sInAbsFileName absolute path to file
	 * @return FileInfo class pointer
	 */
	FileInfo     *fileInfo( const QString &sInAbsFileName );

// 	void          executeProgramm( const QString &sAbsFileName ) const {} // not supported by FTP

	/** Returns pointer to the PluginData object.
	 * @return PluginData class pointer.
	 */
	PluginData   *pluginData() { return m_pPluginData; };

	/** Returns FileInfo list containing currently procesed files.
	 * @return FileInfoList class pointer.
	 */
	FileInfoList *processedFiles() { return m_processedFiles; }

	/** Returns name of current processed file.
	 * @return file name (might includes full path)
	 */
	QString       processedFileName() const { return m_sNameOfProcessedFile; }

	/** Returns status of performing for current operation.
	 * @return TRUE if opearation is finished, otherwise FALSE.
	 */
	bool          operationFinished() const { return m_bOperationFinished; }

	/** Returns reference to the list of files name before they are changed (i.e.: before rename)
	 * @return string list
	 */
	QStringList & listOfItemsToModify() { return m_slItemsToModify; }

	/** Makes unvisible some not supported options in current subsystem.
	 * @param pContextMenu ponter to global context menu
	 * @param pKS pointer to KeyShortcuts object
	 */
	void          updateContextMenu( ContextMenu *pContextMenu, KeyShortcuts *pKS=nullptr );

	/** Sets path to working directory. Such directory is usuful in view file (by external or internal viewer).
	 * @param sDirectoryPath path to directory
	 */
	void          setWorkingDirectoryPath( const QString &sDirectoryPath ) { m_sTmpDir = sDirectoryPath; }

	/** Returns info about given command is handled in FtpSubsystem.
	 * @param sc Subsystem command id
	 * @return TRUE if given operation is handled otherwise FALSE.
	 */
	bool          handledOperation( SubsystemCommand sc );

	/** Checks if connection was lost. If this is true then start new connection, calling open function.
	 * @param sTitle title message showing in confirmation window
	 * @param sRequestingPanel panel name where from is calling operation
	 * @return TRUE if lost connection, otherwise FALSE.
	 */
	bool          isLostConnection( const QString &sTitle, const QString &sRequestingPanel );

private:
	SubsystemError    m_error;
	SubsystemStatus   m_status;
	SubsystemCommand  m_currentCommand;
	SubsystemCommand  m_remoteOperation;

	ContextMenu      *m_pContextMenu;
	QWidget          *m_pParent;

	/** Convert QFtp::state to string
	 * @param state state of FTP session (QFtp::state)
	 * @return string describing QFtp::state
	 */
	QString        qFtpToString( QFtp::State state );

	/** Convert QFtp::command to string
	 * @param command command in FTP session (QFtp::Command)
	 * @return string describing QFtp::Command
	 */
	QString        qFtpToString( QFtp::Command command );

	/** Updates target file (initializing or appending data) in local subsystem for copy or move operation.
	 * If target file already exists then is showing overwrite dialog, which allows choose operation to do.
	 * Uses m_newDwnFile QFile member to initializing or writing data.
	 * File name is getting from m_preProcessedFiles list, which is indexed by properly counter related with operation (copy/view).
	 * @param bInitOnly performs only initializing (open file operation) of file.
	 * @return TRUE if operation finished successful, otherwise FALSE.
	 */
	bool           updateTargetFileInLS( bool bInitOnly=false );

	/** Updates attributes (permission and time) for given file.
	 * Permissions and time are getting from file getting from m_preProcessedFiles list
	 * which is indexed by properly counter related with operation.
	 * @param sAbsFileName file in local subsystem which need to update attributes.
	 * @param bUpdatePermission update permission
	 * @param bUpdateTime update last modifying time
	 */
	void           updateFileAttributesInLS( const QString &sAbsFileName, bool bUpdatePermission, bool bUpdateTime ); // in LocalSubsystem

	/** Shows file overwrite dialog and return status confirming that overwrite is required.
	 * @param sTargetFullFileName full file name in local subsystem
	 * @param pSrcFileInfo FileInfo pointer to incomming file
	 * @return TRUE if overwrite is required, otherwise FALSE.
	 */
	bool           isNeedToOverwriteFile( const QString &sTargetFullFileName, FileInfo *pSrcFileInfo );

	/** Remove file set in m_newDwnFile.
	 * @return true if operation finished succss otherwise false;
	 */
	bool           removeFileInLS();

	/** Creates instance of QFtp object and makes necessary connections (signal -> slot).
	 */
	void           createQFtpObj();
// 	void           notSupportedOp( SubsystemCommand currentCommand, const QString &sRequestingPanel );

	void           cancelCurrentOperation();

	/** Updates internal list containing listed items (m_fiFtpListedItems).
	 * Using this is recommend for every item modifying (remove, add, change attributes).
	 * @param command command describing what update need to do
	 */
	void           updateFtpListedItems( Vfs::SubsystemCommand command );

	/** Return full host name in format: ftp://HOST_NAME/
	 * @return host name as string
	 */
	QString        fullHostName() const;

	typedef QVector < SubsystemCommand > CommandQueue;
	CommandQueue    m_cmdQueue;

	QFtp           *m_pFtp;
	FileInfoList    m_fiFtpListedItems;
	FileInfoList    m_listedlinksList;
	URI             m_currentUri;

	PluginData     *m_pPluginData;

	FileInfoList   *m_processedFiles;
	FileInfoList   *m_preProcessedFiles;

	FileInfo       *statCmdParsing( const QString &sStatDetails );
	void            checkAllLinks();
	int             indexOfNameInStat( const QString &sRow );
	void            makeStatOnListedLinks();

	bool            m_bGettingURL, m_bLinkChecking, m_bReadFile, m_bFileIsReadable;
	int             m_nLinkCheckingCounter;
	QStringList     m_slTargetFileForLinkLst;
	QStringList     m_sFilesNameForStatLst;
	QStringList     m_slItemsToModify;
	int             m_rawCmdCount, m_processedFilesCount;
	QString         m_sTmpDir;

	bool            m_bOperationFinished;

	qint64          m_totalWeight;
	uint            m_totalFiles, m_totalDirs;
	int             m_preRemovedCounter, m_rmFilesCounter, m_rmDirsCounter, m_removedCounter, m_nRmTotalFiles;
	qint64          m_removedWeight;
	FileInfoToRowMap m_selectionMap; // * workaround W1

	QString         m_sRequestingPanel;
	QString         m_sNameOfProcessedFile;

	bool            m_bConfirmDownloadForOpenWith, m_bConfirmDownloadForView;
	int             m_nOpenWithCounter; // used for counting opened in external application or viewed files
	QString         m_sOpenWithTmpDir;

	QString         m_sCopyTargetPath;
	int             m_nCopyCounter; // used for counting copying items
	int             m_nSelectedFiles;
	int             m_nDirsCouter, m_nFilesCouter, m_nTotalDirs, m_nTotalFiles;
	int             m_dtCouter;
	long long       m_nCopyTotalFilesWeigh, m_nBytesCopiedTotaly;
	const bool      m_bLOCAL_PROGRESS, m_bTOTAL_PROGRESS;
	QFile           m_newDwnFile;

	enum UpdatingType { NONEupd=0, DATEupd, SIZEupd };
	UpdatingType    m_eUpdatingFilesWith;
	bool            m_bAlwaysOverwrite, m_bAlwaysOverwriteSelected, m_bNoneOverwriting, m_bSkipCopyingCurrentFile;
	QString         m_sTargetFileName;
	bool            m_bOperationCanceled, m_bRenameTargetFile;

	QFile          *m_pUploadFile;

private slots:
	void slotCommandStarted( int cmdId );
	void slotCommandFinished( int, bool bError );
	void slotStateChanged( int state );
	void slotListInfo( const QUrlInfo &inUrlInfo );
	void slotDataTransferProgress( qint64 done, qint64 total );

	void slotRunNextCommand();

	/** Processing of rawCommand called for connected host.
	 * @param nReplyCode - command code (3 digits reply code)
	 * @param sDetail - the text that follows the reply code
	 */
	void slotRawCommandReply( int nReplyCode, const QString &sDetail );

};


} // namespace Vfs

#endif
