/***************************************************************************
 *   Copyright (C) 2003 by Piotr Mierzwiñski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _IMAGEVIEWER_H_
#define _IMAGEVIEWER_H_

#include <QScrollArea>
#include <QtPrintSupport/QPrinter>
#include <QPixmap>
#include <QLabel>

#include "viewer.h" // includes also: keyshortcuts.h, plugindata.h, subsystemconst.h
#include "iconcache.h"

class QAction;
class QScrollArea;
class QActionGroup;

namespace Preview {
/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class ImageViewer : public Viewer
{
	Q_OBJECT
public:
	ImageViewer();
	~ImageViewer() {}

	/**
	 * @param pParent pointer to parent this view
	 * @param pToolbarActionsLstForConfig actions group used in config dialog
	 * @oaram bToolbarVisible if true then tool bar will be visible otherwise will be hidden
	 * @param pKeyShortcuts pointer to KeyShortcuts object
	 */
	void init( QWidget *pParent, QList <QActionGroup *> *pToolbarActionsLstForConfig, bool bToolbarVisible, KeyShortcuts *pKeyShortcuts );

	/** Returns curret mode of viewer. For details please check: @see Preview::Viewer::ViewerMode\n
	 * @return viewer mode.
	 */
	ViewerMode  viewerMode() const { return RENDER_mode; }
	bool        viewerModeAvailable( const QString &sMime, ViewerMode eViewerMode ) const;

	PluginData *pluginData() const { return m_pPluginData; };
	void        setPluginData( PluginData *pPluginData ) { m_pPluginData = pPluginData; }

	/** Updates key shortcut object with hardcoded key shortcuts.
	 * @param pKeyShortcuts pointer to object where are stored key shortcuts.
	 */
	void        initKeyShortcuts( KeyShortcuts *pKeyShortcuts );

	/** @brief Updates the viewer.\n
	 * @param eReadingMode - reading data mode, for details please check @see Preview::Viewer::ReadingMode
	 * @param eViewerMode - view mode, for details please check @see Preview::Viewer::ViewerMode
	 * @param sMime - opened file mime
	 * @param baBinBuffer - buffer contains data for filling view.\n
	 */
	void        updateContents( Viewer::ReadingMode eReadingMode, Viewer::ViewerMode eViewerMode, const QString &sMime, const QByteArray &baBinBuffer );

	/** Updates status bar with information about amout of read bytes.
	 */
	void        updateStatus();

	void        clear() { m_pImageLabel->clear(); };

	/** Saves data to file with given name.\n
	 * @param sFileName - file name,
	 * @return TRUE if save finish with success, otherwise FALSE.
	 */
	bool        saveToFile( const QString &sFileName );


	/** Returns pointer to actions group related with Tool Bar, exactly Edit menu.\n
	 * @return pointer to actions group.
	 */
	QActionGroup *editToolBarActions() const { return m_pEditToolBarActions; }

	/** Returns pointer to actions group related with Tool Bar, exactly Viewer menu.\n
	 * @return pointer to actions group.
	 */
	QActionGroup *viewToolBarActions() const { return m_pViewToolBarActions; }


//	void setSmoothInfo( bool );
//	void setUseColorContextInfo( bool );

private:
	PluginData *m_pPluginData;
	IconCache  *m_pIconCache;
	QWidget    *m_pParent;

	QScrollArea *m_pScrollArea;

	QActionGroup *m_pEditToolBarActions, *m_pViewToolBarActions; // group for main window
	QAction      *m_pPrintA, *m_pZoomInA, *m_pZoomOutA, *m_pZoomOrigSize, *m_pZoomFitBest;

	ViewerMode m_eViewerMode;
	KeyShortcuts *m_pKeyShortcuts;

	QPixmap m_Pixmap;
	QLabel *m_pImageLabel;
    double m_dScaleFactor;
	QSize m_OriginalSize;

	/** Ids for actions related with key shortcuts.
	*/
	enum ActionsOfImageViewer {
		Print_ImageViewer=2000,
		ZoomIn_ImageViewer,
		ZoomOut_ImageViewer,
		ZoomOrigSize_ImageViewer,
		ZoomFitBest_ImageViewer
	};


	QIcon icon( const QString &sIconName ) const {
		return m_pIconCache->icon(sIconName);
	}

	void initMenuAndToolBarActions( QList <QActionGroup *> *pToolbarActionsLst );

	void applyKeyShortcuts( KeyShortcuts *pKeyShortcuts );

	void zoom( bool bZoomIn );
	void scaleImage( double dFactor );
	void adjustScrollBar( QScrollBar *pScrollBar, double dFactor );

private slots:
	void slotPrint();
	void slotZoomIn()  { zoom(true);  }
	void slotZoomOut() { zoom(false); }
	void slotSetOriginalSize();
	void slotFitToWindow();

// public slots:
// 	void slotSaveSettings()  {}

protected:
	bool eventFilter( QObject *pObj, QEvent *pEvent );

};

} // namespace Preview

#endif // _IMAGEVIEWER_H_
