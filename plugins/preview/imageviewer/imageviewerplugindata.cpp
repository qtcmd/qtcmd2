/***************************************************************************
 *   Copyright (C) 2015 by Piotr Mierzwiñski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "imageviewerplugindata.h"
//#include "imageviewersettings.h"

namespace Preview {

ImageViewerPluginData::ImageViewerPluginData() : PluginData()
{
	name         = "imageviewer_plugin";
	version      = "0.1.0";
	description  = QObject::tr("Plugin handles image view.");
	plugin_class = "viewer";
	mime_icon    = ""; // (used in configuration window, higher priority than own_icon)
	own_icon     = 0;  // QIcon pointer. (used in configuration window)
	mime_type    = "image/";
// 	popup_actions   = NULL; // QActionGroup pointer  (imageviewerSettings->popupActions())
    toolBar      = NULL; // QToolBar pointer      (imageviewerSettings->toolBar())
    configPage   = NULL; // QWidget pointer       (imageviewerSettings->configPage())
}

} // namespace Preview
