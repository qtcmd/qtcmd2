/***************************************************************************
 *   Copyright (C) 2003 by Piotr Mierzwiñski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along withthisprogram; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDebug>
#include <QPainter>
#include <QScrollBar>
#include <QVBoxLayout>
#include <QImageReader>

#include "settings.h"
#include "iconcache.h"
#include "messagebox.h"
#include "imageviewer.h"

#if defined(QT_PRINTSUPPORT_LIB)
#  include <QtPrintSupport/qtprintsupportglobal.h>

#  if QT_CONFIG(printdialog)
#    include <QPrintDialog>
#  endif
#endif

namespace Preview {

static int s_nPixmapWidth;
static QByteArray s_MainBuffer;

ImageViewer::ImageViewer()
{
	qDebug("ImageViewer::ImageViewer.");

	m_pEditToolBarActions = NULL;
	m_pViewToolBarActions = NULL;

	m_pIconCache = IconCache::instance();

	s_MainBuffer.resize(0);
	s_nPixmapWidth = 0;

	QVBoxLayout *pLayout = new QVBoxLayout(this);
	pLayout->setContentsMargins(1, 1, 1, 1);
	m_pScrollArea = new QScrollArea(this);
	m_pScrollArea->setBackgroundRole(QPalette::Dark);
	pLayout->addWidget(m_pScrollArea);

	m_pImageLabel = new QLabel(m_pScrollArea);
	m_pImageLabel->setBackgroundRole(QPalette::Base);
	m_pImageLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
	m_pImageLabel->setScaledContents(true);

	m_pScrollArea->setWidget(m_pImageLabel);
	m_pScrollArea->installEventFilter(this); // for grabing Resize event (need for properly update status bar)
}


void ImageViewer::init( QWidget *pParent, QList< QActionGroup* > *pToolbarActionsLstForConfig, bool bToolbarVisible, KeyShortcuts *pKeyShortcuts )
{
	m_pParent = pParent;

	m_pScrollArea->setFocus();
	m_pScrollArea->showNormal();

	initMenuAndToolBarActions(pToolbarActionsLstForConfig); // create a lot of QAction objects
	initKeyShortcuts(pKeyShortcuts);
}

void ImageViewer::initMenuAndToolBarActions( QList <QActionGroup *> *pToolbarActionsLst )
{
	QActionGroup *pToolsToolBar = new QActionGroup(this);
	pToolsToolBar->setObjectName(tr("Image preview"));

	// --- actions for Print and Zoom
	m_pPrintA = new QAction(icon("document/print"), tr("&Print"), this);
	m_pPrintA->setShortcut(Qt::CTRL + Qt::Key_P);
	m_pZoomInA = new QAction(icon("zoom/in"), tr("Zoom &in")+" (Ctrl+Plus)", this);
	m_pZoomInA->setShortcut(Qt::CTRL + Qt::Key_Plus);
	m_pZoomOutA = new QAction(icon("zoom/out"), tr("Zoom &out")+" (Ctrl+Minus)", this);
	m_pZoomOutA->setShortcut(Qt::CTRL + Qt::Key_Minus);
	m_pZoomOrigSize = new QAction(icon("zoom/fit-best"), tr("Original &size")+" (Ctrl+Space)", this);
	m_pZoomOrigSize->setShortcut(Qt::CTRL + Qt::Key_Space);
	m_pZoomFitBest = new QAction(icon("zoom/fit-best"), tr("&Fit to window")+" (Ctrl+F)", this);
	m_pZoomFitBest->setShortcut(Qt::CTRL + Qt::Key_F);
	m_pZoomFitBest->setCheckable(true);

	connect(m_pPrintA, &QAction::triggered, this, &ImageViewer::slotPrint);
	connect(m_pZoomInA, &QAction::triggered, this, &ImageViewer::slotZoomIn);
	connect(m_pZoomOutA, &QAction::triggered, this, &ImageViewer::slotZoomOut);
	connect(m_pZoomOrigSize, &QAction::triggered, this, &ImageViewer::slotSetOriginalSize);
	connect(m_pZoomFitBest, &QAction::triggered, this, &ImageViewer::slotFitToWindow);


	QAction *pViewerToolBarSep = new QAction(this);
	pViewerToolBarSep->setSeparator(true);

	// --- Viewer menu action group
	m_pViewToolBarActions = new QActionGroup(this); // main window tool bar
	m_pViewToolBarActions->addAction(pViewerToolBarSep);
	m_pViewToolBarActions->addAction(m_pPrintA);
	m_pViewToolBarActions->addAction(m_pZoomInA);
	m_pViewToolBarActions->addAction(m_pZoomOutA);
	m_pViewToolBarActions->addAction(m_pZoomOrigSize);
	m_pViewToolBarActions->addAction(m_pZoomFitBest);
// 	initMenuAndToolBarActions(pToolbarActionsLst); // create QAction objects
	//
	pToolbarActionsLst->append(pToolsToolBar);
	pToolbarActionsLst->append(m_pViewToolBarActions);
}


void ImageViewer::updateContents( Viewer::ReadingMode eReadingMode, Viewer::ViewerMode eViewerMode, const QString &sMime, const QByteArray &baBinBuffer )
{
	if (eReadingMode == NONE) {
		s_MainBuffer.resize(0); // reset for new image
		return;
	}
	qDebug() << "ImageViewer::updateContents.";

	m_dScaleFactor = 1.0;
	m_pScrollArea->takeWidget();

	bool bOK = false;
	int nPixmapWidth = 0, nPixmapHeight = 0;

	if (eReadingMode == ALL) {
		bOK = m_Pixmap.loadFromData( baBinBuffer );
		nPixmapWidth = m_Pixmap.width(), nPixmapHeight = m_Pixmap.height();
	}
	else
	if (eReadingMode == APPEND) { // very slow
		uint oldSize  = s_MainBuffer.size();
		uint newBytes = baBinBuffer.size()-1; // becouse buffer is increased (required for text files)
		s_MainBuffer.resize(oldSize+newBytes);
		for (uint i=0; i<newBytes; i++)
			s_MainBuffer[oldSize+i] = baBinBuffer[i];
		bOK = m_Pixmap.loadFromData(s_MainBuffer);
		nPixmapWidth = m_Pixmap.width(), nPixmapHeight = m_Pixmap.height();
	}

	if (! bOK) {
		emit signalUpdateStatus(tr("Cannot load a picture !"));
		return;
	}

	if (m_pImageLabel->width() < nPixmapWidth)
		m_pImageLabel->resize(nPixmapWidth, nPixmapHeight);

	m_pImageLabel->setPixmap(m_Pixmap);

	if (m_pImageLabel) {
        m_pScrollArea->setWidget(m_pImageLabel);
		if (! m_pImageLabel->isVisible())
			m_pImageLabel->show();
	}

	m_OriginalSize = QSize(m_pImageLabel->width(), m_pImageLabel->height());
	updateStatus();
	emit signalStatusChanged(Vfs::READ_FILE, objectName());
}

bool ImageViewer::saveToFile( const QString &sFileName )
{
	if (m_Pixmap.save(sFileName, QImageReader::imageFormat(sFileName)))
		return true;

	bool bChecked = false;
	MessageBox::critical(this, tr("Save file")+" - QtCommander 2", "",
		"\""+ sFileName +"\""+"\n\n"+ tr("Cannot write to this file"), bChecked
	);

	return false;
}

/*
	void getData( QByteArray & baBinBuffer );
void ImageViewer::getData( QByteArray & baBinBuffer )
{
	//baBinBuffer = mPixmap.data();
}
*/

void ImageViewer::updateStatus()
{
	m_pZoomInA->setEnabled(! m_pZoomFitBest->isChecked());
	m_pZoomOutA->setEnabled(! m_pZoomFitBest->isChecked());

	QString sMessage = QString(tr("Current/Original size:")+" %1x%2/%3x%4, "+tr("Window size:")+"%5x%6, "+tr("Color depth:")+ " %7 "+tr("bits"))
		.arg(m_pImageLabel->width()).arg(m_pImageLabel->height()).arg(m_OriginalSize.width()).arg(m_OriginalSize.height())
		.arg(m_pParent->width()).arg(m_pParent->height()).arg(m_Pixmap.depth());

	emit signalUpdateStatus(sMessage);
}

void ImageViewer::zoom( bool bZoomIn )
{
	qDebug() << "ImageViewer::zoom. In:" << bZoomIn;
	scaleImage((bZoomIn ? 1.25 : 0.8));
	updateStatus();
}

void ImageViewer::scaleImage( double dFactor )
{	// Function comes from Qt 5.15.0 examples (imageviewer)
	m_dScaleFactor *= dFactor;
	m_pImageLabel->resize(m_dScaleFactor * m_pImageLabel->pixmap(Qt::ReturnByValue).size());

	adjustScrollBar(m_pScrollArea->horizontalScrollBar(), dFactor);
	adjustScrollBar(m_pScrollArea->verticalScrollBar(), dFactor);

	m_pZoomInA->setEnabled(m_dScaleFactor < 3.0);
	m_pZoomOutA->setEnabled(m_dScaleFactor > 0.333);
}

void ImageViewer::adjustScrollBar( QScrollBar *pScrollBar, double dFactor )
{	// Function comes from Qt 5.15.0 examples (imageviewer)
	pScrollBar->setValue(int(dFactor * pScrollBar->value() + ((dFactor - 1) * pScrollBar->pageStep()/2)));
}

void ImageViewer::initKeyShortcuts( KeyShortcuts *pKeyShortcuts )
{
	if (pKeyShortcuts == NULL) {
		qDebug() << "ImageViewer::updateKeyShortcuts. One can't update key shortcuts - empty pointer!";
		return;
	}
	m_pKeyShortcuts = pKeyShortcuts;

	m_pKeyShortcuts->setKey(Print_ImageViewer, Qt::CTRL+Qt::Key_P, Qt::CTRL+Qt::Key_P, tr("Print"));
	m_pKeyShortcuts->setKey(ZoomIn_ImageViewer, Qt::CTRL+Qt::Key_Plus, Qt::CTRL+Qt::Key_Plus, tr("Zoom in"));
	m_pKeyShortcuts->setKey(ZoomOut_ImageViewer, Qt::CTRL+Qt::Key_Minus, Qt::CTRL+Qt::Key_Minus, tr("Zoom out"));
	m_pKeyShortcuts->setKey(ZoomOrigSize_ImageViewer, Qt::CTRL+Qt::Key_Space, Qt::CTRL+Qt::Key_Space, tr("Original size"));
	m_pKeyShortcuts->setKey(ZoomFitBest_ImageViewer, Qt::CTRL+Qt::Key_F, Qt::CTRL+Qt::Key_F, tr("Fit to window"));

	m_pKeyShortcuts->updateEntryList("fileViewShortcuts/"); // update from config file
	applyKeyShortcuts(m_pKeyShortcuts); // for actions in current window
}

void ImageViewer::applyKeyShortcuts( KeyShortcuts *pKeyShortcuts )
{
	if (! pKeyShortcuts) {
		qDebug() << "ImageViewer::applyKeyShortcuts. One can't apply key shortcuts - empty pointer!";
		return;
	}
	m_pKeyShortcuts = pKeyShortcuts;

	m_pPrintA->setShortcut(pKeyShortcuts->keyCode(Print_ImageViewer));
	m_pZoomInA->setShortcut(pKeyShortcuts->keyCode(ZoomIn_ImageViewer));
	m_pZoomOutA->setShortcut(pKeyShortcuts->keyCode(ZoomOut_ImageViewer));
	m_pZoomOutA->setShortcut(pKeyShortcuts->keyCode(ZoomOrigSize_ImageViewer));
	m_pZoomOutA->setShortcut(pKeyShortcuts->keyCode(ZoomFitBest_ImageViewer));
}

bool ImageViewer::viewerModeAvailable( const QString &, ViewerMode eViewerMode ) const
{
	if (eViewerMode == RENDER_mode)
		return true;

	return false;
}

		// ----------- SLOTs -----------

void ImageViewer::slotSetOriginalSize()
{
	m_pZoomFitBest->setChecked(false);

	m_pScrollArea->setWidgetResizable(false);
	// code comes from Qt 5.15.0 examples (imageviewer)
	m_pImageLabel->adjustSize();
	m_dScaleFactor = 1.0;

	updateStatus();
}

void ImageViewer::slotFitToWindow()
{	// Function comes from Qt 5.15.0 examples (imageviewer)
	bool bFitToWindow = m_pZoomFitBest->isChecked();
	m_pScrollArea->setWidgetResizable(bFitToWindow);
	if (! bFitToWindow) { // normalSize
		m_pImageLabel->adjustSize();
		m_dScaleFactor = 1.0;
	}
	updateStatus();
}

void ImageViewer::slotPrint()
{  // Function comes from Qt 5.15.0 examples (imageviewer)
	qDebug("ImageViewer::slotPrint.");
	Q_ASSERT(! m_pImageLabel->pixmap(Qt::ReturnByValue).isNull());

	#if defined(QT_PRINTSUPPORT_LIB) && QT_CONFIG(printdialog)
		QPrintDialog dialog(&printer, this);
		if (dialog.exec()) {
			QPainter painter(&printer);
			QPixmap pixmap = m_pImageLabel->pixmap(Qt::ReturnByValue);
			QRect rect = painter.viewport();
			QSize size = pixmap.size();
			size.scale(rect.size(), Qt::KeepAspectRatio);
			painter.setViewport(rect.x(), rect.y(), size.width(), size.height());
			painter.setWindow(pixmap.rect());
			painter.drawPixmap(0, 0, pixmap);
		}
	#endif
}


bool ImageViewer::eventFilter( QObject *pObj, QEvent *pEvent )
{
	if (pObj == m_pScrollArea) {
		if (pEvent->type() == QEvent::Resize) {
			if (m_Pixmap.size().width() > 0)
				updateStatus();
		}
	}

	return QWidget::eventFilter(pObj, pEvent);  // standard event processing
}

} // namespace Preview

//	void setSmoothInfo( bool );
//	void slotSmoothScaling();
//	void slotShowInFullScreen();
