/***************************************************************************
 *   Copyright (C) 2003 by Piotr Mierzwiñski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along withthisprogram; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QDesktopWidget>
#include <QtPrintSupport/QPrinter>
#include <QtPrintSupport/QPrintDialog>
#include <QInputDialog>
#include <QTextStream>
#include <QListWidget>
#include <QScrollBar>
#include <QClipboard>
#include <QActionGroup>
#include <QFileInfo>
#include <QMenu>
#include <QDebug> // need to qDebug()

#include "settings.h"
#include "iconcache.h"
#include "textviewer.h"
#include "messagebox.h"
#include "inputtextdialog.h"


namespace Preview {


TextViewer::TextViewer()
{
	qDebug() << "TextViewer::TextViewer.";
	//setObjectName("TextViewer");
	m_pEditToolBarAG   = nullptr;
	m_pViewToolBarAG = nullptr;
	m_pViewMenuBarAG = nullptr;
	m_pEditMenuBarAG   = nullptr;
	m_pCompletionListBox = nullptr;
	m_pKS = nullptr;
	m_pParent = nullptr;
	m_pPluginData = nullptr;
	m_pSyntaxHighlighter = nullptr;
	m_pIconCache = IconCache::instance();

	m_sFindNextSearchString = "";
	m_PrevResultOfFind = false;
	m_sInsStatStr = "INS"; // default for the QTextEdit obj.
	m_eEOLtype = UNKNOWN_eol;
	m_eViewerMode = UNKNOWN_mode;
	m_bWholeWordsOnly = false;
	m_bCaseSensitive = true;
	m_bDocModified = false;
	m_bFindForward = true;
	m_bFromBegin = true;
	m_bFindOnce = false;
	m_bOvrMode = false;
	m_bAutoSH = true;

	m_pTextEdit = new QTextEdit(this);
	m_pTextEdit->viewport()->installEventFilter(this); // for grabing mouse events from the QTextEdit obj.
	m_pTextEdit->installEventFilter(this); // for grabing KeyEvent from the View obj.
	m_pTextEdit->setReadOnly(true);
	m_pTextEdit->setUndoRedoEnabled(true);
	connect(m_pTextEdit, &QTextEdit::cursorPositionChanged, this, &TextViewer::slotCursorPositionChanged);

	m_pFindReplaceTextBar = new FindReplaceTextBar(this, "textViewer");
	m_pFindReplaceTextBar->setVisible(false);
	connect(m_pFindReplaceTextBar, &FindReplaceTextBar::signalSearch, this, &TextViewer::slotSearch);
	connect(m_pFindReplaceTextBar, &FindReplaceTextBar::signalStartReplace, this, &TextViewer::slotStartReplace);
	connect(m_pFindReplaceTextBar, &FindReplaceTextBar::signalClickedQuestionBtn1, this, &TextViewer::slotQuestionBtn1Clicked);
	connect(m_pFindReplaceTextBar, &FindReplaceTextBar::signalClickedQuestionBtn2, this, &TextViewer::slotQuestionBtn2Clicked);
	connect(m_pFindReplaceTextBar, &FindReplaceTextBar::signalStartSearchFromCursor, this, &TextViewer::slotSetSearchFromCursor);
	connect(m_pFindReplaceTextBar, &FindReplaceTextBar::signalFindWholeWords, this, &TextViewer::slotFindWholeWords);

	QVBoxLayout *pLayout = new QVBoxLayout(this);
	pLayout->setContentsMargins(1, 1, 1, 1);
	pLayout->addWidget(m_pTextEdit);
	pLayout->addWidget(m_pFindReplaceTextBar);

	// -- force white background, because color schemes are not supported
	QPalette palette;
	palette.setColor(m_pTextEdit->viewport()->backgroundRole(), Qt::white);
//	m_pTextEdit->viewport()->setAutoFillBackground(true);
	m_pTextEdit->viewport()->setPalette(palette);
}

TextViewer::~TextViewer()
{
	m_pTextEdit->viewport()->removeEventFilter(this);
	delete m_pSyntaxHighlighter;
	delete m_pFindReplaceTextBar;
	qDebug() << "TextViewer::~TextViewer. DESTRUCTOR (end)";
}


void TextViewer::init( QWidget *pParent, QList< QActionGroup * > *pToolbarActionsLstForConfig, bool bToolbarVisible, KeyShortcuts *pKeyShortcuts )
{
	qDebug() << "TextViewer::init. Load configuration and init menu and tool bar";
	Q_CHECK_PTR(pParent);
	m_pParent = pParent;
	initKeyShortcuts(pKeyShortcuts);

	m_pSyntaxHighlighter = new SyntaxHighlighter(m_pTextEdit);

	m_pTextEdit->setFocus();
	m_pTextEdit->showNormal();

	Settings settings;

	QString sFont = settings.value("textViewer/Font", "Monospace,9").toString();
	// - parse 'font'
	m_sFontFamily = sFont.split(',').at(0); // sFont.left(sFont.indexOf(','));
	QString sFontSize = sFont.section(',', 1,1).simplified();
	QString sBold     = sFont.section(',', 2,2).simplified().toLower();
	QString sItalic   = sFont.section(',', 3,3).simplified().toLower();
	bool ok;
	m_nFontSize = sFontSize.toInt(&ok);
	if (! ok)
		m_nFontSize = 10; // default size

	int tabWidth = settings.value("textViewer/TabWidth", -1).toInt();
	if (tabWidth <= 0)
		tabWidth = 2; // default width
	if (tabWidth > 8)
		tabWidth = 8;
	m_nTabWidth = tabWidth;

	setTabWidth(m_nTabWidth);
	m_bFontBold   = (sBold   == "bold");
	m_bFontItalic = (sItalic == "italic");
	setFont(m_sFontFamily, m_nFontSize, m_bFontBold, m_bFontItalic);

	m_pSyntaxHighlighter->setFont(m_pTextEdit->currentFont());

	//initKeyShortcuts(pKS); // called externally to be able to update shortcuts after their change
	initMenuAndToolBarActions(pToolbarActionsLstForConfig, bToolbarVisible); // create the QAction objects
// 	initKeyShortcuts(pKeyShortcuts); // CRASH

	//FIXME: use config file to set AutoEnc and alt. other encoding
	m_pAutomaticEncA->setChecked(true);
	m_pCeIso2EncA->setChecked(true);
	slotAutoEncoding();
}


void TextViewer::initKeyShortcuts( KeyShortcuts *pKeyShortcuts )
{
	if (pKeyShortcuts == nullptr) {
		qDebug() << "TextViewer::initKeyShortcuts. INTERNAL ERROR. pKeyShortcuts is Null";
		return;
	}

    m_pKS = pKeyShortcuts;
	//void setKey( int nAction, int nShortcut, int nDefaultCode, const QString & sDescription = "" );
	m_pKS->setKey(Find_TextViewer,      Qt::CTRL+Qt::Key_F,     Qt::CTRL+Qt::Key_F,     tr("Find"));
	m_pKS->setKey(FindNext_TextViewer,  Qt::Key_F3,             Qt::Key_F3,             tr("Find next"));
	m_pKS->setKey(Replace_TextViewer,   Qt::CTRL+Qt::Key_R,     Qt::CTRL+Qt::Key_R,     tr("Replace"));
	m_pKS->setKey(GoToLine_TextViewer,  Qt::CTRL+Qt::Key_G,     Qt::CTRL+Qt::Key_G,     tr("Go to line"));
	m_pKS->setKey(Undo_TextViewer,      Qt::CTRL+Qt::Key_Z,     Qt::CTRL+Qt::Key_Z,     tr("Undo"));
	m_pKS->setKey(Redo_TextViewer,      Qt::CTRL+Qt::Key_Y,     Qt::CTRL+Qt::Key_Y,     tr("Redo"));
	m_pKS->setKey(Cut_TextViewer,       Qt::CTRL+Qt::Key_X,     Qt::CTRL+Qt::Key_X,     tr("Cut"));
	m_pKS->setKey(Copy_TextViewer,      Qt::CTRL+Qt::Key_C,     Qt::CTRL+Qt::Key_C,     tr("Copy"));
	m_pKS->setKey(Paste_TextViewer,     Qt::CTRL+Qt::Key_V,     Qt::CTRL+Qt::Key_V,     tr("Paste"));
	m_pKS->setKey(SelectAll_TextViewer, Qt::CTRL+Qt::Key_A,     Qt::CTRL+Qt::Key_A,     tr("Select all"));
	m_pKS->setKey(Print_TextViewer,     Qt::CTRL+Qt::Key_P,     Qt::CTRL+Qt::Key_P,     tr("Print"));
	m_pKS->setKey(ZoomIn_TextViewer,    Qt::CTRL+Qt::Key_Plus,  Qt::CTRL+Qt::Key_Plus,  tr("Zoom in"));
	m_pKS->setKey(ZoomOut_TextViewer,   Qt::CTRL+Qt::Key_Minus, Qt::CTRL+Qt::Key_Minus, tr("Zoom out"));
	m_pKS->setKey(TextCompletion_TextViewer, Qt::CTRL+Qt::Key_Space, Qt::CTRL+Qt::Key_Space, tr("Text completion"));

	// -- load keyshortcuts from config
	int actionId;
	Settings settings;
	QString sActionID, sKeyShortcut, sDefaulsKeyShortcut;
	QHash <int, KeyCode*>::iterator it;
	// -- set new values
	for (it = m_pKS->fistItem(); it != m_pKS->lastItem(); ++it) {
		actionId = it.key() - KeyShortcuts::ACTION_ID_OFFSET;
// 		qDebug() << "TextViewer::initKeyShortcuts. actionId:" << actionId << "desc:" << m_pKS->desc(actionId);
		sActionID = QStringLiteral("ActionID_") + QString("%1").arg(actionId+KeyShortcuts::ACTION_ID_OFFSET);
		sDefaulsKeyShortcut = m_pKS->keyDefaultStr(actionId);
		sKeyShortcut = settings.value("FileViewShortcuts/"+sActionID, sDefaulsKeyShortcut).toString();
// 		if (sActionID == "ActionID_1008")
// 			qDebug() << "TextViewer::initKeyShortcuts." << "sActionID:" << sActionID << "new keyShortcut:" << sKeyShortcut;
		if (sKeyShortcut != sDefaulsKeyShortcut)
			m_pKS->setKey(actionId, sKeyShortcut);
	}
// 	applyKeyShortcuts(m_pKS); // for actions in current window
}


void TextViewer::initMenuAndToolBarActions(QList< QActionGroup * > *pToolbarActionsLstForConfig, bool bToolbarVisible)
{
	qDebug() << "TextViewer::initMenuAndToolBarActions";
	// --- prepare actions for EDIT menuBar (later will be added to QActionGroup)
	// -- menuBar
	m_pUndoAM      = new QAction(icon("edit/undo"),         tr("&Undo"),       this);
	m_pRedoAM      = new QAction(icon("edit/redo"),         tr("&Redo"),       this);
	m_pCutAM       = new QAction(icon("edit/cut"),          tr("C&ut"),        this);
	m_pCopyAM      = new QAction(icon("edit/copy"),         tr("&Copy"),       this);
	m_pPasteAM     = new QAction(icon("edit/paste"),        tr("&Paste"),      this);
	m_pFindAM      = new QAction(icon("edit/find"),         tr("&Find") ,      this);
	m_pFindNextAM  = new QAction(icon("edit/findnext"),     tr("Find &next"),  this);
	m_pReplaceAM   = new QAction(icon("edit/find-replace"), tr("&Replace"),    this);
	m_pGotoLineAM  = new QAction(icon("go/jump-row"),       tr("&Go to line"), this);
	m_pSelectAllAM = new QAction(icon("edit/select-all"),   tr("Select &all"), this);

	connect(m_pUndoAM,      &QAction::triggered, m_pTextEdit, &QTextEdit::undo);
	connect(m_pRedoAM,      &QAction::triggered, m_pTextEdit, &QTextEdit::redo);
	connect(m_pCutAM,       &QAction::triggered, m_pTextEdit, &QTextEdit::cut);
	connect(m_pCopyAM,      &QAction::triggered, m_pTextEdit, &QTextEdit::copy);
	connect(m_pPasteAM,     &QAction::triggered, this,        &TextViewer::slotPastePlainText);
	connect(m_pFindAM,      &QAction::triggered, this,        &TextViewer::slotShowFindBar);
	connect(m_pFindNextAM,  &QAction::triggered, this,        &TextViewer::slotQuestionBtn1Clicked);
	connect(m_pReplaceAM,   &QAction::triggered, this,        &TextViewer::slotShowReplaceBar);
	connect(m_pGotoLineAM,  &QAction::triggered, this,        &TextViewer::slotGoToLine);
	connect(m_pSelectAllAM, &QAction::triggered, m_pTextEdit, &QTextEdit::selectAll);

	// -- toolBar
	m_pUndoAT      = new QAction(icon("edit/undo"),         tr("&Undo"),       this);
	m_pRedoAT      = new QAction(icon("edit/redo"),         tr("&Redo"),       this);
	m_pCutAT       = new QAction(icon("edit/cut"),          tr("C&ut"),        this);
	m_pCopyAT      = new QAction(icon("edit/copy"),         tr("&Copy"),       this);
	m_pPasteAT     = new QAction(icon("edit/paste"),        tr("&Paste"),      this);
	m_pFindAT      = new QAction(icon("edit/find"),         tr("&Find") ,      this);
	m_pFindNextAT  = new QAction(icon("edit/findnext"),     tr("Find &next"),  this);
	m_pReplaceAT   = new QAction(icon("edit/find-replace"), tr("&Replace"),    this);
	m_pGotoLineAT  = new QAction(icon("go/jump-row"),       tr("&Go to line"), this);
	m_pSelectAllAT = new QAction(icon("edit/select-all"),   tr("Select &all"), this);

	connect(m_pUndoAT,      &QAction::triggered, m_pTextEdit, &QTextEdit::undo);
	connect(m_pRedoAT,      &QAction::triggered, m_pTextEdit, &QTextEdit::redo);
	connect(m_pCutAT,       &QAction::triggered, m_pTextEdit, &QTextEdit::cut);
	connect(m_pCopyAT,      &QAction::triggered, m_pTextEdit, &QTextEdit::copy);
	connect(m_pPasteAT,     &QAction::triggered, m_pTextEdit, &QTextEdit::paste);
	connect(m_pFindAT,      &QAction::triggered, this,        &TextViewer::slotShowFindBar);
	connect(m_pFindNextAT,  &QAction::triggered, this,        &TextViewer::slotQuestionBtn1Clicked);
	connect(m_pReplaceAT,   &QAction::triggered, this,        &TextViewer::slotShowReplaceBar);
	connect(m_pGotoLineAT,  &QAction::triggered, this,        &TextViewer::slotGoToLine);
	connect(m_pSelectAllAT, &QAction::triggered, m_pTextEdit, &QTextEdit::selectAll);

	// --- actions for VIEWER menu (EoL menu)
	m_pUnixEolA = new QAction(tr("&Unix"), this);
	m_pUnixEolA->setCheckable(true);
	m_pWinDosEolA = new QAction(tr("&Windows and Dos"), this);
	m_pWinDosEolA->setCheckable(true);

	QMenu *pEolMenu = new QMenu(this);
	pEolMenu->addAction(m_pUnixEolA);
	pEolMenu->addAction(m_pWinDosEolA);
	QAction *pEolActionsSubMenu = new QAction(tr("End of line"), this);
	pEolActionsSubMenu->setMenu(pEolMenu);


	// --- actions for VIEWER menu (Encoding)
	m_pManuallyMenu = new QMenu(this);
	m_pAutomaticEncA = new QAction(tr("Automatic"), this);
	m_pAutomaticEncA->setCheckable(true);
	QAction *pManuallySubMenuA = new QAction(tr("Manually"), this);
	pManuallySubMenuA->setMenu(m_pManuallyMenu);

	QMenu *pSetEncodingMenu = new QMenu(this);
	pSetEncodingMenu->addAction(m_pAutomaticEncA);
	pSetEncodingMenu->addAction(pManuallySubMenuA);
	QAction *pSetEncodingSubMenuA = new QAction(tr("Set encoding"), this);
	pSetEncodingSubMenuA->setMenu(pSetEncodingMenu);

	// TODO all actions with encodings name need to generate from file
	m_pCeIso2EncA  = new QAction(tr("Central European")+" (ISO-8859-2)", this);
	m_pCeIso2EncA->setCheckable(true);
	m_pCeCpEncA    = new QAction(tr("Central European")+" (CP-1250)", this);
	m_pCeCpEncA->setCheckable(true);

	m_pManuallyMenu->addAction(m_pCeIso2EncA);
	m_pManuallyMenu->addAction(m_pCeCpEncA);


	// --- actions for VIEWER menu (highlighting menu)
	m_pHighlightingParentA = new QAction(tr("Highlighting &mode"), this);
	QAction *pHighlightingSourceParentA = new QAction(tr("Source"), this);
	QAction *pHighlightingMarkupParentA = new QAction(tr("Markup"), this);

	// --- actions for VIEWER menu (highlighting items)
	// TODO all actions with highlighting names get from config file
	m_pNoneA = new QAction(tr("None"), this);
	m_pNoneA->setCheckable(true);
	m_pCCppA = new QAction(tr("C/C++"), this);
	m_pCCppA->setCheckable(true);
	m_pHtmlA = new QAction(tr("HTML/XML"), this);
	m_pHtmlA->setCheckable(true);
	m_pBashA = nullptr; //new QAction(tr("Bash"),this);
	m_pPerlA = nullptr; //new QAction(tr("Perl"),this);
	// TODO would be nice to load highlighting menu from config file

	// --- actions for Print and Zoom
	// -- menuBar
	m_pPrintAM   = new QAction(icon("document/print"), tr("&Print"),    this);
	m_pZoomInAM  = new QAction(icon("zoom/in"),        tr("Zoom &in"),  this);
	m_pZoomOutAM = new QAction(icon("zoom/out"),       tr("Zoom &out"), this);

	// -- toolBar
	m_pPrintAT   = new QAction(icon("document/print"), tr("&Print"),    this);
	m_pZoomInAT  = new QAction(icon("zoom/in"),        tr("Zoom &in"),  this);
	m_pZoomOutAT = new QAction(icon("zoom/out"),       tr("Zoom &out"), this);

	QActionGroup *pZoomActions = new QActionGroup(m_pTextEdit);
	pZoomActions->addAction(m_pZoomInAT);
	pZoomActions->addAction(m_pZoomOutAT);

	connect(m_pPrintAT,   &QAction::triggered, this, &TextViewer::slotPrint);
	connect(m_pZoomInAT,  &QAction::triggered, this, &TextViewer::slotZoomIn);
	connect(m_pZoomOutAT, &QAction::triggered, this, &TextViewer::slotZoomOut);

	connect(m_pPrintAM,   &QAction::triggered, this, &TextViewer::slotPrint);
	connect(m_pZoomInAM,  &QAction::triggered, this, &TextViewer::slotZoomIn);
	connect(m_pZoomOutAM, &QAction::triggered, this, &TextViewer::slotZoomOut);

	applyKeyShortcutsForActions(m_pKS);

	// --- separators
	QAction *pEditMenuSep = new QAction(this);   pEditMenuSep->setSeparator(true);
	QAction *pViewerMenuSep = new QAction(this); pViewerMenuSep->setSeparator(true);

	// -- EoL group
	m_pEolActions = new QActionGroup(this);
	m_pEolActions->setExclusive(true);
	m_pEolActions->addAction(m_pUnixEolA);
	m_pEolActions->addAction(m_pWinDosEolA);

	// Viewer->Set Encoding->Manually group
	m_pManuallyCodingActions = new QActionGroup(this);
	m_pManuallyCodingActions->setExclusive(true);
	m_pManuallyCodingActions->addAction(m_pCeIso2EncA);
	m_pManuallyCodingActions->addAction(m_pCeCpEncA);

	// Viewer->Highlighting mode group
	m_pSelectHighlightingActions = new QActionGroup(this);
	m_pSelectHighlightingActions->setExclusive(true);
	m_pSelectHighlightingActions->addAction(m_pNoneA);
	m_pSelectHighlightingActions->addAction(m_pCCppA);
	m_pSelectHighlightingActions->addAction(m_pHtmlA);

	QMenu *pHighlightingMenu = new QMenu(this);
	m_pHighlightingParentA->setMenu(pHighlightingMenu);
	pHighlightingMenu->addAction(m_pNoneA);
	pHighlightingMenu->addAction(pHighlightingSourceParentA);
	pHighlightingMenu->addAction(pHighlightingMarkupParentA);

	// Viewer->Highlighting mode->Source sub-MENU
	QMenu *sourceMenu = new QMenu(this);
	sourceMenu->addAction(m_pCCppA);
	pHighlightingSourceParentA->setMenu(sourceMenu);

	// Viewer->Highlighting mode->Markup sub-MENU
	QMenu *pMarkupMenu = new QMenu(this);
	pMarkupMenu->addAction(m_pHtmlA);
	pHighlightingMarkupParentA->setMenu(pMarkupMenu);

	connect(pEolMenu,                     &QMenu::triggered,        this, &TextViewer::slotChangeEOL);
	connect(m_pAutomaticEncA,             &QAction::triggered,      this, &TextViewer::slotAutoEncoding);
	connect(m_pManuallyCodingActions,     &QActionGroup::triggered, this, &TextViewer::slotChangeEncoding);
	connect(m_pSelectHighlightingActions, &QActionGroup::triggered, this, &TextViewer::slotChangeSyntaxHighlighting);


	// - menu bar
	// --- Viewer menu action group
	m_pViewMenuBarAG = new QActionGroup(this); // main window menu
	//m_pViewMenuBarAG->setObjectName(tr("View"));
	m_pViewMenuBarAG->addAction(pViewerMenuSep);
	m_pViewMenuBarAG->addAction(pEolActionsSubMenu);
	m_pViewMenuBarAG->addAction(pSetEncodingSubMenuA);
	m_pViewMenuBarAG->addAction(m_pPrintAM);
	m_pViewMenuBarAG->addAction(m_pZoomInAM);
	m_pViewMenuBarAG->addAction(m_pZoomOutAM);

	// --- EDIT menu action group
	m_pEditMenuBarAG = new QActionGroup(this); // main window menu
	//m_pEditMenuBarAG->setObjectName(tr("Edit"));
	m_pEditMenuBarAG->addAction(pEditMenuSep);
	m_pEditMenuBarAG->addAction(m_pFindAM);
	m_pEditMenuBarAG->addAction(m_pFindNextAM);
	m_pEditMenuBarAG->addAction(m_pUndoAM);
	m_pEditMenuBarAG->addAction(m_pRedoAM);
	m_pEditMenuBarAG->addAction(m_pCutAM);
	m_pEditMenuBarAG->addAction(m_pCopyAM);
	m_pEditMenuBarAG->addAction(m_pPasteAM);
	m_pEditMenuBarAG->addAction(m_pReplaceAM);
	m_pEditMenuBarAG->addAction(m_pGotoLineAM);
	m_pEditMenuBarAG->addAction(m_pSelectAllAM);

	// --- update toolBars
	// - prepare actions for configuration dialog (input view need to contain all available actions)
	m_pUndoAT->setObjectName("undo_doc");
	m_pRedoAT->setObjectName("redo_doc");
	m_pCutAT->setObjectName("cut_doc");
	m_pCopyAT->setObjectName("copy_doc");
	m_pPasteAT->setObjectName("paste_doc");
	m_pReplaceAT->setObjectName("replace_doc");
	m_pGotoLineAT->setObjectName("goto_doc");
	m_pSelectAllAT->setObjectName("selectall_doc");

	m_pPrintAT->setObjectName("print_doc");
	m_pZoomInAT->setObjectName("zoomin_doc");
	m_pZoomOutAT->setObjectName("zoomout_doc");
	m_pFindAT->setObjectName("find_doc");
	m_pFindNextAT->setObjectName("findnext_doc");

	m_toolBarActionsMap.insert("print_doc",      m_pPrintAT);
	m_toolBarActionsMap.insert("zoomin_doc",     m_pZoomInAT);
	m_toolBarActionsMap.insert("zoomout_doc",    m_pZoomOutAT);

	// map uses to put the actions into the toolbars
	m_toolBarActionsMap.insert("find_doc",       m_pFindAT);
	m_toolBarActionsMap.insert("findnext_doc",   m_pFindNextAT);
	m_toolBarActionsMap.insert("undo_doc",       m_pUndoAT);
	m_toolBarActionsMap.insert("redo_doc",       m_pRedoAT);
	m_toolBarActionsMap.insert("cut_doc",        m_pCutAT);
	m_toolBarActionsMap.insert("copy_doc",       m_pCopyAT);
	m_toolBarActionsMap.insert("paste_doc",      m_pPasteAT);
	m_toolBarActionsMap.insert("replace_doc",    m_pReplaceAT);
	m_toolBarActionsMap.insert("goto_doc",       m_pGotoLineAT);
	m_toolBarActionsMap.insert("selectall_doc",  m_pSelectAllAT);

	// - tool bar
	m_pViewToolBarAG = new QActionGroup(this);
	m_pViewToolBarAG->setObjectName(tr("View"));
	//m_pViewToolBarAG->addAction(pViewToolBarSep);
	m_pViewToolBarAG->addAction(m_pPrintAT);
	m_pViewToolBarAG->addAction(m_pZoomInAT);
	m_pViewToolBarAG->addAction(m_pZoomOutAT);
// 	m_pViewToolBarAG->addAction(m_pFindAT);
// 	m_pViewToolBarAG->addAction(m_pFindNextAT);

	m_pEditToolBarAG = new QActionGroup(this);
	m_pEditToolBarAG->setObjectName(tr("Edit"));
	//m_pEditToolBarAG->addAction(pEditToolBarSep);
	m_pEditToolBarAG->addAction(m_pFindAT);
	m_pEditToolBarAG->addAction(m_pFindNextAT);
	m_pEditToolBarAG->addAction(m_pUndoAT);
	m_pEditToolBarAG->addAction(m_pRedoAT);
	m_pEditToolBarAG->addAction(m_pCutAT);
	m_pEditToolBarAG->addAction(m_pCopyAT);
	m_pEditToolBarAG->addAction(m_pPasteAT);
	m_pEditToolBarAG->addAction(m_pReplaceAT);
	m_pEditToolBarAG->addAction(m_pGotoLineAT);
	m_pEditToolBarAG->addAction(m_pSelectAllAT);

	pToolbarActionsLstForConfig->append(m_pEditToolBarAG);
	pToolbarActionsLstForConfig->append(m_pViewToolBarAG);

	if (bToolbarVisible)
		updateToolBarsByConfigFile();
	// 	(dynamic_cast <(FileViewer *>)m_pParent)->addKeyshortcutToActionToolTip(m_pPrintA, Print_TextViewer); // need to implement in Viewer
}

void TextViewer::updateToolBarsByConfigFile()
{
	if (m_toolBarActionsMap.count() == 0)
		return;

	Settings settings;
	QStringList slViewTB = settings.value("viewer/ViewToolBarActions",
					"print_doc, zoomin_doc, zoomout_doc").toString().remove(" ").split(",");
	QStringList slEditTB = settings.value("viewer/EditToolBarActions",
					"selectall_doc, find_doc, findnext_doc, undo_doc, redo_doc, cut_doc, copy_doc, paste_doc, replace_doc, goto_doc").toString().remove(" ").split(",");

	// -- before adding actions to tollbar, clean all. Usable for any next update of toolbars
// 	m_pViewerToolBarActions->clear(); // missing method
// 	m_pEditToolBarActions->clear(); // missing method
	QStringList slActions;
	slActions <<  "print_doc" << "zoomin_doc" << "zoomout_doc";
	for (int i=0; i<slActions.count(); i++)
		m_pViewToolBarAG->removeAction(m_toolBarActionsMap.value(slActions.at(i)));
	slActions.clear();
	slActions <<  "selectall_doc" << "find_doc" << "findnext_doc" << "undo_doc" << "redo_doc" << "cut_doc" << "copy_doc" << "paste_doc" << "replace_doc" << "goto_doc";
	for (int i=0; i<slActions.count(); i++)
		m_pEditToolBarAG->removeAction(m_toolBarActionsMap.value(slActions.at(i)));

	for (int i=0; i<slViewTB.count(); i++)
		m_pViewToolBarAG->addAction(m_toolBarActionsMap.value(slViewTB.at(i)));
	for (int i=0; i<slEditTB.count(); i++)
		m_pEditToolBarAG->addAction(m_toolBarActionsMap.value(slEditTB.at(i)));
}

void TextViewer::setEoLtype( EOLtype eEOLtype )
{
	if (eEOLtype == m_eEOLtype || eEOLtype == UNKNOWN_eol || eEOLtype < UNKNOWN_eol || eEOLtype > WINandDOS_eol)
		return;

	bool bUnixEol = (eEOLtype == UNIX_eol);
	QString sEoLtype = (bUnixEol) ? "Unix" : "Windows and Dos";
	qDebug() << "TextViewer::setEoLtype. Change EoL to:" << sEoLtype << "type";

	m_pWinDosEolA->setChecked(! bUnixEol);
	m_pUnixEolA->setChecked(bUnixEol);
	m_eEOLtype = eEOLtype;

	QTextDocument *pDoc = m_pTextEdit->document();
	if (! pDoc->isEmpty()) { // replace current EOL
		emit signalUpdateStatus(QString(tr("Changing EoL to type %1")).arg(sEoLtype), 0, false); // 0 - save prev.msg.
		m_sMainBuffer = m_pTextEdit->toPlainText();

		if (eEOLtype == UNIX_eol)
			m_sMainBuffer.replace("\x0d\x0a", "\x0a");
		else // WIN_DOSeol
			m_sMainBuffer.replace("\x0a", "\x0d\x0a");

		m_pTextEdit->setText(m_sMainBuffer);
		emit signalUpdateStatus(QString(tr("Changed EoL to type %1")).arg(sEoLtype), 2000, true);
		updateStatus();
	}
}


void TextViewer::setFont( const QString &sFamily, uint nSize, bool bBold, bool bItalic )
{
	m_pTextEdit->setFont(QFont(sFamily, nSize, (bBold ? QFont::Bold : QFont::Normal), bItalic));
}


void TextViewer::setTabWidth( uint amountOfChars )
{
	m_pTextEdit->setTabStopDistance(QFontMetrics(m_pTextEdit->currentFont()).horizontalAdvance("a")*amountOfChars);
}


void TextViewer::updateContents( Viewer::ReadingMode eReadingMode, Viewer::ViewerMode eViewerMode, const QString &sMime, const QByteArray &baBinBuffer )
{
	qDebug() << "TextViewer::updateContents. ViewerMode:" << Viewer::toString(eViewerMode) << "ReadingMode:" << Viewer::toString(eReadingMode) << "mime:" << sMime;
	bool bTextViewerIsEmpty = m_pTextEdit->document()->isEmpty();

	m_eViewerMode = eViewerMode;
	m_sViewerMode = tr("UNKNOWN");
	if (eViewerMode == Viewer::RAW_mode)    m_sViewerMode = tr("RAW");
	else
	if (eViewerMode == Viewer::RENDER_mode) m_sViewerMode = tr("RENDER");
	else
	if (eViewerMode == Viewer::EDIT_mode)   m_sViewerMode = tr("EDIT");

	// -- speed up toggling to EDIT_mode (it works in RENDER mode also)
	if (eViewerMode == Viewer::EDIT_mode) {
		if (! bTextViewerIsEmpty && ! m_sMainBuffer.isEmpty()) {
			m_pTextEdit->setReadOnly(false);
			m_pTextEdit->moveCursor(QTextCursor::Start);
			m_pTextEdit->document()->setModified(false);
			updateStatus();
			return;
		}
	}

	if (eReadingMode == ALL || (eReadingMode == APPEND && bTextViewerIsEmpty && m_sMainBuffer.length() <= m_sTxtBuffer.length())) {
		//setModeOfView(modeOfView); // shows/hides actions
		//m_pEditActions->setVisible(((m_eViewerMode= eViewerMode) == Viewer::EDIT_mode));

    	if (eViewerMode != RENDER_mode) {
			m_pSyntaxHighlighter->setSyntaxHighlighting((m_bAutoSH) ? SyntaxHighlighter::AUTO : m_eForceKindOfSH, sMime);
            m_pHighlightingParentA->setVisible((m_pSyntaxHighlighter->currentHighlighting() != SyntaxHighlighter::NONE));
			updateHighlightingAction();
		}
		else // mode 'RENDER_mode' - no need the highlighting menu
            m_pHighlightingParentA->setVisible(false);

		// --- set the EOL info
		EOLtype eKindOfEOL = UNIX_eol;
		int p = baBinBuffer.indexOf(QString(QChar(0x0D)).toUtf8());
		if (p != -1)
			if (baBinBuffer.indexOf(QString(QChar(0x0A)).toUtf8(), p) != -1)
				eKindOfEOL = WINandDOS_eol;
		setEoLtype(eKindOfEOL);
	}

	if (! bTextViewerIsEmpty && eReadingMode != APPEND) {
		if (m_sMainBuffer.length() <= m_sTxtBuffer.length()) {
			if (m_sMainBuffer.isEmpty())
				m_sTxtBuffer = m_pTextEdit->toPlainText(); // toggling from RENDER to RAW made in buffer will be only visible text in RENDER mode and not source
			else
				m_sTxtBuffer = m_sMainBuffer;
		}
	}
	if (m_sTxtBuffer.isEmpty() && eReadingMode == ALL) // only for first callthisfunction
		m_sTxtBuffer = QString::fromLocal8Bit(baBinBuffer.data());
	else
	if (eReadingMode == APPEND) {
		m_sTxtBuffer = QString::fromLocal8Bit(baBinBuffer.data());
		if (! m_sPartTagBuf.isEmpty()) {
			m_sTxtBuffer.insert(0, m_sPartTagBuf); m_sPartTagBuf = ""; // faster would be work appending, then this command should be earlier, and here should be appending to mMainBuffer
		}
	}

	if (eReadingMode == ALL || (eReadingMode == APPEND && bTextViewerIsEmpty)) {
		m_eTextFormat = Qt::PlainText;
		if (eViewerMode == RENDER_mode) {
			if (sMime == "text/html" || sMime == "application/x-php")
				m_eTextFormat = Qt::RichText;
		}
		else
		if (eViewerMode == RAW_mode || eViewerMode == EDIT_mode) {
			//FIXME mTextEdit->setPaper(QBrush(Qt::white)); // background always must have white color
			setTabWidth(m_nTabWidth);
		}
	}

	if (eReadingMode == ALL || (eReadingMode == APPEND && bTextViewerIsEmpty)) {
		m_pTextEdit->setReadOnly((eViewerMode != EDIT_mode));
		m_pTextEdit->setUndoRedoEnabled(false);
		if (m_sMainBuffer.length() <= m_sTxtBuffer.length() || eReadingMode == ALL)
			m_pTextEdit->clear();
		if (m_eTextFormat == Qt::RichText && eReadingMode == APPEND) {
			m_sPartTagBuf = m_sTxtBuffer.right(m_sTxtBuffer.length()-m_sTxtBuffer.lastIndexOf('>')-1);
			m_sTxtBuffer.remove(m_sTxtBuffer.length()-m_sPartTagBuf.length(), m_sPartTagBuf.length());
		}
		if (m_pTextEdit->document()->isEmpty() && m_sMainBuffer.length() <= m_sTxtBuffer.length())
			m_sMainBuffer = m_sTxtBuffer;
		if (m_eTextFormat == Qt::RichText) {
// 			m_pTextEdit->setTextFormat(m_eTextFormat);
			m_pTextEdit->setAcceptRichText(true);
			if (m_sMainBuffer.length() <= m_sTxtBuffer.length())
				m_pTextEdit->setText(m_sTxtBuffer);
			else
				m_pTextEdit->setText(m_sMainBuffer); // need to update all html, code becouse QTextEdit fixed incomplete code made it goes bad when it is append new code
			//mTextEdit->setText(mTxtBuffer);
		}
		else { // PlainText
            qDebug() << "TextViewer::updateContents" << "DEBUG: m_sMainBuffer.length():" << m_sMainBuffer.length() << "m_sTxtBuffer.length():" << m_sTxtBuffer.length();
// 			m_pTextEdit->setTextFormat(m_eTextFormat);
			//m_pTextEdit->setAcceptRichText(true);

			if (m_sMainBuffer.length() <= m_sTxtBuffer.length()) {
				//m_pTextEdit->setText(m_sTxtBuffer);
				m_pTextEdit->textCursor().movePosition(QTextCursor::EndOfBlock); // workaround which make able to skip paragraphs (w1)
				m_pTextEdit->textCursor().insertText(m_sTxtBuffer); // (w1) (inserts plain text)
			}
			else {
				m_pTextEdit->clear();
				m_pTextEdit->setText(m_sMainBuffer);
			}
			m_pTextEdit->document()->setModified(false);
			if (eViewerMode == EDIT_mode)
				m_pTextEdit->moveCursor(QTextCursor::Start);
		}
		m_pTextEdit->setUndoRedoEnabled(true);
	}
	else
	if (eReadingMode == APPEND) {
		m_pTextEdit->setUndoRedoEnabled(false);
		if (m_eTextFormat == Qt::RichText) {
			m_sPartTagBuf = m_sTxtBuffer.right(m_sTxtBuffer.length()-m_sTxtBuffer.lastIndexOf('>')-1);
			m_sTxtBuffer.remove(m_sTxtBuffer.length()-m_sPartTagBuf.length(), m_sPartTagBuf.length());
		}
		m_sMainBuffer += m_sTxtBuffer;
		if (m_eTextFormat == Qt::PlainText) {
			m_pTextEdit->textCursor().movePosition(QTextCursor::EndOfBlock); // workaround which make able to skip paragraphs (w1)
			m_pTextEdit->textCursor().insertText(m_sTxtBuffer); // (w1) (inserts plain text)
		}
		else
			m_pTextEdit->append(m_sTxtBuffer);

		m_pTextEdit->document()->setModified(false);
		m_pTextEdit->setUndoRedoEnabled(true);
	}

	if (eViewerMode != Viewer::EDIT_mode)
		m_pTextEdit->moveCursor(QTextCursor::Start);

	updateStatus();
}


void TextViewer::zoom( bool bZoomIn )
{
	if (bZoomIn)
		m_pTextEdit->zoomIn(1);
	else
		m_pTextEdit->zoomOut(1);

	uint currentSize = m_pTextEdit->currentFont().pointSizeF();
// 	qDebug() << "TextViewer::zoom. bZoomIn:" << bZoomIn << "currentSize:" << currentSize;

	if (bZoomIn) {
		if (currentSize < 24)
			m_pZoomOutAT->setEnabled(true);
		else
			m_pZoomInAT->setEnabled(false);
	}
	else {
		if (currentSize > 8)
			m_pZoomInAT->setEnabled(true);
		else
			m_pZoomOutAT->setEnabled(false);
	}

	//m_pSyntaxHighlighter->setFontSize(currentSize); // works very slow

	QString sZoomType = bZoomIn ? tr("Zoom in") : tr("Zoom out");
	emit signalUpdateStatus(QString(tr("Current font size")+" : %1  (%2)").arg(currentSize).arg(sZoomType), 3000, true);
}


// --- TODO: automation of saving to file from filesystem
bool TextViewer::saveToFile( const QString &sFileName )
{
	QFile file(sFileName);
	if (! file.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
		bool bChecked = false; // fake
		MessageBox::critical(this, tr("Save file")+" - QtCommander 2", "",
		 "\""+ sFileName +"\""+"\n\n"+ tr("Cannot write to this file"), bChecked
		);
		return false;
	}
	m_pTextEdit->document()->setModified(false);
	m_bDocModified = false;

	QTextStream stream(&file);
	stream << m_pTextEdit->toPlainText();
	file.close();

	return true;
}

void TextViewer::updateStatus()
{
	QString sMessage = tr("Not recognized format");
	int weight = m_pTextEdit->toPlainText().length();

	if (m_eViewerMode == RAW_mode || m_eViewerMode == RENDER_mode)
		sMessage = QString("%1 "+tr("bytes, mode: ")+"%2").arg(weight).arg(m_sViewerMode);

	if (m_eViewerMode == EDIT_mode) {
		QTextCursor cur = m_pTextEdit->textCursor();
		sMessage = QString(tr("row")+": %1  "+tr("column")+": %2    "+tr("byte")+": %3/%4  %5, mode: %6")
			.arg(cur.blockNumber() + 1).arg(cur.columnNumber() + 1).arg(cur.position()).arg(weight).arg(m_sInsStatStr.toLatin1().data()).arg(m_sViewerMode);
	}

	if (m_eViewerMode == Viewer::EDIT_mode) {
		if (weight != m_sMainBuffer.length()) {
			if (! m_bDocModified) {
				emit signalDocumentChanged(true);
				m_bDocModified = true;
			}
		}
		else {
			m_bDocModified = false;
 			emit signalDocumentChanged(false);
		}
	}

// 	qDebug() << "TextViewer::updateStatus." << "m_eViewerMode:" << toString(m_eViewerMode) <<  "msg:" << sMessage;
	emit signalUpdateStatus(sMessage, 0, false); // doesn't save prev.msg.
}

void TextViewer::getCurrentSelection( QString &sSelections )
{
	if (! m_pTextEdit->textCursor().hasSelection())
		return;

	sSelections = m_pTextEdit->textCursor().selectedText();
	if (m_eViewerMode == RENDER_mode)
		sSelections.remove(0, 20); // remove string <!--StartFragment-->

	int id = 0;
	if (sSelections.length() > 16)
		sSelections = sSelections.left(16);
	if ((id= sSelections.indexOf('\n')) != -1)
		sSelections = sSelections.left(id-1);
}


void TextViewer::setEncoding( const QString &sEncoding )
{
	QString enc = sEncoding.section('(', 1,1);
	if (enc.isEmpty())
		enc = "Automatic";
	else
		enc.remove(enc.length()-1, 1); // remove ')'

	qDebug("TextViewer::setEncoding. To '%s' (not yet implemented)", enc.toLatin1().data());
/* --- from kate ---
  // encoding menu, start with auto checked !
  m_setEncoding = new KSelectAction(i18n("Set &Encoding"), 0, ac, "set_encoding");
  list = KGlobal::charsets()->descriptiveEncodingNames();
  list.prepend(i18n("Auto"));
  m_setEncoding->setItems(list);
  m_setEncoding->setCurrentItem (0);
  connect(m_setEncoding, &activated, this, &slotSetEncoding);

  NOTE: smplayer has encoding list and implemented autodetection of encoding
*/
}


void TextViewer::updateHighlightingAction()
{
	SyntaxHighlighter::SyntaxHighlighting sh = m_pSyntaxHighlighter->currentHighlighting();

//mNoneA, *mCCppA, *mHtmlA, *mBashA, *mPerlA;
	if (sh == SyntaxHighlighter::NONE) {
		m_pNoneA->setChecked(true);
	}
	else if (sh == SyntaxHighlighter::CCpp) {
		m_pCCppA->setChecked(true);
	}
	else if (sh == SyntaxHighlighter::HTML) {
		m_pHtmlA->setChecked(true);
	}
// 	else if (sh == SyntaxHighlighter::BASH) {
// 		m_pBashA->setChecked(true);
// 	}
// 	else if (sh == SyntaxHighlighter::PERL) {
// 		m_pPerlA->setChecked(true);
// 	}
}


void TextViewer::findNext()
{
	// FIXME: After found document is not scrolled to string pozition
	if (m_sFindNextSearchString.isEmpty()) {
		slotShowFindBar();
		return;
	}
// 	qDebug() << "TextViewer::findNext. FindCaseSensitively:" << QTextDocument::FindCaseSensitively << "FindWholeWords:" << QTextDocument::FindWholeWords << "FindBackward:" << QTextDocument::FindBackward;
	QTextDocument::FindFlags flags;
	if (m_bCaseSensitive || m_pFindReplaceTextBar->caseSensitive())
		flags |= QTextDocument::FindCaseSensitively;
	if (m_bWholeWordsOnly || m_pFindReplaceTextBar->findWholeWordsOnly())
		flags |= QTextDocument::FindWholeWords;
	if (! m_bFindForward)
		flags |= QTextDocument::FindBackward;

	m_PrevResultOfFind = ! m_bFromBegin;
	m_sPrevSearchStr = m_sFindNextSearchString;
	if (m_pFindReplaceTextBar->regularExpression())
		m_bFromBegin = ! m_pTextEdit->find(QRegExp(m_sFindNextSearchString), flags);
	else
		m_bFromBegin = ! m_pTextEdit->find(m_sFindNextSearchString, flags);
	m_pFindReplaceTextBar->slotUpdateMatchingStatus((m_bFromBegin ? FindReplaceTextBar::MS_notMatch : FindReplaceTextBar::MS_match));

	m_bFindOnce = false;
	if (! m_bFromBegin) {
		m_bFindOnce = true;
		return;
	}

	FindReplaceTextBar::SearchDirection eSD = m_pFindReplaceTextBar->searchDirection();
	// only if user walks through document then show error message
	if (eSD == FindReplaceTextBar::SD_home || eSD == FindReplaceTextBar::SD_up || eSD == FindReplaceTextBar::SD_down || eSD == FindReplaceTextBar::SD_end) {
		QString s0 = (m_bFindForward) ? tr("end")   : tr("start");
		QString s1 = (m_bFindForward) ? tr("begin") : tr("end");
		if (! m_pFindReplaceTextBar->startFromCursor() || m_bBtnContinueHasPressed) {
			//MessageBox::information(this, tr("Find text")+" - QtCommander 2", m_sFindNextSearchString+"\n\n"+tr("Text not found"));
			//qDebug() << "TextViewer::findNext. (1). m_PrevResultOfFind:" << m_PrevResultOfFind;
			m_pFindReplaceTextBar->setMessage(FindReplaceTextBar::MT_Error, tr("Search reached")+" "+s0+" "+tr("of the document"), false, tr("Continue from")+" "+s1);
			if (m_PrevResultOfFind && m_sPrevSearchStr == m_sFindNextSearchString) {
				m_pFindReplaceTextBar->slotUpdateMatchingStatus(FindReplaceTextBar::MS_match);
				m_bFromBegin = false; // to set correct value in m_PrevResultOfFind in case of next search
			}
			return;
		}
		else {
			m_pFindReplaceTextBar->setMessage(FindReplaceTextBar::MT_Error, tr("Search reached")+" "+s0+" "+tr("of the document"), false, tr("Continue from")+" "+s1);
			//qDebug() << "TextViewer::findNext. (2). m_PrevResultOfFind:" << m_PrevResultOfFind;
			if (m_PrevResultOfFind && m_sPrevSearchStr == m_sFindNextSearchString) {
				m_pFindReplaceTextBar->slotUpdateMatchingStatus(FindReplaceTextBar::MS_match);
				m_bFromBegin = false; // to set correct value in m_PrevResultOfFind in case of next search
			}
		}
	}
}

void TextViewer::replaceNext()
{
	int nSelStart, nSelEnd, nReplacedCount = 0;
	while (true) {
		findNext(); // found string is highlighted
		m_cursor = m_pTextEdit->textCursor();
		if (! m_cursor.hasSelection()) {
			m_pFindReplaceTextBar->setMessage(FindReplaceTextBar::MT_Error, tr("Search reached end of the document"), true);
			break;
		}

        nSelStart = m_cursor.selectionStart();
		nSelEnd   = m_cursor.selectionEnd();
// 		qDebug() << "TextViewer::replaceNext. start:" << nSelStart << "end:" << nSelEnd;
		if (nSelStart == nSelEnd)
			break;

		if (m_eReplaceType == FindReplaceTextBar::RT_ReplaceNextOnly) {
			m_pFindReplaceTextBar->setMessage(FindReplaceTextBar::MT_Info, tr("Replace highlighted occurence?"), false, tr("Yes"), tr("No"));
			break;
		}
		// if replace all
		m_cursor.insertText(m_sNewString);
		nReplacedCount++;
	}

	if (nReplacedCount > 0)
		m_pFindReplaceTextBar->slotUpdateMatchingStatus(FindReplaceTextBar::MS_match);
	if (m_eReplaceType == FindReplaceTextBar::RT_ReplaceAll)
		m_pFindReplaceTextBar->setMessage(FindReplaceTextBar::MT_Info, QString("%1 ").arg(nReplacedCount)+tr("replace made"));
}


void TextViewer::createListBox()
{
	m_pCompletionListBox = new QListWidget(m_pTextEdit);
	m_pCompletionListBox->setWindowFlags(m_pCompletionListBox->windowFlags() | Qt::Popup);
	m_pCompletionListBox->installEventFilter(this);
	connect<void(QListWidget::*)(QListWidgetItem *)>(m_pCompletionListBox, &QListWidget::itemClicked,  this, &TextViewer::slotItemChosen);
}

void TextViewer::adjustListBoxSize( int maxHeight, int maxWidth )
{
	if (! m_pCompletionListBox->count())
		return;

	QFontMetrics fm(m_pCompletionListBox->font());
	int totalHeight = fm.height() * (m_pCompletionListBox->count() + 1);
	int height = qMin(totalHeight, maxHeight);
	if (m_pCompletionListBox->height() > height)
		m_pCompletionListBox->setFixedHeight(height);
	if (m_pCompletionListBox->width() > maxWidth)
		m_pCompletionListBox->setFixedWidth(maxWidth);
}


void TextViewer::applyKeyShortcutsForActions( KeyShortcuts *pKeyShortcuts )
{
	if (pKeyShortcuts != nullptr)
		m_pKS = pKeyShortcuts;

	// Edut.MenuBar
	m_pFindAM->setShortcut(m_pKS->keyCode(Find_TextViewer));
	m_pFindNextAM->setShortcut(m_pKS->keyCode(FindNext_TextViewer));
	m_pReplaceAM->setShortcut(m_pKS->keyCode(Replace_TextViewer));
	m_pGotoLineAM->setShortcut(m_pKS->keyCode(GoToLine_TextViewer));
	m_pUndoAM->setShortcut(m_pKS->keyCode(Undo_TextViewer));
	m_pRedoAM->setShortcut(m_pKS->keyCode(Redo_TextViewer));
	m_pCutAM->setShortcut(m_pKS->keyCode(Cut_TextViewer));
	m_pCopyAM->setShortcut(m_pKS->keyCode(Copy_TextViewer));
	m_pPasteAM->setShortcut(m_pKS->keyCode(Paste_TextViewer));
	m_pSelectAllAM->setShortcut(m_pKS->keyCode(SelectAll_TextViewer));
	// View.MenuBar
	m_pPrintAM->setShortcut(m_pKS->keyCode(Print_TextViewer));
	m_pZoomInAM->setShortcut(m_pKS->keyCode(ZoomIn_TextViewer));
	m_pZoomOutAM->setShortcut(m_pKS->keyCode(ZoomOut_TextViewer));

	// Ecit.Tool Bar
	m_pFindAT->setToolTip(m_pFindAT->toolTip()+" ("+m_pKS->keyStr(Find_TextViewer)+")");
	m_pFindNextAT->setToolTip(m_pFindNextAT->toolTip()+" ("+m_pKS->keyStr(FindNext_TextViewer)+")");
	m_pReplaceAT->setToolTip(m_pReplaceAT->toolTip()+" ("+m_pKS->keyStr(Replace_TextViewer)+")");
	m_pGotoLineAT->setToolTip(m_pGotoLineAT->toolTip()+" ("+m_pKS->keyStr(GoToLine_TextViewer)+")");
	m_pUndoAT->setToolTip(m_pUndoAT->toolTip()+" ("+m_pKS->keyStr(Undo_TextViewer)+")");
	m_pRedoAT->setToolTip(m_pRedoAT->toolTip()+" ("+m_pKS->keyStr(Redo_TextViewer)+")");
	m_pCutAT->setToolTip(m_pCutAT->toolTip()+" ("+m_pKS->keyStr(Cut_TextViewer)+")");
	m_pCopyAT->setToolTip(m_pCopyAT->toolTip()+" ("+m_pKS->keyStr(Copy_TextViewer)+")");
	m_pPasteAT->setToolTip(m_pPasteAT->toolTip()+" ("+m_pKS->keyStr(Paste_TextViewer)+")");
	m_pSelectAllAT->setToolTip(m_pSelectAllAT->toolTip()+" ("+m_pKS->keyStr(SelectAll_TextViewer)+")");
	// View.ToolBar
	m_pPrintAT->setToolTip(m_pPrintAT->toolTip()+" ("+m_pKS->keyStr(Print_TextViewer)+")");
	m_pZoomInAT->setToolTip(m_pZoomInAT->toolTip()+" ("+m_pKS->keyStr(ZoomIn_TextViewer)+")");
	m_pZoomOutAT->setToolTip(m_pZoomOutAT->toolTip()+" ("+m_pKS->keyStr(ZoomOut_TextViewer)+")");
}


void TextViewer::updateEditMenuBarActions( bool bEditMode )
{
	m_pReplaceAT->setVisible(bEditMode);
	m_pGotoLineAT->setVisible(bEditMode);
	m_pUndoAT->setVisible(bEditMode);
	m_pRedoAT->setVisible(bEditMode);
	m_pCutAT->setVisible(bEditMode);
	m_pPasteAT->setVisible(bEditMode);
	// --
	m_pReplaceAM->setVisible(bEditMode);
	m_pGotoLineAM->setVisible(bEditMode);
	m_pUndoAM->setVisible(bEditMode);
	m_pRedoAM->setVisible(bEditMode);
	m_pCutAM->setVisible(bEditMode);
	m_pPasteAM->setVisible(bEditMode);
}

void TextViewer::showContextMenu( const QPoint &pos )
{
// 	qDebug() << "TextViewer::showContextMenu()";
	QMenu *pCtxMenu = m_pTextEdit->createStandardContextMenu();
	if (pCtxMenu == nullptr) {
		qDebug() << "TextViewer::showContextMenu. Internal ERROR. Cannot get StandardContextMenu!";
		return;
	}
	QString sFindKCode    = (m_pKS != nullptr) ? m_pKS->keyStr(Find_TextViewer)    : "CTRL+F";
	QString sReplaceKCode = (m_pKS != nullptr) ? m_pKS->keyStr(Replace_TextViewer) : "CTRL+R";
	QString sSelection;
	// -- rebuild context menu to replace paste option coming from standard context menu
	QMenu *pNewCtxMenu = new QMenu(this);
	foreach (QAction *action, pCtxMenu->actions()) {
		if (action->text() == tr("&Paste")) { // replace standard paste with new one
			pNewCtxMenu->addAction(m_pPasteAM);
			continue;
		}
		pNewCtxMenu->addAction(action);
	}

	if (! m_sCurrentLink.isEmpty()) {
		pNewCtxMenu->addSeparator();
		pNewCtxMenu->addAction(icon("edit/copy"), tr("Copy &link location"), this, &TextViewer::slotCopyLinkToTheClipboard);
	}

	if (m_pTextEdit->textCursor().hasSelection()) {
		getCurrentSelection(sSelection);
		pNewCtxMenu->addSeparator(); // gets selected text and and alternatively cuts it if is too long for menu
		pNewCtxMenu->addAction(icon("edit/find"), tr("&Find")+": \""+ sSelection +"\"\t"+ sFindKCode, this, &TextViewer::slotShowFindBar);

		if (m_eViewerMode == EDIT_mode)
			pNewCtxMenu->addAction(icon("edit/find-replace"), tr("&Replace")+": \""+ sSelection +"\"\t"+ sReplaceKCode, this, &TextViewer::slotShowReplaceBar);
	}

	pNewCtxMenu->exec(m_pTextEdit->mapToGlobal(QPoint(pos.x(), pos.y()+2)));
	delete pNewCtxMenu;
	delete pCtxMenu;
	// -- clear selection
// 	QTextCursor txtCursor = m_pTextEdit->cursorForPosition(pos);
// 	m_pTextEdit->textCursor().setPosition(txtCursor.position());
// 	m_pTextEdit->setTextCursor(txtCursor);
}

bool TextViewer::viewerModeAvailable( const QString &sMime, ViewerMode eViewerMode ) const
{
// 	qDebug() << "TextViewer::viewerModeAvailable. sMime:" << sMime << eViewerMode;
	if (eViewerMode == RENDER_mode) {
		if (   sMime == "text/html"
			|| sMime == "application/php"
			//|| sMime == "application/xhtml+xml" // doesn't render by QTextEdit
			)
			return true;
	}
	else
	if (eViewerMode == RAW_mode || eViewerMode == EDIT_mode)
		return true;

	return false;
}

void TextViewer::setFindReplaceBarVisible( bool bVisible )
{
	// deselect recently found text (clear selection)
	QTextCursor cur = m_pTextEdit->textCursor();
	cur.clearSelection();
	m_pTextEdit->setTextCursor(cur);
	// ... and hide FindReplace bar
	m_pFindReplaceTextBar->setVisible(bVisible);
}

		// ----------- SLOTs -----------

void TextViewer::slotChangeEOL( QAction *pAction )
{
	setEoLtype((pAction == m_pUnixEolA) ? UNIX_eol : WINandDOS_eol);
}


void TextViewer::slotAutoEncoding()
{
	bool bChecked = m_pAutomaticEncA->isChecked();
	m_pManuallyMenu->setEnabled(! bChecked);
	if (bChecked)
		setEncoding("Automatic");
	else {
		QAction *pAction = m_pManuallyCodingActions->checkedAction();
		if (pAction != nullptr)
			setEncoding(pAction->text());
	}
}

void TextViewer::slotChangeEncoding( QAction *pAction )
{
/*	if (pAction == mCeIso2EncA) {
	}
	else if (pAction == mCeCpEncA) {
	}*/
	setEncoding(pAction->text());
}


void TextViewer::slotChangeSyntaxHighlighting( QAction *pAction )
{
	if (pAction == m_pNoneA)
		m_eForceKindOfSH = SyntaxHighlighter::NONE;
	else
	if (pAction == m_pCCppA)
		m_eForceKindOfSH = SyntaxHighlighter::CCpp;
	else
	if (pAction == m_pHtmlA)
		m_eForceKindOfSH = SyntaxHighlighter::HTML;
	else
	if (pAction == m_pBashA)
		m_eForceKindOfSH = SyntaxHighlighter::BASH;
	else
	if (pAction == m_pPerlA)
		m_eForceKindOfSH = SyntaxHighlighter::PERL;

	m_pSyntaxHighlighter->setSyntaxHighlighting(m_eForceKindOfSH);
	m_pSyntaxHighlighter->rehighlight();
}

/*
void TextViewer::slotApplyTextViewerSettings( const TextViewerSettings &tvs )
{
	m_nFontFamily = tvs.fontFamily;
	m_nFontSize   = tvs.fontSize;
	m_nFontBold   = tvs.fontBold;
	m_nFontItalic = tvs.fontItalic;
	m_nTabWidth   = tvs.tabWidth;

	setTabWidth(m_nTabWidth);
	setFont(m_nFontFamily, m_nFontSize, m_nFontBold, m_nFontItalic);

	// do'nt change highlighting for different than current kind of highlighting
	if (tvs.currentSyntaxHighlighting != m_pSyntaxHighlighter->currentHighlighting())
		return;

	for (int i=0; i<11; i++)
		m_pSyntaxHighlighter->initAttributsTable(i, tvs.shAttrib[i].color, tvs.shAttrib[i].bold, tvs.shAttrib[i].italic);

	m_pSyntaxHighlighter->rehighlight();
}*/


void TextViewer::slotSaveSettings()
{
	QString sFont = m_pTextEdit->fontFamily()+","+QString::number(m_pTextEdit->fontPointSize());

	Settings settings;
	settings.setValue("textViewer/Font", sFont);
	settings.setValue("textViewer/TabWidth", m_nTabWidth);
}


void TextViewer::slotGoToLine()
{
	bool bParam;
	QString sInitLineNum;
	if (m_slListOfLineNumbers.size() > 0)
		      sInitLineNum = m_slListOfLineNumbers[0];
	QString sNewLineNum = InputTextDialog::getText(this, InputTextDialog::GO_TO_LINE, sInitLineNum, bParam);

	if (! sNewLineNum.isEmpty()) {
		uint newLine = sNewLineNum.toUInt(&bParam);
		const uint maxLines = m_pTextEdit->document()->blockCount();
		bool bNotFound = true;
		if (newLine == 0)
			newLine = 1;
		else
		if (newLine > maxLines)
			newLine = maxLines;

		sNewLineNum = QString::number(newLine); // need to add (below) correct str.

		for (int i = 0; i < m_slListOfLineNumbers.count(); ++i) {
			if (m_slListOfLineNumbers[i] == sNewLineNum) {
				bNotFound = false;
				break;
			}
		}
		if (bNotFound)
			m_slListOfLineNumbers.append(sNewLineNum);

		QTextCursor cur = m_pTextEdit->textCursor();
		int lines = newLine - cur.blockNumber() - 1;
		if (lines > 0)
			cur.movePosition(QTextCursor::Down, QTextCursor::MoveAnchor,  lines);
		else
			cur.movePosition(QTextCursor::Up,   QTextCursor::MoveAnchor, -lines);

		m_pTextEdit->setTextCursor(cur);
	}
}


void TextViewer::slotShowFindBar()
{
	QString sCurrentSelection = "";
	if (m_pTextEdit->textCursor().hasSelection())
		sCurrentSelection = m_pTextEdit->textCursor().selectedText();

	m_pFindReplaceTextBar->init(sCurrentSelection, (m_eViewerMode == EDIT_mode));
	m_pFindReplaceTextBar->setVisible(true);

	m_bBtnContinueHasPressed = false;
}

void TextViewer::slotSearch( const QString &sSearchedString, bool bFromBeginOrCont, FindReplaceTextBar::StartFrom eStartFrom )
{
	if (sSearchedString.isEmpty()) {
		// deselect recently found string
		QTextCursor cur = m_pTextEdit->textCursor();
		cur.clearSelection();
		m_pTextEdit->setTextCursor(cur);
		// move cursor to begin if it's required
		if (bFromBeginOrCont && ! m_pFindReplaceTextBar->startFromCursor())
			m_pTextEdit->moveCursor(QTextCursor::Start);
		return;
	}
	FindReplaceTextBar::SearchDirection eSearchDirection = m_pFindReplaceTextBar->searchDirection();
// 	qDebug() << "TextViewer::slotSearch. SearchedString:" << sSearchedString << "SearchDirection:" << m_pFindReplaceTextBar->toString(eSearchDirection) << "FromBeginOrCont:" << bFromBeginOrCont;

	if (eStartFrom == FindReplaceTextBar::SF_ignoreFlg) {
// 		qDebug() << "TextViewer::slotSearch. FindReplaceTextBar::SF_ignoreFlg, startFromCursor:" << m_pFindReplaceTextBar->startFromCursor();
		if (bFromBeginOrCont && ! m_pFindReplaceTextBar->startFromCursor())
			m_pTextEdit->moveCursor(QTextCursor::Start);
// 		qDebug() << "TextViewer::slotSearch. set QTextCursor::Start";
	}
	else { // force search direction
// 		qDebug() << "TextViewer::slotSearch. force search direction";
		if (eStartFrom == FindReplaceTextBar::SF_FromVeryBegining) {
			eSearchDirection = FindReplaceTextBar::SD_down;
			if (bFromBeginOrCont)
				m_pTextEdit->moveCursor(QTextCursor::Start);
		}
		else
		if (eStartFrom == FindReplaceTextBar::SF_FromEnd) {
			eSearchDirection = FindReplaceTextBar::SD_up;
			if (bFromBeginOrCont)
				m_pTextEdit->moveCursor(QTextCursor::End);
		}
	}

	m_UpOrDownPressed = (eSearchDirection != FindReplaceTextBar::SD_down_inc); // should be also check: SD_none and SD_behind
// 	qDebug() << "TextViewer::slotSearch. m_UpOrDownPressed:" << m_UpOrDownPressed << m_pFindReplaceTextBar->toString(eSearchDirection);
	m_sFindNextSearchString = sSearchedString;
	m_bFindForward = (eSearchDirection == FindReplaceTextBar::SD_down || eSearchDirection == FindReplaceTextBar::SD_end || eSearchDirection == FindReplaceTextBar::SD_down_inc);
// 	qDebug() << "TextViewer::slotSearch. m_bFindForward:" << m_bFindForward << "SearchedString:" << sSearchedString;
	m_sPrevSearchStr = m_sFindNextSearchString;
	m_PrevResultOfFind = ! m_bFromBegin;

	findNext();
}

void TextViewer::slotSetSearchFromCursor( bool bChecked )
{
// 	qDebug() << "TextViewer::slotSetSearchFromCursor" << bChecked;
	// deselect recently found text (clear selection)
	QTextCursor cur = m_pTextEdit->textCursor();
	cur.clearSelection();
	m_pTextEdit->setTextCursor(cur);
	m_pFindReplaceTextBar->slotUpdateMatchingStatus(FindReplaceTextBar::MS_none);
}

void TextViewer::slotFindWholeWords( bool bChecked )
{
// 	qDebug() << "TextViewer::slotFindWholeWords" << bChecked;
	// deselect recently found text (clear selection)
	QTextCursor cur = m_pTextEdit->textCursor();
	cur.clearSelection();
	m_pTextEdit->setTextCursor(cur);
	m_pFindReplaceTextBar->slotUpdateMatchingStatus(FindReplaceTextBar::MS_none);
}

void TextViewer::slotQuestionBtn1Clicked()
{
	if (m_pFindReplaceTextBar->replaceBarVisible()) {
		m_cursor.insertText(m_sNewString);
		replaceNext();
	}
	else {
		m_bBtnContinueHasPressed = false;

		FindReplaceTextBar::SearchDirection eSD = m_pFindReplaceTextBar->searchDirection();
		m_UpOrDownPressed = (eSD != FindReplaceTextBar::SD_down_inc);
		m_bFindForward = ((eSD == FindReplaceTextBar::SD_down || eSD == FindReplaceTextBar::SD_down_inc) && m_UpOrDownPressed);

		m_pTextEdit->moveCursor(m_bFindForward ? QTextCursor::Start : QTextCursor::End);
		findNext();
	}
}

void TextViewer::slotQuestionBtn2Clicked()
{
	if (m_pFindReplaceTextBar->replaceBarVisible())
		m_pFindReplaceTextBar->hide();
}


void TextViewer::slotShowReplaceBar()
{
	QString sCurrentSelection;
	if (m_pTextEdit->textCursor().hasSelection())
		sCurrentSelection = m_pTextEdit->textCursor().selectedText();

	m_pFindReplaceTextBar->init(sCurrentSelection, true, true); // enable "start search from cursor" checkBox and set as visible ReplaceBar
	m_pFindReplaceTextBar->setVisible(true);
}

void TextViewer::slotStartReplace( FindReplaceTextBar::ReplaceType eReplaceType, const QString &sSearchedString, const QString &sNewString )
{
	m_sFindNextSearchString = sSearchedString;
	m_eReplaceType = eReplaceType;
	m_sNewString = sNewString;

	m_bBtnContinueHasPressed = false;

	replaceNext();
}

void TextViewer::slotPrint()
{
	QPrinter printer(QPrinter::HighResolution);
	printer.setFullPage(true);
	QPrintDialog *pDlg = new QPrintDialog(&printer, this);
	if (m_pTextEdit->textCursor().hasSelection())
		pDlg->addEnabledOption(QAbstractPrintDialog::PrintSelection);
	pDlg->setWindowTitle(tr("Print Document"));
	if (pDlg->exec() == QDialog::Accepted) {
		emit signalUpdateStatus(tr("Printing..."));
		m_pTextEdit->print(&printer);
		emit signalUpdateStatus(tr("Printing completed"), 2000, false);
	} else {
		emit signalUpdateStatus(tr("Printing aborted"), 2000, false);
	}
	delete pDlg;
}


void TextViewer::slotCopyLinkToTheClipboard()
{ // All code of this function comes from Qt-3.1.1 source (QLineEdit::copy())
	disconnect(QApplication::clipboard(), &QClipboard::selectionChanged, this, 0);
	QApplication::clipboard()->setText(m_sCurrentLink, QClipboard::Clipboard);
	connect(QApplication::clipboard(), &QClipboard::selectionChanged, this, &TextViewer::clipboardChanged);
}


void TextViewer::slotCursorPositionChanged()
{
	if (m_eViewerMode == EDIT_mode)
		updateStatus();
}


void TextViewer::slotCompleteText()
{
	QTextCursor cur = m_pTextEdit->textCursor();
	Q_ASSERT(cur.block().isValid());

	int cursorPos = cur.position();
	cur.movePosition(QTextCursor::StartOfWord);
	int wordStart = cur.position();

	QString sContent = m_pTextEdit->toPlainText();
	m_sWordPrefix = sContent.mid(wordStart, cursorPos - wordStart);
	if (m_sWordPrefix.isEmpty())
		return;

	QStringList list = sContent.split(QRegExp("\\W+"));
	QMap<QString, QString> map;
	QStringList::Iterator it = list.begin();
	while (it != list.end()) {
		if ((*it).startsWith(m_sWordPrefix) && (*it).length() > m_sWordPrefix.length())
			map[(*it).toLower()] = *it;
		++it;
	}

	if (map.count() == 1) {
		m_pTextEdit->insertPlainText((*map.begin()).mid(m_sWordPrefix.length()));
	}
	else
	if (map.count() > 1) {
		if (! m_pCompletionListBox)
			createListBox();
		m_pCompletionListBox->clear();
		m_pCompletionListBox->insertItems(0, map.values());
		QPoint point = mapToGlobal(m_pTextEdit->cursorRect().topLeft());
		adjustListBoxSize(qApp->desktop()->height() - point.y(), width() / 2);
		m_pCompletionListBox->setCurrentRow(0);
		m_pCompletionListBox->move(point);
		m_pCompletionListBox->show();
		m_pCompletionListBox->setFocus();
	}
}

void TextViewer::slotItemChosen( QListWidgetItem *pItem )
{
	if (pItem != nullptr)
		m_pTextEdit->insertPlainText(pItem->text().mid(m_sWordPrefix.length()));

	m_pCompletionListBox->close();
	if (m_pParent != nullptr)
		m_pParent->raise(); // QWidget::activateWindow

	m_pTextEdit->setFocus();
}


void TextViewer::slotPastePlainText()
{
	m_pTextEdit->insertPlainText(QApplication::clipboard()->text());
}


void TextViewer::slotApplyTextViewSettings( const TextViewSettings &textViewSettings, const QString &sCategory )
{
	qDebug() << "TextViewer::slotApplyTextViewSettings";
	if (sCategory == tr("Fonts")) {
		m_sFontFamily = textViewSettings.fontFamily;
		m_nFontSize   = textViewSettings.fontSize;
		m_bFontBold   = textViewSettings.fontBold;
		m_bFontItalic = textViewSettings.fontItalic;
		setFont(m_sFontFamily, m_nFontSize, m_bFontBold, m_bFontItalic); // set font for TextView obj.
		//m_pTextEdit->setFont(QFont(m_sFontFamily, m_nFontSize, (m_bFontBold ? QFont::Bold : QFont::Normal), m_bFontItalic)); // doesn't work
		m_pTextEdit->viewport()->update();

		qreal s = m_nFontSize;
		m_pTextEdit->setFontFamily(m_sFontFamily);
		m_pTextEdit->setFontItalic(m_bFontItalic);
		m_pTextEdit->setFontWeight(m_bFontBold ? QFont::Bold : QFont::Normal);
		m_pTextEdit->setFontPointSize(s);
		m_pSyntaxHighlighter->setFont(m_pTextEdit->currentFont());
		m_pSyntaxHighlighter->rehighlight();
	}
	else
	if (sCategory == tr("Syntax highlighting")) {
		qDebug() << "TextViewer::slotApplyTextViewSettings. Apply for Syntax highlighting. (TODO)";
	}
	else
	if (sCategory == tr("Other")) {
		qDebug() << "TextViewer::slotApplyTextViewSettings. Apply for Other. (TODO)";
	}
}

		// ----------- EVENTs -----------

bool TextViewer::keyPressed( QKeyEvent *pKeyEvent )
{
	if (m_pKS == nullptr || pKeyEvent == nullptr)
		return false;

	int key    = pKeyEvent->key();
	int keyMod = pKeyEvent->modifiers();
	QKeySequence ks = m_pKS->ke2ks(pKeyEvent);
// 	QKeySequence *ks = QKeySequence(m_pKeyShortcuts->ke2ks(pKeyEvent));
	//qDebug() << "TextViewer::keyPressed. document()->isModified():" << m_pTextEdit->document()->isModified();

	if (key == Qt::Key_Insert) {
		if (keyMod == Qt::CTRL) {
			m_pTextEdit->copy();
			return false; // eat event
		}
		else
		if (keyMod == Qt::ALT)
			return false; // eat event
		else
		if (keyMod == Qt::SHIFT) {
			slotPastePlainText();
			return false; // eat event
		}

		m_pTextEdit->setOverwriteMode(m_bOvrMode);
		m_bOvrMode = ! m_bOvrMode;
		m_sInsStatStr = (m_bOvrMode) ? "OVR" : "INS";
		updateStatus();
	}
	else
	if (ks.matches(m_pKS->keyCode(SelectAll_TextViewer))) {
		m_pTextEdit->selectAll();
		return false; // eat event
	}
	else
	if (ks.matches(m_pKS->keyCode(TextCompletion_TextViewer))) {
		slotCompleteText();
		return false; // eat event
	}
	else
	if (key == Qt::Key_Space) {
		if (keyMod == Qt::CTRL) {
			slotCompleteText();
			return false; // eat event
		}
	}
	else
	if (key == Qt::Key_Home) {
		if (keyMod == Qt::CTRL) {
			m_pTextEdit->moveCursor(QTextCursor::Start);
		}
	}
	else
	if (key == Qt::Key_End) {
		if (keyMod == Qt::CTRL) {
			m_pTextEdit->moveCursor(QTextCursor::End);
		}
	}
	else
	if (key == Qt::Key_V) {
		if (keyMod == Qt::CTRL) {
			slotPastePlainText();
			return false; // eat event
		}
	}

	return true;
}


void TextViewer::mouseMoveEvent( QMouseEvent *pMouseEvent )
{
	if (m_eViewerMode == RENDER_mode) {
		int xPos = pMouseEvent->pos().x();
		int yPos = pMouseEvent->pos().y();
		int width  = m_pTextEdit->width()  - (m_pTextEdit->verticalScrollBar()->isVisible()   ? m_pTextEdit->verticalScrollBar()->width()+3 : 0);
		int height = m_pTextEdit->height() - (m_pTextEdit->horizontalScrollBar()->isVisible() ? m_pTextEdit->horizontalScrollBar()->height()+3 : 2);

		if (yPos < 0 || xPos < 0 || xPos > width || yPos > height) {
			QApplication::restoreOverrideCursor();
			return;
		}

		QApplication::restoreOverrideCursor();
		if (! m_sCurrentLink.isEmpty())
			QApplication::setOverrideCursor(Qt::PointingHandCursor);

		emit signalUpdateStatus(m_sCurrentLink);
	}
}


void TextViewer::showEvent( QShowEvent * /*pEvent*/ )
{
	m_pTextEdit->setFocus();
}

bool TextViewer::eventFilter( QObject *o, QEvent *e )
{
	if (o == m_pTextEdit) {
		if (e->type() == QEvent::KeyPress) {
			if (! keyPressed((QKeyEvent *)e))
				return true; // eat current event
		}
		else
		if (e->type() == QEvent::FocusOut)
			QApplication::restoreOverrideCursor();
	}
	else
	if (o == m_pTextEdit->viewport()) {
		if (e->type() == QEvent::MouseButtonPress) {
			QMouseEvent *pMouseEvent = (QMouseEvent *)e;
			if (pMouseEvent->button() == Qt::RightButton)  {
				QContextMenuEvent event(QContextMenuEvent::Mouse, pMouseEvent->pos());
				QApplication::sendEvent(m_pTextEdit, &event);
 				return true; // eat current event to prevent resending event RightButtonPressed
			}
			else
			if (pMouseEvent->button() == Qt::MiddleButton/* || pMouseEvent->button() == Qt::MidButton*/)  {
				slotPastePlainText();
				return true;
			}
		}
		else
		if (e->type() == QEvent::MouseButtonRelease) {
			QMouseEvent *pMouseEvent = (QMouseEvent *)e;
			if (pMouseEvent->button() == Qt::MiddleButton /*|| pMouseEvent->button() == Qt::MidButton*/)  {
				return true; // eat event to prevent triggering of QTextEdit::paste()
			}
		}
		else
		if (e->type() == QEvent::MouseMove)
			mouseMoveEvent((QMouseEvent *)e);
		else
		if (e->type() == QEvent::ContextMenu) {
			QContextMenuEvent *cme = (QContextMenuEvent *)e;
			showContextMenu(cme->pos());
			return true; // eat event to prevent showing standard context menu
		}
	}
	else
	if (o == m_pCompletionListBox && e->type() == QEvent::KeyPress) {
		QKeyEvent *keyEvent = static_cast<QKeyEvent*>(e);
		int key = keyEvent->key();
		switch (key) {
		case Qt::Key_Enter:
		case Qt::Key_Return:
			slotItemChosen(m_pCompletionListBox->currentItem());
			return true;
		case Qt::Key_Escape:
			slotItemChosen();
			return true;
		}
	}

	return QWidget::eventFilter(o, e);  // standard event processing
}


} // namespace Preview
