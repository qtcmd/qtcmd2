/***************************************************************************
 *   Copyright (C) 2003 by Piotr Mierzwiñski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _TEXTVIEWER_H_
#define _TEXTVIEWER_H_

#include <QTextEdit>

#include "viewer.h" // includes also: keyshortcuts.h, plugindata.h, subsystemconst.h textviewsettings.h
#include "mimetype.h"
#include "iconcache.h"
#include "syntaxhighlighter.h"
#include "findreplacetextbar.h"

class QAction;
class QTextEdit;
class QListWidget;
class QActionGroup;
class QListWidgetItem;


namespace Preview {

/**
	@author Piotr Mierzwiński <piom@qtcmd.org>
*/
class TextViewer : public Viewer
{
	Q_OBJECT
public:
	/** Constructor of class.
	 * Here you can find initialisation of membersm creating printing object,
	 * text viewer object and sytax highlighting additionaly menu and tool bar
	 * are is initialisated and initialisation font coding.
	 */
	TextViewer();
	~TextViewer();

// 	void setParent( QWidget *pParent ) { m_pParent = pParent; }

	/**
	 * @param pParent pointer to parent this view
	 * @param pToolbarActionsLstForConfig actions group used in config dialog
	 * @oaram bToolbarVisible if true then tool bar will be visible otherwise will be hidden
	 * @param pKeyShortcuts pointer to KeyShortcuts object
	 */
	void init( QWidget *pParent, QList< QActionGroup *> *pToolbarActionsLstForConfig, bool bToolbarVisible, KeyShortcuts *pKeyShortcuts );

	/** Returns curret mode of viewer. For details please check: @see Preview::Viewer::ViewerMode
	 * @return viewer mode.
	 */
	ViewerMode  viewerMode() const { return m_eViewerMode; }
	bool        viewerModeAvailable( const QString &sMime, ViewerMode eViewerMode ) const;

	/** Clean view and used buffers.
	 */
	void        clear() {
		m_pTextEdit->clear();
		m_sTxtBuffer = ""; m_sMainBuffer = "";
	}

	/** Returns pointer to @em PluginData object.
	 * @return pointer to @em PluginData object.
	 */
	PluginData *pluginData() const { return m_pPluginData; };

	/** Sets pointer to @em PluginData object.
	 * @param pPluginData pointer to @em PluginData object.
	 */
	void        setPluginData( PluginData *pPluginData ) { m_pPluginData = pPluginData; }

	/** Updates key shortcut object with hardcoded key shortcuts.
	 * @param pKeyShortcuts pointer to object where are stored key shortcuts.
	 */
	void        initKeyShortcuts( KeyShortcuts *pKeyShortcuts );

	/** @brief Updates the viewer.
	 * @param eReadingMode - reading data mode, for details please check @see Preview::Viewer::ReadingMode
	 * @param eViewerMode - view mode, for details please check @see Preview::Viewer::ViewerMode
	 * @param sMime - opened file mime
	 * @param baBinBuffer - buffer contains data for filling view.
	 */
	void        updateContents( Viewer::ReadingMode eReadingMode, Viewer::ViewerMode eViewerMode, const QString &sMime, const QByteArray &baBinBuffer );

	/** Gets content of the view and put it to given buffer.
	 * @param baBinBuffer - buffer for data.
	 */
	void        getData( QByteArray &baBinBuffer ) {
		baBinBuffer = m_pTextEdit->toPlainText().toLocal8Bit();
	}

	/** Updates status bar with information about amout of read bytes.
	 */
	void        updateStatus();

	/** Saves data to file with given name.
	 * @param sFileName - file name,
	 * @return TRUE if save finish with success, otherwise FALSE.
	 */
	bool        saveToFile( const QString &sFileName );

	/** Returns status of modification in read file.
	 * @return TRUE if file has been modified, otherwise FALSE.
	 */
	bool        isModified() const { return m_pTextEdit->document()->isModified(); }

	/** Returns status of visibility the FindReplaceTextBar.
	 * @return TRUE if bar is visible, otherwise FALSE.
	 */
	bool        findReplaceBarVisible() const { return m_pFindReplaceTextBar->isVisible(); }

	/** Returns status of visibility the ErrorMessageBar.
	 * @return TRUE if bar is visible, otherwise FALSE.
	 */
 	bool        errorMessageBarVisible() const { return m_pFindReplaceTextBar->errorMessageBarVisible(); }

	/** Shows or hides the FindReplaceTextBar.
	 * @param bVisible if TRUE the shows the FindReplaceTextBar otherwise hide it.
	 */
	void        setFindReplaceBarVisible( bool bVisible );  // used in FileViewer::closeEvent

	/** Shows or hides the ErrorMessageBar.
	 * @param bVisible if TRUE the shows the FindReplaceTextBar otherwise hide it.
	 */
	void        setErrorMessageBarVisible( bool bVisible ) {
		m_pFindReplaceTextBar->setMessageBarVisible (bVisible);
	}

	/** Returns pointer to actions group related with Menu Bar, exactly Edit menu.
	 * @return pointer to actions group.
	 */
	QActionGroup *editMenuBarActions() const { return m_pEditMenuBarAG; }

	/** Returns pointer to actions group related with Menu Bar, exactly Viewer menu.
	 * @return pointer to actions group.
	 */
	QActionGroup *viewMenuBarActions() const { return m_pViewMenuBarAG; }

	/** Returns pointer to actions group related with Tool Bar, exactly Edit menu.
	 * @return pointer to actions group.
	 */
	QActionGroup *editToolBarActions() const { return m_pEditToolBarAG; }

	/** Returns pointer to actions group related with Tool Bar, exactly Viewer menu.
	 * @return pointer to actions group.
	 */
	QActionGroup *viewToolBarActions() const { return m_pViewToolBarAG; }


	/** Updates visibility of action widgets placed in menu bar and tool bar.
	 * @param bEditMode TRUE then action are visible, otherwise hidden.
	 */
	void        updateEditMenuBarActions( bool bEditMode );


private:
	PluginData *m_pPluginData;

	QWidget   *m_pParent;
	QTextEdit *m_pTextEdit;

	QString m_sTxtBuffer;
	ViewerMode m_eViewerMode;
	QString m_sViewerMode;

	QString m_sFontFamily;
	int  m_nFontSize, m_nTabWidth;
	bool m_bFontBold, m_bFontItalic;

	QString m_sInsStatStr, m_sCurrentLink;
	bool m_bOvrMode;

	SyntaxHighlighter *m_pSyntaxHighlighter;
	SyntaxHighlighter::SyntaxHighlighting m_eForceKindOfSH;
	bool m_bAutoSH;

	enum EOLtype { UNKNOWN_eol=0, UNIX_eol, WINandDOS_eol };
	EOLtype m_eEOLtype;

	// find and replace dialog
	QString m_sFindNextSearchString, m_sNewString;
	bool m_bCaseSensitive, m_bWholeWordsOnly;
	bool m_bFindForward;
	bool m_bFindOnce, m_bFromBegin, m_bBtnContinueHasPressed;

	QStringList m_slListOfLineNumbers; // used by 'Goto line' dialog

	QString m_sPartTagBuf, m_sMainBuffer;
	Qt::TextFormat m_eTextFormat;

	QString m_sWordPrefix;
	bool m_bDocModified;

	QListWidget *m_pCompletionListBox;

	QActionGroup *m_pViewMenuBarAG, *m_pEditMenuBarAG; // group for main window
	QActionGroup *m_pEditToolBarAG, *m_pViewToolBarAG; // group for main window

	QActionGroup *m_pEolActions, *mCodingFontsActions, *m_pManuallyCodingActions;
	QActionGroup *m_pSelectHighlightingActions;
	QAction *m_pUndoAT, *m_pRedoAT, *m_pCutAT, *m_pCopyAT, *m_pPasteAT, *m_pFindAT, *m_pFindNextAT,
			*m_pReplaceAT, *m_pGotoLineAT, *m_pSelectAllAT, *m_pPrintAT;
	QAction *m_pUndoAM, *m_pRedoAM, *m_pCutAM, *m_pCopyAM, *m_pPasteAM, *m_pFindAM, *m_pFindNextAM,
			*m_pReplaceAM, *m_pGotoLineAM, *m_pSelectAllAM, *m_pPrintAM;

	//QAction *m_pUndoTBA, *m_pRedoTBA, *m_pCutTBA, *m_pCopyTBA, *m_pPasteTBA, *m_pReplaceTBA, *m_pGotoLineTBA, *m_pSelectAllTBA;
	QAction *m_pZoomInAT, *m_pZoomOutAT;
	QAction *m_pZoomInAM, *m_pZoomOutAM;
	QAction *m_pNoneA, *m_pCCppA, *m_pHtmlA, *m_pBashA, *m_pPerlA;
	QAction *m_pUnixEolA, *m_pWinDosEolA;
	QAction *m_pAutomaticEncA, *m_pCeIso2EncA, *m_pCeCpEncA;
	KeyShortcuts *m_pKS;

	QMenu *m_pManuallyMenu;
	QAction *m_pHighlightingParentA;
	IconCache *m_pIconCache;

	FindReplaceTextBar *m_pFindReplaceTextBar;
	bool m_UpOrDownPressed, m_PrevResultOfFind;

	QString m_sPrevSearchStr;

	FindReplaceTextBar::ReplaceType m_eReplaceType;
	QTextCursor m_cursor;

	static const int MAX_FILE_VIEWER_ACT_ID = 1015;

	QMap <QString, QAction *> m_toolBarActionsMap;

	/** Ids for actions related with key shortcuts.
	*/
	enum ActionsOfTextViewer {
		Find_TextViewer=MAX_FILE_VIEWER_ACT_ID+1,
		FindNext_TextViewer,
		Replace_TextViewer,
		GoToLine_TextViewer,
		Undo_TextViewer,
		Redo_TextViewer,
		Cut_TextViewer,
		Copy_TextViewer,
		Paste_TextViewer,
		SelectAll_TextViewer,
		Print_TextViewer,
		ZoomIn_TextViewer,
		ZoomOut_TextViewer,
		TextCompletion_TextViewer
	};


	QIcon icon( const QString &sIconName ) const {
		return m_pIconCache->icon(sIconName);
	}

	void updateToolBarsByConfigFile();

	/** Initializes actions for main menu and tool bar placed in main window.
	 * Pointer of those menus you can get by following methods:
	 * @em editMenuBarActions() , @em viewerMenuBarActions() ,
	 * @em editToolBarActions() , @em viewerToolBarActions()
	 * @param pToolbarActionsLstForConfig actions group used in config dialog
	 * @param bToolbarVisible visibility of toolbar (true yes, false no)
	 */
	void initMenuAndToolBarActions( QList <QActionGroup *> *pToolbarActionsLstForConfig, bool bToolbarVisible );

	/** Applies setting for key shortcuts for current view.
	 * @param pKeyShortcuts - pointer to object holding key shortcuts.
	 */
	void applyKeyShortcutsForActions( KeyShortcuts *pKeyShortcuts );

	/** Sets tabulator size (in spaces) with given value.
	 * @param amountOfChars - amount of spaces for tabulator.
	 */
	void setTabWidth( uint amountOfChars );

	/** Sets end of line (EOL) character on given type.
	 * @param eEOLtype - kind of EOL character, for details @see TextViewer::EOLtype.
	 * Available are following: UNIXeol, WIN_DOSeol, UNKNOWNeol
	 */
	void setEoLtype( Preview::TextViewer::EOLtype eEOLtype );

	/** Sets given encoding for current text.
	 * @param sEncoding - encoding name
	 * Note. Method not implemented yet.
	 */
	void setEncoding( const QString &sEncoding=QString() );

	/** Sets font according given parameters
	 * @param sFamily - family of font
	 * @param nSize - size of font
	 * @param bBold - status of bold/not bold font
	 * @param bItalic - status of italic/not italic font
	 */
	void setFont( const QString &sFamily, uint nSize, bool bBold, bool bItalic );

	/** Updates highlighting actions in menu for current coloring.
	 * Current highlighting is getting from object of SyntaxHighlighter class,
	 * which is memeber of this class.
	 */
	void updateHighlightingAction();

	/** Stores current selected text (not longer than 16 characters)
	 * into given member.
	 * @param sSelections - tu zapisywany jest aktualnie zaznaczony tekst.
	 */
	void getCurrentSelection( QString &sSelections );

	/** Finds next searched string.
	 * Called by @em slotFindNext
	 */
	void findNext();

	/** Finds and replace next searched string.
	 * Called by @em slotStartReplace
	 */
	void replaceNext();

	/** Here is created object of completer text and made its connecting by signals/slots.
	 */
	void createListBox();

	/** Adujsts size of completing list to amount of entries placed on this list.
	 * @param maxHeight - maximum hight
	 * @param maxWidth - maximum weight
	 */
	void adjustListBoxSize( int maxHeight=32767, int maxWidth=32767 );

	/** Enlarges or shrinks font.
	 * @param bZoomIn if TRUE then enlarge font, otherwise shrink font
	 */
    void zoom( bool bZoomIn );

	/** Just shows content menu in given location.
	 * @oaran pos location where should be shown context menu
	 */
	void showContextMenu( const QPoint &pos );

protected:
	/** Sets geometry for text view object.
	 * @param x - new X position,
	 * @param y - new Y position,
	 * @param w - new widhth,
	 * @param h - new hight
	 */
	void setGeometry( int x, int y, int w, int h ) { m_pTextEdit->setGeometry( x,y, w,h); }

	/** Catches every events related with current object (and its children).
	 * @param o - pointer of object related with event,
	 * @param e - pointer to event,
	 * @return status of eating event. TRUE if need to pass handing further
	 * and FALSE if handling should be stopped here.
	 * Function helps in:
	 * catching key pressing event,
	 * clicking or mouse moving over the object placed on view.
	 */
	bool eventFilter( QObject *o, QEvent *e );

	/** Handles of pressing some keys in current view.
	 * @param pKeyEvent - pointer to key event.
	 * @return status of eating event. TRUE if need to pass handing further
	 * and FALSE if handling should be stopped here.
	 * Note. Ctrl+Insert makes copying to clipboard, Ctrl+Insert selects all text.
	 */
	bool keyPressed( QKeyEvent *pKeyEvent );

	/** Handles moving mouse cursor over the text view.
	 * @param pMouseEvent - pointer to mouse event.
	 * Note. Uses in mode: RENDERmode to showing target location for URL placed in document.
	 */
	void mouseMoveEvent( QMouseEvent *pMouseEvent );

	/** Only sets focus onto TextEdit object.
	 * @param pEvent pointer to ShowEvent (not used inside this function).
	 */
    void showEvent( QShowEvent *pEvent );

private slots:
	/** Slot called after every change of cursor in document.
	 */
	void slotCursorPositionChanged();

	/** Copies into the clipboard URL, over which is placed mouse cursor.
	 * Note. The code comes from Qt-3.1.1 (QLineEdit::copy()).
	 */
	void slotCopyLinkToTheClipboard();

	/** Slots with empty definition required to slot @em slotCopyLinkToTheClipboard() worked.
	 */
	void clipboardChanged() {}

	/** Stores some settings of document into the configuration file.
	 * Stored are font family, and tabulator size (abount of spaces).
	 */
	void slotSaveSettings();


	/** Shows searching bar on bottom of window.
	 */
	void slotShowFindBar();

	/** Runs searching in opened document.
	 * @param sSearchedString string to find
	 * @param bFromBeginOrCont if TRUE then search will start from begin of document otherwise will continue from current cursor position
	 * @param eStartFrom if different than SF_ignoreFlg then will be use to pointing direction of searching.
	 */
	void slotSearch( const QString &sSearchedString, bool bFromBeginOrCont, FindReplaceTextBar::StartFrom eStartFrom );

	/** Runs replacing strings in opened document.
	 * @param eReplaceType determines type of replacing. Details please find in @see FindReplaceTextBar::ReplaceType
	 * @param sSearchedString string to find
	 * @param sNewString string which will replace searched string
	 */
	void slotStartReplace ( FindReplaceTextBar::ReplaceType eReplaceType, const QString &sSearchedString, const QString &sNewString );

	/** Turns on/off begining of search from current cursor position.
	 * @param bChecked - TRUE makes search from current cursor position, FALSE makes search from begin of document.
	 */
	void slotSetSearchFromCursor ( bool bChecked );

	/** Turns on/off searching whole words only or any matching string.
	 * @param bChecked - TRUE makes search only whole words, FALSE all string matching
	 */
	void slotFindWholeWords( bool bChecked );

	/** If ReplaceBar not visible then tries to find again last searched string otherwise try to replace again found string
	 * If string is found then became higlighted.
	 */
	void slotQuestionBtn1Clicked();

	/** If ReplaceBar visible then hide it.
	 */
	void slotQuestionBtn2Clicked();

	/** Shows replace bar on bottom of window.
	 */
	void slotShowReplaceBar();

	/** Shows printing dialog for current document.
	 * After clicking OK button is trying to print document.
	 */
	void slotPrint();

	/** Shows dialog allowing to jump to passed line number.
	 * Note. Dialog also keeps the numbers of lines to which user jumped in the past.
	 */
	void slotGoToLine();

	/** Enlarges current font.
	 */
    void slotZoomIn()   { zoom(true);  }

    /** Shrings current font.
	 */
    void slotZoomOut()  { zoom(false); }


	/** Slut only runs method responsible for change of end of line character.
	 * @param pAction - option in menu pointing to proper EOL (for example Unix, Windows).
	 * Note. Slot is used by connect macro for QAction objects.
	 */
	void slotChangeEOL( QAction *pAction );

	/** Slut only runs method responsible for change of encoding of current document.
	 * @param pAction - option in menu pointing to proper EOL (for example ISO..., CP..., etc.).
	 * Slot also switches checking recently turned option in menu.
	 * Slot przeznaczony jest do wykorzystania przez 'connect' dla obiektu 'QAction'.
	 */
	void slotChangeEncoding( QAction *pAction );

	/** Switches to "Automatic" encoding.
	 * In other words: encoding will be detected Automaticaly.
	 */
    void slotAutoEncoding();


	/** Slut only runs method responsible for change of syntax highlighting..
	 * @param pAction - option in menu pointing to proper EOL (for example C++, Bash, etc.).
	 * Slot also switches checking recently turned option in menu.
	 * Note. Slot is used by connect macro for QAction objects.
	 */
	void slotChangeSyntaxHighlighting( QAction *pAction );


	/** Shows copleter list (matching strings) for current typed string.
	 * Here are collected (on this list) all mathing strings placed in document.
	 * If found typed string in the list then it is shown.
	 */
	void slotCompleteText();

	/** Called after chosing matching string placed in completer list.
	 * @param pItem - pointer to selected entry of list.
	 */
    void slotItemChosen( QListWidgetItem *pItem=0 );

	/** Applies setting coming from "text preview" page in configuration dialog.
	 * @param textViewSettings - container with settings
	 * @param sCategory - category in opened section
	 */
	void slotApplyTextViewSettings( const TextViewSettings &textViewSettings, const QString &sCategory );

	void slotPastePlainText();

};

} // namespace Preview

#endif // _TEXTVIEWER_H_
