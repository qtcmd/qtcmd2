/***************************************************************************
 *   Copyright (C) 2015 by Piotr Mierzwiñski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QApplication>
#include <QDebug>

#include "settings.h"
#include "textviewerplugindata.h"
//#include "textviewersettings.h"

namespace Preview {

TextViewerPluginData::TextViewerPluginData() : PluginData()
{
	name         = "textviewer_plugin";
	version      = "0.1.1";
	description  = QObject::tr("Plugin handles text view.");
	plugin_class = "viewer";
	mime_icon    = ""; // (used in configuration window, higher priority than own_icon)
	own_icon     = 0;  // QIcon pointer. (used in configuration window)
	mime_type    = "text/*:application/x-zerosize:application/xhtml+xml";
// 	popup_actions   = NULL; // QActionGroup pointer  (textViewwerSettings->popupActions())
    toolBar      = NULL; // QToolBar pointer      (textViewwerSettings->toolBar())
    configPage   = NULL; // QWidget pointer       (textViewwerSettings->configPage())

	Settings settings;
	QString sMimeLst = settings.value("textViewer/AdditionalMimeTypes", "").toString();
	if (! sMimeLst.isEmpty())
		mime_type += ":"+sMimeLst;
	if (mime_type.count("text/*:") > 1) // sMimeLst already contains main mime, need to remove it
		mime_type.remove(0, QString("text/*:").length());
}

} // namespace Preview

