/***************************************************************************
 *   Copyright (C) 2003 by Piotr Mierzwiñski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QRegExp>

#include "settings.h"
#include "viewer_helper.h"
#include "syntaxhighlighter.h"
#include "syntaxhighlighter_tables.h"

// TODO handle text context hightlighting based on regular expressions


SyntaxHighlighter::SyntaxHighlighter( QTextEdit *pTextEdit )
	: QSyntaxHighlighter(pTextEdit)
{
	Q_CHECK_PTR(pTextEdit);

	// --- inits rulles (list format: Context;default highlighting)
	m_slCcppRullesList.append("Base N Integer;FF00FF,,");
	m_slCcppRullesList.append("Character;008080,,");
	m_slCcppRullesList.append("Comment;808080,,Italic");
	m_slCcppRullesList.append("Data type;800000,,");
	m_slCcppRullesList.append("Decimal or Value;0000FF,,");
	m_slCcppRullesList.append("Extentions;0095FF,Bold,");
	m_slCcppRullesList.append("Floating point;800080,,");
	m_slCcppRullesList.append("Key word;000000,Bold,");
	m_slCcppRullesList.append("Normal;000000,,");
	m_slCcppRullesList.append("Preprocesor;008000,,");
	m_slCcppRullesList.append("String;FF0000,,");

	m_slHtmlRullesList.append("Comment;808080,,Italic");
	m_slHtmlRullesList.append("Data type;800000,,");
	m_slHtmlRullesList.append("Decimal or Value;0000FF,,");
	m_slHtmlRullesList.append("Key word;000000,Bold,");
	m_slHtmlRullesList.append("Normal;000000,,");
	m_slHtmlRullesList.append("String;FF0000,,");

	// --- init context map
	m_contextMap.insert("Base N Integer", BASE_N_INTEGER);
	m_contextMap.insert("Character", CHARACTER);
	m_contextMap.insert("Comment", COMMENT);
	m_contextMap.insert("Data type", DATA);
	m_contextMap.insert("Decimal or Value", DECIMALorVALUE);
	m_contextMap.insert("Extentions", EXTENSION);
	m_contextMap.insert("Floating point", FLOATING_POINT);
	m_contextMap.insert("Key word", KEYWORD);
	m_contextMap.insert("Normal", NORMAL);
	m_contextMap.insert("Preprocesor", PREPROC);
	m_contextMap.insert("String", STRING);

	// --- init mime to SyntaxHighlighting map
	m_mapMimeToSH.insert("text/x-c++src", CCpp);
	m_mapMimeToSH.insert("text/x-csrc", CCpp);
	m_mapMimeToSH.insert("text/x-chdr", CCpp);
	m_mapMimeToSH.insert("text/x-java", JAVA);
	m_mapMimeToSH.insert("text/html", HTML);
	m_mapMimeToSH.insert("application/xml", HTML);
	m_mapMimeToSH.insert("application/x-php", PHP);
	m_mapMimeToSH.insert("application/x-shellscript", BASH);
	m_mapMimeToSH.insert("application/x-perl", PERL);
	m_mapMimeToSH.insert("text/x-python", PYTHON);
	m_mapMimeToSH.insert("application/x-ruby", RUBY);
	m_mapMimeToSH.insert("application/javascript", JAVASCRIPT);
	m_mapMimeToSH.insert("text/css", CSS);
	m_mapMimeToSH.insert("application/sql", SQL);

	m_SyntaxHighlighting = NONE;
	initAttributsTable(0, QColor(0,0,0), false, false);
}

void SyntaxHighlighter::setSyntaxHighlighting( SyntaxHighlighting eSH, const QString &sMime )
{
	if (eSH != AUTO)
		m_SyntaxHighlighting = eSH;
	else
		m_SyntaxHighlighting = m_mapMimeToSH[sMime]; // NONE if not found
	if (m_SyntaxHighlighting == NONE) // no syntax highlighting
		return;

	QString sContext, sNewSH;
	QStringList slRullesList;

	if (m_SyntaxHighlighting == CCpp)
		slRullesList = m_slCcppRullesList;
	else
	if (m_SyntaxHighlighting == HTML)
		slRullesList = m_slHtmlRullesList;
// 	else
// 	if (mSyntaxHighlighting == BASH)
// 		rullesList = mBashRullesList;
// 	else
// 	if (mSyntaxHighlighting == PERL)
// 		rullesList = mPerlRullesList;

	bool bBold, bItalic;
	int ctx;
	Settings settings;
	QString sSHName = toString(m_SyntaxHighlighting);
	for (int i=0; i< slRullesList.count(); i++) {
		sContext = slRullesList[i].section(';', 0,0);
		sNewSH  = settings.value("SyntaxHighlighting/"+ sSHName+"/"+sContext, slRullesList[i].section(';', 1,1)).toString();

		bBold   = sNewSH.section(',', 1,1).toLower() == "bold";
		bItalic = sNewSH.section(',', 2,2).toLower() == "italic";
		sNewSH  = sNewSH.section(',', 0,0); // color

		ctx = m_contextMap[sContext];
		initAttributsTable(ctx, color(sNewSH), bBold, bItalic);
	}
}


//int SyntaxHighlighter::highlightParagraph(const QString & text, int endStateOfLastPara)
void SyntaxHighlighter::highlightBlock( const QString &sText )
{
	int nState = 0;
	int endStateOfLastPara = previousBlockState();

	if (m_SyntaxHighlighting == CCpp)
		nState = setSyntaxHighlightingForCCpp(sText, endStateOfLastPara);
	else
	if (m_SyntaxHighlighting == HTML)
		nState = setSyntaxHighlightingForHtml(sText, endStateOfLastPara);
	else
	if (m_SyntaxHighlighting == HEX)
		nState = setSyntaxHighlightingForHEX(sText, endStateOfLastPara);
	else
	if (m_SyntaxHighlighting == NONE)
		QSyntaxHighlighter::setFormat(0, sText.length(), QColor(0,0,0));

	if (nState != 0)
		setCurrentBlockState(nState);
}


void SyntaxHighlighter::initAttributsTable( int context, const QColor &color, bool bBold, bool bItalic )
{
	attrTab[context].color  = color;
	attrTab[context].bold   = bBold;
	attrTab[context].italic = bItalic;
}


void SyntaxHighlighter::setAttributsFor( ContextOfSH eContext )
{
	m_Color = attrTab[eContext].color;
	m_Font.setBold(attrTab[eContext].bold);
	m_Font.setItalic(attrTab[eContext].italic);
}



namespace {
/*
	bool isPartOfNumber( const QChar &c )
	{
		QString tab(".-abcdefABCDEFlLuU");
		return c.isDigit() || tab.indexOf(c) != -1;
	}
*/
	/// Returns the character at index i, or 0 if i is beyond the length of the string.
	QChar qt3_at( const QString &sText, int pos ) {
		if (pos >= sText.length())
			return 0;
		return sText.at(pos);
	}

} //namespace


int SyntaxHighlighter::setSyntaxHighlightingForCCpp( const QString &sText, int endStateOfLastPara )
{
	QChar chr;
	//QString word;
	int id, textLength= sText.length();
	int pos = 0, beginPos=0, endStat = 0;
	//uint wordLen, keywordLen;

	setAttributsFor(NORMAL);
	setFormat(0, textLength, m_Font, m_Color);

	// Putting of checking keywords, types and qt keywords before checking multiline block able to avoid highlighting them inside such blocks
	// --- cpp keywords
	setAttributsFor(KEYWORD);
	int inx = 0;
	while (cpp_keywords[inx])
	{
		QRegExp expr("\\b"+QString(cpp_keywords[inx])+"\\b");
		int index = sText.indexOf(expr);
		while (index >= 0) {
			int length = expr.matchedLength();
			setFormat(index, length, m_Font, m_Color);
			index = sText.indexOf(expr, index + length);
		}
		++inx;
	}

	// --- cpp types
	setAttributsFor(DATA);
	inx = 0;
	while (cpp_types[inx])
	{
		QRegExp expr("\\b"+QString(cpp_types[inx])+"\\b");
		int index = sText.indexOf(expr);
		while (index >= 0) {
			int length = expr.matchedLength();
			setFormat(index, length, m_Font, m_Color);
			index = sText.indexOf(expr, index + length);
		}
		++inx;
	}
	// --- qt keywords
	setAttributsFor(EXTENSION);
	inx = 0;
	while (qt_extension[inx])
	{
		QRegExp expr("\\b"+QString(qt_extension[inx])+"\\b");
		int index = sText.indexOf(expr);
		while (index >= 0) {
			int length = expr.matchedLength();
			setFormat(index, length, m_Font, m_Color);
			index = sText.indexOf(expr, index + length);
		}
		++inx;
	}

	// --- preprocesor directives
	setAttributsFor(PREPROC);
	inx = 0;
	bool bFoundPrep = false;
	while (cpp_prep_directives[inx])
	{
		QRegExp expr(QString(cpp_prep_directives[inx])+".*");
		int index = sText.indexOf(expr);
		if (! bFoundPrep)
			bFoundPrep = (index != -1);
		while (index >= 0) {
			int length = expr.matchedLength();
			int commId = sText.indexOf("//");
			if (commId == -1)
				commId = sText.indexOf("/*");
			if (commId != -1 && commId != 0) { // correction for length
				length = commId;
				pos = commId;
			}
			setFormat(index, length, m_Font, m_Color);
			index = sText.indexOf(expr, index + length);
		}
		++inx;
	}

	// --- multiline comment
	if (endStateOfLastPara == 1) {
		setAttributsFor(COMMENT);
		int p = sText.indexOf("*/");
		if (p == -1) {
			setFormat(0, textLength, m_Font, m_Color);
			return 1; // inside comment
		} else {
			pos = p + 2;
			setFormat(0, pos, m_Font, m_Color);
		}
	}

	// --- numbers
	if (! bFoundPrep) {
		inx = 0;
		QRegExp expr(QString("(0|[0-9][0-9]*|0[xX][0-9a-f]*)"));
		int index = sText.indexOf(expr);
		while (index >= 0) {
			int length = expr.matchedLength();
			if (sText.contains('.'))
				setAttributsFor(FLOATING_POINT);
			else
			if (sText.at(index) == '0' && !sText.contains('x') && length != 1)
				setAttributsFor(BASE_N_INTEGER);
			else
				setAttributsFor(DECIMALorVALUE);
			setFormat(index, length, m_Font, m_Color);
			index = sText.indexOf(expr, index + length);
		}
		++inx;
	}

	while (pos < textLength) {
		while(pos < textLength && (sText.at(pos) == ' ' || sText.at(pos) == '\t'))
			++pos; // omit some white space character

		if (pos >= textLength)
			break;

		chr = sText.at(pos);
		// --- check for one line comment type: // and /* */
		if (chr == '/') {
			if (pos + 1 < textLength) {
				QChar chr2 = sText.at(pos + 1);
				if (chr2 == '/') {
					setAttributsFor(COMMENT);
					setFormat(pos, textLength - pos, m_Font, m_Color);
					break;
				}
				else if (chr2 == '*') {
					setAttributsFor(COMMENT);
					int p = sText.indexOf("*/", pos + 2);
					if (p == -1) {
						setFormat(pos, textLength - pos, m_Font, m_Color);
						return 1; // inside multiline comment
					} else {
						setFormat(pos, p + 2, m_Font, m_Color);
						pos = p + 2;
					}
				}
			}
		}

		// --- text string combined with special characters ("text", "abc\ndef")
		if (chr == '"') {
 			if (bFoundPrep) // prevent before marking "name" as string in preprocesor directives
				break;
			setAttributsFor(STRING);
			beginPos = pos;
			pos++;
			while (pos < textLength) {
				if (sText.at(pos) == '\\') {
					setFormat(beginPos, pos-beginPos, m_Font, m_Color); // end of string
					beginPos = pos;
					setAttributsFor(CHARACTER);
					while(sText.at(pos) == '\\') {
						if (pos+1 == textLength)
							break;
						if (specChar(sText.at(pos+1)))
							pos+=2;
						else
							break;
					}

					setFormat(beginPos, pos-beginPos, m_Font, m_Color);
					if (pos != textLength) {
						setAttributsFor(STRING);
						beginPos = pos;
						if (sText.at(pos) != '\\') // if '\' then increase pos
							continue; // pos not need increased
						} else break; // end of line
				}
				else {
					if (sText.at(pos) == '"' || pos == textLength) {
						if (pos != textLength) // this same what text.at(pos) == '"') but faster
							pos++;
						setFormat(beginPos, pos-beginPos, m_Font, m_Color);
						if (pos < textLength && sText.at(pos) == '"')  pos--; // for prevent omited next char.
						break;
					}
				}
				pos++;
			}
		}

		// --- character, type: 'n' and special character
		if (chr == '\'') {
			if (qt3_at(sText, pos+1) == '\\') {
				if (! specChar(qt3_at(sText, pos+2))) {
					pos += 4;
					continue;
				}
			}

			if (pos+1 != textLength) {
				id = (qt3_at(sText,pos+1) == '\\' && specChar(qt3_at(sText,pos+2))) ? 1 : 0;
				pos += id;
				if (qt3_at(sText,pos+2) == '\'') {
					setAttributsFor(CHARACTER);
					beginPos = pos-id;
					pos += 2;
					setFormat(beginPos, (pos-beginPos)+1, m_Font, m_Color);
				}
			}
		}

		++pos;
	}//while

	return endStat;
}


int SyntaxHighlighter::setSyntaxHighlightingForHtml( const QString &sText, int endStateOfLastPara )
{
	int beginPos = 0, len= sText.length(), cutLen, offset, offsetTag;
	QString txt, txtTag;
	bool bIsTagO;
// 	qDebug("endStateOfLastPara=%d, text=%s", endStateOfLastPara, text.toLatin1().data());
// === Optimalisation idea:
// 1. reg.exp. might put into QStringList, and iterate them

	setAttributsFor(NORMAL);
	setFormat(0, len, m_Font, m_Color);

	// --- multiline comment
	if (endStateOfLastPara == 1) {
		if ((beginPos= sText.indexOf("-->")) != -1) {
			txtTag = sText;
			len = beginPos+3;
			cutLen = txtTag.length()-len;
			offsetTag = sText.length()-cutLen;
			setFormat(0, len, m_Font, m_Color);
			txtTag = txtTag.right(cutLen);
			if (txtTag.length() < 2)
				return 0;
		}
		else {
			setFormat(0, sText.length(), m_Font, m_Color);
			return 1;
		}
	}
	else {
		offsetTag = 0;
		txtTag = sText;
	}
	// --- opened tag and tag inside
	setAttributsFor(KEYWORD);
	QRegExp tagO("<[^!%?][^>]*>"); // regexp from BlueFish
	bIsTagO = false;
	while((beginPos=txtTag.indexOf(tagO)) != -1) {
		len = tagO.cap().length();
		cutLen = txtTag.length()-(beginPos+len);
		setFormat(beginPos+offsetTag, len, m_Font, m_Color);
		offsetTag = sText.length()-cutLen;
		txtTag = txtTag.right(cutLen);
		bIsTagO = true;
	}

	if (bIsTagO) {
		// - subtags
		offset = 0; txt = sText;
		setAttributsFor(DATA);
		QRegExp data(" [^<\"]*=");
		while ((beginPos=txt.indexOf(data)) != -1) {
			len = data.cap().length();
			cutLen = txt.length()-(beginPos+len);
			setFormat(beginPos+offset, len, m_Font, m_Color);
			offset = sText.length()-cutLen;
			txt = txt.right(cutLen);
		}
		// - values of subtags into ""
		offset = 0; txt = sText;
		setAttributsFor(STRING);
		QRegExp value("\"[^\"]*\""); // regexp from BlueFish
		while((beginPos=txt.indexOf(value)) != -1) {
			len = value.cap().length();
			cutLen = txt.length()-(beginPos+len);
			setFormat(beginPos+offset, len, m_Font, m_Color);
			offset = sText.length()-cutLen;
			txt = txt.right(cutLen);
		}
		// - hex numbers
		offset = 0; txt = sText;
		setAttributsFor(DECIMALorVALUE);
		//QRegExp hexval("\"?#[0-9A-Za-z]{6}\"?");
		QRegExp hexval("[\"=]#[0-9A-Za-z]{6}\""); // "#mirror" is colored into expr. href="#mirror", need to check is before is 'href'
		while((beginPos=txt.indexOf(hexval)) != -1) {
			len = hexval.cap().length();
			cutLen = txt.length()-(beginPos+len);
			setFormat(beginPos+offset, len, m_Font, m_Color);
			offset = sText.length()-cutLen;
			txt = txt.right(cutLen);
		}
		// - decimal numbers in ""
		offset = 0; txt = sText;
//		setAttributsFor(DECIMALorVALUE);
		QRegExp decval("\"[- ]?[0-9]+[% pxPXptPT]*\"");
		while((beginPos=txt.indexOf(decval)) != -1) {
			len = decval.cap().length();
			cutLen = txt.length()-(beginPos+len);
			setFormat(beginPos+offset, len, m_Font, m_Color);
			offset = sText.length()-cutLen;
			txt = txt.right(cutLen);
		}
		// - decimal numbers prefixed by '='
		offset = 0; txt = sText;
//		setAttributsFor(DECIMALorVALUE);
		QRegExp equdec("=[0-9]+%?");
		while((beginPos=txt.indexOf(equdec)) != -1) {
			len = equdec.cap().length()-1; // -/+1 it's correction for skiping '="
			cutLen = txt.length()-(beginPos+len);
			setFormat(beginPos+offset+1, len, m_Font, m_Color);
			offset = sText.length()-cutLen;
			txt = txt.right(cutLen);
		}
		// - value prefixed by '=', eg.expr.: align=center
		// ....
	}
	// --- closed tag
	offset = 0;
	txt = sText;
	setAttributsFor(KEYWORD);
	QRegExp tagC("(<\?)([^?]|\?[^>])*(\?>)"); // regexp from BlueFish
	while((beginPos=txt.indexOf(tagC)) != -1) {
		len = tagC.cap().length();
		cutLen = txt.length()-(beginPos+len);
		setFormat(beginPos+offset, len, m_Font, m_Color);
		offset = sText.length()-cutLen;
		txt = txt.right(cutLen);
	}
	// --- comment (one line)
	offset = 0;
	txt = sText;
	setAttributsFor(COMMENT);
	QRegExp coment("(<!--)([^>]|([^-]>))*-->"); // regexp from BlueFish
	while((beginPos=txt.indexOf(coment)) != -1) {
		len = coment.cap().length();
		cutLen = txt.length()-(beginPos+len);
		setFormat(beginPos+offset, len, m_Font, m_Color);
		offset = sText.length()-cutLen;
		txt = txt.right(cutLen);
	}
	// - check is it begin of multiline comment
	if (beginPos == -1) {
		QRegExp coment("(<!--)([^>]|([^-]>))*"); // regexp from BlueFish
		if ((beginPos=txt.indexOf(coment)) != -1) {
			setAttributsFor(COMMENT);
			len = coment.cap().length();
			setFormat(beginPos, len, m_Font, m_Color);
			return 1;
		}
	}

	return 0;
}


int SyntaxHighlighter::setSyntaxHighlightingForHEX( const QString &sText, int )
{
	QSyntaxHighlighter::setFormat(0, sText.length(), QColor(0,0,0));
	QSyntaxHighlighter::setFormat(0, 8, QColor(139,0,0)); // offset
	int len = sText.length()-(sText.length() - sText.indexOf('|')) - 8;// - 2;
	QSyntaxHighlighter::setFormat(9, len, QColor(0,0,139)); // hex numbers

	return 0;
}


bool SyntaxHighlighter::charOkForDigit( const QChar &chr )
{
	return (!(chr > '?' && chr < '[') && !(chr > '^' && chr < '{') && (chr != '$' || chr == '\n' || chr == '\t'));
}


bool SyntaxHighlighter::charOkBehindKeyword( const QChar &chr )
{
	// after c/cpp keywords can find folowing chars:
	const char charsTab[13] = { '(',')', '{','}', '[',']', ' ', '\t', '\n', ':', ',', ';', 0 };
	bool ok = false;
	uint idx = 0;

	while (idx < 13 && charsTab[idx] != chr)
		++idx;
	if (idx < 12)
		ok = true;

	return ok;
}


bool SyntaxHighlighter::specChar( const QChar &chr )
{
	const char charsTab[12] = { 'e','r','t','a','f','v','b','n','\'','\\','\"', 0 };
	bool ok = false;
	uint idx = 0;

	if (chr.isDigit())
		return true;

	while (idx < 12 && charsTab[idx] != chr)
		++idx;
	if (idx < 11)
		ok = true;

	return ok;
}

void SyntaxHighlighter::setFormat( int start, int count, const QFont &font, const QColor &color )
{
	QTextCharFormat format;
	format.setFont(font);
	format.setForeground(color);
	QSyntaxHighlighter::setFormat(start, count, format);
}

void SyntaxHighlighter::setFontSize( qreal nSize )
{
	m_Font.setPointSizeF(nSize);
	rehighlight();
}

QString SyntaxHighlighter::toString( SyntaxHighlighter::SyntaxHighlighting eSH )
{
	QString sSHName = "Unknown";

	if (eSH == CCpp)  sSHName = "C++";
	else
	if (eSH == HTML)  sSHName = "Html";
	else
	if (eSH == JAVA)  sSHName = "Java";
	else
	if (eSH == PHP)   sSHName = "PHP";
	else
	if (eSH == BASH)  sSHName = "Bash";
	else
	if (eSH == PERL)  sSHName = "Perl";
	else
	if (eSH == PYTHON)  sSHName = "Python";
	else
	if (eSH == RUBY)  sSHName = "Ruby";
	else
	if (eSH == JAVASCRIPT)  sSHName = "JavaScript";
	else
	if (eSH == CSS)  sSHName = "CSS";
	else
	if (eSH == SQL)  sSHName = "SQL";

	return sSHName;
}

