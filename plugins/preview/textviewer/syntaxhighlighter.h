/***************************************************************************
 *   Copyright (C) 2003 by Piotr Mierzwiñski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _SYNTAXHIGHLIGHTER_H_
#define _SYNTAXHIGHLIGHTER_H_

#include <QSyntaxHighlighter>
#include <QTextEdit>

/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
/** @short Klasa zarz�dza kolorowaniem sk�adni widoku tekstowego.
 Klaas ma za zadanie wykonanie kolorowania sk�adni dla podanego jego
 rodzaju pliku. Aby m�c wykorzysta� klase nale�y j� powi�za� z obiektem klasy
 QTextEdit, do k�rego nale�y poda� wska�nik w konstruktorze. \n
 Regu�y kolorowania s� wczytywane z pliku konfiguracyjnego
 (je�li nie zostan� tam znalezione, wtedy brane s� domy�lne).\n
 Ob�ugiwane jest kolorowanie sk�adni dla j�zyk�w: C/C++ oraz HTML/XML.
*/
class SyntaxHighlighter : public QSyntaxHighlighter
{
public:
	/** The list of syntax highlighting type.
	*/
	enum SyntaxHighlighting {
		NONE=0,
		HTML=100, CSS, LaTeX, XML, SGML, VRML, // markup
		BASH=200, PERL, PHP, PYTHON, AWK, SQL, RUBY, JAVASCRIPT, // scripts
		CCpp=300, JAVA, ASM, PASCAL, EIFFEL, PROLOG, HASKELL, ADA, // source
		HEX=400,  CHANGELOG, DIFF, MAKEFILE, RPMspec, // other
		AUTO=1000
	};

	/** Constructor.\n
	 @param pTextEdit - pointer to text view object.\n
	 Here are initialized the rules of syntax highlighting.
	*/
	SyntaxHighlighter( QTextEdit *pTextEdit );

	/** Initialization of syntax highlighting.\n
	 @param eSH - type of syntax highlighting, chekt out @see SyntaxHighlighting,
	 @param sMime - mime file
	 Here is set member describin type of syntaxhighlighting.
	 */
	void setSyntaxHighlighting( SyntaxHighlighter::SyntaxHighlighting eSH, const QString &sMime = QString() );

	/** Inicjuje podanymi warto�ciami bie��cy kontekst kolorowania.\n
	 @param context - kontekst kolorowania, patrz: @see ContextOfSH,
	 @param color - kolor w danym kontek�cie,
	 @param bBold - pogrubienie tekstu (TRUE) w danym kontek�cie,
	 @param bItalic - pochylenie tekstu (TRUE) w danym kontek�cie.\n
	 Jest tutaj inicjowana tablica atrybut�w, z kt�rej s� pobierane informacje
	 o sposobie kolorowania w funkcji ustawiaj�cej atrybuty dla kontekstu.
	 */
	void initAttributsTable( int context, const QColor &color, bool bBold, bool bItalic );

	/** Returns current type of syntax highlighting.\n
	 @em return - type of syntaxhighlighting
	 */
	SyntaxHighlighting currentHighlighting() const { return m_SyntaxHighlighting; }


	void setFont( QFont font ) { m_Font = font; }

	void setFontSize( qreal nSize );

private:
	SyntaxHighlighting m_SyntaxHighlighting;
	QColor m_Color;
	QFont  m_Font;
	QMap <QString, SyntaxHighlighting> m_mapMimeToSH;

	QStringList m_slCcppRullesList, m_slHtmlRullesList;

	struct Attribut {
		QColor color;
		bool bold;
		bool italic;
	};
	Attribut attrTab[11];

	enum ContextOfSH {
		BASE_N_INTEGER,
		CHARACTER,
		COMMENT,
		DATA,
		DECIMALorVALUE,
		EXTENSION,
		FLOATING_POINT,
		KEYWORD,
		NORMAL,
		PREPROC,
		STRING
	};

	typedef QMap<QString, ContextOfSH> ContextMap;
	ContextMap m_contextMap;

	QString toString( SyntaxHighlighting eSH );

protected:
	void setFormat( int start, int count, const QFont &font, const QColor &color );

	void highlightBlock( const QString &sText );


	/** Funkcja parsuj�ca jedn� lini� tekstu dla �r�d�a C/CPP oraz ustawiaj�ca
	 w niej pod�wietlanie sk�adni.\n
	 @param text - linia tekstu do przeparsowania (ci�g zako�czony znakiem '\n'),
	 @param endStateOfLastPara - informuje o wieloliniowym kolorowaniem
	 (wi�cej informacji szukaj w dokumentacji Qt - klasa QSyntaxHighlighting)
	 @return status zako�czenia kolorowania bie��cego paragrafu
	 */
	int setSyntaxHighlightingForCCpp( const QString &sText, int endStateOfLastPara );

	/** Funkcja parsuj�ca jedn� lini� tekstu dla �r�d�a HTML oraz ustawiaj�ca
	 w niej pod�wietlanie sk�adni.\n
	 @param text - linia tekstu do przeparsowania (ci�g zako�czony znakiem '\n'),
	 @param endStateOfLastPara - informuje o wieloliniowym kolorowaniem
	 (wi�cej informacji szukaj w dokumentacji Qt - klasa QSyntaxHighlighting)
	 @return status zako�czenia kolorowania bie��cego paragrafu
	 */
	int setSyntaxHighlightingForHtml( const QString &sText, int endStateOfLastPara );

	/** Funkcja ustawiaj�ca w kolorowanie sk�adni w jednej linii dla widoku HEX.\n
	 @param text - linia tekstu do przeparsowania (ci�g zako�czony znakiem '\n'),
	 @param endStateOfLastPara - informuje o wieloliniowym kolorowaniem
	 (wi�cej informacji szukaj w dokumentacji Qt - klasa QSyntaxHighlighting)
	 @return status zako�czenia kolorowania bie��cego paragrafu
	*/
	int setSyntaxHighlightingForHEX( const QString &sText, int endStateOfLastPara );


	/** Funkcja ustawia atrybuty: color, bold, italic (b�d�ce sk�adowymi klasy)
	 dla podanego kontekstu.\n
	 @param eContext - kontekst kolorowania.
	 */
	void setAttributsFor( ContextOfSH eContext );


	/** Funkcja wykonuje sprawdzenie czy podany znak ma specjalne znaczenie
	 w po��czeniu z '\', np. '\n'. Pomocna przy parsowaniu C/C++.\n
	 @param chr - znak do sprawdzenia,
	 @return TRUE, je�li powy�szy warunek zosta� spe�niony, w przeciwnym razie
	 FALSE.\n
	 Jest u�ywana przez funkcje kolorowania sk�adni dla C/C++.
	 */
	bool specChar( const QChar & );

	/** Funkcja sprawdza czy podany znak :\n
		@li nie jest du�� lub ma�� liter�,
		@li jest r��ny od '$',
		@li jest r�wny '\n' lub '\t'.
	 @param chr - znak do sprawdzenia,
	 @return TRUE, je�li powy�szy warunek zosta� spe�niony, w przeciwnym razie
	 FALSE.\n
	 Jest u�ywana przez funkcje kolorowania sk�adni dla C/C++.
	 */
	bool charOkForDigit( const QChar & chr);

	/** Funkcja sprawdzaj�ca czy podany zosta� znak, kt�ry mo�e wyst�pi� za s�owem
	 kluczowym j�zyka C/C++.\n
	 @param chr - znak do sprawdzenia,
	 @return TRUE, je�li powy�szy warunek zosta� spe�niony, w przeciwnym razie
	 FALSE.\n
	 Jest u�ywana przez funkcje kolorowania sk�adni dla C/C++.
	 */
	bool charOkBehindKeyword( const QChar & chr );

};

#endif
