/***************************************************************************
 *   Copyright (C) 2003 by Piotr Mierzwiñski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along withthisprogram; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QTime>
#include <QLabel>
#include <QDebug>
#include <QVBoxLayout>
#include <phonon/MediaObject>

#include "settings.h"
#include "messagebox.h"
#include "videoviewer.h"


namespace Preview {

using namespace Phonon;

VideoViewer::VideoViewer()
{
	qDebug("VideoViewer::VideoViewer.");
	setObjectName(objectName()+"/VideoViewer");

// 	m_pEditToolBarActions = NULL;
// 	m_pViewerToolBarActions = NULL;

	QVBoxLayout* pLayout = new QVBoxLayout(this);
	pLayout->setContentsMargins(1, 1, 1, 1);

	// -- prepare objects for layout
	m_pVideoPlayer = new VideoPlayer(Phonon::VideoCategory, this);
// 	connect(m_pVideoPlayer, &Phonon::VideoPlayer::finished, m_pVideoPlayer, &Phonon::VideoPlayer::deleteLater);

	m_pControlWidget = new MediaControlWidget(this);
	connect<void(MediaControlWidget::*)(MediaControlWidget::ControlCmd)>(m_pControlWidget, &MediaControlWidget::signalControl,  this, &VideoViewer::slotPlayerControl); // signal is overloaded

	// -- add objects to layout
	pLayout->addWidget(m_pVideoPlayer);
	pLayout->addWidget(m_pControlWidget);


	// -- prepare media objects
	m_pVideoMediaObj = m_pVideoPlayer->mediaObject();
	m_pVideoMediaObj->setTickInterval(1000); // in ms (here is 1 sec. interval)

	connect(m_pVideoMediaObj, &Phonon::MediaObject::tick, this, &VideoViewer::slotTick);
	connect(m_pVideoMediaObj, &Phonon::MediaObject::stateChanged, this, &VideoViewer::slotStateChanged);
	connect(m_pVideoMediaObj, &Phonon::MediaObject::currentSourceChanged, this, &VideoViewer::slotSourceChanged);

	m_pAudioOutput = new Phonon::AudioOutput(Phonon::VideoCategory, this);
	connect<void(Phonon::AudioOutput::*)(qreal)>(m_pAudioOutput, &Phonon::AudioOutput::volumeChanged,  this, &VideoViewer::slotVolumeChanged); // signal is overloaded
	connect<void(Phonon::AudioOutput::*)(bool)>(m_pAudioOutput, &Phonon::AudioOutput::mutedChanged,  this, &VideoViewer::slotMutedChanged); // signal is overloaded

	Phonon::createPath(m_pVideoMediaObj, m_pAudioOutput);
}

VideoViewer::~VideoViewer()
{
// 	qDebug() << "VideoViewer::~VideoViewer. Done for:" << objectName();
	Settings settings;
	settings.update("videoViewer/VideoAutoPlay", m_bAutoPlay);
	// save only if value has been changed or is empty
	if (settings.value("videoViewer/VideoVolume").isNull())
		settings.setValue("videoViewer/VideoVolume", 25);  // default value in percent
	else {
		uint nValue = settings.value("videoViewer/VideoVolume").toUInt();
		uint nCurrentValue = m_pAudioOutput->volume()*100;
		if (nValue != nCurrentValue)
			settings.setValue("videoViewer/VideoVolume", nCurrentValue);
	}
	if (settings.value("videoViewer/VideoSeekInverval").isNull())
		settings.setValue("videoViewer/VideoSeekInverval", 10); // default value (in seconds)
	else {
		qreal bValue = settings.value("videoViewer/VideoSeekInverval").toReal();
		if (bValue != m_nSeekInverval)
			settings.setValue("videoViewer/VideoSeekInverval", m_nSeekInverval);
	}

	m_pVideoPlayer->deleteLater();
}


void VideoViewer::init( QWidget *pParent, QList< QActionGroup * > */*pToolbarActionsLstForConfig*/, bool /*bToolbarVisible*/, KeyShortcuts */*pKeyShortcuts*/ )
{
	m_pParent = pParent;
	m_bVideoIsSeekable = false;

	Settings settings;
	m_bAutoPlay = settings.value("videoViewer/VideoAutoPlay", false).toBool();
	uint nCurrentValue = settings.value("videoViewer/VideoVolume", 25).toUInt();
	m_VideoVolume = ((qreal)nCurrentValue)/100;
	m_nSeekInverval = settings.value("videoViewer/VideoSeekInverval", 10).toReal();

	m_pControlWidget->setVolumeOutput(m_pAudioOutput);
	m_pControlWidget->init(); // update buttons with icons

	m_pControlWidget->updateButtonsState(true, false, false, false, false);
// 	initMenuAndToolBarActions(pToolbarActionsLst); // create QAction objects
}

// void VideoViewer::initMenuAndToolBarActions( QList <QActionGroup *> *pToolbarActionsLst )
// {
// }


void VideoViewer::updateContents( Viewer::ReadingMode eReadingMode, Viewer::ViewerMode eViewerMode, const QString &sMime, const QByteArray &baBinBuffer )
{
	if (eReadingMode == NONE) {
		return;
	}
	qDebug() << "VideoViewer::updateContents. sMime (file name):" << sMime;

	m_pAudioOutput->setVolume(m_VideoVolume);
	m_pControlWidget->setSeekSlider(m_pVideoMediaObj);
	m_pControlWidget->setVolumeSlider(m_pAudioOutput);
	m_pControlWidget->updateCurrentTime("00:00:00");
	m_pControlWidget->setTotalTime("00:00:00");
	m_pControlWidget->updateVolumeSpin(m_VideoVolume*100);

	QString sFileName = sMime.split('|').at(1);

	m_pVideoMediaObj->setCurrentSource(QUrl::fromLocalFile(sFileName)); // load is invoked also

	if (m_bAutoPlay)
		slotPlayerControl(MediaControlWidget::MEDIA_PLAY);
}

/*
void VideoViewer::updateKeyShortcuts( KeyShortcuts *pKeyShortcuts )
{
	if (pKeyShortcuts == NULL) {
		qDebug() << "VideoViewer::updateKeyShortcuts. One can't update key shortcuts - empty pointer!";
		return;
	}
	m_pKeyShortcuts = pKeyShortcuts;

// 	m_pKeyShortcuts->append(Print_VideoViewer, tr("Print"), Qt::CTRL+Qt::Key_P);

	m_pKeyShortcuts->updateEntryList("fileViewShortcuts/"); // update from config file
	applyKeyShortcuts(m_pKeyShortcuts); // for actions in current window
}

void VideoViewer::applyKeyShortcuts( KeyShortcuts *pKeyShortcuts )
{
	if (! pKeyShortcuts) {
		qDebug() << "VideoViewer::applyKeyShortcuts. One can't apply key shortcuts - empty pointer!";
		return;
	}
	m_pKeyShortcuts = pKeyShortcuts;

// 	m_pPrintA->setShortcut(pKeyShortcuts->key(Print_VideoViewer));
}
*/
		// ----------- SLOTs -----------

void VideoViewer::slotPlayerControl( MediaControlWidget::ControlCmd eControlCmd )
{
// 	qDebug() << "VideoViewer::slotPlayerControl. eControlCmd:" << eControlCmd;
	QString sMessage;
	if (eControlCmd == MediaControlWidget::MEDIA_PLAY) {
		m_pVideoMediaObj->play();
		sMessage = tr("Video is playing");
		m_pControlWidget->updateButtonsState(false, true, true, true, true);
	}
	else
	if (eControlCmd == MediaControlWidget::MEDIA_PAUSE) {
		m_pVideoMediaObj->pause();
		sMessage = tr("Paused video");
	}
	else
	if (eControlCmd == MediaControlWidget::MEDIA_STOP) {
// 		m_pVideoMediaObj->seek(0.0); // in contrary order doesn't work (seek to 0 not necessary)
		m_pVideoMediaObj->stop();
		sMessage = tr("Stopped video");
		m_pControlWidget->updateButtonsState(true, false, false, false, false);
	}
	else
	if (eControlCmd == MediaControlWidget::MEDIA_FORWARD || eControlCmd == MediaControlWidget::MEDIA_BACKWARD) {
		if (m_bVideoIsSeekable) {
			qreal newPosition = 0, seekInterval = m_nSeekInverval*1000; // intveral unit are seconds
			if (eControlCmd == MediaControlWidget::MEDIA_FORWARD )
				newPosition = m_pVideoMediaObj->currentTime() + seekInterval;
			else
			if (eControlCmd == MediaControlWidget::MEDIA_BACKWARD)
				newPosition = m_pVideoMediaObj->currentTime() - seekInterval;

			m_pVideoMediaObj->seek(newPosition);
			slotTick(m_pVideoMediaObj->currentTime());
			sMessage = tr("Video has been sought");
		}
	}

	if (! sMessage.isEmpty())
		emit signalUpdateStatus(sMessage);
}

void VideoViewer::slotTick( qint64 nTime )
{ // code came from Qt4.6 phonon example (qmusicplayer) and update for hour by author
	QTime displayTime((nTime / 3600000) % 60, (nTime / 60000) % 60, (nTime / 1000) % 60);

	m_pControlWidget->updateCurrentTime(displayTime.toString("hh:mm:ss"));
}

void VideoViewer::slotStateChanged( Phonon::State newState, Phonon::State oldState )
{ // code came from Qt4.6 phonon example (qmusicplayer), updated in case Phonon::PlayingState by author
// 	qDebug() << "VideoViewer::slotStateChanged." << newState;

	switch (newState) {
		case Phonon::ErrorState:
			if (m_pVideoMediaObj->errorType() == Phonon::FatalError) {
				QMessageBox::warning(this, tr("Fatal Error"), m_pVideoMediaObj->errorString());
			} else {
				QMessageBox::warning(this, tr("Error"), m_pVideoMediaObj->errorString());
			}
			break;

		case Phonon::LoadingState:
			emit signalUpdateStatus(tr("Video is loading..."));
			break;

		case Phonon::BufferingState:
			emit signalUpdateStatus(tr("Video is buffering..."));
			break;

		case Phonon::PlayingState: {
			m_pControlWidget->updateButtonsState(false, true, true, true, true);
			qint64 nTotalTime = m_pVideoMediaObj->totalTime();
			QTime displayTime((nTotalTime / 3600000) % 60, (nTotalTime / 60000) % 60, (nTotalTime / 1000) % 60);
			m_pControlWidget->setTotalTime(displayTime.toString("hh:mm:ss"));
			m_bVideoIsSeekable = m_pVideoMediaObj->isSeekable();
			break;
		}
		case Phonon::StoppedState:
			m_pControlWidget->updateButtonsState(true, false, false, false, false);
			m_pControlWidget->updateCurrentTime("00:00:00");
			emit signalUpdateStatus(tr("Video loaded"));
			break;

		case Phonon::PausedState:
			m_pControlWidget->updateButtonsState(true, false , true, false, false);
			break;

		default:
			;
	}
}

void VideoViewer::slotSourceChanged( Phonon::MediaSource mediaSrc )
{
// 	qDebug() << "VideoViewer::slotSourceChanged. mediaSrc:" << mediaSrc;
	m_pControlWidget->updateCurrentTime("00:00:00");
	m_pControlWidget->setTotalTime("00:00:00");
	emit signalUpdateStatus(tr("Video loaded"));
	emit signalStatusChanged(Vfs::READY_TO_READ, objectName());
	emit signalStatusChanged(Vfs::READ_FILE, objectName());
}

void VideoViewer::slotVolumeChanged( qreal newVolume )
{
// 	qDebug() << "VideoViewer::slotVolumeChanged. newVolume" << newVolume*100;
	m_pControlWidget->updateVolumeSpin(newVolume*100);
	if (m_pAudioOutput->volume() != 0.0)
		m_oldVolumeValue = m_pAudioOutput->volume();
}

void VideoViewer::slotMutedChanged( bool bMuted )
{
	if (bMuted) {
		m_pControlWidget->updateVolumeSpin(0);
	}
	else {
		m_pAudioOutput->setVolume(m_oldVolumeValue);
		m_pControlWidget->updateVolumeSpin(m_oldVolumeValue*100);
		m_pControlWidget->setVolumeSlider(m_pAudioOutput);
	}
// 	QString sMessage = (bMuted) ? tr("Audio muted") : tr("Audio unmuted");
// 	emit signalUpdateStatus(sMessage, 2000);
	// FIXME: after pressing second time of mute (so audio is unmuted) on status bar is showing message "Audio mutes" (previous message)
	// good to have would be option: "don't save current message" as parameter to signalUpdateStatus  (current option acts different)
}

} // namespace Preview
