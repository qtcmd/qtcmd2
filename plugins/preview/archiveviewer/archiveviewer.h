/***************************************************************************
 *   Copyright (C) 2003 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _ARCHIVEVIEWER_H
#define _ARCHIVEVIEWER_H

#include <QPlainTextEdit>

#include "viewer.h" // includes also: keyshortcuts.h, plugindata.h, subsystemconst.h
#include "iconcache.h"
#include "subsystem.h"
#include "findreplacetextbar.h"

class QAction;
class QActionGroup;


namespace Preview {
/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class ArchiveViewer : public Viewer
{
	Q_OBJECT
public:
	ArchiveViewer();
	~ArchiveViewer();

	/**
	 * @param pParent pointer to parent this view
	 * @param pToolbarActionsLstForConfig actions group used in config dialog
	 * @oaram bToolbarVisible if true then tool bar will be visible otherwise will be hidden
	 * @param pKeyShortcuts pointer to KeyShortcuts object
	 */
	void init( QWidget *pParent, QList< QActionGroup* > *pToolbarActionsLstForConfig, bool bToolbarVisible, KeyShortcuts *pKeyShortcuts );

	/** Returns curret mode of viewer. For details please check: @see Preview::Viewer::ViewerMode
	 * @return viewer mode.
	 */
	ViewerMode  viewerMode() const { return RENDER_mode; }

	/** Status of availability of RENDER_mode.
	 * @return TRUE if RENDER_mode is available, otherwise FALSE
	 */
	bool        viewerModeAvailable( const QString &sMime, ViewerMode eViewerMode ) const {
		return (eViewerMode == RENDER_mode);
	}

	PluginData *pluginData() const { return m_pPluginData; };
	void        setPluginData( PluginData *pPluginData ) { m_pPluginData = pPluginData; }

	/** Status showing if plugin itself handles loading file or required internal loading.
	 * Application supports loading internaly, but some plugins not required that.
	 * @return TRUE if loadin must be doing internaly, otherwise FALSE.
	 */
	bool        requiredIntLoadingOfFile() const { return false; }

	/** Updates shortcuts container according to given KeyShortcuts object.
	 * @param pKeyShortcuts container of key shortcuts.
	 */
	void        initKeyShortcuts( KeyShortcuts *pKeyShortcuts );

	/** @brief Updates the viewer.
	 * @param eReadingMode - reading data mode, for details please check @see Preview::Viewer::ReadingMode
	 * @param eViewerMode - view mode, for details please check @see Preview::Viewer::ViewerMode
	 * @param sInMime - opened file mime
	 * @param baBinBuffer - buffer contains data for filling view.
	 */
	void        updateContents( Preview::Viewer::ReadingMode eReadingMode, Preview::Viewer::ViewerMode eViewerMode, const QString &sInMime, const QByteArray &baBinBuffer );

	/** Clean view.
	 */
	void        clear() {
		m_pPlainTextEdit->clear();
	}

	/** Returns status of visibility the FindReplaceTextBar.
	 @return TRUE if bar is visible, otherwise FALSE.
	*/
	bool        findReplaceBarVisible() const { return m_pFindReplaceTextBar->isVisible(); }

	/** Returns status of visibility the ErrorMessageBar.
	 @return TRUE if bar is visible, otherwise FALSE.
	*/
 	bool        errorMessageBarVisible() const { return m_pFindReplaceTextBar->errorMessageBarVisible(); }

	/** Shows or hides the FindReplaceTextBar.
	 * @param bVisible if TRUE the shows the FindReplaceTextBar otherwise hide it.
	 */
 	void        setFindReplaceBarVisible( bool bVisible );  // used in FileViewer::closeEvent

	/** Shows or hides the ErrorMessageBar.
	 * @param bVisible if TRUE the shows the FindReplaceTextBar otherwise hide it.
	 */
	void        setErrorMessageBarVisible( bool bVisible ) {
		m_pFindReplaceTextBar->setMessageBarVisible (bVisible);
	}

	/** Returns pointer to actions group related with Menu Bar, exactly Edit menu.\n
	 @return pointer to actions group.
	 */
	QActionGroup *editMenuBarActions() const { return m_pEditMenuBarActions; }

	/** Returns pointer to actions group related with Menu Bar, exactly Viewer menu.\n
	 @return pointer to actions group.
	 */
	QActionGroup *viewMenuBarActions() const { return m_pViewMenuBarActions; }

	/** Returns pointer to actions group related with Tool Bar, exactly Edit menu.\n
	 @return pointer to actions group.
	 */
	QActionGroup *editToolBarActions() const { return m_pEditToolBarActions; }

	/** Returns pointer to actions group related with Tool Bar, exactly Viewer menu.\n
	 @return pointer to actions group.
	 */
	QActionGroup *viewToolBarActions() const { return m_pViewToolBarActions; }


private:
	PluginData *m_pPluginData;
	QWidget    *m_pParent;
	IconCache  *m_pIconCache;

	// viewer content
	QPlainTextEdit *m_pPlainTextEdit;
	QString m_sTxtBuffer;

	// property of view
	QString m_sFontFamily;
	int m_nFontSize, m_nTabWidth;
	int m_bFontItalic;

	// find dialog
	QString m_sFindNextSearchString, m_sReplacedString;
	bool m_bCaseSensitive, m_bWholeWordsOnly, m_bStartFindFromCursorPos, m_bFindForward;
	bool m_bFindOnce, m_bFromBegin, m_bBtnContinueHasPressed;
	bool m_PrevResultOfFind;
	QStringList m_slSearchStringList;

	// actions
	QActionGroup *m_pViewMenuBarActions, *m_pEditMenuBarActions; // group for main window
	QActionGroup *m_pEditToolBarActions, *m_pViewToolBarActions; // group for main window
	QAction *m_pCopyA, *m_pSelectAllA;
	QAction *m_pFindA, *m_pFindNextA, *m_pPrintA/*, mPrintPreviewA*/;
	QAction *m_pZoomInA, *m_pZoomOutA;
	QAction *m_pCopyTBA, *m_pSelectAllTBA;
	QAction *m_pFindTBA, *m_pFindNextTBA, *m_pPrintTBA/*, mPrintPreviewTBA*/;
	QAction *m_pZoomInTBA, *m_pZoomOutTBA;
	QAction *m_pConfigPrevAct;

	QList <QActionGroup *> *m_pToolbarActionsPrevLst;

	// keyshortcuts
	ViewerMode m_eViewerMode;
	KeyShortcuts *m_pKeyShortcuts;

	Vfs::Subsystem *m_subsystem;

	FindReplaceTextBar *m_pFindReplaceTextBar;

	bool m_UpOrDownPressed;
	QString m_sPrevSearchStr;

	/** Ids for actions related with key shortcuts.
	*/
	enum ActionsOfTextViewer {
		Find_TextViewer=4000,
		FindNext_TextViewer,
		Copy_TextViewer,
		SelectAll_TextViewer,
		Print_TextViewer,
		ZoomIn_TextViewer,
		ZoomOut_TextViewer
	};


	QIcon icon( const QString &sIconName ) const {
		return m_pIconCache->icon(sIconName);
	}

	void setFont( const QString &sFamily, uint nSize, bool bItalic );
	bool saveToFile( const QString &sFileName );
	void updateListOfSearchStrings( const QString &sSearchString );
	void findNext();
	void getCurrentSelection( QString &sSelections );
	void showContextMenu( const QPoint &pos );
	void zoom( bool bZoomIn );

	void initMenuAndToolBarActions( QList <QActionGroup *> *pToolbarActionsLst );

	void applyKeyShortcuts( KeyShortcuts *pKeyShortcuts );

protected:
	bool eventFilter( QObject *pObj, QEvent *pEvent );

private slots:
	void slotSubsystemStatusChanged( Vfs::SubsystemStatus subsysStat, const QString &sRequestingPanel );

	/** Shows searching bar on bottom of window.
	 */
	void slotShowFindBar();

	/** Runs searching in opened document.
	 * @param sSearchedString string to find
	 * @param bFromBeginOrCont if TRUE then search will start from begin of document otherwise will continue from current cursor position
	 * @param eStartFrom if different than SF_ignoreFlg then will be use to pointing direction of searching.
	 */
	void slotSeach( const QString &sSearchedString, bool bFromBeginOrCont, FindReplaceTextBar::StartFrom eStartFrom );

	/** Turns on/off begining of search from current cursor position.
	 * @param bChecked - TRUE makes search from current cursor position, FALSE makes search from begin of document.
	 */
	void slotStartSearchFromCursor( bool bChecked );

	/** Turns on/off searching whole words only or any matching string.
	 * @param bChecked - TRUE makes search only whole words, FALSE all string matching
	 */
	void slotFindWholeWords( bool bChecked );

	/** Tries to find again last searching string.
	 * If string will be found then became higlighted.
	 */
	void slotFindNext();

	void slotPrint();

    void slotZoomIn()   { zoom(true);  }
    void slotZoomOut()  { zoom(false); }

public slots:
	void slotSaveSettings();

};

} // namespace Preview

#endif // _ARCHIVEVIEWER_H
