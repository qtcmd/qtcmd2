/***************************************************************************
 *   Copyright (C) 2003 by Piotr Mierzwiñski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along withthisprogram; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QLabel>
#include <QDebug>
#include <QVBoxLayout>
#include <QtPrintSupport/QPrinter>
#include <QtPrintSupport/QPrintDialog>

#include "settings.h"
#include "iconcache.h"
#include "messagebox.h"
#include "archiveviewer.h"
#include "../../pluginmanager.h"


namespace Preview {

ArchiveViewer::ArchiveViewer()
{
	qDebug("ArchiveViewer::ArchiveViewer.");
	setObjectName(objectName()+"/ArchiveViewer");

	m_pEditToolBarActions = nullptr;
	m_pViewToolBarActions = nullptr;
	m_pViewMenuBarActions = nullptr;
	m_pEditMenuBarActions = nullptr;
	m_pKeyShortcuts = nullptr;
	m_pPluginData = nullptr;
	m_pParent = nullptr;

	m_pIconCache = IconCache::instance();

	m_sFindNextSearchString = "";
	m_bStartFindFromCursorPos = true;
	m_PrevResultOfFind = false;
	m_bWholeWordsOnly = false;
	m_bCaseSensitive = true;
	m_bFindForward = true;
	m_bFromBegin = true;
	m_bFindOnce = false;

	QVBoxLayout* pLayout = new QVBoxLayout(this);
	pLayout->setContentsMargins(1, 1, 1, 1);

	m_pPlainTextEdit = new QPlainTextEdit(this);
	m_pPlainTextEdit->setReadOnly(true);
	m_pPlainTextEdit->viewport()->installEventFilter(this); // for grabing mouse events from the QTextEdit obj.
	m_pPlainTextEdit->installEventFilter(this); // for grabing KeyEvent from the View obj.

	m_pFindReplaceTextBar = new FindReplaceTextBar(this, "archiveViewer");
	m_pFindReplaceTextBar->setVisible(false);
	connect(m_pFindReplaceTextBar, &FindReplaceTextBar::signalSearch, this, &ArchiveViewer::slotSeach);

	// -- add objects to layout
	pLayout->addWidget(m_pPlainTextEdit);
	pLayout->addWidget(m_pFindReplaceTextBar);
}

ArchiveViewer::~ArchiveViewer()
{
	qDebug() << "ArchiveViewer::~ArchiveViewer. DESTRUCTOR" << objectName();
}


void ArchiveViewer::init( QWidget *pParent, QList< QActionGroup * > *pToolbarActionsLstForConfig, bool /*bToolbarVisible*/, KeyShortcuts *pKeyShortcuts )
{
	qDebug() << "ArchiveViewer::init.";
	m_pParent = pParent;
	m_subsystem  = nullptr;

	QActionGroup *pToolsToolBar = new QActionGroup(this);
	pToolsToolBar->setObjectName(tr("Archive preview"));

	setObjectName(pParent->objectName()+"_ArchiveViewer");
	Settings settings;
	QString sSearchStringList = settings.value("archiveViewer/SearchHistory", "").toString();
	m_slSearchStringList = sSearchStringList.split('|');

	QString sFont = settings.value("archiveViewer/Font", "Monospace,9").toString();
	// - parse 'font'
	m_sFontFamily = sFont.split(',').at(0); // sFont.left(sFont.indexOf(','));
	QString sFontSize = sFont.section(',', 1,1).simplified();
	QString sItalic   = sFont.section(',', 2,2).simplified().toLower();

	bool ok;
	m_nFontSize = sFontSize.toInt(&ok);
	if (! ok)
		m_nFontSize = 10; // default size

	m_bFontItalic = (sItalic == "italic");
	setFont(m_sFontFamily, m_nFontSize, m_bFontItalic);

	initMenuAndToolBarActions(pToolbarActionsLstForConfig); // create QAction objects
	initKeyShortcuts(pKeyShortcuts);
}

void ArchiveViewer::initMenuAndToolBarActions( QList <QActionGroup *> *pToolbarActionsLst )
{
// 	m_pFindA = pViewMenu->addAction(icon("edit/find"), tr("&Find"), this, &ArchiveViewer::slotShowFindBar, QKeySequence(Qt::CTRL + Qt::Key_F));
// 	m_pCopyA = new QAction(icon("edit/copy"), tr("&Copy"), m_pPlainTextEdit, &QPlainTextEdit::copy, QKeySequence(Qt::CTRL + Qt::Key_C));

	// --- actions for EDIT menuBar
	m_pCopyA = new QAction(icon("edit/copy"), tr("&Copy"), this);
	m_pCopyA->setShortcut(Qt::CTRL + Qt::Key_C);
	m_pFindA = new QAction(icon("edit/find"),  tr("&Find") + " (Ctrl+F)", this);
	m_pFindA->setShortcut(Qt::CTRL + Qt::Key_F);
	m_pFindNextA = new QAction(icon("edit/findnext"), tr("Find &next") + " (F3)", this);
	m_pFindNextA->setShortcut(Qt::Key_F3);
	m_pSelectAllA = new QAction(icon("edit/select-all"), tr("Select &all"), this);
	m_pSelectAllA->setShortcut(Qt::CTRL + Qt::Key_A);

	connect(m_pCopyA, &QAction::triggered, m_pPlainTextEdit, &QPlainTextEdit::copy);
	connect(m_pFindA, &QAction::triggered, this, &ArchiveViewer::slotShowFindBar);
	connect(m_pFindNextA, &QAction::triggered, this, &ArchiveViewer::slotFindNext);
	connect(m_pSelectAllA, &QAction::triggered, m_pPlainTextEdit, &QPlainTextEdit::selectAll);

	// --- actions for EDIT toolBar
	m_pCopyTBA = new QAction(icon("edit/copy"),  tr("&Copy") + " (Ctrl+C)", this);
	m_pSelectAllTBA = new QAction(icon("edit/select-all"),  tr("Select &all") + " (Ctrl+A)", this);

	connect(m_pCopyTBA, &QAction::triggered, m_pPlainTextEdit, &QPlainTextEdit::copy);
	connect(m_pSelectAllTBA, &QAction::triggered, m_pPlainTextEdit, &QPlainTextEdit::selectAll);

	// --- actions for Print and Zoom
	m_pPrintA = new QAction(icon("document/print"), tr("&Print"), m_pPlainTextEdit);
	m_pPrintA->setShortcut(Qt::CTRL + Qt::Key_P);
	m_pZoomInA = new QAction(icon("zoom/in"), tr("Zoom &in")+" (Ctrl+Plus)", m_pPlainTextEdit);
	m_pZoomInA->setShortcut(Qt::CTRL + Qt::Key_Plus);
	m_pZoomOutA = new QAction(icon("zoom/out"), tr("Zoom &out")+" (Ctrl+Minus)", m_pPlainTextEdit);
	m_pZoomOutA->setShortcut(Qt::CTRL + Qt::Key_Minus);

	// --- separators
	QAction *pEditMenuSep1 = new QAction(this);
	pEditMenuSep1->setSeparator(true);

	// --- EDIT menu action group
	m_pEditMenuBarActions = new QActionGroup(this); // main window menu
	m_pEditMenuBarActions->addAction(m_pFindA);
	m_pEditMenuBarActions->addAction(m_pFindNextA);
	m_pEditMenuBarActions->addAction(pEditMenuSep1);
	m_pEditMenuBarActions->addAction(m_pCopyA);
	m_pEditMenuBarActions->addAction(m_pSelectAllA);

	// Print & Zoom group
	QActionGroup *pZoomActions = new QActionGroup(m_pPlainTextEdit);
	pZoomActions->addAction(m_pZoomInA);
	pZoomActions->addAction(m_pZoomOutA);

	m_pViewMenuBarActions = new QActionGroup(this); // main window menu
	m_pViewMenuBarActions->addAction(m_pPrintA);
	m_pViewMenuBarActions->addAction(m_pZoomInA);
	m_pViewMenuBarActions->addAction(m_pZoomOutA);

	connect(m_pPrintA, &QAction::triggered, this, &ArchiveViewer::slotPrint);
	connect(m_pZoomInA, &QAction::triggered, this, &ArchiveViewer::slotZoomIn);
	connect(m_pZoomOutA, &QAction::triggered, this, &ArchiveViewer::slotZoomOut);

	// --- update toolBars
	m_pEditToolBarActions = new QActionGroup(m_pPlainTextEdit); // group for main window
	m_pEditToolBarActions->addAction(m_pFindA);
	m_pEditToolBarActions->addAction(m_pFindNextA);
	// -- edit actions
	m_pEditToolBarActions->addAction(pEditMenuSep1);
	m_pEditToolBarActions->addAction(m_pCopyTBA);
	m_pEditToolBarActions->addAction(m_pSelectAllTBA);

	QAction *pViewerToolBarSep = new QAction(this);
	pViewerToolBarSep->setSeparator(true);

	m_pViewToolBarActions = new QActionGroup(this);
	m_pViewToolBarActions->addAction(pViewerToolBarSep);
	m_pViewToolBarActions->addAction(m_pPrintA);
	m_pViewToolBarActions->addAction(m_pZoomInA);
	m_pViewToolBarActions->addAction(m_pZoomOutA);

	//
	pToolbarActionsLst->append(m_pEditMenuBarActions);
	pToolbarActionsLst->append(pZoomActions);
	pToolbarActionsLst->append(m_pViewMenuBarActions);
	pToolbarActionsLst->append(m_pEditToolBarActions);
	pToolbarActionsLst->append(m_pViewToolBarActions);
}


void ArchiveViewer::updateContents( Viewer::ReadingMode eReadingMode, Viewer::ViewerMode eViewerMode, const QString &sInMime, const QByteArray &baBinBuffer )
{
	if (eReadingMode == NONE) {
		return;
	}
	QString sFN = "ArchiveViewer::updateContents.";
	qDebug() << sFN << "sMime (with file name):" << sInMime;

	QString sMime = sInMime.split('|').at(0);
	PluginManager *pPluginManager = PluginManager::instance();
	if (! pPluginManager->isPluginRegistrered(sMime, "subsystem")) { // current file mime is not related to any registrered plugin
		qDebug() << sFN  << "Incomming mime is not registrered. Probably this file type is not handled by any plugin.";
		return;
	}
	if (m_subsystem != nullptr) {
		disconnect(m_subsystem, static_cast<void (Vfs::Subsystem::*)(Vfs::SubsystemStatus, const QString &)>(&Vfs::Subsystem::signalStatusChanged), this, &ArchiveViewer::slotSubsystemStatusChanged);
	}
	Vfs::Subsystem *pSubsystem = pPluginManager->subsystem(sMime);
	if (pSubsystem != nullptr)
		m_subsystem = pSubsystem;
	else {
		qDebug() << sFN << "Cannot get plugin object!";
		return;
	}
	qDebug() << sFN << "For view use following subsystem:" << m_subsystem;
	connect<void(Vfs::Subsystem::*)(Vfs::SubsystemStatus, const QString &)>(m_subsystem, &Vfs::Subsystem::signalStatusChanged,  this, &ArchiveViewer::slotSubsystemStatusChanged); // signal is overloaded
	emit signalUpdateStatus(tr("Loading archive content"));

	QString sFileName = sInMime.split('|').at(1);
	Vfs::URI uri(sFileName);
	uri.setHost(sFileName);
	m_subsystem->close(); // switch on ForceReopen flag. Thanks that we are sure that buffer is filled with properly values
	m_subsystem->open(uri, objectName());
}


void ArchiveViewer::initKeyShortcuts( KeyShortcuts *pKeyShortcuts )
{
	if (pKeyShortcuts == nullptr) {
		qDebug() << "ArchiveViewer::updateKeyShortcuts. One can't update key shortcuts - empty pointer!";
		return;
	}
	m_pKeyShortcuts = pKeyShortcuts;

	m_pKeyShortcuts->setKey(Find_TextViewer, Qt::CTRL+Qt::Key_F, Qt::CTRL+Qt::Key_F, tr("Find"));
	m_pKeyShortcuts->setKey(FindNext_TextViewer, Qt::Key_F3, Qt::Key_F3, tr("Find next"));
	m_pKeyShortcuts->setKey(Copy_TextViewer, Qt::CTRL+Qt::Key_C, Qt::CTRL+Qt::Key_C, tr("Copy"));
	m_pKeyShortcuts->setKey(SelectAll_TextViewer, Qt::CTRL+Qt::Key_A, Qt::CTRL+Qt::Key_A, tr("Select all"));
	m_pKeyShortcuts->setKey(Print_TextViewer, Qt::CTRL+Qt::Key_P, Qt::CTRL+Qt::Key_P, tr("Print"));
// 	m_pKeyShortcuts->setKey(ZoomIn_TextViewer,  Qt::CTRL+Qt::Key_Plus, Qt::CTRL+Qt::Key_Plus, tr("Zoom in"));
// 	m_pKeyShortcuts->setKey(ZoomOut_TextViewer, Qt::CTRL+Qt::Key_Minus, Qt::CTRL+Qt::Key_Minus, tr("Zoom out"));

	m_pKeyShortcuts->updateEntryList("fileViewShortcuts/"); // update from config file
	applyKeyShortcuts(m_pKeyShortcuts); // for actions in current window
}

void ArchiveViewer::applyKeyShortcuts( KeyShortcuts *pKeyShortcuts )
{
	if (! pKeyShortcuts) {
		qDebug() << "ArchiveViewer::applyKeyShortcuts. One can't apply key shortcuts - empty pointer!";
		return;
	}
	m_pKeyShortcuts = pKeyShortcuts;

	m_pFindA->setShortcut(pKeyShortcuts->keyCode(Find_TextViewer));
	m_pFindNextA->setShortcut(pKeyShortcuts->keyCode(FindNext_TextViewer));
	m_pCopyA->setShortcut(pKeyShortcuts->keyCode(Copy_TextViewer));
	m_pPrintA->setShortcut(pKeyShortcuts->keyCode(Print_TextViewer));
// 	m_pZoomInA->setShortcut(pKeyShortcuts->keyCode(ZoomIn_TextViewer));
// 	m_pZoomOutA->setShortcut(pKeyShortcuts->keyCode(ZoomOut_TextViewer));
}

// --- TODO: independ saving to file from filesystem
bool ArchiveViewer::saveToFile( const QString &sFileName )
{
	QFile file(sFileName);
	bool bChecked = false;
	if (! file.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
		MessageBox::critical(this, tr("Save file")+" - QtCommander 2", "",
		 "\""+ sFileName +"\""+"\n\n"+ tr("Cannot write to this file"), bChecked
		);
		return false;
	}
	m_pPlainTextEdit->document()->setModified(false);

	QTextStream stream(&file);
	stream << m_pPlainTextEdit->toPlainText();
	file.close();

	return true;
}

void ArchiveViewer::updateListOfSearchStrings( const QString  &sSearchString )
{
	bool bNotFound = true;
	for (int i=0; i<m_slSearchStringList.count(); i++) {
		if (m_slSearchStringList[i] == sSearchString) {
			bNotFound = false;
			break;
		}
	}
	if (bNotFound)
		m_slSearchStringList.append(sSearchString);
}

void ArchiveViewer::setFont( const QString &sFamily, uint nSize, bool bItalic )
{
	QTextCharFormat charFormat;
	charFormat.setFontFamily("Monospace");
	charFormat.setFontItalic(bItalic);
	charFormat.setFontPointSize(nSize);
	m_pPlainTextEdit->setCurrentCharFormat(charFormat);
}

void ArchiveViewer::findNext()
{
	// FIXME: Found string is not highlighted
	if (m_sFindNextSearchString.isEmpty()) {
		slotShowFindBar();
		return;
	}
	QTextDocument::FindFlags flags; // = 0 was deprecated

	if (m_bCaseSensitive || m_pFindReplaceTextBar->caseSensitive())
		flags |= QTextDocument::FindCaseSensitively;
	if (m_bWholeWordsOnly || m_pFindReplaceTextBar->findWholeWordsOnly())
		flags |= QTextDocument::FindWholeWords;
	if (! m_bFindForward)
		flags |= QTextDocument::FindBackward;

	qDebug() << "ArchiveViewer::findNext. m_sFindNextSearchString:" << m_sFindNextSearchString << "flags:" << flags;

	m_PrevResultOfFind = ! m_bFromBegin;
	m_sPrevSearchStr = m_sFindNextSearchString;
	if (m_pFindReplaceTextBar->regularExpression())
		m_bFromBegin = ! m_pPlainTextEdit->find(QRegExp(m_sFindNextSearchString), flags);
	else
		m_bFromBegin = ! m_pPlainTextEdit->find(m_sFindNextSearchString, flags);
	m_pFindReplaceTextBar->slotUpdateMatchingStatus((m_bFromBegin ? FindReplaceTextBar::MS_notMatch : FindReplaceTextBar::MS_match));

	m_bFindOnce = false;
	if (! m_bFromBegin) {
		m_bFindOnce = true;
		return;
	}

	FindReplaceTextBar::SearchDirection eSD = m_pFindReplaceTextBar->searchDirection();
	// only if user walks through document then show error message
	if (eSD == FindReplaceTextBar::SD_home || eSD == FindReplaceTextBar::SD_up || eSD == FindReplaceTextBar::SD_down || eSD == FindReplaceTextBar::SD_end) {
		QString s0 = (m_bFindForward) ? tr("end")   : tr("start");
		QString s1 = (m_bFindForward) ? tr("begin") : tr("end");
		if (! m_pFindReplaceTextBar->startFromCursor() || m_bBtnContinueHasPressed) {
			//MessageBox::information(this, tr("Find text")+" - QtCommander 2", m_sFindNextSearchString+"\n\n"+tr("Text not found"));
			qDebug() << "ArchiveViewer::findNext. (1). m_PrevResultOfFind:" << m_PrevResultOfFind;
			m_pFindReplaceTextBar->setMessage(FindReplaceTextBar::MT_Info, tr("Search reached")+" "+s0+" "+tr("of the document"), false, tr("Continue from")+" "+s1);
			if (m_PrevResultOfFind && m_sPrevSearchStr == m_sFindNextSearchString) {
				m_pFindReplaceTextBar->slotUpdateMatchingStatus(FindReplaceTextBar::MS_match);
				m_bFromBegin = false; // to set correct value in m_PrevResultOfFind in case of next search
			}
			return;
		}
		else {
			m_pFindReplaceTextBar->setMessage(FindReplaceTextBar::MT_Info, tr("Search reached")+" "+s0+" "+tr("of the document"), false, tr("Continue from")+" "+s1);
			qDebug() << "ArchiveViewer::findNext. (2). m_PrevResultOfFind:" << m_PrevResultOfFind;
			if (m_PrevResultOfFind && m_sPrevSearchStr == m_sFindNextSearchString) {
				m_pFindReplaceTextBar->slotUpdateMatchingStatus(FindReplaceTextBar::MS_match);
				m_bFromBegin = false; // to set correct value in m_PrevResultOfFind in case of next search
			}
		}
	}
}

void ArchiveViewer::setFindReplaceBarVisible( bool bVisible )
{
	// deselect recently found text
	QTextCursor cur = m_pPlainTextEdit->textCursor();
	cur.clearSelection();
	m_pPlainTextEdit->setTextCursor(cur);
	// ... and hide FindReplace bar
	m_pFindReplaceTextBar->setVisible(bVisible);
}

void ArchiveViewer::getCurrentSelection( QString &sSelections )
{
	if (! m_pPlainTextEdit->textCursor().hasSelection())
		return;

	sSelections = m_pPlainTextEdit->textCursor().selectedText();
	if (m_eViewerMode == RENDER_mode)
		sSelections.remove(0, 20); // remove string <!--StartFragment-->

	int id = 0;
	if (sSelections.length() > 16)
		sSelections = sSelections.left(16);
	if ((id= sSelections.indexOf('\n')) != -1)
		sSelections = sSelections.left(id-1);
}

void ArchiveViewer::showContextMenu( const QPoint &pos )
{
	//qDebug() << "ArchiveViewer::showContextMenu()";
	QMenu *pCtxMenu = m_pPlainTextEdit->createStandardContextMenu();
	if (pCtxMenu == nullptr) {
		qDebug() << "ArchiveViewer::showContextMenu. Internal ERROR. Cannot get StandardContextMenu!";
		return;
	}
	QString sFindKCode    = (m_pKeyShortcuts != nullptr) ? m_pKeyShortcuts->keyStr(Find_TextViewer) : "CTRL+F";
	QString sSelection;

	if (m_pPlainTextEdit->textCursor().hasSelection()) {
		getCurrentSelection(sSelection);
		pCtxMenu->addSeparator(); // gets selected text and and alternatively cuts it if is too long for menu
		pCtxMenu->addAction(icon("edit/find"), tr("&Find")+": \""+ sSelection +"\"\t"+ sFindKCode, this, SLOT(slotFind()));
	}

	pCtxMenu->exec(m_pPlainTextEdit->mapToGlobal(QPoint(pos.x(), pos.y()+2)));
	delete pCtxMenu;
	// -- clear selection
// 	QTextCursor txtCursor = m_pPlainTextEdit->cursorForPosition(pos);
// 	m_pPlainTextEdit->textCursor().setPosition(txtCursor.position());
// 	m_pPlainTextEdit->setTextCursor(txtCursor);
}

void ArchiveViewer::zoom( bool bZoomIn )
{
// 	if (bZoomIn)
// 		m_pPlainTextEdit->zoomIn(1);
// 	else
// 		m_pPlainTextEdit->zoomOut(1);

	uint currentSize = m_pPlainTextEdit->currentCharFormat().fontPointSize();
// 	qDebug() << "ArchiveViewer::slotZoom. bZoomIn:" << bZoomIn << "currentSize:" << currentSize;

	if (bZoomIn) {
		if (currentSize < 24)
			m_pZoomOutA->setEnabled(true);
		else
			m_pZoomInA->setEnabled(false);
	}
	else {
		if (currentSize > 8)
			m_pZoomInA->setEnabled(true);
		else
			m_pZoomOutA->setEnabled(false);
	}

	QString sZoomType = bZoomIn ? tr("Zoom in") : tr("Zoom out");
	emit signalUpdateStatus(QString(tr("Current font size")+" : %1  (%2)").arg(currentSize).arg(sZoomType), 3000, true);
}


		// ----------- SLOTs -----------

void ArchiveViewer::slotSubsystemStatusChanged( Vfs::SubsystemStatus subsysStat, const QString &sRequestingPanel )
{
	qDebug() << "ArchiveViewer::slotSubsystemStatusChanged. Status:" << Vfs::toString(subsysStat) << "Command:" << Vfs::toString(m_subsystem->currentCommand());
// 	qDebug() <<  "--input:" << Vfs::toString(subsysStat) << "command:" << Vfs::toString(m_subsystem->currentCommand()) << "sent by:" << sRequestingPanel << "current name:" << objectName();
	if (subsysStat != Vfs::ERROR_OCCURED)
		emit signalStatusChanged(subsysStat, sRequestingPanel); // forward signal to SubsystemManager class

	if (sRequestingPanel != objectName())
		return;
	if (subsysStat == Vfs::VIEW_COMPL) {
		if (m_subsystem->currentCommand() == Vfs::VIEW) {
			QString sBuffer;
			m_subsystem->getListBuffer(sBuffer);
			//QString sBuffer = slListBuffer.join("\n");
			m_pPlainTextEdit->setPlainText(sBuffer);
			m_pPlainTextEdit->setFocus();
			m_pPlainTextEdit->moveCursor(QTextCursor::Start);
			emit signalUpdateStatus(tr("Preview archive loaded"), 1000, false);
			emit signalStatusChanged(Vfs::READ_FILE, sRequestingPanel);
			QString sMessage = QString("%1 "+tr("bytes, mode: RAW")).arg(m_pPlainTextEdit->document()->toPlainText().length());
			emit signalUpdateStatus(sMessage, 0, false);
		}
	}
	else if (subsysStat == Vfs::ERROR_OCCURED) {
		qDebug() << "ArchiveViewer::slotSubsystemStatusChanged. ERROR_OCCURED";
	}
}


void ArchiveViewer::slotSaveSettings()
{
	QString sFont = m_pPlainTextEdit->currentCharFormat().fontFamily()+","+QString::number(m_pPlainTextEdit->currentCharFormat().fontPointSize());
	Settings settings;
	settings.setValue("archiveViewer/Font", sFont);
}

void ArchiveViewer::slotShowFindBar()
{
	QString sCurrentSelection;
	if (m_pPlainTextEdit->textCursor().hasSelection())
		sCurrentSelection = m_pPlainTextEdit->textCursor().selectedText();

	m_pFindReplaceTextBar->init(sCurrentSelection, false); // false - document is not editable
	m_pFindReplaceTextBar->setVisible(true);

	m_bBtnContinueHasPressed = false;
}

void ArchiveViewer::slotSeach( const QString &sSearchedString, bool bFromBeginOrCont, FindReplaceTextBar::StartFrom eStartFrom )
{
	if (sSearchedString.isEmpty()) {
		// deselect recently found text
		QTextCursor cur = m_pPlainTextEdit->textCursor();
		cur.clearSelection();
		m_pPlainTextEdit->setTextCursor(cur);
		// move cursor to begin if it is required
		if (bFromBeginOrCont && ! m_pFindReplaceTextBar->startFromCursor())
			m_pPlainTextEdit->moveCursor(QTextCursor::Start);
		return;
	}

	FindReplaceTextBar::SearchDirection eSearchDirection = m_pFindReplaceTextBar->searchDirection();

	if (eStartFrom == FindReplaceTextBar::SF_ignoreFlg) {
		if (bFromBeginOrCont && ! m_pFindReplaceTextBar->startFromCursor())
			m_pPlainTextEdit->moveCursor(QTextCursor::Start);
	}
	else { // force search direction
		if (eStartFrom == FindReplaceTextBar::SF_FromVeryBegining) {
			eSearchDirection = FindReplaceTextBar::SD_down;
			if (bFromBeginOrCont)
				m_pPlainTextEdit->moveCursor(QTextCursor::Start);
		}
		else
		if (eStartFrom == FindReplaceTextBar::SF_FromEnd) {
			eSearchDirection = FindReplaceTextBar::SD_up;
			if (bFromBeginOrCont)
				m_pPlainTextEdit->moveCursor(QTextCursor::End);
		}
	}

	m_UpOrDownPressed = (eSearchDirection != FindReplaceTextBar::SD_down_inc); // should be also check: SD_none and SD_behind
// 	qDebug() << "TextViewer::slotSearch. m_UpOrDownPressed:" << m_UpOrDownPressed << m_pFindReplaceTextBar->toString(eSearchDirection);
	m_sFindNextSearchString = sSearchedString;
	m_bFindForward = (eSearchDirection == FindReplaceTextBar::SD_down || eSearchDirection == FindReplaceTextBar::SD_end || eSearchDirection == FindReplaceTextBar::SD_down_inc);
// 	qDebug() << "TextViewer::slotSearch. m_bFindForward:" << m_bFindForward << "SearchedString:" << sSearchedString;
	m_sPrevSearchStr = m_sFindNextSearchString;
	m_PrevResultOfFind = ! m_bFromBegin;

	findNext();
}

void ArchiveViewer::slotStartSearchFromCursor( bool bChecked )
{
// 	qDebug() << "ArchiveViewer::slotStartSearchFromCursor" << bChecked;
	// deselect recently found text (clear selection)
	QTextCursor cur = m_pPlainTextEdit->textCursor();
	cur.clearSelection();
	m_pPlainTextEdit->setTextCursor(cur);
	m_pFindReplaceTextBar->slotUpdateMatchingStatus(FindReplaceTextBar::MS_none);
}

void ArchiveViewer::slotFindWholeWords( bool bChecked )
{
// 	qDebug() << "ArchiveViewer::slotFindWholeWords" << bChecked;
	// deselect recently found text (clear selection)
	QTextCursor cur = m_pPlainTextEdit->textCursor();
	cur.clearSelection();
	m_pPlainTextEdit->setTextCursor(cur);
	m_pFindReplaceTextBar->slotUpdateMatchingStatus(FindReplaceTextBar::MS_none);
}

void ArchiveViewer::slotFindNext()
{
	m_bBtnContinueHasPressed = false;

 	FindReplaceTextBar::SearchDirection eSD = m_pFindReplaceTextBar->searchDirection();
	m_UpOrDownPressed = (eSD != FindReplaceTextBar::SD_down_inc);
	m_bFindForward = ((eSD == FindReplaceTextBar::SD_down || eSD == FindReplaceTextBar::SD_down_inc) && m_UpOrDownPressed);

	m_pPlainTextEdit->moveCursor(m_bFindForward ? QTextCursor::Start : QTextCursor::End);
	findNext();
}


void ArchiveViewer::slotPrint()
{
	QPrinter printer(QPrinter::HighResolution);
	printer.setFullPage(true);
	QPrintDialog *pDlg = new QPrintDialog(&printer, this);
	if (m_pPlainTextEdit->textCursor().hasSelection())
		pDlg->addEnabledOption(QAbstractPrintDialog::PrintSelection);
	pDlg->setWindowTitle(tr("Print Document"));
	if (pDlg->exec() == QDialog::Accepted) {
		emit signalUpdateStatus(tr("Printing..."));
		m_pPlainTextEdit->print(&printer);
		emit signalUpdateStatus(tr("Printing completed"), 2000, false);
	} else {
		emit signalUpdateStatus(tr("Printing aborted"), 2000, false);
	}
	delete pDlg;
}

bool ArchiveViewer::eventFilter( QObject *pObj, QEvent *pEvent )
{
	if (pObj == m_pPlainTextEdit) {
		if (pEvent->type() == QEvent::ContextMenu) {
			QContextMenuEvent *cme =  (QContextMenuEvent *)pEvent;
			showContextMenu(cme->pos());
			return true; // eat current event
		}
		else
		if (pEvent->type() == QEvent::KeyPress) {
			QKeyEvent *pKeyEvent = (QKeyEvent *)pEvent;
			int key    = pKeyEvent->key();
			int keyMod = pKeyEvent->modifiers();
			if (key == Qt::Key_Home) {
				if (keyMod == Qt::CTRL) {
					m_pPlainTextEdit->moveCursor(QTextCursor::Start);
				}
			}
			else
			if (key == Qt::Key_End) {
				if (keyMod == Qt::CTRL) {
					m_pPlainTextEdit->moveCursor(QTextCursor::End);
				}
			}
			//return true; // eat current event
		}
	}
	else
	if (pObj == m_pPlainTextEdit->viewport()) {
		if (pEvent->type() == QEvent::MouseButtonPress) {
			QMouseEvent *pMouseEvent = (QMouseEvent *)pEvent;
			if (pMouseEvent->button() == Qt::RightButton)  {
				QContextMenuEvent event(QContextMenuEvent::Mouse, pMouseEvent->pos());
				QApplication::sendEvent(m_pPlainTextEdit, &event);
 				return true; // eat current event (prevent before resending event RightButtonPressed)
			}
		}
	}

	return QWidget::eventFilter(pObj, pEvent);  // standard event processing
}


} // namespace Preview
