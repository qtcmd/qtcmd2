/***************************************************************************
 *   Copyright (C) 2015 by Piotr Mierzwiński <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "archiveviewerplugindata.h"
//#include "archiveviewersettings.h"

namespace Preview {

ArchiveViewerPluginData::ArchiveViewerPluginData() : PluginData()
{
	name         = "archiveviewer_plugin";
	version      = "0.1.0";
	description  = QObject::tr("Plugin lists and shows preview of archive and compressed file.");
	plugin_class = "viewer";
	mime_icon    = ""; // (used in configuration window, higher priority than own_icon)
	own_icon     = 0;  // QIcon pointer. (used in configuration window)
	mime_type    = "application/x-tar:application/x-compressed-tar:application/x-bzip-compressed-tar:application/x-lzma-compressed-tar:application/x-xz-compressed-tar:application/x-lzip:application/x-compress:application/zip:application/x-rar:application/vnd.rar";
	// FIXME compressors types should be detected
	mime_type   += ":application/gzip:application/x-bzip:application/x-lzma:application/x-xz:application/x-lzip:application/x-compress";
// 	popup_actions   = NULL; // QActionGroup pointer  (videoViewerSettings->popupActions())
	toolBar      = NULL; // QToolBar pointer      (videoViewerSettings->toolBar())
	configPage   = NULL; // QWidget pointer       (videoViewerSettings->configPage())
}

} // namespace Preview
