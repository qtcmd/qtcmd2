/***************************************************************************
 *   Copyright (C) 2003 by Piotr Mierzwiñski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef _AUDIOVIEWER_H_
#define _AUDIOVIEWER_H_

#include "viewer.h" // includes also: keyshortcuts.h, plugindata.h, subsystemconst.h
#include <phonon/MediaObject>
#include <phonon/AudioOutput>

#include "mediacontrolwidget.h"


// class QAction;
// class QActionGroup;

namespace Preview {
/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class AudioViewer : public Viewer
{
	Q_OBJECT
public:
	AudioViewer();
	~AudioViewer();

	/**
	 * @param pParent pointer to parent this view
	 * @param pToolbarActionsLstForConfig actions group used in config dialog
	 * @oaram bToolbarVisible if true then tool bar will be visible otherwise will be hidden
	 * @param pKeyShortcuts pointer to KeyShortcuts object
	 */
	void init( QWidget *pParent, QList< QActionGroup* > *pToolbarActionsLstForConfig, bool bToolbarVisible, KeyShortcuts *pKeyShortcuts );

	/** Returns curret mode of viewer. For details please check: @see Preview::Viewer::ViewerMode \n
	 * @return viewer mode.
	 */
	ViewerMode  viewerMode() const { return RENDER_mode; }
	bool        viewerModeAvailable( const QString &sMime, ViewerMode eViewerMode ) const {
		return (eViewerMode == RENDER_mode);
	}

	PluginData *pluginData() const { return m_pPluginData; };
	void        setPluginData( PluginData *pPluginData ) { m_pPluginData = pPluginData; }

	/** Status showing if plugin handles itself loading file or required internal loading.
	 * Application supports loading internaly, but some plugins not required that.
	 * @return TRUE if loadin must be doing internaly, otherwise FALSE.
	 */
	bool        requiredIntLoadingOfFile() const { return false; }

// 	void        updateKeyShortcuts( KeyShortcuts *pKeyShortcuts );

	/** @brief Updates the viewer.\n
	 * @param eReadingMode - reading data mode, for details please check @see Preview::Viewer::ReadingMode
	 * @param eViewerMode - view mode, for details please check @see Preview::Viewer::ViewerMode
	 * @param sMime - opened file mime
	 * @param baBinBuffer - buffer contains data for filling view.\n
	 */
	void        updateContents( Viewer::ReadingMode eReadingMode, Viewer::ViewerMode eViewerMode, const QString &sMime, const QByteArray &baBinBuffer );

	void        clear() {
		slotPlayerControl(MediaControlWidget::MEDIA_STOP);
	}

private:
	PluginData *m_pPluginData;
	QWidget    *m_pParent;

	bool  m_bAutoPlay;
	qreal m_AudioVolume;
	qreal m_oldVolumeValue;
	bool  m_bAudioIsSeekable;
	int   m_nSeekInverval;

	ViewerMode m_eViewerMode;
	KeyShortcuts *m_pKeyShortcuts;

 	Phonon::MediaObject *m_pAudioMediaObj;
	Phonon::AudioOutput *m_pAudioOutput;
	MediaControlWidget *m_pControlWidget;


	/** Ids for actions related with key shortcuts.
	*/
// 	enum ActionsOfAudioViewer {
// 		PLAY_AudioViewer=3000,
// 		PAUSE_AudioViewer,
// 		STOP_AudioViewer,
// 		SEEK_FF_AudioViewer,
// 		SEEK_BK_AudioViewer,
// 	};


// 	void initMenuAndToolBarActions( QList <QActionGroup *> *pToolbarActionsLst );

// 	void applyKeyShortcuts( KeyShortcuts *pKeyShortcuts );

private slots:
	void slotPlayerControl( MediaControlWidget::ControlCmd eControlCmd );

	void slotTick( qint64 nTime );
	void slotStateChanged( Phonon::State newState, Phonon::State oldState );
	void slotSourceChanged( Phonon::MediaSource mediaSrc );

	void slotVolumeChanged( qreal newVolume );
	void slotMutedChanged( bool bM1uted );

// 	void slotEnqueueNextSource();

// public slots:
// 	void slotSaveSettings()  {}

};

} // namespace Preview

#endif // _AUDIOVIEWER_H_
