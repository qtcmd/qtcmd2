/***************************************************************************
 *   Copyright (C) 2003 by Piotr Mierzwiñski <piom@nes.pl>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along withthisprogram; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QTime>
#include <QLabel>
#include <QDebug>
#include <QVBoxLayout>
#include <phonon/MediaObject>

#include "settings.h"
#include "messagebox.h"
#include "audioviewer.h"


namespace Preview {

using namespace Phonon;

AudioViewer::AudioViewer()
{
	qDebug("AudioViewer::AudioViewer.");
	setObjectName(objectName()+"/AudioViewer");

// 	m_pEditToolBarActions = NULL;
// 	m_pViewerToolBarActions = NULL;

	QVBoxLayout* pLayout = new QVBoxLayout(this);
	pLayout->setContentsMargins(1, 1, 1, 1);

	// -- prepare objects for layout
	QLabel *pAudioLab = new QLabel(this);
	// TODO: for mp3 files show in AudioLab id3 information

	m_pControlWidget = new MediaControlWidget(this);
	connect(m_pControlWidget, &MediaControlWidget::signalControl, this, &AudioViewer::slotPlayerControl);

	// -- add objects to layout
	pLayout->addWidget(pAudioLab);
	pLayout->addWidget(m_pControlWidget);


	// -- prepare media objects
	m_pAudioMediaObj = new Phonon::MediaObject(this);
	m_pAudioMediaObj->setTickInterval(1000); // in ms (here is 1 sec. interval)

	connect(m_pAudioMediaObj, &Phonon::MediaObject::tick, this, &AudioViewer::slotTick);
	connect(m_pAudioMediaObj, &Phonon::MediaObject::stateChanged, this, &AudioViewer::slotStateChanged);
	connect(m_pAudioMediaObj, &Phonon::MediaObject::currentSourceChanged, this, &AudioViewer::slotSourceChanged);
// 	connect(m_pAudioMediaObj, &Phonon::MediaObject::aboutToFinish, this, &AudioViewer::slotEnqueueNextSource);

	m_pAudioOutput = new Phonon::AudioOutput(Phonon::MusicCategory, this);
	connect(m_pAudioOutput, &Phonon::AudioOutput::volumeChanged, this, &AudioViewer::slotVolumeChanged);
	connect(m_pAudioOutput, &Phonon::AudioOutput::mutedChanged, this, &AudioViewer::slotMutedChanged);

	Phonon::createPath(m_pAudioMediaObj, m_pAudioOutput);
}

AudioViewer::~AudioViewer()
{
	Settings settings;
	settings.update("audioViewer/AudioAutoPlay", m_bAutoPlay);
	// save only if value has been changed or is empty
	if (settings.value("audioViewer/AudioVolume").isNull())
		settings.setValue("audioViewer/AudioVolume", 25);  // default value in percent
	else {
		uint nValue = settings.value("audioViewer/AudioVolume").toUInt();
		uint nCurrentValue = m_pAudioOutput->volume()*100;
		if (nValue != nCurrentValue)
			settings.setValue("audioViewer/AudioVolume", nCurrentValue);
	}
	if (settings.value("audioViewer/AudioSeekInverval").isNull())
		settings.setValue("audioViewer/AudioSeekInverval", 10); // default value (in seconds)
	else {
		qreal bValue = settings.value("audioViewer/AudioSeekInverval").toReal();
		if (bValue != m_nSeekInverval)
			settings.setValue("audioViewer/AudioSeekInverval", m_nSeekInverval);
	}
}


void AudioViewer::init( QWidget *pParent, QList< QActionGroup * > */*pToolbarActionsLstForConfig*/, bool /*bToolbarVisible*/, KeyShortcuts */*pKeyShortcuts*/ )
{
	m_pParent = pParent;
	m_bAudioIsSeekable = false;

	Settings settings;
	m_bAutoPlay = settings.value("audioViewer/AudioAutoPlay", true).toBool();
	uint nCurrentValue = settings.value("audioViewer/AudioVolume", 25).toUInt();
	m_AudioVolume = ((qreal)nCurrentValue)/100;
	m_nSeekInverval = settings.value("audioViewer/AudioSeekInverval", 10).toReal();

	m_pControlWidget->setVolumeOutput(m_pAudioOutput);
	m_pControlWidget->init(); // update buttons with icons

	m_pControlWidget->updateButtonsState(true, false, false, false, false);
// 	initMenuAndToolBarActions(pToolbarActionsLstForConfig); // create QAction objects
}

// void AudioViewer::initMenuAndToolBarActions( QList <QActionGroup *> *pToolbarActionsLst )
// {
// }

void AudioViewer::updateContents( Viewer::ReadingMode eReadingMode, Viewer::ViewerMode eViewerMode, const QString &sMime, const QByteArray &baBinBuffer )
{
	if (eReadingMode == NONE) {
		return;
	}
	qDebug() << "AudioViewer::updateContents. sMime (file name):" << sMime;

	m_pAudioOutput->setVolume(m_AudioVolume);
	m_pControlWidget->setSeekSlider(m_pAudioMediaObj);
	m_pControlWidget->setVolumeSlider(m_pAudioOutput);
	m_pControlWidget->updateCurrentTime("00:00:00");
	m_pControlWidget->setTotalTime("00:00:00");
	m_pControlWidget->updateVolumeSpin(m_AudioVolume*100);

	QString sFileName = sMime.split('|').at(1);

	m_pAudioMediaObj->setCurrentSource(QUrl::fromLocalFile(sFileName)); // as exception in sMime we can find file name

	if (m_bAutoPlay)
		slotPlayerControl(MediaControlWidget::MEDIA_PLAY);
	// TODO if AutoPlay is turned on then one can enqueue all opened audio files
}

/*
void AudioViewer::updateKeyShortcuts( KeyShortcuts *pKeyShortcuts )
{
	if (pKeyShortcuts == NULL) {
		qDebug() << "AudioViewer::updateKeyShortcuts. One can't update key shortcuts - empty pointer!";
		return;
	}
	m_pKeyShortcuts = pKeyShortcuts;

// 	m_pKeyShortcuts->append(Print_AudioViewer, tr("Print"), Qt::CTRL+Qt::Key_P);

	m_pKeyShortcuts->updateEntryList("fileViewShortcuts/"); // update from config file
	applyKeyShortcuts(m_pKeyShortcuts); // for actions in current window
}

void AudioViewer::applyKeyShortcuts( KeyShortcuts *pKeyShortcuts )
{
	if (! pKeyShortcuts) {
		qDebug() << "AudioViewer::applyKeyShortcuts. One can't apply key shortcuts - empty pointer!";
		return;
	}
	m_pKeyShortcuts = pKeyShortcuts;

// 	m_pPrintA->setShortcut(pKeyShortcuts->key(Print_AudioViewer));
}
*/
		// ----------- SLOTs -----------

void AudioViewer::slotPlayerControl( MediaControlWidget::ControlCmd eControlCmd )
{
// 	qDebug() << "AudioViewer::slotPlayerControl. eControlCmd:" << eControlCmd;
	QString sMessage;
	if (eControlCmd == MediaControlWidget::MEDIA_PLAY) {
		m_pAudioMediaObj->play();
		sMessage = tr("Audio is playing");
		m_pControlWidget->updateButtonsState(false, true, true, true, true);
	}
	else
	if (eControlCmd == MediaControlWidget::MEDIA_PAUSE) {
		m_pAudioMediaObj->pause();
		sMessage = tr("Paused audio");
	}
	else
	if (eControlCmd == MediaControlWidget::MEDIA_STOP) {
// 		m_pAudioMediaObj->seek(0.0); // in contrary order doesn't work (seek to 0 not necessary)
		m_pAudioMediaObj->stop();
		sMessage = tr("Stopped audio");
		m_pControlWidget->updateButtonsState(true, false, false, false, false);
	}
	else
	if (eControlCmd == MediaControlWidget::MEDIA_FORWARD || eControlCmd == MediaControlWidget::MEDIA_BACKWARD) {
		if (m_bAudioIsSeekable) {
			qreal newPosition = 0, seekInterval = m_nSeekInverval*1000; // intveral unit are seconds
			if (eControlCmd == MediaControlWidget::MEDIA_FORWARD)
				newPosition = m_pAudioMediaObj->currentTime() + seekInterval;
			else
			if (eControlCmd == MediaControlWidget::MEDIA_BACKWARD)
				newPosition = m_pAudioMediaObj->currentTime() - seekInterval;

			m_pAudioMediaObj->seek(newPosition);
			slotTick(m_pAudioMediaObj->currentTime());
			sMessage = tr("Audio has been sought");
		}
	}

	if (! sMessage.isEmpty())
		emit signalUpdateStatus(sMessage);
}

void AudioViewer::slotTick( qint64 nTime )
{ // code came from Qt4.6 phonon example (qmusicplayer) and update for hour by author
	QTime displayTime((nTime / 3600000) % 60, (nTime / 60000) % 60, (nTime / 1000) % 60);

	m_pControlWidget->updateCurrentTime(displayTime.toString("hh:mm:ss"));
}

void AudioViewer::slotStateChanged( Phonon::State newState, Phonon::State oldState )
{ // code came from Qt4.6 phonon example (qmusicplayer), updated in case Phonon::PlayingState by author
// 	qDebug() << "AudioViewer::slotStateChanged." << newState;

	switch (newState) {
		case Phonon::ErrorState:
			if (m_pAudioMediaObj->errorType() == Phonon::FatalError) {
				QMessageBox::warning(this, tr("Fatal Error"), m_pAudioMediaObj->errorString());
			} else {
				QMessageBox::warning(this, tr("Error"), m_pAudioMediaObj->errorString());
			}
			break;

		case Phonon::LoadingState:
			emit signalUpdateStatus(tr("Audio is loading..."));
			break;

		case Phonon::BufferingState:
			emit signalUpdateStatus(tr("Audio is buffering..."));
			break;

		case Phonon::PlayingState: {
				m_pControlWidget->updateButtonsState(false, true, true, true, true);
				qint64 nTotalTime = m_pAudioMediaObj->totalTime();
				QTime displayTime((nTotalTime / 3600000) % 60, (nTotalTime / 60000) % 60, (nTotalTime / 1000) % 60);
				m_pControlWidget->setTotalTime(displayTime.toString("hh:mm:ss"));
				m_bAudioIsSeekable = m_pAudioMediaObj->isSeekable();
				break;
		}
		case Phonon::StoppedState:
				m_pControlWidget->updateButtonsState(true, false, false, false, false);
				m_pControlWidget->updateCurrentTime("00:00:00");
				emit signalUpdateStatus(tr("Audio loaded"));
				break;

		case Phonon::PausedState:
				m_pControlWidget->updateButtonsState(true, false , true, false, false);
				break;

		default:
			;
	}
}

void AudioViewer::slotSourceChanged( Phonon::MediaSource mediaSrc )
{
// 	qDebug() << "AudioViewer::slotSourceChanged. mediaSrc:" << mediaSrc;
	m_pControlWidget->updateCurrentTime("00:00:00");
	m_pControlWidget->setTotalTime("00:00:00");
	emit signalUpdateStatus(tr("Audio loaded"));
	emit signalStatusChanged(Vfs::READY_TO_READ, objectName());
	emit signalStatusChanged(Vfs::READ_FILE, objectName());
}

void AudioViewer::slotVolumeChanged( qreal newVolume )
{
// 	qDebug() << "AudioViewer::slotVolumeChanged. newVolume" << newVolume*100;
	m_pControlWidget->updateVolumeSpin(newVolume*100);
	if (m_pAudioOutput->volume() != 0.0)
		m_oldVolumeValue = m_pAudioOutput->volume();
}

void AudioViewer::slotMutedChanged( bool bMuted )
{
	if (bMuted) {
		m_pControlWidget->updateVolumeSpin(0);
	}
	else {
		m_pAudioOutput->setVolume(m_oldVolumeValue);
		m_pControlWidget->updateVolumeSpin(m_oldVolumeValue*100);
		m_pControlWidget->setVolumeSlider(m_pAudioOutput);
	}
// 	QString sMessage = (bMuted) ? tr("Audio muted") : tr("Audio unmuted");
// 	emit signalUpdateStatus(sMessage, 2000);
	// FIXME: after pressing second time of mute (so audio is unmuted) on status bar is showing message "Audio mutes" (previous message)
	// good to have would be option: "don't save current message" as parameter to signalUpdateStatus  (current option acts different)
}

// void AudioViewer::slotEnqueueNextSource()
// {
//     m_pAudioMediaObj->enqueue("/home/username/music/song.mp3");
// }

} // namespace Preview
